<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoMediosEnterar.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoUsuarios.php');

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoGrados= new DaoGrados();
$DaoMediosEnterar= new DaoMediosEnterar();
$DaoCiclos= new DaoCiclos();
$ciclo=$DaoCiclos->getActual();
$DaoUsuarios= new DaoUsuarios();

links_head("Alumnos | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Ofertas de inter&eacute;s</h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul class="row list-design">
                        <li class="col-md-4 widget" onclick="mostrar_filtro()"><div class="stats-left"><i class="fa fa-filter"></i> Filtros</div></li>
                        <li class="col-md-4 widget states-mdl" onclick="mostrar_box_email()"><div class="stats-left"><i class="fa fa-envelope"></i> Email</div></li>
                        <li class="col-md-4 widget states-last" onclick="download_excel()"><div class="stats-left"><i class="fa fa-download"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td>Ciclo de inter&eacute;s</td>
                                <td>Fecha de captura</td>
                                <td><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            

                             if($ciclo->getId()>0){
                             $query = "SELECT * FROM inscripciones_ulm 
                             JOIN Ofertas_interes ON inscripciones_ulm.Id_ins=Ofertas_interes.Id_alum
                             WHERE tipo=1  AND inscripciones_ulm.Id_plantel=".$_usu->getId_plantel()." AND Id_ciclo=".$ciclo->getId()." ORDER BY DateCreated DESC";
                             $consulta=$base->advanced_query($query);
                             $row_consulta = $consulta->fetch_assoc();
                             $totalRows_consulta= $consulta->num_rows;
                             if($totalRows_consulta>0){
                               do{
                                       
                                        $nombre_ori="";
                                        $oferta = $DaoOfertas->show($row_consulta['Id_ofe']);
                                        $esp = $DaoEspecialidades->show($row_consulta['Id_esp']);
                                        if ($row_consulta['Id_ori'] > 0) {
                                           $ori = $DaoOrientaciones->show($row_consulta['Id_ori']);
                                           $nombre_ori = $ori->getNombre();
                                         }
                                         $opcion="Plan por materias"; 
                                         if($row_consulta['Opcion_pago']==2){
                                           $opcion="Plan completo";  
                                         }

                                         $MedioEnt="";
                                         if($row_consulta['Id_medio_ent']>0){
                                           $medio=$DaoMediosEnterar->show($row_consulta['Id_medio_ent']);
                                           $MedioEnt=$medio->getMedio();
                                         }
                                        $ciclo=$DaoCiclos->show($row_consulta['Id_ciclo']);
                                        
                                      ?>
                                              <tr id_alum="<?php echo $row_consulta['Id_ins'];?>">
                                                <td><?php echo $row_consulta['Matricula'] ?></td>
                                                <td style="width: 115px;"><?php echo $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins'] ?></td>
                                                <td><?php echo $oferta->getNombre_oferta(); ?></td>
                                                <td><?php echo $esp->getNombre_esp(); ?></td>
                                                <td><?php echo $nombre_ori; ?></td>
                                                <td><?php echo $ciclo->getClave(); ?></td>
                                                <td><?php echo $row_consulta['DateCreated']; ?></td>
                                                <td><input type="checkbox"> </td>
                                              </tr>
                                              <?php
 
                                       }while($row_consulta = $consulta->fetch_assoc());
                                    }
                             }
                             ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="form-group col-md-4">
            <p>Oferta<br>
                    <select class="form-control" id="oferta" onchange="update_curso_box_curso()">
                      <option value="0"></option>
                      <?php
                      foreach($DaoOfertas->showAll() as $k=>$v){
                      ?>
                          <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
                      <?php
                      }
                      ?>
                    </select>
            </p>
        </div>
        <div class="form-group col-md-4">
            <p>Especialidad:<br>
                <select class="form-control" id="curso" onchange="update_orientacion_box_curso();update_grados_ofe()">
                  <option value="0"></option>
                </select>
            </p>
        </div>
        <div id="box_orientacion"></div>
        <div class="form-group col-md-4">
            <p>Ciclo<br>
              <select class="form-control" id="ciclo">
                  <option value="0"></option>
                  <?php
                  foreach($DaoCiclos->showAll() as $k=>$v){
                  ?>
                   <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
                  <?php
                  }
                  ?>
                </select>
            </p>
        </div>
        <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
            <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
        </div>
</div>
<script src="js/ckeditor/ckeditor.js"></script>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();
