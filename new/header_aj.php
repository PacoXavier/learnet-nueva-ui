<?php
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
require_once('../admin_ce/clases/DaoAlumnos.php');
require_once('../admin_ce/clases/DaoNotificaciones.php');
require_once('../admin_ce/clases/DaoNotificacionesTipoUsuario.php');
require_once('../admin_ce/clases/DaoNotificacionesUsuario.php');

if ($_POST['action'] == "updateOferta") {
    setcookie("Id_ofe_alum",$_POST['Id_ofe_alum'],time()+60*60*24*7,'/');
}


if($_POST['action']=="buscarNotificaciones"){
  //Notifcaciones realizadas por medio de la seccion notificaciones
 
  $DaoNotificaciones= new DaoNotificaciones();
  $DaoNotificacionesTipoUsuario= new DaoNotificacionesTipoUsuario();
  $not=$DaoNotificacionesTipoUsuario->getNotificacionTipoUsuario('alum',$_COOKIE['Id_alum']);
  $totalNot=count($not);
  

  
  //if(count($not)>0 || $countNot>0){
      $t="notificación";
      if($totalNot>1){
         $t="notificaciones"; 
      }
    ?>

                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <?php
                            if(count($not)>0){
                            ?>
                            <span class="label-count"><?php echo $totalNot?></span>
                            <?php
                            }
                            ?>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Tienes <?php echo $totalNot?> <?php echo $t?></li>
                            <li class="body">
                                <ul class="menu tasks">
                                    <?php
                                         if(count($not)>0){
                                            //Notificaciones realizadas por medio de la seccion noificaciones
                                            foreach($not as $notificacionTipoUsuario){
                                                $notifiacion=$DaoNotificaciones->show($notificacionTipoUsuario->getId_not());
                                                ?>
                                                    <li>
                                                        <a href="notificaciones.php" class=" waves-effect waves-block" onclick="readnotificacion(<?php echo $notificacionTipoUsuario->getId() ?>,'general')">
                                                            <h4>
                                                                <?php echo ucwords($notifiacion->getTitulo())?>
                                                            </h4>
                                                        </a>
                                            
                                                    </li>
                                                <?php
                                            }
                                        }

                                        ?>

                                    
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="notificaciones.php">Ver Todas</a>
                            </li>
                        </ul>
                    </li>
                    



    <?php
  //}
}













if($_POST['action']=="buscarNotificaciones_2"){

  
  //Notificaciones realizadas a un usuario en especifico
  $DaoNotificacionesUsuario= new DaoNotificacionesUsuario();
  $countNot=count($DaoNotificacionesUsuario->getNotificacionesUsuario($_COOKIE['Id_alum'],'alumno'));
  $totalNot=$countNot;
  
  //if(count($not)>0 || $countNot>0){
      $t="notificación";
      if($totalNot>1){
         $t="notificaciones"; 
      }
    ?>

                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">flag</i>
                            <?php
                            if($countNot>0){
                            ?>
                            <span class="label-count"><?php echo $totalNot?></span>
                            <?php
                            }
                            ?>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Tienes <?php echo $totalNot?> <?php echo $t?></li>
                            <li class="body">
                                <ul class="menu tasks">
                                    <?php
                                          if($countNot>0){
                                            //Notificaciones realizadas a un usuario en especifico
                                            foreach($DaoNotificacionesUsuario->getNotificacionesUsuario($_COOKIE['Id_alum'],'alumno') as $notifiacion){
                                                ?>
                                                  <li>
                                                        <a href="notificaciones.php" class=" waves-effect waves-block"onclick="readnotificacion(<?php echo $notifiacion->getId() ?>,'personalizada')">
                                                            <h4>
                                                                <?php echo ucwords($notifiacion->getTitulo())?>
                                                            </h4>
                                                        </a>
                                            
                                                    </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                        
                                    
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="notificaciones.php">Ver Todas</a>
                            </li>
                        </ul>
                    </li>
                    



    <?php
  //}
}




















if($_POST['action']=="readnotificacion"){
    $DaoNotificacionesTipoUsuario= new DaoNotificacionesTipoUsuario();
    if($_POST['Tipo']=="general"){
        //Notifcaciones realizadas por medio de la seccion notificaciones
        $notificacionesTipoUsuario=$DaoNotificacionesTipoUsuario->show($_POST['Id_not']);
        $notificacionesTipoUsuario->setDateRead(date('Y-m-d'));
        $DaoNotificacionesTipoUsuario->update($notificacionesTipoUsuario);
    }
    
    $DaoNotificacionesUsuario= new DaoNotificacionesUsuario();
    if($_POST['Tipo']=="personalizada"){
        $noti=$DaoNotificacionesUsuario->show($_POST['Id_not']);
        $noti->setDateRead(date('Y-m-d H:i:s'));
        $DaoNotificacionesUsuario->update($noti);
    }
}




if($_POST['action']=="readnotificacion"){
    $DaoNotificacionesTipoUsuario= new DaoNotificacionesTipoUsuario();
    if($_POST['Tipo']=="general"){
        //Notifcaciones realizadas por medio de la seccion notificaciones
        $notificacionesTipoUsuario=$DaoNotificacionesTipoUsuario->show($_POST['Id_not']);
        $notificacionesTipoUsuario->setDateRead(date('Y-m-d'));
        $DaoNotificacionesTipoUsuario->update($notificacionesTipoUsuario);
    }
    
    $DaoNotificacionesUsuario= new DaoNotificacionesUsuario();
    if($_POST['Tipo']=="personalizada"){
        $noti=$DaoNotificacionesUsuario->show($_POST['Id_not']);
        $noti->setDateRead(date('Y-m-d H:i:s'));
        $DaoNotificacionesUsuario->update($noti);
    }
}







