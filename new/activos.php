<?php
require_once('estandares/includes.php');
require_once('require_daos.php');

if (!isset($perm['51'])) {
    header('Location: home.php');
}
$DaoActivos = new DaoActivos();
$DaoTiposActivo = new DaoTiposActivo();
$DaoAulas = new DaoAulas();

links_head("Activos | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-briefcase"></i> Activos</h1>
                </div>
                <ul class="form">
                    <li>Buscar<br><input type="search"  placeholder="Buscar" id="buscar" onkeyup="buscarActivo()" /></li>
                </ul>
                <div id="mascara_tabla">
                    <table  class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>C&oacute;digo</td>
                                <td>Modelo</td>
                                <td>Nombre</td>
                                <td>Tipo</td>
                                <td>Adquisici&oacute;n.</td>
                                <td>Ubicaci&oacute;n</td>
                                <td>Disponibilidad</td>
                                <td>Estatus</td>
                                <td>Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach ($DaoActivos->showAll() as $activo) {
                                $tipo = $DaoTiposActivo->show($activo->getTipo_act());
                                $aula = $DaoAulas->show($activo->getId_aula());

                                if ($activo->getDisponible() == 1) {
                                    $disponibilidad = "Disponible";
                                    $style = 'style="color:green;"';
                                } else {
                                    $disponibilidad = "No Disponible";
                                    $style = 'style="color:red;"';
                                }

                                $status = "Activo";
                                $color = "color:green;";
                                if (strlen($activo->getBaja_activo()) > 0) {
                                    $status = "Baja";
                                    $color = "color:red;";
                                }
                                ?>
                                <tr>
                                    <td onclick="mostrar(<?php echo $activo->getId() ?>)"><?php echo $count; ?></td>
                                    <td style="text-align: center;" onclick="mostrar(<?php echo $activo->getId() ?>)"><?php echo $activo->getCodigo() ?></td>
                                    <td onclick="mostrar(<?php echo $activo->getId() ?>)"><?php echo $activo->getModelo() ?></td>
                                    <td onclick="mostrar(<?php echo $activo->getId() ?>)"><?php echo $activo->getNombre() ?></td>
                                    <td><?php echo $tipo->getNombre() ?></td>
                                    <td><?php echo $activo->getFecha_adq() ?></td>
                                    <td style="text-align:center;"><?php echo $aula->getClave_aula(); ?></td>
                                    <td <?php echo $style; ?>><?php echo $disponibilidad; ?></td>
                                    <td style="text-align:center;<?php echo $color ?>"><?php echo $status; ?></td>
                                    <td>
                                        <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                                        <div class="box-buttom">
                                            <button onclick="delete_activo(<?php echo $activo->getId() ?>)">Eliminar</button><br>
                                            <button onclick="generar_etiqueta(<?php echo $activo->getId() ?>)">Etiqueta</button><br>
                                            <a href="historial_prestamo_activo.php?id=<?php echo $activo->getId(); ?>" target="_blank"><button>Hist. Pres</button></a><br>
                                            <a href="historial_activo.php?id=<?php echo $activo->getId(); ?>" target="_blank"><button>Hist. Mant</button></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="activo.php" class="link">Nuevo</a></li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<?php
write_footer();
