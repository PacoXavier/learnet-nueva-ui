<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoCiclosAlumno= new DaoCiclosAlumno();
$DaoRecargos= new DaoRecargos();
$DaoPagosCiclo= new DaoPagosCiclo();
$DaoCiclos= new DaoCiclos();
$DaoGrupos= new DaoGrupos();
$DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
$DaoTurnos= new DaoTurnos();

links_head("Deudores | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-usd" aria-hidden="true"></i> Deudores</h1>
                </div>
                <div class="box-filter-reportes">
                    <ul class="row list-design">
                        <li class="col-md-4 widget" onclick="mostrar_filtro()"><div class="stats-left"><i class="fa fa-filter"></i> Filtros</div></li>
                        <li class="col-md-4 widget states-mdl" onclick="send_email(this)"><div class="stats-left"><i class="fa fa-envelope"></i> Email cobranza</div></li>
                        <li class="col-md-4 widget states-last" onclick="download_excel()"><div class="stats-left"><i class="fa fa-download"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables">
                    <table class="table">
                        <thead>
                            
                            <tr>
                                <td>#</td>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td>Orientaci&oacute;n</td>
                                <td class="td-center">Adeudo</td>
                                <td class="td-center">Porcentaje Asistencias</td>
                                <td  class="email_checkbox td-center"><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                                <td>Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                             $count=1;
                             $total=0;
                             $ttAdeudo=0;
                             foreach ($DaoAlumnos->getAlumnos() as $k=>$v){
                                       $alum = $DaoAlumnos->show($v['Id_ins']);
                                       $nombre_ori="";

                                       $oferta = $DaoOfertas->show($v['Id_ofe']);
                                       $esp = $DaoEspecialidades->show($v['Id_esp']);
                                       if ($v['Id_ori'] > 0) {
                                          $ori = $DaoOrientaciones->show($v['Id_ori']);
                                          $nombre_ori = $ori->getNombre();
                                        }
                                        $totalAdeudo = $DaoAlumnos->getAdeudoOfertaAlumno($v['Id_ofe_alum']);
                                        if($totalAdeudo>0){
                                        $ttAdeudo+=$totalAdeudo;
                                        $cicloAlumno=$DaoCiclosAlumno->getLastCicloOferta($v['Id_ofe_alum']);
                                        $total=0;
                                        $countGrupos=0;
                                        $porcentajeAsistencias=0;
                                        if($cicloAlumno->getId()>0){
                                            
                                            foreach ($DaoMateriasCicloAlumno->getMateriasCicloAlumno($cicloAlumno->getId()) as $materiaCiclo) {
                                                    if($materiaCiclo->getId_grupo()>0){
                                                        $resp = $DaoGrupos->getAsistenciasGrupoAlum($materiaCiclo->getId_grupo(), $v['Id_ins']);
                                                        $Asis = $resp['Asistencias'];
                                                        $Faltas = $resp['Faltas'];
                                                        $Just = $resp['Justificaciones'];

                                                        $total+= $base->porcentajeAsistencias($resp['Asistencias'], $resp['Justificaciones'], $resp['Faltas']);

                                                        $countGrupos++;
                                                    }
                                              }   
                                        }
                                        $porcentajeAsistencias=$DaoCiclosAlumno->redonderCalificacion($total/$countGrupos);

                                      ?>
                                              <tr id_alum="<?php echo $alum->getId();?>" id_ofe_alum="<?php echo $v['Id_ofe_alum'];?>">
                                                <td width="30px;"><?php echo $count;?></td>
                                                <td><a href="alumno.php?id=<?php echo $alum->getId();?>"><?php echo $alum->getMatricula() ?></a></td>
                                                <td style="width: 115px;"><a href="alumno.php?id=<?php echo $alum->getId();?>"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></a></td>
                                                <td><?php echo $oferta->getNombre_oferta(); ?></td>
                                                <td><?php echo $esp->getNombre_esp(); ?></td>
                                                <td><?php echo $nombre_ori; ?></td>
                                                <td class="td-center" style="color:red">$<?php echo number_format($totalAdeudo,2); ?></td>
                                                <td class="td-center"><?php echo $porcentajeAsistencias; ?></td>
                                                <td class="td-center"><input type="checkbox"> </td>
                                                <td style="width:110px;">
                                                    <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span> 
                                                    <div class="box-buttom">
                                                        <?php
                                                        if($oferta->getTipoOferta()==1){
                                                            ?>
                                                             <a href="pago.php?id=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button>Mostrar pagos</button></a>
                                                            <?php
                                                        }elseif($oferta->getTipoOferta()==2){
                                                            ?>
                                                             <a href="cargos_curso.php?id=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button>Mostrar pagos</button></a>
                                                            <?php   
                                                        }
                                                        ?>
                                                        <button onclick="mostrar_box_seguimiento(<?php echo $alum->getId();?>)">Seguimiento</button>
                                                        <a href="historial_seguimiento.php?id=<?php echo $alum->getId();?>&tipo=alum" target="_blank"><button>Historial</button></a>
                                                    </div>
                                                </td>
                                              </tr>
                                              <?php
                                              $count++;
                                        }  
                                }
                                ?>
                                <tr>
                                       <td colspan="5"></td>
                                       <td style="color:red"><b>Total</b></td>
                                       <td style="color:red" class="td-center"><b>$<?php echo number_format($ttAdeudo,2)?> </b></td>
                                       <td colspan="3"></td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
        <div class="boxUlBuscador">
            <p>Buscar<br><input type="search"  class="buscarFiltro" onkeyup="buscarAlum(this)" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p>Tipo deudor<br>
            <select id="tipo">
              <option value="0"></option>
              <option value="1">Activo</option>
              <option value="2">Baja</option>
            </select>
	</p>
        <p>Ciclo<br>
          <select id="ciclo">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p>Fecha inicio<br><input type="date" id="fecha_ini"/>
        <p>Fecha fin<br><input type="date" id="fecha_fin"/>
        <p>Turno<br>
            <select id="turno">
                <option value="0"></option>
                <?php
                foreach($DaoTurnos->getTurnos() as $turno){
                    ?>
                    <option value="<?php echo $turno->getId()?>" ><?php echo $turno->getNombre()?></option>
                <?php
                }
                ?>
            </select>
        </p>
        <div class="boxUlBuscador" id="box-grupo">
            <p>Grupo<br><input type="text" onkeyup="buscarGruposCiclo(this)" placeholder="Nombre" id="grupo"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
	<p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso()">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();



