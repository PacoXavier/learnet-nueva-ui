<?php
require_once('estandares/includes.php');                              
if(!isset($perm['30'])){
  header('Location: home.php');
}
require_once('estandares/class_medios_enterar.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
links_head("Interesados | ULM");
write_head_body();
write_body();
?>

<div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
  <div class="main-page" style="width: auto;">
    <div class="table-responsive">
      <h3 class="title1"><i class="fa fa-users" aria-hidden="true"></i> Interesados</h3>
      <div id="mascara-tabla" style="overflow-x: auto;">
        <table class="table">
          <thead>
            <tr>
              <td>Id</td>
              <td>Nombre</td>
              <td>Oferta</td>
              <td style="width: 140px">Carrera</td>
              <td>Orientaci&oacute;n:</td>
              <td style="width: 50px;">Alta</td>
              <td>Email</td>
              <td>Cel.</td>
              <td>Prioridad</td>
              <td>Seguimiento</td>
              <td>Contactar</td>
              <td><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
              <td>Acciones</td>
            </tr>
          </thead>
          <tbody>
            <?php
             foreach($DaoAlumnos->getInteresados() as $k=>$v){
              if($v['Id_ofe']>0){
                 $alum = $DaoAlumnos->show($v['Id_ins']);
                 if($v['Id_ofe']>0){
                 $nombre_ori="";

                 $oferta = $DaoOfertas->show($v['Id_ofe']);
                 $esp = $DaoEspecialidades->show($v['Id_esp']);
                 if ($v['Id_ori'] > 0) {
                    $ori = $DaoOrientaciones->show($v['Id_ori']);
                    $nombre_ori = $ori->getNombre();
                  }
                  
                  $color="";
                  $Seguimiento_ins = "";
                  if ($v['Seguimiento_ins'] == 1) {
                    $Seguimiento_ins = "Alta";
                    $color="success";
                  } elseif ($v['Seguimiento_ins'] == 2) {
                    $Seguimiento_ins = "Media";
                    $color="warning";
                  } elseif ($v['Seguimiento_ins'] == 3) {
                    $Seguimiento_ins = "Baja";
                    $color="danger";
                  } elseif ($v['Seguimiento_ins'] == 4) {
                    $Seguimiento_ins = "NULA";
                    $color="pink";
                  }

                   $query_log  = "SELECT * FROM logs_inscripciones_ulm WHERE Idrel_log=".$v['Id_ins']." AND Tipo_relLog='inte' ORDER BY Id_log DESC LIMIT 1";
                   $log = mysql_query($query_log, $cnn) or die(mysql_error());
                   $row_log = mysql_fetch_array($log);
                 }
                ?>
               <tr id_alum="<?php echo $alum->getId();?>" class="<?php echo $color;?>">
                  <td  onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getId(); ?></td>
                  <td onclick="mostrar(<?php echo $alum->getId(); ?>)" style="width: 115px;"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
                  <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $oferta->getNombre_oferta(); ?></td>
                  <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $esp->getNombre_esp(); ?></td>
                  <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $nombre_ori; ?></td>
                  <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getFecha_ins(); ?></td>
                  <td style="word-break: break-all;width: 130px;"><?php echo $alum->getEmail(); ?></td>
                  <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getCel(); ?></td>
                  <td onclick="mostrar(<?php echo $alum->getId(); ?>)" class="prioridad"><?php echo $Seguimiento_ins; ?></td>
                  <td><div class="box_seguimiento"><?php echo $row_log['Comen_log']?></div></td>
                  <td class="contactar"><?php echo $row_log['Dia_contactar']; ?></td>
                  <td><input type="checkbox"> </td>
                  <td>
                    <div class="dropdown">
                      <a href="#"  data-toggle="dropdown" aria-expanded="false">
                        <p><i class="fa fa-ellipsis-v mail-icon"></i></p>
                      </a>
                      <ul class="dropdown-menu float-right">
                        <li>
                          <a href="#" title="" onclick="delete_int(<?php echo $alum->getId(); ?>)">
                            <i class="fa fa-ban mail-icon"></i>
                            Eliminar
                          </a>
                        </li>
                        <li>
                          <a href="#" class="font-red" title="" onclick="mostrar_box_comentario(<?php echo $alum->getId(); ?>)">
                            <i class="fa fa-pencil mail-icon"></i>
                            Seguimiento
                          </a>
                        </li>
                      </ul>
                    </div>
                  </td>
                </tr>
                <?php }
             }?>
          </tbody>
      </table>  
      </div>      
    </div>
  </div>  

<script src="js/ckeditor/ckeditor.js"></script>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();



