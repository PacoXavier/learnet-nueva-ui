<?php
require_once('estandares/includes.php');
require_once('estandares/class_activos.php');
require_once('estandares/class_docentes.php');
require_once('estandares/class_interesados.php');
links_head("Historial de prestamos | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-history"></i> Historial de prestamos</h1>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Usuario</td>
                                <td>Solicitante</td>
                                <td>Comentarios</td>
                                <td style="text-align: center;">Fecha de prestamo</td>
                                <td style="text-align: center;">Fecha de entrega</td>
                                <td style="text-align: center;">Fecha de recibido</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count=1;
                            $class_activos = new class_activos($_REQUEST['id']);
                            $activo=$class_activos->get_activo();
                            foreach ($activo['Historial_prestamos'] as $k => $v) {
                                
                                if($v['Tipo_usu']=="usu"){
                                     $class_usuarios = new class_usuarios($v['Usu_recibe']);
                                     $user=$class_usuarios->get_usu();
                                     $nombre_recibe=$user['Nombre_usu']." ".$user['ApellidoP_usu']." ".$user['ApellidoM_usu'];
                                }elseif($v['Tipo_usu']=="docen"){
                                     $class_docentes = new class_docentes($v['Usu_recibe']);
                                     $docente=$class_docentes->get_docente();
                                     $nombre_recibe=$docente['Nombre_docen']." ".$docente['ApellidoP_docen']." ".$docente['ApellidoM_docen']; 
                                }elseif($v['Tipo_usu']=="alum"){
                                     $class_inscripciones = new class_interesados($v['Usu_recibe']);
                                     $alumn=$class_inscripciones->get_interesado();
                                     $nombre_recibe=$alumn['Nombre_ins']." ".$alumn['ApellidoP_ins']." ".$alumn['ApellidoM_ins'];   
                                }
                                
                                 $class_usuarios = new class_usuarios($v['Usu_entrega']);
                                 $user=$class_usuarios->get_usu();
                                 $nombre_entraga=$user['Nombre_usu']." ".$user['ApellidoP_usu']." ".$user['ApellidoM_usu'];
                                ?>
                            
                                <tr>
                                    <td><?php echo $count ?></td>
                                    <td><?php echo $nombre_entraga ?></td>
                                    <td><?php echo $nombre_recibe ?></td>
                                    <td><?php echo $v['Estado_recibe'] ?></td>
                                    <td style="text-align: center;"><?php echo $v['Fecha_entrega'] ?></td>
                                    <td style="text-align: center;"><?php echo $v['Fecha_devolucion'] ?></td>
                                    <td style="text-align: center;"><?php echo $v['Fecha_entregado'] ?></td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
        <td>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<input type="hidden" id="Id_activo" value="<?php echo $_REQUEST['id'] ?>"/>
<?php
write_footer();
