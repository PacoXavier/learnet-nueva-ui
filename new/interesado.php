<?php
require_once('estandares/includes.php');
if (!isset($perm['30'])) {
    header('Location: home.php');
}
require_once('require_daos.php'); 


$DaoMediosEnterar = new DaoMediosEnterar();
$DaoOfertas = new DaoOfertas();
$DaoEspecialidades = new DaoEspecialidades();
$DaoCiclos = new DaoCiclos();
$DaoGrados = new DaoGrados();
$DaoAlumnos = new DaoAlumnos();
$DaoDirecciones = new DaoDirecciones();
$DaoOfertasAlumno = new DaoOfertasAlumno();
$DaoCiclosAlumno = new DaoCiclosAlumno();
$DaoPagosCiclo = new DaoPagosCiclo();
$DaoOrientaciones = new DaoOrientaciones();
$DaoMediosContacto = new DaoMediosContacto();
$DaoLogsInscripciones = new DaoLogsInscripciones();
$DaoCursosEspecialidad= new DaoCursosEspecialidad();
$DaoTurnos= new DaoTurnos();
$DaoTutores= new DaoTutores();

links_head("Interesado");
write_head_body();
write_body();
$titulo = "Nuevo interesado";
$id_dir = 0;
$id = 0;
$ciudad = "";
$email = "";
$nombre = "";
$apellidoP = "";
$apellidoM = "";
$edad = "";
$tel = "";
$cel = "";
$comentarios = "";
$id_med_ent = "";
$seguimiento = "";
$id_medio_contacto = "";

if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $id = $_REQUEST['id'];
    $int = $DaoAlumnos->show($id);
    $titulo = $int->getNombre() . " " . $int->getApellidoP() . " " . $int->getApellidoM();
    $email = $int->getEmail();
    $nombre = $int->getNombre();
    $apellidoP = $int->getApellidoP();
    $apellidoM = $int->getApellidoM();
    $edad = $int->getEdad();
    $tel = $int->getTel();
    $cel = $int->getCel();
    $comentarios = $int->getComentarios();
    $id_med_ent = $int->getId_medio_ent();
    $seguimiento = $int->getSeguimiento_ins();
    $id_medio_contacto = $int->getMedioContacto();

    if ($int->getId_dir() > 0) {
        $dir = $DaoDirecciones->show($int->getId_dir());
        $id_dir = $dir->getId();
        $ciudad = $dir->getCiudad_dir();
    }

    $log = $DaoLogsInscripciones->getLogInt($id);
    if ($log->getId() > 0) {
        $diaContactar = $log->getDia_contactar();
    } else {
        $diaContactar = $semana = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d') + 7, date('Y')));
    }
} else {
    $diaContactar = $semana = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d') + 7, date('Y')));
}
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-user" aria-hidden="true"></i> <?php echo ucwords(strtolower($titulo)); ?></h1>
                </div>
                <div class="seccion">
                    <h2>Datos Personales</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li><span class="requerido">*</span>Email<br><input type="email" id="email_int" value="<?php echo $email ?>" onkeyup="buscar_int()"/>
                            <ul id="buscador_int"></ul>
                        </li>
                        <li><span class="requerido">*</span>Nombre<br><input type="text" id="nombre_int" value="<?php echo $nombre ?>"/></li>
                        <li><span class="requerido">*</span>Apellido Paterno<br><input type="text" id="apellidoP_int" value="<?php echo $apellidoP ?>"/></li>
                        <li>Apellido Materno<br><input type="text" id="apellidoM_int" value="<?php echo $apellidoM ?>"/></li>
                        <li>Tel&eacute;fono Movil<br><input type="tel" id="cel_int" value="<?php echo $cel ?>"/></li>
                        <li>Medio de Contacto<br>
                            <select id="medio_contacto">
                                <option value="0"></option>
                                <?php
                                foreach ($DaoMediosContacto->getMedios() as $medio) {
                                    ?>
                                    <option value="<?php echo $medio->getId() ?>" <?php if ($id_medio_contacto == $medio->getId()) { ?> selected="selected"<?php } ?>><?php echo $medio->getNombre_medio() ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </li>
                        <?php
                        if (strlen($ciudad) > 0) {
                            $ciudad = 'value="' . $ciudad . '"';
                        }
                        ?>
                        <li>Ciudad<br><input type="tel" id="ciudad_int" <?php echo $ciudad; ?> onkeyup="buscar_ciudad()"/>
                            <ul id="buscador_ciudad"></ul>
                        </li>
                        <li>Contactar el d&iacute;a<br>
                            <input type="date" value="<?php echo $diaContactar; ?>" id="contactar"/>
                        </li>
                        <li>Comentarios:<br><textarea id="comentarios"><?php echo $comentarios ?></textarea></li>
                    </ul>
                </div>
                <div class="seccion">
                    <h2>Datos de inter&eacute;s</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li><span class="requerido">*</span>Medio por el que se enteró<br>
                            <select id="medio">
                                <option value="0"></option>
                                <?php
                                foreach ($DaoMediosEnterar->showAll() as $k2 => $v2) {
                                    ?>
                                    <option value="<?php echo $v2->getId() ?>" <?php if ($id_med_ent == $v2->getId()) { ?> selected="selected"<?php } ?>><?php echo $v2->getMedio() ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </li>
                        <li><span class="requerido">*</span>Prioridad<br>
                            <select id="prioridad">
                                <option value="0"></option>
                                <option value="1" <?php if ($seguimiento == 1) { ?> selected="selected"<?php } ?>>Alta</option>
                                <option value="2" <?php if ($seguimiento == 2) { ?> selected="selected"<?php } ?>>Media</option>
                                <option value="3" <?php if ($seguimiento == 3) { ?> selected="selected"<?php } ?>>Baja</option>
                                <option value="4" <?php if ($seguimiento == 4) { ?> selected="selected"<?php } ?>>Nula</option>
                            </select>
                        </li>
                    </ul>
                </div>
                <div class="seccion">
                    <h2>Datos del padre o tutor</h2>
                    <span class="linea"></span>
                    <div id="list_tutores">
                        <?php
                        $countTu=0;
                        if (isset($id) && $id > 0) {
                            foreach($DaoTutores->getTutoresAlumno($id) as $tutor){
                                    $id_dir=0;
                                    $dir=$DaoDirecciones->getDireccionPorTipoRel($tutor->getId(),'tutor');
                                    if($dir->getId()>0){
                                      $id_dir= $dir->getId(); 
                                    }
                            ?>
                                <ul class="form" id-tutor="<?php echo $tutor->getId();?>" id-dir="<?php echo $dir->getId()?>">
                                    <li>Parentesco<br><input type="text" class="parentesco-tutor" value="<?php echo $tutor->getParentesco();?>"/></li>
                                    <li>Nombre<br><input type="text" class="nombre-tutor" value="<?php echo $tutor->getNombre();?>"/></li>
                                    <li>Apellido paterno<br><input type="text" class="apellidoP-tutor" value="<?php echo $tutor->getApellidoP();?>"/></li>
                                    <li>Apellido materno<br><input type="text" class="apellidoM-tutor" value="<?php echo $tutor->getApellidoM();?>"/></li>
                                    <li>Correo electrónico<br><input type="text" class="email-tutor" value="<?php echo $tutor->getEmail();?>"/></li>
                                    <li>Teléfono<br><input type="text" class="tel-tutor" value="<?php echo $tutor->getTel();?>"/></li>
                                    <li>Celular<br><input type="text" class="cel-tutor" value="<?php echo $tutor->getCel();?>"/></li>
                                    <li>Calle<br><input type="text" class="calle-tutor" value="<?php echo $dir->getCalle_dir();?>"/></li>
                                    <li>Núm ext.<br><input type="text" class="numext-tutor" value="<?php echo $dir->getNumExt_dir();?>"/></li>
                                    <li>Núm int.<br><input type="text" class="numint-tutor" value="<?php echo $dir->getNumInt_dir();?>"/></li>
                                    <li>Colonia<br><input type="text" class="colonia-tutor" value="<?php echo $dir->getColonia_dir();?>"/></li>
                                    <li>Ciudad<br><input type="text" class="ciudad-tutor" value="<?php echo $dir->getCiudad_dir();?>"/></li>
                                    <li>Estado<br><input type="text" class="estado-tutor" value="<?php echo $dir->getEstado_dir();?>"/></li>
                                    <li>Código postal<br><input type="text" class="cp-tutor" value="<?php echo $dir->getCp_dir();?>"/></li>
                                    <li><button type="button" class="btns btns-primary btns-sm delete-tutor" onclick="delete_tutor(<?php echo $tutor->getId() ?>,<?php echo $id_dir?>)">Eliminar tutor</button></li>
                                </ul>
                            <?php
                            $countTu++;
                            }
                        }
                        if($countTu==0){
                        ?>
                            <ul class="form" id-tutor="0" id-dir="0">
                                <li>Parentesco<br><input type="text" class="parentesco-tutor"/></li>
                                <li>Nombre<br><input type="text" class="nombre-tutor"/></li>
                                <li>Apellido paterno<br><input type="text" class="apellidoP-tutor"/></li>
                                <li>Apellido materno<br><input type="text" class="apellidoM-tutor"/></li>
                                <li>Correo electrónico<br><input type="text" class="email-tutor"/></li>
                                <li>Teléfono<br><input type="text" class="tel-tutor"/></li>
                                <li>Celular<br><input type="text" class="cel-tutor"/></li>
                                <li>Calle<br><input type="text" class="calle-tutor"/></li>
                                <li>Núm ext.<br><input type="text" class="numext-tutor"/></li>
                                <li>Núm int.<br><input type="text" class="numint-tutor"/></li>
                                <li>Colonia<br><input type="text" class="colonia-tutor"/></li>
                                <li>Ciudad<br><input type="text" class="ciudad-tutor"/></li>
                                <li>Estado<br><input type="text" class="estado-tutor"/></li>
                                <li>Código postal<br><input type="text" class="cp-tutor"/></li>
                            </ul>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="seccion">
                    <h2>&Aacute;reas de inter&eacute;s</h2>
                    <span class="linea"></span>
                    <div id="list_ofertas">
                        <?php
                        $count_ofertas = 0;
                        if (isset($id) && $id > 0) {
                            foreach ($DaoOfertasAlumno->getOfertasAlumno($id) as $ofeAlum) {
                                $of = $DaoOfertas->show($ofeAlum->getId_ofe());

                                //1= por ciclos, 2= sin ciclos
                                if ($of->getTipoOferta() == 1) {
                                    $primerCiclo = $DaoCiclosAlumno->getPrimerCicloOferta($ofeAlum->getId());
                                    $pago = $DaoPagosCiclo->getPagoIdCicloAlum($primerCiclo->getId());
                                    ?>
                                    <ul class="form" Id_ofe_alum="<?php echo $ofeAlum->getId() ?>" Id_ciclo_alum="<?php echo $primerCiclo->getId() ?>" Id_pago_ciclo="<?php echo $pago->getId() ?>">
                                        <li><span class="requerido">*</span>Oferta de inter&eacute;s<br>            
                                           <select class="oferta" onchange="getTipociclo(this)">
                                                <option value="0"></option>
                                                <?php
                                                foreach ($DaoOfertas->showAll() as $ofe) {
                                                    ?>
                                                    <option value="<?php echo $ofe->getId() ?>" <?php if ($ofe->getId() == $ofeAlum->getId_ofe()) { ?> selected="selected" <?php } ?>> <?php echo $ofe->getNombre_oferta() ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                        <li><span class="requerido">*</span>Especialidad<br>            
                                            <select class="curso" onchange="getOrientacionesEspecialidad(this)">
                                                <option value="0"></option>
                                                <?php
                                                foreach ($DaoEspecialidades->getEspecialidadesOferta($ofeAlum->getId_ofe()) as $esp) {
                                                    ?>
                                                    <option value="<?php echo $esp->getId() ?>" <?php if ($esp->getId() == $ofeAlum->getId_esp()) { ?> selected="selected" <?php } ?>><?php echo $esp->getNombre_esp() ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                        <li><div class="box_orientacion">
                                                <?php
                                                if (count($DaoOrientaciones->getOrientacionesEspecialidad($ofeAlum->getId_esp())) > 0) {
                                                    ?>
                                                    <span class="requerido">*</span>Orientaci&oacute;n<br>
                                                    <select class="orientacion">
                                                        <option value="0"></option>
                                                        <?php
                                                        foreach ($DaoOrientaciones->getOrientacionesEspecialidad($ofeAlum->getId_esp()) as $ori) {
                                                            ?>
                                                            <option value="<?php echo $ori->getId() ?>" <?php if ($ori->getId() == $ofeAlum->getId_ori()) { ?> selected="selected" <?php } ?>> <?php echo $ori->getNombre() ?> </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <?php
                                                }
                                                ?>
                                            </div></li>
                                        <li><span class="requerido">*</span>Grado<br>            
                                            <select class="grado">
                                                <option value="0"></option>
                                                <?php
                                                foreach ($DaoGrados->getGradosEspecialidad($ofeAlum->getId_esp()) as $grado) {
                                                    ?>
                                                    <option value="<?php echo $grado->getId() ?>" <?php if ($grado->getId() == $primerCiclo->getId_grado()) { ?> selected="selected" <?php } ?>> <?php echo $grado->getGrado() ?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                        <li><span class="requerido">*</span>Ciclo<br>
                                            <select class="ciclo">
                                                <option value="0"></option>
                                                <?php
                                                foreach ($DaoCiclos->showAll() as $cicloFut) {
                                                    ?>
                                                    <option value="<?php echo $cicloFut->getId() ?>" <?php if ($cicloFut->getId() == $primerCiclo->getId_ciclo()) { ?> selected="selected" <?php } ?>><?php echo $cicloFut->getClave() ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                        <li><span class="requerido">*</span>Opci&oacute;n de pago<br>
                                            <select class="opcionPago">
                                                <option value="0"></option>
                                                <option value="1" <?php if ($ofeAlum->getOpcionPago() == 1) { ?> selected="selected" <?php } ?>>Materias</option>
                                                <option value="2" <?php if ($ofeAlum->getOpcionPago() == 2) { ?> selected="selected" <?php } ?>>Plan Completo</option>
                                            </select>
                                        </li>
                                        <li><span class="requerido">*</span>Turno<br>
                                            <select class="turno">
                                                <option value="0"></option>
                                                <?php
                                                foreach($DaoTurnos->getTurnos() as $turno){
                                                    ?>
                                                    <option value="<?php echo $turno->getId()?>" <?php if ($ofeAlum->getTurno() == $turno->getId()) { ?> selected="selected" <?php } ?>><?php echo $turno->getNombre()?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </li><br>
                                        <li><button type="button" class="btns btns-primary btns-sm" onclick="delete_ofe_alum(<?php echo $ofeAlum->getId() ?>)">Eliminar oferta</button></li>
                                    </ul>
                                    <?php
                                } elseif ($of->getTipoOferta() == 2) {
                                    $primerCiclo = $DaoCiclosAlumno->getPrimerCicloOferta($ofeAlum->getId());
                                    $pago = $DaoPagosCiclo->getPagoIdCicloAlum($primerCiclo->getId());
                                    ?>
                                    <ul class="form" Id_ofe_alum="<?php echo $ofeAlum->getId() ?>" Id_ciclo_alum="<?php echo $primerCiclo->getId() ?>" Id_pago_ciclo="<?php echo $pago->getId() ?>">
                                        <li><span class="requerido">*</span>Oferta de inter&eacute;s<br>            
                                            <select class="oferta" onchange="getTipociclo(this)">
                                                <option value="0"></option>
                                                <?php
                                                foreach ($DaoOfertas->showAll() as $ofe) {
                                                    ?>
                                                    <option value="<?php echo $ofe->getId() ?>" <?php if ($ofe->getId() == $ofeAlum->getId_ofe()) { ?> selected="selected" <?php } ?>> <?php echo $ofe->getNombre_oferta() ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                        <li><span class="requerido">*</span>Especialidad<br>            
                                            <select class="curso" onchange="getOrientacionesEspecialidad(this)">
                                                <option value="0"></option>
                                                <?php
                                                foreach ($DaoEspecialidades->getEspecialidadesOferta($ofeAlum->getId_ofe()) as $esp) {
                                                    ?>
                                                    <option value="<?php echo $esp->getId() ?>" <?php if ($esp->getId() == $ofeAlum->getId_esp()) { ?> selected="selected" <?php } ?>><?php echo $esp->getNombre_esp() ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                       <li><span class="requerido">*</span>Turno<br>
                                            <select class="turno" onchange="getCursosEspecialidad(this)">
                                                <option value="0"></option>
                                                <?php
                                                foreach($DaoTurnos->getTurnos() as $turno){
                                                    ?>
                                                    <option value="<?php echo $turno->getId()?>" <?php if ($ofeAlum->getTurno() == $turno->getId()) { ?> selected="selected" <?php } ?>><?php echo $turno->getNombre()?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                        <li>
                                            <span class="requerido">*</span>Fecha del curso<br>
                                            <select class="cursos-especialidad">
                                                    <option value="0"></option>
                                                    <?php
                                                    foreach ($DaoCursosEspecialidad->getCursosEspecialidad($ofeAlum->getId_esp()) as $curso) {
                                                        $fechaIni = "Fecha abierta";
                                                        if (strlen($curso->getFechaInicio()) > 0 && strlen($curso->getFechaFin()) > 0) {
                                                            $fechaIni = "Inicio: " . $curso->getFechaInicio() . " - Fin: " . $curso->getFechaFin();
                                                        }
                                                        ?>
                                                        <option value="<?php echo $curso->getId() ?>" <?php if ($curso->getId() == $ofeAlum->getId_curso_esp()) { ?> selected="selected" <?php } ?>><?php echo $fechaIni ?> </option>
                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </li>
                                        <li>
                                            <div class="box_orientacion">
                                             <?php
                                                if (count($DaoOrientaciones->getOrientacionesEspecialidad($ofeAlum->getId_esp())) > 0) {
                                                    ?>
                                                    <span class="requerido">*</span>Orientaci&oacute;n<br>
                                                    <select class="orientacion">
                                                        <option value="0"></option>
                                                        <?php
                                                        foreach ($DaoOrientaciones->getOrientacionesEspecialidad($ofeAlum->getId_esp()) as $ori) {
                                                            ?>
                                                            <option value="<?php echo $ori->getId() ?>" <?php if ($ori->getId() == $ofeAlum->getId_ori()) { ?> selected="selected" <?php } ?>> <?php echo $ori->getNombre() ?> </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </li>
                                        <li><span class="requerido">*</span>Grado<br>            
                                            <select class="grado">
                                                <option value="0"></option>
                                                <?php
                                                foreach ($DaoGrados->getGradosEspecialidad($ofeAlum->getId_esp()) as $grado) {
                                                    ?>
                                                    <option value="<?php echo $grado->getId() ?>" <?php if ($grado->getId() == $primerCiclo->getId_grado()) { ?> selected="selected" <?php } ?>> <?php echo $grado->getGrado() ?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                        <li><span class="requerido">*</span>Opci&oacute;n de pago<br>
                                            <select class="opcionPago">
                                                <option value="0"></option>
                                                <option value="1" <?php if ($ofeAlum->getOpcionPago() == 1) { ?> selected="selected" <?php } ?>>Materias</option>
                                                <option value="2" <?php if ($ofeAlum->getOpcionPago() == 2) { ?> selected="selected" <?php } ?>>Plan Completo</option>
                                            </select>
                                        </li>
                                        <li><button type="button" class="btns btns-primary btns-sm" onclick="delete_ofe_alum(<?php echo $ofeAlum->getId() ?>)">Eliminar oferta</button></li>
                                    </ul>
                                    <?php
                                }
                                $count_ofertas++;
                            }
                        }
                        if($count_ofertas==0){
                         ?>
                           <ul class="form">
                            <li><span class="requerido">*</span>Oferta de inter&eacute;s<br>            
                                <select class="oferta" onchange="getTipociclo(this)">
                                    <option value="0"></option>
                                    <?php
                                    foreach ($DaoOfertas->showAll() as $oferta) {
                                        ?>
                                        <option value="<?php echo $oferta->getId() ?>"> <?php echo $oferta->getNombre_oferta() ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </li>
                            <li><span class="requerido">*</span>Especialidad<br>            
                                <select class="curso" onchange="getOrientacionesEspecialidad(this), getCursosEspecialidad(this)">
                                    <option value="0"></option>
                                </select>
                            </li>
                            <li><div class="box_orientacion"></div></li>
                            <li><span class="requerido">*</span>Grado<br>            
                                <select class="grado">
                                    <option value="0"></option>
                                </select>
                            </li>
                            <li class="box-ciclo"><span class="requerido">*</span>Ciclo<br>
                                <select class="ciclo">
                                    <option value="0"></option>
                                    <?php
                                    foreach ($DaoCiclos->getCiclosFuturos() as $k => $v) {
                                        ?>
                                        <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave() ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </li>
                            <li><span class="requerido">*</span>Opci&oacute;n de pago<br>
                                <select class="opcionPago">
                                    <option value="0"></option>
                                    <option value="1">Materias</option>
                                    <option value="2">Plan Completo</option>
                                </select>
                            </li>
                            <li><span class="requerido">*</span>Turno<br>
                                <select class="turno">
                                    <option value="0"></option>
                                    <?php
                                    foreach($DaoTurnos->getTurnos() as $turno){
                                        ?>
                                        <option value="<?php echo $turno->getId()?>"><?php echo $turno->getNombre()?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </li>
                        </ul>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <button type="button" class="btns btns-primary btns-lg" id="button_ins" onclick="save_inte()">Guardar</button>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="interesado.php" class="link">Nuevo interesado</a></li>
                    <li><span onclick="mostrarBoxTutor()">Añadir tutor </span></li>
                    <?php
                    if (isset($id) && $id > 0) {
                        ?>
                        <li><span onclick="mostrar_box_comentario(<?php echo $id ?>, 'update')">Capturar seguimiento</span></li>
                        <li><span onclick="add_oferta()">Añadir oferta </span></li>
                        <?php
                        if ($count_ofertas > 0) {
                            if (isset($perm['31'])) {
                                ?>
                                <li><span onclick="box_inscripcion()">Inscribir</span></li>
                                <?php
                            }
                        }
                    }
                    ?>

                    <li><a href="interesados.php">Regresar</a></li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_alumn" value="<?php echo $id ?>"/>
<input type="hidden" id="Id_dir" value="<?php echo $id_dir ?>"/>
<?php
write_footer();


