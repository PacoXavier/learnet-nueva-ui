<?php
require_once('ajax/activate_error.php');
require_once('require_daos.php'); 
require_once('estandares/funciones.php');


function hashPassword($password){
  $salt = "mdsksprwuhej6skhs0b08ojx6";
  //encrypt the password, rotate characters by length of original password
  $len = strlen($password);
  $password = md5($password);
  $password = rotateHEX($password,$len);
  return md5($salt.$password);
}

function rotateHEX($string, $n){
  //for more security, randomize this string
  $chars="4hxdxsyx3fkygsg8";
  $str="";
  for ($i=0;$i<strlen($string);$i++)
  {
    $pos = strpos($chars,$string[$i]);
    $pos += $n;
    if ($pos>=strlen($chars))
      $pos = $pos % strlen($chars);
    $str.=$chars[$pos];
  }
  return $str;
}



if(isset($_POST['action']) && $_POST['action']=="login"){
   $base= new base();
   $DaoUsuarios= new DaoUsuarios();
   $query = "SELECT * FROM usuarios_ulm WHERE Email_usu='".$_POST['email']."' AND Pass_usu='".$base->hashPassword($_POST['pass'])."' AND Baja_usu IS NULL";
   $usuario=$DaoUsuarios->existQuery($query);
   if($usuario->getId()>0){
          setcookie("admin/Id_usu",$usuario->getId(),time()+60*60*24*7,'/'); 
          setcookie("admin/Tipo","usu",time()+60*60*24*7,'/'); 
          $usuario->setLast_session(date('Y-m-d H:i:s'));
          $DaoUsuarios->update($usuario);	
          $resp=1;
   }else{
	   $query = "SELECT * FROM usuarios_ulm WHERE Email_usu='".$_POST['email']."' AND Baja_usu IS NULL";
	   $usuario=$DaoUsuarios->existQuery($query);
           if($usuario->getId()>0){
             	$resp=3;   		  
		$msn='<p>La contraseña proporcionada es incorrecta</p>';  
           }else{
          	$resp=2;
		$msn='<p>La dirección de email proporcionada no esta registrada en nuestro sistema,favor de verificar.</p>';     
           }
   }
   echo($resp);
}


if(isset($_POST['action']) && $_POST['action']=="use_database"){
    if($_POST['test']==1){
               setcookie("test_admin",1,time()+60*60*16,'/'); 
      }else{
               setcookie("test_admin","",time()-1,'/');
    }
}

if(isset($_POST['action']) && $_POST['action']=="buscarNotificaciones_interesados"){
  $base= new base();
  $DaoHistorialSeguimiento= new DaoHistorialSeguimiento();
  $DaoUsuarios= new DaoUsuarios();
  $DaoNotificaciones= new DaoNotificaciones();
  $DaoNotificacionesTipoUsuario= new DaoNotificacionesTipoUsuario();
  $DaoNotificacionesUsuario= new DaoNotificacionesUsuario();
  $DaoParametrosPlantel= new DaoParametrosPlantel();
  $params=$DaoParametrosPlantel->getParametrosPlantelArray();

  $arrayTipos=  explode(";", $params['RecibirNotificacionesSeguimientoInteresados']);
  $usuarioCaptura=  $params['RecibirNotificacionSoloUsuarioQueCapturoInteresado'];
  $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
  
  //Notifcaciones realizadas por medio de la seccion notificaciones(masivas)
  $notificacionesMasivas=$DaoNotificacionesTipoUsuario->getNotificacionTipoUsuario('usu',$usu->getId());
  $arrayNotificaciones=array();
  foreach($notificacionesMasivas as $notificacionTipoUsuario){
        $notifiacion=$DaoNotificaciones->show($notificacionTipoUsuario->getId_not());
        $no_TipoUsuario=array();
        $no_TipoUsuario['Id_not']=$notificacionTipoUsuario->getId();
        $no_TipoUsuario['Tipo']="Masiva";
        $no_TipoUsuario['Titulo']=ucwords($notifiacion->getTitulo());
        $no_TipoUsuario['Texto']=$notifiacion->getTexto();
        $no_TipoUsuario['Sticker']=false;
        $no_TipoUsuario['Close']=true;
        array_push($arrayNotificaciones,$no_TipoUsuario);
  }

  //Notificaciones realizadas a un usuario en especifico
  foreach($DaoNotificacionesUsuario->getNotificacionesUsuario($_COOKIE['admin/Id_usu'],'usuario') as $notifiacion){
        $no_TipoUsuario=array();
        $no_TipoUsuario['Id_not']=$notifiacion->getId();
        $no_TipoUsuario['Tipo']="Personalizada";
        $no_TipoUsuario['Titulo']=ucwords($notifiacion->getTitulo());
        $no_TipoUsuario['Texto']=$notifiacion->getTexto();
        $no_TipoUsuario['Sticker']=false;
        $no_TipoUsuario['Close']=true;
        array_push($arrayNotificaciones,$no_TipoUsuario);
  }

  //Notificaciones de seguimiento a interesados
  if(in_array($usu->getTipo_usu(), $arrayTipos) || $usuarioCaptura>0){
        $q="";
        if($usuarioCaptura>0){
           $q=" AND logs_inscripciones_ulm.Id_usu=".$usuarioCaptura;
        }
        $query  = "SELECT logs_inscripciones_ulm.*,
        inscripciones_ulm.Nombre_ins,inscripciones_ulm.ApellidoM_ins,inscripciones_ulm.ApellidoP_ins,inscripciones_ulm.Id_ins
        FROM logs_inscripciones_ulm
        JOIN inscripciones_ulm ON logs_inscripciones_ulm.Idrel_log=inscripciones_ulm.Id_ins
        WHERE inscripciones_ulm.Id_plantel=".$usu->getId_plantel()."  
              AND Tipo_relLog='inte' 
              AND Dia_contactar='".date('Y-m-d')."' 
              AND DateDead_not IS NULL ".$q." ORDER BY Fecha_log DESC";
         foreach($base->advanced_query($query) as   $row_logs_inscripciones_ulm){
                $no_TipoUsuario=array();
                $no_TipoUsuario['Id_not']=$row_logs_inscripciones_ulm['Id_log'] ;
                $no_TipoUsuario['Tipo']="Interesados";
                $no_TipoUsuario['Titulo']=ucwords("Contactar el ".formatFecha($row_logs_inscripciones_ulm['Dia_contactar']));
                $no_TipoUsuario['Sticker']=false;

                $texto='<p><a href="interesado.php?id='.$row_logs_inscripciones_ulm['Id_ins'].'" target="_blank">'.ucwords($row_logs_inscripciones_ulm['Nombre_ins']." ".$row_logs_inscripciones_ulm['ApellidoP_ins']." ".$row_logs_inscripciones_ulm['ApellidoM_ins'])."</a><br>".ucwords($row_logs_inscripciones_ulm['Comen_log'])."</p>";
                //No borrar notificaciones de interesados hasta que exista un seguimiento
                $existe=$DaoHistorialSeguimiento->getExistioSeguimiento($row_logs_inscripciones_ulm['Id_log']);
                if($existe->getId()>0){
                    $no_TipoUsuario['Close']=true;
                }else{
                    $no_TipoUsuario['Close']=false;
                    $texto.='<p style="color: #337ab7;">Dar seguimiento para marcar como leída la notificación</p>';
                }
                $no_TipoUsuario['Texto']=$texto;
                array_push($arrayNotificaciones,$no_TipoUsuario);
        }
  }
        echo json_encode($arrayNotificaciones);
}

if(isset($_POST['action']) && $_POST['action']=="readnot"){
    $DaoLogsInscripciones= new DaoLogsInscripciones();
    $log=$DaoLogsInscripciones->show($_POST['Id_log']);
    $log->setDateDead_not(date('Y-m-d H:i:s'));
    $log->setId_usuRead_not($_COOKIE['admin/Id_usu']);
    $DaoLogsInscripciones->update($log);
}

if(isset($_POST['action']) && $_POST['action']=="readnotificacion"){
    $DaoNotificacionesTipoUsuario= new DaoNotificacionesTipoUsuario();
    $not=$DaoNotificacionesTipoUsuario->show($_POST['Id_not']);
    $not->setDateRead(date('Y-m-d'));
    $DaoNotificacionesTipoUsuario->update($not);
}


if(isset($_POST['action']) && $_POST['action']=="readnotificacionPersonalizada"){
    $DaoNotificacionesUsuario= new DaoNotificacionesUsuario();
    $not=$DaoNotificacionesUsuario->show($_POST['Id_not']);
    $not->setDateRead(date('Y-m-d'));
    $DaoNotificacionesUsuario->update($not);
}

if(isset($_POST['action']) && $_POST['action']=="buscarAlum"){
    if(strlen($_POST['buscar'])>=4){
        $DaoAlumnos= new DaoAlumnos();
        foreach($DaoAlumnos->buscarAlumno($_POST['buscar']) as $k=>$v){
          ?>
            <li id_alum="<?php echo $v->getId();?>"><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li>
          <?php      
        }
    }
}

if(isset($_POST['action']) && $_POST['action']=="buscarDocent"){
    if(strlen($_POST['buscar'])>=4){
        $DaoDocentes= new DaoDocentes();
        foreach($DaoDocentes->buscarDocente($_POST['buscar']) as $k=>$v){
          ?>
           <li id-data="<?php echo $v->getId();?>"><?php echo $v->getNombre_docen()." ".$v->getApellidoP_docen()." ".$v->getApellidoM_docen() ?></li>
          <?php      
        }
    }
}


if(isset($_POST['action']) && $_POST['action']=="buscarUsu"){
    if(strlen($_POST['buscar'])>=4){
        $DaoUsuarios= new DaoUsuarios();
        foreach($DaoUsuarios->buscarUsuario($_POST['buscar']) as $k=>$v){
          ?>
           <li id-data="<?php echo $v->getId();?>"><?php echo $v->getNombre_usu()." ".$v->getApellidoP_usu()." ".$v->getApellidoM_usu() ?></li>
          <?php      
        }
    }
}


if(isset($_POST['action']) && $_POST['action']=="buscarActivo"){
    if(strlen($_POST['buscar'])>=4){
        $DaoActivos= new DaoActivos();
        foreach($DaoActivos->buscarActivo($_POST['buscar']) as $k=>$v){
          ?>
           <li id-data="<?php echo $v->getId();?>"><?php echo $v->getCodigo()." - ".$v->getNombre() ?></li>
          <?php      
        }
    }
}

if(isset($_POST['action']) && $_POST['action']=="buscarLibro"){
    if(strlen($_POST['buscar'])>=4){
        $DaoLibros= new DaoLibros();
        foreach($DaoLibros->buscarLibro($_POST['buscar']) as $k=>$v){
          ?>
           <li id-data="<?php echo $v->getId();?>"><?php echo $v->getCodigo()." ".$v->getTitulo() ?></li>
          <?php      
        }
    }
}

if(isset($_POST['action']) && $_POST['action']=="buscarMateria"){
    if(strlen($_POST['buscar'])>=4){
        $DaoMaterias= new DaoMaterias();
        foreach($DaoMaterias->buscarMateria($_POST['buscar']) as $k=>$v){
          ?>
           <li id-data="<?php echo $v->getId();?>"><?php echo $v->getClave_mat()." ".$v->getNombre() ?></li>
          <?php      
        }
    }
}



if (isset($_POST['action']) && $_POST['action'] == "buscarGruposCiclo") {
    if(strlen($_POST['buscar'])>=4){
        $DaoUsuarios= new DaoUsuarios();
        $DaoCiclos= new DaoCiclos();
        $usu=$DaoCiclos->show($_COOKIE['admin/Id_usu']);
        $ciclo=$DaoCiclos->getActual($usu->getId_plantel());
        $Id_ciclo=$ciclo->getId();
        if($_POST['Id_ciclo']>0){
           $Id_ciclo=$_POST['Id_ciclo'];
        }

        $DaoMaterias= new DaoMaterias();
        $DaoGrupos= new DaoGrupos();
        foreach($DaoGrupos->buscarGrupoCiclo($_POST['buscar'],$Id_ciclo) as $k=>$v){
            $mat=$DaoMaterias->show($v->getId_mat());
          ?>
           <li id-data="<?php echo $v->getId();?>"><?php echo $v->getClave()."-".$mat->getNombre()?></li>
          <?php      
        }
    }
}




if (isset($_POST['action']) && $_POST['action'] == "update_curso") {
  ?>
  <option value="0"></option>
  <?php
  $DaoEspecialidades= new DaoEspecialidades();
  foreach ($DaoEspecialidades->getEspecialidadesOferta($_POST['Id_oferta']) as $k => $v) {
  ?>
  <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre_esp(); ?> </option>
  <?php
  }
}


if (isset($_POST['action']) && $_POST['action'] == "update_orientacion_box_curso") {
  $DaoOrientaciones= new DaoOrientaciones();
  if(count($DaoOrientaciones->getOrientacionesEspecialidad($_POST['Id_esp']))>0){
  ?>
   <p>
  Orientaci&oacute;n<br>
  <select id="orientacion">
    <option value="0"></option>
  <?php
  foreach ($DaoOrientaciones->getOrientacionesEspecialidad($_POST['Id_esp']) as $k => $v) {
  ?>
    <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre() ?> </option>
  <?php
  }
  ?>
  </select>
  </p>
  <?php
   }
}   


if (isset($_POST['action']) && $_POST['action'] == "update_grados_ofe") {
   $DaoGrados= new DaoGrados();
  ?>
  <option value="0"></option>
  <?php
  foreach($DaoGrados->getGradosEspecialidad($_POST['Id_esp']) as $k=>$v){
  ?>
      <option value="<?php echo $v->getId() ?>"> <?php echo $v->getGrado() ?> </option>
  <?php
  }
}


if(isset($_POST['action']) && $_POST['action']=="delete_attac"){
    $DaoAttachments= new DaoAttachments();
    $ata=$DaoAttachments->show($_POST['Id_attc']);
    if($ata->getId()>0){
       $DaoAttachments->delete($_POST['Id_attc']);
    }
    foreach ($DaoAttachments->getAttachmentsUsu($_COOKIE['admin/Id_usu'],"usu") as $k=>$v){
        ?>
        <li><?php echo $v->getNombre_atta()?> <i class="fa fa-trash-o" onclick="delete_attac(<?php echo $v->getId()?>)"></i></li>     
        <?php
    }
}

if(isset($_POST['action']) && $_POST['action']=="send_email"){
    $DaoUsuarios= new DaoUsuarios();
    $DaoAttachments= new DaoAttachments();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
       
   foreach($_POST['alumnos'] as $v){
     $DaoAlumnos= new DaoAlumnos();
     $alum=$DaoAlumnos->show($v);
     if(strlen($alum->getEmail())>0){
       $base = new base();
       $arrayData= array();
       $arrayData['Asunto']=$_POST['asunto'];
       $arrayData['Mensaje']=$_POST['mensaje'];
       $arrayData['IdRel']=$_COOKIE['admin/Id_usu'];
       $arrayData['TipoRel']="usu";
       $arrayData['email-usuario']= $usu->getEmail_usu();
       $arrayData['nombre-usuario']= $usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
       $arrayData['Destinatarios']=array();
       
       $Data= array();
       $Data['email']= $alum->getEmail();
       $Data['name']= $alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
       
       array_push($arrayData['Destinatarios'], $Data);
       $base->send_email($arrayData);
     }
   }
   $DaoAttachments->deleteAttachmentsUsu($_COOKIE['admin/Id_usu'],"usu");
}



if(isset($_POST['action']) && $_POST['action']=="mostrar_box_email"){
?>
<div id="box_emergente" class="send-email">
      <h1><i class="fa fa-envelope"></i> Enviar email</h1>
      <p><input type="text" placeholder="Asunto" id="asunto"/></p>
      <div id="box-box">
          <div id="box-mensaje">
              <textarea class="ckeditor" name="editor1"></textarea>
          </div>
      </div>
      <ul id="list_archivos">
          <?php
           $DaoAttachments= new DaoAttachments();
            foreach ($DaoAttachments->getAttachmentsUsu($_COOKIE['admin/Id_usu'],"usu") as $k=>$v){
                ?>
                <li><?php echo $v->getNombre_atta()?> <span onclick="delete_attac(<?php echo $v->getId()?>)">x</span></li>   
                <?php
            }
          ?>          
      </ul>
      <p><button onclick="send_email(this)" id="boton-email">Enviar</button><button onclick="ocultar_error_layer()">Cerrar</button></p>
      <span id="buttonAdjuntar" onclick="mostrarFinder()"><i class="fa fa-paperclip" style="font-size: 19px;"></i></span>
</div>
<?php	
}

if (isset($_GET['action']) && $_GET['action'] == "uploadAttachment") {
    $DaoAttachments= new DaoAttachments();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    if (count($_FILES) > 0) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {

            $base= new base();
            $key=$base->generarKey();

            $Attachments = new Attachments();
            $Attachments->setNombre_atta($_FILES["file"]["name"]);
            $Attachments->setMime_atta($_FILES["file"]['type']);
            $Attachments->setLlave_atta($key);
            $Attachments->setId_plantel($usu->getId_plantel());
            $Attachments->setIdRel($_COOKIE['admin/Id_usu']);
            $Attachments->setTipoRel("usu");
            $DaoAttachments->add($Attachments);
            
            move_uploaded_file($_FILES["file"]["tmp_name"], "attachments/" . $key . ".ulm");
        }

    } elseif (isset($_GET['action'])) {

        if ($_FILES["file"]["error"] > 0) {

            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {

            $base= new base();
            $key=$base->generarKey();
            
            $Attachments = new Attachments();
            $Attachments->setNombre_atta($_GET["FileName"]);
            $Attachments->setMime_atta($_GET["TypeFile"]);
            $Attachments->setLlave_atta($key);
            $Attachments->setId_plantel($usu->getId_plantel());
            $Attachments->setIdRel($_COOKIE['admin/Id_usu']);
            $Attachments->setTipoRel("usu");
            $DaoAttachments->add($Attachments);

            if (isset($_GET['base64'])) {
                // If the browser does not support sendAsBinary ()
                $content = base64_decode(file_get_contents('php://input'));
            } else {
                $content = file_get_contents('php://input');
            }
            file_put_contents('attachments/' . $key . '.ulm', $content);
        }
    }
    
    foreach ($DaoAttachments->getAttachmentsUsu($_COOKIE['admin/Id_usu'],"usu") as $k=>$v){
        ?>
        <li><?php echo $v->getNombre_atta()?> <i class="fa fa-trash-o" onclick="delete_attac(<?php echo $v->getId()?>)"></i></li>     
        <?php
    }
}



if (isset($_POST['action']) && $_POST['action'] == "update_materias") {
  ?>
  <option value="0"></option>
  <?php
  $DaoMaterias= new DaoMaterias();
  $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
  foreach ($DaoMateriasEspecialidad->getMateriasEspecialidad($_POST['Id_esp']) as $k => $v) {
     $mat=$DaoMaterias->show($v->getId_mat());
     $NombreMat=$mat->getNombre();
     if(strlen($v->getNombreDiferente())>0){
        $NombreMat=$v->getNombreDiferente(); 
     }
  ?>
  <option value="<?php echo $v->getId_mat() ?>" id_mat_esp="<?php echo $v->getId() ?>"> <?php echo $mat->getClave_mat()." - ".$NombreMat ?> </option>
  <?php
  }
}

if(isset($_POST['action']) && $_POST['action']=="deleteAttachments"){
    $DaoAttachments= new DaoAttachments();
    $DaoAttachments->deleteAttachmentsUsu($_COOKIE['admin/Id_usu'],"usu");
}

