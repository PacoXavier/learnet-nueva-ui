<?php
set_time_limit(0);
require_once('../ajax/activate_error.php');
require_once('../require_daos.php');

$base= new base();
$DaoUsuarios= new DaoUsuarios();
$DaoDocentes= new DaoDocentes();
$DaoAlumnos= new DaoAlumnos();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoNotificacionesUsuario= new DaoNotificacionesUsuario();

$hoy=date('Y-m-d');
//7=cambio de oferta academica
//10=error al asignar carrera
//8=falta de ineteres
//13=otro
$count=0;
$query="SELECT * FROM ofertas_alumno WHERE Baja_ofe IS NOT NULL AND GenerarAlerta IS NULL AND Id_mot_baja NOT IN(10,7,8,13)";
foreach($DaoOfertasAlumno->advanced_query($query) as $OfertaAlumno){
    
    $Y=date('Y',strtotime($OfertaAlumno->getBaja_ofe()));
    $m=date('m',strtotime($OfertaAlumno->getBaja_ofe()));
    $d=date('d',strtotime($OfertaAlumno->getBaja_ofe()));
    //Alerta para el alumno que tiene más de un año de baja, para que pueda presentar examen de nivelación	
    $fechaNueva=date("Y-m-d",mktime(0, 0, 0, $m, $d, $Y+1));
    if($fechaNueva>=$hoy){
        
        $OfertaAlumno->setGenerarAlerta(1);
        $DaoOfertasAlumno->update($OfertaAlumno);
        
        $alum=$DaoAlumnos->show($OfertaAlumno->getId_alum());
        if($alum->getId_plantel()==1){
        
            //Crear Notificacion para los administradores del sistema
            foreach($DaoUsuarios->getUsuariosTipo(1,$alum->getId_plantel()) as $usuTipo){
                $Texto="El alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM()." presento su baja el día ".$OfertaAlumno->getBaja_ofe().", por lo cual esta notificación es para que presente su examen de nivelación";
                $NotificacionesUsuario= new NotificacionesUsuario();
                $NotificacionesUsuario->setDateCreated(date('Y-m-d H:i:s'));
                $NotificacionesUsuario->setIdRel($usuTipo->getId());
                $NotificacionesUsuario->setTipoRel('usu');
                $NotificacionesUsuario->setTitulo("Not");
                $NotificacionesUsuario->setTexto($Texto);
                $DaoNotificacionesUsuario->add($NotificacionesUsuario);
            }
            $count++;
        }
    }
}

echo $count;

