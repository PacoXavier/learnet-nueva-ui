<?php
require_once('../Connections/cnn.php');
if (!function_exists("GetSQLValueString")) {

    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
        $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

        $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
}

mysql_select_db($database_cnn, $cnn);
mysql_query("SET NAMES 'utf8'");

$query_Attachments = "SELECT * FROM Attachments";
$Attachments = mysql_query($query_Attachments, $cnn) or die(mysql_error());
$row_Attachments = mysql_fetch_assoc($Attachments);
$totalRows_Attachments = mysql_num_rows($Attachments);
if($totalRows_Attachments>0){
    do{
        if(file_exists("attachments/".$row_Attachments['Llave_atta'].".ulm")) {
            unlink("attachments/".$row_Attachments['Llave_atta'].".ulm");
        }
        $sql_nueva_entrada = sprintf("DELETE FROM Attachments WHERE Id_atta=%s", 
                GetSQLValueString($row_Attachments['Id_atta'] , "int"));
        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    }while($row_Attachments = mysql_fetch_assoc($Attachments));
}

