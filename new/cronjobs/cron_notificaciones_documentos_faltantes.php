<?php
set_time_limit(0);
require_once('../ajax/activate_error.php');
require_once('../require_daos.php');

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();

$query="SELECT * FROM inscripciones_ulm 
        JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
        WHERE tipo=1  
              AND Activo_oferta=1 
              AND Baja_ofe IS NULL
              AND FechaCapturaEgreso IS NULL
              AND IdCicloEgreso IS NULL
              AND IdUsuEgreso IS NULL";
foreach ($base->advanced_query($query) as $k=>$v){
    $nombre_ori="";
    $oferta = $DaoOfertas->show($v['Id_ofe']);
    $esp = $DaoEspecialidades->show($v['Id_esp']);

    //Buscar documentos faltantes
    $Faltantes=$DaoAlumnos->getArchivosFaltantes($v['Id_ofe_alum']);
    if($Faltantes>0){
        $Documentos="";
        $DaoArchivosAlumno= new DaoArchivosAlumno();
        $countDoc=1;
        foreach($DaoArchivosAlumno->getArchivosAlumnoByOfertaAlumno($v['Id_ofe_alum']) as $documento){
            if ($documento['Id_file_original']<= 0) { 
               $Documentos.=$countDoc.".- ".$documento['Nombre_file']."<br>"; 
               $countDoc++;
            }
        }
        $Texto="Al día de hoy adeudas ".$Faltantes." documentos para la oferta academica ".$oferta->getNombre_oferta()."<br>
                Los cuales son los siguientes:<br>";
        
        $Texto.=$Documentos;
        $Texto=mb_strtoupper($Texto,'UTF-8');
        
        $DaoNotificacionesUsuario= new DaoNotificacionesUsuario();
        $NotificacionesUsuario= new NotificacionesUsuario();
        $NotificacionesUsuario->setDateCreated(date('Y-m-d H:i:s'));
        $NotificacionesUsuario->setIdRel($v['Id_ins']);
        $NotificacionesUsuario->setTipoRel('alumno');
        $NotificacionesUsuario->setTitulo("Documentos faltantes");
        $NotificacionesUsuario->setTexto($Texto);
        $DaoNotificacionesUsuario->add($NotificacionesUsuario);
    }
}