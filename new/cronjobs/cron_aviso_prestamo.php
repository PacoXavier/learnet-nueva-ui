<?php
set_time_limit(0);
require_once('../ajax/activate_error.php');
require_once('../require_daos.php');

$base= new base();
$DaoLibros=new DaoLibros();
$DaoActivos= new DaoActivos();
$DaoHistorialPrestamoActivo= new DaoHistorialPrestamoActivo();
$DaoHistorialPrestamoLibro= new DaoHistorialPrestamoLibro();
$DaoUsuarios= new DaoUsuarios();
$DaoDocentes= new DaoDocentes();
$DaoAlumnos= new DaoAlumnos();
$DaoNotificacionesUsuario= new DaoNotificacionesUsuario();

//Mensaje Activos
$hoy=date('Y-m-d');
$query="SELECT * FROM Historial_activo_prestamo WHERE Fecha_entregado IS NULL AND Fecha_devolucion<='".$hoy."'";
foreach($DaoHistorialPrestamoActivo->advanced_query($query) as $historial){
    $activo=$DaoActivos->show($historial->getId_activo());
    $usuEntrega = $DaoUsuarios->show($historial->getUsu_entrega());

    if($historial->getTipo_usu()=="usu"){
        $usuRecibe=$DaoUsuarios->show($historial->getUsu_recibe());
        $Nombreusu=$usuRecibe->getNombre_usu()." ".$usuRecibe->getApellidoP_usu()." ".$usuRecibe->getApellidoM_usu();
        $email=$usuRecibe->getEmail_usu();
        $Id_plantel=$usuRecibe->getId_plantel();
    }elseif($historial->getTipo_usu()=="docen"){
        $usuRecibe=$DaoDocentes->show($historial->getUsu_recibe());
        $Nombreusu=$usuRecibe->getNombre_docen()." ".$usuRecibe->getApellidoP_docen()." ".$usuRecibe->getApellidoM_docen();
        $email=$usuRecibe->getEmail_docen();
        $Id_plantel=$usuRecibe->getId_plantel();
    }elseif($historial->getTipo_usu()=="alum"){
        $usuRecibe=$DaoAlumnos->show($historial->getUsu_recibe());
        $Nombreusu=$usuRecibe->getNombre()." ".$usuRecibe->getApellidoP()." ".$usuRecibe->getApellidoM();
        $email=$usuRecibe->getEmail();
        $Id_plantel=$usuRecibe->getId_plantel();
    }
    if($historial->getFecha_devolucion()==$hoy){
         $mensaje="<p>Este mensaje es solo para notificarle que el día de hoy se vence el plazo para la entrega "
                 . "del activo <b>#".$activo->getCodigo()."-".$activo->getNombre()."</b>, solicitado el día <b>".$historial->getFecha_entrega()."</b></p>"
                 . "<p>Si ya fue entregado por favor hacer caso omiso de este mensaje.</p>";
    }else{
         $mensaje="<p>Este mensaje es solo para notificarte que la fecha de entrega del activo <b>#".$activo->getCodigo()."-".$activo->getNombre()."</b> fue el día ".$historial->getFecha_devolucion()."</p>";
         
        //************************************
        //Crear notificacion para el alumno por no entregar el activo a tiempo
        //************************************
        if($historial->getTipo_usu()=="alum"){
            $TituloNot="Activo pendiente de entregar";
            $TextoNot="La fecha de entrega del activo #".$activo->getCodigo()."-".$activo->getNombre()." fue el día ".$historial->getFecha_devolucion();
            $NotificacionesUsuario= new NotificacionesUsuario();
            $NotificacionesUsuario->setDateCreated(date('Y-m-d H:i:s'));
            $NotificacionesUsuario->setTitulo($TituloNot);
            $NotificacionesUsuario->setTexto($TextoNot);
            $NotificacionesUsuario->setIdRel($historial->getUsu_recibe());
            $NotificacionesUsuario->setTipoRel("alumno");
            $DaoNotificacionesUsuario->add($NotificacionesUsuario);
        }
        
        //************************************************************************************************************************************************
        //Crear notificacion por que no se entrego un activo a tiempo para los tipos de usuarios seleccionados en configurar
        //************************************************************************************************************************************************
        $recibir=$base->getParamPlantel('RecibirNotificacionPorNoEntregarLibro',$activo->getId_plantel());
        $arrayTipos=  explode(";",$recibir);
        foreach($arrayTipos as $TipoUsu){
            foreach($DaoUsuarios->getUsuariosTipo($TipoUsu,$Id_plantel) as $usuario){
                $TituloNot="Activo pendiente de entregar";
                $TextoNot=mb_strtoupper($Nombreusu,'UTF-8')." tiene pendiente de entregar el Activo ".$activo->getNombre().", su fecha de entrega expiro el  día ".$historial->getFecha_devolucion();
                $NotificacionesUsuario= new NotificacionesUsuario();
                $NotificacionesUsuario->setDateCreated(date('Y-m-d H:i:s'));
                $NotificacionesUsuario->setTitulo($TituloNot);
                $NotificacionesUsuario->setTexto($TextoNot);
                $NotificacionesUsuario->setIdRel($usuario->getId());
                $NotificacionesUsuario->setTipoRel("usuario");
                $DaoNotificacionesUsuario->add($NotificacionesUsuario);
            }
        }
    }
    //************************************
    //Crear email de notificacion para el activo
    //************************************
    if(strlen($email)>0){
       $arrayData= array();
       $arrayData['Asunto']="Prestamo de activo";
       $arrayData['Mensaje']=$mensaje;
       $arrayData['Id_plantel']=$usuEntrega->getId_plantel();
       $arrayData['Destinatarios']=array();
       
       $Data= array();
       $Data['email']= $email;
       $Data['name']= $Nombreusu;
       array_push($arrayData['Destinatarios'], $Data);
       $base->send_email($arrayData);
     }
}

//Mensaje Libros
//Si la fecha de entrega = null y la fecha de devolucion es <=hoy
$query="SELECT * FROM Historial_libro_prestamo WHERE Fecha_entregado IS NULL AND Fecha_devolucion<='".$hoy."'";
foreach($DaoHistorialPrestamoLibro->advanced_query($query) as $historial){
    $libro=$DaoLibros->show($historial->getId_libro());
    $usuEntrega = $DaoUsuarios->show($historial->getUsu_entrega());

    if($historial->getTipo_usu()=="usu"){
        $usuRecibe=$DaoUsuarios->show($historial->getUsu_recibe());
        $Nombreusu=$usuRecibe->getNombre_usu()." ".$usuRecibe->getApellidoP_usu()." ".$usuRecibe->getApellidoM_usu();
        $email=$usuRecibe->getEmail_usu();
        $Id_plantel=$usuRecibe->getId_plantel();
    }elseif($historial->getTipo_usu()=="docen"){
        $usuRecibe=$DaoDocentes->show($historial->getUsu_recibe());
        $Nombreusu=$usuRecibe->getNombre_docen()." ".$usuRecibe->getApellidoP_docen()." ".$usuRecibe->getApellidoM_docen();
        $email=$usuRecibe->getEmail_docen();
        $Id_plantel=$usuRecibe->getId_plantel();
    }elseif($historial->getTipo_usu()=="alum"){
        $usuRecibe=$DaoAlumnos->show($historial->getUsu_recibe());
        $Nombreusu=$usuRecibe->getNombre()." ".$usuRecibe->getApellidoP()." ".$usuRecibe->getApellidoM();
        $email=$usuRecibe->getEmail();
        $Id_plantel=$usuRecibe->getId_plantel();
    }
    if($historial->getFecha_devolucion()==$hoy){
         $mensaje="<p>Este mensaje es solo para notificarle que el día de hoy se vence el plazo para la entrega "
                 . "del libro <b>#".$libro->getCodigo()."-".$libro->getTitulo()."</b>, solicitado el día <b>".$historial->getFecha_entrega()."</b></p>"
                 . "<p>Si ya fue entregado por favor hacer caso omiso de este mensaje.</p>";
    }else{
        
        //************************************
        //Crear notificacion para el alumno por no entregar el libro a tiempo
        //************************************
        if($historial->getTipo_usu()=="alum"){
            $TituloNot="Libro pendiente de entregar";
            $TextoNot="La fecha de entrega del libro #".$libro->getCodigo()."-".$libro->getTitulo().", fue el día ".$historial->getFecha_devolucion();
            $NotificacionesUsuario= new NotificacionesUsuario();
            $NotificacionesUsuario->setDateCreated(date('Y-m-d H:i:s'));
            $NotificacionesUsuario->setTitulo($TituloNot);
            $NotificacionesUsuario->setTexto($TextoNot);
            $NotificacionesUsuario->setIdRel($historial->getUsu_recibe());
            $NotificacionesUsuario->setTipoRel("alumno");
            $DaoNotificacionesUsuario->add($NotificacionesUsuario);
        }
        
        
        //************************************
        //Crear cargo si es que la fecha de entrega del libro ya paso
        //************************************
        $base= new base();
        $totalCargo=$base->getParam("CargoEntregaLibroTarde", $Id_plantel);
        if($historial->getTipo_usu()=="alum" && $totalCargo>0){

           $DaoPagosCiclo= new DaoPagosCiclo();
           $DaoCiclosAlumno= new DaoCiclosAlumno();
           $DaoOfertasAlumno= new DaoOfertasAlumno();

           //Seleccionamos la ultima oferta del alumno para insertar el cargo
           $Id_ofe_alum=0;
           foreach($DaoOfertasAlumno->getOfertasAlumno($historial->getUsu_recibe()) as $ofertaAlumno){
               $Id_ofe_alum=$ofertaAlumno->getId();
           }
           //seleccionamos el ultimo ciclo de la oferta del alumno
           $ciclo=$DaoCiclosAlumno->getLastCicloOferta($Id_ofe_alum);
           if($ciclo->getId()>0){
               $fechaCargo=$DaoPagosCiclo->getLastCargoCiclo($ciclo->getId());
               if($fechaCargo->getId()>0){
                   $PagosCiclo= new PagosCiclo();
                   $PagosCiclo->setConcepto("Entrega tardía del libro ".$libro->getCodigo()." - ".$libro->getTitulo());
                   $PagosCiclo->setMensualidad($totalCargo);
                   $PagosCiclo->setFecha_pago($fechaCargo->getFecha_pago());
                   $PagosCiclo->setId_alum($historial->getUsu_recibe());
                   $PagosCiclo->setId_ciclo_alum($ciclo->getId());
                   $PagosCiclo->setIdRel($historial->getUsu_recibe());
                   $PagosCiclo->setTipoRel('alum');
                   $PagosCiclo->setTipo_pago('pago');
                   $DaoPagosCiclo->add($PagosCiclo);
               }
           }
        }
        
        //************************************************************************************************************************************************
        //Crear notificacion por que no se entrego un libro a tiempo para los tipos de usuarios seleccionados en configurar
        //************************************************************************************************************************************************
        $recibir=$base->getParam('RecibirNotificacionPorNoEntregarLibro');
        $arrayTipos=  explode(";",$recibir);
        foreach($arrayTipos as $TipoUsu){
            
            foreach($DaoUsuarios->getUsuariosTipo($TipoUsu,$Id_plantel) as $usuario){
                $TituloNot="Libro pendiente de entregar";
                $TextoNot=mb_strtoupper($Nombreusu,'UTF-8')." tiene pendiente de entregar el libro ".$libro->getTitulo().", su fecha de entrega expiro el  día ".$historial->getFecha_devolucion();
                $NotificacionesUsuario= new NotificacionesUsuario();
                $NotificacionesUsuario->setDateCreated(date('Y-m-d H:i:s'));
                $NotificacionesUsuario->setTitulo($TituloNot);
                $NotificacionesUsuario->setTexto($TextoNot);
                $NotificacionesUsuario->setIdRel($historial->getUsu_recibe());
                $NotificacionesUsuario->setTipoRel("usuario");
                $DaoNotificacionesUsuario->add($NotificacionesUsuario);
            }
        }
    }
    //************************************
    //Crear email de notificacion para el libro
    //************************************
    if(strlen($email)>0){
       $mensaje="<p>Este mensaje es solo para notificarte que la fecha de entrega del libro <b>#".$libro->getCodigo()."-".$libro->getTitulo()."</b> fue el día ".$historial->getFecha_devolucion()."</p>";
       $arrayData= array();
       $arrayData['Asunto']="Prestamo de libros";
       $arrayData['Mensaje']=$mensaje;
       $arrayData['Id_plantel']=$usuEntrega->getId_plantel();
       $arrayData['Destinatarios']=array();
       
       $Data= array();
       $Data['email']= $email;
       $Data['name']= $Nombreusu;
       array_push($arrayData['Destinatarios'], $Data);
       //$base->send_email($arrayData);

     }
}
