<?php
set_time_limit(0);
require_once('../ajax/activate_error.php');
require_once('../require_daos.php');

$base = new base();
$DaoOfertasAlumno = new DaoOfertasAlumno();
$DaoRecargos = new DaoRecargos();
$DaoEspecialidades = new DaoEspecialidades();
$DaoOfertas = new DaoOfertas();
$DaoCiclosAlumno = new DaoCiclosAlumno();
$DaoPagosCiclo = new DaoPagosCiclo();
$DaoAlumnos = new DaoAlumnos();
$DaoCiclos = new DaoCiclos();
$DaoPlanteles = new DaoPlanteles();

/*
  El chrom job se ejecutara todos los dias ya que los pagos tienen fechas distintas

  -Tomar los alumnos que se encuentran activos
  -Obtener las ofertas del alumno que se encuentran activas
  -Obtener los ciclos de cada oferta
  -Obtener los pagos de cada ciclo donde la fecha de pago < ala fecha de hoy

  $mensualidad =Mensualidad-Descuento-Descuento_beca
  si la ((Cantidad_pagada <50% de la  $mensualidad y si la mensualidad es diferente al descuento de la beca) && (no existe un recargo que sea del mes en el que se
  corre el scrip)
  entonces insertamos un recargo
 */


//$hoy=date("Y-m-d",mktime(0, 0, 0, 9, 6, 2015));
$hoy = date('Y-m-d');

foreach ($DaoPlanteles->showAll() as $plantel) {
    $ciclo = $DaoCiclos->getActual($plantel->getId());
    if ($ciclo->getId() > 0) {
        //Obtenemos todos los alumnos
        $query = "SELECT * FROM inscripciones_ulm 
        JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
        WHERE tipo=1  
              AND Activo_oferta=1 
              AND Baja_ofe IS NULL 
              AND FechaCapturaEgreso IS NULL 
              AND IdCicloEgreso IS NULL 
              AND IdUsuEgreso IS NULL 
              AND Id_plantel=".$plantel->getId()."
        ORDER BY Id_ins";
        foreach ($DaoAlumnos->advanced_query($query) as $alum) {
            echo $alum->getNombre() . "" . $alum->getApellidoP() . "<br>";

            //Ofertas del alumno
            foreach ($DaoOfertasAlumno->getOfertasAlumno($alum->getId()) as $k => $v) {
                //Obtengo los ciclos de la oferta que sean del ciclo actual ya que no quiero cobrar recargos futuros
                $query = "SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=" . $v->getId() . " AND Id_ciclo<=" . $ciclo->getId();
                foreach ($DaoCiclosAlumno->advanced_query($query) as $k2 => $v2) {

                    $query = "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=" . $v2->getId() . "  
                         AND Fecha_pago<'" . $hoy . "' 
                         AND (Pagado IS NULL OR Pagado=0)
                         AND Concepto!='Inscripción' 
                         AND DeadCargo IS NULL 
                         AND DeadCargoUsu IS NULL 
                         ORDER BY Id_pago_ciclo ASC";
                    foreach ($DaoPagosCiclo->advanced_query($query) as $k3 => $v3) {

                        //Esta siguiente seccion es para generar los recargos
                        $mensualidad = $v3->getMensualidad() - $v3->getDescuento() - $v3->getDescuento_beca();
                        //Si la cantidad pagada es menor al 50% y la mensualidad es diferente al descuento de la beca (es decir si la beca no es del 100%)
                        if (($v3->getCantidad_Pagada() < (($mensualidad) * 50 / 100)) && ($v3->getMensualidad() != $v3->getDescuento_beca())) {

                            $anio = date("Y", strtotime($hoy));
                            $mes = date("m", strtotime($hoy));
                            $dia = date("d", strtotime($hoy));
                            $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes, $dia + 1, $anio));

                            $recargo = $DaoRecargos->existRecargoMes($v3->getId(), $mes, $anio);
                            if ($recargo->getId() == 0 || $recargo->getId() == NULL) {

                                $oferta = $DaoOfertas->show($v->getId_ofe());
                                $monto = 0;
                                if ($oferta->getTipo_recargo() == "cargofijo") {
                                    $monto = $oferta->getValor();
                                } elseif ($oferta->getTipo_recargo() == "cargoporcentual") {
                                    $cargo = $DaoPagosCiclo->show($v3->getId());
                                    $monto = $cargo->getMensualidad() / $oferta->getValor();
                                }
                                $Recargos = new Recargos();
                                $Recargos->setId_pago($v3->getId());
                                $Recargos->setMonto_recargo($monto);
                                $Recargos->setFecha_recargo($fecha_pago);
                                //$DaoRecargos->add($Recargos);

                                echo "Oferta=" . $v2->getId() . " cargo= #" . $v3->getId() . ", " . $fecha_pago . ", recargo=" . $monto . "<br>";
                            }
                        }
                    }
                }
            }
            echo "<br>";
        }
    }

}


