<?php
require_once 'modelos/base.php';
require_once 'modelos/Materias.php';

class DaoMaterias extends base{
    
	public function add(Materias $x){
	    $query=sprintf("INSERT INTO materias_uml (Nombre, Clave_mat, Precio, Creditos, Tipo_aula, Dias_porSemana,Horas_independientes, Horas_porSemana, Horas_conDocente,
            Promedio_min, Duracion_examen, Max_alumExamen,Activo_mat ,Id_plantel) VALUES (%s, %s,%s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getClave_mat(), "text"), 
            $this->GetSQLValueString($x->getPrecio(), "double"), 
            $this->GetSQLValueString($x->getCreditos(), "int"), 
            $this->GetSQLValueString($x->getTipo_aula(), "int"),
            $this->GetSQLValueString($x->getDias_porSemana(), "int"),
            $this->GetSQLValueString($x->getHoras_independientes(), "int"),
            $this->GetSQLValueString($x->getHoras_porSemana(), "int"),
            $this->GetSQLValueString($x->getHoras_conDocente(), "int"),
            $this->GetSQLValueString($x->getPromedio_min(), "double"),
            $this->GetSQLValueString($x->getDuracion_examen(), "int"),
            $this->GetSQLValueString($x->getMax_alumExamen(), "int"),
            $this->GetSQLValueString($x->getActivo_mat(), "int"),
            $this->GetSQLValueString($x->getId_plantel(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->_cnn->insert_id; 
            }
	} 

    
	public function update(Materias $x){
	    $query=sprintf("UPDATE materias_uml SET  Nombre=%s, Clave_mat=%s, Precio=%s, Creditos=%s, Tipo_aula=%s, Dias_porSemana=%s,Horas_independientes=%s,Horas_porSemana=%s,Horas_conDocente=%s, 
            Promedio_min=%s, Duracion_examen=%s, Max_alumExamen=%s,Activo_mat=%s ,Id_plantel=%s  WHERE Id_mat=%s",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getClave_mat(), "text"), 
            $this->GetSQLValueString($x->getPrecio(), "double"), 
            $this->GetSQLValueString($x->getCreditos(), "int"), 
            $this->GetSQLValueString($x->getTipo_aula(), "int"),
            $this->GetSQLValueString($x->getDias_porSemana(), "int"),
            $this->GetSQLValueString($x->getHoras_independientes(), "int"),
            $this->GetSQLValueString($x->getHoras_porSemana(), "int"),
            $this->GetSQLValueString($x->getHoras_conDocente(), "int"),
            $this->GetSQLValueString($x->getPromedio_min(), "double"),
            $this->GetSQLValueString($x->getDuracion_examen(), "int"),
            $this->GetSQLValueString($x->getMax_alumExamen(), "int"),
            $this->GetSQLValueString($x->getActivo_mat(), "int"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	public function delete($Id){
            $query = sprintf("UPDATE materias_uml SET Activo_mat=0 WHERE Id_mat =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM materias_uml WHERE Id_mat= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM materias_uml WHERE Activo_mat=1 AND Id_plantel=".$this->Id_plantel;
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
        
        

	public function create_object($row){
            $x = new Materias();
            $x->setId($row['Id_mat']);
            $x->setNombre($row['Nombre']);
            $x->setClave_mat($row['Clave_mat']);
            $x->setPrecio($row['Precio']);
            $x->setCreditos($row['Creditos']);
            $x->setTipo_aula($row['Tipo_aula']);
            $x->setDias_porSemana($row['Dias_porSemana']);
            $x->setHoras_independientes($row['Horas_independientes']);
            $x->setHoras_porSemana($row['Horas_porSemana']);
            $x->setHoras_conDocente($row['Horas_conDocente']);
            $x->setPromedio_min($row['Promedio_min']);
            $x->setDuracion_examen($row['Duracion_examen']);
            $x->setMax_alumExamen($row['Max_alumExamen']);
            $x->setActivo_mat($row['Activo_mat']);
            $x->setId_plantel($row['Id_plantel']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function buscarMateria($buscar, $Id_ciclo, $limit = 20) {
           $query = "SELECT * FROM materias_uml 
                     WHERE (Nombre  LIKE '%" . $buscar . "%'  OR Clave_mat LIKE '%" . $buscar . "%')
                            AND Activo_mat=1" . $Id_ciclo . " 
                            AND materias_uml.Id_plantel=" . $this->Id_plantel . " ORDER BY Id_mat DESC LIMIT " . $limit;
            return $this->advanced_query($query);

       }

        public function existeQuery($query) {
            $Result1 = $this->_cnn->query($query);
            if (!$Result1) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            } else {
                return $this->create_object($Result1->fetch_assoc());
            }
        }
}
