<?php
require_once 'modelos/base.php';
require_once 'modelos/OpcionesPreguntaEncuesta.php';

class DaoOpcionesPreguntaEncuesta extends base{
    
        public $table="OpcionesPregunta";
    
	public function add(OpcionesPreguntaEncuesta $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_pre, TextoOpcion, Activo, Valor) VALUES (%s, %s,%s, %s)",
            $this->GetSQLValueString($x->getId_pre(), "int"), 
            $this->GetSQLValueString($x->getTextoOpcion(), "text"), 
            $this->GetSQLValueString($x->getActivo(), "int"), 
            $this->GetSQLValueString($x->getValor(), "double"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(OpcionesPreguntaEncuesta $x){
	    $query=sprintf("UPDATE ".$this->table." SET Id_pre=%s, TextoOpcion=%s, Activo=%s, Valor=%s WHERE Id_op=%s",
            $this->GetSQLValueString($x->getId_pre(), "int"), 
            $this->GetSQLValueString($x->getTextoOpcion(), "text"), 
            $this->GetSQLValueString($x->getActivo(), "int"), 
            $this->GetSQLValueString($x->getValor(), "double"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE ".$this->table." SET Activo=0 WHERE Id_op =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_op= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	public function create_object($row){
            $x = new OpcionesPreguntaEncuesta();
            $x->setId($row['Id_op']);
            $x->setId_pre($row['Id_pre']);
            $x->setTextoOpcion($row['TextoOpcion']);
            $x->setActivo($row['Activo']);
            $x->setValor($row['Valor']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getOpcionesPreguntasEncuesta($Id_pre){
            $query="SELECT *FROM ".$this->table." WHERE Activo=1 AND Id_pre=".$Id_pre;
            return $this->advanced_query($query);
        }


}
