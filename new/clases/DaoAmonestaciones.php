<?php
require_once 'modelos/base.php';
require_once 'modelos/Amonestaciones.php';

class DaoAmonestaciones extends base{

	public function add(Amonestaciones $x){
              $query=sprintf("INSERT INTO Reportes_alumno (Fecha_rep,Id_usu,Motivo,Id_ciclo,Id_recibe,Tipo_recibe, Id_reporta, Tipo_reporta,Id_plantel,FechaCaptura) VALUES (%s ,%s, %s, %s, %s, %s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getFecha_rep(), "date"),
              $this->GetSQLValueString($x->getId_usu(), "int"),
              $this->GetSQLValueString($x->getMotivo(), "text"),
              $this->GetSQLValueString($x->getId_ciclo(), "int"),
              $this->GetSQLValueString($x->getId_recibe(), "int"),
              $this->GetSQLValueString($x->getTipo_recibe(), "text"),
              $this->GetSQLValueString($x->getId_reporta(), "int"),
              $this->GetSQLValueString($x->getTipo_reporta(), "text"),
              $this->GetSQLValueString($x->getId_plantel(), "int"),
              $this->GetSQLValueString($x->getFechaCaptura(), "date"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(Amonestaciones $x){
	    $query=sprintf("UPDATE Reportes_alumno SET Fecha_rep=%s,Id_usu=%s,Motivo=%s,Id_ciclo=%s,Id_recibe=%s,Tipo_recibe=%s, Id_reporta=%s, Tipo_reporta=%s,Id_plantel=%s,FechaCaptura=%s  WHERE Id_rep = %s",
            $this->GetSQLValueString($x->getFecha_rep(), "date"),
            $this->GetSQLValueString($x->getId_usu(), "int"),
            $this->GetSQLValueString($x->getMotivo(), "text"),
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getId_recibe(), "int"),
            $this->GetSQLValueString($x->getTipo_recibe(), "text"),
            $this->GetSQLValueString($x->getId_reporta(), "int"),
            $this->GetSQLValueString($x->getTipo_reporta(), "text"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getFechaCaptura(), "date"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
                $query = sprintf("DELETE FROM Reportes_alumno WHERE Id_rep=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Reportes_alumno WHERE Id_rep= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll($search){
            $query="";
            if($search!=null){
               $query =$search;
            }
            $resp=array();
            $query="SELECT * FROM Reportes_alumno WHERE Id_plantel=".$this->Id_plantel." ".$query." ORDER BY Id_rep DESC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
	
	
	public function create_object($row){
            $x = new Amonestaciones();
            $x->setId($row['Id_rep']);
            $x->setFecha_rep($row['Fecha_rep']);
            $x->setId_usu($row['Id_usu']);
            $x->setMotivo($row['Motivo']);
            $x->setId_ciclo($row['Id_ciclo']); 
            $x->setId_recibe($row['Id_recibe']); 
            $x->setTipo_recibe($row['Tipo_recibe']); 
            $x->setId_reporta($row['Id_reporta']); 
            $x->setTipo_reporta($row['Tipo_reporta']); 
            $x->setId_plantel($row['Id_plantel']); 
            $x->setFechaCaptura($row['FechaCaptura']); 
            return $x;
	}
        
         public function advanced_query($query){
		    $resp=array();
		    $consulta=$this->_cnn->query($query);
		    if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	}
        
        public function getCantidadAmonestaciones($Id_recibe,$tipo_recibe){
            $count=0;
            $query= "SELECT * FROM Reportes_alumno WHERE Id_plantel=".$this->Id_plantel." AND Id_recibe=".$Id_recibe." AND Tipo_recibe='".$tipo_recibe."'";
            foreach($this->advanced_query($query) as $k=>$v){
                $count++;
            }
            return $count;
        } 
        
        public function getAmonestacionesReportado($Id_recibe,$tipo_recibe){
            $query= "SELECT * FROM Reportes_alumno WHERE Id_plantel=".$this->Id_plantel." AND Id_recibe=".$Id_recibe." AND Tipo_recibe='".$tipo_recibe."'";
            return $this->advanced_query($query);
        } 

        
}

