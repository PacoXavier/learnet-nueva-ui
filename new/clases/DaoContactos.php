<?php
require_once 'modelos/base.php';
require_once 'modelos/Contactos.php';


class DaoContactos extends base{
    
        public $table="Contactos";
    
	public function add(Contactos $x){
	    $query=sprintf("INSERT INTO ".$this->table."  (Nombre, Parentesco, Direccion, Tel_casa, Tel_ofi, Cel, Email , IdRel_con , TipoRel_con) VALUES (%s, %s,%s, %s,%s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getParentesco(), "text"), 
            $this->GetSQLValueString($x->getDireccion(), "text"), 
            $this->GetSQLValueString($x->getTel_casa(), "text"), 
            $this->GetSQLValueString($x->getTel_ofi(), "text"), 
            $this->GetSQLValueString($x->getCel(), "text"), 
            $this->GetSQLValueString($x->getEmail(), "text"), 
            $this->GetSQLValueString($x->getIdRel_con(), "int"), 
            $this->GetSQLValueString($x->getTipoRel_con(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
        
	public function update(Contactos $x){
	    $query=sprintf("UPDATE ".$this->table."  SET  Nombre=%s, Parentesco=%s, Direccion=%s, Tel_casa=%s, Tel_ofi=%s, Cel=%s, Email=%s, IdRel_con=%s, TipoRel_con=%s WHERE Id_cont=%s",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getParentesco(), "text"), 
            $this->GetSQLValueString($x->getDireccion(), "text"), 
            $this->GetSQLValueString($x->getTel_casa(), "text"), 
            $this->GetSQLValueString($x->getTel_ofi(), "text"), 
            $this->GetSQLValueString($x->getCel(), "text"), 
            $this->GetSQLValueString($x->getEmail(), "text"), 
            $this->GetSQLValueString($x->getIdRel_con(), "int"), 
            $this->GetSQLValueString($x->getTipoRel_con(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	
	public function show($Id){
	    $query="SELECT * FROM ".$this->table."  WHERE Id_cont= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	

	
	public function create_object($row){
            $x = new Contactos();
            $x->setId($row['Id_cont']);
            $x->setNombre($row['Nombre']);
            $x->setParentesco($row['Parentesco']);
            $x->setDireccion($row['Direccion']);
            $x->setTel_casa($row['Tel_casa']);
            $x->setTel_ofi($row['Tel_ofi']);
            $x->setCel($row['Cel']);
            $x->setEmail($row['Email']);
            $x->setIdRel_con($row['IdRel_con']);
            $x->setTipoRel_con($row['TipoRel_con']);
            return $x;
	}


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getContactos($Id_rel,$tipo){
            $query = "SELECT * FROM Contactos WHERE IdRel_con=".$Id_rel." AND TipoRel_con='".$tipo."'";
            return $this->advanced_query($query);
        }
        
        public function getPrimerContacto($Id_rel,$tipo){
	    $query = "SELECT * FROM Contactos WHERE IdRel_con=".$Id_rel." AND TipoRel_con='".$tipo."'";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
}
