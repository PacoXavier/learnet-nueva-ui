<?php
require_once 'modelos/base.php';
require_once 'modelos/OfertasInteres.php';

class DaoOfertasInteres extends base{
        public $table="Ofertas_interes";
        
	public function add(OfertasInteres $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_ofe, Id_alum, Id_esp, Id_ori,Id_ciclo,Turno,DateCreated,Id_usu) VALUES (%s, %s,%s, %s,%s, %s, %s, %s)",
            $this->GetSQLValueString($x->getId_ofe(), "int"), 
            $this->GetSQLValueString($x->getId_alum(), "int"), 
            $this->GetSQLValueString($x->getId_esp(), "int"), 
            $this->GetSQLValueString($x->getId_ori(), "int"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"), 
            $this->GetSQLValueString($x->getTurno(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getId_usu(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(OfertasInteres $x){
	    $query=sprintf("UPDATE ".$this->table."  SET Id_ofe=%s, Id_alum=%s, Id_esp=%s, Id_ori=%s,Id_ciclo=%s,Turno=%s,DateCreated=%s,Id_usu=%s WHERE Id_ofe_int=%s",
            $this->GetSQLValueString($x->getId_ofe(), "int"), 
            $this->GetSQLValueString($x->getId_alum(), "int"), 
            $this->GetSQLValueString($x->getId_esp(), "int"), 
            $this->GetSQLValueString($x->getId_ori(), "int"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"), 
            $this->GetSQLValueString($x->getTurno(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getId_usu(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM ".$this->table."   WHERE Id_ofe_int =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	
	public function show($Id){
	    $query="SELECT * FROM ".$this->table."  WHERE Id_ofe_int= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}


	
	public function create_object($row){
            $x = new OfertasInteres();
            $x->setId($row['Id_ofe_int']);
            $x->setId_ofe($row['Id_ofe']);
            $x->setId_alum($row['Id_alum']);
            $x->setId_esp($row['Id_esp']);
            $x->setId_ori($row['Id_ori']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setTurno($row['Turno']);
            $x->setDateCreated($row['DateCreated']);
            $x->setId_usu($row['Id_usu']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getOfertasAlumno($Id_alumno){
            $query="SELECT * FROM ".$this->table."  WHERE Id_alum=".$Id_alumno." ORDER BY DateCreated DESC";
            return $this->advanced_query($query);
        }
        
        public function existeQuery($query) {
            $Result1 = $this->_cnn->query($query);
            if (!$Result1) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            } else {
                return $this->create_object($Result1->fetch_assoc());
            }
        }

}
