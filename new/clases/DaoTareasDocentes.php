<?php
require_once 'modelos/base.php';
require_once 'modelos/TareasDocentes.php';

class DaoTareasDocentes extends base{
    
    public function add(TareasDocentes $x){
           $query=sprintf("INSERT INTO TareasDocentes ( Titulo, Descripcion, Ponderacion,Id_grupo,FechaCaptura, Id_docen,FechaEntrega) VALUES (%s, %s,%s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getTitulo(), "text"), 
            $this->GetSQLValueString($x->getDescripcion(), "text"), 
            $this->GetSQLValueString($x->getPonderacion(), "double"),
            $this->GetSQLValueString($x->getId_grupo(), "int"),
            $this->GetSQLValueString($x->getFechaCaptura(), "date"),
            $this->GetSQLValueString($x->getId_docen(), "int"),
             $this->GetSQLValueString($x->getFechaEntrega(), "date"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
public function update(TareasDocentes $x){
           $query=sprintf("UPDATE TareasDocentes SET Titulo=%s, Descripcion=%s, Ponderacion=%s,Id_grupo=%s,FechaCaptura=%s, Id_docen=%s,FechaEntrega=%s WHERE Id_tarea=%s",
            $this->GetSQLValueString($x->getTitulo(), "text"), 
            $this->GetSQLValueString($x->getDescripcion(), "text"), 
            $this->GetSQLValueString($x->getPonderacion(), "double"),
            $this->GetSQLValueString($x->getId_grupo(), "int"),
            $this->GetSQLValueString($x->getFechaCaptura(), "date"),
            $this->GetSQLValueString($x->getId_docen(), "int"),
             $this->GetSQLValueString($x->getFechaEntrega(), "date"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
public function delete($Id){
            $query = sprintf("DELETE FROM TareasDocentes WHERE Id_tarea=".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
}
	
public function show($Id){
        $query="SELECT * FROM TareasDocentes WHERE Id_tarea= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
}



	
public function create_object($row){
            $x = new TareasDocentes();
            $x->setId($row['Id_tarea']);
            $x->setTitulo($row['Titulo']);
            $x->setDescripcion($row['Descripcion']);
            $x->setPonderacion($row['Ponderacion']);
            $x->setId_grupo($row['Id_grupo']);
            $x->setFechaCaptura($row['FechaCaptura']);
            $x->setId_docen($row['Id_docen']);
            $x->setFechaEntrega($row['FechaEntrega']);
            return $x;
}

        
        

public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
}

public function getTareasDocente($Id_doc,$Id_grupo){
    $query="SELECT * FROM TareasDocentes WHERE Id_docen=".$Id_doc." AND Id_grupo=".$Id_grupo." ORDER BY Id_tarea ASC";
    return $this->advanced_query($query);
}
public function getTareasGrupo($Id_grupo){
    $query="SELECT * FROM TareasDocentes WHERE Id_grupo=".$Id_grupo." ORDER BY Id_tarea ASC";
    return $this->advanced_query($query);
}
}