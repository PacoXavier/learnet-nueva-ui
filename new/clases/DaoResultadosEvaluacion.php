<?php
require_once 'modelos/base.php';
require_once 'modelos/ResultadosEvaluacion.php';

class DaoResultadosEvaluacion extends base{
    
        public $table="ResultadosEvaluacion";
    
	public function add(ResultadosEvaluacion $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_camp, Puntos, Id_eva) VALUES (%s, %s,%s)",
            $this->GetSQLValueString($x->getId_camp(), "int"), 
            $this->GetSQLValueString($x->getPuntos(), "double"), 
            $this->GetSQLValueString($x->getId_eva(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(ResultadosEvaluacion $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Id_camp=%s, Puntos=%s, Id_eva=%s WHERE Id_resul=%s",
            $this->GetSQLValueString($x->getId_camp(), "int"), 
            $this->GetSQLValueString($x->getPuntos(), "double"), 
            $this->GetSQLValueString($x->getId_eva(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	
	public function delete($Id){
            $query = sprintf("DELETE FROM ".$this->table." WHERE Id_resul =%s",
            $this->GetSQLValueString($Id, "int")); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function deleteCamposPorEvaluacion($Id){
            $query = sprintf("DELETE FROM ".$this->table." WHERE Id_eva =%s",
            $this->GetSQLValueString($Id, "int")); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	public function show($Id){
	    $query=sprintf("SELECT * FROM ".$this->table." WHERE Id_resul=%s",
            $this->GetSQLValueString($Id, "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new ResultadosEvaluacion();
            $x->setId($row['Id_resul']);
            $x->setId_camp($row['Id_camp']);
            $x->setPuntos($row['Puntos']);
            $x->setId_eva($row['Id_eva']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                   do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        
        public function getResultadosEvaluacion($Id_eva){
            $query="SELECT * FROM ".$this->table." WHERE Id_eva=".$Id_eva." ORDER BY Id_resul ASC";
            return $this->advanced_query($query);
        }

        public function getCampoByEvaluacion($Id_eva,$Id_camp){
	    $query=sprintf("SELECT * FROM ".$this->table." WHERE Id_eva=%s AND Id_camp=%s",
            $this->GetSQLValueString($Id_eva, "int"),
            $this->GetSQLValueString($Id_camp, "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
}
