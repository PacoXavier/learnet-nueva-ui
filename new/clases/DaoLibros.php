<?php
require_once 'modelos/base.php';
require_once 'modelos/Libros.php';
require_once 'DaoCategoriasLibros.php';

class DaoLibros extends base{
    
	public function add(Libros $x){
	    $query=sprintf("INSERT INTO Libros (Titulo, Autor,Editorial,Anio,Edicion, Seccion, Id_aula,  Descripcion, Id_img, Codigo, Id_plantel,Baja_libro, Valor,  Fecha_adq, Garantia_meses, Comentario_baja, Disponible, Fecha_alta, ISBN, Id_cat, Id_subcat,Sinopsis ) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
            $this->GetSQLValueString($x->getTitulo(), "text"), 
            $this->GetSQLValueString($x->getAutor(), "text"), 
            $this->GetSQLValueString($x->getEditorial(), "text"), 
            $this->GetSQLValueString($x->getAnio(), "text"), 
            $this->GetSQLValueString($x->getEdicion(), "text"), 
            $this->GetSQLValueString($x->getSeccion(), "double"), 
            $this->GetSQLValueString($x->getId_aula(), "int"), 
            $this->GetSQLValueString($x->getDescripcion(), "text"), 
            $this->GetSQLValueString($x->getId_img(), "text"), 
            $this->GetSQLValueString($x->getCodigo(), "text"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"), 
            $this->GetSQLValueString($x->getBaja_libro(), "date"), 
            $this->GetSQLValueString($x->getValor(), "double"), 
            $this->GetSQLValueString($x->getFecha_adq(), "date"), 
            $this->GetSQLValueString($x->getGarantia_meses(), "text"), 
            $this->GetSQLValueString($x->getComentario_baja(), "text"), 
            $this->GetSQLValueString($x->getDisponible(), "int"), 
            $this->GetSQLValueString($x->getFecha_alta(), "date"),
            $this->GetSQLValueString($x->getISBN(), "text"),
            $this->GetSQLValueString($x->getId_cat(), "int"),
            $this->GetSQLValueString($x->getId_subcat(), "int"),
            $this->GetSQLValueString($x->getSinopsis(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Libros $x){
	    $query=sprintf("UPDATE Libros SET  Titulo=%s, Autor=%s,Editorial=%s,Anio=%s,Edicion=%s, Seccion=%s, Id_aula=%s,  Descripcion=%s, Id_img=%s, Codigo=%s, Id_plantel=%s, Baja_libro=%s, Valor=%s,  Fecha_adq=%s, Garantia_meses=%s, Comentario_baja=%s, Disponible=%s, Fecha_alta=%s, ISBN=%s, Id_cat=%s, Id_subcat=%s,Sinopsis=%s WHERE Id_lib=%s",
            $this->GetSQLValueString($x->getTitulo(), "text"), 
            $this->GetSQLValueString($x->getAutor(), "text"), 
            $this->GetSQLValueString($x->getEditorial(), "text"), 
            $this->GetSQLValueString($x->getAnio(), "text"), 
            $this->GetSQLValueString($x->getEdicion(), "text"), 
            $this->GetSQLValueString($x->getSeccion(), "double"), 
            $this->GetSQLValueString($x->getId_aula(), "int"), 
            $this->GetSQLValueString($x->getDescripcion(), "text"), 
            $this->GetSQLValueString($x->getId_img(), "text"), 
            $this->GetSQLValueString($x->getCodigo(), "text"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"), 
            $this->GetSQLValueString($x->getBaja_libro(), "date"), 
            $this->GetSQLValueString($x->getValor(), "double"), 
            $this->GetSQLValueString($x->getFecha_adq(), "date"), 
            $this->GetSQLValueString($x->getGarantia_meses(), "text"), 
            $this->GetSQLValueString($x->getComentario_baja(), "text"), 
            $this->GetSQLValueString($x->getDisponible(), "int"), 
            $this->GetSQLValueString($x->getFecha_alta(), "date"),
            $this->GetSQLValueString($x->getISBN(), "text"),
            $this->GetSQLValueString($x->getId_cat(), "int"),
            $this->GetSQLValueString($x->getId_subcat(), "int"),
            $this->GetSQLValueString($x->getSinopsis(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	
	public function show($Id){
	    $query="SELECT * FROM Libros WHERE Id_lib= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Libros WHERE Baja_libro IS NULL AND Id_plantel=".$this->Id_plantel." ORDER BY Fecha_alta DESC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new Libros();
            $x->setId($row['Id_lib']);
            $x->setTitulo($row['Titulo']);
            $x->setAutor($row['Autor']);
            $x->setEditorial($row['Editorial']);
            $x->setAnio($row['Anio']);
            $x->setEdicion($row['Edicion']);
            $x->setSeccion($row['Seccion']);
            $x->setId_aula($row['Id_aula']);
            $x->setDescripcion($row['Descripcion']);
            $x->setId_img($row['Id_img']);
            $x->setCodigo($row['Codigo']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setBaja_libro($row['Baja_libro']);
            $x->setFecha_adq($row['Fecha_adq']);
            $x->setGarantia_meses($row['Garantia_meses']);
            $x->setComentario_baja($row['Comentario_baja']);
            $x->setDisponible($row['Disponible']);
            $x->setFecha_alta($row['Fecha_alta']);
            $x->setISBN($row['ISBN']);
            $x->setId_cat($row['Id_cat']);
            $x->setId_subcat($row['Id_subcat']);
            $x->setSinopsis($row['Sinopsis']);
            $x->setValor($row['Valor']);
            
            return $x;
	}
        

        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


        public function buscarLibro($buscar,$limit=20){
            $resp=array();
            $query = "
            SELECT *
            FROM Libros 
                  WHERE
                    (Titulo LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR ISBN LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR Codigo LIKE '%".str_replace(' ','',$buscar)."%')
                    AND Id_plantel=".$this->Id_plantel." LIMIT ".$limit;
                $consulta=$this->_cnn->query($query);
                if(!$consulta){
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                    $row_consulta= $consulta->fetch_assoc();
                    $totalRows_consulta= $consulta->num_rows;
                    if($totalRows_consulta>0){
                           do{
                              array_push($resp, $this->create_object($row_consulta));
                       }while($row_consulta= $consulta->fetch_assoc());  
                    }
                }
                return $resp;
    }
    
    
    public function generar_codigo_activo($Id_cat,$Id_subcat) {
        //Generamos la matricula del activo
        //Tomando como base el primer digito es del tipo al que pertence
        //y los restantes son la ultima matricula existente
        $DaoCategoriasLibros= new DaoCategoriasLibros();
        $cat=$DaoCategoriasLibros->show($Id_cat);

        $codigoSubcat="";
        if($Id_subcat>0){
           $subcat=$DaoCategoriasLibros->show($Id_subcat);
           $codigoSubcat=$subcat->getCodigo();
        }

        $query = "SELECT * FROM Libros WHERE Codigo IS NOT NULL AND Id_plantel=".$this->Id_plantel."  ORDER BY Id_lib DESC LIMIT 1";
        $consulta = $this->_cnn->query($query);
        $row_consulta= $consulta->fetch_assoc();
        $totalRows_consulta= $consulta->num_rows;
      
        $Codigo=$row_consulta['Id_lib'];
        $Codigo++;
        $ceros="";
        for($i=0;$i<(3-strlen($Codigo));$i++){
         $ceros.="0";
        }
        $Codigo=$cat->getCodigo().$codigoSubcat.$ceros.$Codigo;
        return $Codigo;
    }
    

    public function update_codigo_activo($Id_cat,$Id_subcat,$Id_lib) {
          //Generamos la matricula del activo
          //Tomando como base el primer digito es del tipo al que pertence
          //y los restantes son la ultima matricula existente

          $DaoCategoriasLibros= new DaoCategoriasLibros();
          $cat=$DaoCategoriasLibros->show($Id_cat);
        

          $codigoSubcat="";
          if($Id_subcat>0){
             $subcat=$DaoCategoriasLibros->show($Id_subcat);
             $codigoSubcat=$subcat->getCodigo();
          }

          for($i=0;$i<(3-strlen($Id_lib));$i++){
           $ceros.="0";
          }
          $Codigo=$cat->getCodigo().$codigoSubcat.$ceros.$Id_lib;
          return $Codigo;
    }

    
    
}

