<?php
require_once 'modelos/base.php';
require_once 'modelos/PaquetesDescuentosAlumno.php';


class DaoPaquetesDescuentosAlumno extends base{
    
	public function add(PaquetesDescuentosAlumno $x){
	    $query=sprintf("INSERT INTO DescuentosPaqueteAlumno (Id_ofe_alum, Ia_paq, Porcentaje, FechaCaptura, Id_ciclo, Id_usu_captura) VALUES (%s, %s,%s, %s,%s, %s)",
            $this->GetSQLValueString($x->getId_ofe_alum(), "int"), 
            $this->GetSQLValueString($x->getId_paq(), "int"), 
            $this->GetSQLValueString($x->getPorcentaje(), "int"), 
            $this->GetSQLValueString($x->getFechaCaptura(), "date"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getId_usu_captura(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->_cnn->insert_id; 
            }
	} 
    
	public function update(PaquetesDescuentosAlumno $x){
	    $query=sprintf("UPDATE DescuentosPaqueteAlumno SET  Id_ofe_alum=%s, Ia_paq=%s, Porcentaje=%s, FechaCaptura=%s, Id_ciclo=%s, Id_usu_captura=%s  WHERE Id=%s",
            $this->GetSQLValueString($x->getId_ofe_alum(), "int"), 
            $this->GetSQLValueString($x->getId_paq(), "int"), 
            $this->GetSQLValueString($x->getPorcentaje(), "int"), 
            $this->GetSQLValueString($x->getFechaCaptura(), "date"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getId_usu_captura(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM DescuentosPaqueteAlumno WHERE Id =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM DescuentosPaqueteAlumno WHERE Id= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM DescuentosPaqueteAlumno";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	public function create_object($row){
            $x = new PaquetesDescuentosAlumno();
            $x->setId($row['Id']);
            $x->setId_ofe_alum($row['Id_ofe_alum']);
            $x->setId_paq($row['Ia_paq']);
            $x->setPorcentaje($row['Porcentaje']);
            $x->setFechaCaptura($row['FechaCaptura']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setId_usu_captura($row['Id_usu_captura']);
            return $x;
	}
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getPaqueteDescuentoCicloAlumno($Id_ciclo,$Id_ofe_alum){
	    $query="SELECT * FROM DescuentosPaqueteAlumno WHERE Id_ciclo= ".$Id_ciclo." AND Id_ofe_alum=".$Id_ofe_alum;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

}
