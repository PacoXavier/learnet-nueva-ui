<?php
require_once 'modelos/base.php';
require_once 'modelos/EncuestasEvaluacion.php';


class DaoEncuestasEvaluacion extends base{
    
        public $table="EncuestasEvaluacion";
    
	public function add(EncuestasEvaluacion $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Nombre, DateCreated, Id_usu, Id_ciclo, Activo, FechaInicio, Id_plantel) VALUES (%s, %s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_usu(), "int"),
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getFechaInicio(), "date"),
            $this->GetSQLValueString($x->getId_plantel(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(EncuestasEvaluacion $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Nombre=%s, DateCreated=%s, Id_usu=%s, Id_ciclo=%s, Activo=%s, FechaInicio=%s, Id_plantel=%s WHERE Id_en=%s",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_usu(), "int"),
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getFechaInicio(), "date"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	public function create_object($row){
            $x = new EncuestasEvaluacion();
            $x->setId($row['Id_en']);
            $x->setNombre($row['Nombre']);
            $x->setDateCreated($row['DateCreated']);
            $x->setId_usu($row['Id_usu']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setActivo($row['Activo']);
            $x->setFechaInicio($row['FechaInicio']);
            $x->setId_plantel($row['Id_plantel']);
            return $x;
	}
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_en= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
       public function delete($Id){
            $query = sprintf("UPDATE ".$this->table." SET Activo=0 WHERE Id_en =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function getEncuestasEvauacion(){
            $query="SELECT * FROM ".$this->table." WHERE Activo=1 ORDER Id_en ASC";
            return $this->advanced_query($query);
        }
        
        

}
