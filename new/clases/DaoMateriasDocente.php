<?php
require_once 'modelos/base.php';
require_once 'modelos/MateriasDocente.php';


class DaoMateriasDocente extends base{
    
	public function add(MateriasDocente $x){
	    $query=sprintf("INSERT INTO Materias_docentes (Id_materia, Id_profesor, Id_ciclo, Id_orientacion) VALUES (%s, %s, %s, %s)",
            $this->GetSQLValueString($x->getId_materia(), "int"), 
            $this->GetSQLValueString($x->getId_profesor(), "int"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"), 
            $this->GetSQLValueString($x->getId_orientacion(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

        
    
	public function update(MateriasDocente $x){
	    $query=sprintf("UPDATE Materias_docentes SET Id_materia=%s, Id_profesor=%s, Id_ciclo=%s, Id_orientacion=%s WHERE Id_matp=%s",
            $this->GetSQLValueString($x->getId_materia(), "int"), 
            $this->GetSQLValueString($x->getId_profesor(), "int"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"), 
            $this->GetSQLValueString($x->getId_orientacion(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function deleteByCicloDocente($Id_ciclo,$Id_docente){
            $query = sprintf("DELETE FROM Materias_docentes WHERE Id_ciclo =".$Id_ciclo." AND Id_profesor=".$Id_docente); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function delete($Id){
            $query = sprintf("DELETE FROM Materias_docentes WHERE Id_matp =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Materias_docentes WHERE Id_matp= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new MateriasDocente();
            $x->setId($row['Id_matp']);
            $x->setId_materia($row['Id_materia']);
            $x->setId_profesor($row['Id_profesor']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setId_orientacion($row['Id_orientacion']);

            return $x;
	}
        
	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getUltimoCicloDocente($Id_docen){
	    $query="SELECT * FROM Materias_docentes 
                    JOIN (
                          SELECT * FROM ciclos_ulm 
                                   WHERE Activo_ciclo=1 AND Id_plantel=" . $this->Id_plantel . " ORDER BY Id_ciclo DESC LIMIT 1
                         ) as cicloP 
                    ON Materias_docentes.Id_ciclo=cicloP.Id_ciclo 
                    WHERE Id_profesor=" . $Id_docen . " LIMIT 1";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}


}
