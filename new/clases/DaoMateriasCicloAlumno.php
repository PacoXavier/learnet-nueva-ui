<?php
require_once 'modelos/base.php';
require_once 'modelos/MateriasCicloAlumno.php';

class DaoMateriasCicloAlumno extends base{
    
	public function add(MateriasCicloAlumno $x){
	  $query=sprintf("INSERT INTO materias_ciclo_ulm (Id_ciclo_alum, Id_mat_esp, Id_ori, Id_grupo, CalExtraordinario, CalEspecial,CalTotalParciales, Tipo,Ids_pagos, Activo, Id_alum) VALUES (%s, %s,%s, %s,%s, %s,%s, %s,%s,%s,%s)",
            $this->GetSQLValueString($x->getId_ciclo_alum(), "int"), 
            $this->GetSQLValueString($x->getId_mat_esp(), "int"), 
            $this->GetSQLValueString($x->getId_ori(), "int"), 
            $this->GetSQLValueString($x->getId_grupo(), "int"), 
            $this->GetSQLValueString($x->getCalExtraordinario(), "text"), 
            $this->GetSQLValueString($x->getCalEspecial(), "text"), 
            $this->GetSQLValueString($x->getCalTotalParciales(), "text"),
            $this->GetSQLValueString($x->getTipo(), "text"),
            $this->GetSQLValueString($x->getIds_pagos(), "text"),
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getId_alum(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(MateriasCicloAlumno $x){
	    $query=sprintf("UPDATE materias_ciclo_ulm SET Id_ciclo_alum=%s, Id_mat_esp=%s, Id_ori=%s, Id_grupo=%s, CalExtraordinario=%s, CalEspecial=%s,CalTotalParciales=%s, Tipo=%s,Ids_pagos=%s, Activo=%s, Id_alum=%s WHERE Id_ciclo_mat=%s",
            $this->GetSQLValueString($x->getId_ciclo_alum(), "int"), 
            $this->GetSQLValueString($x->getId_mat_esp(), "int"), 
            $this->GetSQLValueString($x->getId_ori(), "int"), 
            $this->GetSQLValueString($x->getId_grupo(), "int"), 
            $this->GetSQLValueString($x->getCalExtraordinario(), "text"), 
            $this->GetSQLValueString($x->getCalEspecial(), "text"), 
            $this->GetSQLValueString($x->getCalTotalParciales(), "text"),
            $this->GetSQLValueString($x->getTipo(), "text"),
            $this->GetSQLValueString($x->getIds_pagos(), "text"),
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getId_alum(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}
	
	
	
        
	public function delete($Id){
            $query = sprintf("DELETE FROM  materias_ciclo_ulm WHERE Id_ciclo_mat =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        

	
	public function show($Id){
	    $query="SELECT * FROM materias_ciclo_ulm WHERE Id_ciclo_mat= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function getAlumnosGrupo($Id_ciclo,$Id_grupo){
            $resp=array();
            $query="
                SELECT * FROM ofertas_alumno 
                JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
                JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
                JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
                WHERE  ciclos_alum_ulm.Id_ciclo=".$Id_ciclo." AND materias_ciclo_ulm.Id_grupo=".$Id_grupo." AND Activo_oferta=1";   
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	public function create_object($row){
            $x = new MateriasCicloAlumno();
            $x->setId($row['Id_ciclo_mat']);
            $x->setId_ciclo_alum($row['Id_ciclo_alum']);
            $x->setId_mat_esp($row['Id_mat_esp']);
            $x->setId_ori($row['Id_ori']);
            $x->setId_grupo($row['Id_grupo']);
            $x->setCalExtraordinario($row['CalExtraordinario']);
            $x->setCalEspecial($row['CalEspecial']);
            $x->setCalTotalParciales($row['CalTotalParciales']);
            $x->setTipo($row['Tipo']);
            $x->setIds_pagos($row['Ids_pagos']);
            $x->setActivo($row['Activo']);
            $x->setId_alum($row['Id_alum']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getMateriasByIdOfeAlumnAndIdCiclo($Id_ofe_alum,$Id_ciclo) {
            $query = "SELECT * FROM ciclos_alum_ulm 
             JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
             WHERE Id_ofe_alum=" . $Id_ofe_alum . " AND Id_ciclo=".$Id_ciclo." AND Activo=1 ORDER BY Id_ciclo ASC";
            return $this->advanced_query($query);
             
         }
    
        public function getMateriasCicloAlumno($Id_ciclo_alum) {
            $query = "SELECT * FROM materias_ciclo_ulm WHERE Id_ciclo_alum=".$Id_ciclo_alum." AND Activo=1";
            return $this->advanced_query($query);
             
         }
         
       public function deleteMateriasByIdCicloAlumno($Id_ciclo_alum){
            $query = sprintf("DELETE FROM  materias_ciclo_ulm WHERE Id_ciclo_alum =".$Id_ciclo_alum); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function getMateriaCicloAlumno($Id_ciclo_alum,$Id_alum,$Id_mat_esp){
	    $query="SELECT * FROM materias_ciclo_ulm WHERE Id_ciclo_alum= ".$Id_ciclo_alum." AND Id_alum=".$Id_alum." AND Id_mat_esp=".$Id_mat_esp;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function existeQuery($query) {
            $Result1 = $this->_cnn->query($query);
            if (!$Result1) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            } else {
                return $this->create_object($Result1->fetch_assoc());
            }
        }
    
        
        public function getMaterias($Id_mat_esp) {
            $query = "SELECT * FROM materias_ciclo_ulm WHERE Id_mat_esp=".$Id_mat_esp;
            return $this->advanced_query($query);
        }
        
}