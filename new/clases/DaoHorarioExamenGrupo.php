<?php
require_once 'modelos/base.php';
require_once 'modelos/HorarioExamenGrupo.php';
class DaoHorarioExamenGrupo extends base{
	public function add(HorarioExamenGrupo $x){
	    $query=sprintf("INSERT INTO HorarioExamenGrupo (Id_aula, FechaAplicacion,Start, End, DateCreated, Id_usu,IdHoraIni,IdHoraFin,Id_ciclo,Id_periodoExamen) VALUES (%s, %s,%s, %s, %s, %s,%s, %s, %s, %s)",
            $this->GetSQLValueString($x->getId_aula(), "int"), 
            $this->GetSQLValueString($x->getFechaAplicacion(), "date"), 
            $this->GetSQLValueString($x->getStart(), "text"),
            $this->GetSQLValueString($x->getEnd(), "text"),
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_usu(), "int"),
            $this->GetSQLValueString($x->getIdHoraIni(), "int"),
            $this->GetSQLValueString($x->getIdHoraFin(), "int"),
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getId_periodoExamen(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(HorarioExamenGrupo $x){
	    $query=sprintf("UPDATE HorarioExamenGrupo SET Id_aula=%s, FechaAplicacion=%s, Start=%s, End=%s,DateCreated=%s, Id_usu=%s,IdHoraIni=%s,IdHoraFin=%s,Id_ciclo=%s,Id_periodoExamen=%s WHERE Id_horario=%s",
            $this->GetSQLValueString($x->getId_aula(), "int"), 
            $this->GetSQLValueString($x->getFechaAplicacion(), "date"), 
            $this->GetSQLValueString($x->getStart(), "text"),
            $this->GetSQLValueString($x->getEnd(), "text"),
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_usu(), "int"),
            $this->GetSQLValueString($x->getIdHoraIni(), "int"),
            $this->GetSQLValueString($x->getIdHoraFin(), "int"),
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getId_periodoExamen(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM HorarioExamenGrupo WHERE Id_horario =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	
	public function show($Id){
	    $query="SELECT * FROM HorarioExamenGrupo WHERE Id_horario= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}



	
	public function create_object($row){
            $x = new HorarioExamenGrupo();
            $x->setId($row['Id_horario']);
            $x->setId_aula($row['Id_aula']);
            $x->setStart($row['Start']);
            $x->setEnd($row['End']);
            $x->setFechaAplicacion($row['FechaAplicacion']);
            $x->setDateCreated($row['DateCreated']);
            $x->setId_usu($row['Id_usu']);
            $x->setIdHoraIni($row['IdHoraIni']);
            $x->setIdHoraFin($row['IdHoraFin']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setId_periodoExamen($row['Id_periodoExamen']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getExamenesCiclo($Id_ciclo){
            $query="SELECT * FROM HorarioExamenGrupo WHERE  Id_ciclo=".$Id_ciclo;
            return $this->advanced_query($query);
        }

        
        public function getExamenGrupo($Id_grupo){
            $query="SELECT * FROM HorarioExamenGrupo 
                   JOIN GrupoExamen ON HorarioExamenGrupo.Id_horario=GrupoExamen.IdHorarioExamen
                   WHERE GrupoExamen.IdGrupo=".$Id_grupo;
            $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
        }
        
        
        public function getDisponibilidadSalon($Id_aula,$IdHoraIni,$IdHoraFin,$FechaAplicacion){
            //Por si alguien mas quiere agendar entre las horas de inicio y fin de un salon ya seleccionado
	    $query="SELECT * FROM HorarioExamenGrupo 
                          WHERE Id_aula= ".$Id_aula." 
                                AND (IdHoraIni IN(
                                            SELECT Id_hora FROM Horas WHERE Id_hora>= ".$IdHoraIni." AND Id_hora<".$IdHoraFin."
                                          )
                                 OR IdHoraFin IN(
                                          SELECT Id_hora FROM Horas WHERE Id_hora> ".$IdHoraIni." AND Id_hora<".$IdHoraFin."
                                 )
                                 
                                 OR IdHoraIni<=$IdHoraIni AND IdHoraFin>=$IdHoraFin
                               )
                                AND FechaAplicacion='".$FechaAplicacion."'";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function getStatusGrupo($Id_grupo,$Id_periodo){
            $status=0;
	    $query="SELECT * FROM HorarioExamenGrupo 
                    JOIN GrupoExamen ON HorarioExamenGrupo.Id_horario=GrupoExamen.IdHorarioExamen
                    WHERE GrupoExamen.IdGrupo= ".$Id_grupo." 
                         AND HorarioExamenGrupo.Id_periodoExamen=".$Id_periodo;
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                   $status=$row_consulta['Id_horario'];
                }
            }
            return $status;
	}
        
        
        public function validarGrupoPeriodoExamen($Id_grupo,$Id_periodo){
	    $query="SELECT * FROM HorarioExamenGrupo WHERE Id_grupo= ".$Id_grupo."  AND Id_periodoExamen=".$Id_periodo;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

        public function validarDisponibilidadDocente($Dia,$Id_docen,$IdHoraIni,$IdHoraFin){
	    $query="SELECT * FROM HorarioExamenGrupo 
                    JOIN DocenteExamen ON HorarioExamenGrupo.Id_horario=DocenteExamen.IdHorarioExamen
                    WHERE FechaAplicacion= '".$Dia."'
                           AND DocenteExamen.Id_docen=".$Id_docen." 
                           AND (IdHoraIni IN(
                                            SELECT Id_hora FROM Horas WHERE Id_hora>= ".$IdHoraIni." AND Id_hora<".$IdHoraFin."
                                          )
                                 OR IdHoraFin IN(
                                          SELECT Id_hora FROM Horas WHERE Id_hora> ".$IdHoraIni." AND Id_hora<".$IdHoraFin."
                                 )
                                 
                                 OR IdHoraIni<=$IdHoraIni AND IdHoraFin>=$IdHoraFin
                               )";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        
        
        /*
         public function validarDisponibilidadDiaHoraSalon($Id_aula,$IdHoraIni,$IdHoraFin,$FechaAplicacion,$Id_docen){
            //En esta consulta debemos conocer elperidod de otras que se encuentran entra la fecha de inicio y la fecha de fin
            //Por si alguien mas quiere agendar entre las horas de inicio y fin de un salon ya seleccionado
	    $query="SELECT * FROM HorarioExamenGrupo 
                          WHERE Id_aula= ".$Id_aula." 
                                AND (IdHoraIni IN(
                                            SELECT Id_hora FROM Horas WHERE Id_hora>= ".$IdHoraIni." AND Id_hora<".$IdHoraFin."
                                          )
                                 OR IdHoraFin IN(
                                          SELECT Id_hora FROM Horas WHERE Id_hora> ".$IdHoraIni." AND Id_hora<".$IdHoraFin."
                                 )
                                 
                                 OR IdHoraIni<=$IdHoraIni AND IdHoraFin>=$IdHoraFin
                               )
                                AND FechaAplicacion='".$FechaAplicacion."' AND Id_docen!=".$Id_docen;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function validarGrupo($Id_grupo,$Id_docen,$Id_periodo){
	    $query="SELECT * FROM HorarioExamenGrupo WHERE Id_grupo= ".$Id_grupo." AND Id_docen!=".$Id_docen." AND Id_periodoExamen=".$Id_periodo;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function validarGrupoPeriodoExamen($Id_grupo,$Id_periodo){
	    $query="SELECT * FROM HorarioExamenGrupo WHERE Id_grupo= ".$Id_grupo."  AND Id_periodoExamen=".$Id_periodo;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

        public function validarDisponibilidadDocente($Dia,$Id_docen,$IdHoraIni,$IdHoraFin){
	    $query="SELECT * FROM HorarioExamenGrupo 
                    WHERE FechaAplicacion= '".$Dia."'
                           AND Id_docen=".$Id_docen." 
                           AND (IdHoraIni IN(
                                            SELECT Id_hora FROM Horas WHERE Id_hora>= ".$IdHoraIni." AND Id_hora<".$IdHoraFin."
                                          )
                                 OR IdHoraFin IN(
                                          SELECT Id_hora FROM Horas WHERE Id_hora> ".$IdHoraIni." AND Id_hora<".$IdHoraFin."
                                 )
                                 
                                 OR IdHoraIni<=$IdHoraIni AND IdHoraFin>=$IdHoraFin
                               )";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

         */

}
