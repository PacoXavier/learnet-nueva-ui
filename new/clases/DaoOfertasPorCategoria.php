<?php
require_once 'modelos/base.php';
require_once 'modelos/OfertasPorCategoria.php';

class DaoOfertasPorCategoria extends base{
    
        public $table="OfertasPorCategoria";
    
	public function add(OfertasPorCategoria $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_ofe, Id_tipo) VALUES (%s, %s)",
            $this->GetSQLValueString($x->getId_ofe(), "int"), 
            $this->GetSQLValueString($x->getId_tipo(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(OfertasPorCategoria $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Id_ofe=%s, Id_tipo=%s WHERE Id=%s",
            $this->GetSQLValueString($x->getId_ofe(), "int"), 
            $this->GetSQLValueString($x->getId_tipo(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	
	public function delete($Id){
            $query = sprintf("UPDATE ".$this->table." SET Activo=0 WHERE Id =%s",
            $this->GetSQLValueString($Id, "int")); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function deleteByCategoria($Id){
            $query = sprintf("DELETE FROM ".$this->table."  WHERE Id_tipo =%s",
            $this->GetSQLValueString($Id, "int")); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	public function show($Id){
	    $query=sprintf("SELECT * FROM ".$this->table." WHERE Id=%s",
            $this->GetSQLValueString($Id, "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function getOfertaCategoria($Id_cat,$Id_ofe){
	    $query=sprintf("SELECT * FROM ".$this->table." WHERE Id_tipo=%s AND Id_ofe=%s",
            $this->GetSQLValueString($Id_cat, "int"),
            $this->GetSQLValueString($Id_ofe, "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        
        public function getCategoriaPorOferta($Id_ofe){
	    $query=sprintf("SELECT * FROM ".$this->table." WHERE Id_ofe=%s",
            $this->GetSQLValueString($Id_ofe, "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
	
	public function create_object($row){
            $x = new OfertasPorCategoria();
            $x->setId($row['Id']);
            $x->setId_ofe($row['Id_ofe']);
            $x->setId_tipo($row['Id_tipo']);
            return $x;
	}


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        
        

}
