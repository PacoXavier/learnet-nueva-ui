<?php
require_once 'modelos/base.php';
require_once 'modelos/HistorialSeguimiento.php';

class DaoHistorialSeguimiento extends base{
	public function add(HistorialSeguimiento $x){
	    $query=sprintf("INSERT INTO logs_inscripciones_ulm (Id_usu, Fecha_log, Comen_log, Idrel_log, Tipo_relLog, Dia_contactar, DateDead_not, Id_usuRead_not,TipoSeguimiento) VALUES (%s, %s,%s, %s,%s, %s,%s, %s, %s)",
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getFecha_log(), "date"), 
            $this->GetSQLValueString($x->getComen_log(), "text"), 
            $this->GetSQLValueString($x->getIdrel_log(), "int"), 
            $this->GetSQLValueString($x->getTipo_relLog(), "text"), 
            $this->GetSQLValueString($x->getDia_contactar(), "date"),
            $this->GetSQLValueString($x->getDateDead_not(), "date"),
            $this->GetSQLValueString($x->getId_usuRead_not(), "int"),
            $this->GetSQLValueString($x->getTipoSeguimiento(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(HistorialSeguimiento $x){
	    $query=sprintf("UPDATE logs_inscripciones_ulm SET  Id_usu=%s, Fecha_log=%s, Comen_log=%s, Idrel_log=%s, Tipo_relLog=%s, Dia_contactar=%s, DateDead_not=%s, Id_usuRead_not=%s,TipoSeguimiento=%s WHERE Id_aula=%s",
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getFecha_log(), "date"), 
            $this->GetSQLValueString($x->getComen_log(), "text"), 
            $this->GetSQLValueString($x->getIdrel_log(), "int"), 
            $this->GetSQLValueString($x->getTipo_relLog(), "text"), 
            $this->GetSQLValueString($x->getDia_contactar(), "date"),
            $this->GetSQLValueString($x->getDateDead_not(), "date"),
            $this->GetSQLValueString($x->getId_usuRead_not(), "int"),
           $this->GetSQLValueString($x->getTipoSeguimiento(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	public function show($Id){
	    $query="SELECT * FROM logs_inscripciones_ulm WHERE Id_log= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM logs_inscripciones_ulm";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new HistorialSeguimiento();
            $x->setId($row['Id_log']);
            $x->setId_usu($row['Id_usu']);
            $x->setFecha_log($row['Fecha_log']);
            $x->setComen_log($row['Comen_log']);
            $x->setIdrel_log($row['Idrel_log']);
            $x->setTipo_relLog($row['Tipo_relLog']);
            $x->setDia_contactar($row['Dia_contactar']);
            $x->setDateDead_not($row['DateDead_not']);
            $x->setId_usuRead_not($row['Id_usuRead_not']);
            $x->setTipoSeguimiento($row['TipoSeguimiento']);
            return $x;
	}
        


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getHistorialSeguimiento($tipo,$IdRel){
            $query="SELECT * FROM logs_inscripciones_ulm WHERE Tipo_relLog='".$tipo."' AND Idrel_log=".$IdRel." ORDER BY Fecha_log DESC";
            return $this->advanced_query($query);
        }
        
        
        	
	public function getExistioSeguimiento($Id_log){
            $log=$this->show($Id_log);
	    $query="SELECT * FROM logs_inscripciones_ulm WHERE IdRel_log= ".$log->getIdrel_log()." AND Tipo_relLog='inte' AND Fecha_log>'".$log->getDia_contactar()."' AND Id_log!=".$log->getId();
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        


}
