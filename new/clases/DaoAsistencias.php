<?php
require_once 'modelos/base.php';
require_once 'modelos/Asistencias.php';

class DaoAsistencias extends base{
	public function add(Asistencias $x){
	    $query=sprintf("INSERT INTO Asistencias (Id_grupo, Id_ciclo, Id_alum, Fecha_asis, Mes_asis, Anio_asis, Asistio,Id_docente) VALUES (%s, %s,%s, %s,%s, %s, %s, %s)",
            $this->GetSQLValueString($x->getId_grupo(), "int"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"), 
            $this->GetSQLValueString($x->getId_alum(), "int"), 
            $this->GetSQLValueString($x->getFecha_asis(), "date"), 
            $this->GetSQLValueString($x->getMes_asis(), "int"), 
            $this->GetSQLValueString($x->getAnio_asis(), "int"), 
            $this->GetSQLValueString($x->getAsistio(), "int"),
            $this->GetSQLValueString($x->getId_docente(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Asistencias $x){
	    $query=sprintf("UPDATE Asistencias SET  Id_grupo=%s, Id_ciclo=%s, Id_alum=%s, Fecha_asis=%s, Mes_asis=%s, Anio_asis=%s, Asistio=%s, Id_docente=%s WHERE Id_asis=%s",
            $this->GetSQLValueString($x->getId_grupo(), "int"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"), 
            $this->GetSQLValueString($x->getId_alum(), "int"), 
            $this->GetSQLValueString($x->getFecha_asis(), "date"), 
            $this->GetSQLValueString($x->getMes_asis(), "int"), 
            $this->GetSQLValueString($x->getAnio_asis(), "int"), 
            $this->GetSQLValueString($x->getAsistio(), "int"),
            $this->GetSQLValueString($x->getId_docente(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE Asistencias SET Asistio=0 WHERE Id_asis =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
         public function deleteAsistenciasGrupos($Id_grupo, $Id_ciclo, $Id_alum, $Mes_asis, $Anio_asis ){
            $query = sprintf("DELETE FROM Asistencias WHERE Id_grupo=".$Id_grupo." AND Id_ciclo=".$Id_ciclo." AND Id_alum=".$Id_alum." AND Mes_asis=".$Mes_asis." AND Anio_asis=".$Anio_asis); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Asistencias WHERE Id_asis= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Asistencias WHERE Asistio=1";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new Asistencias();
            $x->setId($row['Id_asis']);
            $x->setId_grupo($row['Id_grupo']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setId_alum($row['Id_alum']);
            $x->setFecha_asis($row['Fecha_asis']);
            $x->setMes_asis($row['Mes_asis']);
            $x->setAnio_asis($row['Anio_asis']);
            $x->setAsistio($row['Asistio']);
            $x->setId_docente($row['Id_docente']);
            return $x;
	}


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        
        public function existAsistencia($Id_grupo,$Fecha){
            $totalRows_consulta=0;
            $query = "SELECT * From Asistencias WHERE Id_grupo=".$Id_grupo." AND Fecha_asis='".$Fecha."' GROUP BY Fecha_asis ORDER BY Fecha_asis ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
            }
            return $totalRows_consulta;
        }

        public function deleteAsistencias($Id_grupo, $Id_ciclo, $Fecha_asis,$Id_alum=null, $Mes_asis=null, $Anio_asis=null ){
            
            $array=array();
            if($Id_grupo!=null){
                array_push($array, "Id_grupo=".$Id_grupo);
            }
            
            if($Id_ciclo!=null){
                array_push($array, "Id_ciclo=".$Id_ciclo);
            }
            if($Fecha_asis!=null){
                    array_push($array, "Fecha_asis='".$Fecha_asis."'");
            }
            if($Id_alum!=null){
                array_push($array, "Id_alum=".$Id_alum);
            }
            
            if($Mes_asis!=null){
                array_push($array, "Mes_asis=".$Mes_asis);
            }
            
            if($Anio_asis!=null){
                array_push($array, "Anio_asis=".$Anio_asis);
            }
            $q = implode(" AND ", $array);
            
            $query = sprintf("DELETE FROM Asistencias WHERE ".$q); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al eliminar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}   
        

}
