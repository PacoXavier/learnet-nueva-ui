<?php
require_once 'modelos/base.php';
require_once 'modelos/DisponibilidadDocente.php';

class DaoDisponibilidadDocente extends base{
        public $table="Disponibilidad_docente";

	public function add(DisponibilidadDocente $x){
                $query=sprintf("INSERT INTO ".$this->table." (Id_hora, Lunes, Martes, Miercoles,Jueves, Viernes, Sabado, Id_docente, Id_ciclo,Domingo) VALUES (%s ,%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                $this->GetSQLValueString($x->getId_hora(), "int"),
                $this->GetSQLValueString($x->getLunes(), "int"),
                $this->GetSQLValueString($x->getMartes(), "int"),
                $this->GetSQLValueString($x->getMiercoles(), "int"),
                $this->GetSQLValueString($x->getJueves(), "int"),
                $this->GetSQLValueString($x->getViernes(), "int"),
                $this->GetSQLValueString($x->getSabado(), "int"),
                $this->GetSQLValueString($x->getId_docente(), "int"),
                $this->GetSQLValueString($x->getId_ciclo(), "int"),
                $this->GetSQLValueString($x->getDomingo(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(DisponibilidadDocente $x){
	    $query=sprintf("UPDATE ".$this->table." SET Id_hora, Lunes=%s, Martes=%s, Miercoles=%s,Jueves=%s, Viernes=%s, Sabado=%s, Id_docente=%s, Id_ciclo=%s,Domingo=%s  WHERE Id_dis = %s",
            $this->GetSQLValueString($x->getId_hora(), "int"),
            $this->GetSQLValueString($x->getLunes(), "int"),
            $this->GetSQLValueString($x->getMartes(), "int"),
            $this->GetSQLValueString($x->getMiercoles(), "int"),
            $this->GetSQLValueString($x->getJueves(), "int"),
            $this->GetSQLValueString($x->getViernes(), "int"),
            $this->GetSQLValueString($x->getSabado(), "int"),
            $this->GetSQLValueString($x->getId_docente(), "int"),
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getDomingo(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}

	public function delete($Id){
                $query = sprintf("DELETE FROM ".$this->table." WHERE Id_dis=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
        
        public function deleteDisponibilidadDocenteCiclo($Id_docente,$Id_ciclo){
                $query = sprintf("DELETE FROM ".$this->table." WHERE Id_docente=".$Id_docente." AND Id_ciclo=".$Id_ciclo); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
	
	public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_dis= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new DisponibilidadDocente();
            $x->setId($row['Id_dis']);
            $x->setId_hora($row['Id_hora']);
            $x->setLunes($row['Lunes']);
            $x->setMartes($row['Martes']);
            $x->setMiercoles($row['Miercoles']);
            $x->setJueves($row['Jueves']);
            $x->setViernes($row['Viernes']);
            $x->setSabado($row['Sabado']);
            $x->setId_docente($row['Id_docente']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setDomingo($row['Domingo']);
            return $x;
	}
        
         public function advanced_query($query){
                $resp=array();
                $consulta=$this->_cnn->query($query);
                if(!$consulta){
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
	        return $resp;
	}
        

}

