<?php

require_once 'modelos/base.php';
require_once 'modelos/MediosEnterar.php';

class DaoMediosEnterar extends base {

    public function add(MediosEnterar $x) {
        $query = sprintf("INSERT INTO medios_ulm (Medio,Id_plantel,Activo,FechaIni,FechaFin) VALUES (%s ,%s, %s,%s, %s)", 
        $this->GetSQLValueString($x->getMedio(), "text"), 
        $this->GetSQLValueString($x->getId_plantel(), "int"), 
        $this->GetSQLValueString($x->getActivo(), "int"),
        $this->GetSQLValueString($x->getFechaIni(), "date"),
        $this->GetSQLValueString($x->getFechaFin(), "date"));
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function update(MediosEnterar $x) {
        $query = sprintf("UPDATE medios_ulm SET Medio=%s,Id_plantel=%s,Activo=%s,FechaIni=%s,FechaFin=%s  WHERE Id_medio = %s", 
        $this->GetSQLValueString($x->getMedio(), "text"),
        $this->GetSQLValueString($x->getId_plantel(), "int"), 
        $this->GetSQLValueString($x->getActivo(), "int"), 
        $this->GetSQLValueString($x->getFechaIni(), "date"),
        $this->GetSQLValueString($x->getFechaFin(), "date"),
        $this->GetSQLValueString($x->getId(), "int"));
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id) {
        $query = sprintf("DELETE FROM medios_ulm WHERE Id_medio=" . $Id);
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return true;
        }
    }

    public function show($Id) {
        $query = "SELECT * FROM medios_ulm WHERE Id_medio= " . $Id;
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->create_object($Result1->fetch_assoc());
        }
    }

    public function showAll() {
        $resp = array();
        $query = "SELECT * FROM medios_ulm 
                WHERE Activo=1 
                      AND Id_plantel=" . $this->Id_plantel . "  
                      AND ((FechaIni IS NULL AND FechaFin IS NULL) OR ('".date('Y-m-d')."' >=FechaIni AND '".date('Y-m-d')."' <=FechaFin))
                      ORDER BY Medio ASC";
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
        }
        $totalRows_consulta = $consulta->num_rows;
        if ($totalRows_consulta > 0) {
            do {
                array_push($resp, $this->create_object($row_consulta));
            } while ($row_consulta = $consulta->fetch_assoc());
        }
        return $resp;
    }

    public function create_object($row) {
        $x = new MediosEnterar();
        $x->setId($row['Id_medio']);
        $x->setMedio($row['Medio']);
        $x->setId_plantel($row['Id_plantel']);
        $x->setActivo($row['Activo']);
        $x->setFechaIni($row['FechaIni']);
        $x->setFechaFin($row['FechaFin']);
        return $x;
    }

    public function advanced_query($query) {
        $resp = array();
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                do {
                    array_push($resp, $this->create_object($row_consulta));
                } while ($row_consulta = $consulta->fetch_assoc());
            }
        }
        return $resp;
    }

    public function getMedios(){
        $query = "SELECT * FROM medios_ulm WHERE Activo=1 AND Id_plantel=" . $this->Id_plantel . " ORDER BY Medio ASC";
        return $this->advanced_query($query);
    }
}
