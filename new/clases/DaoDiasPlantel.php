<?php
require_once 'modelos/base.php';
require_once 'modelos/DiasPlantel.php';

class DaoDiasPlantel extends base{
    
        public $table="DiasPlantel";
     
	public function add(DiasPlantel $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_dia, Id_plantel) VALUES (%s, %s)",
            $this->GetSQLValueString($x->getId_dia(), "int"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(DiasPlantel $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Id_dia=%s, Id_plantel=%s WHERE Id_dia_plantel=%s",
            $this->GetSQLValueString($x->getId_dia(), "int"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	public function delete($Id_dia){
            $query = sprintf("DELETE FROM ".$this->table." WHERE Id_dia=".$Id_dia); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function deleteDiasPlantel($Id_plantel){
            $query = sprintf("DELETE FROM ".$this->table." WHERE Id_plantel=".$Id_plantel); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_dia_plantel= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	public function create_object($row){
            $x = new DiasPlantel();
            $x->setId($row['Id_dia_plantel']);
            $x->setId_dia($row['Id_dia']);
            $x->setId_plantel($row['Id_plantel']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getDiasPlantel($Id_plantel) {
            $query= "SELECT * FROM ".$this->table." 
                    JOIN Dias ON ".$this->table.".Id_dia=Dias.Id
                WHERE Id_plantel=" . $Id_plantel . " ORDER BY Dias.Id ASC";
            return $this->advanced_query($query);
        }
       public function getJoinDiasPlantel($Id_plantel) {
            $resp=array();
            $query= "SELECT * FROM ".$this->table." 
                        JOIN Dias ON ".$this->table.".Id_dia=Dias.Id
                    WHERE Id_plantel=" . $Id_plantel . " ORDER BY Dias.Id ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $row_consulta);
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
       }

}

