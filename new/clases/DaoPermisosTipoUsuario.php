<?php
require_once 'modelos/base.php';
require_once 'modelos/PermisosTipoUsuario.php';

class DaoPermisosTipoUsuario extends base{
        public $table="permisos_tipo_usu";
        
	public function add(PermisosTipoUsuario $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_perm, Id_tipo_usu) VALUES (%s, %s)",
            $this->GetSQLValueString($x->getId_perm(), "int"), 
            $this->GetSQLValueString($x->getId_tipo_usu(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(PermisosTipoUsuario $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Id_perm=%s, Id_tipo_usu=%s WHERE Id_perm_tipoUsu=%s",
            $this->GetSQLValueString($x->getId_perm(), "int"), 
            $this->GetSQLValueString($x->getId_tipo_usu(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM ".$this->table."  WHERE Id_perm_tipoUsu =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
       public function deletePermisosTipoUsuario($Id_tipo){
            $query = sprintf("DELETE FROM ".$this->table."  WHERE Id_tipo_usu =".$Id_tipo); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_perm_tipoUsu= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new PermisosTipoUsuario();
            $x->setId($row['Id_perm_tipoUsu']);
            $x->setId_perm($row['Id_perm']);
            $x->setId_tipo_usu($row['Id_tipo_usu']);
            return $x;
	}
        
	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


        public function getPermisosTipoUsuario($Id_tipo){
             $query= "SELECT * FROM ".$this->table." WHERE  Id_tipo_usu=".$Id_tipo;
             return $this->advanced_query($query);
        }
}
