<?php
require_once 'modelos/base.php';
require_once 'modelos/EncuestasAlumno.php';

class DaoEncuestasAlumno extends base{
    
        public $table="EncuentasAlumno";
    
	public function add(EncuestasAlumno $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_alum, Id_enc, Id_docen, DateCreated) VALUES (%s, %s,%s, %s)",
            $this->GetSQLValueString($x->getId_alum(), "int"), 
            $this->GetSQLValueString($x->getId_enc(), "int"), 
            $this->GetSQLValueString($x->getId_docen(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(EncuestasAlumno $x){
	    $query=sprintf("UPDATE ".$this->table." SET Id_alum=%s, Id_enc=%s, Id_docen=%s, DateCreated=%s WHERE Id=%s",
            $this->GetSQLValueString($x->getId_alum(), "int"), 
            $this->GetSQLValueString($x->getId_enc(), "int"), 
            $this->GetSQLValueString($x->getId_docen(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE ".$this->table." SET Activo=0 WHERE Id =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	public function create_object($row){
            $x = new EncuestasAlumno();
            $x->setId($row['Id']);
            $x->setId_alum($row['Id_alum']);
            $x->setId_enc($row['Id_enc']);
            $x->setId_docen($row['Id_docen']);
            $x->setDateCreated($row['DateCreated']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        
        public function getEncuestasAlumno($Id_alum){
            $query="SELECT *FROM ".$this->table." WHERE Id_alum=".$Id_alum;
            return $this->advanced_query($query);
        }


}
