<?php
require_once 'modelos/base.php';
require_once 'modelos/PagosAdelantados.php';


class DaoPagosAdelantados extends base{
    
	public function add(PagosAdelantados $x){
	    $query=sprintf("INSERT INTO Pagos_adelantados (Fecha_pago, Monto_pago, Saldo, Id_usu, Id_ofe_alum, Id_alum, Ids_pagos, Id_met_pago, Id_ciclo_usar, Id_paq_des, DateCreated, Id_plantel, Id_pp) VALUES (%s, %s,%s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
            $this->GetSQLValueString($x->getFecha_pago(), "date"), 
            $this->GetSQLValueString($x->getMonto_pago(), "double"), 
            $this->GetSQLValueString($x->getSaldo(), "double"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getId_ofe_alum(), "int"),
            $this->GetSQLValueString($x->getId_alum(), "int"),
            $this->GetSQLValueString($x->getIds_pagos(), "text"),
            $this->GetSQLValueString($x->getId_met_pago(), "int"),
            $this->GetSQLValueString($x->getId_ciclo_usar(), "int"),
            $this->GetSQLValueString($x->getId_paq_des(), "int"),
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId_pp(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(PagosAdelantados $x){
	    $query=sprintf("UPDATE Pagos_adelantados SET  Fecha_pago=%s, Monto_pago=%s, Saldo=%s, 
             Id_usu=%s, Id_ofe_alum=%s, Id_alum=%s, Ids_pagos=%s, Id_met_pago=%s, Id_ciclo_usar=%s, Id_paq_des=%s, DateCreated=%s, Id_plantel=%s, Id_pp=%s
             WHERE Id_pad=%s",
            $this->GetSQLValueString($x->getFecha_pago(), "date"), 
            $this->GetSQLValueString($x->getMonto_pago(), "double"), 
            $this->GetSQLValueString($x->getSaldo(), "double"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getId_ofe_alum(), "int"),
            $this->GetSQLValueString($x->getId_alum(), "int"),
            $this->GetSQLValueString($x->getIds_pagos(), "text"),
            $this->GetSQLValueString($x->getId_met_pago(), "int"),
            $this->GetSQLValueString($x->getId_ciclo_usar(), "int"),
            $this->GetSQLValueString($x->getId_paq_des(), "int"),
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId_pp(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM Pagos_adelantados  WHERE Id_pad =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Pagos_adelantados WHERE Id_pad= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Pagos_adelantados WHERE Id_plantel=".$this->Id_plantel." ORDER BY Id_pad DESC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new PagosAdelantados();
            $x->setId($row['Id_pad']);
            $x->setFecha_pago($row['Fecha_pago']);
            $x->setMonto_pago($row['Monto_pago']);
            $x->setSaldo($row['Saldo']);
            $x->setId_usu($row['Id_usu']);
            $x->setId_ofe_alum($row['Id_ofe_alum']);
            $x->setId_alum($row['Id_alum']);
            $x->setIds_pagos($row['Ids_pagos']);
            $x->setId_met_pago($row['Id_met_pago']);
            $x->setId_ciclo_usar($row['Id_ciclo_usar']);
            $x->setId_paq_des($row['Id_paq_des']);
            $x->setDateCreated($row['DateCreated']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setId_pp($row['Id_pp']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        
        public function getPagosByAlum($Id_alum){
             $resp=array();
             $x="SELECT * FROM Pagos_adelantados WHERE Id_alum=".$Id_alum." ORDER BY Id_pad ASC";
             foreach($this->advanced_query($x) as $k=>$v){
                 array_push($resp, $this->show($v->getId()));
             }
             return $resp;
         }
         
         public function getPagosByOfertaAlumno($Id_ofe_alum){
             $resp=array();
             $query = "SELECT * FROM Pagos_adelantados WHERE Id_ofe_alum=" .$Id_ofe_alum." ORDER BY Id_pad ASC";
             foreach($this->advanced_query($query) as $k=>$v){
                 array_push($resp, $this->show($v->getId()));
             }
             return $resp;
         }
         
         public function deletePagosByOfertaAlumno($Id_ofe_alum){
            $query = sprintf("DELETE FROM Pagos_adelantados  WHERE Id_ofe_alum =".$Id_ofe_alum); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
         
         
         
         
        


}
