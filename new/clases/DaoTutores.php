<?php
require_once 'modelos/base.php';
require_once 'modelos/Tutores.php';

class DaoTutores extends base{
    
        public $table="Tutores";
    
	public function add(Tutores $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Nombre,ApellidoP,ApellidoM,Parentesco,Id_ins,Tel, Cel, Email) VALUES (%s, %s,%s,%s,%s,%s,%s,%s)",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getApellidoP(), "text"), 
            $this->GetSQLValueString($x->getApellidoM(), "text"), 
            $this->GetSQLValueString($x->getParentesco(), "text"), 
            $this->GetSQLValueString($x->getId_ins(), "int"), 
            $this->GetSQLValueString($x->getTel(), "text"), 
            $this->GetSQLValueString($x->getCel(), "text"),
            $this->GetSQLValueString($x->getEmail(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Tutores $x){
	    $query=sprintf("UPDATE ".$this->table." SET Nombre=%s,ApellidoP=%s,ApellidoM=%s,Parentesco=%s,Id_ins=%s,Tel=%s, Cel=%s, Email=%s WHERE Id_tu=%s",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getApellidoP(), "text"), 
            $this->GetSQLValueString($x->getApellidoM(), "text"), 
            $this->GetSQLValueString($x->getParentesco(), "text"), 
            $this->GetSQLValueString($x->getId_ins(), "int"), 
            $this->GetSQLValueString($x->getTel(), "text"), 
            $this->GetSQLValueString($x->getCel(), "text"),
            $this->GetSQLValueString($x->getEmail(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	
	public function delete($Id){
            $query = sprintf("DELETE FROM ".$this->table." WHERE Id_tu =%s",
            $this->GetSQLValueString($Id, "int")); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function deletePorAlumno($Id){
            $query = sprintf("DELETE FROM ".$this->table." WHERE Id_ins =%s",
            $this->GetSQLValueString($Id, "int")); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	public function show($Id){
	    $query=sprintf("SELECT * FROM ".$this->table." WHERE Id_tu=%s",
            $this->GetSQLValueString($Id, "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function getTutorAlumno($Id,$Nombre,$Tel,$Email){
	    $query=sprintf("SELECT * FROM ".$this->table." WHERE Id_ins=%s AND Nombre=%s AND Tel=%s AND Email=%s",
            $this->GetSQLValueString($Id, "int"),
            $this->GetSQLValueString($Nombre, "text"),
            $this->GetSQLValueString($Tel, "text"),
            $this->GetSQLValueString($Email, "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new Tutores();
            $x->setId($row['Id_tu']);
            $x->setNombre($row['Nombre']);
            $x->setApellidoP($row['ApellidoP']);
            $x->setApellidoM($row['ApellidoM']);
            $x->setParentesco($row['Parentesco']);
            $x->setId_ins($row['Id_ins']);
            $x->setTel($row['Tel']);
            $x->setCel($row['Cel']);
            $x->setEmail($row['Email']);
            return $x;
	}


                
	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                   do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        
        public function getTutoresAlumno($Id_alumno){
            $query=sprintf("SELECT * FROM ".$this->table." WHERE Id_ins=%s",
            $this->GetSQLValueString($Id_alumno, "int"));
            return $this->advanced_query($query);
        }
       
}
