<?php
require_once 'modelos/base.php';
require_once 'modelos/MetodosPago.php';

class DaoMetodosPago extends base{

	public function add(MetodosPago $x){
              $query=sprintf("INSERT INTO Metodo_pago (Nombre,Id_plantel,Activo) VALUES (%s ,%s, %s)",
              $this->GetSQLValueString($x->getNombre(), "text"),
              $this->GetSQLValueString($x->getId_plantel(), "int"),
              $this->GetSQLValueString($x->getActivo(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(MetodosPago $x){
	    $query=sprintf("UPDATE Metodo_pago SET Nombre=%s, Id_plantel=%s, Activo=%s WHERE Id_met = %s",
              $this->GetSQLValueString($x->getNombre(), "text"),
              $this->GetSQLValueString($x->getId_plantel(), "int"),
              $this->GetSQLValueString($x->getActivo(), "int"),
              $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}
	
	
	
	public function delete($Id){
                $query = sprintf("DELETE FROM Metodo_pago WHERE Id_recargo=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Metodo_pago WHERE Id_met= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Metodo_pago WHERE Activo=1 AND Id_plantel=".$this->Id_plantel;
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
	
	
	
	public function create_object($row){
            $x = new MetodosPago();
            $x->setId($row['Id_met']);
            $x->setNombre($row['Nombre']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setActivo($row['Activo']); 
            return $x;
	}

	public function advanced_query($query){
		$resp=array();
		$consulta=$this->_cnn->query($query);
		    if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	}

}

