<?php
require_once 'modelos/base.php';
require_once 'modelos/HorarioDocente.php';

class DaoHorarioDocente extends base{
    
	public function add(HorarioDocente $x){
	    $query=sprintf("INSERT INTO Horario_docente (Id_grupo, Hora, Aula, Lunes, Martes, Miercoles, Jueves,Viernes,  Sabado, Id_ciclo, Id_docente,Domingo) VALUES (%s, %s,%s, %s,%s, %s, %s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getId_grupo(), "int"), 
            $this->GetSQLValueString($x->getHora(), "int"), 
            $this->GetSQLValueString($x->getAula(), "int"), 
            $this->GetSQLValueString($x->getLunes(), "int"), 
            $this->GetSQLValueString($x->getMartes(), "int"), 
            $this->GetSQLValueString($x->getMiercoles(), "int"),
            $this->GetSQLValueString($x->getJueves(), "int"),
            $this->GetSQLValueString($x->getViernes(), "int"),
            $this->GetSQLValueString($x->getSabado(), "int"),
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getId_docente(), "int"),
            $this->GetSQLValueString($x->getDomingo(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(HorarioDocente $x){
	    $query=sprintf("UPDATE Horario_docente SET  Id_grupo=%s, Hora=%s, Aula=%s, Lunes=%s, Martes=%s, Miercoles=%s, Jueves=%s,Viernes=%s,  Sabado=%s, Id_ciclo=%s, Id_docente=%s,Domingo=%s WHERE Idhor_doc=%s",
            $this->GetSQLValueString($x->getId_grupo(), "int"), 
            $this->GetSQLValueString($x->getHora(), "int"), 
            $this->GetSQLValueString($x->getAula(), "int"), 
            $this->GetSQLValueString($x->getLunes(), "int"), 
            $this->GetSQLValueString($x->getMartes(), "int"), 
            $this->GetSQLValueString($x->getMiercoles(), "int"),
            $this->GetSQLValueString($x->getJueves(), "int"),
            $this->GetSQLValueString($x->getViernes(), "int"),
            $this->GetSQLValueString($x->getSabado(), "int"),
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getId_docente(), "int"),
            $this->GetSQLValueString($x->getDomingo(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE Horario_docente WHERE Idhor_doc =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}

        public function deleteGrupoDocente($Id_grupo,$Id_aula,$Id_ciclo,$Id_docente){
            $query = sprintf("DELETE FROM Horario_docente WHERE Id_grupo=$Id_grupo AND Aula=$Id_aula AND Id_ciclo=$Id_ciclo AND Id_docente=".$Id_docente); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}

            
	public function show($Id){
	    $query="SELECT * FROM Horario_docente WHERE Idhor_doc= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Horario_docente";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new HorarioDocente();
            $x->setId($row['Idhor_doc']);
            $x->setId_grupo($row['Id_grupo']);
            $x->setHora($row['Hora']);
            $x->setAula($row['Aula']);
            $x->setLunes($row['Lunes']);
            $x->setMartes($row['Martes']);
            $x->setMiercoles($row['Miercoles']);
            $x->setJueves($row['Jueves']);
            $x->setViernes($row['Viernes']);
            $x->setSabado($row['Sabado']);
            $x->setDomingo($row['Domingo']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setId_docente($row['Id_docente']);
            $x->setDomingo($row['Domingo']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getHorarioByGrupoCiclo($Id_grupo,$Id_ciclo){
	    $query="SELECT * FROM Horario_docente WHERE Id_grupo= ".$Id_grupo." AND Id_ciclo=".$Id_ciclo;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function getHorarioQuery($query){
                $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        
        public function getGruposDocenteCiclo($Id_docen,$Id_ciclo){
            $query="SELECT * FROM Horario_docente WHERE Id_docente= ".$Id_docen." AND Id_ciclo=".$Id_ciclo." GROUP BY Id_grupo";
            return $this->advanced_query($query);
        }
        
}


