<?php
require_once 'modelos/base.php';
require_once 'modelos/Usuarios.php';
require_once 'DaoHistorialUsuario.php';
require_once 'modelos/HistorialUsuario.php';

class DaoUsuarios extends base{

	public function add(Usuarios $x){
	  $query=sprintf("INSERT INTO usuarios_ulm (Nombre_usu, ApellidoP_usu, ApellidoM_usu, Clave_usu, Email_usu, Tel_usu, Cel_usu, 
                    Id_plantel,Nivel_estudios,Tipo_usu,Alta_usu,Tipo_sangre, Alergias,Enfermedades_cronicas, Preinscripciones_medicas) 
                    VALUES (%s, %s,%s, %s,%s, %s,%s, %s,%s,%s,%s, %s,%s,%s,%s)",
            $this->GetSQLValueString($x->getNombre_usu(), "text"),
            $this->GetSQLValueString($x->getApellidoP_usu(), "text"),
            $this->GetSQLValueString($x->getApellidoM_usu(), "text"),
            $this->GetSQLValueString($x->getClave_usu(), "text"),
            $this->GetSQLValueString($x->getEmail_usu(), "text"),
            $this->GetSQLValueString($x->getTel_usu(), "text"),
            $this->GetSQLValueString($x->getCel_usu(), "text"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getNivel_estudios(), "int"),
            $this->GetSQLValueString($x->getTipo_usu(), "int"),
            $this->GetSQLValueString($x->getAlta_usu(), "date"),
            $this->GetSQLValueString($x->getTipo_sangre(), "text"), 
            $this->GetSQLValueString($x->getAlergias(), "text"), 
            $this->GetSQLValueString($x->getEnfermedades_cronicas(), "text"), 
            $this->GetSQLValueString($x->getPreinscripciones_medicas(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Usuarios $x){
	    $query=sprintf("UPDATE usuarios_ulm SET Nombre_usu=%s, ApellidoP_usu=%s, ApellidoM_usu=%s, Clave_usu=%s, Email_usu=%s, 
                Tel_usu=%s, Cel_usu=%s, Id_plantel=%s,Nivel_estudios=%s,Tipo_usu=%s, 
                Tipo_sangre=%s, Alergias=%s,Enfermedades_cronicas=%s, Preinscripciones_medicas=%s,Baja_usu=%s,Img_usu=%s, Doc_RFC=%s, Doc_CURP=%s, Doc_IMSS=%s, Copy_CURP=%s,Copy_IMSS=%s,  Copy_RFC=%s, Pass_usu=%s, Recover_usu=%s   WHERE Id_usu=%s",
                $this->GetSQLValueString($x->getNombre_usu(), "text"),
                $this->GetSQLValueString($x->getApellidoP_usu(), "text"),
                $this->GetSQLValueString($x->getApellidoM_usu(), "text"),
                $this->GetSQLValueString($x->getClave_usu(), "text"),
                $this->GetSQLValueString($x->getEmail_usu(), "text"),
                $this->GetSQLValueString($x->getTel_usu(), "text"),
                $this->GetSQLValueString($x->getCel_usu(), "text"),
                $this->GetSQLValueString($x->getId_plantel(), "int"),
                $this->GetSQLValueString($x->getNivel_estudios(), "int"),
                $this->GetSQLValueString($x->getTipo_usu(), "int"),
                $this->GetSQLValueString($x->getTipo_sangre(), "text"), 
                $this->GetSQLValueString($x->getAlergias(), "text"), 
                $this->GetSQLValueString($x->getEnfermedades_cronicas(), "text"), 
                $this->GetSQLValueString($x->getPreinscripciones_medicas(), "text"),
                $this->GetSQLValueString($x->getBaja_usu(), "date"),
                $this->GetSQLValueString($x->getImg_usu(), "text"),
                $this->GetSQLValueString($x->getDoc_RFC(), "int"),
                $this->GetSQLValueString($x->getDoc_CURP(), "int"),
                $this->GetSQLValueString($x->getDoc_IMSS(), "int"),
                $this->GetSQLValueString($x->getCopy_CURP(), "int"),
                $this->GetSQLValueString($x->getCopy_IMSS(), "int"),
                $this->GetSQLValueString($x->getCopy_RFC(), "int"),
                $this->GetSQLValueString($x->getPass_usu(), "text"),
                $this->GetSQLValueString($x->getRecover_usu(), "text"),
                $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}
            
	
        
	public function delete($Id){
            $query = sprintf("UPDATE usuarios_ulm SET Baja_usu=".  date("Y-m-d H:i.s")." WHERE Id_usu =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	
	public function show($Id){
	    $query="SELECT * FROM usuarios_ulm WHERE Id_usu= ".$Id;
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM usuarios_ulm WHERE Baja_usu IS NULL AND Id_plantel=".$this->Id_plantel;
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new Usuarios();
            $x->setId($row['Id_usu']);
            $x->setNombre_usu($row['Nombre_usu']);
            $x->setApellidoP_usu($row['ApellidoP_usu']);
            $x->setApellidoM_usu($row['ApellidoM_usu']);
            $x->setTipo_usu($row['Tipo_usu']);
            $x->setAlta_usu($row['Alta_usu']);
            $x->setBaja_usu($row['Baja_usu']);
            $x->setId_googleUi($row['Id_googleUi']);
            $x->getId_faceUI($row['Id_faceUI']);
            $x->setEmail_usu($row['Email_usu']);
            $x->setLast_session($row['Last_session_usu']);
            $x->setPass_usu($row['Pass_usu']);
            $x->setRecover_usu($row['Recover_usu']);
            $x->setImg_usu($row['Img_usu']);
            $x->setTel_usu($row['Tel_usu']);
            $x->setClave_usu($row['Clave_usu']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setCel_usu($row['Cel_usu']);
            $x->setDoc_RFC($row['Doc_RFC']);
            $x->setDoc_CURP($row['Doc_CURP']);
            $x->setDoc_IMSS($row['Doc_IMSS']);
            $x->setCopy_CURP($row['Copy_CURP']);
            $x->setCopy_IMSS($row['Copy_IMSS']);
            $x->setCopy_RFC($row['Copy_RFC']);
            $x->setTipo_sangre($row['Tipo_sangre']);
            $x->setAlergias($row['Alergias']);
            $x->setEnfermedades_cronicas($row['Enfermedades_cronicas']);
            $x->setPreinscripciones_medicas($row['Preinscripciones_medicas']);
            $x->setNivel_estudios($row['Nivel_estudios']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getPermisosUsuario($Id_usu){
            $resp=array();
            $query = "SELECT * FROM permisos_extra_usu WHERE Id_usu=".$Id_usu;
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $row_consulta);
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}

        
        public function buscarUsuario($buscar,$limit=20){
            $resp=array();
            $query = "
            SELECT *
            FROM usuarios_ulm 
            WHERE  (Nombre_usu LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR ApellidoP_usu LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR ApellidoM_usu LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR Email_usu LIKE '%".str_replace(' ','',$buscar)."%')
                 AND Baja_usu IS NULL AND Id_plantel=".$this->Id_plantel." LIMIT ".$limit;
                $consulta=$this->_cnn->query($query);
                if(!$consulta){
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                    $row_consulta= $consulta->fetch_assoc();
                    $totalRows_consulta= $consulta->num_rows;
                    if($totalRows_consulta>0){
                           do{
                              array_push($resp, $this->create_object($row_consulta));
                       }while($row_consulta= $consulta->fetch_assoc());  
                    }
                }
                return $resp;
    }
    
    public function capturarHistorialUsuario($texto,$categoria){
            $DaoHistorialUsuario= new DaoHistorialUsuario();
            $HistorialUsuario= new HistorialUsuario();
            
            $HistorialUsuario->setCategoria($categoria);
            $HistorialUsuario->setDateCreated(date('Y-m-d H:i:s'));
            $HistorialUsuario->setId_usu($_COOKIE['admin/Id_usu']);
            $HistorialUsuario->setId_plantel($this->Id_plantel);
            $HistorialUsuario->setTexto($texto);
            $DaoHistorialUsuario->add($HistorialUsuario); 
    }
    
    public function validarPassTipoAdmin($Pass){
	    $query="SELECT * FROM usuarios_ulm WHERE Pass_usu= '".$this->hashPassword($Pass)."' AND Baja_usu IS NULL AND Tipo_usu=1";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
    }
    
    
    public function getUsuariosTipo($tipo,$Id_plantel) {
        
        if($Id_plantel!=null){
            $Id_p=$Id_plantel;
        }else{
           $usu = $this->show($_COOKIE['admin/Id_usu']); 
           $Id_p=$usu->getId_plantel();
        }
        $query = "SELECT * FROM usuarios_ulm WHERE Tipo_usu=" .$tipo." AND Baja_usu IS NULL AND Id_plantel=".$Id_p;
        return $this->advanced_query($query);

    }
    
    
    	public function existQuery($query){
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function getRecoverUsu($recover){
	    $query="SELECT * FROM usuarios_ulm WHERE Recover_usu='".$recover."'";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

}
