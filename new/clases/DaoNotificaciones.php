<?php
require_once 'modelos/base.php';
require_once 'modelos/Notificaciones.php';

class DaoNotificaciones extends base{

	public function add(Notificaciones $x){
              $query=sprintf("INSERT INTO Notificaciones (Titulo, Texto, DateCreated, DateInit,Id_usu, Id_plantel, Tipo, Tipos_usu, Activo) VALUES (%s ,%s, %s, %s, %s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getTitulo(), "text"),
              $this->GetSQLValueString($x->getTexto(), "text"),
              $this->GetSQLValueString($x->getDateCreated(), "date"),
              $this->GetSQLValueString($x->getDateInit(), "date"),
              $this->GetSQLValueString($x->getId_usu(), "int"),
              $this->GetSQLValueString($x->getId_plantel(), "int"),
              $this->GetSQLValueString($x->getTipo(), "text"),
              $this->GetSQLValueString($x->getTipos_usu(), "text"),
              $this->GetSQLValueString($x->getActivo(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(Notificaciones $x){
	  $query=sprintf("UPDATE Notificaciones SET Titulo=%s, Texto=%s, DateCreated=%s, DateInit=%s,Id_usu=%s, Id_plantel=%s, Tipo=%s, Tipos_usu=%s, Activo=%s  WHERE Id_not = %s",
          $this->GetSQLValueString($x->getTitulo(), "text"),
          $this->GetSQLValueString($x->getTexto(), "text"),
          $this->GetSQLValueString($x->getDateCreated(), "date"),
          $this->GetSQLValueString($x->getDateInit(), "date"),
          $this->GetSQLValueString($x->getId_usu(), "int"),
          $this->GetSQLValueString($x->getId_plantel(), "int"),
          $this->GetSQLValueString($x->getTipo(), "text"),
          $this->GetSQLValueString($x->getTipos_usu(), "text"),
          $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}

	public function delete($Id){
                $query = sprintf("DELETE FROM Notificaciones WHERE Id_not=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Notificaciones WHERE Id_not= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new Notificaciones();
            $x->setId($row['Id_not']);
            $x->setTitulo($row['Titulo']);
            $x->setTexto($row['Texto']);
            $x->setDateCreated($row['DateCreated']);
            $x->setDateInit($row['DateInit']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setTipo($row['Tipo']);
            $x->setTipos_usu($row['Tipos_usu']);
            $x->setActivo($row['Activo']);
            $x->setId_usu($row['Id_usu']);
            return $x;
	}
        

         public function advanced_query($query){
		    $resp=array();
		    $consulta=$this->_cnn->query($query);
		    if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	}
        
        public function getNotificaciones() {
            $query= "SELECT * FROM Notificaciones WHERE Id_plantel=".$this->Id_plantel." AND Activo=1 ORDER BY Id_not DESC";
            return $this->advanced_query($query);
       }
       
      

}

