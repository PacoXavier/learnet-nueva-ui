<?php
require_once 'modelos/base.php';
require_once 'modelos/Especialidades.php';

class DaoEspecialidades extends base{
    
	public function add(Especialidades $x){
	    $query=sprintf("INSERT INTO especialidades_ulm (Id_ofe, Nombre_esp, Activo_esp, RegistroValidez_Oficial,
                    Institucion_certifica, Modalidad, Tipo_ciclos, 
                    DuracionSemanas, ClavePlan_estudios, Escala_calificacion, Minima_aprobatotia, Limite_faltas, Precio_curso,
                    Inscripcion_curso, Tipo_insc, Num_pagos, Num_ciclos) VALUES (%s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getId_ofe(), "int"), 
            $this->GetSQLValueString($x->getNombre_esp(), "text"), 
            $this->GetSQLValueString($x->getActivo_esp(), "int"), 
            $this->GetSQLValueString($x->getRegistroValidez_Oficial(), "text"),
            $this->GetSQLValueString($x->getInstitucion_certifica(), "text"),
            $this->GetSQLValueString($x->getModalidad(), "int"),
            $this->GetSQLValueString($x->getTipo_ciclos(), "int"),
            $this->GetSQLValueString($x->getDuracionSemanas(), "text"),
            $this->GetSQLValueString($x->getClavePlan_estudios(), "text"),
            $this->GetSQLValueString($x->getEscala_calificacion(), "int"),
            $this->GetSQLValueString($x->getMinima_aprobatotia(), "double"),
            $this->GetSQLValueString($x->getLimite_faltas(), "text"),
            $this->GetSQLValueString($x->getPrecio_curso(), "double"),
            $this->GetSQLValueString($x->getInscripcion_curso(), "double"),
            $this->GetSQLValueString($x->getTipo_insc(), "int"),
            $this->GetSQLValueString($x->getNum_pagos(), "int"),
            $this->GetSQLValueString($x->getNum_ciclos(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Especialidades $x){
	    $query=sprintf("UPDATE especialidades_ulm SET  Id_ofe=%s, Nombre_esp=%s, Activo_esp=%s, RegistroValidez_Oficial=%s,
                    Institucion_certifica=%s, Modalidad=%s, Tipo_ciclos=%s, 
                    DuracionSemanas=%s, ClavePlan_estudios=%s, Escala_calificacion=%s, Minima_aprobatotia=%s, Limite_faltas=%s, Precio_curso=%s,
                    Inscripcion_curso=%s, Tipo_insc=%s, Num_pagos=%s, Num_ciclos=%s WHERE Id_esp=%s",
            $this->GetSQLValueString($x->getId_ofe(), "int"), 
            $this->GetSQLValueString($x->getNombre_esp(), "text"), 
            $this->GetSQLValueString($x->getActivo_esp(), "int"), 
            $this->GetSQLValueString($x->getRegistroValidez_Oficial(), "text"),
            $this->GetSQLValueString($x->getInstitucion_certifica(), "text"),
            $this->GetSQLValueString($x->getModalidad(), "int"),
            $this->GetSQLValueString($x->getTipo_ciclos(), "int"),
            $this->GetSQLValueString($x->getDuracionSemanas(), "text"),
            $this->GetSQLValueString($x->getClavePlan_estudios(), "text"),
            $this->GetSQLValueString($x->getEscala_calificacion(), "int"),
            $this->GetSQLValueString($x->getMinima_aprobatotia(), "double"),
            $this->GetSQLValueString($x->getLimite_faltas(), "text"),
            $this->GetSQLValueString($x->getPrecio_curso(), "double"),
            $this->GetSQLValueString($x->getInscripcion_curso(), "double"),
            $this->GetSQLValueString($x->getTipo_insc(), "int"),
            $this->GetSQLValueString($x->getNum_pagos(), "int"),
            $this->GetSQLValueString($x->getNum_ciclos(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE especialidades_ulm SET Activo_esp=0 WHERE Id_esp =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM especialidades_ulm WHERE Id_esp= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM especialidades_ulm WHERE Activo_esp=1 ORDER BY Nombre_esp ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new Especialidades();
            $x->setId($row['Id_esp']);
            $x->setId_ofe($row['Id_ofe']);
            $x->setNombre_esp($row['Nombre_esp']);
            $x->setActivo_esp($row['Activo_esp']);
            $x->setRegistroValidez_Oficial($row['RegistroValidez_Oficial']);
            $x->setInstitucion_certifica($row['Institucion_certifica']);
            $x->setModalidad($row['Modalidad']);
            $x->setTipo_ciclos($row['Tipo_ciclos']);
            $x->setDuracionSemanas($row['DuracionSemanas']);
            $x->setClavePlan_estudios($row['ClavePlan_estudios']);
            $x->setEscala_calificacion($row['Escala_calificacion']);
            $x->setMinima_aprobatotia($row['Minima_aprobatotia']);
            $x->setLimite_faltas($row['Limite_faltas']);
            $x->setPrecio_curso($row['Precio_curso']);
            $x->setInscripcion_curso($row['Inscripcion_curso']);
            $x->setTipo_insc($row['Tipo_insc']);
            $x->setNum_pagos($row['Num_pagos']);
            $x->setNum_ciclos($row['Num_ciclos']);
            return $x;
	}
        
        
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getEspecialidadesOferta($Id_ofe) {
            $query="SELECT * FROM especialidades_ulm WHERE Id_ofe= ".$Id_ofe." AND Activo_esp=1 ORDER BY Nombre_esp ASC";
            return $this->advanced_query($query);
        }

}



