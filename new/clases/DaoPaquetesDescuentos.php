<?php
require_once 'modelos/base.php';
require_once 'modelos/PaquetesDescuentos.php';

class DaoPaquetesDescuentos extends base{
	public function add(PaquetesDescuentos $x){
	    $query=sprintf("INSERT INTO Paquetes_descuentos (Fecha_inicio, Fecha_fin, Nombre, Activo_des, Porcentaje, Id_plantel) VALUES (%s, %s,%s, %s,%s, %s)",
            $this->GetSQLValueString($x->getFecha_inicio(), "date"), 
            $this->GetSQLValueString($x->getFecha_fin(), "date"), 
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getActivo_des(), "int"), 
            $this->GetSQLValueString($x->getPorcentaje(), "double"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(PaquetesDescuentos $x){
	    $query=sprintf("UPDATE Paquetes_descuentos SET  Fecha_inicio, Fecha_fin=%s, Nombre=%s, Activo_des=%s, Porcentaje=%s, Id_plantel=%s WHERE Id_des=%s",
            $this->GetSQLValueString($x->getFecha_inicio(), "date"), 
            $this->GetSQLValueString($x->getFecha_fin(), "date"), 
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getActivo_des(), "int"), 
            $this->GetSQLValueString($x->getPorcentaje(), "double"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE Paquetes_descuentos SET Activo_des=0 WHERE Id_des =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Paquetes_descuentos WHERE Id_des= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Paquetes_descuentos WHERE Activo_des=1 AND 
                     ((Fecha_inicio<='".date('Y-m-d')."' AND Fecha_fin>='".date('Y-m-d')."') || (Fecha_inicio IS NULL AND Fecha_fin IS NULL)) AND Id_plantel=".$this->Id_plantel." ORDER BY Nombre ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
	
	public function create_object($row){
            $x = new PaquetesDescuentos();
            $x->setId($row['Id_des']);
            $x->setFecha_inicio($row['Fecha_inicio']);
            $x->setFecha_fin($row['Fecha_fin']);
            $x->setNombre($row['Nombre']);
            $x->setActivo_des($row['Activo_des']);
            $x->setPorcentaje($row['Porcentaje']);
            $x->setId_plantel($row['Id_plantel']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


}
