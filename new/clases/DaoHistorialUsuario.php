<?php
require_once 'modelos/base.php';
require_once 'modelos/HistorialUsuario.php';
require_once 'DaoCiclos.php';


class DaoHistorialUsuario extends base{
	public function add(HistorialUsuario $x){
            $DaoCiclos= new DaoCiclos();
            $ciclo=$DaoCiclos->getActual();
	    $query=sprintf("INSERT INTO HistorialUsuario (Texto, DateCreated, Id_usu, Categoria, Id_plantel,Id_ciclo) VALUES (%s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getTexto(), "text"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getCategoria(), "text"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($ciclo->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

        
    
	public function update(HistorialUsuario $x){
            
	    $query=sprintf("UPDATE HistorialUsuario SET  Texto=%s, DateCreated=%s, Id_usu=%s, Categoria=%s, Id_plantel=%s,Id_ciclo=%s WHERE Id=%s",
            $this->GetSQLValueString($x->getTexto(), "text"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getCategoria(), "text"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM HistorialUsuario WHERE Id =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM HistorialUsuario WHERE Id= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $DaoCiclos= new DaoCiclos();
            $ciclo=$DaoCiclos->getActual();
            $resp=array();
            $query="SELECT * FROM HistorialUsuario WHERE Id_plantel=".$this->Id_plantel." AND Id_ciclo=".$ciclo->getId()." ORDER BY Id DESC LIMIT 200";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
        

	
	public function create_object($row){
            $x = new HistorialUsuario();
            $x->setId($row['Id']);
            $x->setTexto($row['Texto']);
            $x->setDateCreated($row['DateCreated']);
            $x->setId_usu($row['Id_usu']);
            $x->setCategoria($row['Categoria']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setId_ciclo($row['Id_ciclo']);
            return $x;
	}
        
        


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


}
