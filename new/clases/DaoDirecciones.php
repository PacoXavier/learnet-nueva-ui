<?php
require_once 'modelos/base.php';
require_once 'modelos/Direcciones.php';

class DaoDirecciones extends base{
    
	public function add(Direcciones $x){
	    $query=sprintf("INSERT INTO direcciones_uml (Nombre_dir, Calle_dir, NumExt_dir, NumInt_dir, Colonia_dir,Ciudad_dir,Estado_dir, Cp_dir, Pais_dir,TipoRel_dir, IdRel_dir, Tel_dir, Movil_dir  ) VALUES (%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s,%s)",
            $this->GetSQLValueString($x->getNombre_dir(), "text"), 
            $this->GetSQLValueString($x->getCalle_dir(), "text"), 
            $this->GetSQLValueString($x->getNumExt_dir(), "text"), 
            $this->GetSQLValueString($x->getNumInt_dir(), "text"), 
            $this->GetSQLValueString($x->getColonia_dir(), "text"),
            $this->GetSQLValueString($x->getCiudad_dir(), "text"),
            $this->GetSQLValueString($x->getEstado_dir(), "text"),
            $this->GetSQLValueString($x->getCp_dir(), "text"),
            $this->GetSQLValueString($x->getPais_dir(), "text"),
            $this->GetSQLValueString($x->getTipoRel_dir(), "text"),
            $this->GetSQLValueString($x->getIdRel_dir(), "int"),
            $this->GetSQLValueString($x->getTel_dir(), "text"),
            $this->GetSQLValueString($x->getMovil_dir(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

        
    
	public function update(Direcciones $x){
	    $query=sprintf("UPDATE direcciones_uml SET  Nombre_dir=%s, Calle_dir=%s, NumExt_dir=%s, NumInt_dir=%s, Colonia_dir=%s, Ciudad_dir=%s, Estado_dir=%s, Cp_dir=%s, Pais_dir=%s,TipoRel_dir=%s, IdRel_dir=%s, Tel_dir=%s, Movil_dir=%s WHERE Id_dir=%s",
            $this->GetSQLValueString($x->getNombre_dir(), "text"), 
            $this->GetSQLValueString($x->getCalle_dir(), "text"), 
            $this->GetSQLValueString($x->getNumExt_dir(), "text"), 
            $this->GetSQLValueString($x->getNumInt_dir(), "text"), 
            $this->GetSQLValueString($x->getColonia_dir(), "text"),
            $this->GetSQLValueString($x->getCiudad_dir(), "text"),
            $this->GetSQLValueString($x->getEstado_dir(), "text"),
            $this->GetSQLValueString($x->getCp_dir(), "text"),
            $this->GetSQLValueString($x->getPais_dir(), "text"),
            $this->GetSQLValueString($x->getTipoRel_dir(), "text"),
            $this->GetSQLValueString($x->getIdRel_dir(), "int"),
            $this->GetSQLValueString($x->getTel_dir(), "text"),
            $this->GetSQLValueString($x->getMovil_dir(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM direcciones_uml WHERE Id_dir =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM direcciones_uml WHERE Id_dir= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	public function getDireccionPorTipoRel($Id,$tipo){
	    $query="SELECT * FROM direcciones_uml WHERE IdRel_dir= ".$Id." AND TipoRel_dir='".$tipo."'";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
	public function create_object($row){
            $Id_dir="";
            $Nombre_dir="";
            $Calle_dir="";
            $NumExt_dir="";
            $NumInt_dir="";
            $Colonia_dir="";
            $Ciudad_dir="";
            $Estado_dir="";
            $Cp_dir="";
            $Pais_dir="";
            $TipoRel_dir="";
            $IdRel_dir="";
            $Tel_dir="";
            $Movil_dir="";
            if(isset($row['Id_dir'])){
              $Id_dir= $row['Id_dir']; 
            }
            if(isset($row['Nombre_dir'])){
              $Nombre_dir= $row['Nombre_dir']; 
            }
            if(isset($row['Calle_dir'])){
              $Calle_dir= $row['Calle_dir']; 
            }
            if(isset($row['NumExt_dir'])){
              $NumExt_dir= $row['NumExt_dir']; 
            }
            if(isset($row['NumInt_dir'])){
              $NumInt_dir= $row['NumInt_dir']; 
            }
            if(isset($row['Colonia_dir'])){
              $Colonia_dir= $row['Colonia_dir']; 
            }
            if(isset($row['Ciudad_dir'])){
              $Ciudad_dir= $row['Ciudad_dir']; 
            }
            if(isset($row['Estado_dir'])){
              $Estado_dir= $row['Estado_dir']; 
            }
            if(isset($row['Cp_dir'])){
              $Cp_dir= $row['Cp_dir']; 
            }
            if(isset($row['Pais_dir'])){
              $Pais_dir= $row['Pais_dir']; 
            }
            if(isset($row['TipoRel_dir'])){
              $TipoRel_dir= $row['TipoRel_dir']; 
            }
            if(isset($row['IdRel_dir'])){
              $IdRel_dir= $row['IdRel_dir']; 
            }
            if(isset($row['Tel_dir'])){
              $Tel_dir= $row['Tel_dir']; 
            }
            if(isset($row['Movil_dir'])){
              $Movil_dir= $row['Movil_dir']; 
            }
            $x = new Direcciones();
            $x->setId($Id_dir);
            $x->setNombre_dir($Nombre_dir);
            $x->setCalle_dir($Calle_dir);
            $x->setNumExt_dir($NumExt_dir);
            $x->setNumInt_dir($NumInt_dir);
            $x->setColonia_dir($Colonia_dir);
            $x->setCiudad_dir($Ciudad_dir);
            $x->setEstado_dir($Estado_dir);
            $x->setCp_dir($Cp_dir);
            $x->setPais_dir($Pais_dir);
            $x->setTipoRel_dir($TipoRel_dir);
            $x->setIdRel_dir($IdRel_dir);
            $x->setTel_dir($Tel_dir);
            $x->setMovil_dir($Movil_dir);

            return $x;
	}
        
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


}
