<?php
require_once 'modelos/base.php';
require_once 'modelos/GrupoExamen.php';

class DaoGrupoExamen extends base{
	public function add(GrupoExamen $x){
	    $query=sprintf("INSERT INTO GrupoExamen (IdGrupo, IdHorarioExamen) VALUES (%s, %s)",
            $this->GetSQLValueString($x->getIdGrupo(), "int"), 
            $this->GetSQLValueString($x->getIdHorarioExamen(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(GrupoExamen $x){
	    $query=sprintf("UPDATE GrupoExamen SET IdGrupo=%s, IdHorarioExamen=%s WHERE IdGrupoExamen=%s",
            $this->GetSQLValueString($x->getIdGrupo(), "int"), 
            $this->GetSQLValueString($x->getIdHorarioExamen(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM GrupoExamen WHERE IdGrupoExamen =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
       public function deleteGruposExamen($Id){
            $query = sprintf("DELETE FROM GrupoExamen WHERE IdHorarioExamen =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM GrupoExamen WHERE IdGrupoExamen= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
	
	public function create_object($row){
            $x = new GrupoExamen();
            $x->setId($row['IdGrupoExamen']);
            $x->setIdGrupo($row['IdGrupo']);
            $x->setIdHorarioExamen($row['IdHorarioExamen']);
            return $x;
	}
                
	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}

        public function getGruposExamenHorario($IdHorarioExamen) {
            $query= "SELECT * FROM GrupoExamen WHERE IdHorarioExamen=" . $IdHorarioExamen . " ORDER BY IdGrupoExamen ASC";
            return $this->advanced_query($query);
       }
       
       	public function getGrupoExamenHorario($Id){
	    $query="SELECT * FROM GrupoExamen WHERE IdHorarioExamen= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function getGrupoExamenByHorario($IdGrupo,$IdHorarioExamen){
	    $query="SELECT * FROM GrupoExamen WHERE IdGrupo= ".$IdGrupo." AND IdHorarioExamen=".$IdHorarioExamen;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
}
