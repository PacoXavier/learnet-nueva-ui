<?php
require_once 'modelos/base.php';
require_once 'modelos/Dependencias.php';

class DaoDependencias extends base{
	public function add(Dependencias $x){
	    $query=sprintf("INSERT INTO Dependencias (Id_tarea, Id_depe) VALUES (%s, %s)",
            $this->GetSQLValueString($x->getId_tarea(), "int"), 
            $this->GetSQLValueString($x->getId_depe(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Dependencias $x){
	    $query=sprintf("UPDATE Dependencias SET  Id_tarea=%s, Id_depe=%s WHERE Id=%s",
            $this->GetSQLValueString($x->getId_tarea(), "int"), 
            $this->GetSQLValueString($x->getId_depe(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM Dependencias WHERE Id =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Dependencias WHERE Id= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
        }
	
            
        public function showByTareadependencia($Id_tarea,$Id_dep){
	    $query="SELECT * FROM Dependencias WHERE Id_tarea= ".$Id_tarea."  AND Id_depe=".$Id_dep;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
            

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Dependencias";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new Dependencias();
            $x->setId($row['Id']);
            $x->setId_tarea($row['Id_tarea']);
            $x->setId_depe($row['Id_depe']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}

        public function getDependenciasRealizadas($Id_tarea) {
            $query= "SELECT * FROM Dependencias 
            JOIN TareasActividad ON Dependencias.Id_depe=TareasActividad.Id
            WHERE Id_tarea=" . $Id_tarea;
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                $DependeciasRealizadas=0;
                if($totalRows_consulta>0){
                       do{
                          if($row_consulta['Status']==1){
                             $DependeciasRealizadas++; 
                          }
                   }while($row_consulta= $consulta->fetch_assoc());  
                   
                   if($totalRows_consulta==$DependeciasRealizadas){
                       return 1;
                   }else{
                       return 0;
                   }
                }
            }
       }
       
       public function getDependenciasTarea($Id_tarea) {
            $query= "SELECT * FROM Dependencias WHERE Id_tarea=" . $Id_tarea;
            return $this->advanced_query($query);
       }
       
       public function getTareasDependencia($Id_tarea) {
            $query= "SELECT * FROM Dependencias WHERE Id_depe=" . $Id_tarea;
            return $this->advanced_query($query);
       }

}
