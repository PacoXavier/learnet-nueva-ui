<?php
require_once 'modelos/base.php';
require_once 'modelos/Evaluaciones.php';

class DaoEvaluaciones extends base{
    
	public function add(Evaluaciones $x){
	    $query=sprintf("INSERT INTO Evaluaciones (Ponderacion, Id_mat_esp, Nombre_eva, Activo_eva) VALUES (%s, %s,%s, %s)",
            $this->GetSQLValueString($x->getPonderacion(), "int"), 
            $this->GetSQLValueString($x->getId_mat_esp(), "int"), 
            $this->GetSQLValueString($x->getNombre_eva(), "text"), 
            $this->GetSQLValueString($x->getActivo_eva(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Evaluaciones $x){
	    $query=sprintf("UPDATE Evaluaciones SET  Ponderacion=%s, Id_mat_esp=%s, Nombre_eva=%s, Activo_eva=%s WHERE Id_eva=%s",
            $this->GetSQLValueString($x->getPonderacion(), "int"), 
            $this->GetSQLValueString($x->getId_mat_esp(), "int"), 
            $this->GetSQLValueString($x->getNombre_eva(), "text"), 
            $this->GetSQLValueString($x->getActivo_eva(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE Evaluaciones SET Activo_eva=0 WHERE Id_eva =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Evaluaciones WHERE Id_eva= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	public function create_object($row){
            $x = new Evaluaciones();
            $x->setId($row['Id_eva']);
            $x->setPonderacion($row['Ponderacion']);
            $x->setId_mat_esp($row['Id_mat_esp']);
            $x->setNombre_eva($row['Nombre_eva']);
            $x->setActivo_eva($row['Activo_eva']);
            return $x;
	}


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}

        public function getEvaluacionesMat($Id_mat_esp){
            $query="SELECT * FROM Evaluaciones WHERE Activo_eva=1 AND Id_mat_esp=".$Id_mat_esp;
            return $this->advanced_query($query);
	}

}
