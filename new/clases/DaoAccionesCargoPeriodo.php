<?php
require_once 'modelos/base.php';
require_once 'modelos/AccionesCargoPeriodo.php';

class DaoAccionesCargoPeriodo extends base{
    
        public $table="AccionesCargoPeriodo";
    
	public function add(AccionesCargoPeriodo $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_cargo_periodo, Fecha_captura, Id_usu_captura, Monto, Tipo, Comentarios) VALUES (%s, %s, %s, %s,%s, %s)",
            $this->GetSQLValueString($x->getId_cargo_periodo(), "int"), 
            $this->GetSQLValueString($x->getFecha_captura(), "date"), 
            $this->GetSQLValueString($x->getId_usu_captura(), "int"), 
            $this->GetSQLValueString($x->getMonto(), "double"),
            $this->GetSQLValueString($x->getTipo(), "int"),
            $this->GetSQLValueString($x->getComentarios(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(AccionesCargoPeriodo $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Id_cargo_periodo=%s, Fecha_captura=%s, Id_usu_captura=%s, Monto=%s, Tipo=%s, Comentarios=%s WHERE Id_accion=%s",
            $this->GetSQLValueString($x->getId_cargo_periodo(), "int"), 
            $this->GetSQLValueString($x->getFecha_captura(), "date"), 
            $this->GetSQLValueString($x->getId_usu_captura(), "int"), 
            $this->GetSQLValueString($x->getMonto(), "double"),
            $this->GetSQLValueString($x->getTipo(), "int"),
            $this->GetSQLValueString($x->getComentarios(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	public function create_object($row){
            $x = new AccionesCargoPeriodo();
            $x->setId($row['Id_accion']);
            $x->setId_cargo_periodo($row['Id_cargo_periodo']);
            $x->setFecha_captura($row['Fecha_captura']);
            $x->setId_usu_captura($row['Id_usu_captura']);
            $x->setMonto($row['Monto']);
            $x->setComentarios($row['Comentarios']);
            $x->setTipo($row['Tipo']);
            return $x;
	}


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_accion= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
       public function delete($Id){
            $query = sprintf("DELETE FROM  ".$this->table." WHERE Id_accion =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function getAccionesCargoPeriodo($Id_cargo_periodo){
            $query="SELECT * FROM ".$this->table." WHERE Id_cargo_periodo=".$Id_cargo_periodo." ORDER BY Id_accion ASC";
            return $this->advanced_query($query);
        }

}
