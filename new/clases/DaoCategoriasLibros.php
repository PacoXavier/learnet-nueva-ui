<?php
require_once 'modelos/base.php';
require_once 'modelos/CategoriasLibros.php';

class DaoCategoriasLibros extends base{
        public $table="Categorias_libros";
        
	public function add(CategoriasLibros $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Codigo, Nombre, Parent, Id_plantel, Activo) VALUES (%s, %s,%s, %s,%s)",
            $this->GetSQLValueString($x->getCodigo(), "text"), 
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getParent(), "int"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"), 
            $this->GetSQLValueString($x->getActivo(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(CategoriasLibros $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Codigo=%s, Nombre=%s, Parent=%s, Id_plantel=%s, Activo=%s WHERE Id_cat=%s",
            $this->GetSQLValueString($x->getCodigo(), "text"), 
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getParent(), "int"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"), 
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE ".$this->table." SET Activo=0 WHERE Id_cat =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_cat= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM ".$this->table." WHERE Activo=1 AND Id_plantel=".$this->Id_plantel." ORDER BY Nombre ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new CategoriasLibros();
            $x->setId($row['Id_cat']);
            $x->setCodigo($row['Codigo']);
            $x->setNombre($row['Nombre']);
            $x->setParent($row['Parent']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setActivo($row['Activo']);
            return $x;
	}
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


        public function getSubcategorias($Id_cat){
             $query= "SELECT * FROM Categorias_libros WHERE  Activo=1 AND Parent=".$Id_cat." ORDER BY Id_cat DESC";
             return $this->advanced_query($query);
        }
}
