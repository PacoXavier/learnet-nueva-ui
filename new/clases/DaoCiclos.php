<?php
require_once 'modelos/base.php';
require_once 'modelos/Ciclos.php';

class DaoCiclos extends base{

	public function add(Ciclos $x){
              $query=sprintf("INSERT INTO ciclos_ulm (Nombre_ciclo,Clave,Fecha_ini,Fecha_fin,Activo_ciclo,Id_plantel) VALUES (%s ,%s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getNombre_ciclo(), "text"),
              $this->GetSQLValueString($x->getClave(), "text"),
              $this->GetSQLValueString($x->getFecha_ini(), "date"),
              $this->GetSQLValueString($x->getFecha_fin(), "date"),
              $this->GetSQLValueString($x->getActivo_ciclo(), "int"),
              $this->GetSQLValueString($x->getId_plantel(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(Ciclos $x){
	    $query=sprintf("UPDATE ciclos_ulm SET Nombre_ciclo=%s, Clave=%s, Fecha_ini=%s,Fecha_fin=%s,Activo_ciclo=%s, Id_plantel=%s  WHERE Id_ciclo = %s",
            $this->GetSQLValueString($x->getNombre_ciclo(), "text"),
            $this->GetSQLValueString($x->getClave(), "text"),
            $this->GetSQLValueString($x->getFecha_ini(), "date"),
            $this->GetSQLValueString($x->getFecha_fin(), "date"),
            $this->GetSQLValueString($x->getActivo_ciclo(), "int"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}
	
	
	
	public function delete($Id){
                $query = sprintf("DELETE FROM ciclos_ulm WHERE Id_ciclo=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
	
	public function show($Id){
	    $query="SELECT * FROM ciclos_ulm WHERE Id_ciclo= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM ciclos_ulm WHERE Activo_ciclo=1 AND  Id_plantel=".$this->Id_plantel." ORDER BY Id_ciclo DESC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
	
	
	
	public function create_object($row){
            $x = new Ciclos();
            $x->setId($row['Id_ciclo']);
            $x->setNombre_ciclo($row['Nombre_ciclo']);
            $x->setClave($row['Clave']);
            $x->setFecha_ini($row['Fecha_ini']);
            $x->setFecha_fin($row['Fecha_fin']); 
            $x->setActivo_ciclo($row['Activo_ciclo']); 
            $x->setId_plantel($row['Id_plantel']); 
            return $x;
	}
        
        public function getActual($Id_plantel=null){
            $resp=array();
            $query="";
            if($Id_plantel==null){
                $query=" AND Id_plantel=".$this->Id_plantel;
                $Id_p_x=$this->Id_plantel;
            }else{
                $query=" AND Id_plantel=".$Id_plantel;
                $Id_p_x=$Id_plantel;
            }
            $query="SELECT  * FROM ciclos_ulm  WHERE Fecha_ini<='".date('Y-m-d')."' AND Fecha_fin>='".date('Y-m-d')."' AND Activo_ciclo=1 ".$query;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
	        $totalRows_consulta= $Result1->num_rows;
                if($totalRows_consulta>0){
                    $resp=$this->create_object($Result1->fetch_assoc());
                }else{
                    $query="SELECT * FROM ciclos_ulm WHERE Fecha_fin<'".date('Y-m-d')."' AND Activo_ciclo=1 AND Id_plantel=".$Id_p_x." ORDER BY Id_ciclo DESC LIMIT 1";
                    $consulta=$this->advanced_query($query);
                    if(count($consulta)>0){
                       $resp=$this->show($consulta[0]->getId());
                    }else{
                       $resp=$this->show(0);
                    }

                }
                
            }
            return $resp;
         }  

         public function getCiclosFuturos(){
             $resp=array();
             $ciclo=$this->getActual();
             if(strlen($ciclo->getFecha_ini())>0){
                 $query=$ciclo->getFecha_ini();
             }else{
                 $query=date('Y-m-d');
             }
             $x="SELECT * FROM ciclos_ulm WHERE Activo_ciclo=1 AND Id_plantel=".$this->Id_plantel." AND Fecha_ini>='".$query."' ORDER BY Id_ciclo DESC";
             foreach($this->advanced_query($x) as $k=>$v){
                 array_push($resp, $this->show($v->getId()));
             }
             return $resp;
         }
         
         public function advanced_query($query){
		    $resp=array();
		    $consulta=$this->_cnn->query($query);
		    if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	}
        
        
        	
	public function getCicloPorFecha($Fecha){
	    $query="SELECT  * FROM ciclos_ulm  WHERE Fecha_ini<='".$Fecha."' AND Fecha_fin>='".$Fecha."' AND Activo_ciclo=1 AND Id_plantel=".$this->Id_plantel;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
	        $totalRows_consulta= $Result1->num_rows;
                if($totalRows_consulta>0){
                     return $this->create_object($Result1->fetch_assoc());
                }else{
                    return $this->getActual();
                }
            }
	}

	

}

