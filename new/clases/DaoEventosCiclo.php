<?php
require_once 'modelos/base.php';
require_once 'modelos/EventosCiclo.php';

class DaoEventosCiclo extends base{
        public $table="EventosCiclo";
        
	public function add(EventosCiclo $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_ciclo, FechaIni, FechaFin, Nombre, Id_usu, DateCreated, Color,TipoEvento) VALUES (%s, %s,%s, %s,%s, %s, %s, %s)",
            $this->GetSQLValueString($x->getId_ciclo(), "int"), 
            $this->GetSQLValueString($x->getFechaIni(), "date"), 
            $this->GetSQLValueString($x->getFechaFin(), "date"), 
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getColor(), "text"),
            $this->GetSQLValueString($x->getTipoEvento(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(EventosCiclo $x){
	    $query=sprintf("UPDATE ".$this->table."  SET Id_ciclo=%s, FechaIni=%s, FechaFin=%s, Nombre=%s, Id_usu=%s, DateCreated=%s, Color=%s, TipoEvento=%s WHERE Id_eve=%s",
            $this->GetSQLValueString($x->getId_ciclo(), "int"), 
            $this->GetSQLValueString($x->getFechaIni(), "date"), 
            $this->GetSQLValueString($x->getFechaFin(), "date"), 
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getColor(), "text"),
            $this->GetSQLValueString($x->getTipoEvento(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM ".$this->table."   WHERE Id_eve =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	
	public function show($Id){
	    $query="SELECT * FROM ".$this->table."  WHERE Id_eve= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}


	
	public function create_object($row){
            $x = new EventosCiclo();
            $x->setId($row['Id_eve']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setFechaIni($row['FechaIni']);
            $x->setFechaFin($row['FechaFin']);
            $x->setNombre($row['Nombre']);
            $x->setId_usu($row['Id_usu']);
            $x->setDateCreated($row['DateCreated']);
            $x->setColor($row['Color']);
            $x->setTipoEvento($row['TipoEvento']);
            return $x;
	}


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getEventosCiclo($Id_ciclo){
            $query="SELECT * FROM ".$this->table."  WHERE Id_ciclo=".$Id_ciclo." ORDER BY FechaIni ASC";
            return $this->advanced_query($query);
        }
        
        public function getEventosCicloTipo($Id_ciclo,$Tipo){
            $query="SELECT * FROM ".$this->table."  WHERE Id_ciclo=".$Id_ciclo." AND (TipoEvento='".$Tipo."' OR TipoEvento='ambos') ORDER BY FechaIni ASC";
            return $this->advanced_query($query);
        }
        

}
