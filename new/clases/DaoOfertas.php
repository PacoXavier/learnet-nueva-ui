<?php
require_once 'modelos/base.php';
require_once 'modelos/Ofertas.php';

class DaoOfertas extends base{
    
	public function add(Ofertas $x){
	    $query=sprintf("INSERT INTO ofertas_ulm ( Nombre_oferta, Activo_oferta, Clave_oferta,Tipo_recargo,Valor,Id_plantel_x,Date_created, Tipo_oferta,MontoCargoMateriaAcreditada) VALUES (%s, %s,%s, %s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getNombre_oferta(), "text"), 
            $this->GetSQLValueString($x->getActivo_oferta(), "int"), 
            $this->GetSQLValueString($x->getClave_oferta(), "text"),
            $this->GetSQLValueString($x->getTipo_recargo(), "text"),
            $this->GetSQLValueString($x->getValor(), "double"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getTipoOferta(), "int"),
            $this->GetSQLValueString($x->getMontoCargoMateriaAcreditada(), "double"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Ofertas $x){
	    $query=sprintf("UPDATE ofertas_ulm SET  Nombre_oferta=%s, Activo_oferta=%s, Clave_oferta=%s,Tipo_recargo=%s,Valor=%s,Id_plantel_x=%s,Date_created=%s, Tipo_oferta=%s,MontoCargoMateriaAcreditada=%s WHERE Id_oferta=%s",
            $this->GetSQLValueString($x->getNombre_oferta(), "text"), 
            $this->GetSQLValueString($x->getActivo_oferta(), "int"), 
            $this->GetSQLValueString($x->getClave_oferta(), "text"),
            $this->GetSQLValueString($x->getTipo_recargo(), "text"),
            $this->GetSQLValueString($x->getValor(), "double"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getTipoOferta(), "int"),
            $this->GetSQLValueString($x->getMontoCargoMateriaAcreditada(), "double"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE ofertas_ulm SET Activo_oferta=0 WHERE Id_oferta =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM ofertas_ulm WHERE Id_oferta= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM ofertas_ulm 
                WHERE Activo_oferta=1 AND Id_plantel_x=".$this->Id_plantel." ORDER BY Nombre_oferta ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new Ofertas();
            $x->setId($row['Id_oferta']);
            $x->setNombre_oferta($row['Nombre_oferta']);
            $x->setActivo_oferta($row['Activo_oferta']);
            $x->setClave_oferta($row['Clave_oferta']);
            $x->setTipo_recargo($row['Tipo_recargo']);
            $x->setValor($row['Valor']);
            $x->setId_plantel($row['Id_plantel_x']);
            $x->setDateCreated($row['Date_created']);
            $x->setTipoOferta($row['Tipo_oferta']);
            $x->setMontoCargoMateriaAcreditada($row['MontoCargoMateriaAcreditada']);
            return $x;
	}
        
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


}
