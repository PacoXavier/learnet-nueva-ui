<?php
require_once 'modelos/base.php';
require_once 'modelos/AccionesPeriodoCiclo.php';


class DaoAccionesPeriodoCiclo extends base{
    
        public $table="AccionesPeriodoCiclo";
    
	public function add(AccionesPeriodoCiclo $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_periodo_ciclo, Fecha_captura, Id_usu_captura, Monto, Porcentaje, Comentarios, TipoRel, IdRel) VALUES (%s, %s, %s, %s,%s, %s, %s, %s)",
            $this->GetSQLValueString($x->getId_periodo_ciclo(), "int"), 
            $this->GetSQLValueString($x->getFecha_captura(), "date"), 
            $this->GetSQLValueString($x->getId_usu_captura(), "int"), 
            $this->GetSQLValueString($x->getMonto(), "double"),
            $this->GetSQLValueString($x->getPorcentaje(), "double"),
            $this->GetSQLValueString($x->getComentarios(), "text"),
            $this->GetSQLValueString($x->getTipoRel(), "int"),
            $this->GetSQLValueString($x->getIdRel(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(AccionesPeriodoCiclo $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Id_periodo_ciclo=%s, Fecha_captura=%s, Id_usu_captura=%s, Monto=%s, Porcentaje=%s, Comentarios=%s, TipoRel=%s, IdRel=%s WHERE Id_accion=%s",
            $this->GetSQLValueString($x->getId_periodo_ciclo(), "int"), 
            $this->GetSQLValueString($x->getFecha_captura(), "date"), 
            $this->GetSQLValueString($x->getId_usu_captura(), "int"), 
            $this->GetSQLValueString($x->getMonto(), "double"),
            $this->GetSQLValueString($x->getPorcentaje(), "double"),
            $this->GetSQLValueString($x->getComentarios(), "text"),
            $this->GetSQLValueString($x->getTipoRel(), "int"),
            $this->GetSQLValueString($x->getIdRel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	public function create_object($row){
            $x = new AccionesPeriodoCiclo();
            $x->setId($row['Id_accion']);
            $x->setId_periodo_ciclo($row['Id_periodo_ciclo']);
            $x->setFecha_captura($row['Fecha_captura']);
            $x->setId_usu_captura($row['Id_usu_captura']);
            $x->setMonto($row['Monto']);
            $x->setPorcentaje($row['Porcentaje']);
            $x->setComentarios($row['Comentarios']);
            $x->setTipoRel($row['TipoRel']);
            $x->setIdRel($row['IdRel']);
            return $x;
	}


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_accion= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
       public function delete($Id){
            $query = sprintf("DELETE FROM  ".$this->table." WHERE Id_accion =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function getAccionesPeriodoCiclo($Id_periodo_ciclo){
            $query="SELECT * FROM ".$this->table." WHERE Id_periodo_ciclo=".$Id_periodo_ciclo;
            return $this->advanced_query($query);
        }

}
