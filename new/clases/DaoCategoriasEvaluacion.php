<?php
require_once 'modelos/base.php';
require_once 'modelos/CategoriasEvaluacion.php';

class DaoCategoriasEvaluacion extends base{
    
        public $table="CategoriasEvaluacion";
    
	public function add(CategoriasEvaluacion $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Nombre, DatecCeated, Id_usu,Activo,Id_plantel) VALUES (%s, %s,%s,%s,%s)",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getDatecCeated(), "date"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getId_plantel(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(CategoriasEvaluacion $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Nombre=%s, DatecCeated=%s, Id_usu=%s,Activo=%s, Id_plantel=%s WHERE Id_cat=%s",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getDatecCeated(), "date"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	
	public function delete($Id){
            $query = sprintf("UPDATE ".$this->table." SET Activo=0 WHERE Id_cat =%s",
            $this->GetSQLValueString($Id, "int")); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	public function show($Id){
	    $query=sprintf("SELECT * FROM ".$this->table." WHERE Id_cat=%s",
            $this->GetSQLValueString($Id, "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new CategoriasEvaluacion();
            $x->setId($row['Id_cat']);
            $x->setNombre($row['Nombre']);
            $x->setDatecCeated($row['DatecCeated']);
            $x->setId_usu($row['Id_usu']);
            $x->setActivo($row['Activo']);
            $x->setId_plantel($row['Id_plantel']);
            return $x;
	}
        


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                   do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        
        
        public function getCategoriasEvaluacion(){
            $query="SELECT * FROM ".$this->table." WHERE Activo=1 AND Id_plantel=".$this->Id_plantel." ORDER BY Id_cat ASC";
            return $this->advanced_query($query);
        }

        
}
