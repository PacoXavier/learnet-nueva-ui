<?php
require_once 'modelos/base.php';
require_once 'modelos/EventosAutoAsignados.php';

class DaoEventosAutoAsignados extends base{
	public function add(EventosAutoAsignados $x){
	    $query=sprintf("INSERT INTO EventosAutoAsignados (Nombre, Descripcion, HoraInicio, HoraFin, FechaIni, IdRel, TipoRel,FechaFin) VALUES (%s, %s,%s, %s,%s, %s, %s, %s)",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getDescripcion(), "text"), 
            $this->GetSQLValueString($x->getHoraInicio(), "text"), 
            $this->GetSQLValueString($x->getHoraFin(), "text"), 
            $this->GetSQLValueString($x->getFechaIni(), "text"), 
            $this->GetSQLValueString($x->getIdRel(), "int"), 
            $this->GetSQLValueString($x->getTipoRel(), "text"),
            $this->GetSQLValueString($x->getFechaFin(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(EventosAutoAsignados $x){
	    $query=sprintf("UPDATE EventosAutoAsignados SET Nombre=%s, Descripcion=%s, HoraInicio=%s, HoraFin=%s, FechaIni=%s, IdRel=%s, TipoRel=%s,FechaFin=%s WHERE Id_evento=%s",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getDescripcion(), "text"), 
            $this->GetSQLValueString($x->getHoraInicio(), "text"), 
            $this->GetSQLValueString($x->getHoraFin(), "text"), 
            $this->GetSQLValueString($x->getFechaIni(), "text"), 
            $this->GetSQLValueString($x->getIdRel(), "int"), 
            $this->GetSQLValueString($x->getTipoRel(), "text"),
            $this->GetSQLValueString($x->getFechaFin(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM EventosAutoAsignados  WHERE Id_evento =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	
	public function show($Id){
	    $query="SELECT * FROM EventosAutoAsignados WHERE Id_evento= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}


	
	public function create_object($row){
            $x = new EventosAutoAsignados();
            $x->setId($row['Id_evento']);
            $x->setNombre($row['Nombre']);
            $x->setDescripcion($row['Descripcion']);
            $x->setHoraInicio($row['HoraInicio']);
            $x->setHoraFin($row['HoraFin']);
            $x->setFechaIni($row['FechaIni']);
            $x->setIdRel($row['IdRel']);
            $x->setTipoRel($row['TipoRel']);
            $x->setFechaFin($row['FechaFin']);
            return $x;
	}


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getEventos($IdRel,$TipoRel){
            $query="SELECT * FROM EventosAutoAsignados WHERE IdRel=".$IdRel." AND TipoRel='".$TipoRel."'";
            return $this->advanced_query($query);
        }
        

}
