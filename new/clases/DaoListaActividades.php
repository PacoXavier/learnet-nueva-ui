<?php
require_once 'modelos/base.php';
require_once 'modelos/ListaActividades.php';
require_once 'DaoTareasActividad.php';


class DaoListaActividades extends base{
	public function add(ListaActividades $x){
	    $query=sprintf("INSERT INTO ListaActividades (Nombre, Comentarios, Id_usu, Id_plantel, DateCreated, Orden) VALUES (%s, %s,%s, %s,%s,%s)",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getComentarios(), "text"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getOrder(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(ListaActividades $x){
	    $query=sprintf("UPDATE ListaActividades SET  Nombre=%s, Comentarios=%s, Id_usu=%s, Id_plantel=%s, DateCreated=%s, Orden=%s WHERE Id=%s",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getComentarios(), "text"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getOrder(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM ListaActividades WHERE Id =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM ListaActividades WHERE Id= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM ListaActividades WHERE Id_plantel=".$this->Id_plantel." ORDER BY Orden ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new ListaActividades();
            $x->setId($row['Id']);
            $x->setNombre($row['Nombre']);
            $x->setComentarios($row['Comentarios']);
            $x->setId_usu($row['Id_usu']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setDateCreated($row['DateCreated']);
            $x->setOrder($row['Orden']);
            return $x;
	}
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function nextOrder(){
	    $query="SELECT * FROM ListaActividades ORDER BY Orden DESC LIMIT 1";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                $orden=1;
                if($totalRows_consulta>0){ 
                    $orden=$row_consulta['Orden']+1;
                }
            }
            return $orden;
	}
        
        
        public function getActividadesAbiertas(){
            $resp=array();
            $actividades=array();
            $DaoUsuarios= new DaoUsuarios();
            $DaoTareasActividad= new DaoTareasActividad();
            $query="SELECT * FROM ListaActividades WHERE Id_plantel=".$this->Id_plantel." ORDER BY ListaActividades.Orden ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   
                   $count=0;
                   $query_dos="SELECT * FROM TareasActividad WHERE Id_act=".$row_consulta['Id'];
                   foreach ($DaoTareasActividad->advanced_query($query_dos) as $k=>$v){
                           if($v->getStatus()==0 && !in_array($row_consulta['Id'],$actividades)){
                              array_push($actividades, $row_consulta['Id']);
                              array_push($resp, $this->create_object($row_consulta));
                           }
                           $count++;
                   }
                   //Si la actividad es nueva y no tiene tareas, se cuenta como abierta
                   if($count==0){
                      array_push($resp, $this->create_object($row_consulta));
                   }
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
        
        
        public function getActividadesCerradas(){
            $resp=array();
            $actividades=array();
            $DaoTareasActividad= new DaoTareasActividad();
            $query="SELECT * FROM ListaActividades WHERE Id_plantel=".$this->Id_plantel." ORDER BY ListaActividades.Orden ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   //Obtenemos el numero de tareas de cada actividad
                   $count=0;
                   $query_dos="SELECT * FROM TareasActividad WHERE Id_act=".$row_consulta['Id'];
                   foreach ($DaoTareasActividad->advanced_query($query_dos) as $k=>$v){
                           $count++;
                   }
                   
                   $count_aux=0;
                   $query_dos="SELECT * FROM TareasActividad WHERE Id_act=".$row_consulta['Id'];
                   foreach ($DaoTareasActividad->advanced_query($query_dos) as $k=>$v){
                           if($v->getStatus()==1){
                              $count_aux++;
                              if(!in_array($row_consulta['Id'],$actividades)){
                                  array_push($actividades, $row_consulta['Id']);
                              }
                           }
                   }
                   
                   
                   if($count==$count_aux){
                      array_push($resp, $this->create_object($row_consulta)); 
                   }
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}


}
