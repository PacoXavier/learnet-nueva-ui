<?php
require_once 'modelos/base.php';
require_once 'modelos/NotificacionesTipoUsuario.php';

class DaoNotificacionesTipoUsuario extends base{

	public function add(NotificacionesTipoUsuario $x){
              $query=sprintf("INSERT INTO Notificaciones_usuarios (IdRel, TipoRel, DateRead, Id_not) VALUES (%s ,%s, %s, %s)",
              $this->GetSQLValueString($x->getIdRel(), "int"),
              $this->GetSQLValueString($x->getTipoRel(), "text"),
              $this->GetSQLValueString($x->getDateRead(), "date"),
              $this->GetSQLValueString($x->getId_not(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(NotificacionesTipoUsuario $x){
	  $query=sprintf("UPDATE Notificaciones_usuarios SET IdRel=%s, TipoRel=%s, DateRead=%s, Id_not=%s  WHERE Id = %s",
          $this->GetSQLValueString($x->getIdRel(), "int"),
          $this->GetSQLValueString($x->getTipoRel(), "text"),
          $this->GetSQLValueString($x->getDateRead(), "date"),
          $this->GetSQLValueString($x->getId_not(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}

	public function delete($Id){
                $query = sprintf("DELETE FROM Notificaciones_usuarios WHERE Id=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
        
        public function deleteNotificaciones($Id){
                $query = sprintf("DELETE FROM Notificaciones_usuarios WHERE Id_not=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Notificaciones_usuarios WHERE Id= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new NotificacionesTipoUsuario();
            $x->setId($row['Id']);
            $x->setIdRel($row['IdRel']);
            $x->setTipoRel($row['TipoRel']);
            $x->setDateRead($row['DateRead']);
            $x->setId_not($row['Id_not']);
            return $x;
	}

         public function advanced_query($query){
		    $resp=array();
		    $consulta=$this->_cnn->query($query);
		    if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	}
        
        public function getNotificacionTipoUsuario($TipoRel,$IdRel) {
            $query  = "SELECT * FROM Notificaciones_usuarios
                       JOIN (SELECT * FROM Notificaciones WHERE DateInit<='".date('Y-m-d')."' AND Id_plantel=".$this->Id_plantel." AND Activo=1) 
                       AS Noti ON Notificaciones_usuarios.Id_not=Noti.Id_not 
                       WHERE IdRel=".$IdRel." AND TipoRel='".$TipoRel."' AND DateRead IS NULL ORDER BY Id DESC";
            return $this->advanced_query($query);
       }

}

