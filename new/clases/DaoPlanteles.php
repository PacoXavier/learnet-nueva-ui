<?php
require_once 'modelos/base.php';
require_once 'modelos/Planteles.php';


class DaoPlanteles extends base{
	public function add(Planteles $x){
	    $query=sprintf("INSERT INTO planteles_ulm (Nombre_plantel, Id_dir_plantel, Activo_plantel,Clave,Tel,DateCreated,Id_img_logo, Id_img_correo, Abreviatura, Email,Dominio,Formato_pago,Background_login,Formato_pago_evento,EtiquetaLibro,EtiquetaActivo,Formato_inscripcion) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getNombre_plantel(), "text"), 
            $this->GetSQLValueString($x->getId_dir_plantel(), "int"),
            $this->GetSQLValueString($x->getActivo_plantel(), "int"),
            $this->GetSQLValueString($x->getClave(), "text"),
            $this->GetSQLValueString($x->getTel(), "text"),
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getId_img_logo(), "text"),
            $this->GetSQLValueString($x->getId_img_correo(), "text"),
            $this->GetSQLValueString($x->getAbreviatura(), "text"),
            $this->GetSQLValueString($x->getEmail(), "text"),
            $this->GetSQLValueString($x->getDominio(), "text"),
            $this->GetSQLValueString($x->getFormato_pago(), "text"),
            $this->GetSQLValueString($x->getBackground_login(), "text"),
            $this->GetSQLValueString($x->getFormato_pago_evento(), "text"),
            $this->GetSQLValueString($x->getEtiquetaLibro(), "text"),
            $this->GetSQLValueString($x->getEtiquetaActivo(), "text"),
            $this->GetSQLValueString($x->getFormato_inscripcion(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}
        

    
	public function update(Planteles $x){
	    $query=sprintf("UPDATE planteles_ulm SET  Nombre_plantel=%s, Id_dir_plantel=%s, Activo_plantel=%s,Clave=%s,Tel=%s,DateCreated=%s,Id_img_logo=%s, Id_img_correo=%s, Abreviatura=%s, Email=%s,Dominio=%s, Formato_pago=%s,Background_login=%s, Formato_pago_evento=%s, EtiquetaLibro=%s,EtiquetaActivo=%s,Formato_inscripcion=%s WHERE Id_plantel=%s",
            $this->GetSQLValueString($x->getNombre_plantel(), "text"), 
            $this->GetSQLValueString($x->getId_dir_plantel(), "int"),
            $this->GetSQLValueString($x->getActivo_plantel(), "int"),
            $this->GetSQLValueString($x->getClave(), "text"),
            $this->GetSQLValueString($x->getTel(), "text"),
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getId_img_logo(), "text"),
            $this->GetSQLValueString($x->getId_img_correo(), "text"),
            $this->GetSQLValueString($x->getAbreviatura(), "text"),
            $this->GetSQLValueString($x->getEmail(), "text"),
            $this->GetSQLValueString($x->getDominio(), "text"),
            $this->GetSQLValueString($x->getFormato_pago(), "text"),
            $this->GetSQLValueString($x->getBackground_login(), "text"),
            $this->GetSQLValueString($x->getFormato_pago_evento(), "text"),
            $this->GetSQLValueString($x->getEtiquetaLibro(), "text"),
            $this->GetSQLValueString($x->getEtiquetaActivo(), "text"),
            $this->GetSQLValueString($x->getFormato_inscripcion(), "text"),
            $this->GetSQLValueString($x->getId(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE planteles_ulm SET Activo_plantel=0 WHERE Id_plantel =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM planteles_ulm WHERE Id_plantel= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
        }
	
            
	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM planteles_ulm WHERE Activo_plantel=1";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new Planteles();
            $x->setId($row['Id_plantel']);
            $x->setNombre_plantel($row['Nombre_plantel']);
            $x->setId_dir_plantel($row['Id_dir_plantel']);
            $x->setActivo_plantel($row['Activo_plantel']);
            $x->setClave($row['Clave']);
            $x->setTel($row['Tel']);
            $x->setDateCreated($row['DateCreated']);
            $x->setId_img_logo($row['Id_img_logo']);
            $x->setId_img_correo($row['Id_img_correo']);
            $x->setAbreviatura($row['Abreviatura']);
            $x->setEmail($row['Email']);
            $x->setDominio($row['Dominio']);
            $x->setFormato_pago($row['Formato_pago']);
            $x->setBackground_login($row['Background_login']);
            $x->setFormato_pago_evento($row['Formato_pago_evento']);
            $x->setEtiquetaLibro($row['EtiquetaLibro']);
            $x->setEtiquetaActivo($row['EtiquetaActivo']);
            $x->setFormato_inscripcion($row['Formato_inscripcion']);
            return $x;
	}

        
	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
}


