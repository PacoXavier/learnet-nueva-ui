<?php
require_once 'modelos/base.php';
require_once 'modelos/PeriodosExamenes.php';

class DaoPeriodosExamenes extends base{
	public function add(PeriodosExamenes $x){
	    $query=sprintf("INSERT INTO PeriodosExamenes (Id_ciclo, FechaIni, FechaFin, Nombre, Id_usu, DateCreated, Color) VALUES (%s, %s,%s, %s,%s, %s, %s)",
            $this->GetSQLValueString($x->getId_ciclo(), "int"), 
            $this->GetSQLValueString($x->getFechaIni(), "date"), 
            $this->GetSQLValueString($x->getFechaFin(), "date"), 
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getColor(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(PeriodosExamenes $x){
	    $query=sprintf("UPDATE PeriodosExamenes SET Id_ciclo=%s, FechaIni=%s, FechaFin=%s, Nombre=%s, Id_usu=%s, DateCreated=%s, Color=%s WHERE Id_per=%s",
            $this->GetSQLValueString($x->getId_ciclo(), "int"), 
            $this->GetSQLValueString($x->getFechaIni(), "date"), 
            $this->GetSQLValueString($x->getFechaFin(), "date"), 
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getColor(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM PeriodosExamenes  WHERE Id_per =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	
	public function show($Id){
	    $query="SELECT * FROM PeriodosExamenes WHERE Id_per= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}


	
	public function create_object($row){
            $x = new PeriodosExamenes();
            $x->setId($row['Id_per']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setFechaIni($row['FechaIni']);
            $x->setFechaFin($row['FechaFin']);
            $x->setNombre($row['Nombre']);
            $x->setId_usu($row['Id_usu']);
            $x->setDateCreated($row['DateCreated']);
            $x->setColor($row['Color']);
            return $x;
	}


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getPeriodosExamenCiclo($Id_ciclo){
            $query="SELECT * FROM PeriodosExamenes WHERE Id_ciclo=".$Id_ciclo." ORDER BY FechaIni ASC";
            return $this->advanced_query($query);
        }
        

}
