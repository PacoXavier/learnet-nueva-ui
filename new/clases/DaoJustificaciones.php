<?php

require_once 'modelos/base.php';
require_once 'modelos/Justificaciones.php';
require_once 'DaoHoras.php';
require_once 'DaoHorarioDocente.php';

class DaoJustificaciones extends base {

    public function add(Justificaciones $x) {
        $query = sprintf("INSERT INTO Justificaciones (Id_alum, Id_ciclo, Dia_justificado, Id_usu, Date,Comentarios,HoraIni, HoraFin) VALUES (%s, %s, %s, %s, %s, %s,%s, %s)", 
        $this->GetSQLValueString($x->getId_alum(), "int"), 
        $this->GetSQLValueString($x->getId_ciclo(), "int"), 
        $this->GetSQLValueString($x->getDia_justificado(), "date"), 
        $this->GetSQLValueString($x->getId_usu(), "int"), 
        $this->GetSQLValueString($x->getDate(), "date"), 
        $this->GetSQLValueString($x->getComentarios(), "text"), 
        $this->GetSQLValueString($x->getHoraIni(), "text"), 
        $this->GetSQLValueString($x->getHoraFin(), "text"));
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function update(Justificaciones $x) {
        $query = sprintf("UPDATE Justificaciones SET  Id_alum=%s, Id_ciclo=%s, Dia_justificado=%s, Id_usu=%s, Date=%s, Comentarios=%s, HoraIni=%s, HoraFin=%s WHERE Id_jus=%s", $this->GetSQLValueString($x->getId_alum(), "int"), $this->GetSQLValueString($x->getId_ciclo(), "int"), $this->GetSQLValueString($x->getDia_justificado(), "date"), $this->GetSQLValueString($x->getId_usu(), "int"), $this->GetSQLValueString($x->getDate(), "date"), $this->GetSQLValueString($x->getComentarios(), "text"), $this->GetSQLValueString($x->getHoraIni(), "text"), $this->GetSQLValueString($x->getHoraFin(), "text"), $this->GetSQLValueString($x->getId(), "int"));
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id) {
        $query = sprintf("DELETE FROM Justificaciones WHERE Id_jus =" . $Id);
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return true;
        }
    }

    public function show($Id) {
        $query = "SELECT * FROM Justificaciones WHERE Id_jus= " . $Id;
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->create_object($Result1->fetch_assoc());
        }
    }

    public function create_object($row) {
        $x = new Justificaciones();
        $x->setId($row['Id_jus']);
        $x->setId_alum($row['Id_alum']);
        $x->setId_ciclo($row['Id_ciclo']);
        $x->setDia_justificado($row['Dia_justificado']);
        $x->setId_usu($row['Id_usu']);
        $x->setDate($row['Date']);
        $x->setComentarios($row['Comentarios']);
        $x->setHoraIni($row['HoraIni']);
        $x->setHoraFin($row['HoraFin']);

        return $x;
    }

    public function advanced_query($query) {
        $resp = array();
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                do {
                    array_push($resp, $this->create_object($row_consulta));
                } while ($row_consulta = $consulta->fetch_assoc());
            }
        }
        return $resp;
    }

    public function getJustificacionByGrupoAlum($Id_alum, $Id_ciclo, $Id_grupo) {
        $resp = array();
        $DaoHoras= new DaoHoras();
        $DaoHorarioDocente= new DaoHorarioDocente();
        $query = "SELECT * FROM Justificaciones WHERE Id_alum=" . $Id_alum . " AND Id_ciclo=" . $Id_ciclo . " ORDER BY Dia_justificado DESC";
        foreach($this->advanced_query($query) as $just){
  
                //Si se les justificosolo unas horas
                if ($just->getHoraIni() != null && $just->getHoraFin() != null) {
                    $Ini = substr($just->getHoraIni(), 0, strpos($just->getHoraIni(), ":"));
                    $row_HoraIni=$DaoHoras->getTextoHora($Ini);

                    $Fin = substr($just->getHoraFin(), 0, strpos($just->getHoraFin(), ":"));
                    $row_HoraFin = $DaoHoras->getTextoHora($Fin);

                    $queryHora = "";
                    if ($row_HoraIni->getId() == $row_HoraFin->getId()) {
                        $queryHora = " AND Hora=" . $row_HoraIni->getId();
                    } else {
                        

                        $query = "SELECT * FROM Horas WHERE Id_hora>=" . $row_HoraIni->getId() . " && Id_hora<" . $row_HoraFin->getId();
                        foreach($DaoHoras->advanced_query($query) as $hora){
                            $queryHora.="Hora=" . $hora->getId() . " || ";
                        }
                        
                        $queryHora = substr($queryHora, 0, strripos($queryHora, '||'));
                        $queryHora = "AND (" . $queryHora . ")";
                    }
                    //Verificamos el dia justificado
                    $dia = date("N", strtotime($just->getDia_justificado()));
                    $queryDia = "";
                    if ($dia == 1) {
                        $queryDia = "AND Lunes=1";
                    } elseif ($dia == 2) {
                        $queryDia = "AND Martes=1";
                    } elseif ($dia == 3) {
                        $queryDia = "AND Miercoles=1";
                    } elseif ($dia == 4) {
                        $queryDia = "AND Jueves=1";
                    } elseif ($dia == 5) {
                        $queryDia = "AND Viernes=1";
                    } elseif ($dia == 6) {
                        $queryDia = "AND Sabado=1";
                    }

                    $query = "SELECT * FROM Horario_docente WHERE Id_ciclo=" . $Id_ciclo . " AND Id_grupo=" . $Id_grupo . " " . $queryHora . " " . $queryDia;
                    $consult=$DaoHorarioDocente->getHorarioQuery($query);
                    if($consult->getId()>0){
                        array_push($resp, $this->show($just->getId()));
                    }
                } else {
                    //Si se justifico el dia completo
                    array_push($resp, $this->show($just->getId()));
                }
        }
        return $resp;
    }
    
    
    public function getFaltasAlumnoCiclo($Id_ciclo,$Id_alum) {
        $resp = array();
        $query = "SELECT * FROM Asistencias 
                                WHERE Id_ciclo=" .$Id_ciclo." 
                                  AND Id_alum=".$Id_alum." 
                                  AND Asistio=0 
                                  AND Fecha_asis NOT IN (SELECT Dia_justificado FROM Justificaciones WHERE Id_ciclo=".$Id_ciclo." AND Id_alum=".$Id_alum.") 
                GROUP BY Fecha_asis ORDER BY Fecha_asis ASC";
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                do {
                    array_push($resp, $row_consulta);
                } while ($row_consulta = $consulta->fetch_assoc());
            }
        }
        return $resp;
    }
    
    
    public function getJustificacionesByCicloAlum($Id_alum,$Id_ciclo){
            $query  = "SELECT * FROM Justificaciones WHERE Id_alum=".$Id_alum." AND Id_ciclo=".$Id_ciclo." ORDER BY Dia_justificado DESC";
            return $this->advanced_query($query);
     } 

}

