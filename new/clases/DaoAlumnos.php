<?php
require_once 'modelos/base.php';
require_once 'modelos/Alumnos.php';
require_once 'DaoUsuarios.php';
require_once 'DaoCiclosAlumno.php';
require_once 'DaoOfertasAlumno.php';
require_once 'DaoMateriasEspecialidad.php';
require_once 'DaoHorarioDocente.php';
require_once 'DaoGrupos.php';
require_once 'DaoDocentes.php';
require_once 'DaoAulas.php';
require_once 'DaoMaterias.php';
require_once 'DaoPagos.php';
require_once 'DaoRecargos.php';
require_once 'DaoPagosCiclo.php';
require_once 'DaoPaquetesDescuentosAlumno.php';


class DaoAlumnos extends base{
//123244354565665767
	public function add(Alumnos $x){
	  $query=sprintf("INSERT INTO inscripciones_ulm (Nombre_ins, ApellidoP_ins, ApellidoM_ins, Edad_ins, Email_ins,
             Tel_ins, Cel_ins, Id_medio_ent, Seguimiento_ins, Fecha_ins, FechaNac, Sexo, Id_dir, Curp_ins, NombreTutor, EmailTutor,
             TelTutor, UltGrado_est, EscuelaPro, Matricula, Alta_alum, Baja_alum, Activo_alum, Pass_alum, RecoverPass, LastSession
             ,Img_alum, tipo, medio_contacto, Refencia_pago, Comentarios, Tipo_sangre, Alergias, Enfermedades_cronicas, Preinscripciones_medicas
             ,Id_plantel,Id_usu_capturo,MatriculaSep, ExpedienteSep) VALUES (%s,%s, %s,%s,%s, %s,%s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s
             , %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getApellidoP(), "text"), 
            $this->GetSQLValueString($x->getApellidoM(), "text"), 
            $this->GetSQLValueString($x->getEdad(), "text"), 
            $this->GetSQLValueString($x->getEmail(), "text"), 
            $this->GetSQLValueString($x->getTel(), "text"),
            $this->GetSQLValueString($x->getCel(), "text"),
            $this->GetSQLValueString($x->getId_medio_ent(), "int"),
            $this->GetSQLValueString($x->getSeguimiento_ins(), "text"),
            $this->GetSQLValueString($x->getFecha_ins(), "date"),
            $this->GetSQLValueString($x->getFechaNac(), "date"),
            $this->GetSQLValueString($x->getSexo(), "text"),
            $this->GetSQLValueString($x->getId_dir(), "int"),
            $this->GetSQLValueString($x->getCurp_ins(), "text"),
            $this->GetSQLValueString($x->getNombreTutor(), "text"),
            $this->GetSQLValueString($x->getEmailTutor(), "text"),
            $this->GetSQLValueString($x->getTelTutor(), "text"),
            $this->GetSQLValueString($x->getUltGrado_est(), "text"),
            $this->GetSQLValueString($x->getEscuelaPro(), "text"),
            $this->GetSQLValueString($x->getMatricula(), "text"),
            $this->GetSQLValueString($x->getAlta_alum(), "date"),
            $this->GetSQLValueString($x->getBaja_alum(), "date"),
            $this->GetSQLValueString($x->getActivo_alum(), "int"),
            $this->GetSQLValueString($x->getPass_alum(), "text"),
            $this->GetSQLValueString($x->getRecoverPass(), "text"),
            $this->GetSQLValueString($x->getLastSession(), "date"),
            $this->GetSQLValueString($x->getImg_alum(), "text"),
            $this->GetSQLValueString($x->getTipo(), "int"),
            $this->GetSQLValueString($x->getMedioContacto(), "int"),
            $this->GetSQLValueString($x->getRefencia_pago(), "text"),
            $this->GetSQLValueString($x->getComentarios(), "text"),
            $this->GetSQLValueString($x->getTipo_sangre(), "text"),
            $this->GetSQLValueString($x->getAlergias(), "text"),
            $this->GetSQLValueString($x->getEnfermedades_cronicas(), "text"),
            $this->GetSQLValueString($x->getPreinscripciones_medicas(), "text"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId_usu_capturo(), "int"),
            $this->GetSQLValueString($x->getMatriculaSep(), "text"),
            $this->GetSQLValueString($x->getExpedienteSep(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Alumnos $x){
	    $query=sprintf("UPDATE inscripciones_ulm SET  
             Nombre_ins=%s, ApellidoP_ins=%s, ApellidoM_ins=%s, Edad_ins=%s, Email_ins=%s,
             Tel_ins=%s, Cel_ins=%s, Id_medio_ent=%s, Seguimiento_ins=%s, Fecha_ins=%s, FechaNac=%s, Sexo=%s, Id_dir=%s, Curp_ins=%s, NombreTutor=%s, EmailTutor=%s,
             TelTutor=%s, UltGrado_est=%s, EscuelaPro=%s, Matricula=%s, Alta_alum=%s, Baja_alum=%s, Activo_alum=%s, Pass_alum=%s, RecoverPass=%s, LastSession=%s
             ,Img_alum=%s, tipo=%s, medio_contacto=%s, Refencia_pago=%s, Comentarios=%s, Tipo_sangre=%s, Alergias=%s, Enfermedades_cronicas=%s, Preinscripciones_medicas=%s
             ,Id_plantel=%s, Id_usu_capturo=%s,MatriculaSep=%s, ExpedienteSep=%s  WHERE Id_ins=%s",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getApellidoP(), "text"), 
            $this->GetSQLValueString($x->getApellidoM(), "text"), 
            $this->GetSQLValueString($x->getEdad(), "text"), 
            $this->GetSQLValueString($x->getEmail(), "text"), 
            $this->GetSQLValueString($x->getTel(), "text"),
            $this->GetSQLValueString($x->getCel(), "text"),
            $this->GetSQLValueString($x->getId_medio_ent(), "int"),
            $this->GetSQLValueString($x->getSeguimiento_ins(), "text"),
            $this->GetSQLValueString($x->getFecha_ins(), "date"),
            $this->GetSQLValueString($x->getFechaNac(), "date"),
            $this->GetSQLValueString($x->getSexo(), "text"),
            $this->GetSQLValueString($x->getId_dir(), "int"),
            $this->GetSQLValueString($x->getCurp_ins(), "text"),
            $this->GetSQLValueString($x->getNombreTutor(), "text"),
            $this->GetSQLValueString($x->getEmailTutor(), "text"),
            $this->GetSQLValueString($x->getTelTutor(), "text"),
            $this->GetSQLValueString($x->getUltGrado_est(), "text"),
            $this->GetSQLValueString($x->getEscuelaPro(), "text"),
            $this->GetSQLValueString($x->getMatricula(), "text"),
            $this->GetSQLValueString($x->getAlta_alum(), "date"),
            $this->GetSQLValueString($x->getBaja_alum(), "date"),
            $this->GetSQLValueString($x->getActivo_alum(), "int"),
            $this->GetSQLValueString($x->getPass_alum(), "text"),
            $this->GetSQLValueString($x->getRecoverPass(), "text"),
            $this->GetSQLValueString($x->getLastSession(), "date"),
            $this->GetSQLValueString($x->getImg_alum(), "text"),
            $this->GetSQLValueString($x->getTipo(), "int"),
            $this->GetSQLValueString($x->getMedioContacto(), "int"),
            $this->GetSQLValueString($x->getRefencia_pago(), "text"),
            $this->GetSQLValueString($x->getComentarios(), "text"),
            $this->GetSQLValueString($x->getTipo_sangre(), "text"),
            $this->GetSQLValueString($x->getAlergias(), "text"),
            $this->GetSQLValueString($x->getEnfermedades_cronicas(), "text"),
            $this->GetSQLValueString($x->getPreinscripciones_medicas(), "text"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId_usu_capturo(), "int"),
            $this->GetSQLValueString($x->getMatriculaSep(), "text"),
            $this->GetSQLValueString($x->getExpedienteSep(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}

	public function delete($Id){
            $query = sprintf("UPDATE inscripciones_ulm SET Activo=0 WHERE Id_ins =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}

	
	public function show($Id){
	    $query="SELECT * FROM inscripciones_ulm WHERE Id_ins= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	//Todos los alumnos en el sistema
        //No importa si son egresados o no
        
	public function showAllAlumnos(){
            $resp=array();
            $query="SELECT * FROM inscripciones_ulm 
            JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
            WHERE Id_plantel=".$this->Id_plantel." AND tipo=1  AND Baja_ofe IS NULL 
            ORDER BY Id_ins DESC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
        
        public function getInteresados($search,$limit=100,$offset=null){
            $query="";
            if($search!=null){
               $query=$search;  
            }
            $queryLimit="";
            if($limit!=null){
                $queryLimit="LIMIT ".$limit;
                if($offset!=null){
                    $queryLimit=$queryLimit." OFFSET ".$offset;
                }
            }
            
            $resp=array();
            $query="SELECT * FROM inscripciones_ulm 
            LEFT JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
            WHERE Id_plantel=".$this->Id_plantel." AND tipo=0 
            ".$query."
            ORDER BY Id_ins DESC ".$queryLimit;
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $row_consulta);
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

        //Alumnos activos
        public function getAlumnos($activo=null,$search=null,$limit=null,$offset=null) {
            $query="IS NOT NULL"; //egresado
            if($activo==null){
               $query="IS NULL";  //activo
            }
            $querySearch="";
            if($search!=null){
               $querySearch=$search;  
            }
            $queryLimit="";
            if($limit!=null){
                $queryLimit="LIMIT ".$limit;
                if($offset!=null){
                    $queryLimit=$queryLimit." OFFSET ".$offset;
                }
            }
            $resp=array();
            $query="SELECT * FROM inscripciones_ulm 
                    JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
                    WHERE tipo=1  
                          AND inscripciones_ulm.Id_plantel=".$this->Id_plantel." 
                          AND Activo_oferta=1 
                          AND Baja_ofe ".$query." 
                          AND FechaCapturaEgreso ".$query."
                          AND IdCicloEgreso ".$query."
                          AND IdUsuEgreso ".$query." "
                          .$querySearch."
                    ORDER BY Id_ins DESC ".$queryLimit;
            $consulta=base::advanced_query($query);
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $row_consulta);
                   
               } while ( $row_consulta = $consulta->fetch_assoc());
            }
            return $resp;
        }


        //Alumnos egresados
        public function getAlumnosEgresados($search=null,$limit=null,$offset=null) {
            $query="";
            if($search!=null){
               $query=$search;  
            }
            $queryLimit="";
            if($limit!=null){
                $queryLimit="LIMIT ".$limit;
                if($offset!=null){
                    $queryLimit=$queryLimit." OFFSET ".$offset;
                }
            }
            
            $resp=array();
            $queryx="SELECT * FROM inscripciones_ulm 
                    JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
                    WHERE tipo=1  
                          AND inscripciones_ulm.Id_plantel=".$this->Id_plantel." 
                          AND Activo_oferta=1 
                          AND Baja_ofe IS NULL 
                          AND FechaCapturaEgreso IS NOT NULL 
                          AND IdUsuEgreso IS NOT NULL 
                          ".$query." ORDER BY Id_ins DESC ".$queryLimit;
            $consulta=base::advanced_query($queryx);
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $row_consulta);
                   
               } while ( $row_consulta = $consulta->fetch_assoc());
            }
            return $resp;
        }
        
        //Alumnos que se dieron de baja o se cambiaron de plan
        public function getAlumnosBajas($search=null,$limit=null,$offset=null) {
            $query="";
            if($search!=null){
               $query=$search;  
            }
            $queryLimit="";
            if($limit!=null){
                $queryLimit="LIMIT ".$limit;
                if($offset!=null){
                    $queryLimit=$queryLimit." OFFSET ".$offset;
                }
            }
            $resp=array();
             $query= "SELECT * FROM inscripciones_ulm 
                      JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
                      JOIN Motivos_bajas ON ofertas_alumno.Id_mot_baja=Motivos_bajas.Id_mot 
                      WHERE tipo=1  AND inscripciones_ulm.Id_plantel=".$this->Id_plantel."  AND Activo_oferta=0 ".$query." ORDER BY Id_ins DESC ".$queryLimit;
            $consulta=base::advanced_query($query);
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $row_consulta);
                   
               } while ( $row_consulta = $consulta->fetch_assoc());
            }
            return $resp;
        }
       
        
                                                
        /*
        //Alumnos que puedan o no tener ofertas cargadas
        //Se muentran tanto activos como egresados
        public function getAlumnosSistema() {
            $query="SELECT * FROM inscripciones_ulm 
                    LEFT JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
                    WHERE tipo=1  
                          AND inscripciones_ulm.Id_plantel=".$this->Id_plantel." 
                    ORDER BY Id_ins DESC";
            return $this->advanced_query($query);
        }
        */

	public function create_object($row){
            $x = new Alumnos();
            $x->setId($row['Id_ins']);
            $x->setNombre($row['Nombre_ins']);
            $x->setApellidoP($row['ApellidoP_ins']);
            $x->setApellidoM($row['ApellidoM_ins']);
            $x->setEdad($row['Edad_ins']);
            $x->setEmail($row['Email_ins']);
            $x->setTel($row['Tel_ins']);
            $x->setCel($row['Cel_ins']);
            $x->setId_medio_ent($row['Id_medio_ent']);
            $x->setSeguimiento_ins($row['Seguimiento_ins']);
            $x->setFecha_ins($row['Fecha_ins']);
            $x->setSexo($row['Sexo']);
            $x->setFechaNac($row['FechaNac']);
            $x->setId_dir($row['Id_dir']);
            $x->setCurp_ins($row['Curp_ins']);
            $x->setNombreTutor($row['NombreTutor']);
            $x->setEmailTutor($row['EmailTutor']);
            $x->setTelTutor($row['TelTutor']);
            $x->setUltGrado_est($row['UltGrado_est']);
            $x->setEscuelaPro($row['EscuelaPro']);
            $x->setMatricula($row['Matricula']);
            $x->setAlta_alum($row['Alta_alum']);
            $x->setBaja_alum($row['Baja_alum']);
            $x->setActivo_alum($row['Activo_alum']);
            $x->setPass_alum($row['Pass_alum']);
            $x->setRecoverPass($row['RecoverPass']);
            $x->setLastSession($row['LastSession']);
            $x->setImg_alum($row['Img_alum']);
            $x->setTipo($row['tipo']);
            $x->setMedioContacto($row['medio_contacto']);
            $x->setRefencia_pago($row['Refencia_pago']);
            $x->setComentarios($row['Comentarios']);
            $x->setTipo_sangre($row['Tipo_sangre']);
            $x->setAlergias($row['Alergias']);
            $x->setEnfermedades_cronicas($row['Enfermedades_cronicas']);
            $x->setPreinscripciones_medicas($row['Preinscripciones_medicas']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setId_usu_capturo($row['Id_usu_capturo']);
            $x->setMatriculaSep($row['MatriculaSep']);
            $x->setExpedienteSep($row['ExpedienteSep']);

            return $x;
	}
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }

            return $resp;
	}
        
        //http://stackoverflow.com/questions/13439817/why-is-my-mysqli-connection-so-slow
        /*
       public function buscarAlumno($buscar,$limit=20){
            $resp=array();
            $query = "SELECT *, CONCAT(
                IFNULL(REPLACE(Nombre_ins,' ',''),''),
                IFNULL(REPLACE(ApellidoP_ins,' ','') ,''),
                IFNULL(REPLACE(ApellidoM_ins,' ','') ,''),
                IFNULL(REPLACE(Matricula,' ','') ,'')) AS Buscar   
            FROM inscripciones_ulm 
            LEFT JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
            
            HAVING Buscar LIKE '%".str_replace(' ','',$buscar)."%' 
                    AND tipo=1 
                    AND Activo_oferta=1 
                    AND Baja_ofe IS NULL 
                    AND FechaCapturaEgreso IS NULL 
                    AND IdCicloEgreso IS NULL 
                    AND IdUsuEgreso IS NULL 
                    AND inscripciones_ulm.Id_plantel=".$this->Id_plantel." 
                    ORDER BY Id_ins DESC LIMIT ".$limit;
             return $this->advanced_query($query);
    }
        */
        
   public function buscarAlumno($buscar,$limit=20){
            $resp=array();
            $query = "SELECT *,
                CONCAT(Nombre_ins,ApellidoP_ins,ApellidoM_ins,Matricula) AS Buscar
            FROM inscripciones_ulm 
            LEFT JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
            WHERE  (Nombre_ins LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR ApellidoP_ins LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR ApellidoM_ins LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR Matricula LIKE '%".str_replace(' ','',$buscar)."%')
                    AND tipo=1 
                    AND Activo_oferta=1 
                    AND Baja_ofe IS NULL 
                    AND FechaCapturaEgreso IS NULL 
                    AND IdCicloEgreso IS NULL 
                    AND IdUsuEgreso IS NULL 
                    AND inscripciones_ulm.Id_plantel=".$this->Id_plantel." 
                    ORDER BY Id_ins DESC LIMIT ".$limit;
             return $this->advanced_query($query);
    }
    
    public function buscarAlumnosAltasYBajas($buscar,$limit=20){
           $query = "SELECT *
            FROM inscripciones_ulm 
            LEFT JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
            WHERE  (Nombre_ins LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR ApellidoP_ins LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR ApellidoM_ins LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR Matricula LIKE '%".str_replace(' ','',$buscar)."%')
                    AND tipo=1
                    AND inscripciones_ulm.Id_plantel=".$this->Id_plantel." 
                    ORDER BY Id_ins DESC LIMIT ".$limit;
        return $this->advanced_query($query);
    }
    
    
      public function getArchivosFaltantes($Id_ofe_alum){
             $DebeArchivos=0;
             $query = "SELECT Id_ofe_alum,Id_oferta,Nombre_oferta, archivos_ofertas_ulm.Id_file,Nombre_file, ArchivosAlum.* 
                 From ofertas_alumno
             JOIN ofertas_ulm ON ofertas_alumno.Id_ofe=ofertas_ulm.Id_oferta
             JOIN archivos_ofertas_ulm ON ofertas_ulm.Id_oferta=archivos_ofertas_ulm.Id_ofe
             JOIN archivos_ulm ON archivos_ofertas_ulm.Id_file=archivos_ulm.Id_file
             LEFT JOIN (SELECT Id_file AS ArchivoEntregado 
                               FROM archivos_alum_ulm 
                        WHERE Id_ofe_alum=".$Id_ofe_alum."
                        ) AS ArchivosAlum ON archivos_ulm.Id_file=ArchivosAlum.ArchivoEntregado
             WHERE Id_ofe_alum=".$Id_ofe_alum;
            $consulta=base::advanced_query($query);
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   if($row_consulta['ArchivoEntregado']==NULL){
                       $DebeArchivos++;
                   }
               } while ( $row_consulta = $consulta->fetch_assoc());
            }
            return $DebeArchivos;
        }

    
    public function getStatusOferta($Id_ofe_alum){
        $ban = 0;
        $base= new base();
        $MateriasCursadas = 0;
        $resp= array();
        $DaoOfertasAlumno= new DaoOfertasAlumno();
        $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
        $ofertaAlumno=$DaoOfertasAlumno->show($Id_ofe_alum);
        
        //Verificar las materias de la especialidad
        $numMatEsp=count($DaoMateriasEspecialidad->getMateriasEspecialidad($ofertaAlumno->getId_esp()));
        //Verificar el ultimo ciclo
        foreach ($DaoMateriasEspecialidad->getMateriasEspecialidad($ofertaAlumno->getId_esp()) as $k2 => $v2) {
            $query = "SELECT * FROM ofertas_alumno 
            JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
            JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum= materias_ciclo_ulm.Id_ciclo_alum
            JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
            JOIN materias_uml ON Materias_especialidades.Id_mat=materias_uml.Id_mat
            WHERE ofertas_alumno.Id_ofe_alum=" . $ofertaAlumno->getId() . " AND materias_ciclo_ulm.Id_mat_esp=" . $v2->getId();
            $consulta=$base->advanced_query($query);
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta > 0 && (($row_consulta['CalTotalParciales'] >= $row_consulta['Promedio_min']) || ($row_consulta['CalExtraordinario'] >= $row_consulta['Promedio_min']) || ($row_consulta['CalEspecial'] >= $row_consulta['Promedio_min']))) {
                $MateriasCursadas++;
            }
        }
        if(($numMatEsp==$MateriasCursadas) && $MateriasCursadas>0){
            $ban = 1;
        }
        $resp['status']=$ban;
        $resp['cant_mat_esp']=$numMatEsp;
        $resp['cant_mat_cursadas']=$MateriasCursadas;
        
        return $resp;
    }
    
    public function getHorarioAlumno($Id_ofe_alum,$Id_ciclo){
        $DaoHorarioDocente= new DaoHorarioDocente();
        $query="SELECT Horario_docente.*
                   FROM ciclos_alum_ulm 
            JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
            JOIN Horario_docente ON materias_ciclo_ulm.Id_grupo=Horario_docente.Id_grupo
            WHERE Id_ofe_alum=".$Id_ofe_alum."  
                  AND ciclos_alum_ulm.Id_ciclo=".$Id_ciclo."
                  AND Activo=1
                  ORDER BY Hora ASC";
        return $DaoHorarioDocente->advanced_query($query);
    }
    
    
    public function getValidarHoraDiaLibres($Id_ofe_alum,$Id_ciclo,$Id_grupo){
        $Id_horario=0;
        $DaoGrupos=new DaoGrupos(); 
        //Obtenemos el horario del alumno
        foreach ($this->getHorarioAlumno($Id_ofe_alum, $Id_ciclo) as $horarioAlumno){

             //Obtenemos el horario del grupo
            foreach($DaoGrupos->getHorarioGrupo($Id_grupo) as $horarioGrupo){
                //Validamos si no choca el horario del grupo que se quiere registrar
                //Con el horario del alumno
                 if($horarioAlumno->getHora()==$horarioGrupo->getHora() && $horarioAlumno->getLunes()==1 && $horarioGrupo->getLunes()==1){
                     return $horarioAlumno->getId();
                 }elseif($horarioAlumno->getHora()==$horarioGrupo->getHora() && $horarioAlumno->getMartes()==1 && $horarioGrupo->getMartes()==1){
                     return $horarioAlumno->getId();
                 }elseif($horarioAlumno->getHora()==$horarioGrupo->getHora() && $horarioAlumno->getMiercoles()==1 && $horarioGrupo->getMiercoles()==1){
                     return $horarioAlumno->getId();
                 }elseif($horarioAlumno->getHora()==$horarioGrupo->getHora() && $horarioAlumno->getJueves()==1 && $horarioGrupo->getJueves()==1){
                     return $horarioAlumno->getId();
                 }elseif($horarioAlumno->getHora()==$horarioGrupo->getHora() && $horarioAlumno->getViernes()==1 && $horarioGrupo->getViernes()==1){
                     return $horarioAlumno->getId();
                 }elseif($horarioAlumno->getHora()==$horarioGrupo->getHora() && $horarioAlumno->getSabado()==1 && $horarioGrupo->getSabado()==1){
                     return $horarioAlumno->getId();
                 }elseif($horarioAlumno->getHora()==$horarioGrupo->getHora() && $horarioAlumno->getDomingo()==1 && $horarioGrupo->getDomingo()==1){
                     return $horarioAlumno->getId();
                 }
            }
        }
        return $Id_horario;
    }
    
    public function getInfoDiaHorario($Id_ofe_alum,$Id_ciclo,$Id_hora){
        $resp=array();
        $resp['L']=array();
        $resp['M']=array();
        $resp['I']=array();
        $resp['J']=array();
        $resp['V']=array();
        $resp['S']=array();
        $DaoAulas= new DaoAulas();
        $DaoGrupos= new DaoGrupos();
        $DaoDocentes= new DaoDocentes();
        $DaoMaterias= new DaoMaterias();

        foreach($this->getHorarioAlumno($Id_ofe_alum,$Id_ciclo) as $Horario){
            $grupo=$DaoGrupos->show($Horario->getId_grupo());
            $aula=$DaoAulas->show($Horario->getAula());
            $docente=$DaoDocentes->show($Horario->getId_docente());
            $mat=$DaoMaterias->show($grupo->getId_mat());
                
             if($Id_hora==$Horario->getHora() && $Horario->getLunes()==1){
                $resp['L']['Grupo']=$grupo->getClave();
                $resp['L']['Aula']=$aula->getNombre_aula();
                $resp['L']['Docente']=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
                $resp['L']['Materia']=$mat->getNombre();
                        
             }
             if($Id_hora==$Horario->getHora() && $Horario->getMartes()==1){
                $resp['M']['Grupo']=$grupo->getClave();
                $resp['M']['Aula']=$aula->getNombre_aula();
                $resp['M']['Docente']=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
                $resp['M']['Materia']=$mat->getNombre();
                
             }
             if($Id_hora==$Horario->getHora() && $Horario->getMiercoles()==1){
                $resp['I']['Grupo']=$grupo->getClave();
                $resp['I']['Aula']=$aula->getNombre_aula();
                $resp['I']['Docente']=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
                $resp['I']['Materia']=$mat->getNombre();
                
             }
             if($Id_hora==$Horario->getHora() && $Horario->getJueves()==1){
                $resp['J']['Grupo']=$grupo->getClave();
                $resp['J']['Aula']=$aula->getNombre_aula();
                $resp['J']['Docente']=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
                $resp['J']['Materia']=$mat->getNombre();
                
             }
             if($Id_hora==$Horario->getHora() && $Horario->getViernes()==1){
                $resp['V']['Grupo']=$grupo->getClave();
                $resp['V']['Aula']=$aula->getNombre_aula();
                $resp['V']['Docente']=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
                $resp['V']['Materia']=$mat->getNombre();
                
             }
             if($Id_hora==$Horario->getHora() && $Horario->getSabado()==1){
                $resp['S']['Grupo']=$grupo->getClave();
                $resp['S']['Aula']=$aula->getNombre_aula();
                $resp['S']['Docente']=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
                $resp['S']['Materia']=$mat->getNombre();
             }
             if($Id_hora==$Horario->getHora() && $Horario->getDomingo()==1){
                $resp['D']['Grupo']=$grupo->getClave();
                $resp['D']['Aula']=$aula->getNombre_aula();
                $resp['D']['Docente']=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
                $resp['D']['Materia']=$mat->getNombre();
             }
        }
        return $resp;
    }
    
    
    public function validarAlumno($matricula,$pass){
	    $query="SELECT * FROM inscripciones_ulm WHERE Matricula= '".$matricula."' AND Pass_alum='".$this->hashPassword($pass)."' AND tipo=1 AND Alta_alum IS NOT NULL AND Baja_alum IS NULL";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
    }
    
    
    public function validarMatricula($matricula){
	    $query="SELECT * FROM inscripciones_ulm WHERE Matricula= '".$matricula."' AND tipo=1 AND Alta_alum IS NOT NULL AND Baja_alum IS NULL";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
    }
    
    public function validarEmail($email){
	    $query="SELECT * FROM inscripciones_ulm WHERE Email_ins= '".$email."' AND tipo=1 AND Alta_alum IS NOT NULL AND Baja_alum IS NULL";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
    }
    
    public function validarRecover($Recover){
	    $query="SELECT * FROM inscripciones_ulm WHERE RecoverPass= '".$Recover."'";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
    }
    
    public function getCantidadPagadaPorOfertaAlumno($Id_ofe_alum){
        $total=0;
        $DaoPagos=new DaoPagos();
        foreach($DaoPagos->getPagosOfertaAlumno($Id_ofe_alum) as $pago){
           $total+= $pago->getMonto_pp();
        }
        return $total;
    }
    
    public function getAdeudoOfertaAlumno($Id_ofe_alum,$qFechas=null){
        
        $qF="AND Fecha_pago<='".date('Y-m-d')."'";
        if($qFechas!=null){
           $qF=$qFechas;
        }
        $totalAdeudo = 0;
        $totaldecuentoPorPaquete=0;
        $DaoCiclosAlumno= new DaoCiclosAlumno();
        $DaoRecargos= new DaoRecargos();
        $DaoPagosCiclo= new DaoPagosCiclo();
        $DaoPaquetesDescuentosAlumno= new DaoPaquetesDescuentosAlumno();
        foreach ($DaoCiclosAlumno->getCiclosOferta($Id_ofe_alum) as $cicloAlumno) {
            
                $getPorcentaje=0;
                if($cicloAlumno->getId_ciclo()>0){
                    //verificar si tiene un descuento por paquete de descuento y descontar
                    $descuentoPaqueteCiclo=$DaoPaquetesDescuentosAlumno->getPaqueteDescuentoCicloAlumno($cicloAlumno->getId_ciclo(),$Id_ofe_alum);
                    $getPorcentaje=$descuentoPaqueteCiclo->getPorcentaje();
                }

                $query = "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=".$cicloAlumno->getId()." ".$qF." AND DeadCargo IS NULL  AND  DeadCargoUsu IS NULL ORDER BY Id_pago_ciclo ASC";
                foreach($DaoPagosCiclo->advanced_query($query) as $cargo){
                    $recargos = 0;
                    foreach($DaoRecargos->getRecargosPago($cargo->getId()) as $recargo){
                        $recargos+=$recargo->getMonto_recargo();
                    }
                    $cargo=round($cargo->getMensualidad() - $cargo->getDescuento() - $cargo->getDescuento_beca() + $recargos);
                    $totaldecuentoPorPaquete+=($cargo*$getPorcentaje)/100;
                    $totalAdeudo+= round($cargo);
                }

        }   
        $adeudo=$totalAdeudo-$this->getCantidadPagadaPorOfertaAlumno($Id_ofe_alum)-$totaldecuentoPorPaquete;
        //$adeudo=$totaldecuentoPorPaquete;
        
        return $adeudo;
    }    
    
    
}



/*
 * HOrario del alumno
 * SELECT Horario_docente.Id_grupo,Horario_docente.Hora,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado
       FROM ciclos_alum_ulm 
JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
JOIN Horario_docente ON materias_ciclo_ulm.Id_grupo=Horario_docente.Id_grupo
WHERE Id_ofe_alum=169  
      AND ciclos_alum_ulm.Id_ciclo=13
      AND Activo=1
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
SELECT 
       ciclos_alum_ulm.Id_ciclo,
       Materias.*,
       materias_ciclo_ulm.*
       FROM inscripciones_ulm 
JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
JOIN especialidades_ulm ON ofertas_alumno.Id_esp=especialidades_ulm.Id_esp
JOIN (SELECT Materias_especialidades.* 
          FROM Materias_especialidades
          JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat 
      WHERE Activo_mat_esp=1 AND materias_uml.Activo_mat=1
      ) AS Materias ON especialidades_ulm.Id_esp=Materias.Id_esp
JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
WHERE tipo=1  
      AND inscripciones_ulm.Id_plantel=1
      AND Activo_oferta=1 
      AND Baja_ofe IS NULL 
      AND FechaCapturaEgreso IS NULL
      AND IdCicloEgreso IS NULL
      AND IdUsuEgreso IS NULL
      AND Id_ins=145
ORDER BY Id_ins
 *  */