<?php
require_once 'modelos/base.php';
require_once 'modelos/Eventos.php';

class DaoEventos extends base{
	public function add(Eventos $x){
	  $query=sprintf("INSERT INTO Eventos (Nombre, Comentarios, DateCreated, Id_salon, Start, End,Id_usu, Id_ciclo,Id_plantel,Activo,Costo,TipoEvento) VALUES (%s, %s,%s, %s,%s, %s,%s, %s,%s,%s,%s,%s)",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getComentarios(), "text"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_salon(), "int"), 
            $this->GetSQLValueString($x->getStart(), "text"), 
            $this->GetSQLValueString($x->getEnd(), "text"), 
            $this->GetSQLValueString($x->getId_usu(), "int"),
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getActivo(), "text"),
            $this->GetSQLValueString($x->getCosto(), "double"),
            $this->GetSQLValueString($x->getTipoEvento(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Eventos $x){
	    $query=sprintf("UPDATE Eventos SET  Nombre=%s, Comentarios=%s, Id_salon=%s, Start=%s, End=%s, Id_ciclo=%s, Costo=%s,TipoEvento=%s WHERE Id_event=%s",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getComentarios(), "text"), 
            $this->GetSQLValueString($x->getId_salon(), "int"), 
            $this->GetSQLValueString($x->getStart(), "text"), 
            $this->GetSQLValueString($x->getEnd(), "text"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getCosto(), "double"),
            $this->GetSQLValueString($x->getTipoEvento(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE Eventos SET Activo=0 WHERE Id_event =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Eventos WHERE Id_event= ".$Id." ORDER BY Id_event DESC";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Eventos WHERE Activo=1 AND Id_plantel=".$this->Id_plantel;
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new Eventos();
            $x->setId($row['Id_event']);
            $x->setNombre($row['Nombre']);
            $x->setComentarios($row['Comentarios']);
            $x->setDateCreated($row['DateCreated']);
            $x->setId_salon($row['Id_salon']);
            $x->setStart($row['Start']);
            $x->setEnd($row['End']);
            $x->setId_usu($row['Id_usu']);
            $x->setNombreUtiliza($row['NombreUtiliza']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setActivo($row['Activo']);
            $x->setCosto($row['Costo']);
            $x->setTipoEvento($row['TipoEvento']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getEventosCiclo($Id_ciclo){
            $query = "SELECT * FROM Eventos WHERE Activo=1 AND Id_plantel=".$this->Id_plantel." AND Id_ciclo= ".$Id_ciclo." ORDER BY Start DESC";
            return $this->advanced_query($query);
        }
        
        public function getEventosCicloTipo($Id_ciclo,$tipo){
            $query = "SELECT * FROM Eventos WHERE Activo=1 AND Id_plantel=".$this->Id_plantel." AND Id_ciclo= ".$Id_ciclo." AND (TipoEvento='ambos' OR TipoEvento='".$tipo."') ORDER BY Start DESC";
            return $this->advanced_query($query);
        }
        
}
