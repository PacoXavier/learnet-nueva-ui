<?php
require_once 'modelos/base.php';
require_once 'modelos/PermisosExtra.php';

class DaoPermisosExtra extends base{
        public $table="permisos_extra_usu";
        
	public function add(PermisosExtra $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_per, Id_usu) VALUES (%s, %s)",
            $this->GetSQLValueString($x->getId_per(), "int"), 
            $this->GetSQLValueString($x->getId_usu(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(PermisosExtra $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Id_per=%s, Id_usu=%s WHERE Id_per_ext=%s",
            $this->GetSQLValueString($x->getId_per(), "int"), 
            $this->GetSQLValueString($x->getId_usu(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM ".$this->table."  WHERE Id_per_ext =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
       public function deletePermisosUsuario($Id_usu){
            $query = sprintf("DELETE FROM ".$this->table."  WHERE Id_usu =".$Id_usu); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_per_ext= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new PermisosExtra();
            $x->setId($row['Id_per_ext']);
            $x->setId_per($row['Id_per']);
            $x->setId_usu($row['Id_usu']);
            return $x;
	}
        
	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


        public function getPermisosUsuario($Id_usu){
             $query= "SELECT * FROM ".$this->table." WHERE  Id_usu=".$Id_usu;
             return $this->advanced_query($query);
        }
}
