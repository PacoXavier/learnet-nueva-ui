<?php
require_once 'modelos/base.php';
require_once 'modelos/PreguntasRespondidas.php';

class DaoPreguntasRespondidas extends base{
    
        public $table="PreguntasRespondidas";
    
	public function add(PreguntasRespondidas $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_enc_alum, Id_pre, Id_opcion, DateCreated) VALUES (%s, %s,%s, %s)",
            $this->GetSQLValueString($x->getId_enc_alum(), "int"), 
            $this->GetSQLValueString($x->getId_pre(), "int"), 
            $this->GetSQLValueString($x->getId_opcion(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(PreguntasRespondidas $x){
	    $query=sprintf("UPDATE ".$this->table." SET Id_enc_alum=%s, Id_pre=%s, Id_opcion=%s, DateCreated=%s WHERE Id_res=%s",
            $this->GetSQLValueString($x->getId_enc_alum(), "int"), 
            $this->GetSQLValueString($x->getId_pre(), "int"), 
            $this->GetSQLValueString($x->getId_opcion(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE ".$this->table." SET Activo=0 WHERE Id_res =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_res= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	public function create_object($row){
            $x = new PreguntasRespondidas();
            $x->setId($row['Id_res']);
            $x->setId_enc_alum($row['Id_enc_alum']);
            $x->setId_pre($row['Id_pre']);
            $x->setId_opcion($row['Id_opcion']);
            $x->setDateCreated($row['DateCreated']);
            return $x;
	}

                
	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        
        public function getRespuestasEncuesta($Id_enc_alum){
            $query="SELECT *FROM ".$this->table." WHERE Id_enc_alum=".$Id_enc_alum;
            return $this->advanced_query($query);
        }


}
