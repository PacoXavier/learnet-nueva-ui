<?php
require_once 'modelos/base.php';
require_once 'modelos/PagosCiclo.php';
require_once 'DaoCiclosAlumno.php';
require_once 'DaoPagos.php';
require_once 'DaoAlumnos.php';
class DaoPagosCiclo extends base{

	public function add(PagosCiclo $x){
              $query=sprintf("INSERT INTO Pagos_ciclo (Folio,Concepto,Fecha_pago,Mensualidad,Descuento,Cantidad_Pagada, Fecha_pagado, Id_ciclo_alum, tipo_pago, Id_mis, Id_alum, Descuento_beca, DeadCargo, DeadCargoUsu, FechaCapturaBeca, UsuCapturaBeca, FechaCapturaDescuento, UsuCapturaDescuento,Pagado,IdRel,TipoRel, UsuCapturaCargo, FechaCapturaCargo) VALUES (%s ,%s, %s, %s, %s, %s,%s ,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getFolio(), "text"),
              $this->GetSQLValueString($x->getConcepto(), "text"),
              $this->GetSQLValueString($x->getFecha_pago(), "date"),
              $this->GetSQLValueString($x->getMensualidad(), "double"),
              $this->GetSQLValueString($x->getDescuento(), "double"),
              $this->GetSQLValueString($x->getCantidad_Pagada(), "double"),
              $this->GetSQLValueString($x->getFecha_pagado(), "date"),
              $this->GetSQLValueString($x->getId_ciclo_alum(), "int"),
              $this->GetSQLValueString($x->getTipo_pago(), "text"),
              $this->GetSQLValueString($x->getId_mis(), "int"),
              $this->GetSQLValueString($x->getId_alum(), "int"),
              $this->GetSQLValueString($x->getDescuento_beca(), "double"),
              $this->GetSQLValueString($x->getDeadCargo(), "date"),
              $this->GetSQLValueString($x->getDeadCargoUsu(), "int"),
              $this->GetSQLValueString($x->getFechaCapturaBeca(), "date"),
              $this->GetSQLValueString($x->getUsuCapturaBeca(), "int"),
              $this->GetSQLValueString($x->getFechaCapturaDescuento(), "date"),
              $this->GetSQLValueString($x->getUsuCapturaDescuento(), "int"),
              $this->GetSQLValueString($x->getPagado(), "int"),
              $this->GetSQLValueString($x->getIdRel(), "int"),
              $this->GetSQLValueString($x->getTipoRel(), "text"),
              $this->GetSQLValueString($x->getUsuCapturaCargo(), "int"),
              $this->GetSQLValueString($x->getFechaCapturaCargo(), "date"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(PagosCiclo $x){
	    $query=sprintf("UPDATE Pagos_ciclo SET Folio=%s, Concepto=%s, Fecha_pago=%s,Mensualidad=%s,Descuento=%s, Cantidad_Pagada=%s,Fecha_pagado=%s, Id_ciclo_alum=%s,tipo_pago=%s,Id_mis=%s, Id_alum=%s, Descuento_beca=%s,DeadCargo=%s, DeadCargoUsu=%s, FechaCapturaBeca=%s, UsuCapturaBeca=%s, FechaCapturaDescuento=%s, UsuCapturaDescuento=%s, Pagado=%s, IdRel=%s,TipoRel=%s, UsuCapturaCargo=%s, FechaCapturaCargo=%s   WHERE Id_pago_ciclo = %s",
              $this->GetSQLValueString($x->getFolio(), "text"),
              $this->GetSQLValueString($x->getConcepto(), "text"),
              $this->GetSQLValueString($x->getFecha_pago(), "date"),
              $this->GetSQLValueString($x->getMensualidad(), "double"),
              $this->GetSQLValueString($x->getDescuento(), "double"),
              $this->GetSQLValueString($x->getCantidad_Pagada(), "double"),
              $this->GetSQLValueString($x->getFecha_pagado(), "date"),
              $this->GetSQLValueString($x->getId_ciclo_alum(), "int"),
              $this->GetSQLValueString($x->getTipo_pago(), "text"),
              $this->GetSQLValueString($x->getId_mis(), "int"),
              $this->GetSQLValueString($x->getId_alum(), "int"),
              $this->GetSQLValueString($x->getDescuento_beca(), "double"),
              $this->GetSQLValueString($x->getDeadCargo(), "date"),
              $this->GetSQLValueString($x->getDeadCargoUsu(), "int"),
              $this->GetSQLValueString($x->getFechaCapturaBeca(), "date"),
              $this->GetSQLValueString($x->getUsuCapturaBeca(), "int"),
              $this->GetSQLValueString($x->getFechaCapturaDescuento(), "date"),
              $this->GetSQLValueString($x->getUsuCapturaDescuento(), "int"),
              $this->GetSQLValueString($x->getPagado(), "int"),
              $this->GetSQLValueString($x->getIdRel(), "int"),
              $this->GetSQLValueString($x->getTipoRel(), "text"),
              $this->GetSQLValueString($x->getUsuCapturaCargo(), "int"),
              $this->GetSQLValueString($x->getFechaCapturaCargo(), "date"),
              $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}
	
	

	public function delete($Id){
                $query = sprintf("UPDATE Pagos_ciclo SET DeadCargo='".date('Y-m-d H:i:s')."', DeadCargoUsu=".$_COOKIE['admin/Id_usu']." WHERE Id_pago_ciclo=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
        
        public function deleteTrue($Id){
                $query = sprintf("DELETE FROM Pagos_ciclo  WHERE Id_pago_ciclo=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}

	
	public function show($Id){
	    $query="SELECT * FROM Pagos_ciclo WHERE Id_pago_ciclo= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

        public function getPagoByIdCicloAlumn($Id_ciclo_alum){
	    $query="SELECT * FROM  Pagos_ciclo WHERE Id_ciclo_alum=" . $Id_ciclo_alum . " AND Concepto='Mensualidad' LIMIT 1";
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
               throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Pagos_ciclo JOIN inscripciones_ulm ON Pagos_ciclo.Id_alum=inscripciones_ulm.Id_ins
             WHERE Id_plantel=".$this->Id_plantel." AND DeadCargo IS NULL AND  DeadCargoUsu IS NULL";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
	
	
	
	public function create_object($row){
            $x = new PagosCiclo();
            $x->setId($row['Id_pago_ciclo']);
            $x->setFolio($row['Folio']);
            $x->setConcepto($row['Concepto']);
            $x->setFecha_pago($row['Fecha_pago']); 
            $x->setMensualidad($row['Mensualidad']);
            $x->setDescuento($row['Descuento']); 
            $x->setCantidad_Pagada($row['Cantidad_Pagada']); 
            $x->setFecha_pagado($row['Fecha_pagado']); 
            $x->setId_ciclo_alum($row['Id_ciclo_alum']); 
            $x->setTipo_pago($row['tipo_pago']); 
            $x->setId_mis($row['Id_mis']); 
            $x->setId_alum($row['Id_alum']); 
            $x->setDescuento_beca($row['Descuento_beca']); 
            $x->setDeadCargo($row['DeadCargo']); 
            $x->setDeadCargoUsu($row['DeadCargoUsu']); 
            $x->setFechaCapturaBeca($row['FechaCapturaBeca']); 
            $x->setUsuCapturaBeca($row['UsuCapturaBeca']); 
            $x->setFechaCapturaDescuento($row['FechaCapturaDescuento']); 
            $x->setUsuCapturaDescuento($row['UsuCapturaDescuento']); 
            $x->setPagado($row['Pagado']);
            $x->setIdRel($row['IdRel']);
            $x->setTipoRel($row['TipoRel']);
            $x->setUsuCapturaCargo($row['UsuCapturaCargo']);
            $x->setFechaCapturaCargo($row['FechaCapturaCargo']);
            return $x;
	}

	public function advanced_query($query){
		    $resp=array();
		    $consulta=$this->_cnn->query($query);
		    if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	}
        
        public function getPagoInscripcionCiclo($Id_ciclo_alum){
	    $query= "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=".$Id_ciclo_alum." AND Concepto='Inscripción' AND DeadCargo IS NULL AND  DeadCargoUsu IS NULL";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function verificarPagoInscripcion($Id_ofe_alum){
          //Obtener el primer ciclo de la oferta para verificar si ya pago la inscripcion
          $existe=0;
          $DaoCiclosAlumno= new DaoCiclosAlumno();
          $primerCiclo=$DaoCiclosAlumno->getPrimerCicloOferta($Id_ofe_alum);
          $cargo=$this->getPagoInscripcionCiclo($primerCiclo->getId());
          if($cargo->getId()>0){
              $DaoAlumnos= new DaoAlumnos();
              $total=$DaoAlumnos->getCantidadPagadaPorOfertaAlumno($Id_ofe_alum);
              $mensualidad=$cargo->getMensualidad()-$cargo->getDescuento()-$cargo->getDescuento_beca();
              if($total>=$mensualidad){
                  $existe=1;
              }
          }else{
              //Se puede dar el caso que no tenga inscripcion ya que esta puede ser cargada solo una vez cada año
              //Y para este caso solo verificamos si ya
              $NumCargos=$this->getCargosCiclo($primerCiclo->getId());
              if($NumCargos>0){
                 $existe=1; 
              }
          }
          return $existe;
        }
        
        public function getCargosCiclo($Id_ciclo_Alum){
            $resp=array();
            $query= "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=".$Id_ciclo_Alum." AND DeadCargo IS NULL AND  DeadCargoUsu IS NULL ORDER BY Fecha_pago ASC";
            foreach($this->advanced_query($query) as $k=>$v){
                array_push($resp, $v);
            }
            return $resp;
        } 
        
        
        public function getCargosUsuario($Id,$Tipo){
            $query= "SELECT * FROM Pagos_ciclo WHERE IdRel=".$Id." AND TipoRel='".$Tipo."' AND DeadCargo IS NULL AND  DeadCargoUsu IS NULL ORDER BY Fecha_pago ASC";
            return $this->advanced_query($query);
        } 
        
        public function perteneceCargoAOfertaAlumno($Id_ofe_alum,$Id_pago_ciclo){
	    $query= "SELECT * FROM Pagos_ciclo 
                    JOIN ciclos_alum_ulm ON Pagos_ciclo.Id_ciclo_alum=ciclos_alum_ulm.Id_ciclo_alum
                    JOIN ofertas_alumno ON ciclos_alum_ulm.Id_ofe_alum=ofertas_alumno.Id_ofe_alum
                    WHERE ofertas_alumno.Id_ofe_alum=".$Id_ofe_alum." 
                          AND Pagos_ciclo.Id_pago_ciclo=".$Id_pago_ciclo."
                          AND Pagos_ciclo.DeadCargo IS NULL 
                          AND Pagos_ciclo.DeadCargoUsu IS NULL";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        
        public function getLastCargoCiclo($Id){
	    $query="SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum= ".$Id." AND DeadCargo IS NULL AND  DeadCargoUsu IS NULL ORDER BY Id_pago_ciclo DESC LIMIT 1";
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        
        public function getPagoIdCicloAlum($Id){
	    $query="SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum= ".$Id." ORDER BY Id_ciclo_alum ASC LIMIT 1";
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function existeQuery($query) {
            $Result1 = $this->_cnn->query($query);
            if (!$Result1) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            } else {
                return $this->create_object($Result1->fetch_assoc());
            }
        }
        
}




