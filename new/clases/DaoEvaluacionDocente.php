<?php
require_once 'modelos/base.php';
require_once 'modelos/EvaluacionDocente.php';
require_once 'DaoOfertasPorCategoria.php';
require_once 'DaoCategoriasPago.php';
require_once 'DaoPuntosPorCategoriaEvaluadaDocente.php';
require_once 'DaoNivelesPago.php';
 
class DaoEvaluacionDocente extends base{
    
        public $table="Evaluacion_docente";
    
	public function add(EvaluacionDocente $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_doc, Fecha_evaluacion, Id_usu_evaluador,Id_ciclo,Comentarios,Puntos_totales) VALUES (%s, %s,%s,%s,%s,%s)",
            $this->GetSQLValueString($x->getId_doc(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_usu_evaluador(), "int"),
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getComentarios(), "text"),
            $this->GetSQLValueString($x->getPuntosTotales(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(EvaluacionDocente $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Id_doc=%s, Fecha_evaluacion=%s, Id_usu_evaluador=%s,Id_ciclo=%s,Comentarios=%s, Puntos_totales=%s  WHERE Id_eva=%s",
            $this->GetSQLValueString($x->getId_doc(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_usu_evaluador(), "int"),
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getComentarios(), "text"),
            $this->GetSQLValueString($x->getPuntosTotales(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	
	public function delete($Id){
            $query = sprintf("DELETE FROM  ".$this->table."  WHERE Id_eva =%s",
            $this->GetSQLValueString($Id, "int")); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	public function show($Id){
	    $query=sprintf("SELECT * FROM ".$this->table." WHERE Id_eva=%s",
            $this->GetSQLValueString($Id, "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new EvaluacionDocente();
            $x->setId($row['Id_eva']);
            $x->setId_doc($row['Id_doc']);
            $x->setDateCreated($row['Fecha_evaluacion']);
            $x->setId_usu_evaluador($row['Id_usu_evaluador']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setComentarios($row['Comentarios']);
            $x->setPuntosTotales($row['Puntos_totales']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getEvaluaciones(){
            $query="SELECT * FROM ".$this->table." ORDER BY Id_eva ASC";
            return $this->advanced_query($query);
        }
        
        public function getEvaluacionesDocentes($Id_docen){
            $query="SELECT * FROM ".$this->table." WHERE Id_doc=".$Id_docen."  ORDER BY Id_eva DESC";
            return $this->advanced_query($query);
        }
        
        public function getUltimaEvaluacionDocente($Id_doc){
	    $query="SELECT * FROM ".$this->table." WHERE Id_doc=".$Id_doc." ORDER BY Id_eva DESC LIMIT 1";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function getEvaluacionPorciclo($Id_doc,$Id_ciclo){
	    $query="SELECT * FROM ".$this->table." WHERE Id_doc=".$Id_doc." AND Id_ciclo=".$Id_ciclo." ORDER BY Id_eva DESC LIMIT 1";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        
        public function getInfoEvaluacion($Id_docen,$Id_ofe){
            $resp=array();
            
            $DaoPuntosPorCategoriaEvaluadaDocente= new DaoPuntosPorCategoriaEvaluadaDocente();
            $DaoOfertasPorCategoria= new DaoOfertasPorCategoria();
            $DaoCategoriasPago= new DaoCategoriasPago();
            $DaoNivelesPago= new DaoNivelesPago();
            
            //Obtenemos la ultima evaluacion del docente
            $evaluacion = $this->getUltimaEvaluacionDocente($Id_docen);
            if ($evaluacion->getId() > 0) {
                $ofertaPorCategoria = $DaoOfertasPorCategoria->getCategoriaPorOferta($Id_ofe);
                $CategoriaTabulador = $DaoCategoriasPago->show($ofertaPorCategoria->getId_tipo());
                if($CategoriaTabulador->getId()>0){
                    $resp['Tipo'] = $CategoriaTabulador->getNombre_tipo();
                    $resp['Puntos'] = $evaluacion->getPuntosTotales();
                    $resul = $DaoPuntosPorCategoriaEvaluadaDocente->getNivelPorCategoria($evaluacion->getId(), $CategoriaTabulador->getId());
                    if ($resul->getId() > 0) {
                        $nivel = $DaoNivelesPago->show($resul->getId_nivel());
                        $resp['NombreNivel'] = $nivel->getNombre_nivel();
                        $resp['PagoHora'] = $nivel->getPagoNivel();
                    }
                }
            }
            return $resp;
        }
}
