<?php
require_once 'modelos/base.php';
require_once 'modelos/AnalisisSicologico.php';

class DaoAnalisisSicologico extends base{

	public function add(AnalisisSicologico $x){
              $query=sprintf("INSERT INTO AnalisiSicologico (Id_alum,DateCreated,Id_camp, Valor_camp, Id_usu) VALUES (%s ,%s, %s, %s, %s)",
              $this->GetSQLValueString($x->getId_alum(), "int"),
              $this->GetSQLValueString($x->getDateCreated(), "date"),
              $this->GetSQLValueString($x->getId_camp(), "int"),
              $this->GetSQLValueString($x->getValor_camp(), "int"),
              $this->GetSQLValueString($x->getId_usu(), "int"));
              $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(AnalisisSicologico $x){
	    $query=sprintf("UPDATE AnalisiSicologico SET Id_alum=%s ,DateCreated=%s,Id_camp=%s, Valor_camp=%s, Id_usu=%s WHERE Id = %s",
            $this->GetSQLValueString($x->getId_alum(), "int"),
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getId_camp(), "int"),
            $this->GetSQLValueString($x->getValor_camp(), "int"),
            $this->GetSQLValueString($x->getId_usu(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
                $query = sprintf("DELETE FROM AnalisiSicologico WHERE Id=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
        
        public function deleteByAlumno($Id){
                $query = sprintf("DELETE FROM AnalisiSicologico WHERE Id_alum=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
        
	
	public function show($Id){
	    $query="SELECT * FROM AnalisiSicologico WHERE Id= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function showByAlum($Id_alum){
	    $query="SELECT * FROM AnalisiSicologico WHERE Id_alum= ".$Id_alum;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showByAlumCampo($Id_Alum,$Campo){
            $resp=array();
            $query="SELECT * FROM AnalisiSicologico WHERE Id_alum=".$Id_Alum." AND Id_camp=".$Campo;
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 return $this->create_object($consulta->fetch_assoc());
            }
            return $resp;
	}
        
        public function create_object($row){
            $x = new AnalisisSicologico();
            $x->setId($row['Id']);
            $x->setId_alum($row['Id_alum']);
            $x->setDateCreated($row['DateCreated']); 
            $x->setId_camp($row['Id_camp']); 
            $x->setValor_camp($row['Valor_camp']); 
            $x->setId_usu($row['Id_usu']); 
            return $x;
	}
        

        public function advanced_query($query){
		    $resp=array();
		    $consulta=$this->_cnn->query($query);
		    if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	}

}

