<?php

require_once 'modelos/base.php';
require_once 'modelos/Grupos.php';
require_once 'DaoCiclos.php';
require_once 'DaoDiasInhabiles.php';
require_once 'DaoHorarioDocente.php';
require_once 'DaoAsistencias.php';
require_once 'DaoJustificaciones.php';

class DaoGrupos extends base {

    public function add(Grupos $x) {
        $query = sprintf("INSERT INTO Grupos (Clave, Turno, Activo_grupo, Id_ciclo, Id_mat, Id_plantel,Capacidad, Id_mat_esp,Id_ori) VALUES (%s, %s,%s, %s,%s, %s,%s, %s,%s)", 
                $this->GetSQLValueString($x->getClave(), "text"), 
                $this->GetSQLValueString($x->getTurno(), "int"), 
                $this->GetSQLValueString($x->getActivo_grupo(), "int"), 
                $this->GetSQLValueString($x->getId_ciclo(), "int"), 
                $this->GetSQLValueString($x->getId_mat(), "int"), 
                $this->GetSQLValueString($x->getId_plantel(), "int"), 
                $this->GetSQLValueString($x->getCapacidad(), "int"), 
                $this->GetSQLValueString($x->getId_mat_esp(), "int"), 
                $this->GetSQLValueString($x->getId_ori(), "int"));
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function update(Grupos $x) {
        $query = sprintf("UPDATE Grupos SET  Clave=%s, Turno=%s, Activo_grupo=%s, Id_ciclo=%s, Id_mat=%s, Id_plantel=%s,Capacidad=%s, Id_mat_esp=%s,Id_ori=%s WHERE Id_grupo=%s", $this->GetSQLValueString($x->getClave(), "text"), $this->GetSQLValueString($x->getTurno(), "int"), $this->GetSQLValueString($x->getActivo_grupo(), "int"), $this->GetSQLValueString($x->getId_ciclo(), "int"), $this->GetSQLValueString($x->getId_mat(), "int"), $this->GetSQLValueString($x->getId_plantel(), "int"), $this->GetSQLValueString($x->getCapacidad(), "int"), $this->GetSQLValueString($x->getId_mat_esp(), "int"), $this->GetSQLValueString($x->getId_ori(), "int"), $this->GetSQLValueString($x->getId(), "int"));
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id) {
        $query = sprintf("UPDATE Grupos SET Activo_grupo=0 WHERE Id_grupo =" . $Id);
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return true;
        }
    }

    public function show($Id) {
        $query = "SELECT * FROM Grupos WHERE Id_grupo= " . $Id . " ORDER BY Id_grupo DESC";
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->create_object($Result1->fetch_assoc());
        }
    }

    public function showGruposByCiclo($Id_ciclo) {
        $resp = array();
        $query = "SELECT * FROM Grupos WHERE Activo_grupo=1 AND Id_ciclo=" . $Id_ciclo . " AND Id_plantel=" . $this->Id_plantel . " ORDER BY Id_grupo DESC";
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
        }
        $totalRows_consulta = $consulta->num_rows;
        if ($totalRows_consulta > 0) {
            do {
                array_push($resp, $this->create_object($row_consulta));
            } while ($row_consulta = $consulta->fetch_assoc());
        }
        return $resp;
    }

    public function create_object($row) {
        $x = new Grupos();
        $x->setId($row['Id_grupo']);
        $x->setClave($row['Clave']);
        $x->setTurno($row['Turno']);
        $x->setActivo_grupo($row['Activo_grupo']);
        $x->setId_ciclo($row['Id_ciclo']);
        $x->setId_mat($row['Id_mat']);
        $x->setId_plantel($row['Id_plantel']);
        $x->setCapacidad($row['Capacidad']);
        $x->setId_mat_esp($row['Id_mat_esp']);
        $x->setId_ori($row['Id_ori']);

        return $x;
    }

    public function advanced_query($query) {
        $resp = array();
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                do {
                    array_push($resp, $this->create_object($row_consulta));
                } while ($row_consulta = $consulta->fetch_assoc());
            }
        }
        return $resp;
    }

    public function getGruposByCiclo($Id_ciclo) {
        $query= "SELECT * FROM Grupos WHERE Id_ciclo=" . $Id_ciclo . " AND Id_plantel=" . $this->Id_plantel . " AND Activo_grupo=1 ORDER BY Id_grupo DESC";
        return $this->advanced_query($query);
    }
    
    
    public function getDisponibilidadGrupo($Id_grupo){
        $grupo=$this->show($Id_grupo);
        $totalRows_disponibilidad=count($this->getAlumnosBYGrupo($Id_grupo));
        return ($grupo->getCapacidad()*1) - ($totalRows_disponibilidad*1);                       
    }

    public function getAlumnosBYGrupo($Id_grupo) {
        $resp = array();
        if ($Id_grupo > 0) {
            $query = "SELECT * FROM ofertas_alumno 
                        JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
                        JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
                        JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                        JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
                        WHERE  materias_ciclo_ulm.Id_grupo=" . $Id_grupo . " AND Activo_oferta=1";
            $consulta = $this->_cnn->query($query);
            if (!$consulta) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            } else {
                $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                do {
                    array_push($resp, $row_consulta);
                } while ($row_consulta = $consulta->fetch_assoc());
            }
        }
        return $resp;
    }

    public function getAlumnosGrupo($Id_grupo) {
        $resp = array();
        if ($Id_grupo > 0) {
            $query = "SELECT * FROM ofertas_alumno 
                    JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
                    JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
                    JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                    JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
                    WHERE  materias_ciclo_ulm.Id_grupo=" . $Id_grupo . " AND Activo_oferta=1";
            $consulta = $this->_cnn->query($query);
            if (!$consulta) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            } else {
                $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                do {
                    $alumno_grupo = array();
                    $alumno_grupo['Id_ofe_alum'] = $row_consulta['Id_ofe_alum'];
                    $alumno_grupo['Id_ciclo_alum'] = $row_consulta['Id_ciclo_alum'];
                    $alumno_grupo['Id_ciclo_mat'] = $row_consulta['Id_ciclo_mat'];
                    $alumno_grupo['Id_grupo'] = $row_consulta['Id_grupo'];
                    $alumno_grupo['Id_alum'] = $row_consulta['Id_alum'];
                    $alumno_grupo['CalTotalParciales'] = $row_consulta['CalTotalParciales'];
                    $alumno_grupo['CalExtraordinario'] = $row_consulta['CalExtraordinario'];
                    $alumno_grupo['CalEspecial'] = $row_consulta['CalEspecial'];
                    $alumno_grupo['Evaluacion_extraordinaria'] = $row_consulta['Evaluacion_extraordinaria'];
                    $alumno_grupo['Evaluacion_especial'] = $row_consulta['Evaluacion_especial'];
                    $alumno_grupo['NombreDiferente'] = $row_consulta['NombreDiferente'];
                    array_push($resp, $alumno_grupo);
                } while ($row_consulta = $consulta->fetch_assoc());
            }
        }
        return $resp;
    }

    public function getDocenteGrupo($Id_grupo) {
        $query = "SELECT * FROM Horario_docente WHERE Id_grupo= " . $Id_grupo;
        $consulta = $this->_cnn->query($query);
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            return $row_consulta;
        }
    }

    public function buscarGrupoCiclo($buscar, $Id_ciclo, $limit = 20) {
        $resp = array();
        $query = "SELECT * FROM Grupos 
                 JOIN materias_uml ON Grupos.Id_mat=materias_uml.Id_mat
                     WHERE (Clave LIKE  '%" . $buscar . "%'  OR Nombre LIKE '%" . $buscar . "%')
                            AND Activo_grupo=1 
                            AND Id_ciclo=" . $Id_ciclo . " 
                            AND Grupos.Id_plantel=" . $this->Id_plantel . " ORDER BY Id_grupo DESC LIMIT " . $limit;
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                do {
                    array_push($resp, $this->create_object($row_consulta));
                } while ($row_consulta = $consulta->fetch_assoc());
            }
        }
        return $resp;
    }

    function getDiasClaseGrupo($Id_grupo, $Id_ciclo, $Id_docente = null, $FechaIni = null, $FechaFin = null) {
        $query = "";
        if ($Id_docente != null) {
            $query = " AND Id_docente=" . $Id_docente;
        }
        $DaoCiclos = new DaoCiclos;
        $DaoDiasInhabiles = new DaoDiasInhabiles();
        $base = new base();

        $ciclo = $DaoCiclos->show($Id_ciclo);

        //Obtenemos los dias inhabiles del ciclo
        $diasInhabiles = array();
        foreach ($DaoDiasInhabiles->getDiasInhabilesCiclo($ciclo->getId()) as $k => $v) {
            array_push($diasInhabiles, $v->getFecha_inh());
        }

        $Dias_docen = array();
        $claseGrupo['dias'] = array();
        $claseGrupo['horas']['L'] = array();
        $claseGrupo['horas']['M'] = array();
        $claseGrupo['horas']['I'] = array();
        $claseGrupo['horas']['J'] = array();
        $claseGrupo['horas']['V'] = array();
        $claseGrupo['horas']['S'] = array();
        $claseGrupo['horas']['D'] = array();
        $query = "SELECT * FROM Horario_docente JOIN Horas ON Horario_docente.Hora=Horas.Id_hora WHERE Id_grupo= " . $Id_grupo . " " . $query . " ORDER BY Id_hora ASC";
        foreach ($base->advanced_query($query) as $k => $v) {
            //Insertamos solo los dias del grupo
            if ($v['Lunes'] == 1 && !in_array("L", $Dias_docen)) {
                array_push($Dias_docen, "L");
            }
            if ($v['Martes'] == 1 && !in_array("M", $Dias_docen)) {
                array_push($Dias_docen, "M");
            }
            if ($v['Miercoles'] == 1 && !in_array("I", $Dias_docen)) {
                array_push($Dias_docen, "I");
            }
            if ($v['Jueves'] == 1 && !in_array("J", $Dias_docen)) {
                array_push($Dias_docen, "J");
            }
            if ($v['Viernes'] == 1 && !in_array("V", $Dias_docen)) {
                array_push($Dias_docen, "V");
            }
            if ($v['Sabado'] == 1 && !in_array("S", $Dias_docen)) {
                array_push($Dias_docen, "S");
            }

            if ($v['Domingo'] == 1 && !in_array("D", $Dias_docen)) {
                array_push($Dias_docen, "D");
            }
            //Insertamos las horas que da el profesor
            if ($v['Lunes'] == 1) {
                $Hora = array();
                $Hora['Id'] = $v['Id_hora'];
                $Hora['Hora'] = $v['Texto_hora'];
                array_push($claseGrupo['horas']['L'], $Hora);
            }
            if ($v['Martes'] == 1) {
                $Hora = array();
                $Hora['Id'] = $v['Id_hora'];
                $Hora['Hora'] = $v['Texto_hora'];
                array_push($claseGrupo['horas']['M'], $Hora);
            }
            if ($v['Miercoles'] == 1) {
                $Hora = array();
                $Hora['Id'] = $v['Id_hora'];
                $Hora['Hora'] = $v['Texto_hora'];
                array_push($claseGrupo['horas']['I'], $Hora);
            }
            if ($v['Jueves'] == 1) {
                $Hora = array();
                $Hora['Id'] = $v['Id_hora'];
                $Hora['Hora'] = $v['Texto_hora'];
                array_push($claseGrupo['horas']['J'], $Hora);
            }
            if ($v['Viernes'] == 1) {
                $Hora = array();
                $Hora['Id'] = $v['Id_hora'];
                $Hora['Hora'] = $v['Texto_hora'];
                array_push($claseGrupo['horas']['V'], $Hora);
            }
            if ($v['Sabado'] == 1) {
                $Hora = array();
                $Hora['Id'] = $v['Id_hora'];
                $Hora['Hora'] = $v['Texto_hora'];
                array_push($claseGrupo['horas']['S'], $Hora);
            }
            if ($v['Domingo'] == 1) {
                $Hora = array();
                $Hora['Id'] = $v['Id_hora'];
                $Hora['Hora'] = $v['Texto_hora'];
                array_push($claseGrupo['horas']['D'], $Hora);
            }
        }
        //Fecha de inicio y fin de ciclo
        $Fini = strtotime($ciclo->getFecha_ini());
        $Ffin = strtotime($ciclo->getFecha_fin());
        if ($FechaIni != null) {
            $Fini = strtotime($FechaIni);
        }
        if ($FechaFin != null) {
            $Ffin = strtotime($FechaFin);
        }

        $i = 0;
        while ($Ffin >= strtotime("+" . $i . " day", $Fini)) {
            //Eliminamos los dias domingo
            if (date("N", strtotime("+" . $i . " day", $Fini)) == 1) {
                $dia = "L";
            } elseif (date("N", strtotime("+" . $i . " day", $Fini)) == 2) {
                $dia = "M";
            } elseif (date("N", strtotime("+" . $i . " day", $Fini)) == 3) {
                $dia = "I";
            } elseif (date("N", strtotime("+" . $i . " day", $Fini)) == 4) {
                $dia = "J";
            } elseif (date("N", strtotime("+" . $i . " day", $Fini)) == 5) {
                $dia = "V";
            } elseif (date("N", strtotime("+" . $i . " day", $Fini)) == 6) {
                $dia = "S";
            } elseif (date("N", strtotime("+" . $i . " day", $Fini)) == 7) {
                $dia = "D";
            }
            $dateNormal = date("Y-m-d", strtotime("+" . $i . " day", $Fini));
            //Eliminamos los dias domingo
            //Eliminamos los dias inhabiles del ciclo,
            //Solo mostramos los dias que de el profesor o el grupo sino se pasa como parametro el Id_doce
            if (date("N", strtotime("+" . $i . " day", $Fini)) != 7 && !in_array($dateNormal, $diasInhabiles) && in_array($dia, $Dias_docen)) {
                array_push($claseGrupo['dias'], $dateNormal);
            }
            $i++;
        }
        return $claseGrupo;
    }

    public function getHorasDiaGrupo($Id_grupo, $Id_docente = null) {
        $DaoHorarioDocente = new DaoHorarioDocente();
        $resp = array();

        $query = "";
        if ($Id_docente != null) {
            $query = " AND Id_docente=" . $Id_docente;
        }
        $Lunes = 0;
        $Martes = 0;
        $Miercoles = 0;
        $Jueves = 0;
        $Viernes = 0;
        $Sabado = 0;
        $Domingo=0;
        $query = "SELECT * FROM Horario_docente WHERE Id_grupo=" . $Id_grupo . " " . $query." GROUP BY Id_grupo, Hora, Aula,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado";
        foreach ($DaoHorarioDocente->advanced_query($query) as $horario) {
            if ($horario->getLunes() == 1) {
                $Lunes++;
            }
            if ($horario->getMartes() == 1) {
                $Martes++;
            }
            if ($horario->getMiercoles() == 1) {
                $Miercoles++;
            }
            if ($horario->getJueves() == 1) {
                $Jueves++;
            }
            if ($horario->getViernes() == 1) {
                $Viernes++;
            }
            if ($horario->getSabado() == 1) {
                $Sabado++;
            }
            if ($horario->getDomingo() == 1) {
                $Domingo++;
            }
            $resp['L'] = $Lunes;
            $resp['M'] = $Martes;
            $resp['I'] = $Miercoles;
            $resp['J'] = $Jueves;
            $resp['V'] = $Viernes;
            $resp['S'] = $Sabado;
            $resp['D'] = $Domingo;
        }
        return $resp;
    }
    
    
    public function getHorarioGrupo($Id_grupo){
        $DaoHorarioDocente= new DaoHorarioDocente();
        $query="select * FROM Horario_docente WHERE Id_grupo=".$Id_grupo;
        return $DaoHorarioDocente->advanced_query($query);
    }
    
    public function getAsistenciasGrupoAlum($Id_grupo, $Id_alum) {
          $resp=array();
          $Asis = 0;
          $Faltas = 0;
          $Just = 0;
          $DaoAsistencias= new DaoAsistencias();
          $DaoJustificaciones= new DaoJustificaciones();
          $grupo=$this->show($Id_grupo);
          
          //Si se justifica una falta espor todo el dia y para todos los grupos
          $arrayJustificaciones=array();
          foreach ($DaoJustificaciones->getJustificacionByGrupoAlum($Id_alum, $grupo->getId_ciclo(),$Id_grupo) as $jus) {
              array_push($arrayJustificaciones, $jus->getDia_justificado());
               $Just++;
          }
          $query = "SELECT * FROM Asistencias WHERE Id_grupo=" . $Id_grupo . " AND Id_alum=" . $Id_alum;
          foreach($DaoAsistencias->advanced_query($query) as $asistencia){
              if ($asistencia->getAsistio() == 1) {
                   //Si el dia no se encuentra entre los justificados se toma como asistencia
                   if(!in_array($asistencia->getFecha_asis(), $arrayJustificaciones)){
                      $Asis++; 
                   }else{
                      $Faltas++; 
                   }
              } elseif($asistencia->getAsistio() == 0) {
                   $Faltas++;
              }  
          }

          
          $resp['Asistencias']=$Asis;
          $resp['Faltas']=$Faltas;
          $resp['Justificaciones']=$Just;
          return $resp;

    }
    
    public function getGruposByMateria($Id_mat,$Id_ciclo){
        $query  = "SELECT * FROM Grupos WHERE Id_mat=".$Id_mat." AND Id_plantel=".$this->Id_plantel." AND Id_ciclo=".$Id_ciclo." AND Activo_grupo=1 ORDER BY Id_grupo DESC";
        return $this->advanced_query($query);
    }

    
    public function getTotalAlumnosGrupo($Id_grupo) {
        $totalRows_consulta = 0;
        if ($Id_grupo > 0) {
            $query = "SELECT * FROM ofertas_alumno 
                        JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
                        JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
                        JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                        JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
                        WHERE  materias_ciclo_ulm.Id_grupo=" . $Id_grupo . " AND Activo_oferta=1";
            $consulta = $this->_cnn->query($query);
            if (!$consulta) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            } else {
                $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta = $consulta->num_rows;
        }
        return $totalRows_consulta;
    }
}

