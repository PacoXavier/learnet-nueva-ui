<?php

class PagosAdelantados {
    public $Id;
    public $Fecha_pago;
    public $Monto_pago;
    public $Saldo;
    public $Id_usu;
    public $Id_ofe_alum;
    public $Id_alum;
    public $Ids_pagos;
    public $Id_met_pago;
    public $Id_ciclo_usar;
    public $Id_paq_des;
    public $DateCreated;
    public $Id_plantel;
    public $Id_pp;
    
    function __construct() {
        $this->Id = "";
        $this->Fecha_pago = "";
        $this->Monto_pago = "";
        $this->Saldo = "";
        $this->Id_usu = "";
        $this->Id_ofe_alum = "";
        $this->Id_alum = "";
        $this->Ids_pagos = "";
        $this->Id_met_pago = "";
        $this->Id_ciclo_usar = "";
        $this->Id_paq_des = "";
        $this->DateCreated = "";
        $this->Id_plantel = "";
        $this->Id_pp = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getFecha_pago() {
        return $this->Fecha_pago;
    }

    public function getMonto_pago() {
        return $this->Monto_pago;
    }

    public function getSaldo() {
        return $this->Saldo;
    }

    public function getId_usu() {
        return $this->Id_usu;
    }

    public function getId_ofe_alum() {
        return $this->Id_ofe_alum;
    }

    public function getId_alum() {
        return $this->Id_alum;
    }

    public function getIds_pagos() {
        return $this->Ids_pagos;
    }

    public function getId_met_pago() {
        return $this->Id_met_pago;
    }

    public function getId_ciclo_usar() {
        return $this->Id_ciclo_usar;
    }

    public function getId_paq_des() {
        return $this->Id_paq_des;
    }

    public function getDateCreated() {
        return $this->DateCreated;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function getId_pp() {
        return $this->Id_pp;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setFecha_pago($Fecha_pago) {
        $this->Fecha_pago = $Fecha_pago;
    }

    public function setMonto_pago($Monto_pago) {
        $this->Monto_pago = $Monto_pago;
    }

    public function setSaldo($Saldo) {
        $this->Saldo = $Saldo;
    }

    public function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    public function setId_ofe_alum($Id_ofe_alum) {
        $this->Id_ofe_alum = $Id_ofe_alum;
    }

    public function setId_alum($Id_alum) {
        $this->Id_alum = $Id_alum;
    }

    public function setIds_pagos($Ids_pagos) {
        $this->Ids_pagos = $Ids_pagos;
    }

    public function setId_met_pago($Id_met_pago) {
        $this->Id_met_pago = $Id_met_pago;
    }

    public function setId_ciclo_usar($Id_ciclo_usar) {
        $this->Id_ciclo_usar = $Id_ciclo_usar;
    }

    public function setId_paq_des($Id_paq_des) {
        $this->Id_paq_des = $Id_paq_des;
    }

    public function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    public function setId_pp($Id_pp) {
        $this->Id_pp = $Id_pp;
    }



    
}
