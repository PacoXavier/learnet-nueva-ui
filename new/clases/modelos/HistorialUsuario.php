<?php
class HistorialUsuario {
    
    public $Id;
    public $Texto;
    public $DateCreated;
    public $Id_usu;
    public $Categoria;
    public $Id_plantel;
    public $Id_ciclo;
    
    function __construct() {
        $this->Id = "";
        $this->Texto = "";
        $this->DateCreated = "";
        $this->Id_usu = "";
        $this->Categoria = "";
        $this->Id_plantel = "";
        $this->Id_ciclo = "";
    }
   
    
    public function getId_ciclo() {
        return $this->Id_ciclo;
    }

    public function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

        
    public function getId() {
        return $this->Id;
    }

    public function getTexto() {
        return $this->Texto;
    }

    public function getDateCreated() {
        return $this->DateCreated;
    }

    public function getId_usu() {
        return $this->Id_usu;
    }

    public function getCategoria() {
        return $this->Categoria;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setTexto($Texto) {
        $this->Texto = $Texto;
    }

    public function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    public function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    public function setCategoria($Categoria) {
        $this->Categoria = $Categoria;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }



    
    
}
