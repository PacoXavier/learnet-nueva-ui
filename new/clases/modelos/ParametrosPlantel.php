<?php

class ParametrosPlantel {
    public $Id;
    public $Id_param;
    public $Id_plantel;
    public $Valor;

    function __construct() {
        $this->Id = "";
        $this->Id_param = "";
        $this->Id_plantel = "";
        $this->Valor = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getId_param() {
        return $this->Id_param;
    }

    function getId_plantel() {
        return $this->Id_plantel;
    }

    function getValor() {
        return $this->Valor;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_param($Id_param) {
        $this->Id_param = $Id_param;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    function setValor($Valor) {
        $this->Valor = $Valor;
    }



    
    
}
