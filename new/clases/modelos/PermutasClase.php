<?php

class PermutasClase {

    public $Id;
    public $Id_grupo;
    public $Id_docente_asignado;
    public $Dia;
    public $DateCreated;
    public $Id_usu;
    public $Id_ciclo;
    public $Comentario;
    public $Id_hora;
    public $DiaNoCubierto;
    public $Id_aula;

    function __construct() {
        $this->Id = "";
        $this->Id_grupo = "";
        $this->Id_docente_asignado = "";
        $this->Dia = "";
        $this->DateCreated = "";
        $this->Id_usu = "";
        $this->Id_ciclo = "";
        $this->Comentario = "";
        $this->Id_hora = "";
        $this->DiaNoCubierto = "";
        $this->Id_aula = "";
    }
    
    function getId_aula() {
        return $this->Id_aula;
    }

    function setId_aula($Id_aula) {
        $this->Id_aula = $Id_aula;
    }

        
    function getDiaNoCubierto() {
        return $this->DiaNoCubierto;
    }

    function setDiaNoCubierto($DiaNoCubierto) {
        $this->DiaNoCubierto = $DiaNoCubierto;
    }

        
    function getId_hora() {
        return $this->Id_hora;
    }

    function setId_hora($Id_hora) {
        $this->Id_hora = $Id_hora;
    }
    
    function getComentario() {
        return $this->Comentario;
    }

    function setComentario($Comentario) {
        $this->Comentario = $Comentario;
    }

        
    function getId() {
        return $this->Id;
    }

    function getId_grupo() {
        return $this->Id_grupo;
    }

    function getId_docente_asignado() {
        return $this->Id_docente_asignado;
    }

    function getDia() {
        return $this->Dia;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function getId_usu() {
        return $this->Id_usu;
    }

    function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_grupo($Id_grupo) {
        $this->Id_grupo = $Id_grupo;
    }

    function setId_docente_asignado($Id_docente_asignado) {
        $this->Id_docente_asignado = $Id_docente_asignado;
    }

    function setDia($Dia) {
        $this->Dia = $Dia;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

}
