<?php
class ArchivosAlumno {
    
    
    public $Id;
    public $Id_file;
    public $Id_alum;
    public $Id_ofe;
    public $Id_ofe_alum;
    public $Id_file_copy;
    public $Id_file_original;
    public $Id_usu;
    public $Fecha_recibido;
    
    function __construct() {
        $this->Id = "";
        $this->Id_file = "";
        $this->Id_alum = "";
        $this->Id_ofe = "";
        $this->Id_ofe_alum = "";
        $this->Id_file_copy = "";
        $this->Id_file_original = "";
        $this->Id_usu = "";
        $this->Fecha_recibido="";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getId_file() {
        return $this->Id_file;
    }

    public function getId_alum() {
        return $this->Id_alum;
    }

    public function getId_ofe() {
        return $this->Id_ofe;
    }

    public function getId_ofe_alum() {
        return $this->Id_ofe_alum;
    }

    public function getId_file_copy() {
        return $this->Id_file_copy;
    }

    public function getId_file_original() {
        return $this->Id_file_original;
    }

    public function getId_usu() {
        return $this->Id_usu;
    }

    public function getFecha_recibido() {
        return $this->Fecha_recibido;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_file($Id_file) {
        $this->Id_file = $Id_file;
    }

    public function setId_alum($Id_alum) {
        $this->Id_alum = $Id_alum;
    }

    public function setId_ofe($Id_ofe) {
        $this->Id_ofe = $Id_ofe;
    }

    public function setId_ofe_alum($Id_ofe_alum) {
        $this->Id_ofe_alum = $Id_ofe_alum;
    }

    public function setId_file_copy($Id_file_copy) {
        $this->Id_file_copy = $Id_file_copy;
    }

    public function setId_file_original($Id_file_original) {
        $this->Id_file_original = $Id_file_original;
    }

    public function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    public function setFecha_recibido($Fecha_recibido) {
        $this->Fecha_recibido = $Fecha_recibido;
    }

}
