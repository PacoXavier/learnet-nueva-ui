<?php

class Evaluaciones {
    
    public $Id;
    public $Ponderacion;
    public $Id_mat_esp;
    public $Nombre_eva;
    public $Activo_eva;

    function __construct() {
        $this->Id = "";
        $this->Ponderacion = "";
        $this->Id_mat_esp ="";
        $this->Nombre_eva = "";
        $this->Activo_eva = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getPonderacion() {
        return $this->Ponderacion;
    }

    function getId_mat_esp() {
        return $this->Id_mat_esp;
    }

    function getNombre_eva() {
        return $this->Nombre_eva;
    }

    function getActivo_eva() {
        return $this->Activo_eva;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setPonderacion($Ponderacion) {
        $this->Ponderacion = $Ponderacion;
    }

    function setId_mat_esp($Id_mat_esp) {
        $this->Id_mat_esp = $Id_mat_esp;
    }

    function setNombre_eva($Nombre_eva) {
        $this->Nombre_eva = $Nombre_eva;
    }

    function setActivo_eva($Activo_eva) {
        $this->Activo_eva = $Activo_eva;
    }

}
