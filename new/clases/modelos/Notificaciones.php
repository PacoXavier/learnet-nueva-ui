<?php
class Notificaciones {
    
    public $Id;
    public $Titulo;
    public $Texto;
    public $DateCreated;
    public $DateInit;
    public $Id_usu;
    public $Id_plantel;
    public $Tipo;
    public $Tipos_usu;
    public $Activo;

    function __construct() {
        $this->Id = "";
        $this->Titulo = "";
        $this->Texto = "";
        $this->DateCreated = "";
        $this->DateInit = "";
        $this->Id_usu = "";
        $this->Id_plantel = "";
        $this->Tipo = "";
        $this->Tipos_usu = "";
        $this->Activo = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getTitulo() {
        return $this->Titulo;
    }

    function getTexto() {
        return $this->Texto;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function getDateInit() {
        return $this->DateInit;
    }

    function getId_usu() {
        return $this->Id_usu;
    }

    function getId_plantel() {
        return $this->Id_plantel;
    }

    function getTipo() {
        return $this->Tipo;
    }

    function getTipos_usu() {
        return $this->Tipos_usu;
    }

    function getActivo() {
        return $this->Activo;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setTitulo($Titulo) {
        $this->Titulo = $Titulo;
    }

    function setTexto($Texto) {
        $this->Texto = $Texto;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    function setDateInit($DateInit) {
        $this->DateInit = $DateInit;
    }

    function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    function setTipo($Tipo) {
        $this->Tipo = $Tipo;
    }

    function setTipos_usu($Tipos_usu) {
        $this->Tipos_usu = $Tipos_usu;
    }

    function setActivo($Activo) {
        $this->Activo = $Activo;
    }



}
