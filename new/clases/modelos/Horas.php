<?php
class Horas {
    
    public $Id;
    public $Texto_hora;

    function __construct() {
        $this->Id = "";
        $this->Texto_hora = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getTexto_hora() {
        return $this->Texto_hora;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setTexto_hora($Texto_hora) {
        $this->Texto_hora = $Texto_hora;
    }

}
