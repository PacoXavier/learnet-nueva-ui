<?php

class MateriasDocente {
    
    public $Id;
    public $Id_materia;
    public $Id_profesor;
    public $Id_ciclo;
    public $Id_orientacion;
    
    function __construct() {
        $this->Id = "";
        $this->Id_materia = "";
        $this->Id_profesor = "";
        $this->Id_ciclo = "";
        $this->Id_orientacion = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getId_materia() {
        return $this->Id_materia;
    }

    function getId_profesor() {
        return $this->Id_profesor;
    }

    function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function getId_orientacion() {
        return $this->Id_orientacion;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_materia($Id_materia) {
        $this->Id_materia = $Id_materia;
    }

    function setId_profesor($Id_profesor) {
        $this->Id_profesor = $Id_profesor;
    }

    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    function setId_orientacion($Id_orientacion) {
        $this->Id_orientacion = $Id_orientacion;
    }



    

}
