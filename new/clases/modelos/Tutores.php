<?php
class Tutores{
    
    public $Id;
    public $Nombre;
    public $ApellidoP;
    public $ApellidoM;
    public $Parentesco;
    public $Id_ins;
    public $Tel;
    public $Cel;
    public $Email;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->ApellidoP = "";
        $this->ApellidoM = "";
        $this->Parentesco = "";
        $this->Id_ins = "";
        $this->Tel = "";
        $this->Cel = "";
        $this->Email = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getApellidoP() {
        return $this->ApellidoP;
    }

    function getApellidoM() {
        return $this->ApellidoM;
    }

    function getParentesco() {
        return $this->Parentesco;
    }

    function getId_ins() {
        return $this->Id_ins;
    }

    function getTel() {
        return $this->Tel;
    }

    function getCel() {
        return $this->Cel;
    }

    function getEmail() {
        return $this->Email;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setApellidoP($ApellidoP) {
        $this->ApellidoP = $ApellidoP;
    }

    function setApellidoM($ApellidoM) {
        $this->ApellidoM = $ApellidoM;
    }

    function setParentesco($Parentesco) {
        $this->Parentesco = $Parentesco;
    }

    function setId_ins($Id_ins) {
        $this->Id_ins = $Id_ins;
    }

    function setTel($Tel) {
        $this->Tel = $Tel;
    }

    function setCel($Cel) {
        $this->Cel = $Cel;
    }

    function setEmail($Email) {
        $this->Email = $Email;
    }



}