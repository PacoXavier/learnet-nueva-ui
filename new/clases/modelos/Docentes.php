<?php

class Docentes {
    public $Id;
    public $Clave_docen;
    public $Nombre_docen;
    public $ApellidoP_docen;
    public $ApellidoM_docen;
    public $Telefono_docen;
    public $Email_docen;
    public $NivelEst_docen;
    public $Last_session;
    public $Pass_docen;
    public $Baja_docen;
    public $Alta_docent;
    public $Img_docen;
    public $Doc_RFC;
    public $Doc_CURP;
    public $Doc_Curriculum;
    public $Doc_CompDomicilio;
    public $Doc_Contrato;
    public $Doc_CompEstudios;
    public $Cel_docen;
    public $copy_RFC;
    public $copy_Curriculum;
    public $copy_CompDomicilio;
    public $copy_Contrato;
    public $copy_CompEstudios;
    public $copy_CURP;
    public $Tipo_sangre;
    public $Alergias;
    public $Enfermedades_cronicas;
    public $Preinscripciones_medicas;
    public $Id_plantel;
    public $Id_dir;
    public $Recover_docen;
    public $UltimaGeneracion_credencial;
    public $Tiempo_completo;
    public $FechaNacimiento;
    public $Sexo;
    public $Curp;
    
    function __construct() {
        $this->Id = "";
        $this->Clave_docen = "";
        $this->Nombre_docen = "";
        $this->ApellidoP_docen = "";
        $this->ApellidoM_docen = "";
        $this->Telefono_docen = "";
        $this->Email_docen;
        $this->NivelEst_docen = "";
        $this->Last_session = "";
        $this->Pass_docen = "";
        $this->Baja_docen = "";
        $this->Alta_docent = "";
        $this->Img_docen = "";
        $this->Doc_RFC = "";
        $this->Doc_CURP = "";
        $this->Doc_Curriculum = "";
        $this->Doc_CompDomicilio = "";
        $this->Doc_Contrato = "";
        $this->Doc_CompEstudios = "";
        $this->Cel_docen = "";
        $this->copy_RFC = "";
        $this->copy_Curriculum = "";
        $this->copy_CompDomicilio = "";
        $this->copy_Contrato = "";
        $this->copy_CompEstudios ="";
        $this->copy_CURP = "";
        $this->Tipo_sangre = "";
        $this->Alergias = "";
        $this->Enfermedades_cronicas = "";
        $this->Preinscripciones_medicas = "";
        $this->Id_plantel = "";
        $this->Id_dir = "";
        $this->Recover_docen = "";
        $this->UltimaGeneracion_credencial = "";
        $this->Tiempo_completo = "";
        $this->FechaNacimiento = "";
        $this->Sexo = "";
        $this->Curp = "";

    }
    
    function getCurp() {
        return $this->Curp;
    }

    function setCurp($Curp) {
        $this->Curp = $Curp;
    }

    function getFechaNacimiento() {
        return $this->FechaNacimiento;
    }

    function getSexo() {
        return $this->Sexo;
    }

    function setFechaNacimiento($FechaNacimiento) {
        $this->FechaNacimiento = $FechaNacimiento;
    }

    function setSexo($Sexo) {
        $this->Sexo = $Sexo;
    }
 
    public function getId_dir() {
        return $this->Id_dir;
    }

    public function setId_dir($Id_dir) {
        $this->Id_dir = $Id_dir;
    }

        public function getEmail_docen() {
        return $this->Email_docen;
    }

    public function setEmail_docen($Email_docen) {
        $this->Email_docen = $Email_docen;
    }

        public function getId() {
        return $this->Id;
    }

    public function getClave_docen() {
        return $this->Clave_docen;
    }

    public function getNombre_docen() {
        return $this->Nombre_docen;
    }

    public function getApellidoP_docen() {
        return $this->ApellidoP_docen;
    }

    public function getApellidoM_docen() {
        return $this->ApellidoM_docen;
    }

    public function getTelefono_docen() {
        return $this->Telefono_docen;
    }

    public function getNivelEst_docen() {
        return $this->NivelEst_docen;
    }

    public function getLast_session() {
        return $this->Last_session;
    }

    public function getPass_docen() {
        return $this->Pass_docen;
    }

    public function getBaja_docen() {
        return $this->Baja_docen;
    }

    public function getAlta_docent() {
        return $this->Alta_docent;
    }

    public function getImg_docen() {
        return $this->Img_docen;
    }

    public function getDoc_RFC() {
        return $this->Doc_RFC;
    }

    public function getDoc_CURP() {
        return $this->Doc_CURP;
    }

    public function getDoc_Curriculum() {
        return $this->Doc_Curriculum;
    }

    public function getDoc_CompDomicilio() {
        return $this->Doc_CompDomicilio;
    }

    public function getDoc_Contrato() {
        return $this->Doc_Contrato;
    }

    public function getDoc_CompEstudios() {
        return $this->Doc_CompEstudios;
    }

    public function getCel_docen() {
        return $this->Cel_docen;
    }

    public function getCopy_RFC() {
        return $this->copy_RFC;
    }

    public function getCopy_Curriculum() {
        return $this->copy_Curriculum;
    }

    public function getCopy_CompDomicilio() {
        return $this->copy_CompDomicilio;
    }

    public function getCopy_Contrato() {
        return $this->copy_Contrato;
    }

    public function getCopy_CompEstudios() {
        return $this->copy_CompEstudios;
    }

    public function getCopy_CURP() {
        return $this->copy_CURP;
    }

    public function getTipo_sangre() {
        return $this->Tipo_sangre;
    }

    public function getAlergias() {
        return $this->Alergias;
    }

    public function getEnfermedades_cronicas() {
        return $this->Enfermedades_cronicas;
    }

    public function getPreinscripciones_medicas() {
        return $this->Preinscripciones_medicas;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function getRecover_docen() {
        return $this->Recover_docen;
    }

    public function getUltimaGeneracion_credencial() {
        return $this->UltimaGeneracion_credencial;
    }

    public function getTiempo_completo() {
        return $this->Tiempo_completo;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setClave_docen($Clave_docen) {
        $this->Clave_docen = $Clave_docen;
    }

    public function setNombre_docen($Nombre_docen) {
        $this->Nombre_docen = $Nombre_docen;
    }

    public function setApellidoP_docen($ApellidoP_docen) {
        $this->ApellidoP_docen = $ApellidoP_docen;
    }

    public function setApellidoM_docen($ApellidoM_docen) {
        $this->ApellidoM_docen = $ApellidoM_docen;
    }

    public function setTelefono_docen($Telefono_docen) {
        $this->Telefono_docen = $Telefono_docen;
    }

    public function setNivelEst_docen($NivelEst_docen) {
        $this->NivelEst_docen = $NivelEst_docen;
    }

    public function setLast_session($Last_session) {
        $this->Last_session = $Last_session;
    }

    public function setPass_docen($Pass_docen) {
        $this->Pass_docen = $Pass_docen;
    }

    public function setBaja_docen($Baja_docen) {
        $this->Baja_docen = $Baja_docen;
    }

    public function setAlta_docent($Alta_docent) {
        $this->Alta_docent = $Alta_docent;
    }

    public function setImg_docen($Img_docen) {
        $this->Img_docen = $Img_docen;
    }

    public function setDoc_RFC($Doc_RFC) {
        $this->Doc_RFC = $Doc_RFC;
    }

    public function setDoc_CURP($Doc_CURP) {
        $this->Doc_CURP = $Doc_CURP;
    }

    public function setDoc_Curriculum($Doc_Curriculum) {
        $this->Doc_Curriculum = $Doc_Curriculum;
    }

    public function setDoc_CompDomicilio($Doc_CompDomicilio) {
        $this->Doc_CompDomicilio = $Doc_CompDomicilio;
    }

    public function setDoc_Contrato($Doc_Contrato) {
        $this->Doc_Contrato = $Doc_Contrato;
    }

    public function setDoc_CompEstudios($Doc_CompEstudios) {
        $this->Doc_CompEstudios = $Doc_CompEstudios;
    }

    public function setCel_docen($Cel_docen) {
        $this->Cel_docen = $Cel_docen;
    }

    public function setCopy_RFC($copy_RFC) {
        $this->copy_RFC = $copy_RFC;
    }

    public function setCopy_Curriculum($copy_Curriculum) {
        $this->copy_Curriculum = $copy_Curriculum;
    }

    public function setCopy_CompDomicilio($copy_CompDomicilio) {
        $this->copy_CompDomicilio = $copy_CompDomicilio;
    }

    public function setCopy_Contrato($copy_Contrato) {
        $this->copy_Contrato = $copy_Contrato;
    }

    public function setCopy_CompEstudios($copy_CompEstudios) {
        $this->copy_CompEstudios = $copy_CompEstudios;
    }

    public function setCopy_CURP($copy_CURP) {
        $this->copy_CURP = $copy_CURP;
    }

    public function setTipo_sangre($Tipo_sangre) {
        $this->Tipo_sangre = $Tipo_sangre;
    }

    public function setAlergias($Alergias) {
        $this->Alergias = $Alergias;
    }

    public function setEnfermedades_cronicas($Enfermedades_cronicas) {
        $this->Enfermedades_cronicas = $Enfermedades_cronicas;
    }

    public function setPreinscripciones_medicas($Preinscripciones_medicas) {
        $this->Preinscripciones_medicas = $Preinscripciones_medicas;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    public function setRecover_docen($Recover_docen) {
        $this->Recover_docen = $Recover_docen;
    }

    public function setUltimaGeneracion_credencial($UltimaGeneracion_credencial) {
        $this->UltimaGeneracion_credencial = $UltimaGeneracion_credencial;
    }

    public function setTiempo_completo($Tiempo_completo) {
        $this->Tiempo_completo = $Tiempo_completo;
    }



    
    

}
