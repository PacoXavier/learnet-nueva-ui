<?php

class Asistencias {

    public $Id;
    public $Id_grupo;
    public $Id_ciclo;
    public $Id_alum;
    public $Fecha_asis;
    public $Mes_asis;
    public $Anio_asis;
    public $Asistio;
    public $Id_docente;
    
    function __construct() {
        $this->Id = "";
        $this->Id_grupo = "";
        $this->Id_ciclo = "";
        $this->Id_alum = "";
        $this->Fecha_asis = "";
        $this->Mes_asis = "";
        $this->Anio_asis = "";
        $this->Asistio = "";
        $this->Id_docente = "";
    }
    
    function getId_docente() {
        return $this->Id_docente;
    }

    function setId_docente($Id_docente) {
        $this->Id_docente = $Id_docente;
    }

    function getId() {
        return $this->Id;
    }

    function getId_grupo() {
        return $this->Id_grupo;
    }

    function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function getId_alum() {
        return $this->Id_alum;
    }

    function getFecha_asis() {
        return $this->Fecha_asis;
    }

    function getMes_asis() {
        return $this->Mes_asis;
    }

    function getAnio_asis() {
        return $this->Anio_asis;
    }

    function getAsistio() {
        return $this->Asistio;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_grupo($Id_grupo) {
        $this->Id_grupo = $Id_grupo;
    }

    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    function setId_alum($Id_alum) {
        $this->Id_alum = $Id_alum;
    }

    function setFecha_asis($Fecha_asis) {
        $this->Fecha_asis = $Fecha_asis;
    }

    function setMes_asis($Mes_asis) {
        $this->Mes_asis = $Mes_asis;
    }

    function setAnio_asis($Anio_asis) {
        $this->Anio_asis = $Anio_asis;
    }

    function setAsistio($Asistio) {
        $this->Asistio = $Asistio;
    }

}
