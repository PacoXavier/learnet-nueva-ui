<?php
class PermisosTipousuario {
    
    public $Id;
    public $Id_perm;
    public $Id_tipo_usu;
    
    function __construct() {
        $this->Id = "";
        $this->Id_perm = "";
        $this->Id_tipo_usu = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getId_perm() {
        return $this->Id_perm;
    }

    function getId_tipo_usu() {
        return $this->Id_tipo_usu;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_perm($Id_perm) {
        $this->Id_perm = $Id_perm;
    }

    function setId_tipo_usu($Id_tipo_usu) {
        $this->Id_tipo_usu = $Id_tipo_usu;
    }



    
}
