<?php

class Especialidades {
    public $Id;
    public $Id_ofe;
    public $Nombre_esp;
    public $Activo_esp;
    public $RegistroValidez_Oficial;
    public $Institucion_certifica;
    public $Modalidad;
    public $Tipo_ciclos;
    public $DuracionSemanas;
    public $ClavePlan_estudios;
    public $Escala_calificacion;
    public $Minima_aprobatotia;
    public $Limite_faltas;
    public $Precio_curso;
    public $Inscripcion_curso;
    public $Tipo_insc;
    public $Num_pagos;
    public $Num_ciclos;

    function __construct() {
        $this->Id = "";
        $this->Id_ofe = "";
        $this->Nombre_esp = "";
        $this->Activo_esp = "";
        $this->RegistroValidez_Oficial = "";
        $this->Institucion_certifica = "";
        $this->Modalidad = "";
        $this->Tipo_ciclos = "";
        $this->DuracionSemanas = "";
        $this->ClavePlan_estudios = "";
        $this->Escala_calificacion = "";
        $this->Minima_aprobatotia = "";
        $this->Limite_faltas = "";
        $this->Precio_curso = "";
        $this->Inscripcion_curso = "";
        $this->Tipo_insc = "";
        $this->Num_pagos = "";
        $this->Num_ciclos = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getId_ofe() {
        return $this->Id_ofe;
    }

    public function getNombre_esp() {
        return $this->Nombre_esp;
    }

    public function getActivo_esp() {
        return $this->Activo_esp;
    }

    public function getRegistroValidez_Oficial() {
        return $this->RegistroValidez_Oficial;
    }

    public function getInstitucion_certifica() {
        return $this->Institucion_certifica;
    }

    public function getModalidad() {
        return $this->Modalidad;
    }

    public function getTipo_ciclos() {
        return $this->Tipo_ciclos;
    }

    public function getDuracionSemanas() {
        return $this->DuracionSemanas;
    }

    public function getClavePlan_estudios() {
        return $this->ClavePlan_estudios;
    }

    public function getEscala_calificacion() {
        return $this->Escala_calificacion;
    }

    public function getMinima_aprobatotia() {
        return $this->Minima_aprobatotia;
    }

    public function getLimite_faltas() {
        return $this->Limite_faltas;
    }

    public function getPrecio_curso() {
        return $this->Precio_curso;
    }

    public function getInscripcion_curso() {
        return $this->Inscripcion_curso;
    }

    public function getTipo_insc() {
        return $this->Tipo_insc;
    }

    public function getNum_pagos() {
        return $this->Num_pagos;
    }

    public function getNum_ciclos() {
        return $this->Num_ciclos;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_ofe($Id_ofe) {
        $this->Id_ofe = $Id_ofe;
    }

    public function setNombre_esp($Nombre_esp) {
        $this->Nombre_esp = $Nombre_esp;
    }

    public function setActivo_esp($Activo_esp) {
        $this->Activo_esp = $Activo_esp;
    }

    public function setRegistroValidez_Oficial($RegistroValidez_Oficial) {
        $this->RegistroValidez_Oficial = $RegistroValidez_Oficial;
    }

    public function setInstitucion_certifica($Institucion_certifica) {
        $this->Institucion_certifica = $Institucion_certifica;
    }

    public function setModalidad($Modalidad) {
        $this->Modalidad = $Modalidad;
    }

    public function setTipo_ciclos($Tipo_ciclos) {
        $this->Tipo_ciclos = $Tipo_ciclos;
    }

    public function setDuracionSemanas($DuracionSemanas) {
        $this->DuracionSemanas = $DuracionSemanas;
    }

    public function setClavePlan_estudios($ClavePlan_estudios) {
        $this->ClavePlan_estudios = $ClavePlan_estudios;
    }

    public function setEscala_calificacion($Escala_calificacion) {
        $this->Escala_calificacion = $Escala_calificacion;
    }

    public function setMinima_aprobatotia($Minima_aprobatotia) {
        $this->Minima_aprobatotia = $Minima_aprobatotia;
    }

    public function setLimite_faltas($Limite_faltas) {
        $this->Limite_faltas = $Limite_faltas;
    }

    public function setPrecio_curso($Precio_curso) {
        $this->Precio_curso = $Precio_curso;
    }

    public function setInscripcion_curso($Inscripcion_curso) {
        $this->Inscripcion_curso = $Inscripcion_curso;
    }

    public function setTipo_insc($Tipo_insc) {
        $this->Tipo_insc = $Tipo_insc;
    }

    public function setNum_pagos($Num_pagos) {
        $this->Num_pagos = $Num_pagos;
    }

    public function setNum_ciclos($Num_ciclos) {
        $this->Num_ciclos = $Num_ciclos;
    }

}
