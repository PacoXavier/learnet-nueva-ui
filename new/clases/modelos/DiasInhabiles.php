<?php
class DiasInhabiles {
    
    
    public $Id;
    public $fecha_inh;
    public $Id_ciclo;
    public $EsFinDeSemana;
  
    function __construct() {
        $this->Id = "";
        $this->fecha_inh = "";
        $this->Id_ciclo = "";
        $this->EsFinDeSemana = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getFecha_inh() {
        return $this->fecha_inh;
    }

    function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function getEsFinDeSemana() {
        return $this->EsFinDeSemana;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setFecha_inh($fecha_inh) {
        $this->fecha_inh = $fecha_inh;
    }

    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    function setEsFinDeSemana($EsFinDeSemana) {
        $this->EsFinDeSemana = $EsFinDeSemana;
    }



}
