<?php
class CursosEspecialidad{
    
    public $Id;
    public $Id_esp;
    public $FechaInicio;
    public $FechaFin;
    public $DateCreated;
    public $TipoPago;
    public $DiaPago;
    public $Id_usu_capura;
    public $Activo;
    public $Clave;
    public $Num_horas;
    public $Num_sesiones;
    public $Generar_cargos_recurrentes;
    public $Dia_cargo_recurrente;
    public $Turno;
    
    function __construct() {
        $this->Id = "";
        $this->Id_esp = "";
        $this->FechaInicio = "";
        $this->FechaFin = "";
        $this->DateCreated = "";
        $this->TipoPago = "";
        $this->DiaPago = "";
        $this->Id_usu_capura = "";
        $this->Activo = "";
        $this->Clave = "";
        $this->Num_horas = "";
        $this->Num_sesiones = "";
        $this->Generar_cargos_recurrentes = "";
        $this->Dia_cargo_recurrente = "";
        $this->Turno = "";
    }
    
    function getGenerar_cargos_recurrentes() {
        return $this->Generar_cargos_recurrentes;
    }

    function getDia_cargo_recurrente() {
        return $this->Dia_cargo_recurrente;
    }

    function getTurno() {
        return $this->Turno;
    }

    function setGenerar_cargos_recurrentes($Generar_cargos_recurrentes) {
        $this->Generar_cargos_recurrentes = $Generar_cargos_recurrentes;
    }

    function setDia_cargo_recurrente($Dia_cargo_recurrente) {
        $this->Dia_cargo_recurrente = $Dia_cargo_recurrente;
    }

    function setTurno($Turno) {
        $this->Turno = $Turno;
    }

    function getNum_horas() {
        return $this->Num_horas;
    }

    function getNum_sesiones() {
        return $this->Num_sesiones;
    }

    function setNum_horas($Num_horas) {
        $this->Num_horas = $Num_horas;
    }

    function setNum_sesiones($Num_sesiones) {
        $this->Num_sesiones = $Num_sesiones;
    }

    
    function getId() {
        return $this->Id;
    }

    function getId_esp() {
        return $this->Id_esp;
    }

    function getFechaInicio() {
        return $this->FechaInicio;
    }

    function getFechaFin() {
        return $this->FechaFin;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function getTipoPago() {
        return $this->TipoPago;
    }

    function getDiaPago() {
        return $this->DiaPago;
    }

    function getId_usu_capura() {
        return $this->Id_usu_capura;
    }

    function getActivo() {
        return $this->Activo;
    }

    function getClave() {
        return $this->Clave;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_esp($Id_esp) {
        $this->Id_esp = $Id_esp;
    }

    function setFechaInicio($FechaInicio) {
        $this->FechaInicio = $FechaInicio;
    }

    function setFechaFin($FechaFin) {
        $this->FechaFin = $FechaFin;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    function setTipoPago($TipoPago) {
        $this->TipoPago = $TipoPago;
    }

    function setDiaPago($DiaPago) {
        $this->DiaPago = $DiaPago;
    }

    function setId_usu_capura($Id_usu_capura) {
        $this->Id_usu_capura = $Id_usu_capura;
    }

    function setActivo($Activo) {
        $this->Activo = $Activo;
    }

    function setClave($Clave) {
        $this->Clave = $Clave;
    }




}