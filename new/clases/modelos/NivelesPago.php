<?php
class NivelesPago{
    
    public $Id;
    public $Nombre_nivel;
    public $PuntosMin;
    public $PuntosMax;
    public $PagoNivel;
    public $Id_tipo_nivel;
    public $Activo;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre_nivel = "";
        $this->PuntosMin = "";
        $this->PuntosMax = "";
        $this->PagoNivel = "";
        $this->Id_tipo_nivel = "";
        $this->Activo = "";
    }
    
    function getActivo() {
        return $this->Activo;
    }

    function setActivo($Activo) {
        $this->Activo = $Activo;
    }

        
    function getId() {
        return $this->Id;
    }

    function getNombre_nivel() {
        return $this->Nombre_nivel;
    }

    function getPuntosMin() {
        return $this->PuntosMin;
    }

    function getPuntosMax() {
        return $this->PuntosMax;
    }

    function getPagoNivel() {
        return $this->PagoNivel;
    }

    function getId_tipo_nivel() {
        return $this->Id_tipo_nivel;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setNombre_nivel($Nombre_nivel) {
        $this->Nombre_nivel = $Nombre_nivel;
    }

    function setPuntosMin($PuntosMin) {
        $this->PuntosMin = $PuntosMin;
    }

    function setPuntosMax($PuntosMax) {
        $this->PuntosMax = $PuntosMax;
    }

    function setPagoNivel($PagoNivel) {
        $this->PagoNivel = $PagoNivel;
    }

    function setId_tipo_nivel($Id_tipo_nivel) {
        $this->Id_tipo_nivel = $Id_tipo_nivel;
    }



}
