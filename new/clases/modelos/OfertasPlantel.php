<?php
class OfertasPlantel {
    
    public $Id;
    public $Id_plantel;
    public $Id_ofe;

    function __construct() {
        $this->Id = "";
        $this->Id_plantel ="";
        $this->Id_ofe = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function getId_ofe() {
        return $this->Id_ofe;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    public function setId_ofe($Id_ofe) {
        $this->Id_ofe = $Id_ofe;
    }

    
}
