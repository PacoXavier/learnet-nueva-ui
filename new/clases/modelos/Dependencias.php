<?php

class Dependencias {
    
    public $Id;
    public $Id_tarea;
    public $Id_depe;
    
    function __construct() {
        $this->Id = "";
        $this->Id_tarea = "";
        $this->Id_depe = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getId_tarea() {
        return $this->Id_tarea;
    }

    public function getId_depe() {
        return $this->Id_depe;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_tarea($Id_tarea) {
        $this->Id_tarea = $Id_tarea;
    }

    public function setId_depe($Id_depe) {
        $this->Id_depe = $Id_depe;
    }

}
