<?php

class PaquetesDescuentos {
    public $Id;
    public $Fecha_inicio;
    public $Fecha_fin;
    public $Nombre;
    public $Activo_des;
    public $Porcentaje;
    public $Id_plantel;
    
    function __construct() {
        $this->Id = "";
        $this->Fecha_inicio = "";
        $this->Fecha_fin = "";
        $this->Nombre = "";
        $this->Activo_des = "";
        $this->Porcentaje = "";
        $this->Id_plantel = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getFecha_inicio() {
        return $this->Fecha_inicio;
    }

    public function getFecha_fin() {
        return $this->Fecha_fin;
    }

    public function getNombre() {
        return $this->Nombre;
    }

    public function getActivo_des() {
        return $this->Activo_des;
    }

    public function getPorcentaje() {
        return $this->Porcentaje;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setFecha_inicio($Fecha_inicio) {
        $this->Fecha_inicio = $Fecha_inicio;
    }

    public function setFecha_fin($Fecha_fin) {
        $this->Fecha_fin = $Fecha_fin;
    }

    public function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    public function setActivo_des($Activo_des) {
        $this->Activo_des = $Activo_des;
    }

    public function setPorcentaje($Porcentaje) {
        $this->Porcentaje = $Porcentaje;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }




}
