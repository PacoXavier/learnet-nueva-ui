<?php
class CamposCategoria{
    
    public $Id;
    public $Nombre;
    public $Valor;
    public $Id_cat;
    public $Tipo_camp;
    public $Activo;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->Valor = "";
        $this->Id_cat = "";
        $this->Tipo_camp = "";
        $this->Activo = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getValor() {
        return $this->Valor;
    }

    function getId_cat() {
        return $this->Id_cat;
    }

    function getTipo_camp() {
        return $this->Tipo_camp;
    }

    function getActivo() {
        return $this->Activo;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setValor($Valor) {
        $this->Valor = $Valor;
    }

    function setId_cat($Id_cat) {
        $this->Id_cat = $Id_cat;
    }

    function setTipo_camp($Tipo_camp) {
        $this->Tipo_camp = $Tipo_camp;
    }

    function setActivo($Activo) {
        $this->Activo = $Activo;
    }



    
   
}