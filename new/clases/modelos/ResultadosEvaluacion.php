<?php
class ResultadosEvaluacion{
    
    public $Id;
    public $Id_camp;
    public $Puntos;
    public $Id_eva;

    function __construct() {
        $this->Id = "";
        $this->Id_camp = "";
        $this->Puntos = "";
        $this->Id_eva = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getId_camp() {
        return $this->Id_camp;
    }

    function getPuntos() {
        return $this->Puntos;
    }

    function getId_eva() {
        return $this->Id_eva;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_camp($Id_camp) {
        $this->Id_camp = $Id_camp;
    }

    function setPuntos($Puntos) {
        $this->Puntos = $Puntos;
    }

    function setId_eva($Id_eva) {
        $this->Id_eva = $Id_eva;
    }




}
