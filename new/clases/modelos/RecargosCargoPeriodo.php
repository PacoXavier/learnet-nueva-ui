<?php
class RecargosCargoPeriodo{
    
    public $Id;
    public $Fecha_recargo;
    public $Id_cargo;
    public $Monto;
    public $Comentarios;
    public $Id_usu_captura;
    
    function __construct() {
        $this->Id = "";
        $this->Fecha_recargo = "";
        $this->Id_cargo = "";
        $this->Monto = "";
        $this->Comentarios = "";
        $this->Id_usu_captura = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getFecha_recargo() {
        return $this->Fecha_recargo;
    }

    function getId_cargo() {
        return $this->Id_cargo;
    }

    function getMonto() {
        return $this->Monto;
    }

    function getComentarios() {
        return $this->Comentarios;
    }

    function getId_usu_captura() {
        return $this->Id_usu_captura;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setFecha_recargo($Fecha_recargo) {
        $this->Fecha_recargo = $Fecha_recargo;
    }

    function setId_cargo($Id_cargo) {
        $this->Id_cargo = $Id_cargo;
    }

    function setMonto($Monto) {
        $this->Monto = $Monto;
    }

    function setComentarios($Comentarios) {
        $this->Comentarios = $Comentarios;
    }

    function setId_usu_captura($Id_usu_captura) {
        $this->Id_usu_captura = $Id_usu_captura;
    }




    
}
