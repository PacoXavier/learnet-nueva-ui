<?php

class OfertasAlumno {
    public $Id;
    public $Id_ofe;
    public $Id_alum;
    public $Id_esp;
    public $Id_ori;
    public $Id_curso_esp;
    public $Beca;
    public $OpcionPago;
    public $Tipo_beca;
    public $Activo;
    public $Turno;
    public $Alta_ofe;
    public $Baja_ofe;
    public $Id_mot_baja;
    public $Comentario_baja;
    public $UltimaGeneracion_credencial;
    public $Id_usu_captura_baja;
    public $FechaCapturaEgreso;
    public $IdCicloEgreso;
    public $IdUsuEgreso;
    public $GenerarAlerta;
    
    function __construct() {
        $this->Id = "";
        $this->Id_ofe = "";
        $this->Id_alum = "";
        $this->Id_esp = "";
        $this->Id_ori = "";
        $this->Id_curso_esp = "";
        $this->Beca = "";
        $this->OpcionPago = "";
        $this->Tipo_beca = "";
        $this->Activo = "";
        $this->Turno = "";
        $this->Alta_ofe = "";
        $this->Baja_ofe = "";
        $this->Id_mot_baja = "";
        $this->Comentario_baja = "";
        $this->UltimaGeneracion_credencial = "";
        $this->Id_usu_captura_baja = "";
        $this->FechaCapturaEgreso = "";
        $this->IdCicloEgreso = "";
        $this->IdUsuEgreso = "";
        $this->GenerarAlerta = "";
    }
    
    function getId_curso_esp() {
        return $this->Id_curso_esp;
    }

    function setId_curso_esp($Id_curso_esp) {
        $this->Id_curso_esp = $Id_curso_esp;
    }

        function getGenerarAlerta() {
        return $this->GenerarAlerta;
    }

    function setGenerarAlerta($GenerarAlerta) {
        $this->GenerarAlerta = $GenerarAlerta;
    }

    
    public function getId() {
        return $this->Id;
    }

    public function getId_ofe() {
        return $this->Id_ofe;
    }

    public function getId_alum() {
        return $this->Id_alum;
    }

    public function getId_esp() {
        return $this->Id_esp;
    }

    public function getId_ori() {
        return $this->Id_ori;
    }

    public function getBeca() {
        return $this->Beca;
    }

    public function getOpcionPago() {
        return $this->OpcionPago;
    }

    public function getTipo_beca() {
        return $this->Tipo_beca;
    }

    public function getActivo() {
        return $this->Activo;
    }

    public function getTurno() {
        return $this->Turno;
    }

    public function getAlta_ofe() {
        return $this->Alta_ofe;
    }

    public function getBaja_ofe() {
        return $this->Baja_ofe;
    }

    public function getId_mot_baja() {
        return $this->Id_mot_baja;
    }

    public function getComentario_baja() {
        return $this->Comentario_baja;
    }

    public function getUltimaGeneracion_credencial() {
        return $this->UltimaGeneracion_credencial;
    }

    public function getId_usu_captura_baja() {
        return $this->Id_usu_captura_baja;
    }

    public function getFechaCapturaEgreso() {
        return $this->FechaCapturaEgreso;
    }

    public function getIdCicloEgreso() {
        return $this->IdCicloEgreso;
    }

    public function getIdUsuEgreso() {
        return $this->IdUsuEgreso;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_ofe($Id_ofe) {
        $this->Id_ofe = $Id_ofe;
    }

    public function setId_alum($Id_alum) {
        $this->Id_alum = $Id_alum;
    }

    public function setId_esp($Id_esp) {
        $this->Id_esp = $Id_esp;
    }

    public function setId_ori($Id_ori) {
        $this->Id_ori = $Id_ori;
    }

    public function setBeca($Beca) {
        $this->Beca = $Beca;
    }

    public function setOpcionPago($OpcionPago) {
        $this->OpcionPago = $OpcionPago;
    }

    public function setTipo_beca($Tipo_beca) {
        $this->Tipo_beca = $Tipo_beca;
    }

    public function setActivo($Activo) {
        $this->Activo = $Activo;
    }

    public function setTurno($Turno) {
        $this->Turno = $Turno;
    }

    public function setAlta_ofe($Alta_ofe) {
        $this->Alta_ofe = $Alta_ofe;
    }

    public function setBaja_ofe($Baja_ofe) {
        $this->Baja_ofe = $Baja_ofe;
    }

    public function setId_mot_baja($Id_mot_baja) {
        $this->Id_mot_baja = $Id_mot_baja;
    }

    public function setComentario_baja($Comentario_baja) {
        $this->Comentario_baja = $Comentario_baja;
    }

    public function setUltimaGeneracion_credencial($UltimaGeneracion_credencial) {
        $this->UltimaGeneracion_credencial = $UltimaGeneracion_credencial;
    }

    public function setId_usu_captura_baja($Id_usu_captura_baja) {
        $this->Id_usu_captura_baja = $Id_usu_captura_baja;
    }

    public function setFechaCapturaEgreso($FechaCapturaEgreso) {
        $this->FechaCapturaEgreso = $FechaCapturaEgreso;
    }

    public function setIdCicloEgreso($IdCicloEgreso) {
        $this->IdCicloEgreso = $IdCicloEgreso;
    }

    public function setIdUsuEgreso($IdUsuEgreso) {
        $this->IdUsuEgreso = $IdUsuEgreso;
    }



   

}
