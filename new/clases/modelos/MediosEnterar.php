<?php

class MediosEnterar {
    
    public $Id;
    public $Medio;
    public $Id_plantel;
    public $Activo;
    public $FechaIni;
    public $FechaFin;

    function __construct() {
        $this->Id = "";
        $this->Medio = "";
        $this->Id_plantel = "";
        $this->Activo = "";
        $this->FechaIni = "";
        $this->FechaFin = "";
    }
    
    function getFechaIni() {
        return $this->FechaIni;
    }

    function getFechaFin() {
        return $this->FechaFin;
    }

    function setFechaIni($FechaIni) {
        $this->FechaIni = $FechaIni;
    }

    function setFechaFin($FechaFin) {
        $this->FechaFin = $FechaFin;
    }

        
    public function getId() {
        return $this->Id;
    }

    public function getMedio() {
        return $this->Medio;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function getActivo() {
        return $this->Activo;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setMedio($Medio) {
        $this->Medio = $Medio;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    public function setActivo($Activo) {
        $this->Activo = $Activo;
    }

}
