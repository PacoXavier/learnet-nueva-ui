<?php

class DatosBancarios {
    
    public $Id;
    public $Banco;
    public $Beneficiario;
    public $Convenio;
    public $Trasferencias;
    public $RFC;
    public $Id_plantel;
    
    function __construct() {
        
        $this->Id = "";
        $this->Banco = "";
        $this->Beneficiario = "";
        $this->Convenio = "";
        $this->Trasferencias = "";
        $this->RFC = "";
        $this->Id_plantel = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getBanco() {
        return $this->Banco;
    }

    function getBeneficiario() {
        return $this->Beneficiario;
    }

    function getConvenio() {
        return $this->Convenio;
    }

    function getTrasferencias() {
        return $this->Trasferencias;
    }

    function getRFC() {
        return $this->RFC;
    }

    function getId_plantel() {
        return $this->Id_plantel;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setBanco($Banco) {
        $this->Banco = $Banco;
    }

    function setBeneficiario($Beneficiario) {
        $this->Beneficiario = $Beneficiario;
    }

    function setConvenio($Convenio) {
        $this->Convenio = $Convenio;
    }

    function setTrasferencias($Trasferencias) {
        $this->Trasferencias = $Trasferencias;
    }

    function setRFC($RFC) {
        $this->RFC = $RFC;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

}
