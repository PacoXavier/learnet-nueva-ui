<?php

class CategoriasSicologia {
    
    public $Id;
    public $Nombre;
    public $Activo;
    public $Id_plantel;
    public $Orden;
    public $RangoMax;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre =  "";
        $this->Activo =  "";
        $this->Id_plantel =  "";
        $this->Orden = "";
        $this->RangoMax = "";
    }
    
    function getOrden() {
        return $this->Orden;
    }

    function getRangoMax() {
        return $this->RangoMax;
    }

    function setOrden($Orden) {
        $this->Orden = $Orden;
    }

    function setRangoMax($RangoMax) {
        $this->RangoMax = $RangoMax;
    }
    
    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

        public function getId() {
        return $this->Id;
    }

    public function getNombre() {
        return $this->Nombre;
    }

    public function getActivo() {
        return $this->Activo;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    public function setActivo($Activo) {
        $this->Activo = $Activo;
    }

}
