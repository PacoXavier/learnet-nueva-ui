<?php

class PrivacidadActividad {
    
    public $Id;
    public $Id_act;
    public $Id_usu;
   
    function __construct() {
        $this->Id = "";
        $this->Id_act = "";
        $this->Id_usu = "";
    }

    public function getId() {
        return $this->Id;
    }

    public function getId_act() {
        return $this->Id_act;
    }

    public function getId_usu() {
        return $this->Id_usu;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_act($Id_act) {
        $this->Id_act = $Id_act;
    }

    public function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

}
