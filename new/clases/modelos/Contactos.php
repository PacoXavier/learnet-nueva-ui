<?php

class Contactos {
    public $Id;
    public $Nombre;
    public $Parentesco;
    public $Direccion;
    public $Tel_casa;
    public $Tel_ofi;
    public $Cel;
    public $Email;
    public $IdRel_con;
    public $TipoRel_con;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->Parentesco = "";
        $this->Direccion = "";
        $this->Tel_casa = "";
        $this->Tel_ofi = "";
        $this->Cel = "";
        $this->Email ="";
        $this->IdRel_con = "";
        $this->TipoRel_con = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getParentesco() {
        return $this->Parentesco;
    }

    function getDireccion() {
        return $this->Direccion;
    }

    function getTel_casa() {
        return $this->Tel_casa;
    }

    function getTel_ofi() {
        return $this->Tel_ofi;
    }

    function getCel() {
        return $this->Cel;
    }

    function getEmail() {
        return $this->Email;
    }

    function getIdRel_con() {
        return $this->IdRel_con;
    }

    function getTipoRel_con() {
        return $this->TipoRel_con;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setParentesco($Parentesco) {
        $this->Parentesco = $Parentesco;
    }

    function setDireccion($Direccion) {
        $this->Direccion = $Direccion;
    }

    function setTel_casa($Tel_casa) {
        $this->Tel_casa = $Tel_casa;
    }

    function setTel_ofi($Tel_ofi) {
        $this->Tel_ofi = $Tel_ofi;
    }

    function setCel($Cel) {
        $this->Cel = $Cel;
    }

    function setEmail($Email) {
        $this->Email = $Email;
    }

    function setIdRel_con($IdRel_con) {
        $this->IdRel_con = $IdRel_con;
    }

    function setTipoRel_con($TipoRel_con) {
        $this->TipoRel_con = $TipoRel_con;
    }




    
}
