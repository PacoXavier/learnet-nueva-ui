<?php

class Pagos {

    public $Id;
    public $Id_pago_ciclo;
    public $Monto_pp;
    public $Id_usu;
    public $Fecha_pp;
    public $Metodo_pago;
    public $Fecha_captura;
    public $Comentarios;
    public $Id_evento;
    public $NombrePaga;
    public $EmailPaga;
    public $Id_ofe_alum;
    public $Concepto;
    public $Id_plantel;
    public $Activo;
    public $Folio;
    public $DetalleCarrera;
    public $IdRel;
    public $TipoRel;
    public $Id_ciclo_a_usar;
    
    function __construct() {
        $this->Id = "";
        $this->Id_pago_ciclo = "";
        $this->Monto_pp = "";
        $this->Id_usu = "";
        $this->Fecha_pp = "";
        $this->Metodo_pago = "";
        $this->Fecha_captura = "";
        $this->Comentarios = "";
        $this->Id_evento = "";
        $this->NombrePaga = "";
        $this->EmailPaga = "";
        $this->Id_ofe_alum = "";
        $this->Concepto = "";
        $this->Id_plantel = "";
        $this->Activo="";
        $this->Folio="";
        $this->DetalleCarrera="";
        $this->IdRel = "";
        $this->TipoRel = "";
        $this->Id_ciclo_a_usar="";
    }
    function getId_ciclo_a_usar() {
        return $this->Id_ciclo_a_usar;
    }

    function setId_ciclo_a_usar($Id_ciclo_a_usar) {
        $this->Id_ciclo_a_usar = $Id_ciclo_a_usar;
    }

        
    function getIdRel() {
        return $this->IdRel;
    }

    function getTipoRel() {
        return $this->TipoRel;
    }

    function setIdRel($IdRel) {
        $this->IdRel = $IdRel;
    }

    function setTipoRel($TipoRel) {
        $this->TipoRel = $TipoRel;
    }
    
    
    function getDetalleCarrera() {
        return $this->DetalleCarrera;
    }

    function setDetalleCarrera($DetalleCarrera) {
        $this->DetalleCarrera = $DetalleCarrera;
    }

    function getFolio() {
        return $this->Folio;
    }

    function setFolio($Folio) {
        $this->Folio = $Folio;
    }

        
    function getActivo() {
        return $this->Activo;
    }

    function setActivo($Activo) {
        $this->Activo = $Activo;
    }

        function getId_plantel() {
        return $this->Id_plantel;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

        
    function getConcepto() {
        return $this->Concepto;
    }

    function setConcepto($Concepto) {
        $this->Concepto = $Concepto;
    }

    
    function getId_ofe_alum() {
        return $this->Id_ofe_alum;
    }

    function setId_ofe_alum($Id_ofe_alum) {
        $this->Id_ofe_alum = $Id_ofe_alum;
    }

    public function getNombrePaga() {
        return $this->NombrePaga;
    }

    public function getEmailPaga() {
        return $this->EmailPaga;
    }

    public function setNombrePaga($NombrePaga) {
        $this->NombrePaga = $NombrePaga;
    }

    public function setEmailPaga($EmailPaga) {
        $this->EmailPaga = $EmailPaga;
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getId_pago_ciclo() {
        return $this->Id_pago_ciclo;
    }

    public function getMonto_pp() {
        return $this->Monto_pp;
    }

    public function getId_usu() {
        return $this->Id_usu;
    }

    public function getFecha_pp() {
        return $this->Fecha_pp;
    }

    public function getMetodo_pago() {
        return $this->Metodo_pago;
    }

    public function getFecha_captura() {
        return $this->Fecha_captura;
    }

    public function getComentarios() {
        return $this->Comentarios;
    }

    public function getId_evento() {
        return $this->Id_evento;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_pago_ciclo($Id_pago_ciclo) {
        $this->Id_pago_ciclo = $Id_pago_ciclo;
    }

    public function setMonto_pp($Monto_pp) {
        $this->Monto_pp = $Monto_pp;
    }

    public function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    public function setFecha_pp($Fecha_pp) {
        $this->Fecha_pp = $Fecha_pp;
    }

    public function setMetodo_pago($Metodo_pago) {
        $this->Metodo_pago = $Metodo_pago;
    }

    public function setFecha_captura($Fecha_captura) {
        $this->Fecha_captura = $Fecha_captura;
    }

    public function setComentarios($Comentarios) {
        $this->Comentarios = $Comentarios;
    }

    public function setId_evento($Id_evento) {
        $this->Id_evento = $Id_evento;
    }

}
