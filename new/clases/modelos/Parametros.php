<?php

class Parametros {
    
    public $Id;
    public $text_param;
    public $valor_param;
    public $Id_plantel;

    function __construct() {
        $this->Id = "";
        $this->text_param = "";
        $this->valor_param = "";
        $this->Id_plantel = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getText_param() {
        return $this->text_param;
    }

    function getValor_param() {
        return $this->valor_param;
    }

    function getId_plantel() {
        return $this->Id_plantel;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setText_param($text_param) {
        $this->text_param = $text_param;
    }

    function setValor_param($valor_param) {
        $this->valor_param = $valor_param;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }




}
