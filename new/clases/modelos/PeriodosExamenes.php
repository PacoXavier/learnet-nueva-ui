<?php

class PeriodosExamenes{
    public $Id;
    public $Id_ciclo;
    public $FechaIni;
    public $FechaFin;
    public $Nombre;
    public $DateCreated;
    public $Color;
    public $Id_usu;
    
    function __construct() {
        $this->Id = "";
        $this->Id_ciclo = "";
        $this->FechaIni = "";
        $this->FechaFin = "";
        $this->Nombre = "";
        $this->DateCreated = "";
        $this->Color = "";
        $this->Id_usu = "";
    }
    function getDateCreated() {
        return $this->DateCreated;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

        function getId() {
        return $this->Id;
    }

    function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function getFechaIni() {
        return $this->FechaIni;
    }

    function getFechaFin() {
        return $this->FechaFin;
    }

    function getNombre() {
        return $this->Nombre;
    }



    function getColor() {
        return $this->Color;
    }

    function getId_usu() {
        return $this->Id_usu;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    function setFechaIni($FechaIni) {
        $this->FechaIni = $FechaIni;
    }

    function setFechaFin($FechaFin) {
        $this->FechaFin = $FechaFin;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }


    function setColor($Color) {
        $this->Color = $Color;
    }

    function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }
}