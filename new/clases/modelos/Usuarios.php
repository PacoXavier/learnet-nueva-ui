<?php

class Usuarios {
    public $Id;
    public $Nombre_usu;
    public $ApellidoP_usu;
    public $ApellidoM_usu;
    public $Tipo_usu;
    public $Alta_usu;
    public $Baja_usu;
    public $Id_googleUi;
    public $Id_faceUI;
    public $Email_usu;
    public $Last_session;
    public $Pass_usu;
    public $Recover_usu;
    public $Img_usu;
    public $Tel_usu;
    public $Clave_usu;
    public $Id_plantel;
    public $Cel_usu;
    public $Doc_RFC;
    public $Doc_CURP;
    public $Doc_IMSS;
    public $Copy_CURP;
    public $Copy_IMSS;
    public $Copy_RFC;
    public $Tipo_sangre;
    public $Alergias;
    public $Enfermedades_cronicas;
    public $Preinscripciones_medicas;
    public $Nivel_estudios;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre_usu = "";
        $this->ApellidoP_usu = "";
        $this->ApellidoM_usu = "";
        $this->Tipo_usu = "";
        $this->Alta_usu = "";
        $this->Baja_usu = "";
        $this->Id_googleUi = "";
        $this->Id_faceUI = "";
        $this->Email_usu = "";
        $this->Last_session = "";
        $this->Pass_usu = "";
        $this->Recover_usu = "";
        $this->Img_usu = "";
        $this->Tel_usu = "";
        $this->Clave_usu = "";
        $this->Id_plantel = "";
        $this->Cel_usu = "";
        $this->Doc_RFC = "";
        $this->Doc_CURP = "";
        $this->Doc_IMSS = "";
        $this->Copy_CURP = "";
        $this->Copy_IMSS = "";
        $this->Copy_RFC = "";
        $this->Tipo_sangre = "";
        $this->Alergias = "";
        $this->Enfermedades_cronicas = "";
        $this->Preinscripciones_medicas = "";
        $this->Nivel_estudios = "";
    }
    
    function getNivel_estudios() {
        return $this->Nivel_estudios;
    }



    public function getId() {
        return $this->Id;
    }

    public function getNombre_usu() {
        return $this->Nombre_usu;
    }

    public function getApellidoP_usu() {
        return $this->ApellidoP_usu;
    }

    public function getApellidoM_usu() {
        return $this->ApellidoM_usu;
    }

    public function getTipo_usu() {
        return $this->Tipo_usu;
    }

    public function getAlta_usu() {
        return $this->Alta_usu;
    }

    public function getBaja_usu() {
        return $this->Baja_usu;
    }

    public function getId_googleUi() {
        return $this->Id_googleUi;
    }

    public function getId_faceUI() {
        return $this->Id_faceUI;
    }

    public function getEmail_usu() {
        return $this->Email_usu;
    }

    public function getLast_session() {
        return $this->Last_session;
    }

    public function getPass_usu() {
        return $this->Pass_usu;
    }

    public function getRecover_usu() {
        return $this->Recover_usu;
    }

    public function getImg_usu() {
        return $this->Img_usu;
    }

    public function getTel_usu() {
        return $this->Tel_usu;
    }

    public function getClave_usu() {
        return $this->Clave_usu;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function getCel_usu() {
        return $this->Cel_usu;
    }

    public function getDoc_RFC() {
        return $this->Doc_RFC;
    }

    public function getDoc_CURP() {
        return $this->Doc_CURP;
    }

    public function getDoc_IMSS() {
        return $this->Doc_IMSS;
    }

    public function getCopy_CURP() {
        return $this->Copy_CURP;
    }

    public function getCopy_IMSS() {
        return $this->Copy_IMSS;
    }

    public function getCopy_RFC() {
        return $this->Copy_RFC;
    }

    public function getTipo_sangre() {
        return $this->Tipo_sangre;
    }

    public function getAlergias() {
        return $this->Alergias;
    }

    public function getEnfermedades_cronicas() {
        return $this->Enfermedades_cronicas;
    }

    public function getPreinscripciones_medicas() {
        return $this->Preinscripciones_medicas;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setNombre_usu($Nombre_usu) {
        $this->Nombre_usu = $Nombre_usu;
    }

    public function setApellidoP_usu($ApellidoP_usu) {
        $this->ApellidoP_usu = $ApellidoP_usu;
    }

    public function setApellidoM_usu($ApellidoM_usu) {
        $this->ApellidoM_usu = $ApellidoM_usu;
    }

    public function setTipo_usu($Tipo_usu) {
        $this->Tipo_usu = $Tipo_usu;
    }

    public function setAlta_usu($Alta_usu) {
        $this->Alta_usu = $Alta_usu;
    }

    public function setBaja_usu($Baja_usu) {
        $this->Baja_usu = $Baja_usu;
    }

    public function setId_googleUi($Id_googleUi) {
        $this->Id_googleUi = $Id_googleUi;
    }

    public function setId_faceUI($Id_faceUI) {
        $this->Id_faceUI = $Id_faceUI;
    }

    public function setEmail_usu($Email_usu) {
        $this->Email_usu = $Email_usu;
    }

    public function setLast_session($Last_session) {
        $this->Last_session = $Last_session;
    }

    public function setPass_usu($Pass_usu) {
        $this->Pass_usu = $Pass_usu;
    }

    public function setRecover_usu($Recover_usu) {
        $this->Recover_usu = $Recover_usu;
    }

    public function setImg_usu($Img_usu) {
        $this->Img_usu = $Img_usu;
    }

    public function setTel_usu($Tel_usu) {
        $this->Tel_usu = $Tel_usu;
    }

    public function setClave_usu($Clave_usu) {
        $this->Clave_usu = $Clave_usu;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    public function setCel_usu($Cel_usu) {
        $this->Cel_usu = $Cel_usu;
    }

    public function setDoc_RFC($Doc_RFC) {
        $this->Doc_RFC = $Doc_RFC;
    }

    public function setDoc_CURP($Doc_CURP) {
        $this->Doc_CURP = $Doc_CURP;
    }

    public function setDoc_IMSS($Doc_IMSS) {
        $this->Doc_IMSS = $Doc_IMSS;
    }

    public function setCopy_CURP($Copy_CURP) {
        $this->Copy_CURP = $Copy_CURP;
    }

    public function setCopy_IMSS($Copy_IMSS) {
        $this->Copy_IMSS = $Copy_IMSS;
    }

    public function setCopy_RFC($Copy_RFC) {
        $this->Copy_RFC = $Copy_RFC;
    }

    public function setTipo_sangre($Tipo_sangre) {
        $this->Tipo_sangre = $Tipo_sangre;
    }

    public function setAlergias($Alergias) {
        $this->Alergias = $Alergias;
    }

    public function setEnfermedades_cronicas($Enfermedades_cronicas) {
        $this->Enfermedades_cronicas = $Enfermedades_cronicas;
    }

    public function setPreinscripciones_medicas($Preinscripciones_medicas) {
        $this->Preinscripciones_medicas = $Preinscripciones_medicas;
    }

    function setNivel_estudios($Nivel_estudios) {
        $this->Nivel_estudios = $Nivel_estudios;
    }
}
