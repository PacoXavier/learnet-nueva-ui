<?php

class HorarioExamenGrupo{
    public $Id;
    public $Id_aula;
    public $Start;
    public $End;
    public $FechaAplicacion;
    public $DateCreated;
    public $Id_usu;
    public $IdHoraIni;
    public $IdHoraFin;
    public $Id_ciclo;
    public $Id_periodoExamen;
    
    function __construct() {
        $this->Id = "";
        $this->Id_aula = "";
        $this->Start = "";
        $this->End = "";
        $this->FechaAplicacion = "";
        $this->DateCreated = "";
        $this->Id_usu = "";
        $this->IdHoraIni = "";
        $this->IdHoraFin = "";
        $this->Id_ciclo = "";
       $this->Id_periodoExamen = "";
    }
    
    function getId_periodoExamen() {
        return $this->Id_periodoExamen;
    }

    function setId_periodoExamen($Id_periodoExamen) {
        $this->Id_periodoExamen = $Id_periodoExamen;
    }

        function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

        function getIdHoraIni() {
        return $this->IdHoraIni;
    }

    function getIdHoraFin() {
        return $this->IdHoraFin;
    }

    function setIdHoraIni($IdHoraIni) {
        $this->IdHoraIni = $IdHoraIni;
    }

    function setIdHoraFin($IdHoraFin) {
        $this->IdHoraFin = $IdHoraFin;
    }

        function getStart() {
        return $this->Start;
    }

    function getEnd() {
        return $this->End;
    }

    function setStart($Start) {
        $this->Start = $Start;
    }

    function setEnd($End) {
        $this->End = $End;
    }

        function getId() {
        return $this->Id;
    }

    function getId_aula() {
        return $this->Id_aula;
    }


    function getFechaAplicacion() {
        return $this->FechaAplicacion;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function getId_usu() {
        return $this->Id_usu;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_aula($Id_aula) {
        $this->Id_aula = $Id_aula;
    }



    function setFechaAplicacion($FechaAplicacion) {
        $this->FechaAplicacion = $FechaAplicacion;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }


    
   
}