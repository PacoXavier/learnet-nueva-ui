<?php

class PermisosExtra {
    
    public $Id;
    public $Id_per;
    public $Id_usu;
    
    function __construct() {
        $this->Id = "";
        $this->Id_per = "";
        $this->Id_usu = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getId_per() {
        return $this->Id_per;
    }

    function getId_usu() {
        return $this->Id_usu;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_per($Id_per) {
        $this->Id_per = $Id_per;
    }

    function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }




}
