<?php
class CamposSicologia {
    
    public $Id;
    public $Nombre;
    public $Activo;
    public $Id_cat;

    
    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->Activo = "";
        $this->Id_cat = "";
    }
    

    public function getId() {
        return $this->Id;
    }

    public function getNombre() {
        return $this->Nombre;
    }

    public function getActivo() {
        return $this->Activo;
    }

    public function getId_cat() {
        return $this->Id_cat;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    public function setActivo($Activo) {
        $this->Activo = $Activo;
    }

    public function setId_cat($Id_cat) {
        $this->Id_cat = $Id_cat;
    }



    
    
}
