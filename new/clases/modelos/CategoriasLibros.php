<?php
class CategoriasLibros {
    
    public $Id;
    public $Codigo;
    public $Nombre;
    public $Parent;
    public $Id_plantel;
    public $Activo;
    
    function __construct() {
        $this->Id = "";
        $this->Codigo = "";
        $this->Nombre = "";
        $this->Parent = "";
        $this->Id_plantel = "";
        $this->Activo = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getCodigo() {
        return $this->Codigo;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getParent() {
        return $this->Parent;
    }

    function getId_plantel() {
        return $this->Id_plantel;
    }

    function getActivo() {
        return $this->Activo;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setCodigo($Codigo) {
        $this->Codigo = $Codigo;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setParent($Parent) {
        $this->Parent = $Parent;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    function setActivo($Activo) {
        $this->Activo = $Activo;
    }



    
}
