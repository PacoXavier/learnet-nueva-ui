<?php

class Libros {
    
    public $Id;
    public $Titulo;
    public $Autor;
    public $Editorial;
    public $Anio;
    public $Edicion;
    public $Seccion;
    public $Id_aula;
    public $Descripcion;
    public $Id_img;
    public $Codigo;
    public $Id_plantel;
    public $Baja_libro;
    public $Valor;
    public $Fecha_adq;
    public $Garantia_meses;
    public $Comentario_baja;
    public $Disponible;
    public $Fecha_alta;
    public $ISBN;
    public $Id_cat;
    public $Id_subcat;
    public $Sinopsis;
    
    function __construct() {
        $this->Id = "";
        $this->Titulo = "";
        $this->Autor = "";
        $this->Editorial = "";
        $this->Anio = "";
        $this->Edicion = "";
        $this->Seccion = "";
        $this->Id_aula = "";
        $this->Descripcion = "";
        $this->Id_img = "";
        $this->Codigo = "";
        $this->Id_plantel = "";
        $this->Baja_libro = "";
        $this->Valor = "";
        $this->Fecha_adq = "";
        $this->Garantia_meses = "";
        $this->Comentario_baja = "";
        $this->Disponible = "";
        $this->Fecha_alta = "";
        $this->ISBN = "";
        $this->Id_cat = "";
        $this->Id_subcat = "";
        $this->Sinopsis = "";
    }

    function getSinopsis() {
        return $this->Sinopsis;
    }

    function setSinopsis($Sinopsis) {
        $this->Sinopsis = $Sinopsis;
    }

    function getId() {
        return $this->Id;
    }

    function getTitulo() {
        return $this->Titulo;
    }

    function getAutor() {
        return $this->Autor;
    }

    function getEditorial() {
        return $this->Editorial;
    }

    function getAnio() {
        return $this->Anio;
    }

    function getEdicion() {
        return $this->Edicion;
    }

    function getSeccion() {
        return $this->Seccion;
    }

    function getId_aula() {
        return $this->Id_aula;
    }

    function getDescripcion() {
        return $this->Descripcion;
    }

    function getId_img() {
        return $this->Id_img;
    }

    function getCodigo() {
        return $this->Codigo;
    }

    function getId_plantel() {
        return $this->Id_plantel;
    }

    function getBaja_libro() {
        return $this->Baja_libro;
    }

    function getValor() {
        return $this->Valor;
    }

    function getFecha_adq() {
        return $this->Fecha_adq;
    }

    function getGarantia_meses() {
        return $this->Garantia_meses;
    }

    function getComentario_baja() {
        return $this->Comentario_baja;
    }

    function getDisponible() {
        return $this->Disponible;
    }

    function getFecha_alta() {
        return $this->Fecha_alta;
    }

    function getISBN() {
        return $this->ISBN;
    }

    function getId_cat() {
        return $this->Id_cat;
    }

    function getId_subcat() {
        return $this->Id_subcat;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setTitulo($Titulo) {
        $this->Titulo = $Titulo;
    }

    function setAutor($Autor) {
        $this->Autor = $Autor;
    }

    function setEditorial($Editorial) {
        $this->Editorial = $Editorial;
    }

    function setAnio($Anio) {
        $this->Anio = $Anio;
    }

    function setEdicion($Edicion) {
        $this->Edicion = $Edicion;
    }

    function setSeccion($Seccion) {
        $this->Seccion = $Seccion;
    }

    function setId_aula($Id_aula) {
        $this->Id_aula = $Id_aula;
    }

    function setDescripcion($Descripcion) {
        $this->Descripcion = $Descripcion;
    }

    function setId_img($Id_img) {
        $this->Id_img = $Id_img;
    }

    function setCodigo($Codigo) {
        $this->Codigo = $Codigo;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    function setBaja_libro($Baja_libro) {
        $this->Baja_libro = $Baja_libro;
    }

    function setValor($Valor) {
        $this->Valor = $Valor;
    }

    function setFecha_adq($Fecha_adq) {
        $this->Fecha_adq = $Fecha_adq;
    }

    function setGarantia_meses($Garantia_meses) {
        $this->Garantia_meses = $Garantia_meses;
    }

    function setComentario_baja($Comentario_baja) {
        $this->Comentario_baja = $Comentario_baja;
    }

    function setDisponible($Disponible) {
        $this->Disponible = $Disponible;
    }

    function setFecha_alta($Fecha_alta) {
        $this->Fecha_alta = $Fecha_alta;
    }

    function setISBN($ISBN) {
        $this->ISBN = $ISBN;
    }

    function setId_cat($Id_cat) {
        $this->Id_cat = $Id_cat;
    }

    function setId_subcat($Id_subcat) {
        $this->Id_subcat = $Id_subcat;
    }


    


}
