<?php

class DocumentosDrive {
    
    public $Id;
    public $DateCreated;
    public $Id_usu;
    public $Nombre;
    public $TipoDoc;
    public $TipoUsuario;
    public $Id_plantel;
    public $UID_drive;
    public $URL;
    public $Id_parent;
    public $Orden;
    
    function __construct() {
        $this->Id = "";
        $this->DateCreated = "";
        $this->Id_usu = "";
        $this->Nombre = "";
        $this->TipoDoc = "";
        $this->TipoUsuario = "";
        $this->Id_plantel = "";
        $this->UID_drive = "";
        $this->URL = "";
        $this->Id_parent = "";
        $this->Orden = "";
    }

    function getId() {
        return $this->Id;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function getId_usu() {
        return $this->Id_usu;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getTipoDoc() {
        return $this->TipoDoc;
    }

    function getTipoUsuario() {
        return $this->TipoUsuario;
    }

    function getId_plantel() {
        return $this->Id_plantel;
    }

    function getUID_drive() {
        return $this->UID_drive;
    }

    function getURL() {
        return $this->URL;
    }

    function getId_parent() {
        return $this->Id_parent;
    }

    function getOrden() {
        return $this->Orden;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setTipoDoc($TipoDoc) {
        $this->TipoDoc = $TipoDoc;
    }

    function setTipoUsuario($TipoUsuario) {
        $this->TipoUsuario = $TipoUsuario;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    function setUID_drive($UID_drive) {
        $this->UID_drive = $UID_drive;
    }

    function setURL($URL) {
        $this->URL = $URL;
    }

    function setId_parent($Id_parent) {
        $this->Id_parent = $Id_parent;
    }

    function setOrden($Orden) {
        $this->Orden = $Orden;
    }



    

}
