<?php

class Eventos {

    public $Id;
    public $Nombre;
    public $Comentarios;
    public $DateCreated;
    public $Id_salon;
    public $Start;
    public $End;
    public $Id_usu;
    public $NombreUtiliza;
    public $Id_ciclo;
    public $Id_plantel;
    public $Activo;
    public $Costo;
    public $TipoEvento;

    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->Comentarios = "";
        $this->DateCreated = "";
        $this->Id_salon = "";
        $this->Start = "";
        $this->End = "";
        $this->Id_usu = "";
        $this->NombreUtiliza = "";
        $this->Id_ciclo = "";
        $this->Id_plantel = "";
        $this->Activo = "";
        $this->Costo = "";
        $this->TipoEvento = "";
    }

    function getTipoEvento() {
        return $this->TipoEvento;
    }

    function setTipoEvento($TipoEvento) {
        $this->TipoEvento = $TipoEvento;
    }

    public function getId() {
        return $this->Id;
    }

    public function getNombre() {
        return $this->Nombre;
    }

    public function getComentarios() {
        return $this->Comentarios;
    }

    public function getDateCreated() {
        return $this->DateCreated;
    }

    public function getId_salon() {
        return $this->Id_salon;
    }

    public function getStart() {
        return $this->Start;
    }

    public function getEnd() {
        return $this->End;
    }

    public function getId_usu() {
        return $this->Id_usu;
    }

    public function getNombreUtiliza() {
        return $this->NombreUtiliza;
    }

    public function getId_ciclo() {
        return $this->Id_ciclo;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function getActivo() {
        return $this->Activo;
    }

    public function getCosto() {
        return $this->Costo;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    public function setComentarios($Comentarios) {
        $this->Comentarios = $Comentarios;
    }

    public function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    public function setId_salon($Id_salon) {
        $this->Id_salon = $Id_salon;
    }

    public function setStart($Start) {
        $this->Start = $Start;
    }

    public function setEnd($End) {
        $this->End = $End;
    }

    public function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    public function setNombreUtiliza($NombreUtiliza) {
        $this->NombreUtiliza = $NombreUtiliza;
    }

    public function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    public function setActivo($Activo) {
        $this->Activo = $Activo;
    }

    public function setCosto($Costo) {
        $this->Costo = $Costo;
    }

}
