<?php
class Dias{
   public  $Id;
   public $Nombre;
   public $Abreviacion;
   
   function __construct() {
       $this->Id = "";
       $this->Nombre ="";
       $this->Abreviacion ="";
   }
   function getAbreviacion() {
       return $this->Abreviacion;
   }

   function setAbreviacion($Abreviacion) {
       $this->Abreviacion = $Abreviacion;
   }

      function getId() {
       return $this->Id;
   }

   function getNombre() {
       return $this->Nombre;
   }

   function setId($Id) {
       $this->Id = $Id;
   }

   function setNombre($Nombre) {
       $this->Nombre = $Nombre;
   }

}

