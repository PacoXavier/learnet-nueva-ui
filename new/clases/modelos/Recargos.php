<?php

class Recargos {
    public $Id;
    public $Id_pago;
    public $Monto_recargo;
    public $Fecha_recargo;
    
    function __construct() {
        $this->Id = "";
        $this->Id_pago = "";
        $this->Monto_recargo = "";
        $this->Fecha_recargo = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getId_pago() {
        return $this->Id_pago;
    }

    public function getMonto_recargo() {
        return $this->Monto_recargo;
    }

    public function getFecha_recargo() {
        return $this->Fecha_recargo;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_pago($Id_pago) {
        $this->Id_pago = $Id_pago;
    }

    public function setMonto_recargo($Monto_recargo) {
        $this->Monto_recargo = $Monto_recargo;
    }

    public function setFecha_recargo($Fecha_recargo) {
        $this->Fecha_recargo = $Fecha_recargo;
    }



   
    
}
