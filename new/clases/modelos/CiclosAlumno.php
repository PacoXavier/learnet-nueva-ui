<?php

class CiclosAlumno {
    public $Id;
    public $Id_ciclo;
    public $Id_grado;
    public $Id_ofe_alum;
    public $Tipo_beca;
    public $Porcentaje_beca;
    public $IdUsuCapBeca;
    public $DiaCapBeca;
    public $MontoBeca;

    function __construct() {
        $this->Id = "";
        $this->Id_ciclo = "";
        $this->Id_grado = "";
        $this->Id_ofe_alum = "";
        $this->Tipo_beca = "";
        $this->Porcentaje_beca = "";
        $this->IdUsuCapBeca = "";
        $this->DiaCapBeca = "";
        $this->MontoBeca = "";
    }
    
    function getIdUsuCapBeca() {
        return $this->IdUsuCapBeca;
    }

    function getDiaCapBeca() {
        return $this->DiaCapBeca;
    }

    function getMontoBeca() {
        return $this->MontoBeca;
    }

    function setIdUsuCapBeca($IdUsuCapBeca) {
        $this->IdUsuCapBeca = $IdUsuCapBeca;
    }

    function setDiaCapBeca($DiaCapBeca) {
        $this->DiaCapBeca = $DiaCapBeca;
    }

    function setMontoBeca($MontoBeca) {
        $this->MontoBeca = $MontoBeca;
    }

    public function getId() {
        return $this->Id;
    }

    public function getId_ciclo() {
        return $this->Id_ciclo;
    }

    public function getId_grado() {
        return $this->Id_grado;
    }

    public function getId_ofe_alum() {
        return $this->Id_ofe_alum;
    }

    public function getTipo_beca() {
        return $this->Tipo_beca;
    }

    public function getPorcentaje_beca() {
        return $this->Porcentaje_beca;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    public function setId_grado($Id_grado) {
        $this->Id_grado = $Id_grado;
    }

    public function setId_ofe_alum($Id_ofe_alum) {
        $this->Id_ofe_alum = $Id_ofe_alum;
    }

    public function setTipo_beca($Tipo_beca) {
        $this->Tipo_beca = $Tipo_beca;
    }

    public function setPorcentaje_beca($Porcentaje_beca) {
        $this->Porcentaje_beca = $Porcentaje_beca;
    }



}
