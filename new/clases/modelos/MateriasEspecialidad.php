<?php

class MateriasEspecialidad {
    
    public $Id;
    public $Id_esp;
    public $Id_mat;
    public $Activo_mat_esp;
    public $Grado_mat;
    public $Evaluacion_extraordinaria;
    public $Evaluacion_especial;
    public $Tiene_orientacion;
    public $NombreDiferente;

    
    function __construct() {
        $this->Id = "";
        $this->Id_esp = "";
        $this->Id_mat = "";
        $this->Activo_mat_esp = "";
        $this->Grado_mat = "";
        $this->Evaluacion_extraordinaria ="";
        $this->Evaluacion_especial = "";
        $this->Tiene_orientacion = "";
        $this->NombreDiferente = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getId_esp() {
        return $this->Id_esp;
    }

    public function getId_mat() {
        return $this->Id_mat;
    }

    public function getActivo_mat_esp() {
        return $this->Activo_mat_esp;
    }

    public function getGrado_mat() {
        return $this->Grado_mat;
    }

    public function getEvaluacion_extraordinaria() {
        return $this->Evaluacion_extraordinaria;
    }

    public function getEvaluacion_especial() {
        return $this->Evaluacion_especial;
    }

    public function getTiene_orientacion() {
        return $this->Tiene_orientacion;
    }

    public function getNombreDiferente() {
        return $this->NombreDiferente;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_esp($Id_esp) {
        $this->Id_esp = $Id_esp;
    }

    public function setId_mat($Id_mat) {
        $this->Id_mat = $Id_mat;
    }

    public function setActivo_mat_esp($Activo_mat_esp) {
        $this->Activo_mat_esp = $Activo_mat_esp;
    }

    public function setGrado_mat($Grado_mat) {
        $this->Grado_mat = $Grado_mat;
    }

    public function setEvaluacion_extraordinaria($Evaluacion_extraordinaria) {
        $this->Evaluacion_extraordinaria = $Evaluacion_extraordinaria;
    }

    public function setEvaluacion_especial($Evaluacion_especial) {
        $this->Evaluacion_especial = $Evaluacion_especial;
    }

    public function setTiene_orientacion($Tiene_orientacion) {
        $this->Tiene_orientacion = $Tiene_orientacion;
    }

    public function setNombreDiferente($NombreDiferente) {
        $this->NombreDiferente = $NombreDiferente;
    }



    
}
