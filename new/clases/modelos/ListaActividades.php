<?php

class ListaActividades {
    public $Id;
    public $Nombre;
    public $Comentarios;
    public $Id_usu;
    public $Id_plantel;
    public $DateCreated;
    public $Order;
    
    
    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->Comentarios = "";
        $this->Id_usu = "";
        $this->Id_plantel = "";
        $this->DateCreated = "";
        $this->Order = "";
    }
    
    public function getOrder() {
        return $this->Order;
    }

    public function setOrder($Order) {
        $this->Order = $Order;
    }

    
    public function getId() {
        return $this->Id;
    }

    public function getNombre() {
        return $this->Nombre;
    }

    public function getComentarios() {
        return $this->Comentarios;
    }

    public function getId_usu() {
        return $this->Id_usu;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function getDateCreated() {
        return $this->DateCreated;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    public function setComentarios($Comentarios) {
        $this->Comentarios = $Comentarios;
    }

    public function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    public function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }



    
}
