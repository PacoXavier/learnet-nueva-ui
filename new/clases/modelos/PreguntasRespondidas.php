<?php

class PreguntasRespondidas {
    
    public $Id;
    public $Id_enc_alum;
    public $Id_pre;
    public $Id_opcion;
    public $DateCreated;

    function __construct() {
        $this->Id = "";
        $this->Id_enc_alum = "";
        $this->Id_pre = "";
        $this->Id_opcion = "";
        $this->DateCreated = "";
    }

    function getId() {
        return $this->Id;
    }

    function getId_enc_alum() {
        return $this->Id_enc_alum;
    }

    function getId_pre() {
        return $this->Id_pre;
    }

    function getId_opcion() {
        return $this->Id_opcion;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_enc_alum($Id_enc_alum) {
        $this->Id_enc_alum = $Id_enc_alum;
    }

    function setId_pre($Id_pre) {
        $this->Id_pre = $Id_pre;
    }

    function setId_opcion($Id_opcion) {
        $this->Id_opcion = $Id_opcion;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }


    
    
}
