<?php

class Justificaciones {
    
    public $Id;
    public $Id_alum;
    public $Id_ciclo;
    public $Dia_justificado;
    public $Id_usu;
    public $Date;
    public $Comentarios;
    public $HoraIni;
    public $HoraFin;

    function __construct() {
        $this->Id = "";
        $this->Id_alum = "";
        $this->Id_ciclo = "";
        $this->Dia_justificado = "";
        $this->Id_usu = "";
        $this->Date = "";
        $this->Comentarios = "";
        $this->HoraIni = "";
        $this->HoraFin = "";
    }
    
    function getId_alum() {
        return $this->Id_alum;
    }

    function setId_alum($Id_alum) {
        $this->Id_alum = $Id_alum;
    }

        
    function getId() {
        return $this->Id;
    }

    function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function getDia_justificado() {
        return $this->Dia_justificado;
    }

    function getId_usu() {
        return $this->Id_usu;
    }

    function getDate() {
        return $this->Date;
    }

    function getComentarios() {
        return $this->Comentarios;
    }

    function getHoraIni() {
        return $this->HoraIni;
    }

    function getHoraFin() {
        return $this->HoraFin;
    }

    function setId($Id) {
        $this->Id = $Id;
    }



    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    function setDia_justificado($Dia_justificado) {
        $this->Dia_justificado = $Dia_justificado;
    }

    function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    function setDate($Date) {
        $this->Date = $Date;
    }

    function setComentarios($Comentarios) {
        $this->Comentarios = $Comentarios;
    }

    function setHoraIni($HoraIni) {
        $this->HoraIni = $HoraIni;
    }

    function setHoraFin($HoraFin) {
        $this->HoraFin = $HoraFin;
    }

}
