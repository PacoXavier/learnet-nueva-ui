<?php

class EventosAutoAsignados {
    
    public $Id;
    public $Nombre;
    public $Modelo;
    public $Descripcion;
    public $HoraInicio;
    public $HoraFin;
    public $FechaIni;
    public $FechaFin;
    public $IdRel;
    public $TipoRel;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->Modelo = "";
        $this->Descripcion = "";
        $this->HoraInicio = "";
        $this->HoraFin = "";
        $this->FechaIni = "";
        $this->FechaFin = "";
        $this->IdRel = "";
        $this->TipoRel = "";
    }
    function getFechaFin() {
        return $this->FechaFin;
    }

    function setFechaFin($FechaFin) {
        $this->FechaFin = $FechaFin;
    }

        function getId() {
        return $this->Id;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getModelo() {
        return $this->Modelo;
    }

    function getDescripcion() {
        return $this->Descripcion;
    }

    function getHoraInicio() {
        return $this->HoraInicio;
    }

    function getHoraFin() {
        return $this->HoraFin;
    }

    function getFechaIni() {
        return $this->FechaIni;
    }

    function getIdRel() {
        return $this->IdRel;
    }

    function getTipoRel() {
        return $this->TipoRel;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setModelo($Modelo) {
        $this->Modelo = $Modelo;
    }

    function setDescripcion($Descripcion) {
        $this->Descripcion = $Descripcion;
    }

    function setHoraInicio($HoraInicio) {
        $this->HoraInicio = $HoraInicio;
    }

    function setHoraFin($HoraFin) {
        $this->HoraFin = $HoraFin;
    }

    function setFechaIni($FechaIni) {
        $this->FechaIni = $FechaIni;
    }

    function setIdRel($IdRel) {
        $this->IdRel = $IdRel;
    }

    function setTipoRel($TipoRel) {
        $this->TipoRel = $TipoRel;
    }



}
