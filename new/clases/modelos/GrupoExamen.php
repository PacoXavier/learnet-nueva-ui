<?php

class GrupoExamen {
    public $Id;
    public $IdGrupo;
    public $IdHorarioExamen;
    
    function __construct() {
        $this->Id = "";
        $this->IdGrupo = "";
        $this->IdHorarioExamen = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getIdGrupo() {
        return $this->IdGrupo;
    }

    function getIdHorarioExamen() {
        return $this->IdHorarioExamen;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setIdGrupo($IdGrupo) {
        $this->IdGrupo = $IdGrupo;
    }

    function setIdHorarioExamen($IdHorarioExamen) {
        $this->IdHorarioExamen = $IdHorarioExamen;
    }



}
