<?php

class OfertasInteres {
    
    public $Id;
    public $Id_ofe;
    public $Id_alum;
    public $Id_esp;
    public $Id_ori;
    public $Id_ciclo;
    public $Turno;
    public $DateCreated;
    public $Id_usu;
    
    function __construct() {
        $this->Id = "";
        $this->Id_ofe = "";
        $this->Id_alum = "";
        $this->Id_esp = "";
        $this->Id_ori = "";
        $this->Id_ciclo = "";
        $this->Turno = "";
        $this->DateCreated = "";
        $this->Id_usu = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getId_ofe() {
        return $this->Id_ofe;
    }

    function getId_alum() {
        return $this->Id_alum;
    }

    function getId_esp() {
        return $this->Id_esp;
    }

    function getId_ori() {
        return $this->Id_ori;
    }

    function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function getTurno() {
        return $this->Turno;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function getId_usu() {
        return $this->Id_usu;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_ofe($Id_ofe) {
        $this->Id_ofe = $Id_ofe;
    }

    function setId_alum($Id_alum) {
        $this->Id_alum = $Id_alum;
    }

    function setId_esp($Id_esp) {
        $this->Id_esp = $Id_esp;
    }

    function setId_ori($Id_ori) {
        $this->Id_ori = $Id_ori;
    }

    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    function setTurno($Turno) {
        $this->Turno = $Turno;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

}
