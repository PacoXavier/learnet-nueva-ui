<?php
class Grados {
    
    public $Id;
    public $Id_esp;
    public $Grado;
    public $Activo_grado;
   
    function __construct() {
        $this->Id = "";
        $this->Id_esp = "";
        $this->Grado = "";
        $this->Activo_grado = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getId_esp() {
        return $this->Id_esp;
    }

    public function getGrado() {
        return $this->Grado;
    }

    public function getActivo_grado() {
        return $this->Activo_grado;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_esp($Id_esp) {
        $this->Id_esp = $Id_esp;
    }

    public function setGrado($Grado) {
        $this->Grado = $Grado;
    }

    public function setActivo_grado($Activo_grado) {
        $this->Activo_grado = $Activo_grado;
    }



}
