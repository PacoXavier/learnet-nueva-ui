<?php

class MetodosPago {
    public $Id;
    public $Nombre;
    public $Id_plantel;
    public $Activo;

    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->Id_plantel ="";
        $this->Activo = "";
    }

    public function getId() {
        return $this->Id;
    }

    public function getNombre() {
        return $this->Nombre;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function getActivo() {
        return $this->Activo;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    public function setActivo($Activo) {
        $this->Activo = $Activo;
    }



    
    
}
