<?php

class PaquetesDescuentosAlumno {
    
    public $Id;
    public $Id_ofe_alum;
    public $Id_paq;
    public $Porcentaje;
    public $FechaCaptura;
    public $Id_ciclo;
    public $Id_usu_captura;

    function __construct() {
        $this->Id = "";
        $this->Id_ofe_alum = "";
        $this->Id_paq = "";
        $this->Porcentaje = "";
        $this->FechaCaptura = "";
        $this->Id_ciclo = "";
        $this->Id_usu_captura = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getId_ofe_alum() {
        return $this->Id_ofe_alum;
    }

    function getId_paq() {
        return $this->Id_paq;
    }

    function getPorcentaje() {
        return $this->Porcentaje;
    }

    function getFechaCaptura() {
        return $this->FechaCaptura;
    }

    function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function getId_usu_captura() {
        return $this->Id_usu_captura;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_ofe_alum($Id_ofe_alum) {
        $this->Id_ofe_alum = $Id_ofe_alum;
    }

    function setId_paq($Id_paq) {
        $this->Id_paq = $Id_paq;
    }

    function setPorcentaje($Porcentaje) {
        $this->Porcentaje = $Porcentaje;
    }

    function setFechaCaptura($FechaCaptura) {
        $this->FechaCaptura = $FechaCaptura;
    }

    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    function setId_usu_captura($Id_usu_captura) {
        $this->Id_usu_captura = $Id_usu_captura;
    }


}