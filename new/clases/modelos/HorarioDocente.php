<?php

class HorarioDocente {

    public $Id;
    public $Id_grupo;
    public $Hora;
    public $Aula;
    public $Lunes;
    public $Martes;
    public $Miercoles;
    public $Jueves;
    public $Viernes;
    public $Sabado;
    public $Domingo;
    public $Id_ciclo;
    public $Id_docente;
    
    function __construct() {
        $this->Id = "";
        $this->Id_grupo = "";
        $this->Hora = "";
        $this->Aula = "";
        $this->Lunes = "";
        $this->Martes = "";
        $this->Miercoles = "";
        $this->Jueves = "";
        $this->Viernes = "";
        $this->Sabado = "";
        $this->Domingo = "";
        $this->Id_ciclo = "";
        $this->Id_docente = "";
    }
    
    function getDomingo() {
        return $this->Domingo;
    }

    function setDomingo($Domingo) {
        $this->Domingo = $Domingo;
    }

    function getId() {
        return $this->Id;
    }

    function getId_grupo() {
        return $this->Id_grupo;
    }

    function getHora() {
        return $this->Hora;
    }

    function getAula() {
        return $this->Aula;
    }

    function getLunes() {
        return $this->Lunes;
    }

    function getMartes() {
        return $this->Martes;
    }

    function getMiercoles() {
        return $this->Miercoles;
    }

    function getJueves() {
        return $this->Jueves;
    }

    function getViernes() {
        return $this->Viernes;
    }

    function getSabado() {
        return $this->Sabado;
    }

    function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function getId_docente() {
        return $this->Id_docente;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_grupo($Id_grupo) {
        $this->Id_grupo = $Id_grupo;
    }

    function setHora($Hora) {
        $this->Hora = $Hora;
    }

    function setAula($Aula) {
        $this->Aula = $Aula;
    }

    function setLunes($Lunes) {
        $this->Lunes = $Lunes;
    }

    function setMartes($Martes) {
        $this->Martes = $Martes;
    }

    function setMiercoles($Miercoles) {
        $this->Miercoles = $Miercoles;
    }

    function setJueves($Jueves) {
        $this->Jueves = $Jueves;
    }

    function setViernes($Viernes) {
        $this->Viernes = $Viernes;
    }

    function setSabado($Sabado) {
        $this->Sabado = $Sabado;
    }

    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    function setId_docente($Id_docente) {
        $this->Id_docente = $Id_docente;
    }



   
}
