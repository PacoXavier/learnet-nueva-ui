<?php

class PenalizacionDocente{
    
    public $Id_pen;
    public $DateCreated;
    public $Id_usu_created;
    public $Fecha;
    public $Grupo;
    public $Id_ciclo;
    public $Comentarios;
    public $Hora;
    public $Id_docen;
    
    function __construct() {
        $this->Id_pen = "";
        $this->DateCreated = "";
        $this->Id_usu_created = "";
        $this->Fecha = "";
        $this->Grupo = "";
        $this->Id_ciclo = "";
        $this->Comentarios = "";
        $this->Hora = "";
        $this->Id_docen = "";
    }
    
    function getId() {
        return $this->Id_pen;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function getId_usu_created() {
        return $this->Id_usu_created;
    }

    function getFecha() {
        return $this->Fecha;
    }

    function getGrupo() {
        return $this->Grupo;
    }

    function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function getComentarios() {
        return $this->Comentarios;
    }

    function getHora() {
        return $this->Hora;
    }

    function getId_docen() {
        return $this->Id_docen;
    }

    function setId($Id_pen) {
        $this->Id_pen = $Id_pen;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    function setId_usu_created($Id_usu_created) {
        $this->Id_usu_created = $Id_usu_created;
    }

    function setFecha($Fecha) {
        $this->Fecha = $Fecha;
    }

    function setGrupo($Grupo) {
        $this->Grupo = $Grupo;
    }

    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    function setComentarios($Comentarios) {
        $this->Comentarios = $Comentarios;
    }

    function setHora($Hora) {
        $this->Hora = $Hora;
    }

    function setId_docen($Id_docen) {
        $this->Id_docen = $Id_docen;
    }



    
}
