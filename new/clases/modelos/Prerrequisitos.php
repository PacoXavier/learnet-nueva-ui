<?php
class Prerrequisitos {
    
    public $Id;
    public $Id_mat_esp;
    public $Id_mat_esp_pre;

    function __construct() {
        $this->Id = "";
        $this->Id_mat_esp = "";
        $this->Id_mat_esp_pre = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getId_mat_esp() {
        return $this->Id_mat_esp;
    }

    function getId_mat_esp_pre() {
        return $this->Id_mat_esp_pre;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_mat_esp($Id_mat_esp) {
        $this->Id_mat_esp = $Id_mat_esp;
    }

    function setId_mat_esp_pre($Id_mat_esp_pre) {
        $this->Id_mat_esp_pre = $Id_mat_esp_pre;
    }



}