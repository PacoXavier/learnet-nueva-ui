<?php
require_once('Connections.php');
require_once("cURL.php");

class base extends Connections {
    
    public $Id_p="";

    public function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
        $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
        $theValue = function_exists("real_escape_string") ? $this->_cnn->real_escape_string($theValue) : $this->_cnn->real_escape_string($theValue);
        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
    
    public function getParam($param,$Id_plantel=null) {
        $q="";
        if($Id_plantel!=null){
           $q=" AND Id_plantel=".$Id_plantel;
        }
        $query = "SELECT * FROM parametros WHERE text_param='$param' ".$q;
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                do {
                    $resp = $row_consulta["valor_param"];
                } while ($row_consulta = $consulta->fetch_assoc());
            }
        }
        return $resp;
    }
    
    public function getParametros($Id_plantel) {
        $query = "SELECT * FROM parametros WHERE Id_plantel=".$Id_plantel;
        return $this->advanced_query($query);
    }

    public function hashPassword($password) {
        $salt = "mdsksprwuhej6skhs0b08ojx6";
        //encrypt the password, rotate characters by length of original password
        $len = strlen($password);
        $password = md5($password);
        $password = $this->rotateHEX($password, $len);
        return md5($salt . $password);
    }

    public function rotateHEX($string, $n) {
        //for more security, randomize this string
        $chars = "4hxdxsyx3fkygsg8";
        $str = "";
        for ($i = 0; $i < strlen($string); $i++) {
            $pos = strpos($chars, $string[$i]);
            $pos += $n;
            if ($pos >= strlen($chars))
                $pos = $pos % strlen($chars);
            $str.=$chars[$pos];
        }
        return $str;
    }

    public function formatFecha($fechaSQL, $corta = 0) {
        $anioSQL = substr($fechaSQL, 0, 4);
        $mesSQL = substr($fechaSQL, 5, 2);
        $mesSQL = $mesSQL * 1;
        $mesSQL = $this->mesLetra($mesSQL);
        $diaSQL = substr($fechaSQL, 8, 2);
        $diaSQL = $diaSQL * 1;
        if ($corta == 0) {
            return ($diaSQL . " de " . $mesSQL . ", " . $anioSQL);
        } else {
            return ($diaSQL . " " . $mesSQL . ", " . substr($anioSQL, 2, 2));
        }
    }

    public function formatFecha_hora($fechaSQL) {
        $fecha = $this->formatFecha($fechaSQL);
        return $fecha = $fecha . substr($fechaSQL, strpos($fechaSQL, ' '));
    }

    public function mesLetra($mesActual) {
        if ($mesActual == 1) {
            return("Ene");
        }
        if ($mesActual == 2) {
            return("Feb");
        }
        if ($mesActual == 3) {
            return("Mar");
        }
        if ($mesActual == 4) {
            return("Abr");
        }
        if ($mesActual == 5) {
            return("May");
        }
        if ($mesActual == 6) {
            return("Jun");
        }
        if ($mesActual == 7) {
            return("Jul");
        }
        if ($mesActual == 8) {
            return("Ago");
        }
        if ($mesActual == 9) {
            return("Sep");
        }
        if ($mesActual == 10) {
            return("Oct");
        }
        if ($mesActual == 11) {
            return("Nov");
        }
        if ($mesActual == 12) {
            return("Dic");
        }
    }

    public function generarKey($largo = 13) {
        $str="";
        $var_count = 0;
        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((double) microtime() * 1000000);
        while ($var_count < $largo) {
            $num = rand() % 33;
            $str.=substr($chars, $num, 1);
            $var_count = $var_count + 1;
        }
        return($str);
    }

    public function generar_matricula($Id_plantel) {
        //Generamos la matricula del alumno
        //Tomando como base el primer digito es del plantel al que pertence
        //y los restantes son la ultima matricula existente
        $query = "SELECT * FROM inscripciones_ulm WHERE  Matricula IS NOT NULL AND Id_plantel=".$Id_plantel." ORDER BY Matricula DESC LIMIT 1";
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                $matricula = substr($row_consulta['Matricula'], 1);
                $matricula = (int) $Id_plantel . $matricula;
                $matricula++;
            }
        }
        return $matricula;
    }

    public function generar_referenciaPago($matricula) {
        //Se separa la matricula por digitos
        //$matricula=57134679;
        $array_matricula = str_split($matricula);
        //Iniciando de derecha a izquierda, tomamos los dígitos que conforman el número 
        //y los iremos multiplicando secuencialmente por 2 y por 1, respectivamente, hasta 
        //tomar el último dígito que conforma el número		
        $bandera = 0;
        $array_multi_secuencial = array();
        for ($i = (count($array_matricula) - 1); $i >= 0; $i--) {
            if ($bandera == 0) {
                $digito = "2";
                $bandera = 1;
            } else {
                $digito = "1";
                $bandera = 0;
            }
            array_unshift($array_multi_secuencial, ($array_matricula[$i] * $digito));
        }
        //Tomamos ahora los productos de las multiplicaciones anteriores y los sumamos.																			
        //En caso de que algunos de los productos estén integrados por dos dígitos, se separarán
        // los los dos dígitos y se suma cada uno de ellos individualmente																			

        $suma_productos = array();
        foreach ($array_multi_secuencial as $v) {
            if (strlen($v) == 2) {
                $digitos = str_split($v);
                array_push($suma_productos, ($digitos[0] + $digitos[1]));
            } else {
                array_push($suma_productos, $v);
            }
        }
        //Con el resultado de la suma de productos (40), lo dividimos entre 10													
        $total = 0;
        foreach ($suma_productos as $v) {
            $total+= $v;
        }
        //Se resta de 10, el Remanente de la division
        $remanente = ($total % 10);
        //Se resta de 10, el Remanente de la división.
        $digitoVerificador = 10 - $remanente;
        // Si el remanente de la división entre 10 es 0, el dígito verificador será 0 (cero).
        if ($remanente == 0) {
            $digitoVerificador = 0;
        }
        return $matricula . $digitoVerificador;
    }
    
    public function getParamPlantel($param,$Id_plantel) {
        $query = "SELECT * FROM parametros 
                JOIN ParametrosPlantel ON parametros.Id_param=ParametrosPlantel.Id_param
                WHERE parametros.text_param='$param' AND ParametrosPlantel.Id_plantel=".$Id_plantel;
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                do{
                    $resp = $row_consulta["Valor"];
                } while ($row_consulta = $consulta->fetch_assoc());
            }
        }
        return $resp;
    }
    

    public function send_email($data) {
        if(isset($data['Id_plantel']) && $data['Id_plantel']>0){
            $this->Id_p=$data['Id_plantel'];
        }
        $mensaje="";
        if(isset($data['Mensaje']) && strlen($data['Mensaje'])>0){
            $mensaje=$data['Mensaje'];
        }
        $plantel=$this->getPlantel();
        $direccionPlantel=$this->getDireccionPlantel();

        $texto_html = ' <html>
          <head></head>
          <body style="background-color: #F8F8F8;padding-top: 40px;" bgcolor="#F8F8F8">
                          <div id="container" style="width: 600px; background-color: white; font-family: arial; font-size: 13px; margin: auto;">
                               <div id="boxtext" style="width: 540px; padding: 30px;min-height: 300px;">
                               <img src="http://www.'.$plantel['Dominio'].'/admin_ce/files/'.$plantel['Id_img_logo'].'.jpg">
                                   <h1 style="font-size: 25px; font-family: arial; color: #606060; font-weight: 100; margin-top: 20px;margin-bottom: 20px;
margin-bottom: 15px">' . $data['Asunto'] . '</h1>
                                   ' . $mensaje . '
                               </div>
                              <div id="footer" style="background-color: #606060; color: white; font-size: 13px; width: 540px; padding: 30px;padding-top: 5px;padding-bottom: 5px;">
                           <table>
                                    <tr>
                                           <td style="vertical-align: top; padding-right: 5px;" valign="top">
                                                  <div id="text_footer" style="width: 226px; font-size: 13px; color: white; font-family: arial;">
                                                          <p style="margin-bottom: 10px;">Tel&eacute;fonos:<br> '.$plantel['Tel'].' </p>
                                                          <p style="margin-bottom: 10px;">Domicilio: <br>'.$direccionPlantel['Calle_dir'].' No. '.$direccionPlantel['NumExt_dir'].' Col. '.$direccionPlantel['Colonia_dir'].' '.$direccionPlantel['Ciudad_dir'].', '.$direccionPlantel['Estado_dir'].'</p>
                                                   </div>
                                           </td>
                                   </tr></table>
          </div>
                        </div>
                         <p class="footer" style="font-family: arial; font-size: 10px; color: black; width: 600px; margin: 5px auto auto;">' . date('Y') . ' '.$plantel['Abreviatura'].' '.$plantel['Nombre_plantel'].'</p>
                  </body>
          </html>';

        $texto_txt = '
********************************
' . $data['Asunto'] . '
********************************

**********************
Mensaje
**********************


Mensaje: ' . $mensaje . '



Teléfonos:  '.$plantel['Tel'].'
Domicilio:  '.$direccionPlantel['Calle_dir'].' No. '.$direccionPlantel['NumExt_dir'].' Col. '.$direccionPlantel['Colonia_dir'].' '.$direccionPlantel['Ciudad_dir'].', '.$direccionPlantel['Estado_dir'].'

' . date('Y') . '  '.$plantel['Nombre_plantel'];

        if (isset($data['Html']) && strlen($data['Html']) > 0) {
            $texto_html = $data['Html'];
        }

        if (isset($data['Texto']) && strlen($data['Texto']) > 0) {
            $texto_txt = $data['Texto'];
        }
        
        $emailEnvia=$plantel['Email'];
        $nombreEnvia=$plantel['Nombre_plantel'];
        if(isset($data['email-usuario']) && strlen($data['email-usuario'])>0){
           $emailEnvia=$data['email-usuario'];
           $nombreEnvia=$data['nombre-usuario'];
        }

        $KeySistema = $this->getParamPlantel('KeyMandrill',$plantel['Id_plantel']);
        //$KeySistema=  $this->getParamPlantel('TestMandrill',$plantel['Id_plantel']);
        $arrayParams = array();
        $arrayParams['key'] = $KeySistema;

        $arrayParams['message']['html'] = $texto_html;
        $arrayParams['message']['text'] = $texto_txt;
        $arrayParams['message']['subject'] = $data['Asunto'];
        $arrayParams['message']['from_email'] = $emailEnvia;
        $arrayParams['message']['from_name'] = $nombreEnvia;

        
        $arrayParams['message']['to'] = array();
        foreach ($data['Destinatarios'] as $k => $v) {
            $nombre="";
            if(isset($v['nombre'])){
               $nombre=$v['nombre'];
            }
            $arrayTo = array();
            $arrayTo["email"] = $v['email'];
            $arrayTo["name"] = $nombre;
            array_push($arrayParams['message']['to'], $arrayTo);
        }
        
        if(isset($data['replyTo']) && strlen($data['replyTo'])>0){
            $arrayParams['message']['headers']['reply-to']=$data['replyTo'];
        }

        $array_global_merge_vars = array();
        $array_global_merge_vars["name"] = 'example name';
        $array_global_merge_vars["content"] = 'example content';

        $arrayParams['message']['global_merge_vars'] = array();
        array_push($arrayParams['message']['global_merge_vars'], $array_global_merge_vars);

        $array_merge_vars = array();
        $array_merge_vars["rcpt"] = 'example rcpt';

        $array_merge_vars['vars'] = array();
        $array_vars_attr = array();
        $array_vars_attr['name'] = 'example name';
        $array_vars_attr['content'] = 'example content';
        array_push($array_merge_vars['vars'], $array_vars_attr);

        $arrayParams['message']['merge_vars'] = array();
        array_push($arrayParams['message']['merge_vars'], $array_merge_vars);

        $arrayParams['message']['tags'] = array();
        array_push($arrayParams['message']['tags'], "example tags[]");

        $arrayParams['message']['google_analytics_domains'] = array();
        array_push($arrayParams['message']['google_analytics_domains'], "...");

        $arrayParams['message']['google_analytics_campaign'] = "...";
        $arrayParams['message']['metadata'] = array();
        array_push($arrayParams['message']['metadata'], "...");

        $array_recipient_metadata = array();
        $array_recipient_metadata["rcpt"] = 'example rcpt';

        $array_recipient_metadata['values'] = array();
        array_push($array_recipient_metadata['values'], "...");

        $arrayParams['message']['recipient_metadata'] = array();
        array_push($arrayParams['message']['recipient_metadata'], $array_recipient_metadata);

        $arrayParams['message']['attachments'] = array();
        if(isset($data['IdRel']) && strlen($data['IdRel'])>0 && isset($data['TipoRel']) && strlen($data['TipoRel'])>0 ){
            $query = "SELECT * FROM Attachments WHERE IdRel=".$data['IdRel']." AND TipoRel='".$data['TipoRel']."' AND Id_plantel=".$plantel['Id_plantel'];
            foreach($this->advanced_query($query) as $row_consulta){
                    $file = file_get_contents("attachments/" . $row_consulta['Llave_atta'] . ".ulm");
                    $arrarAttrAttachments = array();
                    $arrarAttrAttachments['type'] = $row_consulta['Mime_atta'];
                    $arrarAttrAttachments['name'] = $row_consulta['Nombre_atta'];
                    $arrarAttrAttachments['content'] = base64_encode($file);
                    array_push($arrayParams['message']['attachments'], $arrarAttrAttachments);
            }
        }


        $arrayParams['message']['async'] = '...';

        //create a new cURL resource
        $cc = new cURL();
        $url = "https://mandrillapp.com/api/1.0/messages/send.json";
        //send-template.json"; mensajes con temaplate
        //messages/send.json   mensajes normales
        //messages/search.json busquedas
        $data = $cc->post($url, json_encode($arrayParams));

        //Leer cadena json recibida
        //La cadena recibida tienes que cortarla asta que apetezca puro json
        $data = substr($data, strpos($data, "{"));
        $data = substr($data, 0, strpos($data, "}") + 1);
        $arr = json_decode($data, true);
        //$arr['email']; 
        //$arr['status']; 
        //$arr['_id']; 
        echo json_encode($arr);
    }
    
    public function getUsuarioPlantel() {
        $Id_usu=$_COOKIE['admin/Id_usu'];
        $query="SELECT * FROM usuarios_ulm WHERE Id_usu=".$Id_usu;
        $consulta = $this->_cnn->query($query);
        if(!$consulta){
             throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        }else{
             $row_consulta = $consulta->fetch_assoc();
             return $row_consulta;
        }
        return false;
    }
    
    
    public function getPlantel() {
        if($this->Id_p>0){
            $Id=$this->Id_p;
        }else{
            $usu=$this->getUsuarioPlantel();
            $Id=$usu['Id_plantel'];
        }
        if($Id>0){
            $query="SELECT * FROM planteles_ulm WHERE Id_plantel=".$Id;
            $consulta = $this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
                 return $row_consulta;
            }
        }
        return false;
    }
    
    
    public function getDireccionPlantel() {
        $plantel=$this->getPlantel();
        $Id=$plantel['Id_dir_plantel'];
        
        if($Id>0){
            $query="SELECT * FROM direcciones_uml WHERE Id_dir=".$Id;
            $consulta = $this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
                 return $row_consulta;
            }
        }
        return false;
    }

    public function get_day_pago($dateActual, $Id_ciclo) {
        $fechas = array();
        $query = "SELECT * FROM Dias_inhabiles_ciclo WHERE Id_ciclo=" . $Id_ciclo;
        foreach ($this->advanced_query($query) as $k => $v) {
            array_push($fechas, $v['fecha_inh']);
        }

        $dia = date("d", strtotime($dateActual));
        $mes = date("m", strtotime($dateActual));
        $anio = date("Y", strtotime($dateActual));

        $diaDelaSemana = date('w', strtotime($dateActual));
        //Si es domingo o sabado se incrementa un dia
        if ($diaDelaSemana == 0 || $diaDelaSemana == 6) {
            $dateActual = date("Y-m-d", mktime(0, 0, 0, $mes, $dia + 1, $anio));
            $dateActual = $this->get_day_pago($dateActual, $Id_ciclo);
        } else {
            $dateActual = $dateActual;
        }
        //Comprovamos que el dia actual no se 
        //encuentre  entre  los dias inhabiles 
        //del ciclo
        foreach ($fechas as $k => $v) {
            if ($v == $dateActual) {
                $dia = date("d", strtotime($dateActual));
                $mes = date("m", strtotime($dateActual));
                $anio = date("Y", strtotime($dateActual));
                $dateActual = date("Y-m-d", mktime(0, 0, 0, $mes, $dia + 1, $anio));
                $dateActual = $this->get_day_pago($dateActual, $Id_ciclo);
            }
        }
        return $dateActual;
    }

    public function get_contacto($Id_rel, $tipo) {
        $resp = array();
        $query_Contactos = "SELECT * FROM Contactos WHERE IdRel_con=" . $Id_rel . " AND TipoRel_con='" . $tipo . "'";
        $Contactos = mysql_query($query_Contactos, $this->_cnn) or die(mysql_error());
        $row_Contactos = mysql_fetch_array($Contactos);
        $totalRows_Contactos = mysql_num_rows($Contactos);
        if ($totalRows_Contactos > 0) {
            do {
                $resp['Id_cont'] = $row_Contactos['Id_cont'];
                $resp['Nombre'] = $row_Contactos['Nombre'];
                $resp['Parentesco'] = $row_Contactos['Parentesco'];
                $resp['Direccion'] = $row_Contactos['Direccion'];
                $resp['Tel_casa'] = $row_Contactos['Tel_casa'];
                $resp['Tel_ofi'] = $row_Contactos['Tel_ofi'];
                $resp['Cel'] = $row_Contactos['Cel'];
                $resp['Email'] = $row_Contactos['Email'];
                $resp['IdRel_con'] = $row_Contactos['IdRel_con'];
                $resp['TipoRel_con'] = $row_Contactos['TipoRel_con'];
            } while ($row_Contactos = mysql_fetch_array($Contactos));
        }
        return $resp;
    }

    public function calcularEdad($fecha_nacimiento) {
        list($y, $m, $d) = explode("-", $fecha_nacimiento);
        $y_dif = date("Y") - $y;
        $m_dif = date("m") - $m;
        $d_dif = date("d") - $d;
        if ((($d_dif < 0) && ($m_dif == 0)) || ($m_dif < 0))
            $y_dif--;
        return $y_dif;
    }

    public function mesActual($mesActual) {
        if ($mesActual == 1) {
            return("Enero");
        }
        if ($mesActual == 2) {
            return("Febrero");
        }
        if ($mesActual == 3) {
            return("Marzo");
        }
        if ($mesActual == 4) {
            return("Abril");
        }
        if ($mesActual == 5) {
            return("Mayo");
        }
        if ($mesActual == 6) {
            return("Junio");
        }
        if ($mesActual == 7) {
            return("Julio");
        }
        if ($mesActual == 8) {
            return("Agosto");
        }
        if ($mesActual == 9) {
            return("Septiembre");
        }
        if ($mesActual == 10) {
            return("Octubre");
        }
        if ($mesActual == 11) {
            return("Noviembre");
        }
        if ($mesActual == 12) {
            return("Diciembre");
        }
    }

    public function get_archivos_entregados($Id_ofe_alum) {
        $DebeArchivos = 0;
        $query_archivos = "SELECT Id_ofe_alum,Id_oferta,Nombre_oferta, archivos_ofertas_ulm.Id_file,Nombre_file, ArchivosAlum.* from ofertas_alumno
     JOIN ofertas_ulm ON ofertas_alumno.Id_ofe=ofertas_ulm.Id_oferta
     JOIN archivos_ofertas_ulm ON ofertas_ulm.Id_oferta=archivos_ofertas_ulm.Id_ofe
     JOIN archivos_ulm ON archivos_ofertas_ulm.Id_file=archivos_ulm.Id_file
     LEFT JOIN (SELECT Id_file AS ArchivoEntregado FROM archivos_alum_ulm WHERE Id_ofe_alum=" . $Id_ofe_alum . ") AS ArchivosAlum ON archivos_ulm.Id_file=ArchivosAlum.ArchivoEntregado
     WHERE Id_ofe_alum=" . $Id_ofe_alum;
        $archivos = mysql_query($query_archivos, $this->_cnn) or die(mysql_error());
        $row_archivos = mysql_fetch_array($archivos);
        $totalRows_archivos = mysql_num_rows($archivos);
        if ($totalRows_archivos > 0) {
            do {
                if ($row_archivos['ArchivoEntregado'] == NULL) {
                    $DebeArchivos++;
                }
            } while ($row_archivos = mysql_fetch_array($archivos));
        }
        return $DebeArchivos;
    }

    public function advanced_query($query) {
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error . " Query: $query");
        } else {
            return $consulta;
        }
        return "";
    }

    //Funcion para recorrer las celdas
    public function xrange($start, $end, $limit = 1000) {
        $l = array();
        while ($start !== $end && count($l) < $limit) {
            $l[] = $start;
            $start ++;
        }
        $l[] = $end;
        return $l;
    }

    public function porcentajeAsistencias($Asistencias, $Justificaciones, $Faltas) {
        $totalAsistencias=$Asistencias + $Faltas;
        if($totalAsistencias>0){
           return round((($Asistencias + $Justificaciones) / ($totalAsistencias) * 100), 0, PHP_ROUND_HALF_ODD);
        }else{
           return 0;
        }
    }

    public function simpleCurl($method, $addData = false, $url, $data, $show_headers = false) {
        $headers = array();
        if (count($addData) > 0) {
            foreach ($addData as $value) {
                array_push($headers, $value);
            }
        } else {
            array_push($headers, 'Connection: Keep-Alive');
            array_push($headers, 'Content-type: application/x-www-form-urlencoded;charset=UTF-8');
        }

        $process = curl_init($url);
        curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($process, CURLOPT_HEADER, $show_headers);
        curl_setopt($process, CURLINFO_HEADER_OUT, $show_headers);
        //curl_setopt($process, CURLOPT_INTERFACE, "75.126.74.124");
        //if($interface!==false){
        //	 
        //}
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
        if ($method == "POST") {
            curl_setopt($process, CURLOPT_POSTFIELDS, implode("&", $data));
            curl_setopt($process, CURLOPT_POST, 1);
        } elseif ($method == "PATCH") {
            curl_setopt($process, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($process, CURLOPT_POSTFIELDS, $data);
            curl_setopt($process, CURLOPT_POST, 1);
        } elseif ($method == "PUT") {

            curl_setopt($process, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($process, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($process, CURLOPT_POSTFIELDS, implode("&", $data));
        } elseif ($method == "DELETE") {
            curl_setopt($process, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($process, CURLOPT_CUSTOMREQUEST, "DELETE");
        }

        $return = curl_exec($process);
        curl_close($process);
        return $return;
    }

    public function getTotalRows($query) {
        $totalRows_consulta=0;
        $consulta = $this->_cnn->query($query);
        if(!$consulta){
             throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        }else{
             $row_consulta = $consulta->fetch_assoc();
        }
        $totalRows_consulta= $consulta->num_rows;

        return $totalRows_consulta;
    }
    
    public function redonderCalificacion($val,$presicion=0){
        return round($val,$presicion);
    }
    
    public function covertirCadena($texto,$tipo){
            if($tipo==0){
                //Minusculas
                $cadena=mb_strtolower ($texto,"UTF-8");
            }elseif($tipo==1){
                //Mayusculas
                $cadena=mb_strtoupper($texto,"UTF-8");
            }elseif($tipo==2){
                $cadena = ucfirst(mb_strtolower($texto,"UTF-8")); 
                //$bar = ucfirst(strtolower($bar)); // Hello world!
            }
            return $cadena;
    }
    
    public function getDiasRango($FechaIni,$FechaFin){
         //Fecha de inicio y fin de ciclo
        $fechas=array();
        $fechaInicio=strtotime($FechaIni);
        $fechaFin=strtotime($FechaFin);
        //Recorremos el rango de dias del ciclo para saber cuantos meses hay
        for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){ 
                  $day=date('Y-m-d', $i);
                  array_push($fechas, $day);  
        }   
        return $fechas;
    }
    
    
    public function getDiaMasDias($Fecha,$numDias){
        $Y=date("Y",strtotime($Fecha));
        $m=date("m",strtotime($Fecha));
        $d=date("d",strtotime($Fecha));
        $diaNuevo=date("Y-m-d",mktime(0, 0, 0, $m, $d+$numDias, $Y));
        return $diaNuevo;
    }
    
    public function getCiclosTipoInscripcion($espTipoInsc,$espTipoCiclos){
         //Tipo_insc 1= anual, 2 cuatrimenstral, 3 unica
         //Tipo_ciclos (Semestre=1, Bimestre=2, Mensual=3, Cuatrimestre=4)
         //Eliminar el if despues de que se carguen todos los alumnos

         //Tipo de inscripcion 1
         //Tipo ciclo es 1(semestre) o 2(Bimestre) o 3(Mensual) o 4(Cuatrimestre)
         //modulo cera simepre 1

          //Tipo de inscripcion 2
          //Tipo ciclo es 1(semestre) o 4(Cuatrismetrs)
          //modulo cera simepre 0

          //Tipo de inscripcion 2
          //Tipo ciclo es 2(Bimestre) o 3(Mensual)
          //modulo cera simepre 1
        
            $ciclos=0;
            if($espTipoInsc==1){
                    if($espTipoCiclos==1){
                           $ciclos=2; 
                    }elseif($espTipoCiclos==2){
                           $ciclos=6; 
                    }elseif($espTipoCiclos==3){
                           $ciclos=12; 
                    }elseif($espTipoCiclos==4){
                           $ciclos=3; 
                    }
             }elseif($espTipoInsc==2){

                    if($espTipoCiclos==1){
                           $ciclos=1; 
                    }elseif($espTipoCiclos==2){
                           $ciclos=2; 
                    }elseif($espTipoCiclos==3){
                           $ciclos=4; 
                    }elseif($espTipoCiclos==4){
                           $ciclos=1; 
                    }
             }
             return $ciclos;
    }
    
    
    public function resizeToFile($sourcefile, $dest_x, $dest_y, $targetfile, $jpegqual) {

            $this->setMemoryForImage($sourcefile);
            /* Get the  dimensions of the source picture */
            $picsize = getimagesize("$sourcefile");

            $source_x = $picsize[0];
            $source_y = $picsize[1];
            $source_id = imageCreateFromJPEG("$sourcefile");

            /* Create a new image object (not neccessarily true colour) */

            $target_id = imagecreatetruecolor($dest_x, $dest_y);

            /* Resize the original picture and copy it into the just created image
              object. Because of the lack of space I had to wrap the parameters to
              several lines. I recommend putting them in one line in order keep your
              code clean and readable */


            $target_pic = imagecopyresampled($target_id, $source_id, 0, 0, 0, 0, $dest_x, $dest_y, $source_x, $source_y);

            /* Create a jpeg with the quality of "$jpegqual" out of the
              image object "$target_pic".
              This will be saved as $targetfile */

            imagejpeg($target_id, "$targetfile", $jpegqual);

            return true;
          }

    public function setMemoryForImage($filename) {
          $imageInfo = getimagesize($filename);
          $MB = 10048576;  // number of bytes in 1M
          $K64 = 655536;    // number of bytes in 64K
          $TWEAKFACTOR = 2.5;  // Or whatever works for you
          $memoryNeeded = round(( $imageInfo[0] * $imageInfo[1] * $imageInfo['bits'] * $imageInfo['channels'] / 8 + $K64
                  ) * $TWEAKFACTOR
          );
          //ini_get('memory_limit') only works if compiled with "--enable-memory-limit" also
          //Default memory limit is 8MB so well stick with that. 
          //To find out what yours is, view your php.ini file.
          $memoryLimit = 50 * $MB;
          if (function_exists('memory_get_usage') &&
                  memory_get_usage() + $memoryNeeded > $memoryLimit) {
            $newLimit = $memoryLimitMB + ceil(( memory_get_usage() + $memoryNeeded - $memoryLimit
                            ) / $MB
            );
            ini_set('memory_limit', $newLimit . 'M');
            return true;
          } else {
            return false;
          }
        }
        
    public function getCalificacion($Calificacion){
        //Por si pusieron nomenclatura AC, CP, etc
        $cal="";
        if(is_numeric($Calificacion)){
           $cal=$this->redonderCalificacion($Calificacion);
        }else{
           $cal=$Calificacion;
        }
        return $cal;
    }
    
    public function deleteQuery($query) {
        $consulta = $this->_cnn->query($query);
        if(!$consulta) {
              throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        }else{
                return true;
        }
    }

    function getTxtHora($HoraIni){
        $HoraIni=  substr($HoraIni,strpos($HoraIni,"T")+1);
        $HoraIni=  substr($HoraIni, 0, strripos($HoraIni, ":"));
        return $HoraIni;
    }
    
    public function getConsulta($query) {
        $consulta = $this->_cnn->query($query);
        if(!$consulta){
             throw new Exception("Error al consultar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        }else{
             $row_consulta = $consulta->fetch_assoc();
             return $row_consulta;
        }
    }
}
