<?php
class AccionesCargoPeriodo{
    
    public $Id;
    public $Id_cargo_periodo;
    public $Fecha_captura;
    public $Id_usu_captura;
    public $Monto;
    public $Tipo;
    public $Comentarios;

    function __construct() {
        $this->Id = "";
        $this->Id_cargo_periodo = "";
        $this->Fecha_captura = "";
        $this->Id_usu_captura = "";
        $this->Monto = "";
        $this->Tipo = "";
        $this->Comentarios = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getId_cargo_periodo() {
        return $this->Id_cargo_periodo;
    }

    function getFecha_captura() {
        return $this->Fecha_captura;
    }

    function getId_usu_captura() {
        return $this->Id_usu_captura;
    }

    function getMonto() {
        return $this->Monto;
    }

    function getTipo() {
        return $this->Tipo;
    }

    function getComentarios() {
        return $this->Comentarios;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_cargo_periodo($Id_cargo_periodo) {
        $this->Id_cargo_periodo = $Id_cargo_periodo;
    }

    function setFecha_captura($Fecha_captura) {
        $this->Fecha_captura = $Fecha_captura;
    }

    function setId_usu_captura($Id_usu_captura) {
        $this->Id_usu_captura = $Id_usu_captura;
    }

    function setMonto($Monto) {
        $this->Monto = $Monto;
    }

    function setTipo($Tipo) {
        $this->Tipo = $Tipo;
    }

    function setComentarios($Comentarios) {
        $this->Comentarios = $Comentarios;
    }



    
   

}
