<?php

class Aulas {
    public $Id;
    public $Clave_aula;
    public $Nombre_aula;
    public $Tipo_aula;
    public $Capacidad_aula;
    public $Activo_aula;
    public $Id_plantel;
    
    function __construct() {
        $this->Id = "";
        $this->Clave_aula = "";
        $this->Nombre_aula = "";
        $this->Tipo_aula = "";
        $this->Capacidad_aula = "";
        $this->Activo_aula = "";
        $this->Id_plantel = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getClave_aula() {
        return $this->Clave_aula;
    }

    public function getNombre_aula() {
        return $this->Nombre_aula;
    }

    public function getTipo_aula() {
        return $this->Tipo_aula;
    }

    public function getCapacidad_aula() {
        return $this->Capacidad_aula;
    }

    public function getActivo_aula() {
        return $this->Activo_aula;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setClave_aula($Clave_aula) {
        $this->Clave_aula = $Clave_aula;
    }

    public function setNombre_aula($Nombre_aula) {
        $this->Nombre_aula = $Nombre_aula;
    }

    public function setTipo_aula($Tipo_aula) {
        $this->Tipo_aula = $Tipo_aula;
    }

    public function setCapacidad_aula($Capacidad_aula) {
        $this->Capacidad_aula = $Capacidad_aula;
    }

    public function setActivo_aula($Activo_aula) {
        $this->Activo_aula = $Activo_aula;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }




}
