<?php

class DocenteExamen {
    public $Id;
    public $Id_docen;
    public $IdHorarioExamen;
    
    function __construct() {
        $this->Id = "";
        $this->Id_docen = "";
        $this->IdHorarioExamen = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getId_docen() {
        return $this->Id_docen;
    }

    function getIdHorarioExamen() {
        return $this->IdHorarioExamen;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_docen($Id_docen) {
        $this->Id_docen = $Id_docen;
    }

    function setIdHorarioExamen($IdHorarioExamen) {
        $this->IdHorarioExamen = $IdHorarioExamen;
    }


}
