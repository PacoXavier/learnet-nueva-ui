<?php

class Alumnos {
    public $Id;
    public $Nombre;
    public $ApellidoP;
    public $ApellidoM;
    public $Edad;
    public $Email;
    public $Tel;
    public $Cel;
    public $Id_medio_ent;
    public $Seguimiento_ins;
    public $Fecha_ins;
    public $Sexo;
    public $FechaNac;
    public $Id_dir;
    public $Curp_ins;
    public $NombreTutor;
    public $EmailTutor;
    public $TelTutor;
    public $UltGrado_est;
    public $EscuelaPro;
    public $Matricula;
    public $Alta_alum;
    public $Baja_alum;
    public $Activo_alum;
    public $Pass_alum;
    public $RecoverPass;
    public $LastSession;
    public $Img_alum;
    public $tipo;
    public $MedioContacto;
    public $Refencia_pago;
    public $Comentarios;
    public $Tipo_sangre;
    public $Alergias;
    public $Enfermedades_cronicas;
    public $Preinscripciones_medicas;
    public $Id_plantel;
    public $Id_usu_capturo;
    public $MatriculaSep;
    public $ExpedienteSep;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->ApellidoP = "";
        $this->ApellidoM = "";
        $this->Edad = "";
        $this->Email = "";
        $this->Tel = "";
        $this->Cel = "";
        $this->Id_medio_ent = "";
        $this->Seguimiento_ins = "";
        $this->Fecha_ins = "";
        $this->Sexo = "";
        $this->FechaNac = "";
        $this->Id_dir = "";
        $this->Curp_ins = "";
        $this->NombreTutor = "";
        $this->EmailTutor = "";
        $this->TelTutor = "";
        $this->UltGrado_est = "";
        $this->EscuelaPro = "";
        $this->Matricula = "";
        $this->Alta_alum = "";
        $this->Baja_alum = "";
        $this->Activo_alum = "";
        $this->Pass_alum = "";
        $this->RecoverPass = "";
        $this->LastSession = "";
        $this->Img_alum = "";
        $this->tipo = "";
        $this->MedioContacto = "";
        $this->Refencia_pago = "";
        $this->Comentarios = "";
        $this->Tipo_sangre = "";
        $this->Alergias = "";
        $this->Enfermedades_cronicas = "";
        $this->Preinscripciones_medicas = "";
        $this->Id_plantel = "";
        $this->Id_usu_capturo="";
        $this->MatriculaSep="";
        $this->ExpedienteSep="";
    }
    
    function getMatriculaSep() {
        return $this->MatriculaSep;
    }

    function getExpedienteSep() {
        return $this->ExpedienteSep;
    }

    function setMatriculaSep($MatriculaSep) {
        $this->MatriculaSep = $MatriculaSep;
    }

    function setExpedienteSep($ExpedienteSep) {
        $this->ExpedienteSep = $ExpedienteSep;
    }

    function getId_usu_capturo() {
        return $this->Id_usu_capturo;
    }

    function setId_usu_capturo($Id_usu_capturo) {
        $this->Id_usu_capturo = $Id_usu_capturo;
    }

    
    public function getId() {
        return $this->Id;
    }

    public function getNombre() {
        return $this->Nombre;
    }

    public function getApellidoP() {
        return $this->ApellidoP;
    }

    public function getApellidoM() {
        return $this->ApellidoM;
    }

    public function getEdad() {
        return $this->Edad;
    }

    public function getEmail() {
        return $this->Email;
    }

    public function getTel() {
        return $this->Tel;
    }

    public function getCel() {
        return $this->Cel;
    }

    public function getId_medio_ent() {
        return $this->Id_medio_ent;
    }

    public function getSeguimiento_ins() {
        return $this->Seguimiento_ins;
    }

    public function getFecha_ins() {
        return $this->Fecha_ins;
    }

    public function getSexo() {
        return $this->Sexo;
    }

    public function getFechaNac() {
        return $this->FechaNac;
    }

    public function getId_dir() {
        return $this->Id_dir;
    }

    public function getCurp_ins() {
        return $this->Curp_ins;
    }

    public function getNombreTutor() {
        return $this->NombreTutor;
    }

    public function getEmailTutor() {
        return $this->EmailTutor;
    }

    public function getTelTutor() {
        return $this->TelTutor;
    }

    public function getUltGrado_est() {
        return $this->UltGrado_est;
    }

    public function getEscuelaPro() {
        return $this->EscuelaPro;
    }

    public function getMatricula() {
        return $this->Matricula;
    }

    public function getAlta_alum() {
        return $this->Alta_alum;
    }

    public function getBaja_alum() {
        return $this->Baja_alum;
    }

    public function getActivo_alum() {
        return $this->Activo_alum;
    }

    public function getPass_alum() {
        return $this->Pass_alum;
    }

    public function getRecoverPass() {
        return $this->RecoverPass;
    }

    public function getLastSession() {
        return $this->LastSession;
    }

    public function getImg_alum() {
        return $this->Img_alum;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function getMedioContacto() {
        return $this->MedioContacto;
    }

    public function getRefencia_pago() {
        return $this->Refencia_pago;
    }

    public function getComentarios() {
        return $this->Comentarios;
    }

    public function getTipo_sangre() {
        return $this->Tipo_sangre;
    }

    public function getAlergias() {
        return $this->Alergias;
    }

    public function getEnfermedades_cronicas() {
        return $this->Enfermedades_cronicas;
    }

    public function getPreinscripciones_medicas() {
        return $this->Preinscripciones_medicas;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    public function setApellidoP($ApellidoP) {
        $this->ApellidoP = $ApellidoP;
    }

    public function setApellidoM($ApellidoM) {
        $this->ApellidoM = $ApellidoM;
    }

    public function setEdad($Edad) {
        $this->Edad = $Edad;
    }

    public function setEmail($Email) {
        $this->Email = $Email;
    }

    public function setTel($Tel) {
        $this->Tel = $Tel;
    }

    public function setCel($Cel) {
        $this->Cel = $Cel;
    }

    public function setId_medio_ent($Id_medio_ent) {
        $this->Id_medio_ent = $Id_medio_ent;
    }

    public function setSeguimiento_ins($Seguimiento_ins) {
        $this->Seguimiento_ins = $Seguimiento_ins;
    }

    public function setFecha_ins($Fecha_ins) {
        $this->Fecha_ins = $Fecha_ins;
    }

    public function setSexo($Sexo) {
        $this->Sexo = $Sexo;
    }

    public function setFechaNac($FechaNac) {
        $this->FechaNac = $FechaNac;
    }

    public function setId_dir($Id_dir) {
        $this->Id_dir = $Id_dir;
    }

    public function setCurp_ins($Curp_ins) {
        $this->Curp_ins = $Curp_ins;
    }

    public function setNombreTutor($NombreTutor) {
        $this->NombreTutor = $NombreTutor;
    }

    public function setEmailTutor($EmailTutor) {
        $this->EmailTutor = $EmailTutor;
    }

    public function setTelTutor($TelTutor) {
        $this->TelTutor = $TelTutor;
    }

    public function setUltGrado_est($UltGrado_est) {
        $this->UltGrado_est = $UltGrado_est;
    }

    public function setEscuelaPro($EscuelaPro) {
        $this->EscuelaPro = $EscuelaPro;
    }

    public function setMatricula($Matricula) {
        $this->Matricula = $Matricula;
    }

    public function setAlta_alum($Alta_alum) {
        $this->Alta_alum = $Alta_alum;
    }

    public function setBaja_alum($Baja_alum) {
        $this->Baja_alum = $Baja_alum;
    }

    public function setActivo_alum($Activo_alum) {
        $this->Activo_alum = $Activo_alum;
    }

    public function setPass_alum($Pass_alum) {
        $this->Pass_alum = $Pass_alum;
    }

    public function setRecoverPass($RecoverPass) {
        $this->RecoverPass = $RecoverPass;
    }

    public function setLastSession($LastSession) {
        $this->LastSession = $LastSession;
    }

    public function setImg_alum($Img_alum) {
        $this->Img_alum = $Img_alum;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
    }

    public function setMedioContacto($MedioContacto) {
        $this->MedioContacto = $MedioContacto;
    }

    public function setRefencia_pago($Refencia_pago) {
        $this->Refencia_pago = $Refencia_pago;
    }

    public function setComentarios($Comentarios) {
        $this->Comentarios = $Comentarios;
    }

    public function setTipo_sangre($Tipo_sangre) {
        $this->Tipo_sangre = $Tipo_sangre;
    }

    public function setAlergias($Alergias) {
        $this->Alergias = $Alergias;
    }

    public function setEnfermedades_cronicas($Enfermedades_cronicas) {
        $this->Enfermedades_cronicas = $Enfermedades_cronicas;
    }

    public function setPreinscripciones_medicas($Preinscripciones_medicas) {
        $this->Preinscripciones_medicas = $Preinscripciones_medicas;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }



}
