<?php
require_once 'modelos/base.php';
require_once 'modelos/Pagos.php';
require_once 'DaoPagosCiclo.php';
//require_once 'DaoCargosPeriodo.php';

class DaoPagos extends base{

	public function add(Pagos $x){
              $query=sprintf("INSERT INTO Pagos_pagos (Id_pago_ciclo,Monto_pp,Id_usu,Fecha_pp,metodo_pago,Fecha_captura, Comentarios, Id_evento,NombrePaga,EmailPaga,Id_ofe_alum,ConceptoPago,Id_plantel,Activo,Folio,DetalleCarrera,IdRel,TipoRel,Id_ciclo_a_usar) VALUES (%s ,%s, %s, %s, %s, %s,%s ,%s,%s ,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
              $this->GetSQLValueString($x->getId_pago_ciclo(), "int"),
              $this->GetSQLValueString($x->getMonto_pp(), "double"),
              $this->GetSQLValueString($x->getId_usu(), "int"),
              $this->GetSQLValueString($x->getFecha_pp(), "date"),
              $this->GetSQLValueString($x->getMetodo_pago(), "int"),
              $this->GetSQLValueString($x->getFecha_captura(), "date"),
              $this->GetSQLValueString($x->getComentarios(), "text"),
              $this->GetSQLValueString($x->getId_evento(), "int"),
              $this->GetSQLValueString($x->getNombrePaga(), "text"),
              $this->GetSQLValueString($x->getEmailPaga(), "text"),
              $this->GetSQLValueString($x->getId_ofe_alum(), "int"),
              $this->GetSQLValueString($x->getConcepto(), "text"),
              $this->GetSQLValueString($x->getId_plantel(), "int"),
              $this->GetSQLValueString($x->getActivo(), "int"),
              $this->GetSQLValueString($x->getFolio(), "int"),
              $this->GetSQLValueString($x->getDetalleCarrera(), "text"),
              $this->GetSQLValueString($x->getIdRel(), "int"),
              $this->GetSQLValueString($x->getTipoRel(), "text"),
              $this->GetSQLValueString($x->getId_ciclo_a_usar(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(Pagos $x){
	    $query=sprintf("UPDATE Pagos_pagos SET Id_pago_ciclo=%s, Monto_pp=%s, Id_usu=%s,Fecha_pp=%s,metodo_pago=%s, Fecha_captura=%s,Comentarios=%s, Id_evento=%s,NombrePaga=%s,EmailPaga=%s,Id_ofe_alum=%s,ConceptoPago=%s,Id_plantel=%s,Activo=%s,Folio=%s,DetalleCarrera=%s,IdRel=%s,TipoRel=%s, Id_ciclo_a_usar=%s  WHERE Id_pp = %s",
              $this->GetSQLValueString($x->getId_pago_ciclo(), "int"),
              $this->GetSQLValueString($x->getMonto_pp(), "double"),
              $this->GetSQLValueString($x->getId_usu(), "int"),
              $this->GetSQLValueString($x->getFecha_pp(), "date"),
              $this->GetSQLValueString($x->getMetodo_pago(), "int"),
              $this->GetSQLValueString($x->getFecha_captura(), "date"),
              $this->GetSQLValueString($x->getComentarios(), "text"),
              $this->GetSQLValueString($x->getId_evento(), "int"),
              $this->GetSQLValueString($x->getNombrePaga(), "text"),
              $this->GetSQLValueString($x->getEmailPaga(), "text"),
              $this->GetSQLValueString($x->getId_ofe_alum(), "int"),
              $this->GetSQLValueString($x->getConcepto(), "text"),
              $this->GetSQLValueString($x->getId_plantel(), "int"),
              $this->GetSQLValueString($x->getActivo(), "int"),
              $this->GetSQLValueString($x->getFolio(), "int"),
              $this->GetSQLValueString($x->getDetalleCarrera(), "text"),
              $this->GetSQLValueString($x->getIdRel(), "int"),
              $this->GetSQLValueString($x->getTipoRel(), "text"),
              $this->GetSQLValueString($x->getId_ciclo_a_usar(), "int"),
              $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}
	
	
	
	public function delete($Id){
                $query = sprintf("UPDATE Pagos_pagos SET Activo=0 WHERE Id_pp=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
        
        public function deletePagosCargo($Id_pago_ciclo){
                $query = sprintf("DELETE FROM Pagos_pagos WHERE Id_pago_ciclo=".$Id_pago_ciclo); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Pagos_pagos WHERE Id_pp= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Pagos_pagos WHERE Activo=1";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
	

	
	public function create_object($row){
            $x = new Pagos();
            $x->setId($row['Id_pp']);
            $x->setId_pago_ciclo($row['Id_pago_ciclo']);
            $x->setMonto_pp($row['Monto_pp']);
            $x->setId_usu($row['Id_usu']); 
            $x->setFecha_pp($row['Fecha_pp']);
            $x->setMetodo_pago($row['metodo_pago']); 
            $x->setFecha_captura($row['Fecha_captura']); 
            $x->setComentarios($row['Comentarios']); 
            $x->setId_evento($row['Id_evento']); 
            $x->setNombrePaga($row['NombrePaga']); 
            $x->setEmailPaga($row['EmailPaga']); 
            $x->setId_ofe_alum($row['Id_ofe_alum']);
            $x->setConcepto($row['ConceptoPago']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setActivo($row['Activo']);
            $x->setFolio($row['Folio']);
            $x->setDetalleCarrera($row['DetalleCarrera']);
            $x->setIdRel($row['IdRel']);
            $x->setTipoRel($row['TipoRel']);
            $x->setId_ciclo_a_usar($row['Id_ciclo_a_usar']);
            return $x;
	}

        
        
	public function advanced_query($query){
		    $resp=array();
		    $consulta=$this->_cnn->query($query);
		    if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	}
        
        public function getPagosEvento($Id_event){
            $query="SELECT * FROM Pagos_pagos WHERE Id_evento=".$Id_event." AND Activo=1";
             return $this->advanced_query($query);
	}
        
        
        
         public function generarFolio() {
            $query = "SELECT * FROM Pagos_pagos WHERE Folio IS NOT NULL AND Id_plantel=" . $this->Id_plantel . "  ORDER BY Folio DESC LIMIT 1";
            $consulta = $this->_cnn->query($query);
            if (!$consulta) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            } else {
                $row_consulta = $consulta->fetch_assoc();
                $totalRows_consulta = $consulta->num_rows;
                if ($totalRows_consulta > 0) {
                    $Folio = $row_consulta['Folio'];
                    $Folio++;
                }else{
                    $Folio=$this->Id_plantel."000";   
                }
            }
            return $Folio;
        }
        
        
        public function getPagosUsuario($Id,$Tipo){
            $query= "SELECT * FROM Pagos_pagos WHERE IdRel=".$Id." AND TipoRel='".$Tipo."' AND  Activo=1 ORDER BY Fecha_captura ASC";
            return $this->advanced_query($query);
        } 
        
        public function getTotalPagosUsuario($Id,$Tipo){
             $total=0;
             foreach($this->getPagosUsuario($Id,$Tipo) as $pago){
                  $total+=$pago->getMonto_pp();
             }
             return $total;
        }
        
        
        public function getPagosOfertaAlumno($Id_ofe_alum){
            $query="SELECT * FROM Pagos_pagos WHERE Id_ofe_alum=".$Id_ofe_alum." AND Activo=1";
            return $this->advanced_query($query);
        }
        
        public function getTotalPagosCargo($Id_pago_ciclo){
            $total=0;
            $query="SELECT * FROM Pagos_pagos WHERE Id_pago_ciclo=".$Id_pago_ciclo." AND Activo=1";
            foreach($this->advanced_query($query) as $pago){
                $total+=$pago->getMonto_pp();
            }
            return $total;
        }
        
        public function getTotalPagasOfertaAlumno($Id_ofe_alum){
             $total=0;
             foreach($this->getPagosOfertaAlumno($Id_ofe_alum) as $pago){
                  $total+=$pago->getMonto_pp();
             }
             return $total;
        }
        
        
        public function getTotalPagosACuenta($Id_ofe_alum){
             $total=0;
             //Los pagos a cuenta son los que no estan referenciados a ningun cargo en especifico
             //Ni a ningun ciclo a usar
             //Si es un pago a cuenta(Salda los cargos desde el mas antiguo hasta el mas actual)
             $query="SELECT * FROM Pagos_pagos WHERE Id_ofe_alum=".$Id_ofe_alum." AND Activo=1";
             foreach($this->advanced_query($query) as $pago){
                 
                 if(strlen($pago->getId_pago_ciclo())==0 && strlen($pago->getId_ciclo_a_usar())==0){
                    $total+=$pago->getMonto_pp();
                 }else{

                     //Si el Id_pago_ciclo > 0 y no pertenece a los cargos de la oferta actual
                     //Entonces se toma como a cuenta tambien ya que podria contener un Id_pago_ciclo de un cargo que 
                     //Ya ha sido eliminado por algun cambio de oferta
                    if($pago->getId_pago_ciclo()>0){
                        $DaoPagosCiclo= new DaoPagosCiclo();
                        $existe=$DaoPagosCiclo->perteneceCargoAOfertaAlumno($Id_ofe_alum,$pago->getId_pago_ciclo());
                        if(strlen($existe->getId())==0){
                           $total+=$pago->getMonto_pp(); 
                        }
                    }   

                 }
             }
             
             return $total;
        }
        

        public function getTotalPagosAdelantados($Id_ciclo_usar,$Id_alum,$Id_ofe_alum){
            $q="";
            if($Id_ofe_alum!=null){
                $q=" AND Id_ofe_alum=".$Id_ofe_alum;
            }
             $total=0;
             $query="SELECT * FROM Pagos_pagos WHERE Id_ciclo_a_usar=".$Id_ciclo_usar." AND Activo=1 AND Id_pago_ciclo IS NULL AND IdRel=".$Id_alum." AND TipoRel='alum'".$q;
             foreach($this->advanced_query($query) as $pago){
                  $total+=$pago->getMonto_pp();
             }
             return $total;
        }
        
        
        public function getTotalPagosACuentaPeriodos($Id_ofe_alum){
            $total=0;
            /*
             //Los pagos a cuenta son los que no estan referenciados a ningun cargo en especifico
             //Ni a ningun ciclo a usar
             //Si es un pago a cuenta(Salda los cargos desde el mas antiguo hasta el mas actual)
             $query="SELECT * FROM Pagos_pagos WHERE Id_ofe_alum=".$Id_ofe_alum." AND Activo=1";
             foreach($this->advanced_query($query) as $pago){
                 
                 if(strlen($pago->getId_pago_ciclo())==0 && strlen($pago->getId_ciclo_a_usar())==0){
                    $total+=$pago->getMonto_pp();
                 }else{

                     //Si el Id_pago_ciclo > 0 y no pertenece a los cargos de la oferta actual
                     //Entonces se toma como a cuenta tambien ya que podria contener un Id_pago_ciclo de un cargo que 
                     //Ya ha sido eliminado por algun cambio de oferta
                    if($pago->getId_pago_ciclo()>0){
                        $DaoCargosPeriodo= new DaoCargosPeriodo();
                        $existe=$DaoCargosPeriodo->perteneceCargoAOfertaAlumno($Id_ofe_alum,$pago->getId_pago_ciclo());
                        if(strlen($existe->getId())==0){
                           $total+=$pago->getMonto_pp(); 
                        }
                    }   
                 }
             }
             */
             
             return $total;
        }
        

        
        
        
}

