<?php 
if($_REQUEST['id']>0){
require_once('Connections/cnn.php');
require_once('estandares/Fpdf/fpdf.php'); 
require_once('estandares/Fpdf/fpdi.php'); 
require_once('estandares/cantidad_letra.php'); 
require_once('estandares/class_interesados.php'); 
require_once('estandares/class_ofertas.php'); 
require_once('estandares/class_direcciones.php'); 

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');
//Id_ofe_alum
$class_interesados = new class_interesados($_REQUEST['id']);
$alum=$class_interesados->get_interesado();
$int="";
if ($alum['Id_dir'] > 0) {
  $class_direcciones = new class_direcciones($alum['Id_dir']);
  $dir = $class_direcciones->get_dir();
  if(strlen($dir['NumInt_dir'])>0){
      $int=" Int. ".$dir['NumInt_dir'];
  }
}
$oferta_alumno = $class_interesados->get_oferta_alumno($_REQUEST['id_ofe']);

$primer_ciclo=$class_interesados->get_first_ciclo_oferta($_REQUEST['id_ofe']);
$grado=$class_interesados->get_first_grado_oferta($_REQUEST['id_ofe']);

$class_ofertas = new class_ofertas();
$oferta = $class_ofertas->get_oferta($oferta_alumno['Id_ofe']);
$esp = $class_ofertas->get_especialidad($oferta_alumno['Id_esp']);
if ($oferta_alumno['Id_ori'] > 0) {
  $ori = $class_ofertas->get_orientacion($oferta_alumno['Id_ori']);
  $nombre_ori = $ori['Nombre_ori'];
}
// initiate FPDI 
$pdf = new FPDI(); 

$carrera="";
// add a page 
$pdf->AddPage();
if($oferta['Id_oferta']==11){
    $carrera=$oferta['Nombre_oferta']." ".$esp['Nombre_esp'];
    $setSourceFile="estandares/Contrato_ct.pdf"; 
}elseif($esp['Id_esp']==76){
    $carrera=$oferta['Nombre_oferta']." ".$esp['Nombre_esp'];
    $setSourceFile="estandares/Contrato_lip.pdf";
}elseif($esp['Id_esp']==77){
    $carrera=$esp['Nombre_esp'];
    $setSourceFile="estandares/Contrato_plm.pdf";
}elseif($esp['Id_esp']==74){
    $carrera=$esp['Nombre_esp'];
    $setSourceFile="estandares/Contrato_paa.pdf";
}elseif($esp['Id_esp']==71){
    $carrera=$esp['Nombre_esp'];
    $setSourceFile="estandares/Contrato_pae.pdf";
}elseif($esp['Id_esp']==75){
    $carrera=$esp['Nombre_esp'];
    $setSourceFile="estandares/Contrato_pa.pdf";
}



$pdf->setSourceFile($setSourceFile);

// import page 1 
$tplIdx = $pdf->importPage(1); 
// use the imported page and place it at point 10,10 with a width of 100 mm 
$pdf->useTemplate($tplIdx); 

// now write some text above the imported page 
$pdf->SetFont('Arial','B',8); 
$pdf->SetTextColor(0,0,0); 
$pdf->SetXY(74,49); 
$pdf->MultiCell(70,3 ,utf8_decode(strtoupper($carrera)),0,'C'); 
$pdf->SetXY(93,54); 
$pdf->Write(10, utf8_decode(strtoupper("Cuatrimestre ".$primer_ciclo['Clave'])));
$pdf->SetXY(29,88); 
$pdf->Write(10, utf8_decode(strtoupper($alum['Nombre_ins']." ".$alum['ApellidoP_ins']." ".$alum['ApellidoM_ins'])));
$pdf->SetXY(53,96.5); 
$pdf->Write(10, utf8_decode(strtoupper($dir['Calle_dir']." ".$dir['NumExt_dir']." ".$int))); 
$pdf->SetXY(140,96.5); 
$pdf->Write(10, utf8_decode(strtoupper($dir['Colonia_dir']))); 
$pdf->SetXY(43,104.5); 
$pdf->Write(10, utf8_decode(strtoupper($dir['Estado_dir']))); 
$pdf->SetXY(85,104.5); 
$pdf->Write(10, utf8_decode(strtoupper($dir['Cp_dir']))); 
$pdf->SetXY(120,104.5); 
$pdf->Write(10, utf8_decode($alum['Tel_ins']));

if($oferta['Id_oferta']==11){
    //Carreras tecnicas
    $pdf->SetXY(42,190.8); 
    $pdf->Write(10, utf8_decode(strtoupper($esp['RegistroValidez_Oficial'])));
    $pdf->SetXY(65,200); 
    $pdf->Write(10, utf8_decode(strtoupper($alum['Nombre_ins']." ".$alum['ApellidoP_ins']." ".$alum['ApellidoM_ins'])));
    $pdf->SetXY(55,204.5); 
    $pdf->Write(10, utf8_decode(strtoupper($oferta['Nombre_oferta'])));
    $pdf->SetXY(45,208.7); 
    $clave= substr($primer_ciclo['Clave'], 2,2);
    $pdf->Write(10, utf8_decode(strtoupper($clave)));
    $pdf->SetXY(75,209); 
    $pdf->Write(10, utf8_decode(strtoupper($grado['Grado'])));
}elseif($esp['Id_esp']==76 || $esp['Id_esp']==77){
    //Licenciatura
    $pdf->SetXY(65,204); 
    $pdf->Write(10, utf8_decode(strtoupper($alum['Nombre_ins']." ".$alum['ApellidoP_ins']." ".$alum['ApellidoM_ins'])));
    $pdf->SetXY(55,208.5); 
    $pdf->Write(10, utf8_decode(strtoupper($oferta['Nombre_oferta'])));
    $pdf->SetXY(45,213.4); 
    $clave= substr($primer_ciclo['Clave'], 2,2);
    $pdf->Write(10, utf8_decode(strtoupper($clave)));
    $pdf->SetXY(75,213); 
    $pdf->Write(10, utf8_decode(strtoupper($grado['Grado'])));
}elseif($esp['Id_esp']==74 || $esp['Id_esp']==71 || $esp['Id_esp']==75){
    //Prepa
    $pdf->SetXY(65,217.5); 
    $pdf->Write(10, utf8_decode(strtoupper($alum['Nombre_ins']." ".$alum['ApellidoP_ins']." ".$alum['ApellidoM_ins'])));
    $pdf->SetXY(55,222); 
    $pdf->Write(10, utf8_decode(strtoupper($oferta['Nombre_oferta'])));
    $pdf->SetXY(45,226.5); 
    $clave= substr($primer_ciclo['Clave'], 2,2);
    $pdf->Write(10, utf8_decode(strtoupper($clave)));
    $pdf->SetXY(75,226.5); 
    $pdf->Write(10, utf8_decode(strtoupper($grado['Grado'])));
}

// import page 2 
$pdf->AddPage(); 
$tplIdx = $pdf->importPage(2); 
// use the imported page and place it at point 10,10 with a width of 100 mm 
$pdf->useTemplate($tplIdx); 


//Carreras tecnicas
if($oferta['Id_oferta']==11 || $esp['Id_esp']==77){
    $pdf->SetXY(42,18); 
    $pdf->Write(70,utf8_decode(strtoupper($carrera))); 
    $pdf->SetXY(76,23.5); 
    $pdf->Write(70,utf8_decode("$".number_format($esp['Inscripcion_curso'],2))); 
    $pdf->SetXY(76,27.5); 
    $pdf->Write(70,utf8_decode("$".number_format($esp['Precio_curso']/$esp['Num_ciclos'],2)));
}elseif($esp['Id_esp']==76){
    //Licenciatura
    $pdf->SetXY(42,20); 
    $pdf->Write(70,utf8_decode(strtoupper($carrera))); 
    $pdf->SetXY(76,27); 
    $pdf->Write(70,utf8_decode("$".number_format($esp['Inscripcion_curso'],2))); 
    $pdf->SetXY(76,31); 
    $pdf->Write(70,utf8_decode("$".number_format($esp['Precio_curso']/$esp['Num_ciclos'],2)));
}elseif($esp['Id_esp']==74 || $esp['Id_esp']==71 || $esp['Id_esp']==75){
    //Prepa
    $pdf->SetXY(42,25); 
    $pdf->Write(70,utf8_decode(strtoupper($carrera))); 
    $pdf->SetXY(76,30); 
    $pdf->Write(70,utf8_decode("$".number_format($esp['Inscripcion_curso'],2))); 
    $pdf->SetXY(76,34.5); 
    $pdf->Write(70,utf8_decode("$".number_format($esp['Precio_curso']/$esp['Num_ciclos'],2)));
}


// import page 3 
$pdf->AddPage(); 
$tplIdx = $pdf->importPage(3); 
// use the imported page and place it at point 10,10 with a width of 100 mm 
$pdf->useTemplate($tplIdx); 

// import page 4
$pdf->AddPage(); 
$tplIdx = $pdf->importPage(4); 
// use the imported page and place it at point 10,10 with a width of 100 mm 
$pdf->useTemplate($tplIdx); 

// import page 5
$pdf->AddPage(); 
$tplIdx = $pdf->importPage(5); 
// use the imported page and place it at point 10,10 with a width of 100 mm 
$pdf->useTemplate($tplIdx); 

// import page 6
$pdf->AddPage(); 
$tplIdx = $pdf->importPage(6); 
// use the imported page and place it at point 10,10 with a width of 100 mm 
$pdf->useTemplate($tplIdx); 

// import page 7
$pdf->AddPage(); 
$tplIdx = $pdf->importPage(7); 
// use the imported page and place it at point 10,10 with a width of 100 mm 
$pdf->useTemplate($tplIdx); 


if($esp['Id_esp']==74 || $esp['Id_esp']==71 || $esp['Id_esp']==75){
    //Prepa
    $pdf->SetXY(160,117); 
    $pdf->Write(70,utf8_decode(strtoupper(date('d')))); 

    $mes=$class_interesados->mesActual(date('m'));
    $pdf->SetXY(37,122); 
    $pdf->Write(70,utf8_decode(strtoupper($mes))); 

    $pdf->SetXY(73,122); 
    $anio= substr(date('Y'), 2,2);
    $pdf->Write(70,utf8_decode(strtoupper($anio))); 
}else{
    $pdf->SetXY(160,46); 
    $pdf->Write(70,utf8_decode(strtoupper(date('d')))); 

    $mes=$class_interesados->mesActual(date('m'));
    $pdf->SetXY(37,50.5); 
    $pdf->Write(70,utf8_decode(strtoupper($mes))); 

    $pdf->SetXY(73,50.5); 
    $anio= substr(date('Y'), 2,2);
    $pdf->Write(70,utf8_decode(strtoupper($anio)));    
}

$pdf->Output('Contrato_prestacion_servicios.pdf', 'I'); 
}else{
    //header("Location: interesados.php");
}
