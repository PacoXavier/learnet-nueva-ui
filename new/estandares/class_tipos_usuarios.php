<?php
require_once("class_bdd.php");
require_once("class_permisos.php");
class class_tipos_usuarios extends class_bdd{

             public $Id_tipo;
	     
	     public function  __construct($Id_tipo){
	     	   class_bdd::__construct();
	     	   if($Id_tipo>0){
	     	   	  $this->Id_tipo=$Id_tipo;
	     	   }
	     }
	     
	     public function get_tipos_usuarios(){
	             $resp=array();
		     $query_tipos_usu_ulm = "SELECT * FROM tipos_usu_ulm WHERE Activo_tipo=1 ORDER BY Id_tipo ASC";
		     $tipos_usu_ulm = mysql_query($query_tipos_usu_ulm, $this->cnn) or die(mysql_error());
		     $row_tipos_usu_ulm= mysql_fetch_array($tipos_usu_ulm);
		     $totalRows_tipos_usu_ulm = mysql_num_rows($tipos_usu_ulm);
		     if($totalRows_tipos_usu_ulm>0){
			     do{
			        array_push($resp, $this->get_tipo($row_tipos_usu_ulm['Id_tipo']));
			     }while($row_tipos_usu_ulm= mysql_fetch_array($tipos_usu_ulm));
		     }
		     return $resp;
	     }
	     
	     public function get_tipo($Id_tipo=null){
	         if($Id_tipo==null){
		        $Id_tipo=$this->Id_tipo;
	         }
	         $resp=array();
		     $query_tipos_usu_ulm = "SELECT * FROM tipos_usu_ulm WHERE Id_tipo=".$Id_tipo;
		     $tipos_usu_ulm = mysql_query($query_tipos_usu_ulm, $this->cnn) or die(mysql_error());
		     $row_tipos_usu_ulm= mysql_fetch_array($tipos_usu_ulm);
		     $totalRows_tipos_usu_ulm = mysql_num_rows($tipos_usu_ulm);
		     if($totalRows_tipos_usu_ulm>0){
                        $resp['Id_tipo']=$row_tipos_usu_ulm['Id_tipo'];
                        $resp['Nombre_tipo']=$row_tipos_usu_ulm['Nombre_tipo'];
                        $resp['Activo_tipo']=$row_tipos_usu_ulm['Activo_tipo'];
                        $resp['Permisos']=array();
                        $resp['Permisos']=$this->get_permisos_tipo_usu($row_tipos_usu_ulm['Id_tipo']);
		     }
		     return $resp;
	     }
             
             public function get_permisos_tipo_usu($Id_tipo){
	             $resp=array();
		     $query_permisos_ulm = "SELECT * FROM permisos_tipo_usu WHERE Id_tipo_usu=".$Id_tipo;
		     $permisos_ulm = mysql_query($query_permisos_ulm, $this->cnn) or die(mysql_error());
		     $row_permisos_ulm= mysql_fetch_array($permisos_ulm);
		     $totalRows_permisos_ulm= mysql_num_rows($permisos_ulm);
		     if($totalRows_permisos_ulm>0){
			     do{
                                $class_permisos= new class_permisos();
			        array_push($resp, $class_permisos->get_perm($row_permisos_ulm['Id_perm']));
			     }while($row_permisos_ulm= mysql_fetch_array($permisos_ulm));
		     }
		     return $resp;
	     } 	
}
