<?php
require_once("class_bdd.php");
class class_pagos extends class_bdd{

	     public $Id_pago;
	     
	     public function  __construct($Id_pago){
	     	   class_bdd::__construct();
	     	   if($Id_pago>0){
	     	   	  $this->Id_pago=$Id_pago;
	     	   }
	     }
	     
	     
	     public function get_pago($Id_pago=null){
	     	 if($Id_pago==null){
		        $Id_pago=$this->Id_pago;
	         }
	         $resp=array();
                 $query_Pagos_ciclo  = "SELECT * FROM Pagos_ciclo WHERE Id_pago_ciclo=".$Id_pago;
                 $Pagos_ciclo= mysql_query($query_Pagos_ciclo, $this->cnn) or die(mysql_error());
                 $row_Pagos_ciclo= mysql_fetch_array($Pagos_ciclo);
                 $totalRows_Pagos_ciclo  = mysql_num_rows($Pagos_ciclo);
                 if($totalRows_Pagos_ciclo>0){
                    $resp['Id_pago_ciclo']=$row_Pagos_ciclo['Id_pago_ciclo'];
                    $resp['Folio']=$row_Pagos_ciclo['Folio'];
                    $resp['Concepto']=$row_Pagos_ciclo['Concepto'];
                    $resp['Fecha_pago']=$row_Pagos_ciclo['Fecha_pago'];
                    $resp['Mensualidad']=$row_Pagos_ciclo['Mensualidad'];
                    $resp['Descuento']=$row_Pagos_ciclo['Descuento'];
                    $resp['Cantidad_Pagada']=$row_Pagos_ciclo['Cantidad_Pagada'];
                    $resp['Fecha_pagado']=$row_Pagos_ciclo['Fecha_pagado'];
                    $resp['Id_ciclo_alum']=$row_Pagos_ciclo['Id_ciclo_alum'];
                    $resp['tipo_pago']=$row_Pagos_ciclo['tipo_pago'];
                    $resp['Id_mis']=$row_Pagos_ciclo['Id_mis'];
                    $resp['Id_alum']=$row_Pagos_ciclo['Id_alum'];
                    $resp['Descuento_beca']=$row_Pagos_ciclo['Descuento_beca'];
                    $resp['Id_mis']=$row_Pagos_ciclo['Id_mis'];
                    $resp['Pagos_pagos']=array();
                    $resp['Pagos_pagos']=$this->get_pagos_pagos($row_Pagos_ciclo['Id_pago_ciclo']);
                    $resp['Recargos_pago']=array();
                    $resp['Recargos_pago']=$this->get_recargos_pago($row_Pagos_ciclo['Id_pago_ciclo']);
                 }
                 return $resp;
	     }
             
              public function get_pagos_pagos($Id_pago_ciclo){
	             $resp=array();
		     $query_Pagos_pagos = "SELECT * FROM Pagos_pagos WHERE Id_pago_ciclo=".$Id_pago_ciclo;
		     $Pagos_pagos= mysql_query($query_Pagos_pagos, $this->cnn) or die(mysql_error());
		     $row_Pagos_pagos= mysql_fetch_array($Pagos_pagos);
		     $totalRows_Pagos_pagos  = mysql_num_rows($Pagos_pagos);
		     if($totalRows_Pagos_pagos>0){
			     do{
			      array_push($resp, $this->get_pago_pago($row_Pagos_pagos['Id_pp']));
			     }while($row_Pagos_pagos= mysql_fetch_array($Pagos_pagos));
		     }
		     return $resp;
	     }
	     
	     public function get_pago_pago($Id_pp){
	             $resp=array();
		     $query_Pagos_pagos  = "SELECT * FROM Pagos_pagos WHERE Id_pp=".$Id_pp;
		     $Pagos_pagos= mysql_query($query_Pagos_pagos, $this->cnn) or die(mysql_error());
		     $row_Pagos_pagos= mysql_fetch_array($Pagos_pagos);
		     $totalRows_Pagos_pagos  = mysql_num_rows($Pagos_pagos);
		     if($totalRows_Pagos_pagos>0){
                            $resp['Id_pp']=$row_Pagos_pagos['Id_pp'];
                            $resp['Id_pago_ciclo']=$row_Pagos_pagos['Id_pago_ciclo'];
                            $resp['Monto_pp']=$row_Pagos_pagos['Monto_pp'];
                            $resp['Id_usu']=$row_Pagos_pagos['Id_usu'];
                            $resp['Fecha_pp']=$row_Pagos_pagos['Fecha_pp'];
                            $resp['metodo_pago']=$row_Pagos_pagos['metodo_pago'];
                            $resp['Fecha_captura']=$row_Pagos_pagos['Fecha_captura'];
		     }
		     return $resp;
	     } 
             
            public function get_recargos_pago($Id_pago_ciclo){
	          $resp=array();
                  $query_Recargos_pago = "SELECT * FROM Recargos_pago WHERE Id_pago=" .$Id_pago_ciclo." AND Fecha_recargo<='".date('Y-m-d')."' ORDER BY Fecha_recargo ASC";
                  $Pagos_Recargos_pago = mysql_query($query_Recargos_pago, $this->cnn) or die(mysql_error());
                  $row_Recargos_pago = mysql_fetch_array($Pagos_Recargos_pago);
                  $totalRows_Recargos_pago = mysql_num_rows($Pagos_Recargos_pago);
                  if ($totalRows_Recargos_pago > 0) {
                    do {
                        array_push($resp, $this->get_recargo_pago($row_Recargos_pago['Id_recargo']));
                    } while ($row_Recargos_pago = mysql_fetch_array($Pagos_Recargos_pago));
                  }
		  return $resp;
	     }
	     
	     public function get_recargo_pago($Id_recargo){
	          $resp=array();
                  $query_Recargos_pago = "SELECT * FROM Recargos_pago WHERE Id_recargo=" .$Id_recargo;
                  $Pagos_Recargos_pago = mysql_query($query_Recargos_pago, $this->cnn) or die(mysql_error());
                  $row_Recargos_pago = mysql_fetch_array($Pagos_Recargos_pago);
                  $totalRows_Recargos_pago = mysql_num_rows($Pagos_Recargos_pago);
                  if ($totalRows_Recargos_pago > 0) {
                        $resp['Id_recargo']=$row_Recargos_pago['Id_recargo'];
                        $resp['Id_pago']=$row_Recargos_pago['Id_pago'];
                        $resp['Monto_recargo']=$row_Recargos_pago['Monto_recargo'];
                        $resp['Fecha_recargo']=$row_Recargos_pago['Fecha_recargo'];
                  }
		  return $resp;
	     }  
}
