<?php
require_once("class_bdd.php");
require_once("class_permisos.php");
class class_usuarios extends class_bdd{

		 public $Id_usu;
	     public $description;
	     
	     public function  __construct($Id_usu){
	     	   class_bdd::__construct();
	     	   if($Id_usu>0){
	     	   	  $this->Id_usu=$Id_usu;
	     	   }
	     }
	     
	     public function get_users(){
                    $usu = $this->get_usu($_COOKIE['admin/Id_usu']);
	         $resp=array();
		     $query_usuarios_ulm = "SELECT * FROM usuarios_ulm WHERE Baja_usu IS NULL AND Id_plantel=".$usu['Id_plantel'];
		     $usuarios_ulm = mysql_query($query_usuarios_ulm, $this->cnn) or die(mysql_error());
		     $row_usuarios_ulm= mysql_fetch_array($usuarios_ulm);
		     $totalRows_usuarios_ulm = mysql_num_rows($usuarios_ulm);
		     if($totalRows_usuarios_ulm>0){
			     do{
			        array_push($resp, $this->get_usu($row_usuarios_ulm['Id_usu']));
			     }while($row_usuarios_ulm= mysql_fetch_array($usuarios_ulm));
		     }
		     return $resp;
	     }
	     
	     public function get_usu($Id_usu=null){
	         if($Id_usu==null){
		        $Id_usu=$this->Id_usu;
	         }
	         $resp=array();
                 
		     $query_usuarios_ulm = "SELECT * FROM usuarios_ulm JOIN 
		     tipos_usu_ulm ON usuarios_ulm.Tipo_usu=tipos_usu_ulm.Id_tipo 
		     JOIN planteles_ulm ON usuarios_ulm.Id_plantel=planteles_ulm.Id_plantel WHERE Id_usu=".$Id_usu;
		     $usuarios_ulm = mysql_query($query_usuarios_ulm, $this->cnn) or die(mysql_error());
		     $row_usuarios_ulm= mysql_fetch_array($usuarios_ulm);
		     $totalRows_usuarios_ulm = mysql_num_rows($usuarios_ulm);
		     if($totalRows_usuarios_ulm>0){
                        $resp['Id_usu']=$row_usuarios_ulm['Id_usu'];
                        $resp['Nombre_usu']=$row_usuarios_ulm['Nombre_usu'];
                        $resp['ApellidoP_usu']=$row_usuarios_ulm['ApellidoP_usu'];
                        $resp['ApellidoM_usu']=$row_usuarios_ulm['ApellidoM_usu'];
                        $resp['Alta_usu']=$row_usuarios_ulm['Alta_usu'];
                        $resp['Baja_usu']=$row_usuarios_ulm['Baja_usu'];
                        $resp['Email_usu']=$row_usuarios_ulm['Email_usu'];
                        $resp['Last_session_usu']=$row_usuarios_ulm['Last_session_usu'];
                        $resp['Img_usu']=$row_usuarios_ulm['Img_usu'];
                        $resp['Tipo_usu_nom']=$row_usuarios_ulm['Nombre_tipo'];
                        $resp['Tipo_usu']=$row_usuarios_ulm['Id_tipo'];
                        $resp['Img_usu']=$row_usuarios_ulm['Img_usu'];
                        $resp['Tel_usu']=$row_usuarios_ulm['Tel_usu'];
                        $resp['Cel_usu']=$row_usuarios_ulm['Cel_usu'];
                        $resp['Clave_usu']=$row_usuarios_ulm['Clave_usu'];
                        $resp['Pass_usu']=$row_usuarios_ulm['Pass_usu'];
                        $resp['Nivel_estudios']=$row_usuarios_ulm['Nivel_estudios'];
                        $resp['Id_plantel']=$row_usuarios_ulm['Id_plantel'];
                        $resp['Nombre_plantel']=$row_usuarios_ulm['Nombre_plantel'];
                        $resp['Doc_CURP']=$row_usuarios_ulm['Doc_CURP'];
                        $resp['Doc_IMSS']=$row_usuarios_ulm['Doc_IMSS'];
                        $resp['Doc_RFC']=$row_usuarios_ulm['Doc_RFC'];
                        $resp['Copy_CURP']=$row_usuarios_ulm['Copy_CURP'];
                        $resp['Copy_IMSS']=$row_usuarios_ulm['Copy_IMSS'];
                        $resp['Copy_RFC']=$row_usuarios_ulm['Copy_RFC'];
                        $resp['Tipo_sangre']=$row_usuarios_ulm['Tipo_sangre'];
                        $resp['Alergias']=$row_usuarios_ulm['Alergias'];
                        $resp['Enfermedades_cronicas']=$row_usuarios_ulm['Enfermedades_cronicas'];
                        $resp['Preinscripciones_medicas']=$row_usuarios_ulm['Preinscripciones_medicas'];
                        $resp['Contacto']=array();
                        $resp['Contacto']=$this->get_contacto($row_usuarios_ulm['Id_usu'],'usu');
			$resp['Permisos']=array();      
                        
                        $query_permisos_extra_usu = "SELECT * FROM permisos_extra_usu WHERE Id_usu=".$row_usuarios_ulm['Id_usu'];
                        $permisos_extra_usu = mysql_query($query_permisos_extra_usu, $this->cnn) or die(mysql_error());
                        $row_permisos_extra_usu= mysql_fetch_array($permisos_extra_usu);
                        $totalRows_permisos_extra_usu = mysql_num_rows($permisos_extra_usu);
                        if($totalRows_permisos_extra_usu>0){
                           do{
                               $class_permisos= new class_permisos($row_permisos_extra_usu['Id_per']);
                               $perm= $class_permisos->get_perm();
                               array_push($resp['Permisos'],$perm);
                           }while($row_permisos_extra_usu= mysql_fetch_array($permisos_extra_usu));

                        }
			        
		     }
		     return $resp;
	     }
             
            public function get_amolestaciones_usuario($Id_usu) {
            $resp = array();
            $query_Reportes_alumno = "SELECT * FROM Reportes_alumno WHERE Id_recibe=" .$Id_usu." AND Tipo_recibe='usu'";
            $Reportes_alumno = mysql_query($query_Reportes_alumno, $this->cnn) or die(mysql_error());
            $row_Reportes_alumno = mysql_fetch_array($Reportes_alumno);
            $totalRows_Reportes_alumno = mysql_num_rows($Reportes_alumno);
            if ($totalRows_Reportes_alumno > 0) {
                do {
                    array_push($resp, $this->get_amolestacion_usuario($row_Reportes_alumno['Id_rep']));
                } while ($row_Reportes_alumno = mysql_fetch_array($Reportes_alumno));
            }
            return $resp;
            }


            public function get_amolestacion_usuario($Id_rep) {
            $resp = array();
            $query_Reportes_alumno = "SELECT * FROM Reportes_alumno WHERE Id_rep=" .$Id_rep;
            $Reportes_alumno = mysql_query($query_Reportes_alumno, $this->cnn) or die(mysql_error());
            $row_Reportes_alumno = mysql_fetch_array($Reportes_alumno);
            $totalRows_Reportes_alumno = mysql_num_rows($Reportes_alumno);
            if ($totalRows_Reportes_alumno > 0) {
                    $resp['Id_rep']=$row_Reportes_alumno['Id_rep'];
                    $resp['Fecha_rep']=$row_Reportes_alumno['Fecha_rep'];
                    $resp['Id_usu']=$row_Reportes_alumno['Id_usu'];
                    $resp['Motivo']=$row_Reportes_alumno['Motivo'];
                    $resp['Id_recibe']=$row_Reportes_alumno['Id_recibe'];
                    $resp['Tipo_recibe']=$row_Reportes_alumno['Tipo_recibe'];
                    $resp['Id_reporta']=$row_Reportes_alumno['Id_reporta'];
                    $resp['Tipo_reporta']=$row_Reportes_alumno['Tipo_reporta'];
                    $resp['Id_ciclo']=$row_Reportes_alumno['Id_ciclo'];
            }
            return $resp;
            }
            
            public function get_usuarios_tipo($tipo) {
                $resp = array();
                $usu = $this->get_usu($_COOKIE['admin/Id_usu']);
                $query_Reportes_alumno = "SELECT * FROM usuarios_ulm WHERE Tipo_usu=" .$tipo." AND Baja_usu IS NULL AND Id_plantel=".$usu['Id_plantel'];
                $Reportes_alumno = mysql_query($query_Reportes_alumno, $this->cnn) or die(mysql_error());
                $row_Reportes_alumno = mysql_fetch_array($Reportes_alumno);
                $totalRows_Reportes_alumno = mysql_num_rows($Reportes_alumno);
                if ($totalRows_Reportes_alumno > 0) {
                    do {
                        array_push($resp, $this->get_usu($row_Reportes_alumno['Id_usu']));
                    } while ($row_Reportes_alumno = mysql_fetch_array($Reportes_alumno));
                }
                return $resp;
            }
	
}
?>