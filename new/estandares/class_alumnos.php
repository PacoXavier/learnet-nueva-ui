<?php
/*
require_once("class_bdd.php");
require_once("class_ofertas.php");
require_once("class_archivos.php");
class class_alumnos extends class_bdd{

              public $Id_alum;
	     public $description;
	     
	     public function  __construct($Id_alum){
	     	   class_bdd::__construct();
	     	   if($Id_alum>0){
	     	   	  $this->Id_alum=$Id_alum;
	     	   }
	     }
	     
	     public function get_alumnos($status){
	         $resp=array();
		     $query_alumnos_ulm = "SELECT * FROM alumnos_ulm WHERE Activo_alum=".$status." ORDER BY Id_alum DESC";
		     $alumnos_ulm = mysql_query($query_alumnos_ulm, $this->cnn) or die(mysql_error());
		     $row_alumnos_ulm= mysql_fetch_array($alumnos_ulm);
		     $totalRows_alumnos_ulm= mysql_num_rows($alumnos_ulm);
		     if($totalRows_alumnos_ulm>0){
			     do{
			        array_push($resp, $this->get_alum($row_alumnos_ulm['Id_alum']));
			     }while($row_alumnos_ulm= mysql_fetch_array($alumnos_ulm));
		     }
		     return $resp;
	     }
	     
	     public function get_alum($Id_alum=null){
	         if($Id_alum==null){
		        $Id_alum=$this->Id_alum;
	         }
	         $resp=array();
		     $query_alumno  = "SELECT * FROM alumnos_ulm WHERE Id_alum=".$Id_alum;
		     $alumno = mysql_query($query_alumno, $this->cnn) or die(mysql_error());
		     $row_alumno = mysql_fetch_array($alumno);
		     $totalRows_alumno  = mysql_num_rows($alumno);
		     if($totalRows_alumno>0){
			        $resp['Id_alum']=$row_alumno['Id_alum'];
			        $resp['FechaNac_alum']=$row_alumno['FechaNac_alum'];
			        $resp['Id_foto']=$row_alumno['Id_foto'];
			        $resp['Sexo']=$row_alumno['Sexo'];
			        $resp['Id_dir']=$row_alumno['Id_dir'];
			        $resp['Curp_ins']=$row_alumno['Curp_ins'];
			        $resp['NombreTutor_alumn']=$row_alumno['NombreTutor_alumn'];
			        $resp['EmailTutor_alum']=$row_alumno['EmailTutor_alum'];
			        $resp['TelTutor_alum']=$row_alumno['TelTutor_alum'];
			        $resp['UltGrado_est_alum']=$row_alumno['UltGrado_est_alum'];
			        $resp['EscuelaPro_alum']=$row_alumno['EscuelaPro_alum'];
			        $resp['Matricula_alum']=$row_alumno['Matricula_alum'];
			        $resp['Curp_ins']=$row_alumno['Curp_ins'];
			        $resp['Alta_alum']=$row_alumno['Alta_alum'];
			        $resp['Baja_alum']=$row_alumno['Baja_alum'];
			        $resp['Activo_alum']=$row_alumno['Activo_alum'];
			        $resp['Pass_alum']=$row_alumno['Pass_alum'];
			        $resp['Grado_alum']=$row_alumno['Grado_alum'];
			        $resp['RecoverPass_alum']=$row_alumno['RecoverPass_alum'];
			        $resp['LastSession_alum']=$row_alumno['LastSession_alum'];
			        $resp['Turno_alum']=$row_alumno['Turno_alum'];
			        $resp['Opcion_pago']=$row_alumno['Opcion_pago'];
			        $resp['Img_alum']=$row_alumno['Img_alum'];
			        $resp['Ofertas']=array();
			        $resp['Ofertas']=$this->get_ofertas_alumno($row_alumno['Id_alum']);
                                    $resp['Pagos']=array();
                                    //$resp['Pagos']=$this->get_pagos_alumno($row_alumno['Id_alum']);
		     }
		     return $resp;
	     }
	     
	     
	     
	     public function get_ofertas_alumno($Id_alum=null){
	     	 if($Id_alum==null){
		        $Id_alum=$this->Id_alum;
	         }
	         $resp=array();
		     $query_ofertas_alumno = "SELECT * FROM ofertas_alumno WHERE Id_alum=".$Id_alum;
		     $ofertas_alumno = mysql_query($query_ofertas_alumno, $this->cnn) or die(mysql_error());
		     $row_ofertas_alumno= mysql_fetch_array($ofertas_alumno);
		     $totalRows_ofertas_alumno= mysql_num_rows($ofertas_alumno);
		     if($totalRows_ofertas_alumno>0){
			     do{
			        array_push($resp, $this->get_oferta_alumno($row_ofertas_alumno['Id_ofe_alum']));
			     }while($row_ofertas_alumno= mysql_fetch_array($ofertas_alumno));
		     }
		     return $resp;
	     }
             
               public function get_oferta_alumno($Id_ofe_alum){
	         $resp=array();
		     $query_ofertas_alumno = "SELECT * FROM ofertas_alumno WHERE Id_ofe_alum=".$Id_ofe_alum;
		     $ofertas_alumno = mysql_query($query_ofertas_alumno, $this->cnn) or die(mysql_error());
		     $row_ofertas_alumno= mysql_fetch_array($ofertas_alumno);
		     $totalRows_ofertas_alumno= mysql_num_rows($ofertas_alumno);
		     if($totalRows_ofertas_alumno>0){
			     do{
			        $resp['Id_ofe_alum']=$row_ofertas_alumno['Id_ofe_alum'];
			        $resp['Id_ofe']=$row_ofertas_alumno['Id_ofe'];
			        $resp['Id_alum']=$row_ofertas_alumno['Id_alum'];
			        $resp['Id_esp']=$row_ofertas_alumno['Id_esp'];
                                    $resp['Id_ori']=$row_ofertas_alumno['Id_ori'];
			        $resp['beca']=$row_ofertas_alumno['beca'];
			        $resp['descuento']=$row_ofertas_alumno['descuento']; 
                                    $resp['Opcion_pago']=$row_ofertas_alumno['Opcion_pago'];
                                    $resp['Ciclos_oferta']=array();
                                    $resp['Ciclos_oferta']=$this->get_ciclos_oferta($row_ofertas_alumno['Id_ofe_alum']);
			     }while($row_ofertas_alumno= mysql_fetch_array($ofertas_alumno));
		     }
		     return $resp;
	     }
             
             
               public function get_ciclos_oferta($Id_ofe_alum){
	         $resp=array();
                  $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=".$Id_ofe_alum." ORDER BY Id_ciclo ASC";
                  $ciclos_alum_ulm = mysql_query($query_ciclos_alum_ulm, $this->cnn) or die(mysql_error());
                  $row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm);
                  $totalRows_ciclos_alum_ulm = mysql_num_rows($ciclos_alum_ulm);
                  if($totalRows_ciclos_alum_ulm > 0) {
		    do{
		       array_push($resp, $this->get_ciclo_oferta($row_ciclos_alum_ulm['Id_ciclo_alum']));
		    }while($row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm));
		}
		 return $resp;
	     }
             
              public function get_ciclo_oferta($Id_ciclo_ulm){
	         $resp=array();
                  $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm WHERE Id_ciclo_alum=".$Id_ciclo_ulm." ORDER BY Id_ciclo ASC";
                  $ciclos_alum_ulm = mysql_query($query_ciclos_alum_ulm, $this->cnn) or die(mysql_error());
                  $row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm);
                  $totalRows_ciclos_alum_ulm = mysql_num_rows($ciclos_alum_ulm);
                  if($totalRows_ciclos_alum_ulm > 0) {
		    do{
		       $resp['Id_ciclo_alum']=$row_ciclos_alum_ulm['Id_ciclo_alum'];
                          $resp['Id_ciclo']=$row_ciclos_alum_ulm['Id_ciclo'];
                          $resp['Id_grado']=$row_ciclos_alum_ulm['Id_grado'];
                          $resp['Id_grupo']=$row_ciclos_alum_ulm['Id_grupo'];
                          $resp['Id_ofe_alum']=$row_ciclos_alum_ulm['Id_ofe_alum'];
		    }while($row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm));
		}
		 return $resp;
	     }
             
             
	     
	     
	   	
}
*/