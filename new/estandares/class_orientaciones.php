<?php
require_once("class_bdd.php");
class class_orientaciones extends class_bdd{

	     public $Id_ori;
	     
	     public function  __construct($Id_ori){
	     	   class_bdd::__construct();
	     	   if($Id_ori>0){
	     	   	  $this->Id_ori=$Id_ori;
	     	   }
	     }
	     
	     public function get_orientaciones(){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_orientaciones_uml  = "SELECT * FROM orientaciones_ulm WHERE Activo_ori=1";
		     $orientaciones_uml= mysql_query($query_orientaciones_uml, $this->cnn) or die(mysql_error());
		     $row_orientaciones_uml= mysql_fetch_array($orientaciones_uml);
		     $totalRows_orientaciones_uml = mysql_num_rows($orientaciones_uml);
		     if($totalRows_orientaciones_uml>0){
			     do{
			      array_push($resp, $this->get_ori($row_orientaciones_uml['Id_ori']));
			     }while($row_orientaciones_uml= mysql_fetch_array($orientaciones_uml));
		     }
		     return $resp;
	     }
	     
	     public function get_ori($Id_ori=null){
	     	 if($Id_ori==null){
		        $Id_ori=$this->Id_ori;
	         }
	         $resp=array();
		     $query_orientaciones_uml  = "SELECT * FROM orientaciones_ulm WHERE Id_ori=".$Id_ori;
		     $orientaciones_uml= mysql_query($query_orientaciones_uml, $this->cnn) or die(mysql_error());
		     $row_orientaciones_uml= mysql_fetch_array($orientaciones_uml);
		     $totalRows_orientaciones_uml = mysql_num_rows($orientaciones_uml);
		     if($totalRows_orientaciones_uml>0){

                        $resp['Id_ori']=$row_orientaciones_uml['Id_ori'];
                        $resp['Id_esp']=$row_orientaciones_uml['Id_esp'];
                        $resp['Nombre_ori']=$row_orientaciones_uml['Nombre_ori'];
                        $resp['Activo_ori']=$row_orientaciones_uml['Activo_ori'];
		     }
		     return $resp;
	     }
}


