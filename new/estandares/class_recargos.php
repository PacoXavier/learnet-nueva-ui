<?php
require_once("class_bdd.php");
class class_recargos extends class_bdd{

	     public $Id_recargo;
	     
	     public function  __construct($Id_recargo){
	     	   class_bdd::__construct();
	     	   if($Id_recargo>0){
	     	   	  $this->Id_recargo=$Id_recargo;
	     	   }
	     }
	     
	     public function get_recargos_pago($Id_pago){
	             $resp=array();
		     $query_Recargos_pago  = "SELECT * FROM Recargos_pago WHERE Id_pago=".$Id_pago." AND Fecha_recargo<='".date('Y-m-d')."' ORDER BY Id_recargo DESC";
		     $Recargos_pago= mysql_query($query_Recargos_pago, $this->cnn) or die(mysql_error());
		     $row_Recargos_pago= mysql_fetch_array($Recargos_pago);
		     $totalRows_Recargos_pago  = mysql_num_rows($Recargos_pago);
		     if($totalRows_Recargos_pago>0){
			     do{
			      array_push($resp, $this->get_recargo($row_Recargos_pago['Id_recargo']));
			     }while($row_Recargos_pago= mysql_fetch_array($Recargos_pago));
		     }
		     return $resp;
	     }
	     
	     public function get_recargo($Id_recargo=null){
	     	 if($Id_recargo==null){
		        $Id_recargo=$this->Id_recargo;
	         }
	         $resp=array();
                 $query_Recargos_pago = "SELECT * FROM Recargos_pago WHERE Id_recargo=".$Id_recargo;              
                 $Recargos_pago= mysql_query($query_Recargos_pago, $this->cnn) or die(mysql_error());
                 $row_Recargos_pago= mysql_fetch_array($Recargos_pago);
                 $totalRows_Recargos_pago  = mysql_num_rows($Recargos_pago);
                 if($totalRows_Recargos_pago>0){
                    $resp['Id_recargo']=$row_Recargos_pago['Id_recargo'];
                    $resp['Id_pago']=$row_Recargos_pago['Id_pago'];
                    $resp['Monto_recargo']=$row_Recargos_pago['Monto_recargo'];
                    $resp['Fecha_recargo']=$row_Recargos_pago['Fecha_recargo'];   
                 }
                 return $resp;
	     }
}
