<?php
require_once("class_bdd.php");
class class_archivos extends class_bdd{

		 public $Id_file;
	     public $description;
	     
	     public function  __construct($Id_file){
	     	   class_bdd::__construct();
	     	   if($Id_file>0){
	     	   	  $this->Id_file=$Id_file;
	     	   }
	     }
	     
	     public function get_archivos(){
	         $resp=array();
		     $query_archivos  = "SELECT * FROM archivos_ulm";
		     $archivos= mysql_query($query_archivos, $this->cnn) or die(mysql_error());
		     $row_archivos= mysql_fetch_array($archivos);
		     $totalRows_archivos  = mysql_num_rows($archivos);
		     if($totalRows_archivos>0){
			     do{
			      array_push($resp, $this->get_archivo($row_archivos['Id_file']));
			     }while($row_archivos= mysql_fetch_array($archivos));
		     }
		     return $resp;
	     }
	     
	     public function get_archivo($Id_file=null){
	     	 if($Id_file==null){
		        $Id_file=$this->Id_file;
	         }
	         $resp=array();
		     $query_archivos_ulm  = "SELECT * FROM archivos_ulm WHERE Id_file=".$Id_file;
		     $archivos_ulm= mysql_query($query_archivos_ulm, $this->cnn) or die(mysql_error());
		     $row_archivos_ulm= mysql_fetch_array($archivos_ulm);
		     $totalRows_archivos_ulm  = mysql_num_rows($archivos_ulm);
		     if($totalRows_archivos_ulm>0){

			        $resp['Id_file']=$row_archivos_ulm['Id_file'];
			        $resp['Nombre_file']=$row_archivos_ulm['Nombre_file'];

		     }
		     return $resp;
	     }

	
}
