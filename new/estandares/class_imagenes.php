<?php
require_once("class_bdd.php");
require_once("class_usuarios.php");
class class_imagenes extends class_bdd{

	     public $Id_img;
	     
	     public function  __construct($Id_img){
	     	   class_bdd::__construct();
	     	   if($Id_img>0){
	     	   	  $this->Id_img=$Id_img;
	     	   }
	     }
	     
	     public function get_imagenes(){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_Imageness = "SELECT * FROM Imagenes WHERE Id_plantel=".$usu['Id_plantel']." ORDER BY Id DESC";
		     $Imagenes= mysql_query($query_Imageness, $this->cnn) or die(mysql_error());
		     $row_Imagenes= mysql_fetch_array($Imagenes);
		     $totalRows_Imagenes  = mysql_num_rows($Imagenes);
		     if($totalRows_Imagenes>0){
			     do{
			      array_push($resp, $this->get_img($row_Imagenes['Id']));
			     }while($row_Imagenes= mysql_fetch_array($Imagenes));
		     }
		     return $resp;
	     }
	     
	     public function get_img($Id_img=null){
	     	 if($Id_img==null){
		        $Id_img=$this->Id_img;
	         }
	         $resp=array();
		     $query_Imagenes  = "SELECT * FROM Imagenes WHERE Id=".$Id_img;
		     $Imagenes= mysql_query($query_Imagenes, $this->cnn) or die(mysql_error());
		     $row_Imagenes= mysql_fetch_array($Imagenes);
		     $totalRows_Imagenes  = mysql_num_rows($Imagenes);
		     if($totalRows_Imagenes>0){

			        $resp['Id']=$row_Imagenes['Id'];
			        $resp['Llave']=$row_Imagenes['Llave'];
                                $resp['Nombre_ori']=$row_Imagenes['Nombre_ori'];
                                $resp['Nombre']=$row_Imagenes['Nombre'];
                                $resp['Id_plantel']=$row_Imagenes['Id_plantel'];
                                $resp['Id_usu']=$row_Imagenes['Id_usu'];
                                $resp['DateCreated']=$row_Imagenes['DateCreated'];
		     }
		     return $resp;
	     }  
              
}
