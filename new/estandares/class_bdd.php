<?php
error_reporting(E_ERROR);
class class_bdd {
/*
  private $server = "localhost";
  private $user_dbb = "";
  private $pass = "x!wK785F,S.N";
  protected $database_cnn = "";
  protected $cnn;
 
 */
  private $server = "";
  private $user_dbb = "";
  private $pass = "";
  protected $database_cnn = "";
  protected $cnn;

  public function __construct() {
    if(isset($_COOKIE['test_admin'])){
      $this->serve = "localhost";
      $this->user_dbb = "ulmmx_usersis";
      $this->pass= "x!wK785F,S.N";
      $this->database_cnn = "ulmmx_sistema";  
    }else{
      $this->serve = "localhost";
      $this->user_dbb = "SisteM";
      $this->pass= "SisTeM123@";
      $this->database_cnn = "sistema";      
    }
    $this->cnn = mysql_pconnect($this->server, $this->user_dbb, $this->pass) or trigger_error(mysql_error(), E_USER_ERROR);
    mysql_select_db($this->database_cnn, $this->cnn);
    mysql_query("SET NAMES 'utf8'");
    date_default_timezone_set('America/Mexico_City');
  }

  public function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

    $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

    switch ($theType) {
      case "text":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "long":
      case "int":
        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
        break;
      case "double":
        $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
        break;
      case "date":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "defined":
        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
        break;
    }
    return $theValue;
  }

  public function hashPassword($password) {
    $salt = "mdsksprwuhej6skhs0b08ojx6";
    //encrypt the password, rotate characters by length of original password
    $len = strlen($password);
    $password = md5($password);
    $password = $this->rotateHEX($password, $len);
    return md5($salt . $password);
  }

  public function rotateHEX($string, $n) {
    //for more security, randomize this string
    $chars = "4hxdxsyx3fkygsg8";
    $str = "";
    for ($i = 0; $i < strlen($string); $i++) {
      $pos = strpos($chars, $string[$i]);
      $pos += $n;
      if ($pos >= strlen($chars))
        $pos = $pos % strlen($chars);
      $str.=$chars[$pos];
    }
    return $str;
  }

  public function formatFecha($fechaSQL, $corta = 0) {
    $anioSQL = substr($fechaSQL, 0, 4);
    $mesSQL = substr($fechaSQL, 5, 2);
    $mesSQL = $mesSQL * 1;
    $mesSQL = $this->mesLetra($mesSQL);
    $diaSQL = substr($fechaSQL, 8, 2);
    $diaSQL = $diaSQL * 1;
    if ($corta == 0) {
      return ($diaSQL . " de " . $mesSQL . ", " . $anioSQL);
    } else {
      return ($diaSQL . " " . $mesSQL . ", " . substr($anioSQL, 2, 2));
    }
  }

  public function formatFecha_hora($fechaSQL) {
    $fecha = $this->formatFecha($fechaSQL);
    return $fecha = $fecha . substr($fechaSQL, strpos($fechaSQL, ' '));
  }

  public function mesLetra($mesActual) {
    if ($mesActual == 1) {
      return("Ene");
    }
    if ($mesActual == 2) {
      return("Feb");
    }
    if ($mesActual == 3) {
      return("Mar");
    }
    if ($mesActual == 4) {
      return("Abr");
    }
    if ($mesActual == 5) {
      return("May");
    }
    if ($mesActual == 6) {
      return("Jun");
    }
    if ($mesActual == 7) {
      return("Jul");
    }
    if ($mesActual == 8) {
      return("Ago");
    }
    if ($mesActual == 9) {
      return("Sep");
    }
    if ($mesActual == 10) {
      return("Oct");
    }
    if ($mesActual == 11) {
      return("Nov");
    }
    if ($mesActual == 12) {
      return("Dic");
    }
  }

  public function generarKey($largo = 13) {
    $var_count = 0;
    $chars = "abcdefghijkmnopqrstuvwxyz023456789";
    srand((double) microtime() * 1000000);
    while ($var_count < $largo) {
      $num = rand() % 33;
      $str.=substr($chars, $num, 1);
      $var_count = $var_count + 1;
    }
    return($str);
  }
  /*
  public function generar_matricula($Id_plantel) {
    //Generamos la matricula del alumno
    //Tomando como base el primer digito es del plantel al que pertence
    //y los restantes son la ultima matricula existente
    $query_inscripciones_ulm = "SELECT * FROM inscripciones_ulm WHERE (tipo=1 OR tipo=2) AND Matricula IS NOT NULL ORDER BY Matricula DESC LIMIT 1";
    $inscripciones_ulm = mysql_query($query_inscripciones_ulm, $this->cnn) or die(mysql_error());
    $row_inscripciones_ulm = mysql_fetch_array($inscripciones_ulm);
    $totalRows_inscripciones_ulm = mysql_num_rows($inscripciones_ulm);
    if ($totalRows_inscripciones_ulm > 0) {
      $matricula = substr($row_inscripciones_ulm['Matricula'], 1);
      $matricula = (int) $Id_plantel . $matricula;
      $matricula++;
    } else {
      $matricula = "100822";
    }
    return $matricula;
  }
   */
 public function generar_matricula($Id_plantel) {
    //Generamos la matricula del alumno
    //Tomando como base el primer digito es del plantel al que pertence
    //y los restantes son la ultima matricula existente
    $query_inscripciones_ulm = "SELECT * FROM inscripciones_ulm WHERE  Matricula IS NOT NULL AND Id_plantel=".$Id_plantel." ORDER BY Matricula DESC LIMIT 1";
    $inscripciones_ulm = mysql_query($query_inscripciones_ulm, $this->cnn) or die(mysql_error());
    $row_inscripciones_ulm = mysql_fetch_array($inscripciones_ulm);
    $totalRows_inscripciones_ulm = mysql_num_rows($inscripciones_ulm);
    if ($totalRows_inscripciones_ulm > 0) {
       $matricula=$row_inscripciones_ulm['Matricula'];
       $matricula++;

    }else{
        $matricula=$Id_plantel."00000";
    }
    return $matricula;
  }
  

  
  

  public function generar_referenciaPago($matricula) {
    //Se separa la matricula por digitos
    //$matricula=57134679;
    $array_matricula = str_split($matricula);
    //Iniciando de derecha a izquierda, tomamos los dígitos que conforman el número 
    //y los iremos multiplicando secuencialmente por 2 y por 1, respectivamente, hasta 
    //tomar el último dígito que conforma el número		
    $bandera = 0;
    $array_multi_secuencial = array();
    for ($i = (count($array_matricula) - 1); $i >= 0; $i--) {
      if ($bandera == 0) {
        $digito = "2";
        $bandera = 1;
      } else {
        $digito = "1";
        $bandera = 0;
      }
      array_unshift($array_multi_secuencial, ($array_matricula[$i] * $digito));
    }
    //Tomamos ahora los productos de las multiplicaciones anteriores y los sumamos.																			
    //En caso de que algunos de los productos estén integrados por dos dígitos, se separarán
    // los los dos dígitos y se suma cada uno de ellos individualmente																			

    $suma_productos = array();
    foreach ($array_multi_secuencial as $v) {
      if (strlen($v) == 2) {
        $digitos = str_split($v);
        array_push($suma_productos, ($digitos[0] + $digitos[1]));
      } else {
        array_push($suma_productos, $v);
      }
    }
    //Con el resultado de la suma de productos (40), lo dividimos entre 10													
    $total = 0;
    foreach ($suma_productos as $v) {
      $total+= $v;
    }
    //Se resta de 10, el Remanente de la division
    $remanente = ($total % 10);
    //Se resta de 10, el Remanente de la división.
    $digitoVerificador = 10 - $remanente;
    // Si el remanente de la división entre 10 es 0, el dígito verificador será 0 (cero).
    if ($remanente == 0) {
      $digitoVerificador = 0;
    }
    return $matricula . $digitoVerificador;
  }


  public function send_email($data) {
    require_once("cURL.php");
    //user: ulm_sistema@hotmail.com
    //pass: ulm_sistema
    $arrayParams = array();
    $arrayParams['key'] = "wtchUd5oZJnac6Hy2bRZpA";

    $arrayParams['message']['html'] = $data['Html'];
    $arrayParams['message']['text'] =  $data['Texto'];
    $arrayParams['message']['subject'] = $data['Asunto'];
    $arrayParams['message']['from_email'] = "noreply@ulm.mx";
    $arrayParams['message']['from_name'] = 'ULM';
    
            
    //$arrayParams['headers']['Reply-To']="";
    $arrayParams['message']['to']=array();
    foreach($data['Destinatarios'] as $k=>$v){
      $arrayTo=array();
      $arrayTo["email"]=$v['email'];
      $arrayTo["name"]=$v['nombre'];
      array_push($arrayParams['message']['to'], $arrayTo);
    }

    $array_global_merge_vars = array();
    $array_global_merge_vars["name"] = 'example name';
    $array_global_merge_vars["content"] = 'example content';

    $arrayParams['message']['global_merge_vars'] = array();
    array_push($arrayParams['message']['global_merge_vars'], $array_global_merge_vars);

    $array_merge_vars = array();
    $array_merge_vars["rcpt"] = 'example rcpt';

    $array_merge_vars['vars'] = array();
    $array_vars_attr = array();
    $array_vars_attr['name'] = 'example name';
    $array_vars_attr['content'] = 'example content';
    array_push($array_merge_vars['vars'], $array_vars_attr);

    $arrayParams['message']['merge_vars'] = array();
    array_push($arrayParams['message']['merge_vars'], $array_merge_vars);

    $arrayParams['message']['tags'] = array();
    array_push($arrayParams['message']['tags'], "example tags[]");

    $arrayParams['message']['google_analytics_domains'] = array();
    array_push($arrayParams['message']['google_analytics_domains'], "...");

    $arrayParams['message']['google_analytics_campaign'] = "...";
    $arrayParams['message']['metadata'] = array();
    array_push($arrayParams['message']['metadata'], "...");

    $array_recipient_metadata = array();
    $array_recipient_metadata["rcpt"] = 'example rcpt';

    $array_recipient_metadata['values'] = array();
    array_push($array_recipient_metadata['values'], "...");

    $arrayParams['message']['recipient_metadata'] = array();
    array_push($arrayParams['message']['recipient_metadata'], $array_recipient_metadata);
    
    $arrayParams['message']['async'] = '...';

    //create a new cURL resource
    $cc = new cURL();
    $url = "https://mandrillapp.com/api/1.0/messages/send.json";
    //send-template.json"; mensajes con temaplate
    //messages/send.json   mensajes normales
    //messages/search.json busquedas
    $data = $cc->post($url, json_encode($arrayParams));

    //Leer cadena json recibida
    //La cadena recibida tienes que cortarla asta que apetezca puro json
    $data = substr($data, strpos($data, "{"));
    $data = substr($data, 0, strpos($data, "}") + 1);
    $arr = json_decode($data, true);
    //$arr['email']; 
    //$arr['status']; 
    //$arr['_id']; 
    echo json_encode($arr);
  }
  public function get_day_pago($dateActual,$Id_ciclo){
      $fechas=array();
      $query_dias_inhabilies = "SELECT * FROM Dias_inhabiles_ciclo WHERE Id_ciclo=".$Id_ciclo;
      $dias_inhabilies = mysql_query($query_dias_inhabilies, $this->cnn) or die(mysql_error());
      $row_dias_inhabilies = mysql_fetch_array($dias_inhabilies);
      $totalRows_dias_inhabilies = mysql_num_rows($dias_inhabilies);
      if ($totalRows_dias_inhabilies > 0) {
        do{
            array_push($fechas, $row_dias_inhabilies['fecha_inh']);
        }while($row_dias_inhabilies = mysql_fetch_array($dias_inhabilies));
      } 

      $dia=date("d",strtotime($dateActual));
      $mes=date("m",strtotime($dateActual));
      $anio=date("Y",strtotime($dateActual));

      $diaDelaSemana=date('w', strtotime($dateActual));
      //Si es domingo o sabado se incrementa un dia
      if($diaDelaSemana==0 || $diaDelaSemana==6){
         $dateActual=date("Y-m-d",mktime(0, 0, 0, $mes, $dia+1, $anio));
         $dateActual=$this->get_day_pago($dateActual,$Id_ciclo);
      }else{
         $dateActual=$dateActual;
      }
      //Comprovamos que el dia actual no se 
      //encuentre  entre  los dias inhabiles 
      //del ciclo
      foreach($fechas as $k=>$v){
        if($v==$dateActual){
          $dia=date("d",strtotime($dateActual));
          $mes=date("m",strtotime($dateActual));
          $anio=date("Y",strtotime($dateActual));
          $dateActual=date("Y-m-d",mktime(0, 0, 0, $mes, $dia+1, $anio));
          $dateActual=$this->get_day_pago($dateActual,$Id_ciclo);
        }
      }
      return $dateActual;
    }
    
    
	public function get_contacto($Id_rel,$tipo){
         $resp=array();
         $query_Contactos = "SELECT * FROM Contactos WHERE IdRel_con=".$Id_rel." AND TipoRel_con='".$tipo."'";
         $Contactos= mysql_query($query_Contactos, $this->cnn) or die(mysql_error());
         $row_Contactos = mysql_fetch_array($Contactos);
         $totalRows_Contactos = mysql_num_rows($Contactos);
         if($totalRows_Contactos > 0) {
			do{
				  $resp['Id_cont']=$row_Contactos['Id_cont'];
                  $resp['Nombre']=$row_Contactos['Nombre'];
                  $resp['Parentesco']=$row_Contactos['Parentesco'];
                  $resp['Direccion']=$row_Contactos['Direccion'];
                  $resp['Tel_casa']=$row_Contactos['Tel_casa'];
                  $resp['Tel_ofi']=$row_Contactos['Tel_ofi'];
                  $resp['Cel']=$row_Contactos['Cel'];
                  $resp['Email']=$row_Contactos['Email'];
                  $resp['IdRel_con']=$row_Contactos['IdRel_con'];
                  $resp['TipoRel_con']=$row_Contactos['TipoRel_con'];

			}while($row_Contactos = mysql_fetch_array($Contactos));
		}
        return $resp;
    }
    
    public function calcularEdad ($fecha_nacimiento) {
        list($y, $m, $d) = explode("-", $fecha_nacimiento);
        $y_dif = date("Y") - $y;
        $m_dif = date("m") - $m;
        $d_dif = date("d") - $d;
        if ((($d_dif < 0) && ($m_dif == 0)) || ($m_dif < 0))
            $y_dif--;
        return $y_dif;
    }
    
   public function mesActual($mesActual) {
    if ($mesActual == 1) {
      return("Enero");
    }
    if ($mesActual == 2) {
      return("Febrero");
    }
    if ($mesActual == 3) {
      return("Marzo");
    }
    if ($mesActual == 4) {
      return("Abril");
    }
    if ($mesActual == 5) {
      return("Mayo");
    }
    if ($mesActual == 6) {
      return("Junio");
    }
    if ($mesActual == 7) {
      return("Julio");
    }
    if ($mesActual == 8) {
      return("Agosto");
    }
    if ($mesActual == 9) {
      return("Septiembre");
    }
    if ($mesActual == 10) {
      return("Octubre");
    }
    if ($mesActual == 11) {
      return("Noviembre");
    }
    if ($mesActual == 12) {
      return("Diciembre");
    }
  }
  
  public function get_archivos_entregados($Id_ofe_alum){
     $DebeArchivos=0;
     $query_archivos = "SELECT Id_ofe_alum,Id_oferta,Nombre_oferta, archivos_ofertas_ulm.Id_file,Nombre_file, ArchivosAlum.* from ofertas_alumno
     JOIN ofertas_ulm ON ofertas_alumno.Id_ofe=ofertas_ulm.Id_oferta
     JOIN archivos_ofertas_ulm ON ofertas_ulm.Id_oferta=archivos_ofertas_ulm.Id_ofe
     JOIN archivos_ulm ON archivos_ofertas_ulm.Id_file=archivos_ulm.Id_file
     LEFT JOIN (SELECT Id_file AS ArchivoEntregado FROM archivos_alum_ulm WHERE Id_ofe_alum=".$Id_ofe_alum.") AS ArchivosAlum ON archivos_ulm.Id_file=ArchivosAlum.ArchivoEntregado
     WHERE Id_ofe_alum=".$Id_ofe_alum;
     $archivos = mysql_query($query_archivos, $this->cnn) or die(mysql_error());
     $row_archivos= mysql_fetch_array($archivos);
     $totalRows_archivos= mysql_num_rows($archivos);
     if($totalRows_archivos>0){
       do{
           if($row_archivos['ArchivoEntregado']==NULL){
               $DebeArchivos++;
           }

       }while($row_archivos= mysql_fetch_array($archivos)); 
     }
    return $DebeArchivos;
}

    public function porcentaje_asistencias($Asistencias,$Justificaciones,$Faltas){
        return round((($Asistencias+$Justificaciones) / ($Asistencias + $Faltas) * 100),0,PHP_ROUND_HALF_ODD);
    }
  

}
