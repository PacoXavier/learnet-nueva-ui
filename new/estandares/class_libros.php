<?php
require_once("class_bdd.php"); 
require_once("class_usuarios.php");
require_once("class_categorias_libros.php");
class class_libros extends class_bdd{

            public $Id_libro;
	     
	     public function  __construct($Id_libro){
	     	   class_bdd::__construct();
	     	   if($Id_libro>0){
	     	   	  $this->Id_libro=$Id_libro;
	     	   }
	     }
	     
	     public function get_libros(){
                    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                    $usu = $class_usuarios->get_usu();
	         $resp=array();
		     $query_Libros = "SELECT * FROM Libros WHERE  Id_plantel=".$usu['Id_plantel']." ORDER BY Id_lib DESC";
		     $Libros = mysql_query($query_Libros, $this->cnn) or die(mysql_error());
		     $row_Libros= mysql_fetch_array($Libros);
		     $totalRows_Libros = mysql_num_rows($Libros);
		     if($totalRows_Libros>0){
                               do{
                                  array_push($resp, $this->get_libro($row_Libros['Id_lib']));
                               }while($row_Libros= mysql_fetch_array($Libros));
		     }
		     return $resp;
	     }
	     
	     public function get_libro($Id_libro=null){
	         if($Id_libro==null){
		        $Id_libro=$this->Id_libro;
	         }
	         $resp=array();
		     $query_Libros = "SELECT * FROM Libros WHERE Id_lib=".$Id_libro;
		     $Libros = mysql_query($query_Libros, $this->cnn) or die(mysql_error());
		     $row_Libros= mysql_fetch_array($Libros);
		     $totalRows_Libros = mysql_num_rows($Libros);
		     if($totalRows_Libros>0){
			        $resp['Id_lib']=$row_Libros['Id_lib'];
			        $resp['Titulo']=$row_Libros['Titulo'];
			        $resp['Autor']=$row_Libros['Autor'];
                                $resp['Editorial']=$row_Libros['Editorial'];
                                $resp['Anio']=$row_Libros['Anio'];
			        $resp['Edicion']=$row_Libros['Edicion'];
			        $resp['Seccion']=$row_Libros['Seccion'];
			        $resp['Id_cat']=$row_Libros['Id_cat'];
                                $resp['Id_subcat']=$row_Libros['Id_subcat'];
			        $resp['Id_aula']=$row_Libros['Id_aula'];
			        $resp['Descripcion']=$row_Libros['Descripcion'];
			        $resp['Id_img']=$row_Libros['Id_img'];
			        $resp['Codigo']=$row_Libros['Codigo'];
			        $resp['Id_plantel']=$row_Libros['Id_plantel'];
			        $resp['Baja_libro']=$row_Libros['Baja_libro'];
			        $resp['Valor']=$row_Libros['Valor'];
			        $resp['Fecha_adq']=$row_Libros['Fecha_adq'];
                                $resp['Garantia_meses']=$row_Libros['Garantia_meses'];
                                $resp['Comentario_baja']=$row_Libros['Comentario_baja'];
                                $resp['Disponible']=$row_Libros['Disponible'];
                                $resp['Fecha_alta']=$row_Libros['Fecha_alta'];
                                $resp['ISBN']=$row_Libros['ISBN'];
                                $resp['Sinopsis']=$row_Libros['Sinopsis'];
                                $resp['Historial_prestamos']=array();
                                          
                                $query_historial = "SELECT * FROM Historial_libro_prestamo WHERE Id_libro=".$row_Libros['Id_lib']." ORDER  BY Id_prestamo DESC";
                                $historial = mysql_query($query_historial, $this->cnn) or die(mysql_error());
                                $row_historial= mysql_fetch_array($historial);
                                $totalRows_historial = mysql_num_rows($historial);
                                if($totalRows_historial>0){
                                do{
                                    $hist=array();
                                    $hist['Id_prestamo']=$row_historial['Id_prestamo'];
                                    $hist['Id_libro']=$row_historial['Id_libro'];
                                    $hist['Comentarios']=$row_historial['Comentarios'];
                                    $hist['Usu_entrega']=$row_historial['Usu_entrega'];
                                    $hist['Usu_recibe']=$row_historial['Usu_recibe'];
                                    $hist['Estado_entrega']=$row_historial['Estado_entrega'];
                                    $hist['Estado_recibe']=$row_historial['Estado_recibe'];
                                    $hist['Fecha_entrega']=$row_historial['Fecha_entrega'];
                                    $hist['Fecha_devolucion']=$row_historial['Fecha_devolucion'];
                                    $hist['Fecha_entregado']=$row_historial['Fecha_entregado'];
                                    $hist['Tipo_usu']=$row_historial['Tipo_usu'];
                                    
                                    array_push($resp['Historial_prestamos'], $hist);
                                }while($row_historial= mysql_fetch_array($historial));
                                }
		     }
		     return $resp;
	     }	
             
              public function generar_codigo_activo($Id_cat,$Id_subcat) {
                //Generamos la matricula del activo
                //Tomando como base el primer digito es del tipo al que pertence
                //y los restantes son la ultima matricula existente
                $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                $usu = $class_usuarios->get_usu();
                
                $class_categorias_libros= new class_categorias_libros($Id_cat);
                $cat=$class_categorias_libros->get_categoria();
                
                if($Id_subcat>0){
                   $subcat=$class_categorias_libros->get_categoria($Id_subcat);
                }
                
                $query_Libros = "SELECT * FROM Libros WHERE Codigo IS NOT NULL AND Id_plantel=".$usu['Id_plantel']."  ORDER BY Id_lib DESC LIMIT 1";
                $Libros = mysql_query($query_Libros, $this->cnn) or die(mysql_error());
                $row_Libros = mysql_fetch_array($Libros);
                $totalRows_Libros = mysql_num_rows($Libros);
                $Codigo=$row_Libros['Id_lib'];
                $Codigo++;
                $ceros="";
                for($i=0;$i<(3-strlen($Codigo));$i++){
                 $ceros.="0";
                }
                $Codigo=$cat['Codigo'].$subcat['Codigo'].$ceros.$Codigo;
                return $Codigo;
              }
              
              public function update_codigo_activo($Id_cat,$Id_subcat,$Id_lib) {
                //Generamos la matricula del activo
                //Tomando como base el primer digito es del tipo al que pertence
                //y los restantes son la ultima matricula existente
                $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                $usu = $class_usuarios->get_usu();
                
                $class_categorias_libros= new class_categorias_libros($Id_cat);
                $cat=$class_categorias_libros->get_categoria();
                
                if($Id_subcat>0){
                   $subcat=$class_categorias_libros->get_categoria($Id_subcat);
                }
                
                for($i=0;$i<(3-strlen($Id_lib));$i++){
                 $ceros.="0";
                }
                $Codigo=$cat['Codigo'].$subcat['Codigo'].$ceros.$Id_lib;
                return $Codigo;
              }
              
}



