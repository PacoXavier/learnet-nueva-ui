<?php
require_once("class_bdd.php");
require_once("class_usuarios.php");
class class_aulas extends class_bdd{

	     public $Id_aula;
	     
	     public function  __construct($Id_aula){
	     	   class_bdd::__construct();
	     	   if($Id_aula>0){
	     	   	  $this->Id_aula=$Id_aula;
	     	   }
	     }
	     
	     public function get_aulas(){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_aulas = "SELECT * FROM aulas WHERE Activo_aula=1 AND Id_plantel=".$usu['Id_plantel']." ORDER BY Id_aula DESC";
		     $aulas= mysql_query($query_aulas, $this->cnn) or die(mysql_error());
		     $row_aulas= mysql_fetch_array($aulas);
		     $totalRows_aulas  = mysql_num_rows($aulas);
		     if($totalRows_aulas>0){
			     do{
			      array_push($resp, $this->get_aula($row_aulas['Id_aula']));
			     }while($row_aulas= mysql_fetch_array($aulas));
		     }
		     return $resp;
	     }
	     
	     public function get_aula($Id_aula=null){
	     	 if($Id_aula==null){
		        $Id_aula=$this->Id_aula;
	         }
	         $resp=array();
		     $query_ciclos_ulm  = "SELECT * FROM aulas WHERE Id_aula=".$Id_aula;
		     $ciclos_ulm= mysql_query($query_ciclos_ulm, $this->cnn) or die(mysql_error());
		     $row_ciclos_ulm= mysql_fetch_array($ciclos_ulm);
		     $totalRows_ciclos_ulm  = mysql_num_rows($ciclos_ulm);
		     if($totalRows_ciclos_ulm>0){

			        $resp['Id_aula']=$row_ciclos_ulm['Id_aula'];
			        $resp['Clave_aula']=$row_ciclos_ulm['Clave_aula'];
                                $resp['Nombre_aula']=$row_ciclos_ulm['Nombre_aula'];
                                $resp['Tipo_aula']=$row_ciclos_ulm['Tipo_aula'];
                                $resp['Capacidad_aula']=$row_ciclos_ulm['Capacidad_aula'];
                                $resp['Activo_aula']=$row_ciclos_ulm['Activo_aula'];
                                $resp['Id_plantel']=$row_ciclos_ulm['Id_plantel'];
		     }
		     return $resp;
	     }  
              
}
