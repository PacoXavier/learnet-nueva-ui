<?php
require_once("class_bdd.php");
class class_medios_enterar extends class_bdd{

             public $Id_medio;
	     public function  __construct($Id_medio){
	     	   class_bdd::__construct();
	     	   if($Id_medio>0){
	     	   	  $this->Id_medio=$Id_medio;
	     	   }
	     }
	     
	     public function get_medios(){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
                     
		     $query_medios_ulm = "SELECT * FROM medios_ulm WHERE Activo=1 AND Id_plantel=".$usu['Id_plantel']." ORDER BY Id_medio DESC";
		     $medios_ulm = mysql_query($query_medios_ulm, $this->cnn) or die(mysql_error());
		     $row_medios_ulm= mysql_fetch_array($medios_ulm);
		     $totalRows_medios_ulm= mysql_num_rows($medios_ulm);
		     if($totalRows_medios_ulm>0){
			     do{
			        array_push($resp, $this->get_medio($row_medios_ulm['Id_medio']));
			     }while($row_medios_ulm= mysql_fetch_array($medios_ulm));
		     }
		     return $resp;
	     }
	     
	     public function get_medio($Id_medio=null){
	         if($Id_medio==null){
		        $Id_medio=$this->Id_medio;
	         }
	         $resp=array();
                 $query_medios_ulm = "SELECT * FROM medios_ulm WHERE Id_medio=".$Id_medio;
                 $medios_ulm = mysql_query($query_medios_ulm, $this->cnn) or die(mysql_error());
                 $row_medios_ulm= mysql_fetch_array($medios_ulm);
                 $totalRows_medios_ulm= mysql_num_rows($medios_ulm);
                 if($totalRows_medios_ulm>0){
                            $resp['Id_medio']=$row_medios_ulm['Id_medio'];
                            $resp['Medio']=$row_medios_ulm['Medio'];
                            $resp['Id_plantel']=$row_medios_ulm['Id_plantel'];
                            $resp['Activo']=$row_medios_ulm['Activo'];
                 }
                 return $resp;
	     }

	     
	     
	   	
}
