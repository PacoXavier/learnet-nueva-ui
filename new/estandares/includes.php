<?php
error_reporting(E_ERROR);
if (!$_COOKIE['admin/Id_usu'] > 0 && $_SERVER['SERVER_PORT'] !== "8888" && $pagina !== "index") {
    header('Location: index.php');
}
require_once('Connections/cnn.php');
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");
date_default_timezone_set('America/Mexico_City');

$pagina = $_SERVER['SCRIPT_FILENAME'];
$pagina = substr($pagina, 0, strpos($pagina, ".php"));
while (strpos($pagina, "/") !== false) {
    $pagina = substr($pagina, strpos($pagina, "/") + 1);
}
require_once 'estandares/class_usuarios.php';
require_once 'estandares/class_tipos_usuarios.php';
require_once('clases/DaoTareasActividad.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoPlanteles.php');
require_once('clases/DaoParametrosPlantel.php');


function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}


$class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
$usu = $class_usuarios->get_usu();

$class_tipos_usuarios = new class_tipos_usuarios($usu['Tipo_usu']);
$tipo_usu = $class_tipos_usuarios->get_tipo();

$DaoParametrosPlantel= new DaoParametrosPlantel();
$params=$DaoParametrosPlantel->getParametrosPlantelArray();
$curPageURL=curPageURL();

// ver si tienes wwww
if(strpos($curPageURL,"//www.")=== false){
	$urlDest="http://www.".$params['Dominio']."/";
	$curPageURL=substr($curPageURL, strpos($curPageURL,"//")+2);
	$curPageURL=substr($curPageURL, strpos($curPageURL,"/")+1);
	//header("Location: $urlDest$curPageURL");
	//exit();
}
$DaoPlanteles=new DaoPlanteles();
$DaoUsuarios= new DaoUsuarios();

$_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
$ptl=$DaoPlanteles->show($_usu->getId_plantel());
$perm=array();

foreach($DaoUsuarios->getPermisosUsuario($_usu->getId()) as $k=>$v){
    $perm[$v['Id_per']]=1;
}

 function links_head($title) {
     global $pagina;
    ?>
    <!DOCTYPE html>
    <!--[if lt IE 7 ]> <html lang="es" class="no-js ie6"> <![endif]-->
    <!--[if IE 7 ]>    <html lang="es" class="no-js ie7"> <![endif]-->
    <!--[if IE 8 ]>    <html lang="es" class="no-js ie8"> <![endif]-->
    <!--[if IE 9 ]>    <html lang="es" class="no-js ie9"> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->

    <html lang="es" class="no-js" itemscope itemtype="http://schema.org/Article"> <!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <!--[if IE]><![endif]-->
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
            <title><?php echo $title; ?></title>
            <meta name="description" content="">
            <meta name="keywords" content="" />
            <meta name="author" content="Learnet">
            <link href='favicon.png' rel='shortcut icon' type='image/png'/>

            <!-- !CSS -->
             <?php
                if(file_exists("css/".$pagina.".css")){
                    ?>
                    <link rel="stylesheet" href="css/<?php echo $pagina; ?>.css?v=2011.5.5.13.35.4">
                   <?php
                }
                ?>
            <!-- Bootstrap Core CSS -->
            <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
            <!-- Custom CSS -->
            <link href="css/style.css" rel='stylesheet' type='text/css' />
            <!-- font CSS -->
            <!-- font-awesome icons -->
            <link href="css/font-awesome.css" rel="stylesheet"> 
            <!-- //font-awesome icons -->
             <!-- js-->
            <script src="js/jquery-1.11.1.min.js"></script>
            <script src="js/modernizr.custom.js"></script>
            <!--webfonts-->
            <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
            <!--//webfonts--> 
            <!--animate-->
            <link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
            <link rel="stylesheet" href="css/custom-css.css">
            <script src="js/wow.min.js"></script>
                <script>
                     new WOW().init();
                </script>
            <!--//end-animate-->

            <!-- Metis Menu -->
            <script src="js/metisMenu.js"></script>
            <script src="js/custom.js"></script>
            <link href="css/custom.css" rel="stylesheet">
            <!--//Metis Menu -->
            <script src="js/selectores.js"></script>
            <!-- chart -->
            <script src="js/Chart.js"></script>

            <!--Number increment -->
            <script src="js/number_increment/jquery.spincrement.js"></script>
            <!--End Number increment -->
    <?php
}


   function write_head_body() {
       global $pagina,$perm,$usu,$_usu,$ptl,$tipo_usu;
        ?>
            </head>
            <!-- !Body -->
            <body class="cbp-spmenu-push">
    <div id="errorLayer"></div>
    <div id="errorLayerSelector"></div>
    <div class="main-content">
        <!--left-fixed -navigation-->
        <div class=" sidebar" role="navigation">
            <div class="navbar-collapse">
                <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#"><i class="fa fa-users nav_icon"></i>Interesados<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="interesados.php">Interesados</a>
                                </li>
                                <li>
                                    <a href="interesado.php">Nuevo</a>
                                </li>
                                <li>
                                    <a href="reporte_seguimientos_interesados.php"">Seguimientos</a>
                                </li>
                                <li>
                                    <a href="reporte_primer_ingreso.php">Fecha de captura/ingreso</a>
                                </li>
                                <li>
                                    <a href="reporte_acciones.php">Acciones de interesados</a>
                                </li>
                                <li>
                                    <a href="reporte_ofertas_interes.php">Alumnos con interés</a>
                                </li>
                                <div class="submenu">Horarios<i class="fa fa-angle-down"></i></div>
                                    <li>
                                        <a href="reporte_salones.php">Horario salones</a>
                                    </li>
                                    <li>
                                        <a href="reporte_horario_docente.php">Horario docente</a>
                                    </li>
                            </ul>

                        </li>
                        <li>
                            <a href="#"><i class="fa fa-child nav_icon"></i>Alumnos<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="alumnos.php">Alumnos</a>
                                </li>
                                <li>
                                    <a href="reporte_horario_alumno.php">Horario</a>
                                </li>
                                <li>
                                    <a href="ofertas_alumno.php">Ofertas</a>
                                </li>
                                <li>
                                    <a href="justificacion_asistencias.php">Justificar faltas</a>
                                </li>
                                <li>
                                    <a href="amolestaciones.php">Amonestaciones</a>
                                </li>
                                <li>
                                    <a href="analisis_sicologico.php">Análisis psicológico</a>
                                </li>
                                <li>
                                    <a href="reporte_por_egresar.php">Alumnos a egresar</a>
                                </li>
                                <li>
                                    <a href="reporte_cambiar_ciclo.php">Cambio de nivel</a>
                                </li>
                                <li>
                                    <a href="aplicar_becas.php">Aplicar becas</a>
                                </li>
                                <li>
                                    <a href="reporte_documentos_faltantes.php">Documentos faltantes</a>
                                </li>
                                <li>
                                    <a href="reporte_credenciales.php">Credenciales</a>
                                </li>
                                <div class="submenu">Calificaciones<i class="fa fa-angle-down"></i></div>
                                    <li>
                                        <a href="kardex.php">Kardex</a>
                                    </li>
                                    <li>
                                        <a href="reporte_grupos_alumno.php">Alumno</a>
                                    </li>
                                    <li>
                                        <a href="reporte_calificaciones_grupo.php">Materia</a>
                                    </li>
                                    <li>
                                        <a href="capturar_calificaciones.php">Captura calificaciones</a>
                                    </li>
                                    <li>
                                        <a href="reporte_envio_calificaciones.php">Envió de calificaciones</a>
                                    </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-user-circle nav_icon"></i>Docentes<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="docentes.php">Docentes</a>
                                </li>
                                <li>
                                    <a href="docente.php">Nuevo</a>
                                </li>
                                <li>
                                    <a href="ligar_materias.php">Asignar Materias</a>
                                </li>
                                <li>
                                    <a href="evaluaciones_docente.php">Evaluar Docente</a>
                                </li>
                                <li>
                                    <a href="permutas_clase.php">Permuta clase</a>
                                </li>
                                <li>
                                    <a href="penalizacion.php">Incidencia</a>
                                </li>
                                <li>
                                    <a href="disponibilidad_docente.php"">Disponibilidad</a>
                                </li>
                                <li>
                                    <a href="reporte_horario_docente.php">Horario</a>
                                </li>
                                 <li>
                                    <a href="reporte_credenciales_docentes.php">Credenciales</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-calendar-o nav_icon"></i>Horarios<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <div class="submenu">Crear<i class="fa fa-angle-down"></i></div>
                                    <li>
                                        <a href="grupos.php">Grupos</a>
                                    </li>
                                    <li>
                                        <a href="horario_docente.php">Crear horario clase</a>
                                    </li>
                                    <li>
                                        <a href="horario_examen.php">Crear horario examen</a>
                                    </li>
                                    <li>
                                        <a href="eventos.php">Crear evento</a>
                                    </li>
                                <div class="submenu">Visualizar<i class="fa fa-angle-down"></i></div>
                                    <li>
                                        <a href="reporte_salones.php">Aulas</a>
                                    </li>
                                    <li>
                                        <a href="reporte_horario_docente.php">Docente</a>
                                    </li>
                                    <li>
                                        <a href="reporte_horario_alumno.php">Alumno</a>
                                    </li>
                                    <li>
                                        <a href="reporte_examenes.php">Exámenes</a>
                                    </li>
                                    <li>
                                        <a href="reporte_eventos.php">Eventos</a>
                                    </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-calculator nav_icon"></i>Finanzas<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="reporte_horas_trabajadas.php">Sesiones trabajadas</a>
                                </li>
                                <li>
                                    <a href="reporte_rentabilidad.php">Rentabilidad de grupos</a>
                                </li>
                                <li>
                                    <a href="estado_cuenta.php">Cobranza usuarios/docente</a>
                                </li>
                                <li>
                                    <a href="reporte_recibos.php">Recibos generados</a>
                                </li>
                                <li>
                                    <a href="reporte_alumnos_deudores.php">Deudores</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-envelope nav_icon"></i>Comunicación<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="imagenes.php">Imágenes</a>
                                </li>
                                <li>
                                    <a href="avisos.php">Enviar correo</a>
                                </li>
                                <li>
                                    <a href="notificaciones.php">Notificaciones</a>
                                </li>
                                <li>
                                    <a href="documentos_drive.php">Documentos drive</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-plus-square nav_icon"></i>Servicios<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li>
                                        <a href="reporte_seguro.php">Datos del seguro</a>
                                    </li>
                                <div class="submenu">Biblioteca<i class="fa fa-angle-down"></i></div>
                                    <li>
                                        <a href="libros.php">Libros</a>
                                    </li>
                                    <li>
                                        <a href="prestamo_libros.php">Prestamo</a>
                                    </li>
                                    <li>
                                        <a href="reporte_prestamo_libros.php">Historial</a>
                                    </li>
                                <div class="submenu">Activos<i class="fa fa-angle-down"></i></div>
                                    <li>
                                        <a href="activos.php">Activos</a>
                                    </li>
                                    <li>
                                        <a href="prestamo_activos.php">Prestamos</a>
                                    </li>
                                    <li>
                                        <a href="reporte_prestamo_activos.php">Historial</a>
                                    </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-file-text nav_icon"></i>Reportes<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="reporte_alumnos.php">Alumnos</a>
                                </li>
                                <li>
                                    <a href="reporte_alumnos_bajas.php">Bajas</a>
                                </li>
                                <li>
                                    <a href="reporte_egresados.php">Egresados</a>
                                </li>
                                <li>
                                    <a href="reporte_alumnos_sin_grupo.php">Alumnos sin grupo</a>
                                </li>
                                <li>
                                    <a href="reporte_amolestaciones.php">Amonestados</a>
                                </li>
                                <li>
                                    <a href="reporte_alumnos_becados.php">Becados</a>
                                </li>
                                <li>
                                    <a href="reporte_alumnos_reprobados.php">Reprobados</a>
                                </li>
                                <li>
                                    <a href="reporte_grupos.php">Grupos y calificaciones</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cogs nav_icon"></i>Configuración<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li>
                                    <a href="planteles.php">Planteles</a>
                                </li>
                                <li>
                                    <a href="ofertas_academicas.php">Ofertas académicas</a>
                                </li>
                                <li>
                                    <a href="configurar.php">Parametros</a>
                                </li>
                                <div class="submenu">Docentes<i class="fa fa-angle-down"></i></div>
                                    <li>
                                        <a href="categorias_evaluacion.php">Categorías de evaluación</a>
                                    </li>
                                    <li>
                                        <a href="niveles_docente.php">Tabulador de pago</a>
                                    </li>
                                <div class="submenu">Usuarios<i class="fa fa-angle-down"></i></div>
                                    <li>
                                        <a href="usuarios.php">Usuarios</a>
                                    </li>
                                    <li>
                                        <a href="usuario.php">Nuevo</a>
                                    </li>
                                    <li>
                                        <a href="actividades.php">Actividades</a>
                                    </li>
                                    <li>
                                        <a href="reporte_historial_usuarios.php">Historial</a>
                                    </li>
                            </ul>
                        </li>

                    </ul>
                    <!-- //sidebar-collapse -->
                </nav>
            </div>
        </div>
        <!--left-fixed -navigation-->
        <!-- header-starts -->
        <div class="sticky-header header-section ">
            <div class="header-left">
                <!--toggle button start-->
                <button id="showLeftPush"><i class="fa fa-bars"></i></button>
                <!--toggle button end-->
                <!--logo -->
                <div class="logo">
                        <?php
                        $logo='';
                       if(strlen($ptl->getId_img_logo())>0){
                           $logo='src="files/'.$ptl->getId_img_logo().'.jpg"';
                       }

                        ?>
                        <a href="home.php"><img <?php echo $logo?>  id="logo"></a>
                </div>
                <!--//logo-->
                <!--search-box-->
 
                <!--//end-search-box-->
                <div class="clearfix"> </div>
            </div>

            <div class="header-right">
                <div class="profile_details_left"><!--notifications of menu start -->
                    <ul class="nofitications-dropdown">
                        <li class="dropdown head-dpdn">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue">3</span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="notification_header">
                                        <h3>You have 3 new notification</h3>
                                    </div>
                                </li>
                                <li><a href="#">
                                    <div class="user_img"><img src="" alt=""></div>
                                   <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet</p>
                                    <p><span>1 hour ago</span></p>
                                    </div>
                                  <div class="clearfix"></div>  
                                 </a></li>
                                 <li class="odd"><a href="#">
                                    <div class="user_img"><img src="" alt=""></div>
                                   <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet </p>
                                    <p><span>1 hour ago</span></p>
                                    </div>
                                   <div class="clearfix"></div> 
                                 </a></li>
                                 <li><a href="#">
                                    <div class="user_img"><img src="" alt=""></div>
                                   <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet </p>
                                    <p><span>1 hour ago</span></p>
                                    </div>
                                   <div class="clearfix"></div> 
                                 </a></li>
                                 <li>
                                    <div class="notification_bottom">
                                        <a href="#">See all notifications</a>
                                    </div> 
                                </li>
                            </ul>
                        </li>   


                        <li class="dropdown head-dpdn">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tasks"></i><span class="badge blue1">15</span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="notification_header">
                                        <h3>You have 8 pending task</h3>
                                    </div>
                                </li>
                                <li><a href="#">
                                    <div class="task-info">
                                        <span class="task-desc">Database update</span><span class="percentage">40%</span>
                                        <div class="clearfix"></div>    
                                    </div>
                                    <div class="progress progress-striped active">
                                        <div class="bar yellow" style="width:40%;"></div>
                                    </div>
                                </a></li>
                                <li><a href="#">
                                    <div class="task-info">
                                        <span class="task-desc">Dashboard done</span><span class="percentage">90%</span>
                                       <div class="clearfix"></div> 
                                    </div>
                                    <div class="progress progress-striped active">
                                         <div class="bar green" style="width:90%;"></div>
                                    </div>
                                </a></li>
                                <li><a href="#">
                                    <div class="task-info">
                                        <span class="task-desc">Mobile App</span><span class="percentage">33%</span>
                                        <div class="clearfix"></div>    
                                    </div>
                                   <div class="progress progress-striped active">
                                         <div class="bar red" style="width: 33%;"></div>
                                    </div>
                                </a></li>
                                <li><a href="#">
                                    <div class="task-info">
                                        <span class="task-desc">Issues fixed</span><span class="percentage">80%</span>
                                       <div class="clearfix"></div> 
                                    </div>
                                    <div class="progress progress-striped active">
                                         <div class="bar  blue" style="width: 80%;"></div>
                                    </div>
                                </a></li>
                                <li>
                                    <div class="notification_bottom">
                                        <a href="#">See all pending tasks</a>
                                    </div> 
                                </li>
                            </ul>
                        </li>   
                    </ul>
                    <div class="clearfix"> </div>
                </div>
                <!--notification menu end -->
                <div class="profile_details">       
                    <ul>
                        <li class="dropdown profile_details_drop">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <div class="profile_img">   
                                    <span class="prfil-img">
                                             <img
                                                <?php if(strlen($usu['Img_usu'])>0){?> 
                                                src="files/<?php echo $usu['Img_usu']?>.jpg <?php } ?>">
                                    </span> 
                                    <div class="user-name">
                                        <p><?php echo ucwords(strtolower($usu['Nombre_usu']))?></p>
                                        <span> <?php echo ucwords(strtolower($tipo_usu['Nombre_tipo']))?></span>
                                    </div>
                                    <i class="fa fa-angle-down lnr"></i>
                                    <i class="fa fa-angle-up lnr"></i>
                                    <div class="clearfix"></div>    
                                </div>  
                            </a>
                            <ul class="dropdown-menu drp-mnu">
                                <li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li> 
                                <li> <a href="#"><i class="fa fa-user"></i> Profile</a> </li> 
                                <li> <a href="logout.php"><i class="fa fa-sign-out"></i>Salir</a> </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"> </div>               
            </div>
            <div class="clearfix"> </div>  

            <nav>
                <div id="box-box-tareas">
                <?php
                $DaoTareasActividad= new DaoTareasActividad();
                
                $z=count($DaoTareasActividad->getTareasActividadUserFaltantes($_COOKIE['admin/Id_usu']));
                if($z>0){
                    ?>
                        <i class="fa fa-bell" id="icon-lista-de-tareas"></i>
                        <a href="actividades.php">
                            <div id="box-count-tareas">
                                  <?php echo $z?> 
                            </div>    
                       </a>                    
                <?php
                 }
                 ?>
                </div>
             </nav> 
        </div>
        <!-- //header-ends -->

 
        <?php
    }







    function write_body() {
        global $database_cnn, $cnn,$usu,$_usu;
        ?>
                    <div id="page-wrapper">
                        <div class="main-page">
                        <div id="box-notificaciones"></div>
                    <?php
                    if(isset($_COOKIE['test_admin'])){
                    ?>
                        <span id="text_prueba">Modo de Prueba</span>
                        <?php
                    }
    }

    


     function write_footer() {
          global $pagina,$ptl;
                    ?>

                        <div id="box_alerta">
                            <p></p>
                            <!--<button onclick="ocultar_alerta()">Aceptar</button>-->
                        </div>
                   </div><!--!/#main-page -->
              </div> <!--!/#page-wrapper -->

                
                <footer><!--1811927-->

                </footer>
                <!-- Classie -->
                <script src="js/classie.js"></script>
                <script>
                    var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
                        showLeftPush = document.getElementById( 'showLeftPush' ),
                        body = document.body;
                        
                    showLeftPush.onclick = function() {
                        classie.toggle( this, 'active' );
                        classie.toggle( body, 'cbp-spmenu-push-toright' );
                        classie.toggle( menuLeft, 'cbp-spmenu-open' );
                        disableOther( 'showLeftPush' );
                    };
                    

                    function disableOther( button ) {
                        if( button !== 'showLeftPush' ) {
                            classie.toggle( showLeftPush, 'disabled' );
                        }
                    }
                </script>
                <!--scrolling js-->
                <script src="js/jquery.nicescroll.js"></script>
                <script src="js/scripts.js"></script>
                <!--//scrolling js-->

                <!-- Bootstrap Core JavaScript -->
                <script src="js/bootstrap.js"> </script>



                <?php
                if(file_exists("js/".$pagina.".js")){
                    ?>
                   <script src="js/<?php echo $pagina; ?>.js"></script> 
                   <?php
                }
                ?>
                <script src="js/header.js"></script>
                <!--<script src="jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>-->
                <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
                <link rel="stylesheet" href="jquery-ui-1.10.4.custom/css/start/jquery-ui-1.10.4.custom.min.css" />
                <!-- notificaciones-->
                <script type="text/javascript" src="js/pnotify.custom.min.js"></script>
                <link href="css/pnotify.custom.min.css" media="all" rel="stylesheet" type="text/css" />
            </body>
        </html>
        <?php
    }

    