<?php
//error_reporting(E_ALL);
require_once('Connections/cnn.php');
require_once('clases/DaoPlanteles.php');
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$DaoPlanteles= new DaoPlanteles();

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="es" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="es" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="es" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="es" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<html lang="es" class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<!--[if IE]><![endif]-->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Login</title>
<meta name="description" content="">
<meta name="keywords" content="" />
<meta name="author" content="Learnet">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
<link rel="icon" href="favicon.ico"> 

<!-- !CSS -->
<link rel="stylesheet" href="css/index.css">
<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Lato'>
</head>
<!-- !Body -->

<body>
  <div class="cont">
    <div class="demo">
      <div class="login">
        <div class="img">                         
                        <?php
                           $imgLogo="";
                           $Nombre="";
                           $server=$_SERVER["SERVER_NAME"];
                           if(strpos($server,"www")!==false){
                              $server=substr($server,strpos($server,"www")+4);
                           }
                           foreach($DaoPlanteles->showAll() as $plantel){
                               if($server==$plantel->getDominio()){
                                   $imgLogo='src="files/'.$plantel->getId_img_logo().'.jpg"';
                                   $Nombre=$plantel->getNombre_plantel();
                               }
                           }
                         ?>
			<img <?php echo $imgLogo?>>
          </div>
          <div class="login__form">
            <div class="login__row">
              <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
                <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
              </svg>
              <input type="text" class="login__input name" id="user" placeholder="Email" autocomplete="off" placeholder="Usuario"/>
            </div>
            <div class="login__row">
              <svg class="login__icon pass svg-icon" viewBox="0 0 20 20">
                <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />
              </svg>
              <input type="password" class="login__input pass" id="pass" autocomplete="off" placeholder="Contraseña"/>
            </div>
            <button type="button" class="login__submit" id="buttonLogin" onclick="login()">Ingresar</button>
            <p id="olvide" class="login__signup"><a href="reset_account.php">Olvide mi contraseña</a></p>
          </div>
      </div>
    </div>
  </div>
  	<!-- !Javascript - at the bottom for fast page loading -->
  	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>
    <script src="js/jquery_1.11.0.js?a=2"></script>

</body>
</html>

