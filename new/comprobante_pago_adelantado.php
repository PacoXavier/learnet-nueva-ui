<?php 
if($_REQUEST['id']>0){
require_once('Connections/cnn.php');
require_once('estandares/Fpdf/fpdf.php'); 
require_once('estandares/Fpdf/fpdi.php'); 
require_once("estandares/QRcode/qrlib.php");
require_once('estandares/cantidad_letra.php'); 
require_once('estandares/class_interesados.php'); 
require_once('estandares/class_ofertas.php'); 
require_once('estandares/class_metodos_pago.php');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');

// initiate FPDI 
$pdf = new FPDI(); 

// add a page 
$pdf->AddPage(); 

// set the sourcefile 
$pdf->setSourceFile('estandares/recibo.pdf'); 

// import page 1 
$tplIdx = $pdf->importPage(1); 

// use the imported page and place it at point 10,10 with a width of 100 mm 
$pdf->useTemplate($tplIdx); 

$query_Pagos_adelantados = "SELECT * FROM Pagos_adelantados WHERE Id_pad=".$_REQUEST['id'];
$Pagos_adelantados= mysql_query($query_Pagos_adelantados, $cnn) or die(mysql_error());
$row_Pagos_adelantados = mysql_fetch_array($Pagos_adelantados);
$totalRows_Pagos_adelantados= mysql_num_rows($Pagos_adelantados);
if ($totalRows_Pagos_adelantados > 0) {
$class_interesados = new class_interesados($row_Pagos_adelantados['Id_alum']);
$alum=$class_interesados->get_interesado();

// now write some text above the imported page 
$pdf->SetFont('Arial','B',8); 
$pdf->SetTextColor(0,0,0); 

$pdf->SetXY(179,4); 
$pdf->Write(10, utf8_decode(date('Y-m-d'))); 

$pdf->SetXY(77,61); 
$pdf->Write(10, utf8_decode(mb_strtoupper($alum['Nombre_ins']." ".$alum['ApellidoP_ins']." ".$alum['ApellidoM_ins']))); 

$pdf->SetXY(77,69.5); 
$pdf->Write(10, utf8_decode($alum['Matricula']));  

$subtotal=0;

    
    $pdf->SetXY(179,12); 
    $pdf->Write(10, utf8_decode($row_Pagos_adelantados['Id_pad'])); 

    $pdf->SetXY(77,105); 
    $pdf->Write(10, utf8_decode($row_Pagos_adelantados['Fecha_pago'])); 
    
    $class_metodos_pago= new class_metodos_pago($row_Pagos_adelantados['Id_met_pago']);
    $metodoPago=$class_metodos_pago->get_metodo();
        

    $Oferta_alum=$class_interesados->get_oferta_alumno($row_Pagos_adelantados['Id_ofe_alum']);
    $class_ofertas= new class_ofertas($Oferta_alum['Id_ofe']);

    $ofe=$class_ofertas->get_oferta();
    $esp=$class_ofertas->get_especialidad($Oferta_alum['Id_esp']);
    if($Oferta_alum['Id_ori']>0){
       $ori=$class_ofertas->get_orientacion($Oferta_alum['Id_ori']);
    }

    $pdf->SetXY(77,95); 
    $pdf->MultiCell(90,3 ,utf8_decode(mb_strtoupper("Colegiatura/ ".$ofe['Nombre_oferta']." - ".$esp['Nombre_esp']." ".$ori['Nombre_ori']." / ".$row_ciclo['Clave'])),0,'L'); 
    $pdf->SetXY(77,117); 
    $pdf->Write(10, mb_strtoupper(utf8_decode($metodoPago['Nombre']))); 
    $pdf->SetXY(77,78); 
    $pdf->Write(10,"$".  number_format($row_Pagos_adelantados['Monto_pago'],2)); 
    $pdf->SetXY(77,83.5); 
    $pdf->Write(10,strtoupper(num2letras($row_Pagos_adelantados['Monto_pago'],2))); 

    // add QRcode
    //texto a encliptar                                       //Ruta donde se guarda la imagen
    QRcode::png('?matricula='.$alum['Matricula'].'&folio='.$row_Pagos_pagos['Id_pad'].'&cantidad='.number_format($row_Pagos_adelantados['Monto_pago'],2), 'files/test.png');
    $pdf->Image("files/test.png", 23 , 27, 33 ,33);    
}

$pdf->Output('recibo_'.$alum['Matricula'].'.pdf', 'I'); 
}