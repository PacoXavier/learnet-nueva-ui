var rutaAjax = "ajax/create_account_aj.php"

function mostrar_error(tipo_error){
    var error=""
    if(tipo_error=="password_match"){
	   error='LAS CONTRASEÑAS NO COINCIDEN';
    }else if(tipo_error=="password_security"){
	   error='<span>POR FAVOR ELIGE UNA CONTRASEÑA CON<br>POR LO MENOS 6 CARÁCTERES, NÚMEROS Y LETRAS</span>'
    }if(tipo_error=="ok"){
	   error='CONTRASEÑA REESTABLECIDA CON ÉXITO' 
    }
    
    $('.error').html(error)
    $('.error').css('visibility','visible')
    $('.error').animate({
        opacity: 1
    },500,function(){
	    if(tipo_error=="ok"){
	       window.location="index.php"
	    }
    })
}


function reset_password(){
	var params= new Object()
	    params.action="reset_password"
	    params.pass=$('#pass').val()
	    params.confir=$('#confir').val()
	    params.key=$('#key').val()
    var functionExito= function(resp){
        if(resp.tipo.length>0){
        	mostrar_error(resp.tipo,resp.email)
	    }
    }
    $.post(rutaAjax,params,functionExito,"json")
}
