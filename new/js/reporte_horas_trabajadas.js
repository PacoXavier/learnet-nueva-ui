var rutaAjax = "ajax/reporte_horas_trabajadas_aj.php"
$(window).ready(function(){
    $(".boxfil").draggable();
})



function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_docen=$('.buscarFiltro').attr('id-data')
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Id_ofe=$('#oferta option:selected').val()
       params.Fecha_ini=$('#fecha_ini').val()
       params.Fecha_fin=$('#fecha_fin').val()
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}


function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Id_docen=$('.buscarFiltro').attr('id-data')
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Id_ofe=$('#oferta option:selected').val()
       params.Fecha_ini=$('#fecha_ini').val()
       params.Fecha_fin=$('#fecha_fin').val()
   var query=$.param( params );
   window.open(rutaAjax+"?"+query, '_blank');
}


function mostrar_grupos(Id_docen,Id_ciclo,Id_ofe,Fecha_ini,Fecha_fin){
    var params= new Object();	
        params.action="mostrar_grupos"
        params.Id_docen=Id_docen
        params.Id_ciclo=Id_ciclo
        params.Id_ofe=Id_ofe
        params.Fecha_ini=Fecha_ini
        params.Fecha_fin=Fecha_fin
    var funcionExito= function(resp){
         mostrar_error_layer(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}


 function mostrarDetallePenalizaciones(Obj){
    var params= new Object();	
        params.action="mostrarDetallePenalizaciones"
        params.Id_docen=$(Obj).closest('tr').attr('id_doc');
        params.Id_ciclo=$(Obj).closest('tr').attr('id-ciclo');
        params.Id_ofe=$(Obj).closest('tr').attr('id-ofe');
        params.Fecha_ini=$(Obj).closest('tr').attr('fecha-ini');
        params.Fecha_fin=$(Obj).closest('tr').attr('fecha-fin');
        
    var funcionExito= function(resp){
        mostrar_error_layer(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}



 function mostrarDetalleExamenes(Obj){
    var params= new Object();	
        params.action="mostrarDetalleExamenes"
        params.Id_docen=$(Obj).closest('tr').attr('id_doc');
        params.Id_ciclo=$(Obj).closest('tr').attr('id-ciclo');
        params.Id_ofe=$(Obj).closest('tr').attr('id-ofe');
        params.Fecha_ini=$(Obj).closest('tr').attr('fecha-ini');
        params.Fecha_fin=$(Obj).closest('tr').attr('fecha-fin');
        
    var funcionExito= function(resp){
        mostrar_error_layer(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}