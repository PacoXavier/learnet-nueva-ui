$(window).ready(function(){
    $(".boxfil").draggable();
})


function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_ofe=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post('reporte_por_egresar_aj.php',params,funcionExito);	
}


function mostrar_box_egresar(Id_ofe_alum){
    var params= new Object();	
        params.action="mostrar_box_egresar"
        params.Id_ofe_alum=Id_ofe_alum
    var funcionExito= function(resp){
         mostrar_error_layer(resp)
    }
    $.post("reporte_por_egresar_aj.php",params,funcionExito);
}

function save_egreso(Id_ofe_alum){
 var params= new Object();	
    params.action="save_egreso"
    params.Id_ofe_alum=Id_ofe_alum
    params.Id_ciclo=$('#Id_ciclo option:selected').val()
    if($('#Id_ciclo option:selected').val()>0){
        var funcionExito= function(resp){
           $('.table tbody').html(resp)
           ocultar_error_layer()
        }
        $.post("reporte_por_egresar_aj.php",params,funcionExito);
    }else{
        alert("Selecciona el ciclo")
    }
}


function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Id_docen=$('.buscarFiltro').attr('id-data')
       params.Id_ofe=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
       
   var query=$.param( params );
   window.open("reporte_por_egresar_aj.php?"+query, '_blank');
}

