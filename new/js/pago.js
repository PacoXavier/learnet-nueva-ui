/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function mostrar_fecha_pago(Id_pago,Num_pago){
    var params= new Object();	
        params.action="mostrar_fecha_pago"
        params.Id_pago=Id_pago
        params.Num_pago=Num_pago
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("pago_aj.php",params,funcionExito);
}

function mostrar_descuento(Id_pago,Num_pago){
    var params= new Object();	
        params.action="mostrar_descuento"
        params.Id_pago=Id_pago
        params.Num_pago=Num_pago
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("pago_aj.php",params,funcionExito);
}



function delete_recargo(Id_recargo){
   if(confirm("¿Está seguro de eliminar el recargo?")){
        var params= new Object();	
            params.action="delete_recargo"
            params.Id_recargo=Id_recargo
             params.id_ofe_alum=$('#id_ofe_alumno').val() 
        var funcionExito= function(resp){
            $('#tabla').html(resp)
            ocultar_error_layer();
        }
        $.post("pago_aj.php",params,funcionExito);
    }
}



function show_box_recargo(Id_pago){
    var params= new Object();	
        params.action="show_box_recargo"
        params.Id_pago=Id_pago
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("pago_aj.php",params,funcionExito);
}






function save_descuento(Id_pago){
    var params= new Object();	
        params.action="save_descuento"
        params.Id_pago=Id_pago
        params.descuento=$('#descuento').val() 
        params.id_ofe_alum=$('#id_ofe_alumno').val() 
    if($('#descuento').val().length>0){
    var funcionExito= function(resp){
        $('#tabla').html(resp)
        ocultar_error_layer();
    }
    $.post("pago_aj.php",params,funcionExito);
     }else{
         alert("Ingresa una cantidad")
     }
}


function mostrar_log_seguimiento(Id_pago){
    var params= new Object();	
        params.action="mostrar_log_seguimiento"
        params.Id_pago=Id_pago
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("pago_aj.php",params,funcionExito);
}





function save_comentario(Id_pago){
    var params= new Object();	
        params.action="save_comentario"
        params.Id_pago=Id_pago
        params.comentario=$('#comentario').val()
    if($('#comentario').val().length>0){
    var funcionExito= function(resp){
        $('#comentario').val('')
        $('#tabla_comentarios tbody').html(resp)
        //ocultar_error_layer();
    }
    $.post("pago_aj.php",params,funcionExito);
    }else{
        alert("Ingresa un comentario")
    }
}



function mostrar_recibo(Id_pp,Id_ciclo){
    window.open("comprobante_pago.php?id="+$('#id_alumno').val()+"&id_pp="+Id_pp+"&id_ciclo="+Id_ciclo);
}

function send_recibo(Id_pp,Id_ciclo,Obj){
    if(confirm("¿Esta seguro de enviar el recibo?")){
    var params= new Object();	
        params.action="send_recibo"
        params.id=$('#id_alumno').val()
        params.id_pp=Id_pp
        params.id_ciclo=Id_ciclo 
    var funcionExito= function(resp){
        if(resp.status!="sent"){
            alert("Recibo enviado por email")
        }else{
           alert("Error al enviar el recibo");  
        }  

    }
    $.post("pago_aj.php",params,funcionExito,"json");
   }
}
  




function send_email_bancario(Obj){
   if(confirm("¿Enviar email?")){
   var params= new Object()
       params.action="send_email_bancario"
       params.alumnos= new Array()
       params.id=$('#id_alumno').val()
       $(Obj).html('Enviando...')
       $(Obj).removeAttr('onClick')
       var funcionExito= function(resp){
           $(Obj).html('Enviar datos bancarios')
           $(Obj).attr('onClick','send_email_bancario(this)')
           alert('Mensaje enviado con éxito')
       }
        $.post('pago_aj.php',params,funcionExito);	
   }
}


function mostrar_botones(Obj){
   if($(Obj).next().is(":hidden")) {
	   $(Obj).next().slideDown("fast");
           $(Obj).html('Ocultar opciones')
   } else {
	   $(Obj).next().slideUp("fast");
           $(Obj).html('Mostrar opciones')
   }
}




function update_recargos(Id_pago){
    var params= new Object();	
        params.action="update_recargos"
        params.Id_pago=Id_pago
        params.fecha_deposito=$('#fecha_deposito').val() 
    var funcionExito= function(resp){
        $('#recargos-pago').html('$'+resp.recargos)
        $('#td-adeudo').html('$'+resp.adeudo)
        $('#adeudo').val(resp.adeudo)
        $('#monto_cubrir').val(resp.adeudo)
    }
    $.post("pago_aj.php",params,funcionExito,"json");
}



var ObjBotom
function mostrar_pago_carrito(Obj,Id_ciclo){
    var params= new Object();	
        params.action="mostrar_pago_carrito"
        params.Id_ciclo=Id_ciclo
        params.Pagos= new Array()
        $(Obj).closest('.list_usu').find('tbody tr').each(function(){
           if($(this).find('input[type="checkbox"]').is(":checked")){
                var pago= new Object()
                    pago.Id_pago=$(this).attr('id-pago')
                    params.Pagos.push(pago)
           }
        })
        ObjBotom=Obj
        if(params.Pagos.length>0){
        var funcionExito= function(resp){
            mostrar_error_layer(resp);
        }
        $.post("pago_aj.php",params,funcionExito);
    }else{
        alert("Seleccione las mensualidades a cubrir")
    }

}




function mostrar_fecha_recargo(Id_recargo){
    var params= new Object();	
        params.action="mostrar_fecha_recargo"
        params.Id_recargo=Id_recargo
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("pago_aj.php",params,funcionExito);
}


function save_fecha_recargo(Id_recargo){
    var params= new Object();	
        params.action="save_fecha_recargo"
        params.Id_recargo=Id_recargo
        params.fecha_pago=$('#fecha_pago').val() 
        params.id_ofe_alum=$('#id_ofe_alumno').val() 
    if($('#fecha_pago').val().length>0){
    var funcionExito= function(resp){
        $('#tabla').html(resp)
        ocultar_error_layer();
    }
    $.post("pago_aj.php",params,funcionExito);
     }else{
         alert("Ingresa una fecha")
     }
}


/**/

function delete_cargo(Id_pago_mis){
    if(confirm("¿Está seguro de eliminar el cargo?")){
       var params= new Object();	
            params.action="delete_cargo"
            params.Id_pago_mis=Id_pago_mis
            params.id_ofe_alum=$('#id_ofe_alumno').val() 
            params.Id_alum=$('#Id_alumn').val()
        var funcionExito= function(resp){
           $('#tabla').html(resp)
             ocultar_error_layer() 
        }
        $.post("pago_aj.php",params,funcionExito);   
    }
}


function show_miscelaneos(){
    var params= new Object();	
        params.action="show_miscelaneos"
        params.Id_ofe_alum=$('#id_ofe_alumno').val() 
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("pago_aj.php",params,funcionExito);
}


function select_miscelaneo(){
    var params= new Object();	
        params.action="select_miscelaneo"
        params.Id_mis=$('#list_mis option:selected').val()
    var funcionExito= function(resp){
        $('#costo_miscelaneo').val(resp)
    }
    $.post("pago_aj.php",params,funcionExito);
}


function save_miscelaneo(){
    var params= new Object();	
        params.action="save_miscelaneo"
        params.Id_mis=$('#list_mis option:selected').val()
        params.Id_ciclo_alum=$('#ciclo-alumno option:selected').val() 
        params.Id_ofe_alum=$('#id_ofe_alumno').val() 
        params.id_alum=$('#id_alumno').val() 
        params.cantidad=$('#cantidad').val() 
    if($('#list_mis option:selected').val()>0 && $('#ciclo-alumno option:selected').val()>0){
        var funcionExito= function(resp){
            $('#tabla').html(resp)
            ocultar_error_layer();
        } 
        $.post("pago_aj.php",params,funcionExito);
    }else{
        alert("Selecciona un miscelaneo")
    }
}


function mostrar_pago(Id_pago){
    var params= new Object();	
        params.action="mostrar_pago"
        params.Id_pago=Id_pago
        params.Id_ofe_alum=$('#id_ofe_alumno').val() 
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("pago_aj.php",params,funcionExito);
}

function save_monto(Id_pago,Obj){
    var params= new Object();	
        params.action="save_monto"
        params.Id_pago=Id_pago
        params.concepto=$('#concepto option:selected').val()
        params.monto=$('#monto_cubrir').val()
        params.Id_ofe_alum=$('#id_ofe_alumno').val() 
        params.Fecha_deposito=$('#fecha_deposito').val() 
        params.Comentarios=$('#Comentarios').val() 
        params.metodo_pago=$('#metodo_pago option:selected').val() 
        params.Id_ciclo_a_usar=$('#Id_ciclo_a_usar option:selected').val() 
        var ban=1
        if($('#concepto option:selected').val()=="-3"){
            ban=0
            if($('#Id_ciclo_a_usar option:selected').val()>0){
                ban=1
            }
        }
    if($('#monto_cubrir').val()>0 && $('#metodo_pago option:selected').val() >0 && $('#fecha_deposito').val().length>0 && $('#concepto option:selected').val()!=="-1" && ban==1){
            $(Obj).removeAttr('onclick')
            var funcionExito= function(resp){
                $('#tabla').html(resp)
                ocultar_error_layer();
               $(Obj).attr('onclick','save_monto('+Id_pago+',this)')
            }
            $.post("pago_aj.php",params,funcionExito);  
    }else{
        alert("Completa los campos")
    }
}

function delete_pago(Id_pago){
    if(confirm("¿Está seguro de eliminar el pago?")){
       var params= new Object();	
            params.action="delete_pago"
            params.Id_pago=Id_pago
            params.Id_ofe_alum=$('#id_ofe_alumno').val() 
            params.Id_alum=$('#Id_alumn').val()
        var funcionExito= function(resp){
           $('#tabla').html(resp)
             ocultar_error_layer() 
        }
        $.post("pago_aj.php",params,funcionExito);   
    }
}


function save_fecha(Id_pago){
    var params= new Object();	
        params.action="save_fecha"
        params.Id_pago=Id_pago
        params.fecha_pago=$('#fecha_pago').val() 
        params.id_ofe_alum=$('#id_ofe_alumno').val() 
    if($('#fecha_pago').val().length>0){
    var funcionExito= function(resp){
        $('#tabla').html(resp)
        ocultar_error_layer();
    }
    $.post("pago_aj.php",params,funcionExito);
     }else{
         alert("Ingresa una fecha")
     }
}

function save_recargo(Id_pago,Obj){
    var params= new Object();	
        params.action="save_recargo"
        params.Id_pago=Id_pago
        params.Fecha_recargo=$('#fecha_recargo').val() 
        params.monto=$('#monto').val() 
        params.id_ofe_alum=$('#id_ofe_alumno').val() 
    if($('#fecha_recargo').val().length>0 && $('#monto').val()>0){    
        $(Obj).removeAttr('onClick')
        var funcionExito= function(resp){
            $('#tabla').html(resp)
            $(Obj).attr('onClick','save_recargo('+Id_pago+',this)')
            ocultar_error_layer();
        }
        $.post("pago_aj.php",params,funcionExito);
    }else{
        alert("Completa los campos faltantes")
    }
}



function updateMontoCargo(){
  var adeudo=$('#concepto option:selected').attr('adeudo')
  if($('#concepto option:selected').val()=="-2" || $('#concepto option:selected').val()=="-3"){
     $('#monto_cubrir').removeAttr('readonly')
  }else{
      $('#monto_cubrir').attr('readonly','readonly')
  }
  if($('#concepto option:selected').val()=="-3"){
     $('#p-ciclo-usar').css('display','block')
  }else{
     $('#p-ciclo-usar').css('display','none') 
  }
  $('#monto_cubrir').val(adeudo)
}


function updateCampoDescuento(){
  var porcentaje=$('#paquete option:selected').attr('porcentaje')
  if($('#paquete option:selected').val()>0){
     $('#porcentajePaquete').val(porcentaje)
  }else{
     $('#porcentajePaquete').val(0)
  }
}






function show_box_paquete_descuento(Id){
    var params= new Object();	
        params.action="show_box_paquete_descuento"
        params.Id=Id
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("pago_aj.php",params,funcionExito);
}

function save_paquete_descuento(Id,Obj){
    var params= new Object();	
        params.action="save_paquete_descuento"
        params.Id=Id
        params.Id_ciclo=$('#ciclo option:selected').val() 
        params.Id_paq=$('#paquete option:selected').val() 
        params.Porcentaje=$('#paquete option:selected').attr('porcentaje')
        params.id_ofe_alum=$('#id_ofe_alumno').val() 
    if($('#ciclo option:selected').val() >0 && $('#paquete option:selected').val()>0){    
        $(Obj).removeAttr('onClick')
        var funcionExito= function(resp){
            $('#tabla').html(resp)
            $(Obj).attr('onClick','save_paquete_descuento('+Id+',this)')
            ocultar_error_layer();
        }
        $.post("pago_aj.php",params,funcionExito);
    }else{
        alert("Completa los campos faltantes")
    }
}
