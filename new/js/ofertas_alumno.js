var rutaAjax = "ajax/ofertas_alumno_aj.php"

function buscar_int(){
 if($.trim($('#email_int').val())){
       var params= new Object()
           params.action="buscarAlum"
           params.buscar=$('#email_int').val()
      var funcionExito =function(resp){
          $('#buscador_int').html(resp) 
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      $('#buscador_int').html('')
  }
}


function box_beca(Id_ofe_alum){
    var params= new Object();	
        params.action="box_beca"
        params.Id_ofe_alum=Id_ofe_alum
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}

function updatePorcentajeBeca(){
    var params= new Object();	
        params.action="updatePorcentajeBeca"
        params.Id_beca=$('#tipo_beca option:selected').val()
    var funcionExito= function(resp){
         $('#porcentaje').val(resp+"%")
    }
    $.post(rutaAjax,params,funcionExito);
}

var porcentaje=0
function getCalificacionMin(Id_beca){
    var params= new Object();	
        params.action="getCalificacionMin"
        params.Id_beca=Id_beca
    var funcionExito= function(resp){
         porcentaje=resp
    }
    $.post(rutaAjax,params,funcionExito);
}


function aplicar_beca(Id_alumn,Id_ofe_alum){
    var params= new Object();	
        params.action="aplicar_beca"
        params.Id_alum=Id_alumn
        params.Id_ofe_alum=Id_ofe_alum
        params.Tipo_beca=$('#tipo_beca option:selected').val()
        params.Id_ciclo_alum=$('#ciclo_alum option:selected').val()
        getCalificacionMin(params.Tipo_beca)
    if($('#tipo_beca option:selected').val()>0 && $('#ciclo_alum option:selected').val()>0){
        var funcionExito= function(resp){
                  if(resp.indexOf('<td id="column_one">')>0){
                     $('#tabla').html(resp) 
                      ocultar_error_layer() 
                  }else{
                      if(confirm("El alumno cuenta con un promedio de "+resp+", para aplicar la beca se requiere un mínimo de "+porcentaje+" en su calificacion")){
                          aplicar_beca(Id_alumn,Id_ofe_alum)
                      }else{
                        ocultar_error_layer()   
                      }
                  }
        }
        $.post(rutaAjax,params,funcionExito);
     }else{
         alert("Completa los campos faltantes")
     }
}

function mostrar_botones(Obj){
   if($(Obj).next().is(":hidden")) {
	   $(Obj).next().slideDown("fast");
           $(Obj).html('Ocultar')
   } else {
	   $(Obj).next().slideUp("fast");
           $(Obj).html('Mostrar')
   }
}