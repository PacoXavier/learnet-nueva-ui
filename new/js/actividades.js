var rutaAjax = "ajax/actividades_aj.php"
$(window).ready(function(){
    draggabble()
    ventanasTitle()
})

function ventanasTitle(){
   //$(document).tooltip();
   $('[title]').each(function(){
        var params= new Object();
            params.action="getTextoTooltip"
            params.Id_tarea=$(this).attr('value');
            var Id=$(this).attr('value')
            var funcionExito=function(resp){
            $('.box-input[value="'+Id+'"]').tooltip({
                content: function() {
                    return resp;
                }
            })
        }
        $.post(rutaAjax,params,funcionExito); 
   })
}




function draggabble(){
    $(".tareas" ).sortable({
            placeholder: "ui-state-highlight",
            handle: ".fa-arrows",
            stop: function(click) {
	       saveOrdenTareas(this)
        }
    });
    $(".tareas").disableSelection();
    $("#lista-actividades" ).sortable({
            placeholder: "ui-state-highlight",
            handle: ".fa-arrows",
            stop: function(click) {
	       saveOrdenActividades(this)
        }
    });
    $("#lista-actividades").disableSelection();
}


function saveOrdenActividades(Obj){
    var params= new Object();
        params.action="saveOrdenActividades"
        params.Actividades= new Array();
		$('#lista-actividades li.actividad-li').each(function(index) {
		   act= new Object();
		   act.id_act=$(this).attr('id-act')
		   params.Actividades.push(act)
	     });
	var funcionExito=function(respuesta){
	    //alert(respuesta)
	}
    $.post(rutaAjax,params,funcionExito); 	
}

function saveOrdenTareas(Obj){
    var params= new Object();
        params.action="saveOrdenTareas"
        params.Tareas= new Array();
		$(Obj).closest('.tareas ').find('li').each(function(index) {
		   tarea= new Object();
		   tarea.id_tarea=$(this).attr('id-tarea')
		   params.Tareas.push(tarea)
	     });
	var funcionExito=function(respuesta){
	    //alert(respuesta)
	}
    $.post(rutaAjax,params,funcionExito); 	
}


function mostrar_actividad(Obj){
   if($(Obj).closest('li').find('.box-tareas').is(":hidden")) {
	   $(Obj).closest('li').find('.box-tareas').slideDown("fast");
           $(Obj).attr('class','fa fa-minus-square-o')
   } else {
           $(Obj).attr('class','fa fa-plus-square-o')
	   $(Obj).closest('li').find('.box-tareas').slideUp("fast");
   }   
}
function mostrar_comentario(Obj){
   if($(Obj).closest('li').find('.comentarios-tarea-completo').is(":hidden")) {
	   $(Obj).closest('li').find('.comentarios-tarea-completo').slideDown("fast");
           $(Obj).text('menos')
   } else {
	   $(Obj).closest('li').find('.comentarios-tarea-completo').slideUp("fast");
           $(Obj).text('más')
   }   
}





function mostrar_box_actividad(Id_actividad){
    var params= new Object();	
        params.action="mostrar_box_actividad"
        params.Id_actividad=Id_actividad
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}

function save_actividad(Id_actividad){
    var params= new Object();	
        params.action="save_actividad"
        params.Id_actividad=Id_actividad
        params.Nombre=$('#nombre').val()
        params.Comentarios=$('#comentarios').val()
    if($('#nombre').val().length>0 && $('#comentarios').val().length>0){
        var funcionExito= function(resp){
           getActividadesAbiertas()
           ocultar_error_layer()
        }
        
        $.post(rutaAjax,params,funcionExito);
    }else{
        alert("Completa los campos faltantes")
    }
}

function delete_actividad(Id){
    if(confirm("¿Esta seguro de eliminar la actividad?")){
        var params= new Object();	
            params.action="delete_actividad"
            params.Id_actividad=Id
        var funcionExito= function(resp){
            getActividadesAbiertas()
        }
        $.post(rutaAjax,params,funcionExito);
    }
}


function mostrar_box_tarea(Id_tarea,Obj,Id_act){
    var params= new Object();	
        params.action="mostrar_box_tarea"
        params.Id_tarea=Id_tarea
        params.Id_act=Id_act
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
        $(".box-tareas-emergente").tabs();
    }
    $.post(rutaAjax,params,funcionExito);
}



function buscarUsuario(){
 if($.trim($('#nombre-usu').val())){
       var params= new Object()
           params.action="buscarUsuario"
           params.buscar=$('#nombre-usu').val()
      var funcionExito=function(resp){
          $('#list-usuarios').html(resp) 
          $('#list-usuarios li').click(function() {
             $('#nombre-usu').attr('id-usu',$(this).attr('id-usu'))
             $('#nombre-usu').val($(this).text())
             $('#list-usuarios').html('') 
          });
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
     $('#list-usuarios').html('')
     $('#nombre-usu').removeAttr('id-usu')
  }
}


function save_tarea(Id_tarea,Id_act){
    var params= new Object();	
        params.action="save_tarea"
        params.Id_tarea=Id_tarea
        params.Id_usuRes=$('#nombre-usu').attr('id-usu')
        params.fechaIni=$('#fechaIni').val()
        params.fechaFin=$('#fechaFin').val()
        params.comentarios=$('#comentarios').val()
        params.prioridad=$('#priodidad option:selected').val()
        params.Id_act=Id_act
        params.dependencias= new Array()
        $('.tareas-tab li input').each(function(){
             if($(this).is(':checked')){
                params.dependencias.push($(this).val())
             }
        })
        
    if($('#nombre-usu').attr('id-usu')>0 && $('#comentarios').val().length>0 && $('#priodidad option:selected').val()>0){
        var funcionExito= function(resp){
           ocultar_error_layer()
           getActividadesAbiertas()
           updateIconTareas()
        }
        $.post(rutaAjax,params,funcionExito);
    }else{
        alert("Completa los campos faltantes")
    }
}


function getTareas(Id_act,Obj){
    var params= new Object();	
        params.action="getTareas"
        params.Id_act=Id_act
        $('#act-tab li').each(function(){
            $(this).removeAttr('class')
        })
        $(Obj).addClass('actividad-activa')
        $('[list-act]').each(function(){
            $(this).css('visibility','hidden')
        })
        $('[list-act="'+Id_act+'"]').css('visibility','visible')
}


function mostrar_tareas_abiertas(){
    var params= new Object();	
        params.action="mostrar_tareas_abiertas"
    var funcionExito= function(resp){
        $('#lista-actividades').html(resp)
        draggabble()
        ventanasTitle()
        $('#tituloActividad').html('<i class="fa fa-folder-open-o"></i> Actividades abiertas')
    }
    $.post(rutaAjax,params,funcionExito);
}


function mostrar_tareas_cerradas(){
    var params= new Object();	
        params.action="mostrar_tareas_cerradas"
    var funcionExito= function(resp){
        $('#lista-actividades').html(resp)
        $('.box-tareas').css('display','none')
        $('.box-tareas').closest('li').find('table tr td i:last-child').attr('class','fa fa-plus-square-o')
        draggabble()
        ventanasTitle()
        $('#tituloActividad').html('<i class="fa fa-folder-o"></i> Actividades cerradas')
    }
    $.post(rutaAjax,params,funcionExito);
}



function tareaStatus(Obj) {
    
    if($(Obj).is(':checked')){
        var coment = prompt("Comentario");
        if (coment != null) {
            var params= new Object();	
                params.action="tareaStatus"
                params.comentario=coment
                params.id=$(Obj).attr('data-id')
                params.status=1
            var funcionExito= function(resp){
                getActividadesAbiertas()
                updateIconTareas()
            }
            $.post(rutaAjax,params,funcionExito);
        }else{
            $(Obj).removeAttr('checked')
        }
    }else{
        var params= new Object();	
            params.action="tareaStatus"
            params.comentario=coment
            params.id=$(Obj).attr('data-id')
            params.status=0
        var funcionExito= function(resp){
            $(Obj).removeAttr('checked')
            $('#lista-actividades').html(resp)
            draggabble()
            ventanasTitle()
            updateIconTareas()
        }
        $.post(rutaAjax,params,funcionExito); 
    }
}


function delete_tarea(Id){
    if(confirm("¿Esta seguro de eliminar la tarea?")){
        var params= new Object();	
            params.action="delete_tarea"
            params.Id_tarea=Id
        var funcionExito= function(resp){
           ocultar_error_layer()
           getActividadesAbiertas()
           updateIconTareas()
        }
        $.post(rutaAjax,params,funcionExito);
    }
}


function getActividadesAbiertas(Obj){
    var params= new Object();
        params.action="getActividadesAbiertas"
	var funcionExito=function(resp){
	   $('#lista-actividades').html(resp)
           draggabble()
           ventanasTitle()
	}
    $.post(rutaAjax,params,funcionExito); 	
}




function updateIconTareas(){
    var params= new Object();
        params.action="updateIconTareas"
	var funcionExito=function(resp){
	   $('#box-box-tareas').html(resp)
	}
    $.post(rutaAjax,params,funcionExito); 	
}


