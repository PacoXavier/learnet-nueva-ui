var rutaAjax = "ajax/justificacion_asistencias_aj.php"

function buscar_int(){
 if($.trim($('#email_int').val())){
       var params= new Object()
           params.action="buscar_int"
           params.buscar=$('#email_int').val()
      var funcionExito =function(resp){
          $('#buscador_int').html(resp) 
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      $('#buscador_int').html('')
  }
}


function mostrar_box_justificacion(Id_jus){
    var params= new Object();	
        params.action="mostrar_box_justificacion"
        params.Id_alum=$('#Id_alum').val()
        params.Id_ciclo=$('#ciclo option:selected').val()
        params.Id_jus=Id_jus
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
         inputTime()
    }
    $.post(rutaAjax,params,funcionExito);
}


function inputTime(){
     $('.box-justificacion .time').timepicker({
        'minTime': '07:00:00',
        'maxTime': '21:00:00',
        //'showDuration': true,
        'timeFormat': 'H:i:s',
        'step':'60'
    });
    // initialize datepair
    $('.box-justificacion').datepair();   
}



function delete_justi(Id_jus){
   if(confirm("¿Está seguro de eliminar la justificación?")){
        var params= new Object();	
            params.action="delete_justi"
            params.Id_jus=Id_jus
            params.Id_alum=$('#Id_alum').val()
            params.Id_ciclo=$('#ciclo option:selected').val()
        var funcionExito= function(resp){
            $('#tabla').html(resp)
        }
        $.post(rutaAjax,params,funcionExito);
    }
}


function mostrar_justificaciones(){
  var params= new Object()
       params.action="mostrar_justificaciones"
       params.Id_alum=$('#Id_alum').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
  var funcionExito =function(resp){
      $('#tabla').html(resp)
  }
  $.post(rutaAjax,params,funcionExito);
}




function add_justificacion(Id_jus){
  var params= new Object()
       params.action="add_justificacion"
       params.Id_alum=$('#Id_alum').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Fecha=$('#list_fechas option:selected').val()
       params.Comentarios=$('#comentario').val()
       params.Id_jus=Id_jus
       params.HoraInicio=$('#horaInicio').val()
       params.HoraFin=$('#horaFin').val()
       
  if($('#list_fechas option:selected').val().length>0 && $('#horaInicio').val().length>0 && $('#horaFin').val().length>0){
      if($('#horaFin').val()!="22:00:00"){
      var funcionExito =function(resp){
         $('#tabla').html(resp)
         ocultar_error_layer();
      }
      $.post(rutaAjax,params,funcionExito);

      }else{
          alert("La hora de fin no es válida")
      }
  }else{
      alert("Completa los campos faltantes")
  }
}