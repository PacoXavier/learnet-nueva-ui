var rutaAjax = "ajax/alumnos_aj.php"
function mostrar(id){
	window.location="alumno.php?id="+id
}

var editor
function mostrar_box_email(){
    var params= new Object();	
        params.action="mostrar_box_email"
    var funcionExito= function(resp){
        mostrar_error_layer(resp);

        // The instanceReady event is fired, when an instance of CKEditor has finished
	// its initialization.
        CKEDITOR.on( 'instanceReady', function( ev ) {
        });
        CKEDITOR.replace('editor1',{
            height: '100px',
            enterMode : CKEDITOR.ENTER_P 
            
            
        });
        editor = CKEDITOR.instances.editor1;
        
        CKEDITOR.instances.editor1.on('key', function (e){
              if (e.data.keyCode === 13) {
            // do your enter handling
              //$('#boton-email').attr('onclick','send_email(this)')
                //alert(editor)
              }

        });

    }
    $.post(rutaAjax,params,funcionExito);
}



function buscarAlum(){

   if($('#buscar').val().length>=4){
   var params= new Object()
       params.action="buscarAlum"
       params.buscar="%"
       if($.trim($('#buscar').val())){
       params.buscar=$('#buscar').val()
       }
   var funcionExito= function(resp){
	   $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
    }
}


function alerta(text){
    var div='<div id="box_emergente"><h1>'+text+'</h1><p><button class="delete">Aceptar</button><button onclick="ocultar_error_layer()">Cancelar</button></p></div>'
    mostrar_error_layer(div);	
}

function delete_alumn(Id_ins){
    if(confirm("¿Está seguro de eliminar el alumno?")){
	    var params= new Object();	
	        params.action="delete_alumn"
	        params.Id_ins=Id_ins
	    var funcionExito= function(resp){
	         $('.table tbody').html(resp)
	         ocultar_error_layer()
	    }
	    $.post(alerta,params,funcionExito);
    }
}

var ban_matricula=0;
var ban_nombre=0;
var ban_tel=0;
var ban_email=0;
var ban_cel=0;
var ban_fecha=0;
var ban_oferta=0;

function filtro(tipo,Obj){
    var params= new Object();	
        params.action="filtro"
        params.tipo=tipo
        $('.table thead tr td span').html('&#9660;')
        if(tipo=="Nombre" && ban_nombre==0){
            $(Obj).html('&#9650;')
            ban_nombre=1
        }else if(tipo=="Nombre" && ban_nombre==1){
            $(Obj).html('&#9660;')
            ban_nombre=0
        }        
        
        if(tipo=="Tel" && ban_tel==0){
            $(Obj).html('&#9650;')
            ban_tel=1
        }else if(tipo=="Tel" && ban_tel==1){
            $(Obj).html('&#9660;')
            ban_tel=0
        }
        
        if(tipo=="Email" && ban_email==0){
            $(Obj).html('&#9650;')
            ban_email=1
        }else if(tipo=="Email" && ban_email==1){
            $(Obj).html('&#9660;')
            ban_email=0
        }
         
        if(tipo=="Cel" && ban_cel==0){
            $(Obj).html('&#9650;')
            ban_cel=1
        }else if(tipo=="Cel" && ban_cel==1){
            $(Obj).html('&#9660;')
            ban_cel=0
        }
        
        if(tipo=="Matricula" && ban_matricula==0){
            $(Obj).html('&#9650;')
            ban_matricula=1
        }else if(tipo=="Matricula" && ban_matricula==1){
            $(Obj).html('&#9660;')
            ban_matricula=0
        }
        
        if(tipo=="Fecha" && ban_fecha==0){
            $(Obj).html('&#9650;')
            ban_fecha=1
        }else if(tipo=="Fecha" && ban_fecha==1){
            $(Obj).html('&#9660;')
            ban_fecha=0
        }   
        
        if(tipo=="Oferta" && ban_oferta==0){
            $(Obj).html('&#9650;')
            ban_oferta=1
        }else if(tipo=="Oferta" && ban_oferta==1){
            $(Obj).html('&#9660;')
            ban_oferta=0
        }  
        
        
        params.ban_nombre=ban_nombre
        params.ban_tel=ban_tel
        params.ban_email=ban_email
        params.ban_cel=ban_cel
        params.ban_matricula=ban_matricula
        params.ban_fecha=ban_fecha
        params.ban_oferta=ban_oferta
        
    var funcionExito= function(resp){
        $('.table tbody').html(resp)
    }
    $.post(alerta,params,funcionExito);
}


function alerta_dos(text){
    var div='<div id="box_emergente"><h1>'+text+'</h1><p><button onclick="ocultar_error_layer()">Aceptar</button></p></div>'
    mostrar_error_layer(div);	
}

function send_email(Obj){
   var params= new Object()
       params.action="send_email"
       params.asunto=$('#asunto').val()
       params.mensaje=editor.getData()
       params.alumnos= new Array()
       $('.table tbody tr ').each(function(){
           if($(this).find('input[type="checkbox"]').is(":checked")){
               params.alumnos.push($(this).attr('id_alum'))
           }
       })
       params.alum=0
       if($('#cAlumno').is(':checked')){
          params.alum=1 
       }
       params.padre=0
       if($('#cPadres').is(':checked')){
          params.padre=1 
       }
   if(params.alumnos.length>0){
       if($('#asunto').val().length>0 && params.mensaje.length>0){
           $(Obj).html('Enviando...')
           $(Obj).removeAttr('onClick')
           var funcionExito= function(resp){
              var mns='Mensaje enviado con  éxito';
               alert(mns);
               deleteAttachments()
               $(Obj).html('Enviar')
               $(Obj).attr('onClick','send_email(this)')
               ocultar_error_layer()
           }
            $.post(alerta,params,funcionExito);	
       }else{
           alert("Completa los campos faltantes")
       }
   }else{
       alert("Selecciona alumnos para enviar el mensaje")
   }
}


function marcar_alumnos(){
    if($('#all_alumns').is(":checked")){
      $('.table tbody tr  input[type="checkbox"]').prop('checked',true);
    }else{
      $('.table tbody tr  input[type="checkbox"]').prop('checked',false);
    }
}

