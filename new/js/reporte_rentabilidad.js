var rutaAjax = "ajax/reporte_rentabilidad_aj.php"

$(window).ready(function(){
    $(".boxfil").draggable();
});

function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_docen=$('.buscarFiltro').attr('id-data')
       params.Id_turno=$('#turno option:selected').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Id_ofe=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_mat=$('#lista_materias option:selected').val()
       params.Sin_docente=0
       if($('#sin_docente').is(':checked')){
           params.Sin_docente=1 
       }
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
     $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}


function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Id_docen=$('.buscarFiltro').attr('id-data')
       params.Id_turno=$('#turno option:selected').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Id_ofe=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_mat=$('#lista_materias option:selected').val()
       params.Sin_docente=0
       if($('#sin_docente').is(':checked')){
           params.Sin_docente=1 
       }
       
   var query=$.param( params );
   window.open(rutaAjax+"?"+query, '_blank');
}



