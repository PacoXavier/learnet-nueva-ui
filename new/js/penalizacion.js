var rutaAjax = "ajax/penalizacion_aj.php"
function buscar(){
 if($.trim($('#email_int').val())){
       var params= new Object()
           params.action="buscar"
           params.buscar=$('#email_int').val()
      var funcionExito =function(resp){
          $('#buscador_int').html(resp) 
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      $('#buscador_int').html('')
  }
}

function mostrar(){
  var params= new Object()
       params.action="mostrar"
       params.Id_docen=$('#Id_docen').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
  var funcionExito =function(resp){
      $('#tabla').html(resp)
  }
  $.post(rutaAjax,params,funcionExito);
}


function deletePena(Id_pena){
   if(confirm("¿Está seguro de eliminar la penalización?")){
        var params= new Object();	
            params.action="deletePena"
            params.Id=Id_pena
            params.Id_docen=$('#Id_docen').val()
            params.Id_ciclo=$('#ciclo option:selected').val()
        var funcionExito= function(resp){
            $('#tabla').html(resp)
        }
        $.post(rutaAjax,params,funcionExito);
    }
}





function mostrar_box(Id_pena){
    var params= new Object();	
        params.action="mostrar_box"
        params.Id_docen=$('#Id_docen').val()
        params.Id_ciclo=$('#ciclo option:selected').val()
        params.Id_pena=Id_pena
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
         getDiasClase()
         //inputTime()
    }
    $.post(rutaAjax,params,funcionExito);
}


function getDiasClase(){
   var params= new Object()
       params.action="getDiasClase"
       params.Id_grupo=$('#grupos option:selected').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
   if($('#grupos option:selected').val()>0){
       var funcionExito= function(resp){
           $('.box-dias-ciclo').html(resp)
           $.datepicker.regional["es"] = {
             closeText: 'Cerrar',
             prevText: '<Ant',
             nextText: 'Sig>',
             currentText: 'Hoy',
             monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
             monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
             dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
             dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
             dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
             weekHeader: 'Sm',
             dateFormat: 'dd/mm/yy',
             firstDay: 1,
             isRTL: false,
             showMonthAfterYear: false,
             yearSuffix: ''
         };
        $.datepicker.setDefaults( $.datepicker.regional["es"]);
	$('.datepicker').datepicker({
	   dateFormat: "yy-mm-dd",
           onSelect: function(){
             getHorasClase();
	    }
	});
        refreshDatePicker()
       }
       $.post(rutaAjax,params,funcionExito);	
   }
}




function refreshDatePicker(){
        var rango=new Array();
        var dias_inhabiles=new Array();
        
        //Dias del ciclo
        $(".dias_ciclo").each(function(){
               addDateToRango($(this).val()) 
        })
        
        function addDateToRango(fecha){
            var anio_ini=fecha.substr(0, 4)
            var mes_ini=fecha.substr(5, 2)
                mes_ini=parseInt(mes_ini)
            var dia_ini=fecha.substr(8, 2)
                dia_ini=parseInt(dia_ini)
            //Creamos una fecha
            //enero es 0
            var fechaIni=new Date(anio_ini,(mes_ini-1), dia_ini)	
                rango.push(mes_ini+"-"+dia_ini+"-"+anio_ini)
        }
        

        function enableAllTheseDays(date) {
            var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
            //Habilitar los dias que estan dentro del ciclo
            for (i = 0; i < rango.length; i++) {
                if($.inArray((m+1) + '-' + d + '-' + y,rango) != -1) {
                    return [true, 'ui-state-active', ''];
                }
            }

            return [false];
        }
        
       $('.datepicker').datepicker("option", "beforeShowDay",enableAllTheseDays)
       $('.datepicker').datepicker( "refresh" );
}


function getHorasClase(){
   var params= new Object()
       params.action="getHorasClase"
       params.Id_grupo=$('#grupos option:selected').val()
       params.Fecha=$('#fecha-permuta').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
   if($('#grupos option:selected').val()>0){
       var funcionExito= function(resp){
           $('#hora').html(resp)
       }
       $.post(rutaAjax,params,funcionExito);	
   }
}



function add_pena(Id_pena){
  var params= new Object()
       params.action="add_pena"
       params.Id_docen=$('#Id_docen').val()
       params.Id_grupo=$('#grupos option:selected').val()
       params.Fecha=$('#fecha-permuta').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Comentarios=$('#comentario').val()
       params.Id_pena=Id_pena
       params.Hora=$('#hora option:selected').val()

       
  if($('#fecha-permuta').val().length>0 && $('#grupos option:selected').val()>0 && $('#hora option:selected').val()>0){
      var funcionExito =function(resp){
         $('#tabla').html(resp)
         ocultar_error_layer();
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      alert("Completa los campos faltantes")
  }
}
