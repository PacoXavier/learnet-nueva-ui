var rutaAjax = "ajax/niveles_docente_aj.php";

function mostrarCategoria(Id){
    var params= new Object();	
        params.action="mostrarCategoria";
        params.Id_cat=Id;
    $.post(rutaAjax,params,function(resp){
          mostrar_error_layer(resp);
    });
}

function mostrarNivel(Id){
    var params= new Object();	
        params.action="mostrarNivel";
        params.Id_nivel=Id;
    $.post(rutaAjax,params,function(resp){
          mostrar_error_layer(resp);
    });
}



function updatePage(){
    var params= new Object();	
        params.action="updatePage";
    $.post(rutaAjax,params,function(resp){
          $('#tabla').html(resp);
    });
}

function saveCategoria(Id){
    var params= new Object();	
        params.action="saveCategoria";
        params.Id_cat=Id;
        params.nombre=$('#nombre-cat').val();
        params.ofertas= new Array();
    $('#lista-ofertas-modal input[type="checkbox"]:checked').each(function(){
        params.ofertas.push($(this).val());
    });
    if($('#nombre-cat').val().length>0){
        $.post(rutaAjax,params,function(resp){
            alert("Datos guardados con éxito");
            updatePage();
            ocultar_error_layer();
        });
     }else{
         alert("Completa los campos")
     }
}

function saveNivel(Id){
    var params= new Object();	
        params.action="saveNivel";
        params.Id_nivel=Id;
        params.nombre=$('#nombre-niv').val();
        params.puntosMin=$('#puntos-min').val();
        params.puntoMax=$('#puntos-max').val();
        params.pagoHora=$('#pago-hora').val();
        params.id_cat=$('#select-categorias option:selected').val();
    if($('#nombre-niv').val().length>0 && $('#puntos-min').val().length>0 && $('#pago-hora').val().length>0 && $('#select-categorias option:selected').val()>0){
            $.post(rutaAjax,params,function(resp){
                alert("Datos guardados con éxito");
                updatePage();
                ocultar_error_layer();
            });
        }else{
         alert("Completa los campos")
     }
}




function eliminarCategoria(Id_cat){
    if(confirm("¿Está seguro de eliminar la categoría?")){
        var params= new Object();	
            params.action="eliminarCategoria";
            params.Id_cat=Id_cat;
        $.post(rutaAjax,params,function(){
            alert("Datos guardados con éxito");
            updatePage();
        });
    }
}

function eliminarNivel(Id_nivel){
    if(confirm("¿Está seguro de eliminar el nivel?")){
        var params= new Object();	
            params.action="eliminarNivel";
            params.Id_nivel=Id_nivel;
        $.post(rutaAjax,params,function(){
            alert("Datos guardados con éxito");
            updatePage();
        });
    }
}
