var rutaAjax = "ajax/permutas_clase_aj.php"
function buscar_int(){
 if($.trim($('#email_int').val())){
       var params= new Object()
           params.action="buscar_int"
           params.buscar=$('#email_int').val()
      var funcionExito =function(resp){
          $('#buscador_int').html(resp) 
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      $('#buscador_int').html('')
  }
}

function mostrar_box_permutas(){
    var params= new Object();	
        params.action="mostrar_box_permutas"
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
         $.datepicker.regional["es"] = {
             closeText: 'Cerrar',
             prevText: '<Ant',
             nextText: 'Sig>',
             currentText: 'Hoy',
             monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
             monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
             dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
             dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
             dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
             weekHeader: 'Sm',
             dateFormat: 'dd/mm/yy',
             firstDay: 1,
             isRTL: false,
             showMonthAfterYear: false,
             yearSuffix: ''
         };
         $.datepicker.setDefaults( $.datepicker.regional["es"]);
	$('.datepicker').datepicker({
	   dateFormat: "yy-mm-dd",
           onSelect: function(){
             //getHorasClase();
	    }
	});
        refreshDatePicker()

    }
    $.post(rutaAjax,params,funcionExito);
}



function refreshDatePicker(){
        var rango=new Array();
        var dias_inhabiles=new Array();
        
        //Dias del ciclo
        $(".dias_ciclo").each(function(){
               addDateToRango($(this).val()) 
        })
        
        function addDateToRango(fecha){
            var anio_ini=fecha.substr(0, 4)
            var mes_ini=fecha.substr(5, 2)
                mes_ini=parseInt(mes_ini)
            var dia_ini=fecha.substr(8, 2)
                dia_ini=parseInt(dia_ini)
            //Creamos una fecha
            //enero es 0
            var fechaIni=new Date(anio_ini,(mes_ini-1), dia_ini)	
                rango.push(mes_ini+"-"+dia_ini+"-"+anio_ini)
        }
        

        function enableAllTheseDays(date) {
            var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
            //Habilitar los dias que estan dentro del ciclo
            for (i = 0; i < rango.length; i++) {
                if($.inArray((m+1) + '-' + d + '-' + y,rango) != -1) {
                    return [true, 'ui-state-active', ''];
                }
            }

            return [false];
        }
        
       $('#fecha-no-cubierta').datepicker("option", "beforeShowDay",enableAllTheseDays)
       $('#fecha-no-cubierta').datepicker( "refresh" );
}


function add_permuta(){
  var params= new Object()
     params.action="add_permuta"
     params.Id_grupo=$('#grupo').attr('id-data')
     params.Id_doc=$('#Id_docente').val()
     params.Id_ciclo=$('#Id_ciclo option:selected').val()
     params.FechaNoCubierta=$('#fecha-no-cubierta').val()
     params.FechaPermuta=$('#fecha-permuta').val()
     params.Hora=$('#hora option:selected').val()
     params.Id_aula=$('#aula option:selected').val()
     params.Comentarios=$('#Comentarios').val()
  if($('#Id_ciclo option:selected').val()>0 && $('#Id_docente').val()>0 && $('#grupo').attr('id-data')>0 && $('#fecha-permuta').val().length>0 && $('#hora option:selected').val()>0 && $('#fecha-no-cubierta').val().length>0){
  var funcionExito =function(resp){
      if(resp.indexOf('<td id="column_one">')!=-1){
         $('#tabla').html(resp) 
         ocultar_error_layer(resp); 
      }else{
          alert("La clase a permutar tiene caputaradas asistencias");
      }
  }
  $.post(rutaAjax,params,funcionExito);
  }else{
      alert("Completa los campos faltantes")
  }
}


function getDiasClase(){
   var params= new Object()
       params.action="getDiasClase"
       params.Id_grupo=$('#grupo').attr('id-data')
   if($('#grupo').attr('id-data')>0){
       var funcionExito= function(resp){
           $('.box-dias-ciclo').html(resp)
           refreshDatePicker()
       }
       $.post(rutaAjax,params,funcionExito);	
   }
}



function getHorasClase(){
   var params= new Object()
       params.action="getHorasClase"
       params.Id_grupo=$('#grupo').attr('id-data')
       params.Fecha=$('#fecha-permuta').val()
   if($('#grupo').attr('id-data')>0){
       var funcionExito= function(resp){
           $('#hora').html(resp)
       }
       $.post(rutaAjax,params,funcionExito);	
   }
}


function getPermutas(){
   var params= new Object()
       params.action="getPermutas"
       params.Id_ciclo=$('#Id_ciclo option:selected').val()
       params.Id_doc=$('#Id_docente').val()
   if($('#Id_ciclo option:selected').val()>0){
       var funcionExito= function(resp){
           $('#tabla').html(resp) 
       }
       $.post(rutaAjax,params,funcionExito);	
   }
}



function validarSiExistenAsistencias(Id_per){
   var params= new Object()
       params.action="validarSiExistenAsistencias"
       params.Id_per=Id_per
       params.Id_ciclo=$('#Id_ciclo option:selected').val()
       params.Id_doc=$('#Id_docente').val()
       var funcionExito= function(resp){
          if(resp==0){
              if(confirm("¿Está seguro de eliminar la permuta?")){
                  deletePermuta(Id_per)
              }
          }else{
              if(confirm("Ya existen asistencias para el grupo,¿Desea eliminar la permuta?")){
                 deletePermuta(Id_per)
              }
          }
       }
       $.post(rutaAjax,params,funcionExito);
}



function deletePermuta(Id_per){
   
   var params= new Object()
       params.action="deletePermuta"
       params.Id_per=Id_per
       params.Id_ciclo=$('#Id_ciclo option:selected').val()
       params.Id_doc=$('#Id_docente').val()
       var funcionExito= function(resp){
          $('#tabla').html(resp) 
       }
       $.post(rutaAjax,params,funcionExito);
}



function buscarDocent(Obj){
  if($.trim($(Obj).val())){
   var params= new Object()
       params.action="buscarDocent"
       params.buscar="%"
       params.Id_ciclo=$('#ciclo option:selected').val()
       if($.trim($(Obj).val())){
           params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id-data',$(this).attr('id-data'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post('index_aj.php',params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id-data')
  }
}


function buscarGruposCiclo(Obj){
   if($.trim($(Obj).val())){
   var params= new Object()
       params.action="buscarGruposCiclo"
       params.buscar="%"
       params.Id_ciclo=$('#Id_ciclo option:selected').val()
       if($.trim($(Obj).val())){
           params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id-data',$(this).attr('id-data'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
          //$('#hora').html('')
          $('#fecha-permuta').val('')
          getDiasClase()
      });
   }
   $.post('index_aj.php',params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id-data')
      $('.box-dias-ciclo').html('')
      $('#hora').html('')
      $('#fecha-permuta').val('')
  }
}
function verificarDisponibilidad(){
     var params= new Object();	
        params.action="verificarDisponibilidad"
        params.Id_ciclo=$('#Id_ciclo option:selected').val()
        params.FechaPermuta=$('#fecha-permuta').val()
        params.Id_Hora=$('#hora option:selected').val()
        params.Id_aula=$('#aula option:selected').val()
    if($('#Id_ciclo option:selected').val()>0 && $('#aula option:selected').val()>0 && $('#fecha-permuta').val().length>0){
            var funcionExito= function(resp){
                if(resp.Ban==1){
                   if(confirm(resp.Texto)){
                       add_permuta()
                   }else{
                       ocultar_error_layer()
                   }
               }else{
                   add_permuta()
               }
            }
            $.post(rutaAjax,params,funcionExito,"json");
    }else{
        alert("Completa los campos faltantes")
    }   
}



/*
function mostrarGrupo(){
   var params= new Object()
       params.action="mostrarGrupo"
       params.Id_ciclo=$('#ciclo option:selected').val()
   if($('#ciclo option:selected').val()>0){
       var funcionExito= function(resp){
           $('#box-grupo').html(resp)
           $('.box-dias-ciclo').html('')
           $('#fecha-permuta').val('')
           $('#hora').html('')
       }
       $.post('permutas_clase_aj.php',params,funcionExito);	
   }else{
       $('#box-grupo').html('')
   }  
}
*/