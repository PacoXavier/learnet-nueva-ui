var rutaAjax = "ajax/reporte_alumnos_bajas_aj.php"

$(window).ready(function(){
    $(".boxfil").draggable();
})
function buscarAlum(){
   if($.trim($('.buscarFiltro').val()) && $('.buscarFiltro').val().length>=4){
   var params= new Object()
       params.action="buscarAlum"
       params.buscar="%"
       if($.trim($('.buscarFiltro').val())){
       params.buscar=$('.buscarFiltro').val()
       }
   var funcionExito= function(resp){
      $('.Ulbuscador').html(resp)
      $('.Ulbuscador li').on("click", function(event){
          $('.buscarFiltro').val($(this).text())
          $('.buscarFiltro').attr('id_alum',$(this).attr('id_alum'))
          $('.Ulbuscador').html('')
       });
   }
   $.post(rutaAjax,params,funcionExito);	
   }else{
      $('.Ulbuscador').html('')
      $('.buscarFiltro').removeAttr('id_alum')
  }
}

function alumno(id){
    window.location="alumno.php?id="+id
}

function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_alum=$('.buscarFiltro').attr('id_alum')
       params.fecha_ini=$('#fecha_ini').val()
       params.fecha_fin=$('#fecha_fin').val()
       params.Id_motivo=$('#motivos option:selected').val()
       params.Id_oferta=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}



function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Id_alum=$('.buscarFiltro').attr('id_alum')
       params.fecha_ini=$('#fecha_ini').val()
       params.fecha_fin=$('#fecha_fin').val()
       params.Id_motivo=$('#motivos option:selected').val()
       params.Id_oferta=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
   var query=$.param( params );
   window.open(rutaAjax+"?"+query, '_blank');
}

function mostrarCicloReactivar(Id_ofe_alum){
   var params= new Object()
       params.action="mostrarCicloReactivar"
       params.Id_ofe_alum=Id_ofe_alum
   var funcionExito= function(resp){
       mostrar_error_layer(resp);
   }
   $.post(rutaAjax,params,funcionExito);	
}


function reactivarOferta(Id_ofe_alum){
   if(confirm("¿Está seguro de reactivar la oferta del alumno?")){
       var params= new Object()
           params.action="reactivarOferta"
           params.Id_ofe_alum=Id_ofe_alum
           params.Id_ciclo=$('#ciclo option:selected').val()
           params.generarMensualiadades=0
           if($('#generarMensualiadades').is(":checked")){
               params.generarMensualiadades=1
           }
       if($('#ciclo option:selected').val()>0){
           var funcionExito= function(resp){
               ocultar_error_layer();
               getAlumnosBajas();
               alert("Alumno reactivado")
           }
           $.post(rutaAjax,params,funcionExito);	
       }else{
           alert("Selecciona el ciclo")
       }
   }
}

function mostrarReactivarSinCiclo(Id_ofe_alum){
   var params= new Object()
       params.action="mostrarReactivarSinCiclo"
       params.Id_ofe_alum=Id_ofe_alum
   var funcionExito= function(resp){
       mostrar_error_layer(resp);
   }
   $.post(rutaAjax,params,funcionExito);	
}

function getCursosEspecialidad(Id_ofe_alum){
    var params= new Object();	
        params.action="getCursosEspecialidad"
        params.Id_ofe_alum=Id_ofe_alum
    var funcionExito= function(resp){
        $('#cursos-especialidad').html(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}


function mostrar_box_masivo(){
    var params= new Object();	
        params.action="mostrar_box_masivo"
    if($('.table tbody tr td input[type="checkbox"]:checked').size()>0){
        var funcionExito= function(resp){
            mostrar_error_layer(resp);
        }
        $.post(rutaAjax,params,funcionExito);
    }else{
        alert("Selecciona por lo menos un alumno a reactivar")
    }
}

var size
function confirmarReactivacion(){
    if($('#ciclo option:selected').val()>0){
        size=$('.table tbody tr td input[type="checkbox"]:checked').size();
        if(size>0){
            reactivacionMasivaAlumnos();
        }else{
            alert("Selecciona por lo menos un alumno a reactivar");
        }
    }else{
           alert("Selecciona el ciclo")
    }
}


var row=0;
var arrayRows= new Array();
function reactivacionMasivaAlumnos(){
    var numRows=$('.table tbody tr td input[type="checkbox"]:checked').size();
    $('.table tbody tr td input[type="checkbox"]:checked').each(function(index){
        arrayRows.push($(this).closest('tr').attr('num-tr'));
        if((index+1)==numRows){
            doRow();
        }
    });
}

function doRow(){
    var params= new Object()
        params.action="reactivarOferta";
        params.Id_ofe_alum=$('tr[num-tr="'+arrayRows[row]+'"]').attr("id-ofe-alum");
        params.Id_ciclo=$('#ciclo option:selected').val();
        params.generarMensualiadades=0;
        if($('#generarMensualiadades').is(":checked")){
            params.generarMensualiadades=1;
        }
    $.post(rutaAjax,params,function(resp){
        if(row<(size-1)){
           row++;
           doRow();
        }else{
           row=0;
           getAlumnosBajas();
           ocultar_error_layer();
           alert("Alumnos reactivados con éxito");
        }
    });
}


function getAlumnosBajas(){
     var params= new Object()
         params.action="getAlumnosBajas";
    $.post(rutaAjax,params,function(resp){
          $('.table tbody').html(resp)
    });
}


function confirmarReactivacionSinCiclo(Id_ofe_alum){
    if(confirm("¿Está seguro de reactivar la oferta del alumno?")){
        var params= new Object()
            params.action="confirmarReactivacionSinCiclo";
            params.Id_curso_esp=$("#cursos-especialidad option:selected").val();
            params.Id_ofe_alum=Id_ofe_alum;
            params.generarMensualidades=0;
            if($('#generarMensualiadades').is(":checked")){
                params.generarMensualidades=1;
            }
        if($("#cursos-especialidad option:selected").val()>0){
            $.post(rutaAjax,params,function(){
                getAlumnosBajas();
                ocultar_error_layer();
            });
         }else{
             alert("No existe un curso disponible");
         }
    }
}