var rutaAjax = "ajax/prestamo_libros_aj.php"

function box_prestamo(){
    var params= new Object();	
        params.action="box_prestamo"
        params.Id_lib=$('#Id_lib').val()
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}
function box_recepcion(){
    var params= new Object();	
        params.action="box_recepcion"
        params.Id_lib=$('#Id_lib').val()
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}




function buscarActivo(){
   var params= new Object()
       params.action="buscarActivo"
       params.buscar="%"
       if($.trim($('#buscar').val())){
       params.buscar=$('#buscar').val()
       }
   var funcionExito= function(resp){
	   $('#list_usu tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}


function save_prestamo(Obj){
    var params= new Object();	
        params.action="save_prestamo"
        params.Id_lib=$('#Id_lib').val()
        params.usuario_entrega=$('#usuario_entrega').val()
        params.usuario_recibe=$('#usuario_recibe').attr('Id_usu')
        params.tipo=$('#usuario_recibe').attr('tipo_usu')
        params.fecha_devolucion=$('#fecha_devolucion').val()
        params.Estado_entrega=$('#Estado_entrega').val()
    if($('#usuario_recibe').val().length>0 && $('#fecha_devolucion').val().length>0 && $('#usuario_recibe').attr('Id_usu')>0 &&  $('#usuario_recibe').attr('tipo_usu').length>0){
        $(Obj).html('Guardando...')
        $(Obj).removeAttr('onClick')
        var funcionExito= function(resp){
           $('#contenido').html(resp)
           $(Obj).html('Guardar')
           $(Obj).attr('onClick','save_prestamo(this)')
	  ocultar_error_layer()
        }
        $.post(rutaAjax,params,funcionExito);
    }else{
        alert("Completa los datos faltantes")
    }
}

function save_recepcion(){
    var params= new Object();	
        params.action="save_recepcion"
        params.Id_lib=$('#Id_lib').val()
        params.Id_prestamo=$('#Id_prestamo').val()
        params.Estado_entrega=$('#Estado_entrega').val()
    if($('#Estado_entrega').val().length>0){
        var funcionExito= function(resp){
           $('#contenido').html(resp)
	  ocultar_error_layer()

        }
        $.post(rutaAjax,params,funcionExito);
    }else{
        alert("Completa los datos faltantes")
    }
}

function buscar_int(){
 if($.trim($('#email_int').val())){
       var params= new Object()
           params.action="buscar_int"
           params.buscar=$('#email_int').val()
      var funcionExito =function(resp){
          $('#buscador_int').html(resp) 
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      $('#buscador_int').html('')
  }
}


function buscarUsuarioPrestamo(){
 if($.trim($('#usuario_recibe').val())){
       var params= new Object()
           params.action="buscarUsuarioPrestamo"
           params.buscar=$('#usuario_recibe').val()
      var funcionExito=function(resp){
          $('#buscador_prestamo').html(resp) 
          $('#buscador_prestamo li').click(function() {
             $('#usuario_recibe').attr('Id_usu',$(this).attr('Id_usu'))
             $('#usuario_recibe').attr('tipo_usu',$(this).attr('tipo_usu'))
             $('#usuario_recibe').val($(this).text())
             $('#buscador_prestamo').html('') 
          });
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
     $('#buscador_prestamo').html('')
     $('#usuario_recibe').removeAttr('Id_usu')
     $('#usuario_recibe').removeAttr('tipo_usu')
  }
}
