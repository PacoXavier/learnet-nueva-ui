var rutaAjax = "ajax/alumno_aj.php"

function mostrarFinder(){
    $('#files').click()
}  
if($('#files').length){
        document.getElementById('files').addEventListener('change', handleFileSelect, false);
}

function imprimir(){
    window.print()
} 

  
function handleFileSelect(evt) {
    //Check for the various File API support.
	if (window.File && window.FileReader && window.FileList && window.Blob) {
	    // Great success! All the File APIs are supported.

	    // FileList object
		var files = evt.target.files; 
		
		var count=0;
		var bin,name,type,size
		
		//Funcion loadStartImg
		var loadStartImg=function() { 
			//alert("inicia") 
		}
		
		//Funcion loadEndImg
		var loadEndImg=function(e) {
			var xhr
			if(window.XMLHttpRequest){
				 xhr = new XMLHttpRequest();
			}else if(window.ActiveXObject){
				 xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
			//var bin = reader.result;
			
			// progress bar loadend
			var eventSource = xhr.upload || xhr;
			    eventSource.addEventListener("progress", function(e) {  
			var pc = parseInt((e.loaded / e.total * 100));  
	            //$('#mascara_img span').html(pc+'%') 
	             var mns='Cargando ...'+pc+'%'
                 alerta(mns);
                 if(pc==100){
	                ocultar_error_layer() 
                 }
			}, false); 
				            
			xhr.onreadystatechange=function(){
				if(xhr.readyState==4 && xhr.status==200){
				  $('.foto_alumno').attr('style','background-image:url(files/'+xhr.responseText+'.jpg)')
				}
			}
			xhr.open('POST', rutaAjax+'?action=upload_image&Id_alum='+$('#Id_alumn').val(), true);
			var boundary = 'xxxxxxxxx';
			var body = '--' + boundary + "\r\n";  
			body += "Content-Disposition: form-data; name='upload'; filename='" + name + "'\r\n";  
			body += "Content-Type: application/octet-stream\r\n\r\n";  
			body += bin + "\r\n";  
			body += '--' + boundary + '--';      
			xhr.setRequestHeader('content-type', 'multipart/form-data; boundary=' + boundary);
			// Firefox 3.6 provides a feature sendAsBinary ()
			if(xhr.sendAsBinary != null) { 
				xhr.sendAsBinary(body); 
			// Chrome 7 sends data but you must use the base64_decode on the PHP side
			} else {
				xhr.open('POST', rutaAjax+'?action=upload_image&Id_alum='+$('#Id_alumn').val()+'&base64=ok&FileName='+name+'&TypeFile='+type, true);
				xhr.setRequestHeader('UP-FILENAME', name);
				xhr.setRequestHeader('UP-SIZE', size);
				xhr.setRequestHeader('UP-TYPE', type);
				xhr.send(window.btoa(bin));
			}
			if(status) {
			   //document.getElementById(status).innerHTML = '';
			}	
		}
		
		//Funcion loadErrorImg
		var loadErrorImg=function(evt) {
			switch(evt.target.error.code) {
			  case evt.target.error.NOT_FOUND_ERR:
			    alert('File Not Found!');
			    break;
			  case evt.target.error.NOT_READABLE_ERR:
			    alert('File is not readable');
			    break;
			  case evt.target.error.ABORT_ERR:
			    break; // noop
			  default:
			    alert('An error occurred reading this file.');
			};
		}
		
	    //Loop through the FileList and render image files as thumbnails.
		for (var i = 0, f; f = files[i]; i++) {
			  // Only process image files.
			  if (!f.type.match('image.*')) {
			    continue;
			  }		 
			  
			  var reader = new FileReader();
			  var preview = new FileReader();
			     
			  // Closure to capture the file information.
			  reader.onload = (function(theFile) {
			    return function(e) {
			      // Render thumbnail binari.
			      bin =e.target.result
			      name=theFile.name
			      type=theFile.type
			      size=theFile.size
			    };
			  })(f);
			  
			  preview.onload = (function(theFile) {
			    return function(e) {
			      // Render thumbnail.
			      /*
			      var li = document.createElement('li');
			          $(li).attr('id','img_'+count)
			          li.innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/><div id="mascara_img"><span></span></div>'].join('');
			      $('#visor_subir_img').append(li)
			      $('#visor_subir_img li img').css({'width':'85px','height':'85px'});
			      $('#ruta_img').val($('#files').val())
			      count++;
			      */
			    };
			  })(f);
			  
			  //Read in the image file as a binary string.
			  reader.readAsBinaryString(f);
			  //Read in the image file as a data URL.
			  preview.readAsDataURL(f);
			 	      
			  // Firefox 3.6, WebKit
			  if(reader.addEventListener) { 
					reader.addEventListener('loadend', loadEndImg, false);
					reader.addEventListener('loadstart', loadStartImg, false);
					if(status != null) {
						reader.addEventListener('error', loadErrorImg, false);
					}
			  // Chrome 7
			  } else { 
					reader.onloadend = loadEndImg;
					reader.onloadend = loadStartImg;
					if (status != null) {
						reader.onerror = loadErrorImg;
					}
			  }
		}
	  
	} else {
	   alert('The File APIs are not fully supported in this browser.');
	}
}

function box_documents(){
    var params= new Object();	
        params.action="box_documents"
        params.Id_alum=$('#Id_alumn').val()
    var funcionExito= function(resp){
        $('#cssPrintpage').remove()
        $('head').append('<link rel="stylesheet" media="print" href="css/alumno_documentos.css?v=2011.6.16.18.36" id="cssPrintDocument"> ')
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}

function cssImprimirPage(){
    $('#cssPrintDocument').remove()
     $('head').append('<link rel="stylesheet" media="print" href="css/alumno_print.css?v=2011.6.16.18.35" id="cssPrintpage"> ')
}
function box_curso(){
    var params= new Object();	
        params.action="box_curso"
        params.Id_alum=$('#Id_alumn').val()
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}

function box_curso_interes(){
    var params= new Object();	
        params.action="box_curso_interes"
        params.Id_alum=$('#Id_alumn').val()
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}

function add_ofe_alum(){
    var params= new Object();	
        params.action="add_ofe_alum"
        params.Id_alumn=$('#Id_alumn').val()
        params.Id_oferta=$('#oferta option:selected').val()
        params.Id_esp=$('#curso option:selected').val()
        params.Id_ori=$('#orientacion option:selected').val()
        params.Id_grado=$('#grado option:selected').val()
        params.Id_ciclo=$('#ciclo option:selected').val()
        params.opcion=$('#opcionPago option:selected').val()
        params.Turno=$('#turno option:selected').val()
        params.Id_curso_esp=$('#cursos-especialidad option:selected').val()
        
    if($('#oferta option:selected').val()>0 && $('#curso option:selected').val()>0 && $('#grado option:selected').val()>0 && $('#opcionPago option:selected').val() && ($('#ciclo option:selected').val()>0 || $('#cursos-especialidad option:selected').val()>0)){
	    var funcionExito= function(resp){
                  if(resp.length>0){
                     $('#contenido').html(resp)
                     alert('Datos guardados con éxito');
                     ocultar_error_layer() 
                  }else{
                     alert('El curso ya se encuentra registrado para el alumno');
                  }
	    }
	   $.post(rutaAjax,params,funcionExito);	
    }else{
	    alert("Completa los campos")
    }
}

function generar_pdf_insc(Id_alum,Id_ofe_alum){
    window.open("formato_pago.php?id="+Id_alum+"&id_ofe="+Id_ofe_alum);
    //function para crear pdf de inscripcion
    ocultar_error_layer();
}


function save_alum(){
    var params= new Object();	
        params.action="save_alum";
        params.matricula=$('#matricula').val()
        params.referencia_pago=$('#referencia_pago').val()
        params.Id_alumn=$('#Id_alumn').val()
        params.Id_dir=$('#Id_dir').val()
        params.nombre_alum=$('#nombre_alum').val()
        params.apellidoP_alum=$('#apellidoP_alum').val()
        params.apellidoM_alum=$('#apellidoM_alum').val()
        params.edad_alum=$('#edad_alum').val()
        params.sexo=$('#sexo option:selected').val()
        params.fechaN_alum=$('#fechaN_alum').val()
        params.email_alum=$('#email_alum').val()
        params.tel_alum=$('#tel_alum').val()
        params.cel_alum=$('#cel_alum').val()
        params.curp_alum=$('#curp_alum').val()
        params.calle_alum=$('#calle_alum').val()
        params.numInt_alum=$('#numInt_alum').val()
        params.numExt_alum=$('#numExt_alum').val()
        params.colonia_alum=$('#colonia_alum').val()
        params.cp_alum=$('#cp_alum').val()
        params.ciudad_alum=$('#ciudad_alum').val()
        params.estado_alum=$('#estado_alum').val()
        params.nombre_tutor=$('#nombre_tutor').val()
        params.email_tutor=$('#email_tutor').val()
        params.tel_tutor=$('#tel_tutor').val()
        params.medio_alumn=$('#medio_alumn option:selected').val()
        params.priodidad=$('#priodidad option:selected').val()
        params.escula_pro=$('#escula_pro').val()
        params.comentarios=$('#comentarios').val()
        params.escolaridad=$('#escolaridad option:selected').val()
        params.matriculaSep=$('#matriculaSep').val()
        params.expedienteSep=$('#expedienteSep').val()
        
        params.Id_contacto=$('#Id_contacto').val()
        params.nombre_contacto=$('#nombre_contacto').val()
        params.parentesco_contacto=$('#parentesco_contacto').val()
        params.direccion_contacto=$('#direccion_contacto').val()
        params.tel_contacto=$('#tel_contacto').val()
        params.telOfic_contacto=$('#telOfic_contacto').val()
        params.celular_contacto=$('#celular_contacto').val()
        params.email_contacto=$('#email_contacto').val()
        params.tipo_sangre=$('#tipo_sangre').val()
        params.alergias_contacto=$('#alergias_contacto').val()
        params.cronicas_contacto=$('#cronicas_contacto').val()
        params.preinscripciones_medicas_contacto=$('#preinscripciones_medicas_contacto').val()
        
        var ban=1;
        //Esto es para migrar un alumno des sistema pasado
        if(params.Id_alumn.length==0){
             params.ofertas=new Array();
            //Ofertas del interesado
            $('#list_ofertas ul').each(function(){
                    var ofe= new Object();
                        ofe.Id_oferta=$(this).find('.oferta option:selected').val()
                        ofe.Id_esp=$(this).find('.curso option:selected').val()
                        ofe.Id_ori=$(this).find('.orientacion option:selected').val()
                        ofe.Id_grado=$(this).find('.grado option:selected').val()
                        ofe.Id_ciclo=$(this).find('.ciclo option:selected').val()
                        ofe.opcion=$(this).find('.opcionPago option:selected').val()
                        ofe.Id_ofe_alum=$(this).attr('Id_ofe_alum')
                        ofe.Id_ciclo_alum=$(this).attr('Id_ciclo_alum')
                        ofe.Id_pago_ciclo=$(this).attr('Id_pago_ciclo')

                        if($(this).find('.oferta option:selected').val()>0 
                                && $(this).find('.curso option:selected').val()>0 
                                && $(this).find('.grado option:selected').val()>0 
                                && $(this).find('.ciclo option:selected').val()>0 
                                && $(this).find('.opcionPago option:selected').val()>0){
                                params.ofertas.push(ofe)  
                        }else{
                            ban=0;
                        }
             });
         }
        params.tutores=new Array();
        $('#list_tutores ul').each(function(){
            var tutor= new Object();
                tutor.id=$(this).attr('id-tutor');
                tutor.id_dir=$(this).attr('id-dir');
                tutor.parantesco=$(this).find('.parentesco-tutor').val();
                tutor.apellidoP=$(this).find('.apellidoP-tutor').val();
                tutor.apellidoM=$(this).find('.apellidoM-tutor').val();
                tutor.nombre=$(this).find('.nombre-tutor').val();
                tutor.tel=$(this).find('.tel-tutor').val();
                tutor.cel=$(this).find('.cel-tutor').val();
                tutor.emal=$(this).find('.email-tutor').val();
                tutor.calle=$(this).find('.calle-tutor').val();
                tutor.colonia=$(this).find('.colonia-tutor').val();
                tutor.ciudad=$(this).find('.ciudad-tutor').val();
                tutor.estado=$(this).find('.estado-tutor').val();
                tutor.numExt=$(this).find('.numext-tutor').val();
                tutor.numInt=$(this).find('.numint-tutor').val();
                tutor.cp=$(this).find('.cp-tutor').val();
                params.tutores.push(tutor)  
        });
        
    if($('#nombre_alum').val().length>0 && $('#apellidoP_alum').val().length>0  && $('#apellidoM_alum').val().length>0 && $('#priodidad option:selected').val()>0 && ban==1 && $('#calle_alum').val().length>0 && $('#numExt_alum').val().length>0 &&$('#ciudad_alum').val().length>0){
        $.post(rutaAjax,params,function(resp){
            window.location="alumno.php?id="+resp
        });
    }else{
         var mns='Completa los campos faltantes'
         alert(mns);
    }
}


function save_documentos_alumno(Id_ofes,Id_ofe_alum,Obj){
    var params= new Object();	
        params.action="save_documentos_alumno"
        params.Id_alumn=$('#Id_alumn').val()
        params.Id_ofe=Id_ofes
        params.Id_ofe_alum=Id_ofe_alum
        params.files= new Array()
        $(Obj).closest('.list_oferta_alumno').find("table tbody tr").each(function(){
             var file= new Object()
                 file.Id_file=$(this).attr('id-file')
                 file.original_file=0
                 if($(this).find('.file_original').is(':checked')){
                     file.original_file=1
                 }
                 
                 file.copy_file=0
                 if($(this).find('.copi_file').is(':checked')){
                     file.copy_file=1
                 }
                 if($(this).find('.file_original').is(':checked') || $(this).find('.copi_file').is(':checked')){
                    params.files.push(file)
                 }
        })
    var funcionExito= function(resp){
        ocultar_error_layer()
    }
    $.post(rutaAjax,params,funcionExito);	
}





function update_oferta(Obj){
    var params= new Object();	
        params.action="update_oferta"
        params.Id_plantel=$(Obj).closest('.form').find('.plantel option:selected').val()
    var funcionExito= function(resp){
         $(Obj).closest('.form').find('.oferta').html(resp)
         $(Obj).closest('.form').find('.curso').html('')
         $(Obj).closest('.form').find('.box_orientacion').html('')
         $(Obj).closest('.form').find('.grado').html('')
    }
    $.post(rutaAjax,params,funcionExito);
}

function update_curso(Obj){
    var params= new Object();	
        params.action="update_curso"
        params.Id_oferta=$(Obj).closest('.form').find('.oferta option:selected').val()
    var funcionExito= function(resp){
         $(Obj).closest('.form').find('.curso').html(resp)
         $(Obj).closest('.form').find('.box_orientacion').html('')
         $(Obj).closest('.form').find('.grado').html('')
    }
    $.post(rutaAjax,params,funcionExito);
}

function update_orientacion(Obj){
    var params= new Object();	
        params.action="update_orientacion"
        params.Id_esp=$(Obj).closest('.form').find('.curso option:selected').val()
    var funcionExito= function(resp){
         $(Obj).closest('.form').find('.box_orientacion').html(resp)
          update_grado(Obj);
    }
    $.post(rutaAjax,params,funcionExito);
}



function update_grado(Obj){
    var params= new Object();	
        params.action="update_grado"
        params.Id_esp=$(Obj).closest('.form').find('.curso option:selected').val()
    var funcionExito= function(resp){
         $(Obj).closest('.form').find('.grado').html(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}

function add_oferta(){
    var params= new Object();	
        params.action="add_oferta"
    var funcionExito= function(resp){
         $('#list_ofertas').append(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}


//Funciones para el div box_curso
function update_oferta_box_curso(){
    var params= new Object();	
        params.action="update_oferta"
        params.Id_plantel=$('#plantel option:selected').val()
    var funcionExito= function(resp){
        
         $('#oferta').html(resp)
         $('#curso').html('')
         $('#box_orientacion').html('')
         $('#grado').html('')
    }
    $.post(rutaAjax,params,funcionExito);
}

function update_curso_box_curso(){
    var params= new Object();	
        params.action="update_curso"
        params.Id_oferta=$('#oferta option:selected').val()
    var funcionExito= function(resp){
         $('#curso').html(resp)
         $('#box_orientacion').html('')
         $('#grado').html('')
    }
    $.post(rutaAjax,params,funcionExito);
}


function update_orientacion_box_curso(){
    var params= new Object();	
        params.action="update_orientacion_box_curso"
        params.Id_esp=$('#curso option:selected').val()
    var funcionExito= function(resp){
         $('#box_orientacion').html(resp)
          update_grado_box_curso();
    }
    $.post(rutaAjax,params,funcionExito);
}

function update_grado_box_curso(){
    var params= new Object();	
        params.action="update_grado"
        params.Id_esp=$('#curso option:selected').val()
    var funcionExito= function(resp){
         $('#grado').html(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}

function inscribir(Id_ofe_alum){
    if(confirm("Desea incribir al alumno")){
        var params= new Object();	
            params.action="inscribir"
            params.Id_int=$('#Id_alumn').val()
            params.Id_ofe_alum=Id_ofe_alum
        var funcionExito= function(resp){
            $('#contenido').html(resp)
            alert('Datos guardados con éxito')

        }
        $.post(rutaAjax,params,funcionExito);
    }
}

function mostrar_box_baja(Id_ofe_alum){
    var params= new Object();	
        params.action="mostrar_box_baja"
        params.Id_ofe_alum=Id_ofe_alum
        params.Id_int=$('#Id_alumn').val()
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}


function save_motivo(Id_ofe_alum){
    if(confirm("¿Desea eliminar la oferta del alumno?")){
        var params= new Object();	
            params.action="save_motivo"
            params.Id_ofe_alum=Id_ofe_alum
            params.Id_int=$('#Id_alumn').val()
            params.Id_mot=$('#motivos_bajas option:selected').val()
            params.comentario=$('#comentario').val()
            params.Id_alum=$('#Id_alumn').val()
       if($('#motivos_bajas option:selected').val()>0){
        var funcionExito= function(resp){
            $('#contenido').html(resp)
            ocultar_error_layer() 
        }
        $.post(rutaAjax,params,funcionExito);
       }else{
           alert("Selecciona el motivo de baja")
       }
    }
}



function mostrar_cambiar_plan(Id_ofe_alum){
    var params= new Object();	
        params.action="mostrar_cambiar_plan"
        params.Id_alum=$('#Id_alumn').val()
        params.Id_ofe_alum=Id_ofe_alum
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}


function cambiar_plan_pago(Id_ofe_alum){
        var params= new Object();	
            params.action="cambiar_plan_pago"
            params.Id_alum=$('#Id_alumn').val()
            params.Id_ofe_alum=Id_ofe_alum
            params.opcionPago=$('#opcionPago option:selected').val()
        if($('#opcionPago option:selected').val()>0){
            if(confirm("¿Desea cambiar el plan de pago?\n\nEsta opción solo debera ser utiliza al inicio del ciclo\nTome en cuenta que se eliminara para el ciclo actual del alumno lo siguiente:\n\n-Materias asignadas\n-Cargos de mensualidades\n-Grupos en los que se encuentra\n-Asistencias\n-Calificaciones")){
                var funcionExito= function(resp){
                     $('#contenido').html(resp)
                     ocultar_error_layer() 
                }
            }
            $.post(rutaAjax,params,funcionExito);
        }else{
            alert("Completa los campos faltantes")
        }
}



function mostrar_botones(Obj){
   if($(Obj).next().is(":hidden")) {
	   $(Obj).next().slideDown("fast");
           $(Obj).html('Ocultar')
   } else {
	   $(Obj).next().slideUp("fast");
           $(Obj).html('Mostrar')
   }

}



function mostrar_cambiar_oferta(Id_ofe_alum){
    var params= new Object();	
        params.action="mostrar_cambiar_oferta"
        params.Id_alum=$('#Id_alumn').val()
        params.Id_ofe_alum=Id_ofe_alum
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}





function cambiar_oferta(Id_ofe_alum){
    var params= new Object();	
        params.action="cambiar_oferta"
        params.Id_ofe_alum=Id_ofe_alum
        params.Id_alumn=$('#Id_alumn').val()
        params.Id_oferta=$('#oferta option:selected').val()
        params.Id_esp=$('#curso option:selected').val()
        params.Id_ori=$('#orientacion option:selected').val()
        params.Id_grado=$('#grado option:selected').val()
        params.Id_ciclo=$('#ciclo option:selected').val()
        params.opcion=$('#opcionPago option:selected').val()
        params.Turno=$('#turno option:selected').val()
        params.Id_curso_esp=$('#cursos-especialidad option:selected').val()
        
    if($('#oferta option:selected').val()>0 && $('#curso option:selected').val()>0 && $('#grado option:selected').val()>0 && $('#opcionPago option:selected').val()>0 && ($('#ciclo option:selected').val()>0 || $('#cursos-especialidad option:selected').val()>0)){
           if(confirm("¿Esta seguro de cambiar la oferta?")){
	    var funcionExito= function(resp){
                 $('#contenido').html(resp)
                 alert('Datos guardados con éxito');
                 ocultar_error_layer() 
	    }
	   $.post(rutaAjax,params,funcionExito);	
           }
    }else{
	    alert("Completa los campos")
    }
}



function add_ofe_alum_interes(){
    var params= new Object();	
        params.action="add_ofe_alum_interes"
        params.Id_alumn=$('#Id_alumn').val()
        params.Id_oferta=$('#oferta option:selected').val()
        params.Id_esp=$('#curso option:selected').val()
        params.Id_ori=$('#orientacion option:selected').val()
        params.Id_ciclo=$('#ciclo option:selected').val()
        params.Turno=$('#turno option:selected').val()
    if($('#oferta option:selected').val()>0 && $('#curso option:selected').val()>0 && $('#ciclo option:selected').val()>0){
	    var funcionExito= function(resp){
                  if(resp.length>0){
                     $('#contenido').html(resp)
                     alert('Datos guardados con éxito');
                     ocultar_error_layer() 
                  }else{
                     alert('El curso ya se encuentra registrado para el alumno');
                  }
	    }
	   $.post(rutaAjax,params,funcionExito);	
    }else{
	    alert("Completa los campos")
    }
}

function getTipociclo (){
    var params= new Object();	
        params.action="getTipoCiclo"
        params.Id_ofe=$('#oferta option:selected').val()
    var funcionExito= function(resp){
         $('.box-emergente-form').html(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}

function getTipoCicloCambioOferta (Id_ofe_alum){
    var params= new Object();	
        params.action="getTipoCicloCambioOferta"
        params.Id_ofe=$('#oferta option:selected').val()
        params.Id_ofe_alum=Id_ofe_alum
    var funcionExito= function(resp){
         $('.box-emergente-form').html(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}


function getCursosEspecialidad(){
    var params= new Object();	
        params.action="getCursosEspecialidad"
        params.Id_esp=$('#curso option:selected').val()
        params.Turno=$('#turno option:selected').val()
    $.post(rutaAjax,params,function(resp){
        $('#cursos-especialidad').html(resp);
    });
}

function delete_tutor(id_tutor,id_dir){
    if(confirm("¿Está seguro de eliminar al tutor?")){
        var params= new Object();	
            params.action="delete_tutor";
            params.id_tutor=id_tutor;
            params.id_dir=id_dir;
            params.id_alumn=$('#Id_alumn').val();
        $.post(rutaAjax,params,function(resp){
            $('#contenido').html(resp);
            ocultar_error_layer();
        });
    }
}

function box_tutor(){
    var params= new Object();	
        params.action="box_tutor";
        params.Id_alum=$('#Id_alumn').val();
    $.post(rutaAjax,params,function(resp){
        mostrar_error_layer(resp);
    });
}

function add_tutor(){
    var params= new Object();	
        params.action="add_tutor";
        params.Id_alum=$('#Id_alumn').val();
        params.parantesco=$('#parentesco-tutor').val();
        params.apellidoP=$('#apellidoP-tutor').val();
        params.apellidoM=$('#apellidoM-tutor').val();
        params.nombre=$('#nombre-tutor').val();
        params.tel=$('#tel-tutor').val();
        params.cel=$('#cel-tutor').val();
        params.emal=$('#email-tutor').val();
        params.calle=$('#calle-tutor').val();
        params.colonia=$('#colonia-tutor').val();
        params.ciudad=$('#ciudad-tutor').val();
        params.estado=$('#estado-tutor').val();
        params.numExt=$('#numext-tutor').val();
        params.numInt=$('#numint-tutor').val();
        params.cp=$('#cp-tutor').val();
    $.post(rutaAjax,params,function(resp){
            $('#contenido').html(resp);
            alert('Datos guardados con éxito');
            ocultar_error_layer() 
    });
}

