function mostrarSelector(text){
    $('#errorLayerSelector').html(text)
    $('#errorLayerSelector').css('visibility','visible')
    $('#errorLayerSelector').animate({
      opacity: 1
    }, 300);
}

function ocultarSelector(){
    $('#errorLayerSelector').animate({
      opacity: 0
    }, 300,function(){
       $('#errorLayerSelector').css('visibility','hidden')
       $('#errorLayerSelector').html('')
    });
}


function mostrar_box_selector_img(){
    var params= new Object();	
        params.action="mostrar_box_selector_img"
    var funcionExito= function(resp){
	   mostrarSelector(resp)
    }
    $.post("selectores_aj.php",params,funcionExito);	
}
