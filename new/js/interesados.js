 
$(window).ready(function(){
    $(".boxfil").draggable();
    if($('#files').length) {
       document.getElementById('files').addEventListener('change', handleFileSelect, false);
    }
})



function mostrar(id){
        window.open("interesado.php?id="+id, '_blank');
}


function buscarInt(){
    if($('#buscar').val().length>=4){
       var params= new Object()
           params.action="buscarInt"
           params.buscar="%"
           if($.trim($('#buscar').val())){
           params.buscar=$('#buscar').val()
           }
       var funcionExito= function(resp){
          $('.table tbody').html(resp)
       }
       $.post('interesados_aj.php',params,funcionExito);
    }
}


function delete_int(Id_int){
   if(confirm("¿Está seguro de eliminar el interesado?")){
	    var params= new Object();	
	        params.action="delete_int"
	        params.Id_int=Id_int
	    var funcionExito= function(resp){
	         $('.table tbody').html(resp)
	         ocultar_error_layer()
	    }
	    $.post("interesados_aj.php",params,funcionExito);
    }
}



function filtro(Obj){
    $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_usu=$('.buscarFiltro').attr('id-data')
       params.Id_ofe=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
       params.Id_medio=$('#medio_entero option:selected').val()
       params.Prioridad=$('#priodidad option:selected').val()
       params.Fecha_ini=$('#fecha_ini').val()
       params.Fecha_fin=$('#fecha_fin').val()
       params.seguimiento=0
       if($('#todos').is(":checked")){
           params.seguimiento=0
       }else if($('#conseguimiento').is(":checked")){
          params.seguimiento=1 
       }else if($('#SinSeguimiento').is(":checked")){
          params.seguimiento=2
       }
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post('interesados_aj.php',params,funcionExito);	
}


function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Id_usu=$('.buscarFiltro').attr('id-data')
       params.Id_ofe=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
       params.Id_medio=$('#medio_entero option:selected').val()
       params.Prioridad=$('#priodidad option:selected').val()
       params.Fecha_ini=$('#fecha_ini').val()
       params.Fecha_fin=$('#fecha_fin').val()
   var query=$.param( params );
   window.open("interesados_aj.php?"+query, '_blank');
}



function handleFileSelect(evt) {
    //Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // Great success! All the File APIs are supported.

        // FileList object
        var files = evt.target.files;

        var count = 0;
        var bin, name, type, size

        //Funcion loadStartImg
        var loadStartImg = function() {
            //alert("inicia") 
        }

        //Funcion loadEndImg
        var loadEndImg = function(e) {
            var xhr
            if (window.XMLHttpRequest) {
                xhr = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
            //var bin = reader.result;

            // progress bar loadend
            var eventSource = xhr.upload || xhr;
            eventSource.addEventListener("progress", function(e) {
                var pc = parseInt((e.loaded / e.total * 100));
                //$('#mascara_img span').html(pc+'%') 
                var mns = 'Cargando ...' + pc + '%'
                //alerta(mns);
                if (pc == 100) {
                    //ocultar_error_layer()
                }
            }, false);

            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && xhr.status == 200) {
                   $('#list_archivos').html(xhr.responseText)
                   $('#files').val('')
                }
            }
            xhr.open('POST', 'index_aj.php?action=uploadAttachment', true);
            var boundary = 'xxxxxxxxx';
            var body = '--' + boundary + "\r\n";
            body += "Content-Disposition: form-data; name='upload'; filename='" + name + "'\r\n";
            body += "Content-Type: application/octet-stream\r\n\r\n";
            body += bin + "\r\n";
            body += '--' + boundary + '--';
            xhr.setRequestHeader('content-type', 'multipart/form-data; boundary=' + boundary);
            // Firefox 3.6 provides a feature sendAsBinary ()
            if (xhr.sendAsBinary != null) {
                xhr.sendAsBinary(body);
                // Chrome 7 sends data but you must use the base64_decode on the PHP side
            } else {
                xhr.open('POST', 'index_aj.php?action=uploadAttachment&base64=ok&FileName=' + name + '&TypeFile=' + type, true);
                xhr.setRequestHeader('UP-FILENAME', name);
                xhr.setRequestHeader('UP-SIZE', size);
                xhr.setRequestHeader('UP-TYPE', type);
                xhr.send(window.btoa(bin));
            }
            if (status) {
                //document.getElementById(status).innerHTML = '';
            }
        }

        //Funcion loadErrorImg
        var loadErrorImg = function(evt) {
            switch (evt.target.error.code) {
                case evt.target.error.NOT_FOUND_ERR:
                    alert('File Not Found!');
                    break;
                case evt.target.error.NOT_READABLE_ERR:
                    alert('File is not readable');
                    break;
                case evt.target.error.ABORT_ERR:
                    break; // noop
                default:
                    alert('An error occurred reading this file.');
            }
            ;
        }

        //Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {
            /*
                                    // Only process image files.
                                    if (!f.type.match('image.*')) {
                                        alert(1)
                                        continue;
                                    }
                            */

            var reader = new FileReader();
            var preview = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail binari.
                    bin = e.target.result
                    name = theFile.name
                    type = theFile.type
                    size = theFile.size
                };
            })(f);

            preview.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail.
                    /*
                     var li = document.createElement('li');
                     $(li).attr('id','img_'+count)
                     li.innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/><div id="mascara_img"><span></span></div>'].join('');
                     $('#visor_subir_img').append(li)
                     $('#visor_subir_img li img').css({'width':'85px','height':'85px'});
                     $('#ruta_img').val($('#files').val())
                     count++;
                     */
                };
            })(f);

            //Read in the image file as a binary string.
            reader.readAsBinaryString(f);
            //Read in the image file as a data URL.
            preview.readAsDataURL(f);

            // Firefox 3.6, WebKit
            if (reader.addEventListener) {
                reader.addEventListener('loadend', loadEndImg, false);
                reader.addEventListener('loadstart', loadStartImg, false);
                if (status != null) {
                    reader.addEventListener('error', loadErrorImg, false);
                }
                // Chrome 7
            } else {
                reader.onloadend = loadEndImg;
                reader.onloadend = loadStartImg;
                if (status != null) {
                    reader.onerror = loadErrorImg;
                }
            }
        }

    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
}



function send_email(Obj){
 var params= new Object();	
    params.action="send_email"
    params.asunto=$('#asunto').val()
    params.mensaje=editor.getData()
    params.alumnos= new Array()
    $('.table tbody tr td input[type="checkbox"]:checked ').each(function(){
           params.alumnos.push($(this).closest('tr').attr('id_alum'))
    })
    if(params.alumnos.length>0){
        $(Obj).html('Enviando..')
        $(Obj).removeAttr('onclick')
        if($('#asunto').val().length>0 && params.mensaje.length>0){
            var funcionExito= function(resp){
               ocultar_error_layer()
               editor.destroy();
               deleteAttachments()
               $(Obj).html('Enviar')
               $(Obj).attr('onclick','send_email(this)')
               
            }
            $.post("interesados_aj.php",params,funcionExito);
         }else{
           alert("Completa los campos faltantes")  
         }
     }else{
         alert("Selecciona por lo menos un interesado")
     }
}


function mostrar_box_comentario(Id_int){
    var params= new Object();	
        params.action="mostrar_box_comentario"
        params.Id_int=Id_int
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("interesados_aj.php",params,funcionExito);
}


function capturar_seguimiento(Id_int){
    var params= new Object();	
        params.action="capturar_seguimiento"
        params.Id_int=Id_int
        params.coment=$('#coment').val()
        params.contactar=$('#contactar').val()
        if($('#tipo-seg').length>0){
           params.tipo=$('#tipo-seg option:selected').val()
        }
        params.prioridad=$('#prioridad option:selected').val()
    if($('#coment').val().length>0 && $('#prioridad option:selected').val()>0){
    var funcionExito= function(resp){
         ocultar_error_layer();
        buscarNotificaciones();
        $('tr[id_alum="'+Id_int+'"]').removeAttr('class')
        $('tr[id_alum="'+Id_int+'"]').attr('class',resp.color)
        $('tr[id_alum="'+Id_int+'"] .box_seguimiento').html(resp.comentario)
        $('tr[id_alum="'+Id_int+'"] .contactar').html(resp.contactar)
        $('tr[id_alum="'+Id_int+'"] .prioridad').html(resp.Prioridad)
        
    }
    $.post("interesados_aj.php",params,funcionExito,"json");
    }else{
	    alert("Completa los datos")
    }
}

function mostrar_box_masivo(){
    var params= new Object();	
        params.action="mostrar_box_masivo"
        params.alumnos= new Array()
        $('.table tbody tr ').each(function(){
           if($(this).find('input[type="checkbox"]').is(":checked")){
               params.alumnos.push($(this).attr('id_alum'))
           }
        })
        if(params.alumnos.length>0){
            var funcionExito= function(resp){
                mostrar_error_layer(resp);
            }
            $.post("interesados_aj.php",params,funcionExito);
        }else{
            alert("Selecciona por lo menos un interesado")
        }
}

function capturar_seguimiento_masivo(Obj){
    var params= new Object();	
        params.action="capturar_seguimiento_masivo"
        params.coment=$('#coment').val()
        params.prioridad=$('#prioridad option:selected').val()
        params.tipo=$('#tipo-seg option:selected').val()
        params.alumnos= new Array()
        $('.table tbody tr td input[type="checkbox"]:checked').each(function(){
               params.alumnos.push($(this).closest('tr').attr('id_alum'))
        })
    if($('#coment').val().length>0 && $('#prioridad option:selected').val()>0){
        $(Obj).removeAttr('onclick')
        var funcionExito= function(resp){
            $(Obj).attr('onclick','capturar_seguimiento_masivo(this)')
            $('.table tbody').html(resp)
            ocultar_error_layer()
        }
        $.post("interesados_aj.php",params,funcionExito);
    }else{
	    alert("Completa los datos")
    }
}

