var rutaAjax = "ajax/reporte_cambiar_ciclo_aj.php"
$(window).ready(function(){
    $(".boxfil").draggable();
    $(".box-ciclos").draggable();
})




function mostrar_box(){ 
    $('.box-ciclos').css('visibility','visible')
	$('.box-ciclos').animate({
		opacity: 1
	},400,function(){

    })
}

function ocultar_box(){ 
	$('.box-ciclos').animate({
		opacity: 0
	},400,function(){
	    $('.box-ciclos').css('visibility','hidden')
	})
}



function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_docen=$('.buscarFiltro').attr('id-data')
       params.Id_ofe=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
       params.Id_ciclo=$('#cicloFiltro option:selected').val()
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}

var Id_ciclo_actual=0
var Id_ciclo_nuevo=0
function confirmarCambio(){
    if(confirm("¿Está seguro de cambiar de nivel los alumnos?")){
        if($('#Id_ciclo_actual option:selected').val()>0 && $('#Id_ciclo_nuevo option:selected').val()>0){
            Id_ciclo_actual=$('#Id_ciclo_actual option:selected').val()
            Id_ciclo_nuevo=$('#Id_ciclo_nuevo option:selected').val()
            cambiar_ciclo()
        }else{
            alert("Selecciona los ciclos")
        }
    }
}





var row=0;
function cambiar_ciclo(){
    if(row<$(".table tbody tr").length){
            doMensajeRow() 
    }else{
         row=-1
          alert("Cambio terminado!");  
    }
}

function doMensajeRow(){
	row=row+1;
	if($(".table tbody tr:nth-child("+((row))+")").attr("id_alum")>0 && $(".table tbody tr:nth-child("+((row))+")").find('input[type="checkbox"]').is(':checked')){
		var params= new Object()
			params.action="cambiar_ciclo"
			params.Id_alum=$(".table tbody tr:nth-child("+((row))+")").attr("id_alum")
			params.Id_ofe_alum=$(".table tbody tr:nth-child("+((row))+")").attr("id_ofe_alum")
                        params.Id_ciclo_actual=Id_ciclo_actual
                        params.Id_ciclo_nuevo=Id_ciclo_nuevo
			$.post(rutaAjax,params,function(respuesta){
				if(respuesta.length>0){
                                        $('#alumnosCAmbiados').html('Alumnos cambiados: '+row)
                                        $(".table tbody tr:nth-child("+((row))+") td:last-child").css('background','lightgreen')
                                        $(".table tbody tr:nth-child("+((row))+") td:last-child").css('color','black')
                                        $(".table tbody tr:nth-child("+((row))+") td").find('.ciclo-actual').html(respuesta)
					$(".table tbody tr:nth-child("+((row))+") td:last-child span").html('Listo')
				}
				cambiar_ciclo()
			})
	}else{
	    cambiar_ciclo()
	}
}


function marcar_alumnos(){
    if($('#all_alumns').is(":checked")){
      $('.table tbody tr  input[type="checkbox"]').prop('checked',true);
    }else{
      $('.table tbody tr  input[type="checkbox"]').prop('checked',false);
    }
}