function mostrar(id) {
    window.location = "oferta.php?id=" + id
}


function box_update_oferta(Id_oferta) {
    var params = new Object();
    params.action = "box_update_oferta"
    params.Id_oferta = Id_oferta
    var funcionExito = function (resp) {
        mostrar_error_layer(resp);
    }
    $.post("ajax/ofertas_academicas_aj.php", params, funcionExito);
}

function save_oferta(Id_oferta) {
    var params = new Object();
        params.action = "save_oferta"
        params.Id_oferta = Id_oferta
        params.nombre = $('#nombre').val()
        params.montoCargoMateriaAcreditada = $('#montoCargoMateriaAcreditada').val()
        params.valor = $('#valor').val()
        params.tipo_recargo = ""
        if ($('#tipo-recargo option:selected').val() == 1) {
            params.tipo_recargo = "cargofijo";
        } else if ($('#tipo-recargo option:selected').val() == 2) {
            params.tipo_recargo = "cargoporcentual";
        }
        params.tipo_plan = $('#tipo-plan option:selected').val()

    if ($('#nombre').val().length > 0 && $('#tipo-plan option:selected').val()>0) {
        var funcionExito = function (resp) {
            $('.table tbody').html(resp)
            ocultar_error_layer(resp);
        }
        $.post("ajax/ofertas_academicas_aj.php", params, funcionExito);
    } else {
        alert("Completa los campos faltantes")
    }
}




function mostrar_box_busqueda() {
    var params = new Object();
    params.action = "mostrar_box_busqueda"
    var funcionExito = function (resp) {
        mostrar_error_layer(resp);
    }
    $.post("ajax/ofertas_academicas_aj.php", params, funcionExito);
}



function buscarOferta() {
    var params = new Object()
    params.action = "buscarOferta"
    params.buscar = "%"
    if ($.trim($('#buscar').val())) {
        params.buscar = $('#buscar').val()
    }
    var funcionExito = function (resp) {
        $('.table tbody').html(resp)
    }
    $.post('ajax/ofertas_academicas_aj.php', params, funcionExito);
}

function alerta(text) {
    var div = '<div id="box_emergente"><h1>' + text + '</h1><p><button class="delete">Aceptar</button><button onclick="ocultar_error_layer()">Cancelar</button></p></div>'
    mostrar_error_layer(div);
}

function delete_ofe(Id_ofe) {
    if (confirm("¿Está seguro de eliminar el nivel académico?")) {
        var params = new Object();
        params.action = "delete_ofe"
        params.Id_ofe = Id_ofe
        var funcionExito = function (resp) {

            $('.table tbody').html(resp)
            ocultar_error_layer()
        }
        $.post("ajax/ofertas_academicas_aj.php", params, funcionExito);
    }
}