function save_subnivel(Id_ori) {
    var params = new Object();
    params.action = "save_subnivel"
    params.Id_oferta = $('#Id_oferta').val()
    params.nombre_subnivel = $('#nombre_subnivel').val()
    params.Id_esp = $('#nivel option:selected').val()
    params.Id_ori = Id_ori
    if ($('#nombre_subnivel').val().length > 0 && $('#nivel option:selected').val() > 0) {
        var funcionExito = function (resp) {
            $('#column_one').html(resp);
            ocultar_error_layer()
        }
        $.post("ajax/oferta_aj.php", params, funcionExito);
    } else {
        alerta("Completa los datos requeridos")
    }
}

function alerta_dos(text) {
    var div = '<div id="box_emergente"><h1>' + text + '</h1><p><button class="delete">Aceptar</button><button onclick="ocultar_error_layer()">Cancelar</button></p></div>'
    mostrar_error_layer(div);
}
function delete_esp(Id_esp) {
    if (confirm("¿Está seguro de eliminar el nivel?")) {
        var params = new Object();
        params.action = "delete_esp"
        params.Id_esp = Id_esp
        params.Id_oferta = $('#Id_oferta').val()
        var funcionExito = function (resp) {
            $('#column_one').html(resp)
            ocultar_error_layer()
        }
        $.post("ajax/oferta_aj.php", params, funcionExito);
    }
}



function delete_sub(Id_ori) {
    if (confirm("¿Está seguro de eliminar el subnivel?")) {
        var params = new Object();
        params.action = "delete_sub"
        params.Id_ori = Id_ori
        params.Id_oferta = $('#Id_oferta').val()
        var funcionExito = function (resp) {
            $('#column_one').html(resp)
            ocultar_error_layer()
        }
        $.post("ajax/oferta_aj.php", params, funcionExito);
    }
}





function new_subnivel(Id_ori) {
    var params = new Object();
    params.action = "new_subnivel"
    params.Id_ori = Id_ori
    params.Id_oferta = $('#Id_oferta').val()
    var funcionExito = function (resp) {
        mostrar_error_layer(resp);
    }
    $.post("ajax/oferta_aj.php", params, funcionExito);
}



function get_especialdades() {
    var params = new Object();
    params.action = "get_especialdades"
    params.Id_oferta = $('#oferta option:selected').val()
    if ($('#oferta option:selected').val() > 0) {
        var funcionExito = function (resp) {
            //alert(resp)
            $('#nivel').html(resp)
        }
        $.post("ajax/oferta_aj.php", params, funcionExito);
    }
}



