var rutaAjax = "ajax/categorias_evaluacion_aj.php";

function mostrarCategoria(Id){
    var params= new Object();	
        params.action="mostrarCategoria";
        params.Id_cat=Id;
    $.post(rutaAjax,params,function(resp){
          mostrar_error_layer(resp);
    });
}

function mostrarCampo(Id){
    var params= new Object();	
        params.action="mostrarCampo";
        params.Id_campo=Id;
    $.post(rutaAjax,params,function(resp){
          mostrar_error_layer(resp);
    });
}



function updatePage(){
    var params= new Object();	
        params.action="updatePage";
    $.post(rutaAjax,params,function(resp){
          $('#tabla').html(resp);
    });
}

function saveCategoria(Id){
    var params= new Object();	
        params.action="saveCategoria";
        params.Id_cat=Id;
        params.nombre=$('#nombre-cat').val();
    if($('#nombre-cat').val().length>0){
        $.post(rutaAjax,params,function(resp){
            alert("Datos guardados con éxito");
            updatePage();
            ocultar_error_layer();
        });
     }else{
         alert("Completa los campos")
     }
}

function saveCampo(Id){
    var params= new Object();	
        params.action="saveCampo";
        params.Id_campo=Id;
        params.nombre=$('#nombre').val();
        params.valor=$('#valor').val();
        params.tipocampo=$('#tipo-campo option:selected').val();
        params.id_cat=$('#select-categorias option:selected').val();
    if($('#nombre').val().length>0 && $('#valor').val().length>0 && $('#tipo-campo option:selected').val()>0 && $('#select-categorias option:selected').val()>0){
            $.post(rutaAjax,params,function(resp){
                alert("Datos guardados con éxito");
                updatePage();
                ocultar_error_layer();
            });
        }else{
         alert("Completa los campos");
     }
}




function eliminarCategoria(Id_cat){
    if(confirm("¿Está seguro de eliminar la categoría?")){
        var params= new Object();	
            params.action="eliminarCategoria";
            params.Id_cat=Id_cat;
        $.post(rutaAjax,params,function(){
            alert("Datos guardados con éxito");
            updatePage();
        });
    }
}

function eliminarCampo(Id_nivel){
    if(confirm("¿Está seguro de eliminar el nivel?")){
        var params= new Object();	
            params.action="eliminarCampo";
            params.Id_campo=Id_nivel;
        $.post(rutaAjax,params,function(){
            alert("Datos guardados con éxito");
            updatePage();
        });
    }
}
