var rutaAjax = "ajax/evaluacion_docente_aj.php";

function updateCampos(){
    var totalPuntos=0;
    $('.categorias-evaluacion').each(function(){
        var puntosCat=0;
        $(this).find('input').each(function(){

              if($(this).attr('type')=="checkbox"){
                  puntosCat+=0; 
                  if($(this).is(':checked')){
                     puntosCat+=($(this).attr('valor-maximo-campo')*1); 
                  }
              }else if($(this).attr('type')=="text"){
                  
                    puntosCat+=0;
                    if($(this).val()>0){
                        if($(this).val()<=($(this).attr('valor-maximo-campo')*1)){
                           puntosCat+=($(this).val()*1);
                        }else{
                            $(this).val('');
                            alert("Ingresa un rango de 1 a "+$(this).attr('valor-maximo-campo'));
                        }
                    }
              }
        });
        $(this).find('.puntos-cat-evaluacion').text(Math.round(puntosCat));
        totalPuntos+=Math.round(puntosCat);
    });
    $('#puntos_totales').text(totalPuntos);
    updatePuntosTotales();
}



function updatePuntosTotales(){
   var params= new Object()
       params.action="updatePuntosTotales";
       params.Id_docente=$('#Id_docente').val();
       params.puntosTotales=($('#puntos_totales').text()*1);
       params.Id_eva=$('#Id_eva').val();
       params.categorias= new Array();
        $('.categorias-evaluacion').each(function(){
            var cat= new Object();
                cat.id_cat=$(this).attr('id-cat-eva');
                cat.puntos=$(this).find('.puntos-cat-evaluacion').text();
                params.categorias.push(cat);
        });
    $.post(rutaAjax,params,function(resp){
        $('#ul-categorias').html(resp);
    });
}


function save_evaluacion(){
    var totalPuntos=0;
    var params= new Object()
        params.action="save_evaluacion";
        params.Id_docente=$('#Id_docente').val();
        params.Id_eva=$('#Id_eva').val();
        params.Comentarios=$('#comentarios').val();
        params.Id_ciclo=$('#Id_ciclo option:selected').val();
        params.CategoriasEvaluadas= new Array();
       $('.categorias-evaluacion').each(function(){
           var puntosCat=0;
           var cat=new Object();
               cat.id=$(this).attr('id-cat-eva');
               cat.inputs= new Array();
               $(this).find('input[type="checkbox"],input[type="text"]').each(function(){
                     var puntosInput=0;
                     var input= new Object();
                         input.id=$(this).attr('valor-id');
                     if($(this).attr('type')=="checkbox"){
                         puntosCat+=0; 
                         if($(this).is(':checked')){
                            puntosCat+=($(this).attr('valor-maximo-campo')*1); 
                            puntosInput=($(this).attr('valor-maximo-campo')*1);
                         }
                     }else if($(this).attr('type')=="text"){
                           puntosCat+=0;
                           if($(this).val()>0){
                               if($(this).val()<=($(this).attr('valor-maximo-campo')*1)){
                                  puntosCat+=($(this).val()*1);
                                  puntosInput=($(this).val()*1);
                               }
                           }
                     }
                     input.punto=puntosInput;
                     cat.inputs.push(input);
               });
           totalPuntos+=Math.round(puntosCat);
           params.CategoriasEvaluadas.push(cat);
       });
        params.puntosTotales=totalPuntos;
        params.NivelesObtenidos= new Array();
        $('.resultado_nivel_por_categoria').each(function(){
            var nivel=new Object();
                nivel.id=$(this).val();
                params.NivelesObtenidos.push(nivel);
        });
       if($('#Id_ciclo option:selected').val()>0){
           $.post(rutaAjax,params,function(resp){
                 if (resp.tipo == "new") {
                     window.location = "evaluacion_docente.php?id_doc=" +$('#Id_docente').val()+"&id="+resp.id
                 } else {
                     alert("Datos guardados con éxito")
                 }
           },"json");
       }else{
           alert("Selecciona un ciclo");
       }
}





