var rutaAjax = "ajax/planteles_aj.php"

function mostrar(id) {
    window.location = "plantel.php?id=" + id
}



function delete_plantel(Id_plantel) {
    if (confirm("¿Está seguro de eliminar el plantel?")) {
        var params = new Object();
        params.action = "delete_plantel"
        params.Id_plantel = Id_plantel
        var funcionExito = function(resp) {
            $('.table tbody').html(resp)
            ocultar_error_layer()
        }
        $.post(rutaAjax, params, funcionExito);
    }
}