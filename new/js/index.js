$(document).ready(function() {
  
  var animating = false,
      submitPhase1 = 1100,
      submitPhase2 = 400,
      logoutPhase1 = 800,
      $login = $(".login"),
      $app = $(".app");
  
  function ripple(elem, e) {
    $(".ripple").remove();
    var elTop = elem.offset().top,
        elLeft = elem.offset().left,
        x = e.pageX - elLeft,
        y = e.pageY - elTop;
    var $ripple = $("<div class='ripple'></div>");
    $ripple.css({top: y, left: x});
    elem.append($ripple);
  };
  
  $(document).on("click", ".login__submit", function(e) {
    if (animating) return;
    animating = true;
    var that = this;
    ripple($(that), e);
    $(that).addClass("processing");
    setTimeout(function() {
      $(that).addClass("success");
      setTimeout(function() {
        $app.show();
        $app.css("top");
        $app.addClass("active");
      }, submitPhase2 - 70);
      setTimeout(function() {
        // $login.hide();
        // $login.addClass("inactive");
        animating = false;
        $(that).removeClass("success processing");
      }, submitPhase2);
    }, submitPhase1);
  });
  
  $(document).on("click", ".app__logout", function(e) {
    if (animating) return;
    $(".ripple").remove();
    animating = true;
    var that = this;
    $(that).addClass("clicked");
    setTimeout(function() {
      $app.removeClass("active");
      $login.show();
      $login.css("top");
      $login.removeClass("inactive");
    }, logoutPhase1 - 120);
    setTimeout(function() {
      $app.hide();
      animating = false;
      $(that).removeClass("clicked");
    }, logoutPhase1);
  });
  
});


function login(){
    var params= new Object(); 
        params.action="login"
        params.email=$('#user').val()
        params.pass=$('#pass').val()
    var funcionExito= function(resp){
         if(resp==1){
           window.location="home.php"
         }else if(resp==2){
           alert('La dirección de email proporcionada no está; registrada en nuestro sistema,favor de verificar')
         }else if(resp==3){
           alert('La contraseña proporcionada es incorrecta')
         }
    }
    $.post("index_aj.php",params,funcionExito);
}

$(window).ready(function(){
  use_database()
})


$(window).keypress(function(e) {
    if(e.which == 13) {
        login()
    }
});


function use_database(){
        var params= new Object()
           params.action="use_database"
           params.test=0
        var url=$(location).attr('href')
            if(url.indexOf("test=x")!=-1){
                   params.test=1
            }
        var funcionExito=function(respuesta){

        }
        $.post("index_aj.php",params,funcionExito); 
}