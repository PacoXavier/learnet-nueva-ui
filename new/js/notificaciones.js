var rutaAjax = "ajax/notificaciones_aj.php"

function mostrar_box_notificaciones(Id_not){
    var params= new Object();	
        params.action="mostrar_box_notificaciones"
        params.Id_not=Id_not
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}

function mostrar_tipos_usu(){
    var params= new Object();	
        params.action="mostrar_tipos_usu"
        params.Tipo=$('#Tipo option:selected').val()
    var funcionExito= function(resp){
         $('#box-tipo_usu').html(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}



function delete_not(Id_not){
   if(confirm("¿Está seguro de eliminar la notificación?")){
	    var params= new Object();	
	        params.action="delete_not"
	        params.Id_not=Id_not
	    var funcionExito= function(resp){
	         $('.table tbody').html(resp)
	         ocultar_error_layer()
                 buscarNotificaciones()
	    }
	    $.post(rutaAjax,params,funcionExito);
    }
}

function add_notificacion(Id_not){
  var params= new Object()
       params.action="add_notificacion"
       params.Id_not=Id_not
       params.titulo=$('#titulo').val()
       params.fecha_ini=$('#fecha_ini').val()
       params.texto=$('#texto').val()
       if($('#oferta').length>0){
           params.Id_ofe=$('#oferta option:selected').val()
           params.Id_esp=$('#curso option:selected').val()
           params.Id_ori=$('#orientacion option:selected').val()
       }
       params.Tipo=$('#Tipo option:selected').val()
       params.Tipos_usuarios= new Array()
       var ban=1
       if(params.Tipo==1){
           $('.box-emergente-form input[type="checkbox"]').each(function(){
               if($(this).is(':checked')){
                   ban=1
                   var tipos= new Object()
                       tipos.Id_tipo=$(this).val()
                       params.Tipos_usuarios.push(tipos)
               }
           })
           if(params.Tipos_usuarios.length==0){
               ban=0
           }
       }

  if($('#titulo').val().length>0 && $('#Tipo option:selected').val()>0 && $('#fecha_ini').val().length>0 && $('#texto').val().length>0
          && ban==1){
      var funcionExito =function(resp){
         $('.table tbody').html(resp)
         ocultar_error_layer();
         buscarNotificaciones()
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      alert("Completa los campos faltantes")
  }
}


function mostrar_botones(Obj){
   if($(Obj).next().is(":hidden")) {
	   $(Obj).next().slideDown("fast");
           $(Obj).html('Ocultar')
   } else {
	   $(Obj).next().slideUp("fast");
           $(Obj).html('Mostrar')
   }

}