var rutaAjax = "ajax/reset_account_aj.php"

function reset_account() {
    var params = new Object()
    params.action = "reset_account"
    params.email = $('#user').val()
    var functionExito = function (resp) {
        console.log(resp)
        if (resp.status == "sent") {
            error = 'Se ha enviado una liga de recuperación<br> de contraseña a <span class="email">' + resp.email + '</span><br><br><a href="index.php">Regresar</a>';
        } else {
            error = 'El correo no se encuentra registrado<br><br><a href="index.php">Regresar</a';
        }
        $('#texto-reset').html(error);
        $('#box_user,#buttonLogin').css('display','none');
    };
    $.post(rutaAjax, params, functionExito, "json");
}

