$(document).ready(function () {
    if ($('#Id_curso').val() > 0) {
        $('.form input.readonly').each(function () {
            $(this).attr('readonly', true);
        })

        //readOnly select
        $('.form select').each(function () {
            //change the selction back to the original
            if ($(this).attr('id') == "oferta" || $(this).attr('id') == "oferta") {
                $(this).change(function (e) {
                    $(this).val($(this).find('option[selected="selected"]').val());
                });
                $(this).removeAttr('onchange')
            }
        })


    }
});

function save_curso() {
    var params = new Object();
    params.action = "save_curso"
    params.Id_curso = $('#Id_curso').val()
    params.Id_oferta = $('#oferta option:selected').val()
    params.nombre_curso = $('#nombre_curso').val()
    params.resgistro = $('#resgistro').val()
    params.insti = $('#insti').val()
    params.modalidad = $('#modalidad option:selected').val()
    params.duracion = $('#duracion').val()
    params.clave_Plan = $('#clave_Plan').val()
    params.escala = $('#escala option:selected').val()
    params.minima_apro = $('#minima_apro').val()
    params.limite_faltas = $('#limite_faltas').val()
    params.duracion = $('#duracion').val()
    params.tipo_insc = $('#tipo_insc option:selected').val()
    params.tipoCiclo = $('#tipoCiclo option:selected').val()
    params.Precio_curso = $('#Precio_curso').val()
    params.Inscripcion_curso = $('#Inscripcion_curso').val()
    params.Num_ciclos = $('#Num_ciclos').val()
    params.num_pagos = $('#num_pagos').val()
    if ($('#nivel_padre').length > 0) {
        params.nivel_padre = $('#nivel_padre').val()
    }
    params.tipo = $('#tipo').val()
    params.tipoCiclo = $('#tipoCiclo option:selected').val()

    var error = 0
    var errores = new Array()

    if ($('#nombre_curso').val().length == 0) {
        error = "Nombre requerido"
        errores.push(error);
    }
    if ($('#oferta option:selected').val() == 0) {
        error = "Nivel acádemico requerido"
        errores.push(error);
    }
    if ($('#Num_ciclos').val().length == 0) {
        error = "Número de grados requerido"
        errores.push(error);
    }
    if ($('#escala option:selected').val() == 0) {
        error = "Escala de calificación requerido"
        errores.push(error);
    }
    if ($('#minima_apro').val().length == 0) {
        error = "Calificación mínima requerido"
        errores.push(error);
    }
    if ($('#tipoCiclo option:selected').val() == 0) {
        error = "Tipo  de ciclo requerido"
        errores.push(error);
    }
    if ($('#num_pagos').val().length == 0) {
        error = "Número de pagos por grado requerido"
        errores.push(error);
    }

    if ($('#Precio_curso').val().length == 0) {
        error = "Costo total requerido"
        errores.push(error);
    }
    if (errores.length == 0) {
        $.post("ajax/curso_aj.php", params, function () {
            alert("Datos guardados con éxito")
            window.location = "oferta.php?id=" + params.Id_oferta
        }, "json");
    } else {
        imprimirErrores(errores)
    }
}
