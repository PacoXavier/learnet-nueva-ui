function mostrar_box_pagos(){
    var params= new Object();	
        params.action="mostrar_box_pagos"
        params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("pagos_adelantados_aj.php",params,funcionExito);
}



function save_pago_adelantado(){
    var params= new Object();	
        params.action="save_pago_adelantado"
        params.Id_alum=$('#Id_alum').val()
        params.Fecha_pago=$('#Fecha_pago').val()
        params.Monto=$('#Monto').val()
        params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
        params.Id_met_pago=$('#metodo_pago option:selected').val()
        params.Id_ciclo_usar=$('#ciclo option:selected').val()
        params.Id_paq_des=$('#tipo_descuento option:selected').val()
    if($('#Fecha_pago').val().length>0 && $('#Monto').val().length>0 && $('#metodo_pago option:selected').val()>0 && $('#ciclo option:selected').val()>0){
    var funcionExito= function(resp){
        $('#tabla').html(resp)
        ocultar_error_layer()
    }
    $.post("pagos_adelantados_aj.php",params,funcionExito);
     }else{
         alert("Completa los campos faltantes")
     }
}

function delete_pago_adelantado(Id_pad){
   if(confirm("¿Está seguro de eliminar el pago adelantado?")){
	    var params= new Object();	
	        params.action="delete_pago_adelantado"
	        params.Id_pad=Id_pad
                params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
	    var funcionExito= function(resp){
            $('#tabla').html(resp)
            ocultar_error_layer()
	    }
	    $.post("pagos_adelantados_aj.php",params,funcionExito);
    }
}

function mostrar_recibo(Id_pp){
    window.open("comprobante_pago_adelantado.php?id="+Id_pp);
}



function mostrar_botones(Obj){
   if($(Obj).next().is(":hidden")) {
	   $(Obj).next().slideDown("fast");
           $(Obj).html('Ocultar')
   } else {
	   $(Obj).next().slideUp("fast");
           $(Obj).html('Mostrar')
   }
}

function send_recibo(Id_pad,Obj){
    if(confirm("¿Esta seguro de enviar el recibo?")){
    var params= new Object();	
        params.action="send_recibo"
        params.Id_pad=Id_pad
        $(Obj).html('Enviando...')
    var funcionExito= function(resp){
        if(resp.status!="sent"){
            alert("Recibo enviado por email")
        }else{
           alert("Error al enviar el recibo");  
        }  
        $(Obj).html('Enviar recibo')
    }
    $.post("pagos_adelantados_aj.php",params,funcionExito,"json");
   }
}

function mostrar_cargos_saldados(Id_pad){
    var params= new Object();	
        params.action="mostrar_cargos_saldados"
        params.Id_pad=Id_pad
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("pagos_adelantados_aj.php",params,funcionExito);
}
