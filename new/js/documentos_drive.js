var rutaAjax = "ajax/documentos_drive_aj.php"
var Id_Obj_focus
var ObjFocus
$(window).keypress(function(e) {
    if(Id_Obj_focus.length>0){
       deleteEditaName()
    }
});


if($('#files').length) {
   document.getElementById('files').addEventListener('change', handleFileSelect, false);
}

function mostrarFinder() {
    $('#files').click()
}
function handleFileSelect(evt) {
    //Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // Great success! All the File APIs are supported.

        // FileList object
        var files = evt.target.files;

        var count = 0;
        var bin, name, type, size

        //Funcion loadStartImg
        var loadStartImg = function() {
            //alert("inicia") 
            mostrar_error_layer('<div id="cargando"><i class="fa fa-spinner fa-spin"></i></div>');
        }

        //Funcion loadEndImg
        var loadEndImg = function(e) {
            var xhr
            if (window.XMLHttpRequest) {
                xhr = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
            //var bin = reader.result;

            // progress bar loadend
            var eventSource = xhr.upload || xhr;
            eventSource.addEventListener("progress", function(e) {
                var pc = parseInt((e.loaded / e.total * 100));
                //$('#mascara_img span').html(pc+'%') 
                var mns = 'Cargando ...' + pc + '%'
                //alerta(mns);
                if (pc == 100) {
                    //ocultar_error_layer()
                }
            }, false);

            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && xhr.status == 200) {
                   //$('#ruta').html(xhr.responseText)
                   //alert(xhr.responseText)
                   //alert("Archivo cargado con éxito")
                   //$('#files').val('')
                   window.location.reload()
                }
            }
            xhr.open('POST', rutaAjax+'.php?action=uploadFile', true);
            var boundary = 'xxxxxxxxx';
            var body = '--' + boundary + "\r\n";
            body += "Content-Disposition: form-data; name='upload'; filename='" + name + "'; tipo_usu='"+$('#tipo-usu').val()+"'; id_parent='"+$('#id-parent').val()+"';\r\n";
            body += "Content-Type: application/octet-stream\r\n\r\n";
            body += bin + "\r\n";
            body += '--' + boundary + '--';
            xhr.setRequestHeader('content-type', 'multipart/form-data; boundary=' + boundary);
            // Firefox 3.6 provides a feature sendAsBinary ()
            if (xhr.sendAsBinary != null) {
                xhr.sendAsBinary(body);
                // Chrome 7 sends data but you must use the base64_decode on the PHP side
            } else {
                xhr.open('POST', rutaAjax+'?action=uploadFile&base64=ok&FileName=' + name + '&TypeFile=' + type+'&tipo_usu='+$('#tipo-usu').val()+'&id_parent='+$('#id-parent').val(), true);
                xhr.setRequestHeader('UP-FILENAME', name);
                xhr.setRequestHeader('UP-SIZE', size);
                xhr.setRequestHeader('UP-TYPE', type);
                xhr.send(window.btoa(bin));
            }
            if (status) {
                //document.getElementById(status).innerHTML = '';
            }
        }

        //Funcion loadErrorImg
        var loadErrorImg = function(evt) {
            switch (evt.target.error.code) {
                case evt.target.error.NOT_FOUND_ERR:
                    alert('File Not Found!');
                    break;
                case evt.target.error.NOT_READABLE_ERR:
                    alert('File is not readable');
                    break;
                case evt.target.error.ABORT_ERR:
                    break; // noop
                default:
                    alert('An error occurred reading this file.');
            }
            ;
        }

        //Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {
            /*
                                    // Only process image files.
                                    if (!f.type.match('image.*')) {
                                        alert(1)
                                        continue;
                                    }
                            */

            var reader = new FileReader();
            var preview = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail binari.
                    bin = e.target.result
                    name = theFile.name
                    type = theFile.type
                    size = theFile.size
                };
            })(f);

            preview.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail.
                    /*
                     var li = document.createElement('li');
                     $(li).attr('id','img_'+count)
                     li.innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/><div id="mascara_img"><span></span></div>'].join('');
                     $('#visor_subir_img').append(li)
                     $('#visor_subir_img li img').css({'width':'85px','height':'85px'});
                     $('#ruta_img').val($('#files').val())
                     count++;
                     */
                };
            })(f);

            //Read in the image file as a binary string.
            reader.readAsBinaryString(f);
            //Read in the image file as a data URL.
            preview.readAsDataURL(f);

            // Firefox 3.6, WebKit
            if (reader.addEventListener) {
                reader.addEventListener('loadend', loadEndImg, false);
                reader.addEventListener('loadstart', loadStartImg, false);
                if (status != null) {
                    reader.addEventListener('error', loadErrorImg, false);
                }
                // Chrome 7
            } else {
                reader.onloadend = loadEndImg;
                reader.onloadend = loadStartImg;
                if (status != null) {
                    reader.onerror = loadErrorImg;
                }
            }
        }

    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
}





function editaName(Obj,IdKey){
    Id_Obj_focus=IdKey
    ObjFocus=$(Obj)
    $('.edit-name').removeClass('edit-name')
    $('.edit-name').removeAttr("contenteditable")
    $(Obj).addClass('edit-name')
    $(Obj).attr("contenteditable","true")
}


function deleteEditaName(){
    if(Id_Obj_focus.length>0){
        var params= new Object();
             params.action="editNameFile"
             params.Id_file=Id_Obj_focus
             params.Name=$(ObjFocus).text()
        var funcionExito=function(resp){
            $('.edit-name').removeClass('edit-name')
            $('.edit-name').removeAttr("contenteditable")
            Id_Obj_focus= null
        }
        $.post(rutaAjax,params,funcionExito);
    }
}

function getFiles(Id,Tipo,URL){
    
    if(Tipo=="Carpeta"){
       window.location="documentos_drive.php?id="+Id 
    }else{
       window.open(URL, '_blank');
    }
}


function mostrarBoxNuevo(){
    var params= new Object();
         params.action="mostrarBoxNuevo"
    var funcionExito=function(resp){
        mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
          
}


function saveDocumento(Obj){
    var params= new Object();
         params.action="saveDocumento"
         params.nombre=$('#nombre').val()
         params.tipoDoc=$('#tipo-doc option:selected').val()
         params.id_parent=$('#id-parent').val()
         params.tipo_usu=$('#tipo-usu').val()

    if($('#nombre').val().length>0 && $('#tipo-doc option:selected').val()>0){
        $(Obj).removeAttr('onClick')
        var funcionExito=function(resp){
            window.location.reload()
        }
        $.post(rutaAjax,params,funcionExito);
    }else{
        alert("Completa los campo")
    }
}

function deleteFile(Id_file){
    if(confirm("¿Está seguro de eliminar el documento?")){
        var params= new Object();
             params.action="deleteFile"
             params.Id_file=Id_file
        var funcionExito=function(resp){
           window.location.reload()
        }
        $.post(rutaAjax,params,funcionExito);    
    }
}