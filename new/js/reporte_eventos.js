var rutaAjax = "ajax/reporte_eventos_aj.php"
$(window).ready(function(){
    $(".boxfil").draggable();
})

function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Id_salon=$('#aula option:selected').val()
       params.Fecha_ini=$('#fecha_ini').val()
       params.Fecha_fin=$('#fecha_fin').val()
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}


function mostrar_box_pagar(Id_pago){
    var params= new Object();	
        params.action="mostrar_box_pagar"
        params.Id_pago=Id_pago
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}

function pagar(Id_event){
    var params= new Object();	
        params.action="pagar"
        params.Id_event=Id_event
        params.Cantidad=$('#Cantidad').val()
        params.FechaPago=$('#FechaPago').val()
        params.Id_met=$('#Id_met option:selected').val()
        params.Comentarios=$('#comentarios').val()
        params.Nombre=$('#Nombre').val()
        params.Email=$('#Email').val()
    if($('#Id_met option:selected').val()>0  && $('#Cantidad').val().length>0 && $('#FechaPago').val().length>0 && $('#Nombre').val().length>0 && $('#Email').val().length>0){
    var funcionExito= function(resp){
        $('.table tbody').html(resp)
	ocultar_error_layer()
    }
    $.post(rutaAjax,params,funcionExito);
    }else{
        alert("Completa los campos faltantes")
    }
}


function mostrar_botones(Obj){
   if($(Obj).next().is(":hidden")) {
	   $(Obj).next().slideDown("fast");
           $(Obj).html('Ocultar opciones')
   } else {
	   $(Obj).next().slideUp("fast");
           $(Obj).html('Mostrar opciones')
   }
}


function mostrar_historial(Id_evento){
    var params= new Object();	
        params.action="mostrar_historial"
        params.Id_evento=Id_evento
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}



function mostrar_recibo(Id_pp){
    window.open("comprobante_pago_evento.php?id="+Id_pp);
}



function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Id_salon=$('#aula option:selected').val()
       params.Fecha_ini=$('#fecha_ini').val()
       params.Fecha_fin=$('#fecha_fin').val()
       
   var query=$.param( params );
   window.open(rutaAjax+"?"+query, '_blank');
}