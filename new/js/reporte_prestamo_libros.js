var rutaAjax = "ajax/reporte_prestamo_libros_aj.php"
$(window).ready(function(){
    $(".boxfil").draggable();
})

function imprimir(){
    window.print()
} 

function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_libro=$('#libro').attr('id-data')
       params.Id_usuario=$('#usuario').attr('id-data')
       params.Fecha_ini=$('#fecha_ini').val()
       params.Fecha_fin=$('#fecha_fin').val()
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}


function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Id_libro=$('#libro').attr('id-data')
       params.Id_usuario=$('#usuario').attr('id-data')
       params.Fecha_ini=$('#fecha_ini').val()
       params.Fecha_fin=$('#fecha_fin').val()
       
   var query=$.param( params );
   window.open(rutaAjax+"?"+query, '_blank');
}





function buscarUsu(Obj){
  if($.trim($(Obj).val()) && $(Obj).val().length>=4){
   var params= new Object()
       params.action="buscarUsu"
       params.buscar="%"
       if($.trim($(Obj).val())){
           params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id-data',$(this).attr('id-data'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post('index_aj.php',params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id-data')
  }
}


function buscarLibro(Obj){
  if($.trim($(Obj).val())){
   var params= new Object()
       params.action="buscarLibro"
       params.buscar="%"
       if($.trim($(Obj).val())){
           params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id-data',$(this).attr('id-data'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post('index_aj.php',params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id-data')
  }
}
