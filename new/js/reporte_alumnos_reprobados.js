var rutaAjax = "ajax/reporte_alumnos_reprobados_aj.php"
$(window).ready(function(){
    $(".boxfil").draggable();
    if($('#files').length) {
       document.getElementById('files').addEventListener('change', handleFileSelect, false);
    }
})


function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Id_docente=$('#Id_docente').attr('id-data')
       params.Id_grupo=$('#Id_grupo').attr('id-data')
       params.Calificacion=$('#calificacion').val()
       //params.Id_ofe=$('#oferta option:selected').val()
       //params.Id_esp=$('#curso option:selected').val()
       //params.Id_ori=$('#orientacion option:selected').val()
       
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}

function buscarDocente(Obj){
  if($.trim($(Obj).val()) && $(Obj).val().length>=4){
   var params= new Object()
       params.action="buscarDocent"
       params.buscar="%"
       if($.trim($(Obj).val())){
           params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id-data',$(this).attr('id-data'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post('index_aj.php',params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id-data')
  }
}


function buscarGruposCiclo(Obj){
  if($.trim($(Obj).val())){
   var params= new Object()
       params.action="buscarGruposCiclo"
       params.buscar="%"
       if($.trim($(Obj).val())){
           params.buscar=$(Obj).val()
       }
       params.Id_ciclo=$('#ciclo option:selected').val()
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id-data',$(this).attr('id-data'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post('index_aj.php',params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id-data')
  }
}


function handleFileSelect(evt) {
    //Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // Great success! All the File APIs are supported.

        // FileList object
        var files = evt.target.files;

        var count = 0;
        var bin, name, type, size

        //Funcion loadStartImg
        var loadStartImg = function() {
            //alert("inicia") 
        }

        //Funcion loadEndImg
        var loadEndImg = function(e) {
            var xhr
            if (window.XMLHttpRequest) {
                xhr = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
            //var bin = reader.result;

            // progress bar loadend
            var eventSource = xhr.upload || xhr;
            eventSource.addEventListener("progress", function(e) {
                var pc = parseInt((e.loaded / e.total * 100));
                //$('#mascara_img span').html(pc+'%') 
                var mns = 'Cargando ...' + pc + '%'
                //alerta(mns);
                if (pc == 100) {
                    //ocultar_error_layer()
                }
            }, false);

            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && xhr.status == 200) {
                   $('#list_archivos').html(xhr.responseText)
                   $('#files').val('')
                }
            }
            xhr.open('POST', 'index_aj.php?action=uploadAttachment', true);
            var boundary = 'xxxxxxxxx';
            var body = '--' + boundary + "\r\n";
            body += "Content-Disposition: form-data; name='upload'; filename='" + name + "'\r\n";
            body += "Content-Type: application/octet-stream\r\n\r\n";
            body += bin + "\r\n";
            body += '--' + boundary + '--';
            xhr.setRequestHeader('content-type', 'multipart/form-data; boundary=' + boundary);
            // Firefox 3.6 provides a feature sendAsBinary ()
            if (xhr.sendAsBinary != null) {
                xhr.sendAsBinary(body);
                // Chrome 7 sends data but you must use the base64_decode on the PHP side
            } else {
                xhr.open('POST', 'index_aj.php?action=uploadAttachment&base64=ok&FileName=' + name + '&TypeFile=' + type, true);
                xhr.setRequestHeader('UP-FILENAME', name);
                xhr.setRequestHeader('UP-SIZE', size);
                xhr.setRequestHeader('UP-TYPE', type);
                xhr.send(window.btoa(bin));
            }
            if (status) {
                //document.getElementById(status).innerHTML = '';
            }
        }

        //Funcion loadErrorImg
        var loadErrorImg = function(evt) {
            switch (evt.target.error.code) {
                case evt.target.error.NOT_FOUND_ERR:
                    alert('File Not Found!');
                    break;
                case evt.target.error.NOT_READABLE_ERR:
                    alert('File is not readable');
                    break;
                case evt.target.error.ABORT_ERR:
                    break; // noop
                default:
                    alert('An error occurred reading this file.');
            }
            ;
        }

        //Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {
            /*
                                    // Only process image files.
                                    if (!f.type.match('image.*')) {
                                        alert(1)
                                        continue;
                                    }
                            */

            var reader = new FileReader();
            var preview = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail binari.
                    bin = e.target.result
                    name = theFile.name
                    type = theFile.type
                    size = theFile.size
                };
            })(f);

            preview.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail.
                    /*
                     var li = document.createElement('li');
                     $(li).attr('id','img_'+count)
                     li.innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/><div id="mascara_img"><span></span></div>'].join('');
                     $('#visor_subir_img').append(li)
                     $('#visor_subir_img li img').css({'width':'85px','height':'85px'});
                     $('#ruta_img').val($('#files').val())
                     count++;
                     */
                };
            })(f);

            //Read in the image file as a binary string.
            reader.readAsBinaryString(f);
            //Read in the image file as a data URL.
            preview.readAsDataURL(f);

            // Firefox 3.6, WebKit
            if (reader.addEventListener) {
                reader.addEventListener('loadend', loadEndImg, false);
                reader.addEventListener('loadstart', loadStartImg, false);
                if (status != null) {
                    reader.addEventListener('error', loadErrorImg, false);
                }
                // Chrome 7
            } else {
                reader.onloadend = loadEndImg;
                reader.onloadend = loadStartImg;
                if (status != null) {
                    reader.onerror = loadErrorImg;
                }
            }
        }

    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
}



function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Id_docente=$('#Id_docente').attr('id-data')
       params.Id_grupo=$('#Id_grupo').attr('id-data')
       params.Calificacion=$('#calificacion').val()
       
   var query=$.param( params );
   window.open(rutaAjax+"?"+query, '_blank');
}
