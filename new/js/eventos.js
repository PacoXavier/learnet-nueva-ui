var rutaAjax = "ajax/eventos_aj.php"
$(function() {
   updateCalendar();
});

function updateCalendar(){
        $('#calendar').fullCalendar('destroy')
 	$('#calendar').fullCalendar({
                //theme: true,
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
                hiddenDays:[0],
                lang: 'es',
		editable: true,
		eventLimit: true, // allow "more" link when too many events
		events: function(start, end, timezone, callback) {
                    $.ajax({
                        method: "POST",
                        url: rutaAjax,
                        dataType: 'json',
                        data: {
                            // our hypothetical feed requires UNIX timestamps
                            action: "getEventosCiclo",
                            Id_ciclo: $('#Id_ciclo_eventos').val()
                        },
                        success: function(resp) {
                            var events = [];
                            
                             //Fecha de inicio y fin del ciclo
                             $.each(resp.Ciclo, function(key, val) {
                                    events.push({
                                        rendering: 'inverse-background',
                                        color: '#606060',
                                        start: val.FechaIni,
                                        end:val.FechaFin
                                    });
                            });
                            
                            //Obtenemos los dias festivos
                            $.each(resp.DiasFestivos, function(key, val) {
                                    events.push({
                                        rendering: 'background',
                                        color: '#606060',
                                        start: val.FechaIni
                                    });
                            });
                            
                            $.each(resp.Eventos, function(key, val) {
                                    events.push({
                                        id: val.Id,
                                        //color: '#ff9f89',
                                        
                                        title: val.Nombre,
                                        start: val.Start,
                                        end:val.End
                                    });
                            });
                            callback(events);
                        }
                    });
                },
                dayClick: function(date, view) {
                    //$(this).css('background-color', 'red');
                    mostrar_box_evento(date.format(),view.name,0)
                },
                eventClick: function(calEvent) {
                    mostrar_info_evento(calEvent.id,calEvent.start.format(),calEvent.end.format())
                },
                eventDrop: function(event, delta, revertFunc) {
                   // if (confirm("¿Está seguro de cambiar el día del evento?")) {
                        update_evento_drop(event.id,event.start.format(),event.end.format())
                    //}
                },
                eventResize: function(event, jsEvent, ui, view ) {
                    //if (confirm("¿Está seguro de cambiar la hora del evento?")) {
                        update_evento_resize(event.id,event.start.format(),event.end.format())
                        //var start = moment(event.start).format("DD-MM-YYYY HH:mm");
                    //}
                }
	});   
}
//

function mostrar_box_evento(Date,tipo,Id){
    var params= new Object();	
        params.action="mostrar_box_evento"
        params.Id_evento=Id
        params.Start=Date
        params.Tipo=tipo
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
        inputTime();
    }
    $.post(rutaAjax,params,funcionExito);
}

function mostrar_info_evento(Id,start,end){
    var params= new Object();	
        params.action="mostrar_box_evento"
        params.Id_evento=Id
        params.FechaInicio=start.substr(0, start.indexOf('T'));
        params.HoraInicio=start.substr(start.indexOf('T')+1);
        params.FechaFin=end.substr(0, end.indexOf('T'));
        params.HoraFin=end.substr(end.indexOf('T')+1);
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
        inputTime();
    }
    $.post(rutaAjax,params,funcionExito);
}


function delete_evento(Id){
    if(confirm("¿Esta seguro de cancelar el evento?")){
        var params= new Object();	
            params.action="delete_evento"
            params.Id_evento=Id
        var funcionExito= function(resp){
           updateCalendar()
           ocultar_error_layer()
        }
        $.post(rutaAjax,params,funcionExito);
    }
}


function inputTime(){
     $('.eventos .time').timepicker({
        'minTime': '07:00:00',
        'maxTime': '21:00:00',
        'timeFormat': 'H:i:s'
    });
    // initialize datepair
    $('.eventos').datepair();   
}


function save_evento(Id_evento){
    var params= new Object();	
        params.action="save_evento"
        params.Id_evento=Id_evento
        params.Nombre=$('#nombre').val()
        params.start=$('#start').val()
        params.Id_ciclo=$('#ciclo option:selected').val()
        params.aula=$('#aula option:selected').val()
        params.costo=$('#costo').val()
        params.comentarios=$('#comentarios').val()
        params.HoraInicio=$('#horaInicio').val()
        params.HoraFin=$('#horaFin').val()
        params.tipo="";
        if($('#alumno').is(':checked') && $('#docente').is(':checked')){
            params.tipo="ambos";
        }else if($('#alumno').is(':checked')){
            params.tipo="alumno";
        }else if($('#docente').is(':checked')){
            params.tipo="docente";
        }
    if($('#nombre').val().length>0 && $('#ciclo option:selected').val()>0 && $('#aula option:selected').val()>0 && $('#horaInicio').val().length>0 && $('#horaFin').val().length>0 &&  params.tipo.length>0){
    var funcionExito= function(resp){
       updateCalendar()
       ocultar_error_layer()
    }
    $.post(rutaAjax,params,funcionExito);
    }else{
        alert("Completa los campos faltantes")
    }
}

function verificarDisponibilidad(Id_evento){
     var params= new Object();	
        params.action="verificarDisponibilidad"
        params.Id_evento=Id_evento
        params.Nombre=$('#nombre').val()
        params.start=$('#start').val()
        params.Id_ciclo=$('#ciclo option:selected').val()
        params.aula=$('#aula option:selected').val()
        params.costo=$('#costo').val()
        params.comentarios=$('#comentarios').val()
        params.HoraInicio=$('#horaInicio').val()
        params.HoraFin=$('#horaFin').val()
        
    if($('#nombre').val().length>0 && $('#ciclo option:selected').val()>0 && $('#aula option:selected').val()>0 && $('#horaInicio').val().length>0 && $('#horaFin').val().length>0){
    var funcionExito= function(resp){
        if(resp.Ban==1){
           if(confirm(resp.Texto)){
               save_evento(Id_evento)
           }else{
               ocultar_error_layer()
           }
       }else{
           save_evento(Id_evento)
       }
    }
    $.post(rutaAjax,params,funcionExito,"json");
    }else{
        alert("Completa los campos faltantes")
    }   
    
}

function update_evento_drop(Id_evento,start,end){
    var params= new Object();	
        params.action="update_evento_drop"
        params.Id_evento=Id_evento
        params.FechaInicio=start.substr(0, start.indexOf('T'));
        params.HoraInicio=start.substr(start.indexOf('T')+1);
        params.FechaFin=end.substr(0, end.indexOf('T'));
        params.HoraFin=end.substr(end.indexOf('T')+1);
    var funcionExito= function(resp){
      //alert(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}


function update_evento_resize(Id_evento,start,end){
    var params= new Object();	
        params.action="update_evento_resize"
        params.Id_evento=Id_evento
        params.start=start
        params.end=end
    var funcionExito= function(resp){
        //alert(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}
