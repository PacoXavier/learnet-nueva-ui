var rutaAjax = "ajax/ligar_materias_aj.php"

function buscar_int(){
 if($.trim($('#email_int').val())){
       var params= new Object()
           params.action="buscar_int"
           params.buscar=$('#email_int').val()
      var funcionExito =function(resp){
          $('#buscador_int').html(resp) 
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      $('#buscador_int').html('')
  }
}

function mostrar_box_materias(){
    var params= new Object();	
        params.action="mostrar_box_materias"
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}

function mostrar_box_materias_ciclo(){
    var params= new Object();	
        params.action="mostrar_box_materias_ciclo"
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}



function add_mat(){
  var params= new Object()
     params.action="add_mat"
     params.Id_docente=$('#Id_docente').val()
     params.Id_ciclo=$('#list_ciclos option:selected').val()
     params.Id_mate=$('#lista_materias option:selected').val()
     params.Id_orientacion=$('#orientaciones option:selected').val()
  if($('#list_ciclos option:selected').val()>0 && $('#lista_materias option:selected').val()){
  var funcionExito =function(resp){
      if(resp.indexOf('h2')>0){
         $('#box_info_materias').html(resp) 
          ocultar_error_layer(resp);
      }else{
          alert("La materia ya ha sido asignada")
      }

  }
  $.post(rutaAjax,params,funcionExito);
  }else{
      alert("Completa los campos faltantes")
  }
}


function get_mat_ciclo(){
    var params= new Object();	
        params.action="get_mat_ciclo"
        params.Id_docente=$('#Id_docente').val()
        params.Id_ciclo=$('#Id_ciclo option:selected').val()
      if($('#Id_ciclo option:selected').val()>0){
            var funcionExito= function(resp){
               $('#box_info_materias').html(resp) 
            }
            $.post(rutaAjax,params,funcionExito);
        }else{
            alerta("Selecciona un ciclo")
        }
}





/*
function confirm(text){
    var div='<div id="box_emergente"><h1>'+text+'</h1><p><button class="delete_confirm button">Aceptar</button><button onclick="ocultar_error_layer()" class="button">Cancelar</button></p></div>'
    mostrar_error_layer(div);	
}
*/

function delete_mat_doc(Id_matp){
   if(confirm("¿Está seguro de eliminar la materia asignada?")){
	    var params= new Object();	
                 params.action="delete_mat_doc"
                 params.Id_docente=$('#Id_docente').val()
                 params.Id_ciclo=$('#Id_ciclo option:selected').val()
                 params.Id_matp=Id_matp
	    var funcionExito= function(resp){
                $('#box_info_materias').html(resp) 
                ocultar_error_layer(resp);
	    }
	    $.post(rutaAjax,params,funcionExito);
    }
}

/*
function validar_si_asignada_a_grupo(Id_matp){
    if(confirm("¿Está seguro de eliminar la materia asignada?")){
        var params= new Object();	
            params.action="validar_si_asignada_a_grupo"
            params.Id_docente=$('#Id_docente').val()
            params.Id_ciclo=$('#Id_ciclo option:selected').val()
            params.Id_matp=Id_matp
          if($('#Id_ciclo option:selected').val()>0){
                var funcionExito= function(resp){
                    if(resp.existe==1){
                        alert("La materia no puede ser eliminada debido a que fue asignada al grupo "+resp.grupo+" en el horario del docente")
                    }else{
                        delete_mat_doc(Id_matp)
                    }
                }
                $.post("ligar_materias_aj.php",params,funcionExito,"json");
            }else{
                alerta("Selecciona un ciclo")
            }
    }
}
*/




function verificar_orientaciones(Obj){
    var params= new Object();	
        params.action="verificar_orientaciones"
        params.Id_mat_esp=$('#lista_materias option:selected').attr('Id_mat_esp')
    var funcionExito= function(resp){
        $('#box_orientaciones').html(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}

function buscar_materias(){
    var params= new Object();	
        params.action="buscar_materias"
        params.Id_ciclo=$('#Id_ciclo_buscar option:selected').val()
        params.Id_docen=$('#Id_docente').val()
    var funcionExito= function(resp){
        $('#box_materias_ciclo').html(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}



function marcar_materias(){
    if($('#all_materias').is(":checked")){
      $('.table tbody tr  input[type="checkbox"]').prop('checked',true);
    }else{
      $('.table tbody tr  input[type="checkbox"]').prop('checked',false);
    }
}

function add_mat_ciclo(){
  var params= new Object()
     params.action="add_mat_ciclo"
     params.Id_docente=$('#Id_docente').val()
     params.Id_ciclo=$('#Id_ciclo_actual option:selected').val()
     params.Materias= new Array()
    $('.table tbody tr ').each(function(){
       if($(this).find('input[type="checkbox"]').is(":checked")){
            var mat= new Object()
                mat.Id_mat=$(this).attr('id-mat')
                mat.Id_ori=$(this).attr('id-ori')
           params.Materias.push(mat)
       }
    })
  if($('#Id_ciclo_actual option:selected').val()>0){
  var funcionExito =function(resp){
      $('#box_info_materias').html(resp) 
      ocultar_error_layer(resp);
  }
  $.post(rutaAjax,params,funcionExito);
  }else{
      alert("Completa los campos faltantes")
  }
}