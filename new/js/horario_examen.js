var rutaAjax = "ajax/horario_examen_aj.php"
$(function() {
        updateCalendar();
});
var arrayDiasExamenes=new Array()
var Id_per_examen=0
function updateCalendar(){
        $('#calendar').fullCalendar('destroy')
 	$('#calendar').fullCalendar({
                //theme: true,
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
                hiddenDays:[0],
                lang: 'es',
		editable: false,
		eventLimit: true, // allow "more" link when too many events
		events: function(start, end, timezone, callback) {
                    $.ajax({
                        method: "POST",
                        url: rutaAjax,
                        dataType: 'json',
                        data: {
                            // our hypothetical feed requires UNIX timestamps
                            action: "getExamenesDocente",
                            Id_ciclo: $('#ciclo').val(),
                            Id_docen: $('#Id_docente').val()
                        },
                        success: function(resp) {
                            var events = [];
                            
                             //Fecha de inicio y fin del ciclo
                             $.each(resp.Ciclo, function(key, val) {
                                    events.push({
                                        rendering: 'inverse-background',
                                        color: '#606060',
                                        start: val.FechaIni,
                                        end:val.FechaFin
                                    });
                            });
                            
                            //Obtenemos los dias festivos
                            $.each(resp.DiasFestivos, function(key, val) {
                                    events.push({
                                        rendering: 'background',
                                        color: '#606060',
                                        start: val.FechaIni
                                    });
                            });
                             //Obtenemos el periodo de examen
                            $.each(resp.PeriodoExamen, function(key, val) {
                                    //
                                    
                                    $.each(val.Dias,function(k,v){
                                        arrayDiasExamenes.push(v)
                                    })
                                    events.push({
                                        rendering: 'background',
                                        color: '#f39c12',
                                        start: val.FechaIni,
                                        end:val.FechaFin
                                    });
                            });
                            
                            //Obtenemos el periodo de vacaciones
                            $.each(resp.PeriodoVacaciones, function(key, val) {
                                    events.push({
                                        rendering: 'background',
                                        color: '#00c0ef',
                                        start: val.FechaIni,
                                        end:val.FechaFin
                                    });
                            });
                            
                            
                            $.each(resp.Examenes, function(key, val) {
                                    events.push({
                                        id: val.Id,
                                        //color: '#ff9f89',
                                        title: val.Nombre,
                                        start: val.Start,
                                        end:val.End
                                    });
                            });
                            callback(events);
                        }
                    });
                },
                dayClick: function(date, view) {
                    
                    if($('#ciclo').val()>0){
                       
                           
                          //Si el dia esta dentro del periodo de examenes entonces mostrar el box
                          if(arrayDiasExamenes.indexOf(date.format())!=-1){
                               var params= new Object();	
                                    params.action="getIdPer"
                                    params.Fecha=date.format()
                                    params.Id_ciclo=$('#ciclo option:selected').val()
                                var funcionExito= function(resp){
                                    Id_per_examen=resp
                                    mostrar_box_evento(date.format(),view.name,0)
                                }
                                $.post(rutaAjax,params,funcionExito);
                          }else{
                            alert("Seleccione una fecha que se encuentre dentro de un  período de exámen")
                         }
                     }
                },
                eventClick: function(calEvent) {
                    if($('#ciclo').val()>0){
                         mostrar_info_evento(calEvent.id,calEvent.start.format(),calEvent.end.format())
                     }
                },
                eventDrop: function(event, delta, revertFunc) {
                    //if($('#ciclo').val()>0){
                   // if (confirm("¿Está seguro de cambiar el día del evento?")) {
                        //update_evento_drop(event.id,event.start.format(),event.end.format())
                    //}
                    //}
                },
                eventResize: function(event, jsEvent, ui, view ) {
                    //if($('#ciclo').val()>0){
                    //if (confirm("¿Está seguro de cambiar la hora del evento?")) {
                        //update_evento_resize(event.id,event.start.format(),event.end.format())
                        //var start = moment(event.start).format("DD-MM-YYYY HH:mm");
                    //}
                    //}
                }
	});   
}
//

function mostrar_box_evento(Date,tipo,Id){
    var params= new Object();	
        params.action="mostrar_box_evento"
        params.Id_examen=Id
        params.Start=Date
        params.Tipo=tipo
        params.Id_per=Id_per_examen
        params.Id_ciclo=$('#ciclo option:selected').val()
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
        inputTime();
    }
    $.post(rutaAjax,params,funcionExito);
}

function mostrar_info_evento(Id,start,end){
    var params= new Object();	
        params.action="mostrar_box_evento"
        params.Id_examen=Id
        params.FechaInicio=start.substr(0, start.indexOf('T'));
        params.HoraInicio=start.substr(start.indexOf('T')+1);
        params.FechaFin=end.substr(0, end.indexOf('T'));
        params.HoraFin=end.substr(end.indexOf('T')+1);
        params.Id_per=Id_per_examen
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
        inputTime();
    }
    $.post(rutaAjax,params,funcionExito);
}


function update_evento_drop(Id_evento,start,end){
    var params= new Object();	
        params.action="update_evento_drop"
        params.Id_evento=Id_evento
        params.FechaInicio=start.substr(0, start.indexOf('T'));
        params.HoraInicio=start.substr(start.indexOf('T')+1);
        params.FechaFin=end.substr(0, end.indexOf('T'));
        params.HoraFin=end.substr(end.indexOf('T')+1);
    var funcionExito= function(resp){
      //alert(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}


function update_evento_resize(Id_evento,start,end){
    var params= new Object();	
        params.action="update_evento_resize"
        params.Id_evento=Id_evento
        params.start=start
        params.end=end
    var funcionExito= function(resp){
        //alert(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}


/**/

function buscarDocent(Obj){
   if($.trim($(Obj).val()) && $(Obj).val().length>=4){
   var params= new Object()
       params.action="buscarDocent"
       params.buscar="%"
       if($.trim($(Obj).val())){
       params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id-data',$(this).attr('id-data'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post(rutaAjax,params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id-data')
  }
}


function buscarGruposCiclo(Obj){
   if($.trim($(Obj).val()) && $(Obj).val().length>=4){
       
   var params= new Object()
       params.action="buscarGruposCiclo"
       params.buscar="%"
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Id_per_examen=$('#Id_per_examen').val()
       if($.trim($(Obj).val())){
       params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id-data',$(this).attr('id-data'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post(rutaAjax,params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id-data')
  }
}


function addGrupoExamen(){
    if($('#Id_grupo').val().length>0 && $('#Id_grupo').attr('id-data')>0){
            $('#lista-grupos').append('<li id-data="'+$('#Id_grupo').attr('id-data')+'">'+$('#Id_grupo').val()+' <i class="fa fa-trash-o" aria-hidden="true" onclick="deleteNodo(this)"></i></li>')
            $('#Id_grupo').val('')
            $('#Id_grupo').removeAttr('id-data')
    }else{
        alert("Selecciona el grupo")
    }
}

function addDocenteExamen(){
    if($('#Id_docen').val().length>0 && $('#Id_docen').attr('id-data')>0){
            $('#lista-docentes').append('<li id-data="'+$('#Id_docen').attr('id-data')+'">'+$('#Id_docen').val()+' <i class="fa fa-trash-o" aria-hidden="true" onclick="deleteNodo(this)"></i></li>')
            $('#Id_docen').val('')
            $('#Id_docen').removeAttr('id-data')
    }else{
        alert("Selecciona el docente")
    }  
}

function deleteNodo(Obj){
    if(confirm("¿Está seguro de eliminar este elemento?")){
      $(Obj).closest('li').remove()
    }
}

function verificarDisponibilidad(Id_examen){
     var params= new Object();	
        params.action="verificarDisponibilidad"
        params.Id_examen=Id_examen
        params.Docentes=new Array()
        params.Grupos=new Array()
        params.start=$('#start').val()
        params.Id_ciclo=$('#ciclo option:selected').val()
        params.Id_aula=$('#aula option:selected').val()
        params.HoraInicio=$('#horaInicio').val()
        params.HoraFin=$('#horaFin').val()
        params.Id_per_examen=$('#Id_per_examen').val()
        $('#lista-grupos li').each(function(){
            params.Grupos.push($(this).attr('id-data'))
        })
        
         $('#lista-docentes li').each(function(){
            params.Docentes.push($(this).attr('id-data'))
        })
       
    if($('#ciclo option:selected').val()>0 && $('#aula option:selected').val()>0 && $('#horaInicio').val().length>0 && $('#horaFin').val().length>0 && params.Grupos.length>0 && params.Docentes.length>0){
    var funcionExito= function(resp){
        
        if(resp.status==1){
           alert(resp.error)
       }else{
           save_examen(Id_examen)
       }
    }
    $.post(rutaAjax,params,funcionExito,"json");
    }else{
        alert("Completa los campos faltantes")
    }   
}


function save_examen(Id_exa){
    var params= new Object();	
        params.action="save_examen"
        params.Id_examen=Id_exa
        params.Docentes=new Array()
        params.Grupos=new Array()
        params.start=$('#start').val()
        params.Id_ciclo=$('#ciclo option:selected').val()
        params.Id_aula=$('#aula option:selected').val()
        params.HoraInicio=$('#horaInicio').val()
        params.HoraFin=$('#horaFin').val()
        params.Id_per_examen=$('#Id_per_examen').val()
        
        $('#lista-grupos li').each(function(){
            params.Grupos.push($(this).attr('id-data'))
        })
        
         $('#lista-docentes li').each(function(){
            params.Docentes.push($(this).attr('id-data'))
        })
    if($('#ciclo option:selected').val()>0 && $('#aula option:selected').val()>0 && $('#horaInicio').val().length>0 && $('#horaFin').val().length>0 && params.Grupos.length>0 && params.Docentes.length>0){
        var funcionExito= function(resp){
           updateCalendar()
           ocultar_error_layer()

        }
    $.post(rutaAjax,params,funcionExito);
    }else{
        alert("Completa los campos faltantes")
    }
}


function delete_examen(Id){
    if(confirm("¿Está seguro de cancelar el exámen?")){
        var params= new Object();	
            params.action="delete_examen"
            params.Id_evento=Id
        var funcionExito= function(resp){
           updateCalendar()
           ocultar_error_layer()
        }
        $.post(rutaAjax,params,funcionExito);
    }
}


function inputTime(){
     $('.eventos .time').timepicker({
        'minTime': '07:00:00',
        'maxTime': '21:00:00',
        'timeFormat': 'H:i:s',
        'step':'60'
    });
    // initialize datepair
    $('.eventos').datepair();   
}