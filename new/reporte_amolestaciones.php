<?php
require_once('estandares/includes.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoAmonestaciones.php');
require_once('clases/modelos/base.php');
require_once('clases/modelos/Amonestaciones.php');

$base = new base();
$DaoOfertas = new DaoOfertas();
$DaoAlumnos = new DaoAlumnos();
$DaoEspecialidades = new DaoEspecialidades();
$DaoOrientaciones = new DaoOrientaciones();
$DaoOfertasAlumno = new DaoOfertasAlumno();
$DaoGrados = new DaoGrados();
$DaoDocentes= new DaoDocentes();
$DaoCiclos= new DaoCiclos();
$DaoAmonestaciones= new DaoAmonestaciones();

$ciclo=$DaoCiclos->getActual();
$Id_ciclo=$ciclo->getId();

links_head("Amonestaciones | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Amonestados</h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes" style="margin-bottom: 30px">
                    <ul class="row" style="list-style: none; font-size: 20px;">
                        <li class="col-md-6 weather-grids widget-shadow" onclick="mostrar_filtro()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-filter" style="margin-right: 5px;"></i> Filtros</div></li>
                        <li class="col-md-6 weather-grids weather-right widget-shadow states-last" onclick="download_excel()"><div class="stats-right" style="width:100%; height: 100px; display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-download" style="margin-right: 5px;"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Ciclo</td>
                                <td>Tipo</td>
                                <td style="width: 55px">Clave</td>
                                <td>Recibe</td>
                                <td>Fecha</td>
                                <td>Reporta</td>
                                <td style="width: 300px">Motivo</td>
                                <td>Num. Reportes</td>
                                <td>Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php        
                            
                             $count=1;
                             $query="AND Id_ciclo=".$Id_ciclo;
                             foreach($DaoAmonestaciones->showAll($query) as $k=>$v){
                                 $cantidad=0;
                                 $ciclo= $DaoCiclos->show($v->getId_ciclo());
                                 if($v->getTipo_recibe()=="usu"){
                                     
                                     $tipoRecibe="Usuario";
                                     $tipo="usu";
                                     $usu_recibe = $DaoUsuarios->show($v->getId_recibe());
                                     $Id=$usu_recibe->getId();
                                     $ClaveRecibe=$usu_recibe->getClave_usu();
                                     $NombreRecibe=$usu_recibe->getNombre_usu()." ".$usu_recibe->getApellidoP_usu()." ".$usu_recibe->getApellidoM_usu();
                                     $cantidad=$DaoAmonestaciones->getCantidadAmonestaciones($Id,"usu");
                                 }elseif($v->getTipo_recibe()=="docen"){
                                     $tipoRecibe="Docente";
                                     $tipo="docen";
                                     $doc=$DaoDocentes->show($v->getId_recibe());
                                     $Id=$doc->getId();
                                     $ClaveRecibe=$doc->getClave_docen();
                                     $NombreRecibe=$doc->getNombre_docen()." ".$doc->getApellidoP_docen()." ".$doc->getApellidoM_docen();
                                     $cantidad=$DaoAmonestaciones->getCantidadAmonestaciones($Id,"docen");
                                 }elseif($v->getTipo_recibe()=="alum"){
                                     $tipoRecibe="Alumno";
                                     $tipo="alum";
                                     $alum=$DaoAlumnos->show($v->getId_recibe());
                                     $Id=$alum->getId();
                                     $ClaveRecibe=$alum->getMatricula();
                                     $NombreRecibe=$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
                                     $cantidad=$DaoAmonestaciones->getCantidadAmonestaciones($Id,"alum");
                                 }
                                 
                                 if($v->getTipo_reporta()=="usu"){
                                     $usu_reporta = $DaoUsuarios->show($v->getId_reporta());
                                     $ClaveReporta=$usu_reporta->getClave_usu();
                                     $NombreReporta=$usu_reporta->getNombre_usu()." ".$usu_reporta->getApellidoP_usu()." ".$usu_reporta->getApellidoM_usu();
                                 }elseif($v->getTipo_reporta()=="docen"){
                                     $doc=$DaoDocentes->show($v->getId_reporta());
                                     $ClaveReporta=$doc->getClave_docen();
                                     $NombreReporta=$doc->getNombre_docen()." ".$doc->getApellidoP_docen()." ".$doc->getApellidoM_docen();
                                 }

                                  $style='class=""';
                                  if($cantidad>=3){
                                      $style='class="pink"';
                                  }
                             ?>
                                  <tr <?php echo $style;?>>
                                        <td><?php echo $count;?></td>
                                        <td><?php echo $ciclo->getClave()?></td>
                                        <td><?php echo $tipoRecibe?></td>
                                        <td><?php echo $ClaveRecibe?></td>
                                        <td><?php echo $NombreRecibe?></td>
                                        <td><?php echo $v->getFecha_rep()?></td>
                                        <td><?php echo $NombreReporta?></td>
                                        <td><?php echo $v->getMotivo()?></td>
                                        <td style="text-align: center;"><?php echo $cantidad;?></td>
                                        <td><a href="amolestaciones.php?id=<?php echo $Id?>&tipo=<?php echo $tipo ?>" target="_blank"><button>Amonestaciones</button></a></td>
                                    </tr>
                                  <?php
                                      
                                  $count++;
                                 }
                               
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>

<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="boxUlBuscador col-md-6">
            <p>Buscar<br><input type="search"  class="buscarFiltro form-control" onkeyup="buscar()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p class="col-md-6">Ciclo<br>
          <select id="ciclo" class="form-control" >
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<?php
write_footer();
