 <?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoGrupos.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoMateriasEspecialidad.php');
require_once('clases/modelos/Docentes.php');
require_once('clases/DaoTurnos.php');

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoGrupos= new DaoGrupos();
$DaoMaterias= new DaoMaterias();
$DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
$DaoDocentes= new DaoDocentes();
$DaoTurnos= new DaoTurnos();

$DaoCiclos= new DaoCiclos();
$ciclo=$DaoCiclos->getActual($_usu->getId_plantel());

links_head("Grupos | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-users"></i> Grupos</h1>
                </div>
                <div class="box-filter-reportes" style="margin-bottom: 30px">
                    <ul class="row" style="list-style: none; font-size: 20px;">
                        <li class="col-md-6 weather-grids widget-shadow" onclick="mostrar_filtro()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-filter" style="margin-right: 5px;"></i> Filtros</div></li>
                        <li class="col-md-6 weather-grids weather-right widget-shadow states-last" onclick="download_excel()"><div class="stats-right" style="width:100%; height: 100px; display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-download" style="margin-right: 5px;"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td style="width: 80px">CLAVE GRUPAL</td>
                                <td>Ciclo</td>
                                <td>MATERIA</td>
                                <td>Orientaci&oacute;n</td>
                                <td>TURNO</td>
                                <td style="width: 100px;text-align: center;">CAPACIDAD</td>
                                <td>PROFESOR</td>
                                <td style="text-align: center;">NUm. Clases</td>
                                <td style="text-align: center;">Primer parcial</td>
                                <td style="text-align: center;">Segundo parcial</td>
                                <td style="text-align: center;">Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                                if($ciclo->getId()>0){
                                foreach($DaoGrupos->showGruposByCiclo($ciclo->getId()) as $k=>$v){
                                        $mat = $DaoMaterias->show($v->getId_mat());
                                        $mat_esp=$DaoMateriasEspecialidad->show($v->getId_mat_esp());

                                        $tur = $DaoTurnos->show($v->getTurno());
                                        $turno=$tur->getNombre();
        
                                        $nombre_ori="";
                                        if($v->getId_ori()>0){
                                          $ori = $DaoOrientaciones->show($v->getId_ori());
                                          $nombre_ori=$ori->getNombre();
                                        }

                                        
                                        $TotalAsis=0; 
                                        $arrayAsistencias=array();
                                        if($v->getId()>0){
                                            $query_Asistencias = "SELECT * FROM Asistencias WHERE Id_grupo=".$v->getId();
                                            $Asistencias = mysql_query($query_Asistencias, $cnn) or die(mysql_error());
                                            $row_Asistencias = mysql_fetch_array($Asistencias);
                                            $totalRows_Asistencias = mysql_num_rows($Asistencias);
                                            if($totalRows_Asistencias>0){
                                               do{
                                                   if($row_Asistencias['Asistio']==1 || $row_Asistencias['Asistio']==0){
                                                       if(!isset($arrayAsistencias[$row_Asistencias['Fecha_asis']])){
                                                           $arrayAsistencias[$row_Asistencias['Fecha_asis']]=1;
                                                       }
                                                   }
                                               }while($row_Asistencias = mysql_fetch_array($Asistencias));
                                            }
                                        }
                                         $NombreMat=$mat->getNombre();
                                         if(strlen($mat_esp->getNombreDiferente())>0){
                                            $NombreMat=$mat_esp->getNombreDiferente(); 
                                         }                                 
                                         $TotalAlumnos=count($DaoGrupos->getAlumnosGrupo($v->getId()));
                                         $CalPrimerParcial=0;
                                         $CalSegundoParcial=0;
                                         
                                         $Id_eva_primerParcial=0;
                                         $Id_eva_SegundoParcial=0;
                                         
                                         $BloqueadosPrimer=0;
                                         $BloqueadosSegundo=0;
                                         
                                         
                                         $query_Evaluaciones = "SELECT * FROM Evaluaciones WHERE Id_mat_esp=".$v->getId_mat_esp()." AND Nombre_eva='PRIMER PARCIAL'";
                                         $Evaluaciones = mysql_query($query_Evaluaciones, $cnn) or die(mysql_error());
                                         $row_Evaluaciones= mysql_fetch_array($Evaluaciones);
                                         $totalRows_Evaluaciones = mysql_num_rows($Evaluaciones);
                                         if($totalRows_Evaluaciones>0){
                                            $Id_eva_primerParcial=$row_Evaluaciones['Id_eva'];
                                         }
                                         
                                         $query_Evaluaciones = "SELECT * FROM Evaluaciones WHERE Id_mat_esp=".$v->getId_mat_esp()." AND Nombre_eva='SEGUNDO PARCIAL'";
                                         $Evaluaciones = mysql_query($query_Evaluaciones, $cnn) or die(mysql_error());
                                         $row_Evaluaciones= mysql_fetch_array($Evaluaciones);
                                         $totalRows_Evaluaciones = mysql_num_rows($Evaluaciones);
                                         if($totalRows_Evaluaciones>0){
                                            $Id_eva_SegundoParcial=$row_Evaluaciones['Id_eva'];
                                         }

                                         //Primer parcial
                                         foreach($DaoGrupos->getAlumnosBYGrupo($v->getId()) as $k2=>$v2){

                                                $query_Calificaciones = "SELECT * FROM Calificaciones WHERE Id_ciclo_mat=".$v2['Id_ciclo_mat']." AND Id_eva=".$Id_eva_primerParcial;
                                                $Calificaciones = mysql_query($query_Calificaciones, $cnn) or die(mysql_error());
                                                $row_Calificaciones= mysql_fetch_array($Calificaciones);
                                                $totalRows_Calificaciones = mysql_num_rows($Calificaciones);
                                                if($totalRows_Calificaciones>0 && ($row_Calificaciones['Calificacion']>0 || $row_Calificaciones['Calificacion']=="AC" || $row_Calificaciones['Calificacion']=="SD" || $row_Calificaciones['Calificacion']=="EC" || $row_Calificaciones['Calificacion']=="CP" || $row_Calificaciones['Calificacion']=="NP")){
                                               // if($totalRows_Calificaciones>0 && strlen($row_Calificaciones['Calificacion'])>0){
                                                    $CalPrimerParcial+=1;
                                                }
                                                

                                                $query_Calificaciones = "SELECT * FROM Calificaciones WHERE Id_ciclo_mat=".$v2['Id_ciclo_mat']." AND Id_eva=".$Id_eva_SegundoParcial;
                                                $Calificaciones = mysql_query($query_Calificaciones, $cnn) or die(mysql_error());
                                                $row_Calificaciones= mysql_fetch_array($Calificaciones);
                                                $totalRows_Calificaciones_se = mysql_num_rows($Calificaciones);
                                                if($totalRows_Calificaciones_se>0 && ($row_Calificaciones['Calificacion']>0 || $row_Calificaciones['Calificacion']=="AC" || $row_Calificaciones['Calificacion']=="SD" || $row_Calificaciones['Calificacion']=="EC" || $row_Calificaciones['Calificacion']=="CP" || $row_Calificaciones['Calificacion']=="NP")){
                                                //if($totalRows_Calificaciones>0 && strlen($row_Calificaciones['Calificacion'])>0){
                                                    $CalSegundoParcial+=1;
                                                }
                                                
                                                if($v2['Id_ofe_alum']>0){
                                                    $Adeudo=$DaoAlumnos->getAdeudoOfertaAlumno($v2['Id_ofe_alum']);
                                                    if($Adeudo>0){
                                                         $BloqueadosPrimer++;
                                                         $BloqueadosSegundo++;
                                                    }
                                                }  
                                         }
                                         
                                         //$FaltantesPrimero=($TotalAlumnos-($CalPrimerParcial+$BloqueadosPrimer));
                                         $FaltantesPrimero=($TotalAlumnos-($CalPrimerParcial));
                                         $classPrimer='class="pink"';
                                         $TextPrimerParcial="Faltantes ".$FaltantesPrimero;
                                         if($FaltantesPrimero<=0){
                                            $TextPrimerParcial="Completo";
                                            $classPrimer='class="green"';
                                         }

                                         //$FaltantesSegundo=($TotalAlumnos-($CalSegundoParcial+$BloqueadosSegundo));
                                         $FaltantesSegundo=($TotalAlumnos-($CalSegundoParcial));
                                         $classSegundo='class="pink"';
                                         $TextSegundoParcial="Faltantes ".$FaltantesSegundo;
                                         if($FaltantesSegundo<=0){
                                            $TextSegundoParcial="Completo";
                                            $classSegundo='class="green"';
                                         }
                                         $resp=$DaoGrupos->getDocenteGrupo($v->getId());
                                         $NombreDocente="";
                                         if($resp['Id_docente']>0){
                                             $docente=$DaoDocentes->show($resp['Id_docente']);
                                             $NombreDocente=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
                                         }
                                ?>
                            
                            
                                <tr>
                                    <td><?php echo $v->getClave()?> </td>
                                    <td><?php echo $ciclo->getClave()?> </td>
                                    <td style="width: 120px;"><?php echo $NombreMat?></td>
                                    <td><?php echo $nombre_ori;?></td>
                                    <td><?php echo $turno;?></td>
                                    <td style="text-align: center;"><?php echo $v->getCapacidad()?></td>
                                    <td style="text-align: center;"><?php echo $NombreDocente?></td>
                                    <td style="color:black;font-weight: bold;text-align: center;"><?php echo count($arrayAsistencias);?></td>
                                    <td <?php echo $classPrimer?>><?php echo $TextPrimerParcial;?></td>
                                    <td <?php echo $classSegundo?>><?php echo $TextSegundoParcial;?></td>
                                    <td style="text-align: center;"><a href="reporte_alumnos_grupo.php?id=<?php echo $v->getId()?>" target="_blank"><button>Alumnos</button></a>
                                        <?php
                                        if(isset($perm['60'])){
                                        ?>
                                        <a href="reporte_capturar_calificaciones.php?id=<?php echo $v->getId()?>" target="_blank"><button>Calificaciones</button></a>
                                        <?php
                                        }
                                        if(isset($perm['61'])){
                                        ?>
                                        <!--<a href="reporte_capturar_asistencias.php?id=<?php echo $v->getId()?>" target="_blank"><button>Asistencias</button></a>-->
                                        <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                }
                                }
                                
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="boxUlBuscador col-md-3">
            <p>Docente<br><input type="search"  class="buscarFiltro form-control" onkeyup="buscarDocent()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p class="col-md-3" style="display: flex; align-items: center; height: 70px;"><input type="checkbox" class="form-control" id="sin_docente" style="margin-right: 10px; position: relative; top: -2px"/> Grupos sin docente</p>
        <p class="col-md-3">Ciclo<br>
          <select id="ciclo" class="form-control">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p class="col-md-3">Turno<br>
           <select id="turno" class="form-control">
              <option value="0"></option>
                <?php
                foreach($DaoTurnos->getTurnos() as $turno){
                    ?>
                    <option value="<?php echo $turno->getId()?>" ><?php echo $turno->getNombre()?></option>
                <?php
                }
                ?>
            </select>
        </p>
    </div>
    <div class="row">
        <p class="col-md-3">Oferta<br>
                <select id="oferta" class="form-control" onchange="update_curso_box_curso()">
                  <option value="0"></option>
                  <?php
                  foreach($DaoOfertas->showAll() as $k=>$v){
                  ?>
                      <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
                  <?php
                  }
                  ?>
                </select>
        </p>
        <p class="col-md-3">Especialidad:<br>
            <select id="curso" class="form-control" onchange="update_materias()">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion" class="col-md-3"></div>
        <p class="col-md-3">Materia:<br>
            <select id="lista_materias" class="form-control">
	        <option value="0"></option>
              </select>
        </p>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<?php
write_footer();


