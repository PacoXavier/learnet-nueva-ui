<?php
require_once('estandares/includes.php');
if(!isset($perm['48'])){
  header('Location: home.php');
}
require_once('clases/DaoPenalizacionDocente.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoCiclos.php');
$DaoPenalizacionDocente= new DaoPenalizacionDocente();
$DaoDocentes= new DaoDocentes();
$DaoCiclos= new DaoCiclos();
links_head("Incidencias | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-exclamation"></i> Incidencias</h1>
                </div>
                <div class="seccion">
                    <div id="box_uno">
                        <?php
                        if ($_REQUEST['id'] > 0) {
                            $docen=$DaoDocentes->show($_REQUEST['id']);
                            $nombre = $docen->getNombre_docen()." ".$docen->getApellidoP_docen()." ".$docen->getApellidoM_docen();
                        }
                        ?>
                        <ul class="form">
                            <li>Docente<br><input type="text" id="email_int"  onkeyup="buscar()" placeholder="Nombre o clave" value="<?php echo $nombre ?>"/>
                                <ul id="buscador_int"></ul>
                            </li><br>
                            <?php
                            if ($_REQUEST['id'] > 0) {
                                ?>
                                <li>Ciclo<br><select id="ciclo" onchange="mostrar()">
                                        <option value="0">Selecciona el ciclo</option>
                                        <?php
                                            foreach($DaoCiclos->showAll() as $ciclo){
                                             ?>
                                                <option value="<?php echo $ciclo->getId() ?>" <?php if($Id_ciclo==$ciclo->getId()){?> selected="selected" <?php }?> ><?php echo $ciclo->getClave() ?></option>
                                              <?php
                                              }
                                              ?>
                                        </select>
                                </li> 
                            <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <table id="usu_login">
                    <tr>
                        <td><div class="nom_usu"><div id="img_usu" 
                                                      <?php if (strlen($usu['Img_usu']) > 0) { ?> style="background-image:url(files/<?php echo $usu['Img_usu'] ?>.jpg) <?php } ?>"></div>
                                                      <?php echo $usu['Nombre_usu'] . " " . $usu['ApellidoP_usu'] . " " . $usu['ApellidoM_usu'] ?>
                                <img src="images/linea_uno.png" alt="linea_uno" width="185" height="5" class="lineas"/></div>
                        </td>
                        <td>
                            <div class="opcion">
                                <a href="perfil.php?id=<?php echo $usu['Id_usu'] ?>">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Mi perfil</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/>
                            </div>
                        </td>
                        <td><div class="opcion">
                                <a href="logout.php">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Salir</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/></div>
                        </td>
                    </tr>
                </table>
                <h2><img src="images/flecha_blue.png" alt="flecha_blue" width="6" height="11" />Opciones</h2>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_docen" value="<?php echo $_REQUEST['id']; ?>"/>
<div class="box-dias-ciclo"></div>
<?php
write_footer();
?>
<!--
<script type="text/javascript" src="js/timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/jquery.timepicker.css">
<script type="text/javascript" src="js/timepicker/lib/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/lib/bootstrap-datepicker.css">
<script type="text/javascript" src="js/timepicker/lib/site.js"></script>
<script type="text/javascript" src="js/Datepair/dist/datepair.js"></script>
<script type="text/javascript" src="js/Datepair/dist/jquery.datepair.js"></script>
-->
