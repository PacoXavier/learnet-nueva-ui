<?php
require_once('estandares/includes.php');
if (!isset($perm['64'])) {
    header('Location: home.php');
}

require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoAmonestaciones.php');
require_once('clases/modelos/base.php');
require_once('clases/modelos/Amonestaciones.php');

$base = new base();
$DaoOfertas = new DaoOfertas();
$DaoAlumnos = new DaoAlumnos();
$DaoEspecialidades = new DaoEspecialidades();
$DaoOrientaciones = new DaoOrientaciones();
$DaoOfertasAlumno = new DaoOfertasAlumno();
$DaoGrados = new DaoGrados();
$DaoDocentes = new DaoDocentes();
$DaoCiclos = new DaoCiclos();
$DaoAmonestaciones = new DaoAmonestaciones();

links_head("Amonestaciones | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-exclamation-circle"></i> Amonestaciones</h1>
                </div>
                <div class="seccion">
                    <div id="box_uno">
                        <?php
                        if ($_REQUEST['id'] > 0 && $_REQUEST['tipo'] == "alum") {
                            $tipoRecibe = "Alumno";
                            $tipo = "alum";
                            $alum = $DaoAlumnos->show($_REQUEST['id']);
                            $Id = $alum->getId();
                            $ClaveRecibe = $alum->getMatricula();
                            $NombreRecibe = $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM();
                            $cantidad = $DaoAmonestaciones->getCantidadAmonestaciones($Id, "alum");
                        } elseif ($_REQUEST['id'] > 0 && $_REQUEST['tipo'] == "usu") {
                            $tipoRecibe = "Usuario";
                            $tipo = "usu";
                            $usu_recibe = $DaoUsuarios->show($_REQUEST['id']);
                            $Id = $usu_recibe->getId();
                            $ClaveRecibe = $usu_recibe->getClave_usu();
                            $NombreRecibe = $usu_recibe->getNombre_usu() . " " . $usu_recibe->getApellidoP_usu() . " " . $usu_recibe->getApellidoM_usu();
                            $cantidad = $DaoAmonestaciones->getCantidadAmonestaciones($Id, "usu");
                        } elseif ($_REQUEST['id'] > 0 && $_REQUEST['tipo'] == "docen") {
                            $tipoRecibe = "Docente";
                            $tipo = "docen";
                            $doc = $DaoDocentes->show($_REQUEST['id']);
                            $Id = $doc->getId();
                            $ClaveRecibe = $doc->getClave_docen();
                            $NombreRecibe = $doc->getNombre_docen() . " " . $doc->getApellidoP_docen() . " " . $doc->getApellidoM_docen();
                            $cantidad = $DaoAmonestaciones->getCantidadAmonestaciones($Id, "docen");
                        }
                        ?>
                        <ul class="form">
                            <li>Nombre<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o clave" value="<?php echo $NombreRecibe ?>"/>
                                <ul id="buscador_int"></ul>
                            </li>
                        </ul>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td colspan="7" style="text-align: center;font-size: 11px;">Amonestaciones</td>
                                </tr>
                                <tr>
                                    <td>#</td>
                                    <td>Ciclo</td>
                                    <td style="width:60px;">Fecha</td>
                                    <td>Usuario reporta</td>
                                    <td style="width:225px;">Motivo</td>
                                    <td>Usuario recibe</td>
                                    <td>Acciones</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if($_REQUEST['id']>0 && strlen($_REQUEST['tipo'])>0){
                                $count = 1;
                                foreach ($DaoAmonestaciones->getAmonestacionesReportado($_REQUEST['id'], $_REQUEST['tipo']) as $k => $v) {
                                    $u = $DaoUsuarios->show($v->getId_usu());
                                    $ciclo = $DaoCiclos->show($v->getId_ciclo());

                                    if ($v->getTipo_reporta() == "usu") {
                                        $usu_reporta = $DaoUsuarios->show($v->getId_reporta());
                                        $ClaveReporta = $usu_reporta->getClave_usu();
                                        $NombreReporta = $usu_reporta->getNombre_usu() . " " . $usu_reporta->getApellidoP_usu() . " " . $usu_reporta->getApellidoM_usu();
                                    } elseif ($v->getTipo_reporta() == "docen") {
                                        $doc = $DaoDocentes->show($v->getId_reporta());
                                        $ClaveReporta = $doc->getClave_docen();
                                        $NombreReporta = $doc->getNombre_docen() . " " . $doc->getApellidoP_docen() . " " . $doc->getApellidoM_docen();
                                    }
                                    ?>
                                    <tr id_rep="<?php echo $v->getId() ?>">
                                        <td><?php echo $count ?></td>
                                        <td><?php echo $ciclo->getClave() ?></td>
                                        <td><?php echo $v->getFecha_rep() ?></td>
                                        <td><?php echo $NombreReporta ?></td>
                                        <td><?php echo $v->getMotivo() ?></td>
                                        <td><?php echo $u->getNombre_usu() . " " . $u->getApellidoP_usu() . " " . $u->getApellidoM_usu() ?></td>
                                        <td style="width:110px;">
                                            <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                                            <div class="box-buttom">
                                                <button onclick="delete_rep(<?php echo $v->getId() ?>)">Eliminar</button>
                                                <a href="acta_administrativa.php?id=<?php echo $v->getId() ?>&tipo=<?php echo $_REQUEST['tipo'] ?>" target="_blank"><button>Formato</button></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $count++;
                                }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <?php
                    if ($_REQUEST['id'] > 0) {
                        ?>
                        <li><span onclick="mostrar_box_amolestacion()">Levantar amonestaci&oacute;n </span></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_amolestado" value="<?php echo $_REQUEST['id']; ?>"/>
<input type="hidden" id="Tipo_amolestado" value="<?php echo $_REQUEST['tipo']; ?>"/>
<?php
write_footer();
?>
