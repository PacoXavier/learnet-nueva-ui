<?php
require_once('estandares/includes.php');
require_once('estandares/class_usuarios.php');
require_once('clases/DaoAlumnos.php');
require_once 'estandares/class_interesados.php';
require_once("clases/DaoUsuarios.php");
require_once('clases/DaoListaActividades.php');
require_once('clases/DaoTareasActividad.php');
require_once('clases/DaoDependencias.php');
require_once('clases/modelos/ListaActividades.php');
require_once('clases/modelos/TareasActividad.php');
require_once('clases/modelos/base.php');
require_once('clases/modelos/Dependencias.php');

links_head("Inicio");
write_head_body();
write_body();

$class_interesados = new class_interesados();
$total_alumnos = $class_interesados->get_TotalAlumnos();
$total_interesados = $class_interesados->get_TotalInteresados();


$class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
$usu= $class_usuarios->get_usu();

$Id_usuCookie=$_COOKIE['admin/Id_usu'];
$TipoUsu=$DaoUsuarios->show($Id_usuCookie);



$DaoListaActividades= new DaoListaActividades();
$DaoUsuarios= new DaoUsuarios();
$DaoTareasActividad= new DaoTareasActividad();
$DaoDependencias= new DaoDependencias();
$base= new base();

?>
<!--Calender-->
 <link rel="stylesheet" href="css/clndr.css" type="text/css" />
 <script src="js/underscore-min.js" type="text/javascript"></script>
 <script src= "js/moment-2.2.1.js" type="text/javascript"></script>
 <script src="js/clndr.js" type="text/javascript"></script>
 <script src="js/site.js" type="text/javascript"></script>
 <!--End Calender-->	

<div class="row-one">
					<div class="col-md-6 weather-grids widget-shadow">
						<div class="stats-left ">
							<h5>Total</h5>
							<h4>Interesados</h4>
						</div>
						<div class="stats-right">
							<label class="spincrement"> 
								<?php 
									echo $total_interesados;
								 ?>
							</label>
						</div>
						<div class="clearfix"> </div>	
					</div>
					
					<div class="col-md-6 weather-grids weather-right widget-shadow states-last">
						<div class="stats-left">
							<h5>Total</h5>
							<h4>Alumnos</h4>
						</div>
						<div class="stats-right">
							<label class="spincrement">
								<?php 
									echo $total_alumnos;
								 ?>
							</label>
						</div>
						<div class="clearfix"> </div>	
					</div>
					<div class="clearfix"> </div>	
				</div>
				

				<div class="row calender widget-shadow">
					<h4 class="title title_actividades">Actividades
					<div class="dropdown options">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue">3</span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="notification_header">
                                        <h3>You have 3 new notification</h3>
                                    </div>
                                </li>
                                <li><a href="#">
                                    <div class="user_img"><img src="" alt=""></div>
                                   <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet</p>
                                    <p><span>1 hour ago</span></p>
                                    </div>
                                  <div class="clearfix"></div>  
                                 </a></li>
                                 <li class="odd"><a href="#">
                                    <div class="user_img"><img src="" alt=""></div>
                                   <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet </p>
                                    <p><span>1 hour ago</span></p>
                                    </div>
                                   <div class="clearfix"></div> 
                                 </a></li>
                                 <li><a href="#">
                                    <div class="user_img"><img src="" alt=""></div>
                                   <div class="notification_desc">
                                    <p>Lorem ipsum dolor amet </p>
                                    <p><span>1 hour ago</span></p>
                                    </div>
                                   <div class="clearfix"></div> 
                                 </a></li>
                                 <li>
                                    <div class="notification_bottom">
                                        <a href="#">See all notifications</a>
                                    </div> 
                                </li>
                            </ul>
						</h4> 
						</div>
					<div class="stats-info stats-last widget-shadow">
					<table id="tabla">
					    <tr>
					        <td id="column_one">
					            <div class="fondo">
					                <div id="mascara_tabla">
					                    <ul id="lista-actividades">
					                       <?php
					                        $count=1;
					                        foreach($DaoListaActividades->getActividadesAbiertas() as $k=>$v){ 
					                            $usuario=$DaoUsuarios->show($v->getId_usu());
					                            $Id_usuAux=0;
					                            $r=0;
					                            //Visualiza solo las actividades en las que esta relacionado
					                            foreach($DaoTareasActividad->getTareasActividad($v->getId()) as $k2=>$v2){
					                                if(($v2->getId_usuRespon()==$Id_usuCookie) || ($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
					                                    $Id_usuAux=1;
					                                }
					                                $r++;
					                            }
					                            if($Id_usuAux==1 || $r==0){
					                        ?>
					                            <li id-act="<?php echo $v->getId()?>" class="actividad-li">
					                                <table class="table">
										                <thead>
										                <tr>
										                  <th>Actividad</th>
										                  <th>Fecha Creada</th>
										                  <th>Usuario</th>
										                  <th>Opciones</th>
										                </tr>
										              </thead>

					                                    <tbody>
					                                         <tr>
					                                            <td style="width: 21.875em;"><?php echo $v->getNombre();?></td>
					                                             <td><?php echo $v->getDateCreated()?></td>
					                                             <td><?php echo $usuario->getNombre_usu()." ".$usuario->getApellidoP_usu()?></td>
					                                             <td style="width: 6.250em;text-align: right;"> 
					                                                 <?php
					                                                  if(($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
					                                                 ?>
					                                                    <i class="fa fa-pencil-square-o" onclick="mostrar_box_actividad(<?php echo $v->getId()?>)"></i>
					                                                    <i class="fa fa-arrows"></i>
					                                                    <i class="fa fa-times" onclick="delete_actividad(<?php echo $v->getId()?>)"></i>
					                                                 <?php
					                                                  }
					                                                 ?>
					                                                    <i class="fa fa-minus-square-o" onclick="mostrar_actividad(this)"></i>
					                                             </td>
					                                         </tr>
					                                    </tbody>
					                                </table>

					                                <div class="box-tareas">
					                                    <div class="comentario-act"><?php echo $v->getComentarios()?></div>
					                                    <ul class="tareas">

					                                        <?php
					                                         foreach($DaoTareasActividad->getTareasActividad($v->getId()) as $k2=>$v2){
					                                             $usuario=$DaoUsuarios->show($v2->getId_usuRespon());
					                                             $classPrioridad="";
					                                             $textoPrioridad="";
					                                             if($v2->getPrioridad()=="alta"){
					                                                $classPrioridad="prioridad-red"; 
					                                                $textoPrioridad="Prioridad alta";
					                                             }elseif($v2->getPrioridad()=="media"){
					                                                $classPrioridad="prioridad-yellow";
					                                                $textoPrioridad="Prioridad media";
					                                             }elseif($v2->getPrioridad()=="baja"){
					                                                $classPrioridad="prioridad-green";
					                                                $textoPrioridad="Prioridad baja";
					                                             }elseif($v2->getPrioridad()=="ninguna"){
					                                                $classPrioridad="prioridad-none";
					                                                $textoPrioridad="Sin prioridad";
					                                             }

					                                             $fecha="";
					                                             if($v2->getStartDate()!=null && $v2->getEndDate()==null){
					                                                $fecha="(Inicia el ".$base->formatFecha($v2->getStartDate()).")";
					                                             }
					                                             if($v2->getStartDate()==null && $v2->getEndDate()!=null){
					                                                $fecha="(Termina el ".$base->formatFecha($v2->getEndDate()).")";
					                                             }

					                                             if($v2->getStartDate()!=null && $v2->getEndDate()!=null){
					                                                $fecha="(Inicia el ".$base->formatFecha($v2->getStartDate()).", termina el ".$base->formatFecha($v2->getEndDate()).")";
					                                             }
					                                             //Puede visializar solo:
					                                             //El usuario que creo la actividad
					                                             //El usuario al que le asignaron la actividad
					                                             //El que sea administrador 
					                                             //if(($v2->getId_usuRespon()==$Id_usuCookie) || ($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
					                                                 $dependencias=$DaoDependencias->getDependenciasRealizadas($v2->getId());
					                                                 $totalDependencias=count($DaoDependencias->getDependenciasTarea($v2->getId()));
					                                            ?>
					                                            <li id-tarea="<?php echo $v2->getId()?>" id-act="<?php echo  $v->getId()?>">
					                                                <table class="table">
					                                                    <tbody>
					                                                         <tr>
					                                                             <td style="width: 85px;padding-right: 0px">
					                                                                 <?php
					                                                                 $class="hidden-i";
					                                                                 if(($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
					                                                                   $class="visible-i";
					                                                                 }
					                                                                 ?>
					                                                                 <div class="box-i <?php echo $class;?>">
					                                                                    <i class="fa fa-arrows"></i>
					                                                                    <i class="fa fa-pencil-square-o" onclick="mostrar_box_tarea(<?php echo $v2->getId()?>,this,<?php echo $v->getId()?>)"></i>
					                                                                    <i class="fa fa-times" onclick="delete_tarea(<?php echo $v2->getId()?>)"></i>
					                                                                 </div>
					                                                                 <div class="box-input" <?php if($dependencias==0 && $totalDependencias>0){?> title="<?php echo $textTitle?>" <?php }?> value="<?php echo $v2->getId()?>">
					                                                                     <input type="checkbox" <?php if($v2->getStatus()==1){ ?> checked="checked" <?php }?> 
					                                                                         <?php 
					                                                                         //El usuario puede visualizar todas las tareas aunque no sea el usuario asignado
					                                                                         //Pero solo podra dar click en las que este asignado
					                                                                         if(($dependencias==0 && $totalDependencias>0) || (($v2->getId_usuRespon()!=$Id_usuCookie &&  $v->getId_usu()!=$Id_usuCookie) && (!in_array($Id_usuCookie, $UsuariosPermitidos)))){
					                                                                         ?> 
					                                                                            disabled="disabled" 
					                                                                         <?php 
					                                                                         }
					                                                                         ?> 
					                                                                            onchange="tareaStatus(this)" 
					                                                                            data-id="<?php echo $v2->getId()?>"
					                                                                       >
					                                                                 </div>
					                                                             </td>
					                                                             <td><div class="asignado"><?php echo $usuario->getNombre_usu()." ".$usuario->getApellidoP_usu()?></div>
					                                                                 <div class="comentarios-tarea"><?php echo $v2->getComentario()?></div>
					                                                                 <div class="mas">
					                                                                     <span onclick="mostrar_comentario(this)"> <?php echo strtolower("m&aacute;s")?> </span> 
					                                                                     <i class="fa fa-exclamation-circle <?php echo $classPrioridad?>" title="<?php echo $textoPrioridad;?>"></i>
					                                                                     <span class="fechas"><?php echo $fecha;?></span>
					                                                                 </div>
					                                                             </td>
					                                                         </tr>
					                                                         
					                                                    </tbody>
					                                                </table> 
					                                                <div class="comentarios-tarea-completo">
					                                                    <p><?php echo $v2->getComentario()?></p>
					                                                    <?php
					                                                      if(strlen($v2->getComentario_realizo())>0){
					                                                    ?>
					                                                    <br>
					                                                    <p><b>¿C&oacute;mo se completo?</b></p> 
					                                                    <p><?php echo $v2->getComentario_realizo()?></p>
					                                                    <?php
					                                                      }
					                                                    ?>
					                                                </div>
					                                            </li>
					                                        <?php
					                                             //}
					                                        }
					                                        if(($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
					                                        ?>
					                                            <li><span class="button-tarea" onclick="mostrar_box_tarea(0,this,<?php echo $v->getId()?>)"><i class="fa fa-plus"></i> Añadir tarea</span></li>
					                                            <?php
					                                        }
					                                        ?>
					                                    </ul>
					                                </div>
					                            </li>
					                       <?php
					                            }
					                            
					                       $count++; 
					                       }
					                       //print_r($UsuariosPermitidos);
					                       ?>   
					                    </ul>

					                </div>
					            </div>
					        </td>
					    </tr>
					</table>
							
					</div>
				</div>
				<div class="row calender widget-shadow">
					<h4 class="title">Eventos</h4>
					<div class="cal1">
						
					</div>
				</div>
				<div class="clearfix"> </div>


		<script type="text/javascript">
			$(document).ready(function (){
				$('.spincrement').spincrement({
					from: 0,
					duration: 3000,
					fade: true
				});
			});
		</script>

<script type="text/javascript" src="js/actividades.js"></script>
<?php
write_footer();
