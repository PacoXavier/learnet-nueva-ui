<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoGrados= new DaoGrados();
$DaoMediosEnterar= new DaoMediosEnterar();
$DaoTurnos= new DaoTurnos();

links_head("Alumnos | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-user"></i> Alumnos</h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul class="row list-design">
                        <li class="col-md-4 widget" onclick="mostrar_filtro()"><div class="stats-left"><i class="fa fa-filter"></i> Filtros</div></li>
                        <li class="col-md-4 widget states-mdl" onclick="mostrar_box_email()"><div class="stats-left"><i class="fa fa-envelope"></i> Email</div></li>
                        <li class="col-md-4 widget states-last" onclick="download_excel()"><div class="stats-left"><i class="fa fa-download"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td>Email</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td>Grado</td>
                                <td>Opcion de pago</td>
                                <td>Fecha. inte</td>
                                <td>Medio</td>
                                <td>Turno</td>
                                <td><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                                <td>Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count=1;
                            foreach ($DaoAlumnos->getAlumnos() as $k=>$v){
                               $nombre_ori="";
                               $oferta = $DaoOfertas->show($v['Id_ofe']);
                               $esp = $DaoEspecialidades->show($v['Id_esp']);
                               if ($v['Id_ori'] > 0) {
                                  $ori = $DaoOrientaciones->show($v['Id_ori']);
                                  $nombre_ori = $ori->getNombre();
                                }
                                $opcion="Plan por materias"; 
                                if($v['Opcion_pago']==2){
                                  $opcion="Plan completo";  
                                }

                                $grado = $DaoOfertasAlumno->getLastGradoOferta($v['Id_ofe_alum']);

                                $MedioEnt="";
                                if($v['Id_medio_ent']>0){
                                  $medio=$DaoMediosEnterar->show($v['Id_medio_ent']);
                                  $MedioEnt=$medio->getMedio();
                                }

                                $tur = $DaoTurnos->show($v['Turno']);
                                $turno=$tur->getNombre();
                                 
                                 ?>
                                         <tr id_alum="<?php echo $v['Id_ins'];?>" id="<?php echo $v['Id_ofe_alum'];?>">
                                           <td><?php echo $count;?></td>
                                           <td><?php echo $v['Matricula'] ?></td>
                                           <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                                           <td><?php echo $v['Email_ins']?></td>
                                           <td><?php echo $oferta->getNombre_oferta() ?></td>
                                           <td><?php echo $esp->getNombre_esp(); ?></td>
                                           <td><?php echo $nombre_ori; ?></td>
                                           <td><?php echo $grado['Grado']; ?></td>
                                           <td><?php echo $opcion; ?></td>
                                            <td><?php echo $v['Fecha_ins']; ?></td>
                                            <td><?php echo $MedioEnt; ?></td>
                                            <td><?php echo $turno;?></td>
                                            <td><input type="checkbox"> </td>
                                            <td>
                                                   <?php
                                                    if($oferta->getTipoOferta()==1){
                                                        ?>
                                                         <a href="pago.php?id=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button>Pagos</button></a>
                                                        <?php
                                                    }elseif($oferta->getTipoOferta()==2){
                                                        ?>
                                                         <a href="cargos_curso.php?id=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button>Pagos</button></a>
                                                        <?php   
                                                    }
                                                    ?>
                                            </td>
                                         </tr>
                                         <?php
                                         $count++;
                                }
                                  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <p class="col-md-3">Oferta<br>
                <select id="oferta" class="form-control" onchange="update_curso_box_curso()">
                  <option value="0"></option>
                  <?php
                  foreach($DaoOfertas->showAll() as $k=>$v){
                  ?>
                      <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
                  <?php
                  }
                  ?>
                </select>
        </p>
        <p class="col-md-3">Especialidad:<br>
            <select id="curso" class="form-control" onchange="update_orientacion_box_curso();update_grados_ofe()">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion" class="col-md-3"></div>
        <p class="col-md-3">Grado:<br>
            <select id="grado" class="form-control">
              <option value="0"></option>
            </select>
        </p>
    </div>
    <div class="row">
        <p class="col-md-4">Opci&oacute;n de pago:<br>
            <select id="opcion" class="form-control">
              <option value="0"></option>
              <option value="1">POR MATERIAS</option>
              <option value="2">PLAN COMPLETO</option>
            </select>
        </p>
        <p class="col-md-4">Medio por el que se entero:<br>
            <select id="medio_entero" class="form-control" >
              <option value="0"></option>
              <?php
                foreach($DaoMediosEnterar->showAll() as $k2=>$v2){
                    ?>
                    <option value="<?php echo $v2->getId() ?>"><?php echo $v2->getMedio() ?></option>
                    <?php  
                }
                ?>
            </select>
        </p>
        <p class="col-md-4">Turno<br>
            <select id="turno" class="form-control">
                <option value="0"></option>
                <?php
                foreach($DaoTurnos->getTurnos() as $turno){
                    ?>
                    <option value="<?php echo $turno->getId()?>" ><?php echo $turno->getNombre()?></option>
                <?php
                }
                ?>
            </select>
        </p>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<script src="js/ckeditor/ckeditor.js"></script>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();
