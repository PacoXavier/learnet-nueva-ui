<?php
require_once('estandares/includes.php');
require_once('estandares/class_activos.php');
require_once('estandares/class_docentes.php');

links_head("Historial mantenimiento | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1>Historial de mantenimiento</h1>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Usuario</td>
                                <td>Fecha de mantenimiento</td>
                                <td>Estado</td>
                                <td>Comentarios</td>
                                <td>Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count=1;
                            $class_activos = new class_activos($_REQUEST['id']);
                            $activo=$class_activos->get_activo();
                            foreach ($activo['Historial'] as $k => $v) {
                                
                                if($v['Tipo_usu']==1){
                                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                                     $user=$class_usuarios->get_usu();
                                     $nombre=$user['Nombre_usu']." ".$user['ApellidoP_usu']." ".$user['ApellidoM_usu'];
                                }else{
                                     $class_docentes = new class_docentes($_COOKIE['admin/Id_usu']);
                                     $docente=$class_docentes->get_docente();
                                     $nombre=$docente['Nombre_docen']." ".$docente['ApellidoP_docen']." ".$docente['ApellidoM_docen']; 
                                }
                                ?>
                            
                                <tr>
                                    <td><?php echo $count ?></td>
                                    <td><?php echo $nombre ?></td>
                                    <td><?php echo $v['Fecha_hist'] ?></td>
                                     <td><?php echo $v['Estado'] ?></td>
                                    <td><?php echo $v['Comentarios'] ?></td>
                                    <td><button onclick="delete_hist(<?php echo $v['Id_hist'] ?>)">Eliminar</button></td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <table id="usu_login">
                    <tr>
                        <td><div class="nom_usu"><div id="img_usu" 
                                                      <?php if (strlen($usu['Img_usu']) > 0) { ?> style="background-image:url(files/<?php echo $usu['Img_usu'] ?>.jpg) <?php } ?>"></div>
                                                      <?php echo $usu['Nombre_usu'] . " " . $usu['ApellidoP_usu'] . " " . $usu['ApellidoM_usu'] ?>
                                <img src="images/linea_uno.png" alt="linea_uno" width="185" height="5" class="lineas"/></div>
                        </td>
                        <td>
                            <div class="opcion">
                                <a href="perfil.php?id=<?php echo $usu['Id_usu'] ?>">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Mi perfil</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/>
                            </div>
                        </td>
                        <td><div class="opcion">
                                <a href="logout.php">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Salir</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/></div>
                        </td>
                    </tr>
                </table>
                <h2><img src="images/flecha_blue.png" alt="flecha_blue" width="6" height="11" />Opciones Activos</h2>
                <ul>
                   
                    <li><span onclick="box_captura()">Capturar mantenimiento</span></li>
                    <?php
            if($_REQUEST['tipo']==1){
             $url="activos";
            }else{
             $url="activo";  
            }
       ?>
                     <li><a href="<?php echo $url?>.php?id=<?php echo $_REQUEST['id']?>" class="link">Regresar</a></li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<input type="hidden" id="Id_activo" value="<?php echo $_REQUEST['id'] ?>"/>
<?php
write_footer();
