 <?php
require_once('estandares/includes.php');
require_once('clases/DaoCiclos.php');
$DaoCiclos=new DaoCiclos();
$ciclo=$DaoCiclos->getActual();
//http://fullcalendar.io/download/
links_head("Eventos | ULM");
?>
<link rel="stylesheet" href="js/fullcalendar/lib/cupertino/jquery-ui.min.css">
<link rel='stylesheet' href='js/fullcalendar/fullcalendar.css' />
<link href="js/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">
<?php
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Eventos</h1>
                </div>
                <div id="mascara_tabla">
                    <div id="calendar" class="fc fc-ltr fc-unthemed"></div>
                </div>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_ciclo_eventos" value="<?php echo $ciclo->getId()?>"/>
<?php

write_footer();
?>

<script src="js/fullcalendar/lib/moment.min.js"></script>
<!--<script src="js/fullcalendar/lib/jquery.min.js"></script>-->
<script src="js/fullcalendar/fullcalendar.min.js"></script>
<script src='js/fullcalendar/fullcalendar.js'></script>
<script src='js/fullcalendar/lang/es.js'></script>

<!--
http://jonthornton.github.io/jquery-timepicker/
http://jonthornton.github.io/Datepair.js/
-->

<script type="text/javascript" src="js/timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/jquery.timepicker.css">
<script type="text/javascript" src="js/timepicker/lib/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/lib/bootstrap-datepicker.css">
<script type="text/javascript" src="js/timepicker/lib/site.js"></script>
<script type="text/javascript" src="js/Datepair/dist/datepair.js"></script>
<script type="text/javascript" src="js/Datepair/dist/jquery.datepair.js"></script>

