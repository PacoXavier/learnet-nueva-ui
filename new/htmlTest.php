<?php
require_once('estandares/includes.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoCiclos.php');
require_once('estandares/class_docentes.php');
$DaoDocentes= new DaoDocentes();
$DaoCiclos= new DaoCiclos();

links_head("Evaluaci&oacute;n Docente | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <?php
    if ($_REQUEST['id_doc'] > 0) {
        $docente = $DaoDocentes->show($_REQUEST['id_doc']);
        $class_docentes= new class_docentes($_REQUEST['id_doc']);
        if ($_REQUEST['id']) {
            
            $evaluacion = $class_docentes->get_evaluacion($_REQUEST['id']);
        }else{
            
            $evaluacion = $class_docentes->get_evaluacion_ultima_eva_docente($_REQUEST['id_doc']);
        }
    }
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1>Evaluaci&oacute;n Docente</h1>
                </div>
                <?php
                if ($_REQUEST['id_doc'] > 0) {
                    ?>
                    <div class="seccion">
                        <h2 style="font-size: 15px;margin-bottom: 15px;"><?php echo $docente->getClave_docen() . "  - " . $docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . " " . $docente->getApellidoM_docen() ?></h2>
                        <ul class="form">
                            <li>Ciclo:<br>
                                <select id="Id_ciclo">
                                    <option value="0"></option>
                                    <?php
                                    foreach ($DaoCiclos->showAll() as $k => $v) {
                                        ?>
                                        <option value="<?php echo $v->getId()?>" <?php if ($evaluacion['Id_ciclo'] == $v->getId() && $_REQUEST['id']>0) { ?> selected="selected" <?php } ?> ><?php echo $v->getClave() ?></option>
                                        <?php
                                    }
                                    ?>
                                </select></li>
                        </ul>
                        <h2>Expediente</h2>
                        <span class="linea"></span>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>CV</td>
                                    <td>IFE</td>
                                    <td>RFC</td>
                                    <td>CURP</td>
                                    <td>FOTOS</td>
                                    <td>C. DOMI.</td>
                                    <td class="puntos"> </td>
                                    <td>Com. Estudios</td>
                                    <td>Técnico Vali</td>
                                    <td class="puntos">Puntos</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <?php
                                    $total_expendiente = 0;
                                    ?>
                                    <td><input type="checkbox" class="cv" value="1.6" onchange="update_expediente(this)" <?php if ($evaluacion['CV'] == 1) {
                                    $total_expendiente+=1.6; ?> checked="checked"<?php } ?> /></td>
                                    <td><input type="checkbox" class="ife" value="1.6" onchange="update_expediente(this)" <?php if ($evaluacion['IFE'] == 1) {
                                    $total_expendiente+=1.6; ?> checked="checked"<?php } ?> /></td>
                                    <td><input type="checkbox" class="rfc" value="1.6" onchange="update_expediente(this)" <?php if ($evaluacion['RFC'] == 1) {
                                    $total_expendiente+=1.6; ?> checked="checked"<?php } ?> /></td>
                                    <td><input type="checkbox" class="curp" value="1.6" onchange="update_expediente(this)" <?php if ($evaluacion['CURP'] == 1) {
                                    $total_expendiente+=1.6; ?> checked="checked"<?php } ?> /></td>
                                    <td><input type="checkbox" class="fotos" value="1.6" onchange="update_expediente(this)" <?php if ($evaluacion['Fotos'] == 1) {
                                    $total_expendiente+=1.6; ?> checked="checked"<?php } ?> /></td>
                                    <td><input type="checkbox" class="com_domi" value="1.6" onchange="update_expediente(this)" <?php if ($evaluacion['Comp_domicilio'] == 1) {
                                    $total_expendiente+=1.6; ?> checked="checked"<?php } ?> /></td>
                                    <td class="puntos"><span class="puntos_exp_uno"><?php echo round($total_expendiente); ?></span></td>
                                    <td><input type="checkbox" class="com_est" value="10" onchange="update_expediente(this)" <?php if ($evaluacion['Comp_estudios'] == 1) {
                                    $total_expendiente+=10; ?> checked="checked"<?php } ?> /></td>
                                    <td><input type="checkbox" class="tec_valid" value="10" onchange="update_expediente(this)" <?php if ($evaluacion['Tecnico_val'] == 1) {
                                    $total_expendiente+=10; ?> checked="checked"<?php } ?> /></td>
                                    <td class="puntos"><span class="puntos_exp_dos"><?php echo round($total_expendiente); ?></span></td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" value="<?php echo round($total_expendiente); ?>" id="puntos_expediente"/>
                    </div>
                    <div class="seccion">
                        <h2>Desempe&ntilde;o Acad&eacute;mico</h2>
                        <span class="linea"></span>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>Planeaciones</td>
                                    <td>Asistencias</td>
                                    <td>Puntualidad</td>
                                    <td>Encuestas</td>
                                    <td>Evaluación Interna</td>
                                    <td>Juntas Academia.</td>
                                    <td class="puntos">Puntos</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
    <?php
    $total_academico = $evaluacion['Planeaciones'] + $evaluacion['Asistencias'] + $evaluacion['Puntualidad'] + $evaluacion['Encuestas'] + $evaluacion['Eva_int'] + $evaluacion['Juntas'];
    ?>
                                    <td><input type="text" class="planeaciones" onkeyup="update_desempenio(this)" value="<?php echo $evaluacion['Planeaciones'] ?>"/></td>
                                    <td><input type="text" class="asistencias" onkeyup="update_desempenio(this)" value="<?php echo $evaluacion['Asistencias'] ?>"/></td>
                                    <td><input type="text" class="puntualidad" onkeyup="update_desempenio(this)" value="<?php echo $evaluacion['Puntualidad'] ?>"/></td>
                                    <td><input type="text" class="encuestas" onkeyup="update_desempenio(this)" value="<?php echo $evaluacion['Encuestas'] ?>"/></td>
                                    <td><input type="text" class="eval_int" onkeyup="update_desempenio(this)" value="<?php echo $evaluacion['Eva_int'] ?>"/></td>
                                    <td><input type="text" class="juntas_academicas" onkeyup="update_desempenio(this)" value="<?php echo $evaluacion['Juntas'] ?>"/></td>
                                    <td class="puntos">
                                        <span class="desempenio_span" onkeyup="update_desempenio(this)">
    <?php echo $total_academico; ?>
                                        </span></td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" value="<?php echo round($total_academico); ?>" id="puntos_desempenio_academico"/>
                    </div>
                    <div class="seccion">
                        <h2>Capacitaci&oacute;n</h2>
                        <span class="linea"></span>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>T&iacute;tulo o C&eacute;dula</td>
                                    <td>Equivalencia</td>
                                    <td>Maestr&iacute;a</td>
                                    <td>Capacitaci&oacute;n Interna</td>
                                    <td>Capacitaci&oacute;n Externa</td>
                                    <td>Cursos a fin</td>
                                    <td class="puntos">Puntos</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
    <?php
    $total_capacitacion = $evaluacion['Cap_int'] + $evaluacion['Cap_ext'] + $evaluacion['Cursos_afin'];
    ?>
                                    <td><input type="checkbox" class="titulo_cedula" value="20" onchange="update_capacitacion()" <?php if ($evaluacion['Titulo_cedula'] == 1) {
                                            $total_capacitacion+=20; ?> checked="checked"<?php } ?> /></td>
                                    <td><input type="checkbox" class="equivalencia" value="10"  onchange="update_capacitacion()" <?php if ($evaluacion['Equivalencia'] == 1) {
                                            $total_capacitacion+=10; ?> checked="checked"<?php } ?> /></td>
                                    <td><input type="checkbox" class="maestria" value="30"  onchange="update_capacitacion()" <?php if ($evaluacion['Maestria'] == 1) {
                                            $total_capacitacion+=30; ?> checked="checked"<?php } ?> /></td>
                                    <td><input type="text" class="cap_int"  onkeyup="update_capacitacion()" value="<?php echo $evaluacion['Cap_int'] ?>"/></td>
                                    <td><input type="text" class="cap_ext" onkeyup="update_capacitacion()" value="<?php echo $evaluacion['Cap_ext'] ?>"/></td>
                                    <td><input type="text" class="cursos_afin" onkeyup="update_capacitacion()" value="<?php echo $evaluacion['Cursos_afin'] ?>"/></td>
                                    <td class="puntos"><span class="capacitacion_span">
    <?php echo $total_capacitacion; ?>
                                        </span></td>
                                </tr>
                            </tbody>
                        </table>
                        <input type="hidden" value="<?php echo round($total_capacitacion); ?>" id="puntos_capacitacion"/>
                    </div>
                    <div class="seccion">
                        <h2>Cualitativos</h2>
                        <span class="linea"></span>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>Libros o Manuales</td>
                                    <td>Discos</td>
                                    <td>Ponente</td>
                                    <td>Capacitador Externo</td>
                                    <td>Investigador Academico</td>
                                    <td>Presentaciones Profesionales</td>
                                    <td class="puntos">Puntos</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
    <?php
    $total_cualitativos = $evaluacion['Libros'] + $evaluacion['Discografia'] + $evaluacion['Ponente'] + $evaluacion['Capacitador_externo'] + $evaluacion['Inv_academico']+$evaluacion['Presentaciones_profecionales'];
    ?>
                                    <td><input type="text" class="libros_m" onkeyup="update_cualitativos()" value="<?php echo $evaluacion['Libros'] ?>"/></td>
                                    <td><input type="text" class="discos" onkeyup="update_cualitativos()" value="<?php echo $evaluacion['Discografia'] ?>"/></td>
                                    <td><input type="text" class="ponente" onkeyup="update_cualitativos()" value="<?php echo $evaluacion['Ponente'] ?>"/></td>
                                    <td><input type="text" class="capacitador_ext" onkeyup="update_cualitativos()" value="<?php echo $evaluacion['Capacitador_externo'] ?>"/></td>
                                    <td><input type="text" class="inv_academico" onkeyup="update_cualitativos()" value="<?php echo $evaluacion['Inv_academico'] ?>"/></td>
                                    <td><input type="text" class="presentaciones_profecionales" onkeyup="update_cualitativos()" value="<?php echo $evaluacion['presentaciones_profecionales'] ?>"/></td>
                                    <td class="puntos"><span class="cualitativos_span">
    <?php echo $total_cualitativos; ?>
                                        </span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="seccion">
                        <h2>Comentarios</h2>
                        <span class="linea"></span>
                        <textarea id="comentarios"><?php echo $evaluacion['Comentarios'] ?></textarea></p>
                        <input type="hidden" value="<?php echo round($total_cualitativos); ?>" id="puntos_cualitativos"/>
                        <button id="button_ins" onclick="save_evaluacion()">Guardar</button>
                    </div>
    <?php
}
?>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <table id="usu_login">
<?php
$class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
$usu = $class_usuarios->get_usu();
?>
                    <tr>
                        <td><div class="nom_usu"><div id="img_usu" 
<?php if (strlen($usu['Img_usu']) > 0) { ?> style="background-image:url(files/<?php echo $usu['Img_usu'] ?>.jpg) <?php } ?>"></div>
<?php echo $usu['Nombre_usu'] . " " . $usu['ApellidoP_usu'] . " " . $usu['ApellidoM_usu'] ?>
                                <img src="images/linea_uno.png" alt="linea_uno" width="185" height="5" class="lineas"/></div>
                        </td>
                        <td>
                            <div class="opcion">
                                <a href="perfil.php?id=<?php echo $usu['Id_usu'] ?>">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Mi perfil</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/>
                            </div>
                        </td>
                        <td><div class="opcion">
                                <a href="logout.php">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Salir</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/></div>
                        </td>
                    </tr>
                </table>
                <h2><img src="images/flecha_blue.png" alt="flecha_blue" width="6" height="11" />Puntajes</h2>
                <ul>
                    <?php
                    $total = $total_expendiente + $total_academico + $total_capacitacion + $total_cualitativos;
                    //Profesor A
                    $query_profesorA_x = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=1 AND Nombre_nivel='Nivel 1'";
                    $profesorA_x = mysql_query($query_profesorA_x, $cnn) or die(mysql_error());
                    $row_profesorA_x = mysql_fetch_array($profesorA_x);

                    $query_profesorA_a = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=1 AND Nombre_nivel='Nivel 2'";
                    $profesorA_a = mysql_query($query_profesorA_a, $cnn) or die(mysql_error());
                    $row_profesorA_a = mysql_fetch_array($profesorA_a);

                    $query_profesorA_b = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=1 AND Nombre_nivel='Nivel 3'";
                    $nivel_profesorA_b = mysql_query($query_profesorA_b, $cnn) or die(mysql_error());
                    $row_profesorA_b = mysql_fetch_array($nivel_profesorA_b);

                    $query_profesorA_c = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=1 AND Nombre_nivel='Nivel 4'";
                    $profesorA_c = mysql_query($query_profesorA_c, $cnn) or die(mysql_error());
                    $row_profesorA_c = mysql_fetch_array($profesorA_c);
                    //140
                    if ($total < $row_profesorA_a['Puntos_nivel']) {
                        $nivel_a = "Nivel 1";
                        $nivel_a_input = $row_profesorA_x['Id_nivel'];
                        //140 && 160
                    } elseif ($total >= $row_profesorA_a['Puntos_nivel'] && $total < $row_profesorA_b['Puntos_nivel']) {
                        $nivel_a = "Nivel 2";
                        $nivel_a_input = $row_profesorA_a['Id_nivel'];
                        //160 && 190
                    } elseif ($total >= $row_profesorA_b['Puntos_nivel'] && $total < $row_profesorA_c['Puntos_nivel']) {
                        $nivel_a = "Nivel 3";
                        $nivel_a_input = $row_profesorA_b['Id_nivel'];
                    } elseif ($total >= $row_profesorA_c['Puntos_nivel']) {
                        $nivel_a = "Nivel 4";
                        $nivel_a_input = $row_profesorA_c['Id_nivel'];
                    }

                    //Profesor B
                    $query_profesorB_x = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=2 AND Nombre_nivel='Nivel 1'";
                    $profesorB_x = mysql_query($query_profesorB_x, $cnn) or die(mysql_error());
                    $row_profesorB_x = mysql_fetch_array($profesorB_x);

                    $query_profesorB_a = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=2 AND Nombre_nivel='Nivel 2'";
                    $profesorB_a = mysql_query($query_profesorB_a, $cnn) or die(mysql_error());
                    $row_profesorB_a = mysql_fetch_array($profesorB_a);

                    $query_profesorB_b = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=2 AND Nombre_nivel='Nivel 3'";
                    $nivel_profesorB_b = mysql_query($query_profesorB_b, $cnn) or die(mysql_error());
                    $row_profesorB_b = mysql_fetch_array($nivel_profesorB_b);

                    $query_profesorB_c = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=2 AND Nombre_nivel='Nivel 4'";
                    $profesorB_c = mysql_query($query_profesorB_c, $cnn) or die(mysql_error());
                    $row_profesorB_c = mysql_fetch_array($profesorB_c);
                    //160
                    if ($total < $row_profesorB_a['Puntos_nivel']) {
                        $nivel_b = "Nivel 1";
                        $nivel_b_input = $row_profesorB_x['Id_nivel'];
                        //160 && 180
                    } elseif ($total >= $row_profesorB_a['Puntos_nivel'] && $total < $row_profesorB_b['Puntos_nivel']) {
                        $nivel_b = "Nivel 2";
                        $nivel_b_input = $row_profesorB_a['Id_nivel'];
                        //180 && 210
                    } elseif ($total >= $row_profesorB_b['Puntos_nivel'] && $total < $row_profesorB_c['Puntos_nivel']) {
                        $nivel_b = "Nivel 3";
                        $nivel_b_input = $row_profesorB_b['Id_nivel'];
                    } elseif ($total >= $row_profesorB_c['Puntos_nivel']) {
                        $nivel_b = "Nivel 4";
                        $nivel_b_input = $row_profesorB_c['Id_nivel'];
                    }

                    //Profesor C
                    $query_profesorC_x = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=3 AND Nombre_nivel='Nivel 1'";
                    $profesorC_x = mysql_query($query_profesorC_x, $cnn) or die(mysql_error());
                    $row_profesorC_x = mysql_fetch_array($profesorC_x);

                    $query_profesorC_a = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=3 AND Nombre_nivel='Nivel 2'";
                    $profesorC_a = mysql_query($query_profesorC_a, $cnn) or die(mysql_error());
                    $row_profesorC_a = mysql_fetch_array($profesorC_a);

                    $query_profesorC_b = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=3 AND Nombre_nivel='Nivel 3'";
                    $nivel_profesorC_b = mysql_query($query_profesorC_b, $cnn) or die(mysql_error());
                    $row_profesorC_b = mysql_fetch_array($nivel_profesorC_b);

                    $query_profesorC_c = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=3 AND Nombre_nivel='Nivel 4'";
                    $profesorC_c = mysql_query($query_profesorC_c, $cnn) or die(mysql_error());
                    $row_profesorC_c = mysql_fetch_array($profesorC_c);
                    //160
                    if ($total < $row_profesorC_a['Puntos_nivel']) {
                        $nivel_c = "Nivel 1";
                        $nivel_c_input = $row_profesorC_x['Id_nivel'];
                        //160 && 180
                    } elseif ($total >= $row_profesorC_a['Puntos_nivel'] && $total < $row_profesorC_b['Puntos_nivel']) {
                        $nivel_c = "Nivel 2";
                        $nivel_c_input = $row_profesorC_a['Id_nivel'];
                        //180 && 210
                    } elseif ($total >= $row_profesorC_b['Puntos_nivel'] && $total < $row_profesorC_c['Puntos_nivel']) {
                        $nivel_c = "Nivel 3";
                        $nivel_c_input = $row_profesorC_b['Id_nivel'];
                    } elseif ($total >= $row_profesorC_c['Puntos_nivel']) {
                        $nivel_c = "Nivel 4";
                        $nivel_c_input = $row_profesorC_c['Id_nivel'];
                    }

                    //Profesor D
                    $query_profesorD_x = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=4 AND Nombre_nivel='Nivel 1'";
                    $profesorD_x = mysql_query($query_profesorD_x, $cnn) or die(mysql_error());
                    $row_profesorD_x = mysql_fetch_array($profesorD_x);

                    $query_profesorD_a = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=4 AND Nombre_nivel='Nivel 2'";
                    $profesorD_a = mysql_query($query_profesorD_a, $cnn) or die(mysql_error());
                    $row_profesorD_a = mysql_fetch_array($profesorD_a);

                    $query_profesorD_b = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=4 AND Nombre_nivel='Nivel 3'";
                    $nivel_profesorD_b = mysql_query($query_profesorD_b, $cnn) or die(mysql_error());
                    $row_profesorD_b = mysql_fetch_array($nivel_profesorD_b);

                    $query_profesorD_c = "SELECT * FROM Niveles_docentes WHERE Id_tipo_nivel=4 AND Nombre_nivel='Nivel 4'";
                    $profesorD_c = mysql_query($query_profesorD_c, $cnn) or die(mysql_error());
                    $row_profesorD_c = mysql_fetch_array($profesorD_c);
                    //160
                    if ($total < $row_profesorD_a['Puntos_nivel']) {
                        $nivel_d = "Nivel 1";
                        $nivel_d_input = $row_profesorD_x['Id_nivel'];
                        //160 && 180
                    } elseif ($total >= $row_profesorD_a['Puntos_nivel'] && $total < $row_profesorD_b['Puntos_nivel']) {
                        $nivel_d = "Nivel 2";
                        $nivel_d_input = $row_profesorD_a['Id_nivel'];
                        //180 && 210
                    } elseif ($total >= $row_profesorD_b['Puntos_nivel'] && $total < $row_profesorD_c['Puntos_nivel']) {
                        $nivel_d = "Nivel 3";
                        $nivel_d_input = $row_profesorD_b['Id_nivel'];
                    } elseif ($total >= $row_profesorD_c['Puntos_nivel']) {
                        $nivel_d = "Nivel 4";
                        $nivel_d_input = $row_profesorD_c['Id_nivel'];
                    }
                    ?>
                    <li><span>Puntos:   <b id="puntos_totales"><?php echo round($total); ?> </b></span></li>
                    <li><span>Profesor A   <b><?php echo $nivel_a; ?></b></span><input type="hidden" id="nivel_profesor_a" value="<?php echo $nivel_a_input ?>"/></li>

                    <li><span>Profesor B   <b><?php echo $nivel_b; ?></b></span><input type="hidden" id="nivel_profesor_b" value="<?php echo $nivel_b_input ?>"/></li>
                    <li><span>Profesor C   <b><?php echo $nivel_c; ?></b></span><input type="hidden" id="nivel_profesor_c" value="<?php echo $nivel_c_input ?>"/></li>
                    <li><span>Profesor D   <b><?php echo $nivel_d; ?></b></span><input type="hidden" id="nivel_profesor_d" value="<?php echo $nivel_d_input ?>"/></li>
<?php
if ($_REQUEST['id_doc'] > 0) {
    ?>
                        <li><a href="evaluaciones_docente.php?id=<?php echo $_REQUEST['id_doc']; ?>">Regresar</a></li>
    <?php
}
?>
                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_docente" value="<?php echo $_REQUEST['id_doc']; ?>"/>
<input type="hidden" id="Id_eva" value="<?php echo $_REQUEST['id'] ?>"/>
<input type="hidden" id="puntos_totales_evaluacion" value="<?php echo round($total); ?>"/>
<?php
write_footer();
?>
