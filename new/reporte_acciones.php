<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoAccionesInteresado.php');
require_once('clases/modelos/AccionesInteresado.php');
require_once('clases/modelos/Alumnos.php');

$DaoAccionesInteresado= new DaoAccionesInteresado();
$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();

links_head("Acciones | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Acciones</h1>
                </div>
                <div class="box-filter-reportes">
                <ul class="row list-design" >
                    <li class="col-xs-6 weather-grids widget-shadow" onclick="mostrar_filtro()"><div class="stats-left text-white"><i class="fa fa-filter" style="margin-right: 5px;"></i> Filtros</div></li>
                    <li class="col-xs-6 weather-grids weather-right widget-shadow states-last" onclick="download_excel()"><div class="stats-left text-white"><i class="fa fa-download" style="margin-right: 5px;"></i> Descargar</div></li>
                </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Acci&oacute;n</td>
                                <td>Nombre</td>
                                <td>Oferta</td>
                                <td style="width: 200px;">Especialidad</td>
                                <td>Orientaci&oacute;n</td>
                                <td>Fecha acci&oacute;n</td>
                                <td>Fecha cita</td>
                                <td>Usuario</td>
                                <td>Asistio</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($DaoAccionesInteresado->showAll() as $k=>$v){
                                    $int=$DaoAlumnos->show($v->getId_int());
                                    $ofe_alum=$DaoOfertasAlumno->show($v->getId_ofe_alum());
                                    if($ofe_alum->getId_ofe()>0){
                                    $oferta=$DaoOfertas->show($ofe_alum->getId_ofe());
                                    $esp = $DaoEspecialidades->show($ofe_alum->getId_esp());
                                    $nombre_ori="";
                                    if ($ofe_alum->getId_ori() > 0) {
                                      $ori = $DaoOrientaciones->show($ofe_alum->getId_ori());
                                      $nombre_ori = $ori->getNombre();
                                    }
                                    
                                    $usuAtendio=$DaoUsuarios->show($v->getUsuario());
                                    $TipoAccion="";
                                    if($v->getTipoAccion()=="1"){
                                       $TipoAccion="Examen"; 
                                    }elseif($v->getTipoAccion()=="2"){
                                       $TipoAccion="Sesi&oacuten informativa";  
                                    }elseif($v->getTipoAccion()=="2"){
                                        $TipoAccion="Clase de muestra"; 
                                    }
                                    
                                ?>
                            
                                <tr>
                                    <td><?php echo $TipoAccion?> </td>
                                    <td><?php echo $int->getNombre()." ".$int->getApellidoP()." ".$int->getApellidoM()?> </td>
                                    <td><?php echo  $oferta->getNombre_oferta(); ?></td>
                                    <td><?php echo $esp->getNombre_esp(); ?></td>
                                    <td><?php echo $nombre_ori; ?></td>
                                    <td><?php echo $v->getFechaAccion();?></td>
                                    <td><?php echo $v->getFechaSession()?></td>
                                    <td><?php echo $usuAtendio->getNombre_usu." ".$usuAtendio->getApellidoP_usu()." ".$usuAtendio->getApellidoM_usu()?></td>
                                    <td><button class="btn btn-info" onclick="save_asistio(<?php echo $v->getId()?>,this)">Guardar</button></td>
                                </tr>
                                <?php
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="form-group col-md-4">
            <p>Accion:<br>
                <select class="form-control" id="Tipo">
                  <option value="0"></option>
                  <option value="1">Examen</option>
                  <option value="2">Sesi&oacuten informativa</option>
                  <option value="3">Clase de muestra</option>
                </select>
            </p>
        </div>
        <div class="form-group col-md-4">
            <p>Fecha inicio<br><input class="form-control" type="date" id="fecha_ini"/></p>
        </div>
        <div class="form-group col-md-4">
            <p>Fecha fin<br><input class="form-control" type="date" id="fecha_fin"/></p>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-6">
            <p>Oferta<br>
                    <select class="form-control" id="oferta" onchange="update_curso_box_curso()">
                      <option value="0"></option>
                      <?php
                      foreach($DaoOfertas->showAll() as $k=>$v){
                      ?>
                          <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
                      <?php
                      }
                      ?>
                    </select>
            </p>
        </div>
        <div class="form-group col-md-6">
            <p>Especialidad:<br>
                <select class="form-control" id="curso" onchange="update_orientacion_box_curso()">
                  <option value="0"></option>
                </select>
            </p>
            <div id="box_orientacion"></div>
        </div>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<?php
write_footer();
