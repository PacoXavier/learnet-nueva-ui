<?php
require_once('estandares/includes.php');
if(!isset($perm['67'])){
  header('Location: home.php');
}

require_once('clases/DaoNotificaciones.php');
require_once('clases/DaoNotificacionesTipoUsuario.php');
require_once('clases/DaoTiposUsuarios.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/modelos/Notificaciones.php');
require_once('clases/modelos/NotificacionesTipoUsuario.php');

$DaoNotificaciones= new DaoNotificaciones();
$DaoNotificacionesTipoUsuario= new DaoNotificacionesTipoUsuario();
$DaoUsuarios= new DaoUsuarios();
$DaoDocentes= new DaoDocentes();
$DaoAlumnos= new DaoAlumnos();

links_head("Notificaciones | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-bell-o"></i> Notificaciones</h1>
                </div>
                <div id="mascara_tabla">
                    <table  class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>T&iacute;tulo</td>
                                <td>Texto</td>
                                <td>Tipo</td>
                                <td>Creada</td>
                                <td>Inicia</td>
                                <td>Usuario</td>
                                <td>Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count=1;
                            foreach ($DaoNotificaciones->getNotificaciones() as $k => $v) {

                                if($v->getTipo()=="usu"){
                                 $tipo="Usuarios";  
                                }elseif($v->getTipo()=="docen"){
                                    $tipo="Docentes"; 
                                }elseif($v->getTipo()=="alumn"){
                                   $tipo="Alumnos";  
                                }
                                $usu_not=$DaoUsuarios->show($v->getId_usu());
            ?>
                                <tr>
                                    <td><?php echo $count;?></td>
                                    <td><?php echo $v->getTitulo() ?></td>
                                    <td style="width: 150px;"><?php echo $v->getTexto() ?></td>
                                    <td><?php echo $tipo ?></td>
                                    <td><?php echo $v->getDateCreated() ?></td>
                                    <td><?php echo $v->getDateInit();?></td>
                                    <td><?php echo $usu_not->getNombre_usu()." ".$usu_not->getApellidoP_usu()." ".$usu_not->getApellidoM_usu();?></td>
                                    <td style="width: 110px;">
                                        <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                                        <div class="box-buttom">
                                            <button onclick="delete_not(<?php echo $v->getId() ?>)">Eliminar</button><br>
                                            <button onclick="mostrar_box_notificaciones(<?php echo $v->getId() ?>)">Editar</button><br>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                    <?php
                    require_once 'estandares/menu_derecho.php';
                    ?>
                <ul>
                    <li><span onclick="mostrar_box_notificaciones()">Nueva</span></li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<?php
write_footer();
