<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

//Losporcentajes de los tipos no coincidiran ya que
//antes no existian los tipos de beca y se asignaba 
//un porcentaje al azar (Tienen que coincidir apartir del 2015C)
$base = new base();
$DaoOfertas = new DaoOfertas();
$DaoAlumnos = new DaoAlumnos();
$DaoEspecialidades = new DaoEspecialidades();
$DaoOrientaciones = new DaoOrientaciones();
$DaoOfertasAlumno = new DaoOfertasAlumno();
$DaoGrados = new DaoGrados();
$DaoMediosEnterar = new DaoMediosEnterar();
$DaoMaterias = new DaoMaterias();
$DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
$DaoBecas = new DaoBecas();
$DaoCiclos = new DaoCiclos();
$DaoTurnos= new DaoTurnos();

$ciclo = $DaoCiclos->getActual(1);

links_head("Alumnos becados| ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Alumnos becados</h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul class="row list-design">
                        <li class="col-md-4 widget" onclick="mostrar_filtro()"><div class="stats-left"><i class="fa fa-filter"></i> Filtros</div></li>
                        <li class="col-md-4 widget states-mdl" onclick="mostrar_box_email()"><div class="stats-left"><i class="fa fa-envelope"></i> Email</div></li>
                        <li class="col-md-4 widget states-last" onclick="download_excel()"><div class="stats-left"><i class="fa fa-download"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td>Grado</td>
                                <td>Opci&oacute;n de pago</td>
                                <td style="text-align:center;">Beca</td>
                                <td>Tipo</td>
                                <td style="text-align:center;">Promedio</td>
                                <td style="text-align:center;">Ciclo</td>
                                <td style="text-align:center;">Turno</td>
                                <td style="text-align:center;"><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($ciclo->getId() > 0) {
                                $count = 1;
                                $query = "SELECT * FROM ciclos_alum_ulm 
                            JOIN ofertas_alumno ON ciclos_alum_ulm.Id_ofe_alum=ofertas_alumno.Id_ofe_alum
                            JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
                            JOIN ciclos_ulm ON ciclos_alum_ulm.Id_ciclo=ciclos_ulm.Id_ciclo
                        WHERE inscripciones_ulm.tipo=1  
                              AND inscripciones_ulm.Id_plantel=" . $_usu->getId_plantel() . " 
                              AND ofertas_alumno.Activo_oferta=1 
                              AND ofertas_alumno.Baja_ofe IS NULL
                              AND ofertas_alumno.FechaCapturaEgreso IS NULL
                              AND ofertas_alumno.IdCicloEgreso IS NULL
                              AND ofertas_alumno.IdUsuEgreso IS NULL
                              AND ciclos_alum_ulm.Porcentaje_beca >0 AND ciclos_alum_ulm.Id_ciclo= " . $ciclo->getId() . " ORDER BY ciclos_alum_ulm.Id_ciclo DESC";
                                $becados = $base->advanced_query($query);
                                $row_becados = $becados->fetch_assoc();
                                $totalRows_becados = $becados->num_rows;
                                if ($totalRows_becados) {
                                    do {
                                        $nombre_ori = "";
                                        $oferta = $DaoOfertas->show($row_becados['Id_ofe']);
                                        $esp = $DaoEspecialidades->show($row_becados['Id_esp']);
                                        if ($row_becados['Id_ori'] > 0) {
                                            $ori = $DaoOrientaciones->show($row_becados['Id_ori']);
                                            $nombre_ori = $ori->getNombre();
                                        }
                                        $opcion = "Plan por materias";
                                        if ($row_becados['Opcion_pago'] == 2) {
                                            $opcion = "Plan completo";
                                        }

                                        $tur = $DaoTurnos->show($row_becados['Turno']);
                                        $t=$tur->getNombre();

                                        $nombreBeca="";
                                        if($row_becados['Tipo_beca']>0){
                                           $beca = $DaoBecas->show($row_becados['Tipo_beca']);
                                           $nombreBeca=$beca->getNombre();   
                                        }
       
                                        $Grado = $DaoGrados->show($row_becados['Id_grado']);
                                        $calTotal = 0;
                                        $countMat = 0;
                                        $query = "
                                SELECT * FROM ciclos_alum_ulm 
                                JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
                                WHERE Id_ofe_alum=" . $row_becados['Id_ofe_alum'] . " AND Id_ciclo<" . $row_becados['Id_ciclo'] . "  AND materias_ciclo_ulm.Activo=1";
                                        foreach ($base->advanced_query($query) as $k2 => $v2) {
                                            $mat_esp = $DaoMateriasEspecialidad->show($v2['Id_mat_esp']);
                                            $mat = $DaoMaterias->show($mat_esp->getId_mat());

                                            if (strlen($v2['CalTotalParciales']) > 0) {
                                                $calTotal+=$v2['CalTotalParciales'];
                                            } elseif (strlen($v2['CalExtraordinario']) > 0) {
                                                $calTotal+=$v2['CalExtraordinario'];
                                            } elseif (strlen($v2['CalEspecial']) > 0) {
                                                $calTotal+=$v2['CalEspecial'];
                                            }
                                            $countMat++;
                                        }
                                        $calTotal = $calTotal / $countMat;
                                        $pink = "";
                                        if ($calTotal < $beca->getCalificacionMin()) {
                                            $pink = "pink";
                                        }
                                        ?>
                                        <tr id_alum="<?php echo $row_becados['Id_ins']; ?>" class="<?php echo $pink . " " . $calTotal . " " . $countMat ?>">
                                            <td><?php echo $count; ?></td>
                                            <td><?php echo $row_becados['Matricula'] ?></td>
                                            <td style="width: 115px;"><?php echo $row_becados['Nombre_ins'] . " " . $row_becados['ApellidoP_ins'] . " " . $row_becados['ApellidoM_ins'] ?></td>
                                            <td><?php echo $oferta->getNombre_oferta() ?></td>
                                            <td><?php echo $esp->getNombre_esp(); ?></td>
                                            <td><?php echo $nombre_ori; ?></td>
                                            <td><?php echo $Grado->getGrado(); ?></td>
                                            <td><?php echo $opcion; ?></td>
                                            <td style="text-align:center;"><?php echo $row_becados['Porcentaje_beca']; ?>%</td>
                                            <td><?php echo $nombreBeca; ?></td>
                                            <td style="text-align:center;"><b><?php echo number_format($calTotal, 2); ?></b></td>
                                            <td style="text-align:center;"><?php echo $row_becados['Clave'] ?></td>
                                            <td style="text-align:center;"><?php echo $t ?></td>
                                            <td style="text-align:center;"><input type="checkbox"> </td>
                                        </tr>
                                        <?php
                                        $count++;
                                    } while ($row_becados = $becados->fetch_assoc());
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <p class="col-md-4">Ciclo<br>
            <select class="form-control" id="ciclo">
                <option value="0"></option>
                <?php
                foreach ($DaoCiclos->showAll() as $k => $v) {
                    ?>
                    <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
                    <?php
                }
                ?>
            </select>
        </p>
        <p class="col-md-4">Turno<br>
            <select class="form-control" id="turno">
                <option value="0"></option>
                <?php
                foreach($DaoTurnos->getTurnos() as $turno){
                    ?>
                    <option value="<?php echo $turno->getId()?>" ><?php echo $turno->getNombre()?></option>
                <?php
                }
                ?>
            </select>
        </p>
        <p class="col-md-4">Oferta<br>
            <select class="form-control" id="oferta" onchange="update_curso_box_curso()">
                <option value="0"></option>
                <?php
                foreach ($DaoOfertas->showAll() as $k => $v) {
                    ?>
                    <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
                    <?php
                }
                ?>
            </select>
        </p>
    </div>
    <div class="row">
        <p class="col-md-4"> Especialidad:<br>
            <select class="form-control" id="curso" onchange="update_orientacion_box_curso();">
                <option value="0"></option>
            </select>
        </p>
        <div class="col-md-4" id="box_orientacion"></div>
        <p class="col-md-4">Opci&oacute;n de pago:<br>
            <select class="form-control"id="opcion">
                <option value="0"></option>
                <option value="1">POR MATERIAS</option>
                <option value="2">PLAN COMPLETO</option>
            </select>
        </p>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<script src="js/ckeditor/ckeditor.js"></script>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();
