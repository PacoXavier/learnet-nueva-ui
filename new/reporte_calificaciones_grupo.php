 <?php
require_once('estandares/includes.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoCiclos.php');
links_head("Calificaciones | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Calificaciones por materia o grupo</h1>
                </div>
                <div class="box-filter-reportes" style="margin-bottom: 150px;">
                    <ul style="list-style: none; font-size: 20px;">
                        <li class="weather-grids widget-shadow" style="width: 100%;" onclick="mostrar_filtro()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-filter"></i> Filtrar</div></li>
                    </ul>
                </div>
                <ul id="lista-x">
                    <li><div id="box-faltas"></div>Reprobado por faltas</li>
                </ul>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Matricula</td>
                                <td style="width: 100px">Alumno</td>
                                <td>TURNO</td>
                                <td>Grupo</td>
                                <td>Materia</td>
                                <td>Orientaci&oacute;n</td>
                                <td>Faltas</td>
                                <td>Asistencias</td>
                                <td>Justificaciones</td>
                                <td>Porcentaje</td>
                                <td>Calificaci&oacute;n</td>
                                <td>Evaluaci&oacute;n<br> Extraordinaria</td>
                                <td>Evaluaci&oacute;n<br> Especial</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="col-xs-4">
            <p>Ciclo<br>
              <select id="ciclo" class="form-control">
                  <option value="0"></option>
                  <?php
                  $DaoCiclos = new DaoCiclos();
                  foreach($DaoCiclos->showAll() as $ciclo){
                  ?>
                   <option value="<?php echo $ciclo->getId() ?>"><?php echo $ciclo->getClave() ?></option>
                  <?php
                  }
                  ?>
                </select>
            </p>
        </div>
        <div class="col-xs-4">
            <div class="boxUlBuscador">
                <p>Docente<br><input type="search" class="buscarFiltro form-control" id="Id_docente" onkeyup="buscarDocent(this)"/></p>
                <ul class="Ulbuscador"></ul>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="boxUlBuscador">
                <p>Grupo<br><input type="search"  class="buscarFiltro form-control" id="Id_grupo" onkeyup="buscarGruposCiclo(this)"/></p>
                <ul class="Ulbuscador"></ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <p>Turno<br>
               <select id="turno" class="form-control">
                  <option value="0"></option>
                  <option value="1">Matutino</option>
                  <option value="2">Vespertino</option>
                  <option value="3">Nocturno</option>
                </select>
            </p>
        </div>
        <div class="col-xs-6">
            <p>Oferta<br>
                    <select id="oferta" class="form-control" onchange="update_curso_box_curso()">
                      <option value="0"></option>
                      <?php
                      $DaoOfertas = new DaoOfertas();
                      foreach($DaoOfertas->showAll() as $oferta){
                      ?>
                       <option value="<?php echo $oferta->getId() ?>"> <?php echo $oferta->getNombre_oferta() ?> </option>
                      <?php
                      }
                      ?>
                    </select>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <p>Especialidad:<br>
                <select id="curso" class="form-control" onchange="update_materias()">
                    <option value="0"></option>
                </select>
            </p>
        </div>
        <div class="col-xs-6">
            <p>Materia:<br>
                <select id="lista_materias" class="form-control" onclick="verificar_orientaciones()">
                <option value="0"></option>
                  </select>
            </p>
        </div>
    </div>
        <div id="box_orientacion"></div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<?php
write_footer();
