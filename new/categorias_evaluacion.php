<?php
require_once('estandares/includes.php');
require_once('require_daos.php');
$DaoCategoriasEvaluacion = new DaoCategoriasEvaluacion();
$DaoCamposCategoria = new DaoCamposCategoria();

links_head("Categorías de evaluación | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-list-ul" aria-hidden="true"></i> Categorías de evaluación</h1>
                </div>
                <?php
                foreach ($DaoCategoriasEvaluacion->getCategoriasEvaluacion() as $cat) {
                    ?>
                    <div class="seccion" id_tipo="<?php echo $cat->getId() ?>">
                        <h2>Categoría</h2>
                        <p style="margin-bottom: 20px;">Nombre categoría<br>
                            <input type="text" value="<?php echo $cat->getNombre() ?>" class="nombre-cat" disabled="disabled"/>
                            <i class="fa fa-pencil-square-o" aria-hidden="true" title="Editar" onclick="mostrarCategoria(<?php echo $cat->getId()?>)"></i>
                            <i class="fa fa-trash" aria-hidden="true" title="Eliminar" onclick="eliminarCategoria(<?php echo $cat->getId()?>)"></i>
                        </p>
                        <table id="main-niveles">
                            <tbody>
                                <tr>
                                    <td>
                                        <h2>Campos de la categoría</h2>
                                        <?php
                                        foreach ($DaoCamposCategoria->getCamposCategoria($cat->getId()) as $campo) {
                                            ?>
                                            <ul class="form" id_nivel="<?php echo $campo->getId() ?>">
                                                <li>Nombre del campo<br><input type="text" value="<?php echo $campo->getNombre(); ?>" class="nombre" disabled="disabled"/></li>
                                                <li>Valor máximo<br><input type="text" value="<?php echo $campo->getValor(); ?>" class="puntaje_profesor" disabled="disabled"/></li>
                                                <li>Tipo de campo<br>           
                                                    <select disabled="disabled">
                                                        <option value="0"></option>
                                                        <option value="1" <?php if($campo->getTipo_camp()=="1"){ ?> selected="selected" <?php } ?> >Caja de texto</option>
                                                        <option value="2" <?php if($campo->getTipo_camp()=="2"){ ?> selected="selected" <?php } ?> >Caja checkbox</option>
                                                        <option value="3" <?php if($campo->getTipo_camp()=="3"){ ?> selected="selected" <?php } ?> >Caja radio</option>
                                                    </select>
                                                </li>
                                                <li>
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true" title="Editar" onclick="mostrarCampo(<?php echo $campo->getId()?>)"></i>
                                                    <i class="fa fa-trash" aria-hidden="true" title="Eliminar" onclick="eliminarCampo(<?php echo $campo->getId()?>)"></i>
                                                </li>
                                                <br>
                                            </ul>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul> 
                    <li class="link" onclick="mostrarCategoria(0)">Agregar categoría</li>
                    <li class="link" onclick="mostrarCampo(0)">Agregar campo</li>
                </ul>
            </div> 
        </td>
    </tr>
</table
<?php
write_footer();

