<?php
require_once('estandares/includes.php');
require_once('require_daos.php');

$base = new base();
$DaoOfertas = new DaoOfertas();
$DaoAlumnos = new DaoAlumnos();
$DaoEspecialidades = new DaoEspecialidades();
$DaoOrientaciones = new DaoOrientaciones();
$DaoOfertasAlumno = new DaoOfertasAlumno();
$DaoGrados = new DaoGrados();
$DaoMediosEnterar = new DaoMediosEnterar();
$DaoCiclos = new DaoCiclos();
$DaoTurnos = new DaoTurnos();

links_head("Ingresos | ULM");
write_head_body();
write_body();
?>
<table id="tabla" >
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Ciclo de ingreso</h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul class="row list-design">
                        <li class="col-md-4 widget" onclick="mostrar_filtro()"><div class="stats-left"><i class="fa fa-filter"></i> Filtros</div></li>
                        <li class="col-md-4 widget states-mdl" onclick="mostrar_box_email()"><div class="stats-left"><i class="fa fa-envelope"></i> Email</div></li>
                        <li class="col-md-4 widget states-last" onclick="download_excel()"><div class="stats-left"><i class="fa fa-download"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Tipo</td>
                                <td>Ciclo</td>
                                <td>Matricula</td>
                                <td>Nombre</td>
                                <td>Tel.</td>
                                <td>Cel.</td>
                                <td>Email</td>
                                <td style="width: 120px">Oferta</td>
                                <td style="width: 150px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td>Turno</td>
                                <td>Usuario</td>
                                <td>Opción de pago</td>
                                <td>Medio</td>
                                <td><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil" style="margin-top: 15px">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="form-group col-md-2">
            <p>Tipo<br>
                <select id="tipo" class="form-control">
                    <option value="-1"></option>
                    <option value="0">Interesados</option>
                    <option value="1">Alumnos</option>
                </select>
            </p>
        </div>
        <div class="form-group col-md-2">
            <p>Ciclo<br>
                <select id="ciclo" class="form-control">
                    <option value="0"></option>
                    <?php
                    foreach ($DaoCiclos->showAll() as $k => $v) {
                        ?>
                        <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
                        <?php
                    }
                    ?>
                </select>
            </p>
        </div>
        <div class="boxUlBuscador form-group col-md-8">
            <p>Usuario<br><input class="form-control" type="search"  class="buscarFiltro" onkeyup="buscarUsu()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <p>Fecha inicio<br><input class="form-control" type="date" id="fecha_ini"/></p>
        </div>
        <div class="col-md-4">
            <p>Fecha fin<br><input class="form-control" type="date" id="fecha_fin"/></p>
        </div>
        <div class="col-md-4">
            <p>Turno<br>
                <select class="form-control" id="turno">
                    <option value="0"></option>
                    <?php
                    foreach ($DaoTurnos->getTurnos() as $turno) {
                        ?>
                        <option value="<?php echo $turno->getId() ?>" ><?php echo $turno->getNombre() ?></option>
                        <?php
                    }
                    ?>
                </select>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <p>Oferta<br>
                <select id="oferta" class="form-control" onchange="update_curso_box_curso()">
                    <option value="0"></option>
                    <?php
                    foreach ($DaoOfertas->showAll() as $k => $v) {
                        ?>
                        <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
                        <?php
                    }
                    ?>
                </select>
            </p>
        </div>
        <div class="col-md-6">
            <p>Especialidad:<br>
                <select class="form-control" id="curso" onchange="update_orientacion_box_curso();
                            update_grados_ofe()">
                    <option value="0"></option>
                </select>
            </p>
        </div>
    </div>

    <div id="box_orientacion"></div>
    <div class="row">
        <div class="col-md-6">
            <p>Opci&oacute;n de pago:<br>
                <select class="form-control" id="opcion">
                    <option value="0"></option>
                    <option value="1">POR MATERIAS</option>
                    <option value="2">PLAN COMPLETO</option>
                </select>
            </p>
        </div>
        <div class="col-md-6">
            <p>Medio por el que se entero:<br>
                <select class="form-control" id="medio_entero" >
                    <option value="0"></option>
                    <?php
                    foreach ($DaoMediosEnterar->showAll() as $k2 => $v2) {
                        ?>
                        <option value="<?php echo $v2->getId() ?>"><?php echo $v2->getMedio() ?></option>
                        <?php
                    }
                    ?>
                </select>
            </p>
        </div>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<script src="js/ckeditor/ckeditor.js"></script>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();
