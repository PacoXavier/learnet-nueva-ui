<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

$DaoDocentes= new DaoDocentes();
$DaoHoras= new DaoHoras();
$DaoCiclos= new DaoCiclos();
$DaoDiasPlantel= new DaoDiasPlantel();

links_head("Horario Alumno | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-clock-o" aria-hidden="true"></i> Horario Alumno</h1>
                </div>
                
                <div class="spanfiltros widget filtros" onclick="mostrar_filtro()">Mostrar filtros</div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                            <thead>
                                <tr>
                                    <td>Sesiones</td>
                                    <?php
                                    foreach($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia){
                                        ?>
                                          <td><?php echo $dia['Nombre']?></td>
                                        <?php
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                                  foreach($DaoHoras->showAll() as $hora){    
                                 ?>
                                    <tr id_hora="<?php echo $hora->getId()?>">
                                        <td><?php echo $hora->getTexto_hora()?></td>
                                        <?php
                                        foreach($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia){
                                            ?>
                                              <td><span></span><br></td>
                                            <?php
                                        }
                                        ?>
                                    </tr>
                                 <?php
                                 }
                                 ?>
                            </tbody>
                        </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="form-group col-md-4">
            <div class="boxUlBuscador">
                <p>Alumno<br><input type="search"  class="buscarFiltro form-control" onkeyup="buscarAlum()" placeholder="Nombre"/></p>
                <ul class="Ulbuscador"></ul>
            </div>
        </div>
        <div class="form-group col-md-4">
            <p>Oferta<br>
                <select id="Id_ofe_alum" class="form-control">
                  <option value="0"></option>
                </select>
            </p>
        </div>
        <div class="form-group col-md-4">
            <p>Ciclo<br>
              <select id="ciclo" class="form-control">
                  <option value="0"></option>
                      <?php
                      foreach($DaoCiclos->showAll() as $k=>$v){
                      ?>
                       <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave() ?></option>
                      <?php
                      }
                      ?>
                </select>
            </p>
        </div>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<?php
write_footer();
