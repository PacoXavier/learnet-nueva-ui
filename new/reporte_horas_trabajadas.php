<?php
require_once('estandares/includes.php');
require_once('require_daos.php');

$base = new base();
$DaoDocentes = new DaoDocentes();
$DaoEspecialidades = new DaoEspecialidades();
$DaoPermutasClase = new DaoPermutasClase();
$DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
$DaoGrupos = new DaoGrupos();
$DaoOfertas = new DaoOfertas();
$DaoAsistencias = new DaoAsistencias();
$DaoCiclos = new DaoCiclos();
$DaoPenalizacionDocente = new DaoPenalizacionDocente();
$DaoDocenteExamen = new DaoDocenteExamen();
$DaoCategoriasPago = new DaoCategoriasPago();
$DaoNivelesPago = new DaoNivelesPago();
$DaoOfertasPorCategoria = new DaoOfertasPorCategoria();
$DaoEvaluacionDocente = new DaoEvaluacionDocente();
$DaoPuntosPorCategoriaEvaluadaDocente = new DaoPuntosPorCategoriaEvaluadaDocente();

links_head("Sesiones trabajadas");
write_head_body();
write_body();

?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-clock-o" aria-hidden="true"></i> Sesiones trabajadas</h1>
                </div>
                <div class="box-filter-reportes">
                    <ul class="row" style="list-style: none; font-size: 20px;">
                        <li class="col-md-6 weather-grids widget-shadow" onclick="mostrar_filtro()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-filter"></i> Filtros</div></li>
                        <li class="col-md-6 weather-grids weather-right widget-shadow states-last" onclick="download_excel()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-download"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Ciclo</td>
                                <td>Nombre</td>
                                <td>Oferta</td>
                                <td style="text-align: center;">Tipo</td>
                                <td style="text-align: center;">Nivel</td>
                                <td style="text-align: center;">Puntos</td>
                                <td style="text-align: center;">Pago por sesión</td>
                                <td style="text-align: center;">Fecha inicio</td>
                                <td style="text-align: center;">Fecha fin</td>
                                <td style="text-align: center;">Grupos</td>
                                <td style="text-align: center;">Sesiones por semana</td>
                                <td style="text-align: center;">Horas exámen</td>
                                <td style="text-align: center;">Sesiones trabajadas</td>
                                <td style="text-align: center;">Incidencias</td>
                                <td style="text-align: center;">Total</td>
                                <td style="text-align: center;">Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cicloActual = $DaoCiclos->getActual();
                            if ($cicloActual->getId()) {
                                foreach ($DaoDocentes->showAll() as $docente) {
                                    if ($docente->getTiempo_completo() != 1) {
                                        $Id_docen = $docente->getId();
                                        $Id_ciclo = $cicloActual->getId();
                                        //Totales
                                        $count = 1;
                                        $totalHorasTrabajadas = 0;
                                        $totalHorasPorSemana = 0;
                                        $totalPenalizaciones = 0;
                                        $totalPaga = 0;
                                        $totalHorasExamen = 0;
                                        foreach ($DaoOfertas->showAll() as $ofe) {

                                            $Tipo = "";
                                            $nombreNivel = "";
                                            $PagoXhora = "0";
                                            $puntos = "0";
                                            $totalPenalizacionGrupo = 0;
                                            $horas_trabajadas = 0;
                                            $horasXSemana = "0";
                                            $TotalHorasPermuta = 0;
                                            $TotalHorasCedidas = 0;
                                            $descuentoPorPenalizacion = "0";
                                            $TotalHoras = "0";
                                            $total = "0";

                                            //Informacion de los grupos que imparte el docente
                                            $InfoHoras = $DaoDocentes->getNumGruposDocente($docente->getId(), $Id_ciclo, $ofe->getId(), $cicloActual->getFecha_ini(), date('Y-m-d'));

                                            //Informacion de los grupos que imparte el docente
                                            foreach ($InfoHoras['Grupos'] as $x) {
                                                $horas_trabajadas+=$x['HorasPor_semana_trabajados'];
                                                $horasXSemana+=$x['HorasPor_semana'];
                                                foreach ($DaoPenalizacionDocente->getPenalizacionesDocente($docente->getId(), $Id_ciclo, $x['Id_grupo'], $cicloActual->getFecha_ini(), date('Y-m-d')) as $penalizacion) {
                                                    $totalPenalizacionGrupo++;
                                                    $totalPenalizaciones++;
                                                }
                                            }

                                            //Número de grupos del docente
                                            $numGrupos = $InfoHoras['NumGrupos'];

                                            //Número de horas de examen
                                            $horasExamen = $DaoDocenteExamen->getTotalHorasTrabajadasExamen($Id_ciclo, $Id_docen, $ofe->getId(), null, $cicloActual->getFecha_ini(), date('Y-m-d'));

                                            //Número horas de grupos permutados para la oferta
                                            foreach ($DaoPermutasClase->getPermutasDocenteByCiclo($docente->getId(), $cicloActual->getId(), $cicloActual->getFecha_ini(), date('Y-m-d')) as $permuta) {
                                                //Si el grupo pertenece a la oferta
                                                $grupo = $DaoGrupos->show($permuta->getId_grupo());
                                                $mat_esp = $DaoMateriasEspecialidad->show($grupo->getId_mat_esp());
                                                $esp = $DaoEspecialidades->show($mat_esp->getId_esp());
                                                $oferta = $DaoOfertas->show($esp->getId_ofe());
                                                if ($oferta->getId() == $ofe->getId()) {
                                                    //Verificamos si capturo asistencias en el grupo
                                                    $existe = $DaoAsistencias->existAsistencia($permuta->getId_grupo(), $permuta->getDia());
                                                    if ($existe == 1) {
                                                        //Cada permuta es una hora
                                                        //Si el docente asignado para la permuta es el que esta en el ciclo
                                                        if ($permuta->getId_docente_asignado() == $docente->getId()) {
                                                            $TotalHorasPermuta++;
                                                        } else {
                                                            $TotalHorasCedidas++;
                                                        }
                                                    }
                                                }
                                            }

                                            //Obtenemos la ultima evaluacion del docente
                                            $evaluacion = $DaoEvaluacionDocente->getUltimaEvaluacionDocente($Id_docen);
                                            if ($evaluacion->getId() > 0) {
                                                $ofertaPorCategoria = $DaoOfertasPorCategoria->getCategoriaPorOferta($ofe->getId());
                                                $CategoriaTabulador = $DaoCategoriasPago->show($ofertaPorCategoria->getId_tipo());
                                                if($CategoriaTabulador->getId()>0){
                                                    $Tipo = $CategoriaTabulador->getNombre_tipo();
                                                    $puntos = $evaluacion->getPuntosTotales();
                                                    $resul = $DaoPuntosPorCategoriaEvaluadaDocente->getNivelPorCategoria($evaluacion->getId(), $CategoriaTabulador->getId());
                                                    if ($resul->getId() > 0) {
                                                        $nivel = $DaoNivelesPago->show($resul->getId_nivel());
                                                        $nombreNivel = $nivel->getNombre_nivel();
                                                        $PagoXhora = $nivel->getPagoNivel();
                                                    }
                                                }
                                            }

                                            //Descuento por penalizacion 
                                            if (isset($params['PorcentajeHoraPenalizada']) && $params['PorcentajeHoraPenalizada'] > 0 && $totalPenalizacionGrupo > 0) {
                                                $descuentoPorPenalizacion = ($params['PorcentajeHoraPenalizada'] * $PagoXhora / 100) * $totalPenalizacionGrupo;
                                            }

                                            //Total de horas 
                                            $TotalHoras = $horas_trabajadas + ($TotalHorasPermuta - $TotalHorasCedidas) + $horasExamen;

                                            //Pago total
                                            $total = ($TotalHoras * $PagoXhora) - $descuentoPorPenalizacion;
                                            $nombre = $base->covertirCadena($docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . " " . $docente->getApellidoM_docen(), 2);
                                            ?>
                                            <tr id_doc="<?php echo $docente->getId(); ?>" id-ciclo="<?php echo $cicloActual->getId() ?>" id-ofe="<?php echo $ofe->getId() ?>" fecha-ini="<?php echo $cicloActual->getFecha_ini() ?>" fecha-fin="<?php echo date('Y-m-d') ?>">
                                                <td><?php echo $count ?></td>
                                                <td><?php echo $cicloActual->getClave() ?></td>
                                                <td><?php echo $nombre ?></td>
                                                <td><?php echo $ofe->getNombre_oferta(); ?></td>
                                                <td style="text-align: center;"><?php echo $Tipo ?></td>
                                                <td style="text-align: center;"><?php echo $nombreNivel ?></td>
                                                <td style="text-align: center;"><?php echo $puntos; ?></td>
                                                <td style="text-align: center;">$<?php echo number_format($PagoXhora, 2); ?></td>
                                                <td style="text-align: center;"><?php echo $cicloActual->getFecha_ini(); ?></td>
                                                <td style="text-align: center;"><?php echo date('Y-m-d'); ?></td>
                                                <td style="text-align: center;"><?php echo $numGrupos ?></td>
                                                <td style="text-align: center;"><?php echo $horasXSemana; ?></td>
                                                <td style="text-align: center;" onclick="mostrarDetalleExamenes(this)" class="detalle-examen"><?php echo $horasExamen; ?></td>
                                                <td style="text-align: center;"><?php echo $TotalHoras; ?></td>
                                                <td style="text-align: center;<?php if ($totalPenalizacionGrupo > 0) { ?> color:red; <?php } ?>" onclick="mostrarDetallePenalizaciones(this)" class="incidencias"><?php echo $totalPenalizacionGrupo; ?></td>
                                                <td style="text-align: center;">$<?php echo number_format($total, 2); ?></td>
                                                <td style="text-align: center;">
                                                    <button onclick="mostrar_grupos(<?php echo $docente->getId() ?>,<?php echo $cicloActual->getId() ?>,<?php echo $ofe->getId() ?>, '<?php echo $cicloActual->getFecha_ini() ?>', '<?php echo date('Y-m-d') ?>')">Detalles</button>
                                                </td>
                                            </tr>
                                            <?php
                                            $count++;
                                            $totalHorasPorSemana+=$horasXSemana;
                                            $totalHorasTrabajadas+=$TotalHoras;
                                            $totalHorasExamen+=$horasExamen;
                                            $totalPaga+=$total;
                                        }
                                        ?>
                                        <tr class="totales">
                                            <td colspan="11">Total</td>
                                            <td style="text-align: center;"><?php echo $totalHorasPorSemana; ?></td>
                                            <td style="text-align: center;"><?php echo $totalHorasExamen; ?></td>
                                            <td style="text-align: center;"><?php echo $totalHorasTrabajadas; ?></td>
                                            <td style="text-align: center;<?php if ($totalPenalizaciones > 0) { ?> color:red; <?php } ?>"><?php echo $totalPenalizaciones; ?></td>
                                            <td style="text-align: center;">$<?php echo number_format($totalPaga, 2); ?></td>
                                            <td style="text-align: center;"></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="boxUlBuscador col-md-8">
            <p>Docente<br><input type="search"  class="buscarFiltro form-control" onkeyup="buscarDocent()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p class="col-md-4">Ciclo<br>
            <select id="ciclo" class="form-control">
                <option value="0"></option>
                <?php
                foreach ($DaoCiclos->showAll() as $k => $v) {
                    ?>
                    <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave() ?></option>
                    <?php
                }
                ?>
            </select>
        </p>
    </div>
    <div class="row">
        <p class="col-md-4">Oferta<br>
            <select id="oferta" class="form-control" onchange="update_curso_box_curso()">
                <option value="0"></option>
                <?php
                foreach ($DaoOfertas->showAll() as $k => $v) {
                    ?>
                    <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
                    <?php
                }
                ?>
            </select>
        </p>
        <p class="col-md-4">Fecha inicio<br><input class="form-control" type="date"  id="fecha_ini"/></p>
        <p class="col-md-4">Fecha fin<br><input class="form-control" type="date"  id="fecha_fin"/></p>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<?php
write_footer();
