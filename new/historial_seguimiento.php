<?php
require_once('estandares/includes.php');
require_once('clases/DaoUsuarios.php');
require_once('estandares/class_interesados.php');
require_once('clases/DaoSeguimientoAdeudoAlumno.php');
require_once('clases/modelos/SeguimientoAdeudoAlumno.php');
$DaoSeguimientoAdeudoAlumno= new DaoSeguimientoAdeudoAlumno();
$DaoUsuarios= new DaoUsuarios();
links_head("Historial de seguimiento | ULM");
write_head_body();
write_body();
?>
<table id="tabla" class="table">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1>Historial de prestamos</h1>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td style="width: 200px;">Usuario</td>
                                <td style="width: 100px;text-align: center;">Fecha</td>
                                <td>Comentarios</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if($_REQUEST['id']>0){
                            $count=1;
                            $query="SELECT * FROM SeguimientoAdeudoAlumno WHERE TipoRel='".$_REQUEST['tipo']."' AND IdRel=".$_REQUEST['id'];
                            foreach ($DaoSeguimientoAdeudoAlumno->advanced_query($query) as $k => $v) {
                                $usu=$DaoUsuarios->show($v->getId_usu());
                                ?>
                                <tr>
                                    <td><?php echo $count ?></td>
                                    <td><?php echo $usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu() ?></td>
                                    <td style="text-align: center;"><?php echo $v->getDateCreated()?></td>
                                    <td><?php echo $v->getComentario()  ?></td>
                                </tr>
                                <?php
                                $count++;
                            }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
        <td>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<input type="hidden" id="Id_activo" value="<?php echo $_REQUEST['id'] ?>"/>
<?php
write_footer();
