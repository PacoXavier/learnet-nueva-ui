<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoMediosEnterar.php');

links_head("Avisos | ULM");
write_head_body();
write_body();

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoGrados= new DaoGrados();
$DaoMediosEnterar= new DaoMediosEnterar();

?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-envelope"></i> Generar Avisos</h1>
                </div>
                <div class="seccion ">
                    <form class="formulario panel-body widget-shadow">
                        <div style="width: 100%">
                            <p>Asunto<br><input type="text" id="asunto" class="form-control" /></p>
                            <!--<p>Docentes<br><input type="text" id="destinatarios_docentes" placeholder="Email"/></p>
                            <p>Alumnos<br><input type="text" id="destinatarios" placeholder="Email"/></p>-->
                            <div id="box-editor">
                            <p>Mensaje<br><textarea id="editor1" class="form-control"></textarea></p>
                            </div>
                        </div>
                        <div style="width: 100%">
                            <p><b>Destinatarios</b></p>
                            <ul class="destinatarios">
                                <li><input type="radio" name="destinatarios" id="profesores" onclick="get_profesores()"/> Profesores</li>
                                <li><input type="radio" name="destinatarios" id="alumnos" class="alumnos" onclick="get_alumnos()" checked="checked"/> Alumnos
                                    <ul>
                                        <li class="box-tutores"><input type="checkbox" id="tutores"/> Tutores</li>
                                        <li class="box-tutores"><input type="checkbox" id="alum" checked="checked"/> Alumnos</li>

                                    </ul></li>
                                <li><input type="radio" name="destinatarios" id="grupos" onclick="get_grupos()"/> Grupos</li>
                            </ul>
                        </div>
                        <div class="archivos">
                            <p><b><i class="fa fa-paperclip"></i> Archivos adjuntos</b><br><span>(M&aacute;ximo 25MB)</span></p>
                            <ul id="list_archivos"></ul>
                        </div>
                        <p><button class="btn btn-primary" onclick="send_email(this)">Enviar</button></p>
                    </form>
                    <div id="box-tabla" class="panel-body widget-shadow tables">
                        <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Matricula</td>
                                <td>Nombre</td>
                                <td>Oferta</td>
                                <td>Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count=1;
                            foreach ($DaoAlumnos->getAlumnos() as $k=>$v){
                               $nombre_ori="";
                               $oferta = $DaoOfertas->show($v['Id_ofe']);
                               $esp = $DaoEspecialidades->show($v['Id_esp']);
                               if ($v['Id_ori'] > 0) {
                                  $ori = $DaoOrientaciones->show($v['Id_ori']);
                                  $nombre_ori = $ori->getNombre();
                                }
                                $opcion="Plan por materias"; 
                                if($v['Opcion_pago']==2){
                                  $opcion="Plan completo";  
                                }

                                $Grado=$DaoOfertasAlumno->getLastCicloOfertaSinCiclo($v['Id_ofe_alum']);
                                $Grado=$DaoGrados->show($Grado['Id_grado']);
                                
                                $MedioEnt="";
                                if($v['Id_medio_ent']>0){
                                  $medio=$DaoMediosEnterar->show($v['Id_medio_ent']);
                                  $MedioEnt=$medio->getMedio();
                                }
                                 
                                 ?>
                                         <tr id-data="<?php echo $v['Id_ins'];?>" tipo="alumno">
                                           <td><?php echo $count;?></td>
                                           <td><?php echo $v['Matricula'] ?></td>
                                           <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                                           <td><?php echo $oferta->getNombre_oferta() ?></td>
                                           <td><?php echo $esp->getNombre_esp(); ?></td>
                                           <td><?php echo $nombre_ori; ?></td>
                                           <td><input type="checkbox"> </td>
                                         </tr>
                                         <?php
                                         $count++;
                                }
                                  ?>
                        </tbody>
                    </table>
                    </div>
                </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><span onclick="mostrarFinder()" >Adjuntar archivos</span></li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil_aviso"></div>
<input type="file" id="files" name="files" multiple="">
<script src="js/ckeditor/ckeditor.js"></script>
<?php
write_footer();
?>
