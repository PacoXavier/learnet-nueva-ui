<?php 
if($_REQUEST['id']>0){
require_once('estandares/class_bdd.php');
require_once('Connections/cnn.php');
require_once('estandares/Fpdf/fpdf.php'); 
require_once('estandares/Fpdf/fpdi.php'); 
require_once('estandares/cantidad_letra.php'); 
require_once('estandares/class_interesados.php');
require_once('estandares/class_docentes.php');
require_once('estandares/class_usuarios.php');
require_once('estandares/class_ofertas.php'); 
require_once('estandares/class_direcciones.php'); 

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');
$class_bdd= new class_bdd();
if ($_REQUEST['id'] > 0 && $_REQUEST['tipo']=="alum") {
    $class_interasados = new class_interesados();
    $amo=$class_interasados->get_amolestacion_alumno($_REQUEST['id']);
    $alum = $class_interasados->get_interesado($amo['Id_recibe']);
    $nombre = $alum['Nombre_ins'] . " " . $alum['ApellidoP_ins'] . " " . $alum['ApellidoM_ins'];
    $matricula=$alum['Matricula'];
    $tipo="alumno (a)";
   
}elseif($_REQUEST['id'] > 0 && $_REQUEST['tipo']=="usu"){
    $class_usu = new class_usuarios();
    $amo=$class_usu->get_amolestacion_usuario($_REQUEST['id']);
    $usus = $class_usu->get_usu($amo['Id_recibe']);
    $nombre = $usus['Nombre_usu'] . " " . $usus['ApellidoP_usu'] . " " . $usus['ApellidoM_usu'];
    $matricula=$usus['Clave_usu'];
    $tipo="docente";
    
}elseif($_REQUEST['id'] > 0 && $_REQUEST['tipo']=="docen"){
    $class_docentes = new class_docentes();
    $amo=$class_docentes->get_amolestacion_docente($_REQUEST['id']);
    $docente = $class_docentes->get_docente($amo['Id_recibe']);
    $nombre = $docente['Nombre_docen'] . " " . $docente['ApellidoP_docen'] . " " . $docente['ApellidoM_docen'];
    $matricula=$docente['Clave_docen'];
    $tipo="trabajador";
}
                      

// initiate FPDI 
$pdf = new FPDI(); 

$carrera="";
// add a page 
$pdf->AddPage();
$setSourceFile="estandares/acta_administrativa_alumno.pdf";

$pdf->setSourceFile($setSourceFile);

// import page 1 
$tplIdx = $pdf->importPage(1); 
// use the imported page and place it at point 10,10 with a width of 100 mm 
$pdf->useTemplate($tplIdx); 

// now write some text above the imported page 
$pdf->SetFont('Arial','B',9); 
$pdf->SetTextColor(0,0,0); 

$pdf->SetXY(144,71.5); 
$pdf->Write(10, utf8_decode(strtoupper($tipo)));

$pdf->SetXY(65,76); 
$pdf->Write(10, utf8_decode(mb_strtoupper($nombre,"utf-8")));

$pdf->SetXY(30,85); 
$pdf->Write(10, utf8_decode($class_bdd->formatFecha($amo['Fecha_rep'])));
$pdf->SetXY(29,117); 
$pdf->MultiCell(135,3 ,utf8_decode(mb_strtoupper($amo['Motivo'],"utf-8")),0,'L'); 
$pdf->SetXY(75,170); 
$pdf->Write(10, utf8_decode($class_bdd->formatFecha(date('Y-m-d'))));

$pdf->Output('Acta_administrativa_'.$matricula.'.pdf', 'I'); 
}else{
    //header("Location: interesados.php");
}
