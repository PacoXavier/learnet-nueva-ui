 <?php
require_once('estandares/includes.php');
require_once('clases/DaoEventos.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoAulas.php');
require_once('clases/DaoPagos.php');
require_once('clases/modelos/Aulas.php');
require_once('clases/modelos/Ciclos.php');
require_once('clases/modelos/Eventos.php');
require_once('clases/modelos/Pagos.php');
links_head("Eventos | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Eventos</h1>
                </div>
                <div class="box-filter-reportes">
                    <ul class="row" style="list-style: none; font-size: 20px;">
                        <li class="col-md-6 weather-grids widget-shadow" onclick="mostrar_filtro()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-filter"></i> Filtros</div></li>
                        <li class="col-md-6 weather-grids weather-right widget-shadow states-last" onclick="download_excel()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-download"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Nombre</td>
                                <td>Ciclo</td>
                                <td>Aula</td>
                                <td>Fecha</td>
                                <td>Hora inicio</td>
                                <td>Hora fin</td>
                                <td style="width: 150px">Comentarios</td>
                                <td>Costo renta</td>
                                <td>Pagado</td>
                                <td style="text-align: center;">Opciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $count=1;
                                $DaoEventos= new DaoEventos();
                                $DaoCiclos= new DaoCiclos();
                                $DaoAulas= new DaoAulas();
                                $DaoPagos= new DaoPagos();
                                foreach($DaoEventos->showAll() as $k=>$v){
                                    $ciclo=$DaoCiclos->show($v->getId_ciclo());
                                    $aula=$DaoAulas->show($v->getId_salon());

                                    $Start=date('Y-m-d', strtotime($v->getStart()));
                                    $HoraInicio=date('H:i:s', strtotime($v->getStart()));
                                    $HoraFin=date('H:i:s', strtotime($v->getEnd()));
                                    
                                    $TotalPagado=0;
                                    foreach($DaoPagos->getPagosEvento($v->getId()) as $k2=>$v2){
                                        $TotalPagado+=$v2->getMonto_pp();
                                    }
                                ?>
                                <tr>
                                    <td><?php echo $count?> </td>
                                    <td><?php echo $v->getNombre()?> </td>
                                    <td><?php echo $ciclo->getClave()?> </td>
                                    <td><?php echo $aula->getClave_aula()." - ".$aula->getNombre_aula() ?></td>
                                    <td><?php echo $Start;?></td>
                                    <td><?php echo $HoraInicio;?></td>
                                    <td><?php echo $HoraFin?></td>
                                    <td><?php echo $v->getComentarios()?></td>
                                    <td><b>$<?php echo number_format($v->getCosto(),2);?></b></td>
                                    <td><b>$<?php echo number_format($TotalPagado,2);?></b></td>
                                    <td style="text-align: center;" class="menu">
                                         <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar opciones</span>
                                         <div class="box-buttom">
                                             <?php
                                             if($v->getCosto()>$TotalPagado){
                                                 ?>
                                                 <button onclick="mostrar_box_pagar(<?php echo $v->getId()?>)">Pagar</button>
                                             <?php
                                             }
                                             ?>
                                             <button onclick="mostrar_historial(<?php echo $v->getId()?>)">Historial</button>
                                         </div>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <p class="col-md-3">Fecha inicio<br><input  class="form-control" type="date"  id="fecha_ini"/></p>
        <p class="col-md-3">Fecha fin<br><input  class="form-control" type="date"  id="fecha_fin"/></p>
        <p class="col-md-3">Ciclo<br>
          <select id="ciclo" class="form-control" >
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p class="col-md-3">Aula<br>
           <select id="aula" class="form-control" >
              <option value="0"></option>
              	<?php
                foreach($DaoAulas->showAll() as $k=>$v){
                  	?>
                          <option value="<?php echo $v->getId()?>"><?php echo $v->getClave_aula()." - ".$v->getNombre_aula() ?></option>
	                <?php  
                }
                ?>
            </select>
        </p>
        <div style="display: flex; justify-content: flex-end; margin-top: 75px; padding-right: 15px">
            <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
        </div>
    </div>
</div>
<?php
write_footer();
