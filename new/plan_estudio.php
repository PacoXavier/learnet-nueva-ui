<?php
require_once('estandares/includes.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoMateriasEspecialidad.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoPrerrequisitos.php');

require_once('estandares/class_ofertas.php');
require_once('estandares/class_materias.php');
links_head("Plan de estudios | ULM");
write_head_body();
write_body();

$DaoPrerrequisitos = new DaoPrerrequisitos();
$DaoOfertas = new DaoOfertas();
$DaoEspecialidades = new DaoEspecialidades();
$DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
$DaoMaterias = new DaoMaterias();
$DaoGrados = new DaoGrados();
$nombre = "";
$Id_esp = 0;
if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $esp = $DaoEspecialidades->show($_REQUEST['id']);
    $ofe = $DaoOfertas->show($esp->getId_ofe());
    $nombre = $ofe->getNombre_oferta() . " " . $esp->getNombre_esp();
    $Id_esp = $_REQUEST['id'];
}
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-list-ul" aria-hidden="true"></i> Plan de Estudios</h1>
                </div>
                <div class="seccion">
                    <h2><?php echo $nombre ?></h2>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <td>Materia</td>
                            <td>Clave Plan</td>
                            <td>Clave Materia</td>
                            <td>Sesiones por semana</td>
                            <td>Dias por semana</td>
                            <td>Cr&eacute;ditos</td>
                            <td>Grado</td>
                            <td>Requisitos</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($DaoMateriasEspecialidad->getMateriasEspecialidad($Id_esp) as $matEsp) {
                            $mat = $DaoMaterias->show($matEsp->getId_mat());

                            $grado = $DaoGrados->show($matEsp->getGrado_mat());
                            $NombreMat = $mat->getNombre();
                            if (strlen($matEsp->getNombreDiferente()) > 0) {
                                $NombreMat = $matEsp->getNombreDiferente();
                            }
                            ?>
                            <tr>
                                <td><a href="materia.php?id_esp=<?php echo $matEsp->getId_esp()?>&id_mat=<?php echo $mat->getId() ?>"><?php echo $NombreMat ?></a></td>
                                <td><?php echo $esp->getClavePlan_estudios() ?></td>
                                <td><?php echo $mat->getClave_mat() ?></td>
                                <td><?php echo $mat->getHoras_porSemana() ?> sesiones</td>
                                <td><?php echo $mat->getDias_porSemana() ?> </td>
                                <td><?php echo $mat->getCreditos() ?></td>
                                <td><?php echo $grado->getGrado() ?></td>
                                <td>
                                    <ul>
                                        <?php
                                        foreach ($DaoPrerrequisitos->getPrerrequisitoByIdMatEsp($matEsp->getId()) as $prerrequisito) {
                                            $mat_esp = $DaoMateriasEspecialidad->show($prerrequisito->getId_mat_esp_pre());
                                            $mat = $DaoMaterias->show($mat_esp->getId_mat());
                                            ?>
                                            <li style="color: #336699;"><?php echo $mat->getClave_mat() . " - " . $mat->getNombre() ?></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </td>
                            </tr>
    <?php
}
?>
                    </tbody>
                </table>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <?php
                    if (!isset($_REQUEST['id_mat']) > 0) {
                        ?>
                        <li><a href="curso.php?id=<?php echo $_REQUEST['id']; ?>" class="link">Regresar</a></li>
                        <?php
                    } else {
                        ?>
                        <li><a href="materia.php?id_mat=<?php echo $_REQUEST['id_mat']; ?>" class="link">Regresar</a></li>
                        <?php
                    }
                    ?>

                </ul>
            </div>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<?php
write_footer();
