<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoGrados= new DaoGrados();
$DaoMediosEnterar= new DaoMediosEnterar();
$DaoCiclos= new DaoCiclos();
$DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
$DaoCiclosAlumno= new DaoCiclosAlumno();
$DaoGrupos= new DaoGrupos();
$DaoDocentes= new DaoDocentes();
$DaoMaterias= new DaoMaterias();
$DaoBecas = new DaoBecas();

$ciclo=$DaoCiclos->getActual($_usu->getId_plantel());

links_head("Alumnos reprobados | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-user"></i> Alumnos reprobados</h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul class="row list-design">
                        <li class="col-md-4 widget" onclick="mostrar_filtro()"><div class="stats-left"><i class="fa fa-filter"></i> Filtros</div></li>
                        <li class="col-md-4 widget states-mdl" onclick="mostrar_box_email()"><div class="stats-left"><i class="fa fa-envelope"></i> Email</div></li>
                        <li class="col-md-4 widget states-last" onclick="download_excel()"><div class="stats-left"><i class="fa fa-download"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td style="width: 80px">CICLO</td>
                                <td style="width: 80px">Grupo</td>
                                <td>Turno</td>
                                <td>Docente</td>
                                <td>Materia</td>
                                <td>Alumno</td>
                                <td class="td-center">Promedio mínimo</td>
                                <td class="td-center">calificación obtenida</td>
                                <td class="td-center"><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if($ciclo->getId()>0){
                            $count=1;
                            $query="
                                SELECT 
                                        ciclos_ulm.Clave AS ClaveCiclo,
                                        ciclos_alum_ulm.Id_ciclo,
                                        Grupos.Clave,Grupos.Turno,
                                        CASE 
                                             WHEN Grupos.Turno=1 THEN 'MATUTINO' 
                                             WHEN Grupos.Turno=2 THEN 'VESPERTINO' 
                                             WHEN Grupos.Turno=3 THEN 'NOCTURNO' 
                                        END AS Turno,
                                        materias_uml.Nombre,materias_uml.Promedio_min,
                                        materias_ciclo_ulm.CalEspecial, materias_ciclo_ulm.CalExtraordinario, ROUND(materias_ciclo_ulm.CalTotalParciales) AS CalificacionTotalParciales,
                                        inscripciones_ulm.Matricula,inscripciones_ulm.Nombre_ins,inscripciones_ulm.ApellidoP_ins, inscripciones_ulm.ApellidoM_ins,inscripciones_ulm.Id_plantel,inscripciones_ulm.Id_ins,
                                        Docentes.Nombre_docen, Docentes.ApellidoP_docen,Docentes.ApellidoM_docen
                                        FROM ciclos_alum_ulm 
                                 JOIN ciclos_ulm ON ciclos_alum_ulm.Id_ciclo=ciclos_ulm.Id_ciclo
                                 JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
                                 JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                                 JOIN materias_uml ON Materias_especialidades.Id_mat=materias_uml.Id_mat
                                 JOIN inscripciones_ulm ON materias_ciclo_ulm.Id_alum=inscripciones_ulm.Id_ins
                                 JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
                                 JOIN Grupos ON materias_ciclo_ulm.Id_grupo=Grupos.Id_grupo
                                 JOIN Horario_docente ON Grupos.Id_grupo=Horario_docente.Id_grupo
                                 JOIN Docentes ON Horario_docente.Id_docente=Docentes.Id_docen
                                 WHERE ciclos_alum_ulm.Id_ciclo=".$ciclo->getId()."
                                       AND materias_ciclo_ulm.Activo=1
                                       AND inscripciones_ulm.Id_plantel=".$_usu->getId_plantel()."
                                       AND inscripciones_ulm.tipo=1 
                                       AND ofertas_alumno.Activo_oferta=1 
                                       AND ofertas_alumno.Baja_ofe IS NULL
                                       AND ofertas_alumno.FechaCapturaEgreso IS NULL
                                       AND ofertas_alumno.IdCicloEgreso IS NULL
                                       AND ofertas_alumno.IdUsuEgreso IS NULL
                                       AND Round(materias_ciclo_ulm.CalTotalParciales)<materias_uml.Promedio_min
                                 GROUP BY Grupos.Clave,inscripciones_ulm.Id_ins";
                            foreach ($base->advanced_query($query) as $k=>$v){

                                 ?>
                                         <tr id_alum="<?php echo $v['Id_ins'];?>">
                                           <td><?php echo $count;?></td>
                                           <td><?php echo $v['ClaveCiclo'] ?></td>
                                           <td><?php echo $v['Clave'] ?></td>
                                           <td><?php echo $v['Turno'] ?></td>
                                           <td><?php echo $v['Nombre_docen'] . " " . $v['ApellidoP_docen'] . " " . $v['ApellidoM_docen'] ?></td>
                                           <td><?php echo $v['Nombre'] ?></td>
                                           <td><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                                           <td class="td-center"><?php echo $v['Promedio_min'] ?></td>
                                           <td class="td-center"><?php echo $v['CalificacionTotalParciales'] ?></td>
                                           <td class="td-center"><input type="checkbox"> </td>
                                         </tr>
                                         <?php
                                         $count++;
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <p class="col-md-3">Ciclo<br>
          <select id="ciclo" class="form-control">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p class="col-md-3">Calificación menor o igual a<br><input class="form-control" type="number" id="calificacion" /></p>
           <div class="boxUlBuscador col-md-3">
                <p>Docente<br><input type="search"  class="buscarFiltro form-control" onkeyup="buscarDocente(this)" id="Id_docente"/></p>
                <ul class="Ulbuscador"></ul>
            </div>
            <div class="boxUlBuscador col-md-3">
                <p>Grupo<br><input type="search"  class="buscarFiltro form-control" onkeyup="buscarGruposCiclo(this)" id="Id_grupo"/></p>
                <ul class="Ulbuscador"></ul>
            </div>
        <!--
	<p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              /*
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
               */
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso();update_grados_ofe()">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
        -->
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<script src="js/ckeditor/ckeditor.js"></script>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();