<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoMediosEnterar.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoCiclosAlumno.php');

$base = new base();
$DaoOfertas = new DaoOfertas();
$DaoAlumnos = new DaoAlumnos();
$DaoEspecialidades = new DaoEspecialidades();
$DaoOrientaciones = new DaoOrientaciones();
$DaoOfertasAlumno = new DaoOfertasAlumno();
$DaoGrados = new DaoGrados();
$DaoMediosEnterar = new DaoMediosEnterar();
$DaoCiclos = new DaoCiclos();
$DaoCiclosAlumno = new DaoCiclosAlumno();

$Id_ciclo=13;
links_head("Cambiar nivel | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-repeat"></i> Cambiar de nivel</h1>
                </div>
                <ul class="form list-design">
                    <li><!--Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" />--> <span id="alumnosCAmbiados"></span></li>
                </ul>
                <div class="box-filter-reportes">
                    <ul class="row list-design" >
                        <li class="col-md-6 weather-grids widget-shadow" onclick="mostrar_filtro()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-filter"></i> Filtros</div></li>
                        <li class="col-md-6 weather-grids weather-right widget-shadow states-last" onclick="mostrar_box()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-repeat"></i> Cambiar de nivel</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla"  class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td class="td-center">Orientaci&oacute;n:</td>
                                <td class="td-center">Opcion de pago</td>
                                <td class="td-center">Ciclo Actual</td>
                                <td class="td-center"><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                                <td class="td-center">Estatus</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count=1;
                            //Se mostran alumnos duplicados si es que tienen mas dos ofertas
                            $query = "SELECT * FROM inscripciones_ulm 
                             JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
                             JOIN ofertas_ulm ON ofertas_alumno.Id_ofe=ofertas_ulm.Id_oferta
                             WHERE tipo=1  AND inscripciones_ulm.Id_plantel=" . $_usu->getId_plantel()."
                                   AND ofertas_alumno.Activo_oferta=1 
                                   AND Baja_ofe IS NULL 
                                   AND FechaCapturaEgreso IS NULL
                                   AND IdCicloEgreso IS NULL 
                                   AND IdUsuEgreso IS NULL
                                   AND ofertas_ulm.Tipo_oferta=1
                                   ORDER BY Id_ins";
                            $inscripciones_ulm = $base->advanced_query($query);
                            $row_inscripciones_ulm = $inscripciones_ulm->fetch_assoc();
                            $totalRows_inscripciones_ulm = $inscripciones_ulm->num_rows;
                            if ($totalRows_inscripciones_ulm) {
                                do {

                                    $alum = $DaoAlumnos->show($row_inscripciones_ulm['Id_ins']);
                                    $nombre_ori = "";
                                    $oferta = $DaoOfertas->show($row_inscripciones_ulm['Id_ofe']);
                                    $esp = $DaoEspecialidades->show($row_inscripciones_ulm['Id_esp']);
                                    if ($row_inscripciones_ulm['Id_ori'] > 0) {
                                        $ori = $DaoOrientaciones->show($row_inscripciones_ulm['Id_ori']);
                                        $nombre_ori = $ori->getNombre();
                                    }

                                    $opcion = "Plan por materias";
                                    if ($row_inscripciones_ulm['Opcion_pago'] == 2) {
                                        $opcion = "Plan completo";
                                    }

                                    $cicloAlumno = $DaoCiclosAlumno->getLastCicloOferta($row_inscripciones_ulm['Id_ofe_alum']);
                                    $ciclo = $DaoCiclos->show($cicloAlumno->getId_ciclo());
                                    //if($ciclo->getId()==$Id_ciclo){
                                    ?>
                                    <tr id_alum="<?php echo $alum->getId(); ?>" id_ofe_alum="<?php echo $row_inscripciones_ulm['Id_ofe_alum']; ?>">
                                        <td><?php echo $count;?></td>
                                        <td><?php echo $alum->getMatricula() ?></td>
                                        <td style="width: 115px;"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
                                        <td><?php echo $oferta->getNombre_oferta(); ?></td>
                                        <td><?php echo $esp->getNombre_esp(); ?></td>
                                        <td class="td-center"><?php echo $nombre_ori; ?></td>
                                        <td class="td-center"><?php echo $opcion; ?></td>
                                        <td class="td-center"><span class="ciclo-actual"><?php echo $ciclo->getClave() ?></span></td>
                                        <td style="text-align:center;"><input type="checkbox"> </td>
                                        <td class="td-center"><span>Sin cambio</span></td>
                                    </tr>
                                    <?php
                                    $count++;
                                    //}
                                } while ($row_inscripciones_ulm = $inscripciones_ulm->fetch_assoc());
                            }
                            ?>
                                    
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
	<p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso();update_grados_ofe()">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
        <p>Ciclo<br>
            <select id="cicloFiltro">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getClave() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>

<div class="box-ciclos">
	<h1>Cambiar de nivel</h1>
	<p>Del ciclo<br>
            <select id="Id_ciclo_actual">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getClave() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
	<p>Al ciclo<br>
            <select id="Id_ciclo_nuevo">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getClave() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p><button onclick="confirmarCambio(this)">Cambiar</button><button onclick="ocultar_box()">Cancelar</button></p>
</div>
<?php
write_footer();
