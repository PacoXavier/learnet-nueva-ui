<?php
require_once('estandares/includes.php');
if(!isset($perm['52'])){
  header('Location: home.php');
}
require_once('require_daos.php'); 
$DaoLibros= new DaoLibros();
$DaoAulas= new DaoAulas();
$DaoCategoriasLibros= new DaoCategoriasLibros();

links_head("Libros | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-book"></i> Libros</h1>
                </div>
                <ul class="form">
                    <li>Buscar<br><input type="search"  placeholder="Buscar" id="buscar" onkeyup="buscarLibro()" /></li>
                </ul>
                <div id="mascara_tabla">
                    <table  class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>C&oacute;digo</td>
                                <td>T&iacute;tulo</td>
                                <td>Autor</td>
                                <td>Categor&iacute;a</td>
                                <td>Adquisici&oacute;n.</td>
                                <td>Ubicaci&oacute;n</td>
                                <td>Disponibilidad</td>
                                <td>Estatus</td>
                                <td>Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count=1;
                            foreach ($DaoLibros->showAll() as $libro) {
                                if($libro->getDisponible()==1){
                                  $disponibilidad="Disponible";
                                  $style='style="color:green;"';
                                }else{
                                  $disponibilidad="No Disponible";
                                  $style='style="color:red;"';
                                }

                                $aula=$DaoAulas->show($libro->getId_aula());
                                $cat=$DaoCategoriasLibros->show($libro->getId_cat());
 
                                 $status="Activo";
                                 $color="color:green;";
                                 if(strlen($libro->getBaja_libro())>0){
                                   $status="Baja";  
                                   $color="color:red;";
                                 }
                                
            ?>
                                <tr>
                                    <td onclick="mostrar(<?php echo $libro->getId(); ?>)"><?php echo $count;?></td>
                                    <td style="text-align: center;" onclick="mostrar(<?php echo $libro->getId(); ?>)"><?php echo $libro->getCodigo() ?></td>
                                    <td onclick="mostrar(<?php echo $libro->getId(); ?>)"><?php echo $libro->getTitulo(); ?></td>
                                    <td onclick="mostrar(<?php echo $libro->getId(); ?>)"><?php echo $libro->getAutor() ?></td>
                                    <td><?php echo $cat->getNombre(); ?></td>
                                    <td><?php echo $libro->getFecha_adq() ?></td>
                                    <td style="text-align:center;"><?php echo $aula->getClave_aula();?></td>
                                    <td <?php echo $style;?>><?php echo $disponibilidad;?></td>
                                    <td style="text-align:center;<?php echo $color?>"><?php echo $status;?></td>
                                    <td>
                                        <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                                        <div class="box-buttom">
                                            <button onclick="delete_libro(<?php echo $libro->getId(); ?>)">Eliminar</button><br>
                                            <button onclick="generar_etiqueta(<?php echo $libro->getId(); ?>)">Etiqueta</button><br>
                                            <a href="historial_prestamo_libro.php?id=<?php echo $libro->getId();;?>" target="_blank"><button>Hist. Pres</button></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="libro.php" class="link">Nuevo</a></li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<?php
write_footer();
