<?php
require_once('estandares/includes.php');
require_once('estandares/class_interesados.php');
require_once('estandares/class_materias.php');
require_once('estandares/class_grupos.php');
require_once('estandares/class_ciclos.php');
require_once("estandares/class_justificaciones.php");


require_once('clases/DaoAlumnos.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoCiclosAlumno.php');

$DaoAlumnos= new DaoAlumnos();

links_head("Asistencias grupo | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Asistencias</h1>
                </div>
                <span class="spanfiltros" onclick="mostrar_filtro()">Mostrar filtros</span>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td style="width: 80px">CLAVE GRUPAL</td>
                                <td style="width: 120px">MATERIA</td>
                                <td>TURNO</td>
                                <td>Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 50px">Faltas</td>
                                <td style="width: 50px">Asistencias</td>
                                <td style="width: 50px">Justificaciones</td>
                                <td>Porcentaje</td>
                                <td>Estatus</td>
                                <td>Calificaci&oacute;n</td>
                                <td>Evaluaci&oacute;n<br> Extraordinaria</td>
                                <td>Evaluaci&oacute;n<br> Especial</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                    $class_grupos= new class_grupos($_REQUEST['id']);
                                    $grupo=$class_grupos->get_grupo();
                                    $class_materias = new class_materias($grupo['Id_mat']);
                                    $mat = $class_materias->get_materia();
                                    $mat_esp=$class_materias->get_esp_mat($grupo['Id_mat_esp']);
                                    
                                        $turno="";
                                        if($grupo['Turno']==1){
                                           $turno="MATUTINO";
                                        }if($grupo['Turno']==2){
                                           $turno="VESPERTINO";
                                        }if($grupo['Turno']==3){
                                           $turno="NOCTURNO";
                                        }
                                    foreach($grupo['Alumnos'] as $k=>$v){
                                        $class_interesados= new class_interesados($v['Id_alum']);
                                        $alumn=$class_interesados->get_interesado();
                                        
                                        $asis=$class_grupos->get_asistencias_grupo_alum($grupo['Id_grupo'],$v['Id_alum']);
                                        $total= $class_grupos->porcentaje_asistencias($asis['Asistencias'],$asis['Justificaciones'],$asis['Faltas']);

                                        $colorCalTotalParciales="red";
                                        if($v['CalTotalParciales']>=$mat['Promedio_min']){
                                            $colorCalTotalParciales="green";
                                        }
                                        $colorCalExtraordinario="red";
                                        if($v['CalExtraordinario']>=$mat['Promedio_min']){
                                            $colorCalExtraordinario="green";
                                        }
                                        $colorCalEspecial="red";
                                        if($v['CalEspecial']>=$mat['Promedio_min']){
                                            $colorCalEspecial="green";
                                        }
                                     $NombreMat=$mat['Nombre'];
                                     if(strlen($mat_esp['NombreDiferente'])>0){
                                        $NombreMat=$mat_esp['NombreDiferente']; 
                                     }
                                    ?>
                                    <tr>
                                        <td><?php echo $grupo['Clave']?> </td>
                                        <td style="width: 120px;"><?php echo $NombreMat?></td>
                                        <td><?php echo $turno;?></td>
                                        <td><?php echo $alumn['Matricula']?></td>
                                        <td><?php echo $alumn['Nombre_ins']." ".$alumn['ApellidoP_ins']." ".$alumn['ApellidoM_ins']?></td>
                                        <td style="text-align: center;color: red;"><?php echo $asis['Faltas'];?></td>
                                        <td style="text-align: center;color: green;"><?php echo $asis['Asistencias']; ?></td>
                                        <td style="text-align: center; color: black;"><?php echo $asis['Justificaciones']; ?></td>
                                        <td style="text-align: center; color: black;"><?php echo number_format($total, 2) ?>%</td>
                                        <td <?php if($total<80){?> class="reprobado" <?php }else{ ?> class="aprobado"  <?php }?>>
                                            <?php
                                            if($total<80){
                                            ?>
                                            <span>SD</span>
                                            <?php
                                            }else{
                                            ?>
                                            <span>D</span>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td style="text-align: center;color: <?php echo $colorCalTotalParciales;?>;"><?php echo $DaoAlumnos->redonderCalificacion($v['CalTotalParciales'])?></td>
                                        <td style="text-align: center;color: <?php echo $colorCalExtraordinario;?>;"><?php echo $DaoAlumnos->redonderCalificacion($v['CalExtraordinario'])?></td>
                                        <td style="text-align: center;color: <?php echo $colorCalEspecial;?>;"><?php echo $DaoAlumnos->redonderCalificacion($v['CalEspecial'])?></td>
                                    </tr>
                                    <?php
                                    $count++;
                                    }
                                    ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
        <p>Estatus:<br>
            <select id="status">
                <option value="0"></option>
              <option value="1">Con derecho</option>
              <option value="2">Sin derecho</option>
            </select>
        </p>
        <p>Asistencias<br><input type="number" id="asis"/>
        <p>Faltas<br><input type="number" id="faltas"/>
        <p>Justificaciones<br><input type="number" id="justificaciones"/>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<input type="hidden" id="Id_grupo" value="<?php echo $_REQUEST['id'];?>"/>
<?php
write_footer();

