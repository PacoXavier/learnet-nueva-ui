<?php
require_once('clases/DaoPlanteles.php');
require_once('clases/DaoUsuarios.php');
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="es" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="es" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="es" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="es" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<html lang="es" class="no-js">

    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <!--[if IE]><![endif]--> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Login</title>
        <meta name="description" content="">
        <meta name="keywords" content="" />
        <meta name="author" content="GeneraWeb">
        <meta name="viewport" content="width=980, initial-scale=1.0">

        <!-- !CSS -->
        <link rel="stylesheet" href="css/reset_password.css?v=2011.5.5.13.35">
        <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    </head>
    <!-- !Body -->
    <body>
        <div id="errorLayer"></div>
        <div id="container">
            <div id="contenido">
                <div id="box_login">
                    <?php
                    $DaoPlanteles = new DaoPlanteles();
                    $DaoUsuarios = new DaoUsuarios();
                    $imgLogo = "";
                    $Nombre = "";
                    $server = $_SERVER["SERVER_NAME"];
                    if (strpos($server, "www") !== false) {
                        $server = substr($server, strpos($server, "www") + 4);
                    }
                    foreach ($DaoPlanteles->showAll() as $plantel) {
                        if ($server == $plantel->getDominio()) {
                            $imgLogo = 'src="files/' . $plantel->getId_img_logo() . '.jpg"';
                            $Nombre = $plantel->getNombre_plantel();
                        }
                    }
                    ?>
                    <p><img <?php echo $imgLogo ?>></p>
                    <h1>Restablecer contrase&ntilde;a</h1>
                    <?php
                    if (strlen($_REQUEST['key']) > 0) {
                        //Verificamos si es usuario o es docente
                        $usu = $DaoUsuarios->getRecoverUsu($_REQUEST['key']);
                        if ($usu->getId() > 0) {
                            $email = $usu->getEmail_usu();
                            $tipo = "usu";
                            ?>
                            <p>Por favor elige una nueva contrase&ntilde;a para iniciar sesi&oacute;n <b><?php echo $email ?></b></p>
                            <div class="box_pass"><input type="password" id="pass"  autocomplete="off"></div>
                            <div class="box_pass"><input type="password" id="confir"  autocomplete="off"></div>
                            <p><button id="buttonLogin" onclick="reset_password('<?php echo $tipo; ?>')">Reestablecer</button></p>
                            <p class="error"></p>
                            <?php
                        } else {
                            ?>
                            <p class="error">La llave proporcionada no es v&aacute;lida <a href="index.php">regresar</a></p>
                            <?php
                        }
                    } else {
                        ?>
                        <p class="error">La llave proporcionada no es v&aacute;lida <a href="index.php">regresar</a></p>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <!--!/#contenido -->
            <div id="box_img_foot"><img src="images/img_foot.png" alt="img_foot" width="1187" height="21">
                <p>&copy; <?php echo date('Y') ?> <?php echo $Nombre; ?></p>
            </div>
        </div>
        <input type="hidden" id="key" value="<?php echo $_REQUEST['key'] ?>"/>
        <!--!/#container -->
        <footer> </footer>
        <!-- !Javascript - at the bottom for fast page loading -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="js/reset_password.js?a=2"></script>
    </body>
</html>