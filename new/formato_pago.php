<?php
require_once('require_daos.php');
require_once('estandares/Fpdf/fpdf.php');
require_once('estandares/Fpdf/fpdi.php');
require_once('estandares/cantidad_letra.php');
    
if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0 && isset($_REQUEST['id_ofe']) && $_REQUEST['id_ofe'] > 0) {
    $base= new base();
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoPagos = new DaoPagos();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoAlumnos = new DaoAlumnos();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoCiclos = new DaoCiclos();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoRecargos = new DaoRecargos();
    $DaoMetodosPago = new DaoMetodosPago();
    $DaoPlanteles = new DaoPlanteles();
    $DaoUsuarios = new DaoUsuarios();
    $DaoDocentes = new DaoDocentes();

    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $plantel = $DaoPlanteles->show($usu->getId_plantel());

    if (strlen($plantel->getFormato_inscripcion()) > 0) {
        // initiate FPDI 
        $pdf = new FPDI();

        // add a page 
        $pdf->AddPage();

        // set the sourcefile 
        $pdf->setSourceFile('files/'.$plantel->getFormato_inscripcion().'.pdf');

        // import page 1 
        $tplIdx = $pdf->importPage(1);

        // use the imported page and place it at point 10,10 with a width of 100 mm 
        $pdf->useTemplate($tplIdx);
        
        //Id_ofe_alum
        $alum = $DaoAlumnos->show($_REQUEST['id']);
        $ofe_alum = $DaoOfertasAlumno->show($_REQUEST['id_ofe']);

        $ofe = $DaoOfertas->show($ofe_alum->getId_ofe());
        $esp = $DaoEspecialidades->show($ofe_alum->getId_esp());
        $nombreOri="";
        if ($ofe_alum->getId_ori() > 0) {
            $ori = $DaoOrientaciones->show($ofe_alum->getId_ori());
            $nombreOri=$ori->getNombre();
        }

        $nombre=$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
        $nombre=$base->covertirCadena($nombre, 2);

        $primerCiclo=$DaoCiclosAlumno->getPrimerCicloOferta($ofe_alum->getId());
        
        //Checamos si ya pago su primera inscripcion
        $pago=$DaoPagosCiclo->getPagoInscripcionCiclo($primerCiclo->getId());

        //ciclo del adeudo
        $ciclo=$DaoCiclos->show($primerCiclo->getId_ciclo());

        // now write some text above the imported page 
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->SetXY(42, 77);
        $pdf->Write(10, utf8_decode($nombre));

        $mensualidad = $pago->getMensualidad() - $pago->getDescuento() - $pago->getDescuento_beca();
        $pdf->SetXY(123, 97);
        $pdf->Write(10, "$ " . number_format($mensualidad, 2));
        $pdf->SetXY(123, 105);
        $pdf->MultiCell(50, 3, strtoupper(num2letras($mensualidad, 2)), 0, 'L');

        //Pagos
        $str=$pago->getConcepto() . "/ " . $ofe->getNombre_oferta() . " - " . $esp->getNombre_esp() . " " . $nombreOri . " " . $ciclo->getClave();
        $pdf->SetXY(42, 111);
        $pdf->MultiCell(50, 3, utf8_decode(mb_strtoupper($str,"UTF-8")), 0, 'L');

        $pdf->Output('formato_pago.pdf', 'I');
    }
} else {
    header("Location: interesados.php");
}
