<?php
require_once('estandares/includes.php');
require_once('require_daos.php');
$DaoDocentes = new DaoDocentes();
$DaoCiclos = new DaoCiclos();
$DaoEvaluacionDocente = new DaoEvaluacionDocente();
$DaoCategoriasEvaluacion = new DaoCategoriasEvaluacion();
$DaoCamposCategoria = new DaoCamposCategoria();
$DaoCategoriasPago = new DaoCategoriasPago();
$DaoResultadosEvaluacion= new DaoResultadosEvaluacion();
$DaoPuntosPorCategoriaEvaluadaDocente= new DaoPuntosPorCategoriaEvaluadaDocente();
$DaoNivelesPago= new DaoNivelesPago();

$Id_docente = 0;
$Id_eva = 0;
$Id_ciclo=0;

$comentarios="";
links_head("Evaluaci&oacute;n Docente | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <?php
    if (isset($_REQUEST['id_doc']) && $_REQUEST['id_doc'] > 0) {
        $docente = $DaoDocentes->show($_REQUEST['id_doc']);
        $Id_docente = $docente->getId();
        if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
            $evaluacion = $DaoEvaluacionDocente->show($_REQUEST['id']);
            $Id_eva = $evaluacion->getId();
            $comentarios=$evaluacion->getComentarios();
            $Id_ciclo=$evaluacion->getId_ciclo();
        } else {
            $evaluacion = $DaoEvaluacionDocente->getUltimaEvaluacionDocente($_REQUEST['id_doc']);
            $Id_eva = $evaluacion->getId();
            $comentarios=$evaluacion->getComentarios();
            $Id_ciclo=$evaluacion->getId_ciclo();
        }
    }
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-tasks"></i> Evaluaci&oacute;n Docente</h1>
                </div>
                <?php
                if ($Id_docente > 0) {
                    ?>
                    <div class="seccion">
                        <h2 style="font-size: 15px;margin-bottom: 15px;"><?php echo $docente->getClave_docen() . "  - " . $docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . " " . $docente->getApellidoM_docen() ?></h2>
                        <ul class="form">
                            <li>Ciclo:<br>
                                <select id="Id_ciclo">
                                    <option value="0"></option>
                                    <?php
                                    foreach ($DaoCiclos->showAll() as $ciclo) {
                                        ?>
                                        <option value="<?php echo $ciclo->getId() ?>" <?php if ($Id_ciclo == $ciclo->getId() && $Id_docente > 0) { ?> selected="selected" <?php } ?> ><?php echo $ciclo->getClave() ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </li>
                        </ul>
                    </div>
                    <?php
                    $totalCategorias=0;
                    foreach ($DaoCategoriasEvaluacion->getCategoriasEvaluacion() as $cat) {
                        ?>
                        <div class="seccion categorias-evaluacion" id-cat-eva="<?php echo $cat->getId()?>">
                            <h2><?php echo $cat->getNombre() ?></h2>
                            <span class="linea"></span>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <?php
                                        foreach ($DaoCamposCategoria->getCamposCategoria($cat->getId()) as $camp) {
                                            ?>
                                            <td><?php echo $camp->getNombre(); ?></td>
                                            <?php
                                        }
                                        ?>
                                        <td class="puntos">Puntos</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php
                                        $totalPorCategoria = 0;
                                        foreach ($DaoCamposCategoria->getCamposCategoria($cat->getId()) as $camp) {
                                            if ($camp->getTipo_camp() == 1) {
                                                $tipoCampo = "text";
                                            } elseif ($camp->getTipo_camp() == 2) {
                                                $tipoCampo = "checkbox";
                                            }
                                            $checked='';
                                            $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($Id_eva,$camp->getId());
                                            if($resultado->getPuntos()>0 && $camp->getTipo_camp()==2){
                                               $checked='checked="checked"'; 
                                            }
                                            ?>
                                             <td> 
                                                 <input 
                                                     type="<?php echo $tipoCampo; ?>" 
                                                     <?php echo $checked?> 
                                                     class="<?php echo $camp->getNombre(); ?>"  
                                                     valor-maximo-campo="<?php echo $camp->getValor(); ?>" 
                                                     value="<?php echo $resultado->getPuntos();?>"
                                                     onkeyup="updateCampos()"
                                                     onclick="updateCampos()"
                                                     valor-id="<?php echo $camp->getId()?>"
                                                 />
                                             </td>
                                            <?php
                                            $totalPorCategoria+=$resultado->getPuntos();
                                        }
                                        ?>
                                        <td class="puntos"><span class="puntos-cat-evaluacion"><?php echo round($totalPorCategoria); ?></span></td>
                                    </tr>
                                </tbody>
                            </table>
                            <input type="hidden" value="<?php echo round($totalPorCategoria); ?>" class="total-cat"/>
                        </div>
                        <?php
                        $totalCategorias+=$totalPorCategoria;
                    }
                    ?>
                    <div class="seccion">
                        <h2>Comentarios</h2>
                        <span class="linea"></span>
                        <textarea id="comentarios"><?php echo $comentarios ?></textarea></p>
                        <input type="hidden" value="<?php echo round($total_cualitativos); ?>" id="puntos_cualitativos"/>
                        <button class="btns btns-primary btns-lg" onclick="save_evaluacion()">Guardar</button>
                    </div>
                    <?php
                }
                ?>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <h2><img src="images/flecha_blue.png" alt="flecha_blue" width="6" height="11" />Puntajes</h2>
                <ul id="ul-categorias">
                    <li><span>Puntos:   <b id="puntos_totales"><?php echo round($totalCategorias); ?> </b></span></li>
                    <?php
                    foreach ($DaoCategoriasPago->getCategoriasPago() as $cat) {
                        $resul=$DaoPuntosPorCategoriaEvaluadaDocente->getNivelPorCategoria($Id_eva,$cat->getId());
                        $nombreNIvel="";
                        if($resul->getId()>0){
                           $nivel=$DaoNivelesPago->show($resul->getId_nivel());
                           $nombreNIvel=$nivel->getNombre_nivel();
                        }
                        ?>
                        <li><span><?php echo $cat->getNombre_tipo() ?>   <b><?php echo $nombreNIvel; ?></b></span><input type="hidden" class="resultado_nivel_por_categoria"  value="<?php echo $resul->getId_nivel() ?>"/></li>
                        <?php
                    }
                    if ($Id_docente > 0) {
                        ?>
                        <li><a href="evaluaciones_docente.php?id=<?php echo $Id_docente; ?>">Regresar</a></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_docente" value="<?php echo $Id_docente; ?>"/>
<input type="hidden" id="Id_eva" value="<?php echo $Id_eva ?>"/>
<input type="hidden" id="puntos_totales_evaluacion" value="<?php echo round($total); ?>"/>
<?php
write_footer();
