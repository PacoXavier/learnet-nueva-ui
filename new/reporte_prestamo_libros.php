 <?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoLibros.php');
require_once('clases/DaoHistorialPrestamoLibro.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoCiclos.php');

$base= new base();
$DaoLibros= new DaoLibros();
$DaoHistorialPrestamoLibro= new DaoHistorialPrestamoLibro();
$DaoAlumnos= new DaoAlumnos();
$DaoDocentes= new DaoDocentes();
$DaoUsuarios= new DaoUsuarios();

links_head("Historial de prestamo | ULM");
?>
<link rel="stylesheet" media="print" href="css/reporte_prestamo_libro_print.css?v=2011.6.16.18.36"> 
<?php
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-history"></i> Historial de prestamo de libros</h1>
                </div>
                <div class="box-filter-reportes">
                    <ul class="row list-design">
                        <li class="col-md-4 widget" onclick="mostrar_filtro()"><div class="stats-left"s><i class="fa fa-filter"></i> Filtros</div></li>
                        <li class="col-md-4 widget states-mdl" onclick="download_excel()"><div class="stats-left"s><i class="fa fa-download"></i> Descargar</div></li>
                        <li class="col-md-4 widget states-last" onclick="imprimir()"><div class="stats-left"s><i class="fa fa-download"></i> Imprimir</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Código</td>
                                <td>Título</td>
                                <td>Usuario</td>
                                <td>Solicitante</td>
                                <td>Fecha de prestamo</td>
                                <td>Fecha de entrega</td>
                                <td>fecha de recibido</td>
                                <td>Comentarios</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach($DaoHistorialPrestamoLibro->getPrestamosLibro() as $historial) {
                                $libro=$DaoLibros->show($historial->getId_libro());
                                $usuEntrega = $DaoUsuarios->show($historial->getUsu_entrega());

                                if($historial->getTipo_usu()=="usu"){
                                    $usuRecibe=$DaoUsuarios->show($historial->getUsu_recibe());
                                    $Nombreusu=$usuRecibe->getNombre_usu()." ".$usuRecibe->getApellidoP_usu()." ".$usuRecibe->getApellidoM_usu();
                                }elseif($historial->getTipo_usu()=="docen"){
                                    $usuRecibe=$DaoDocentes->show($historial->getUsu_recibe());
                                    $Nombreusu=$usuRecibe->getNombre_docen()." ".$usuRecibe->getApellidoP_docen()." ".$usuRecibe->getApellidoM_docen();
                                }elseif($historial->getTipo_usu()=="alum"){
                                    $usuRecibe=$DaoAlumnos->show($historial->getUsu_recibe());
                                    $Nombreusu=$usuRecibe->getNombre()." ".$usuRecibe->getApellidoP()." ".$usuRecibe->getApellidoM();     
                                }
                                ?>
                                <tr>
                                    <td><?php echo $historial->getId(); ?></td>
                                    <td><?php echo $libro->getCodigo() ?></td>
                                    <td style="width: 115px;"><?php echo $libro->getTitulo() ?></td>
                                    <td><?php echo $usuEntrega->getNombre_usu()." ".$usuEntrega->getApellidoP_usu()." ".$usuEntrega->getApellidoM_usu(); ?></td>
                                    <td><?php echo $Nombreusu; ?></td>
                                    <td><?php echo $historial->getFecha_entrega(); ?></td>
                                    <td><?php echo $historial->getFecha_devolucion(); ?></td>
                                    <td><?php echo $historial->getFecha_entregado(); ?></td>
                                    <td><?php echo $historial->getComentarios(); ?></td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
        <div class="boxUlBuscador">
            <p>Código<br><input type="search"  class="buscarFiltro" onkeyup="buscarLibro(this)" id="libro"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p>Fecha inicio de prestamo<br><input type="date" id="fecha_ini"/>
        <p>Fecha fin de prestamo<br><input type="date" id="fecha_fin"/>
        <div class="boxUlBuscador">
            <p>Usuario<br><input type="search"  class="buscarFiltro" onkeyup="buscarUsu(this)" id="usuario"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();



