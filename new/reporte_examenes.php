<?php
require_once('estandares/includes.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoDiasInhabiles.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoAulas.php');
require_once('clases/DaoHoras.php');
require_once('clases/DaoGrupos.php');
require_once('clases/DaoHorarioExamenGrupo.php');
require_once('clases/DaoDocentes.php');
require_once('clases/modelos/HorarioExamenGrupo.php');
require_once('clases/DaoPeriodosExamenes.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoDocenteExamen.php');
require_once('clases/DaoGrupoExamen.php');

$base= new base();
$DaoHoras= new DaoHoras();
$DaoGrupos= new DaoGrupos();
$DaoAulas= new DaoAulas();
$DaoDocentes= new DaoDocentes();
$DaoHorarioExamenGrupo= new DaoHorarioExamenGrupo();
$DaoMaterias= new DaoMaterias();
$DaoCiclos= new DaoCiclos();
$DaoPeriodosExamenes= new DaoPeriodosExamenes();
$DaoUsuarios=new DaoUsuarios();
$DaoGrupoExamen= new DaoGrupoExamen();
$DaoDocenteExamen= new DaoDocenteExamen();

$ciclo=$DaoCiclos->getActual($_usu->getId_plantel());
links_head("Horario de exámenes | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-clock-o"></i> Horario de exámenes </h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul class="row" style="list-style: none; font-size: 20px;">
                        <li class="col-md-6 weather-grids widget-shadow" onclick="mostrar_filtro()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-filter"></i> Filtros</div></li>
                        <!--<li onclick="mostrar_box_email()"><i class="fa fa-envelope"></i> Email</li>-->
                        <li class="col-md-6 weather-grids weather-right widget-shadow states-last" onclick="download_excel()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-download"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Docente(s)</td>
                                <td>Grupo(s)</td>
                                <td>Materia</td>
                                <td>Aula</td>
                                <td class="td-center">Fecha de exámen</td>
                                <td class="td-center">Hora inicio</td>
                                <td class="td-center">Hora Fin</td>
                                <td class="td-center">Ciclo</td>
                                <td>Período de exámen</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count=1;
                            foreach ($DaoHorarioExamenGrupo->getExamenesCiclo($ciclo->getId()) as $examen){
                                     $per=$DaoPeriodosExamenes->show($examen->getId_periodoExamen());
                                     $grupos=array();
                                     $docentes=array();
                                     foreach($DaoGrupoExamen->getGruposExamenHorario($examen->getId()) as $ObjGrupo){
                                               $grupo=$DaoGrupos->show($ObjGrupo->getIdGrupo());
                                               $clave=$DaoGrupos->covertirCadena($grupo->getClave(), 1);
                                               array_push($grupos, $clave);
                                               $materia=$DaoMaterias->show($grupo->getId_mat());
                                     } 

                                     foreach($DaoDocenteExamen->getDocentesExamenHorario($examen->getId()) as $ObjDocente){
                                           $docente=$DaoDocentes->show($ObjDocente->getId_docen());
                                           $docenteNombre=$DaoDocentes->covertirCadena($docente->getNombre_docen()." ".$docente->getApellidoP_docen(),1);
                                           array_push($docentes, $docenteNombre);
                                     }

                                     $start=  substr($examen->getStart(),strpos($examen->getStart(), "T")+1);
                                     $end=  substr($examen->getEnd(),strpos($examen->getEnd(), "T")+1);
                                     $aula=$DaoAulas->show($examen->getId_aula());
                                     $c=$DaoCiclos->show($examen->getId_ciclo());
                                 ?>
                                         <tr>
                                           <td><?php echo $count;?></td>
                                           <td><?php echo implode(", ", $docentes)?></td>
                                           <td><?php echo implode(", ", $grupos)?></td>
                                           <td><?php echo $DaoDocentes->covertirCadena($materia->getNombre(),2)?></td>
                                           <td><?php echo $DaoDocentes->covertirCadena($aula->getNombre_aula(),2)?></td>
                                           <td class="td-center"><?php echo $examen->getFechaAplicacion() ?></td>
                                           <td class="td-center"><?php echo $start; ?></td>
                                           <td class="td-center"><?php echo $end; ?></td>
                                           <td class="td-center"><?php echo $c->getClave(); ?></td>
                                           <td><?php echo $per->getNombre(); ?></td>
                                         </tr>
                                         <?php
                                         $count++;
                                }
                                  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="boxUlBuscador col-xs-6">
            <p>Docente<br><input type="text" id="Id_docente" class="buscarFiltro form-control" onkeyup="buscarDocent(this)" /></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <div class="boxUlBuscado col-xs-6">
            <p>Grupo<br><input type="text" class="form-control" id="Id_grupo" class="buscarFiltro" onkeyup="buscarGruposCiclo(this)"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
    </div>
    <div class="row">
        <div class="boxUlBuscador col-xs-6">
            <p>Materia<br><input type="text" class="form-control" id="Id_materia" class="buscarFiltro" onkeyup="buscarMateria(this)"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p class="col-xs-6">Aula<br>
          <select id="Id_aula" class="form-control">
              <option value="0"></option>
              <?php
              foreach($DaoAulas->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre_aula(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
    </div>
    <div class="row">
        <p class="col-xs-4">Ciclo<br>
          <select id="ciclo" class="form-control">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
              <option value="<?php echo $v->getId() ?>" <?php if($ciclo->getId()==$v->getId()){ ?> selected="selected" <?php } ?>><?php echo $v->getClave(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p class="col-xs-4">Fecha inicio:<br><input class="form-control" type="date" id="fechaIni"/></p>
        <p class="col-xs-4">Fecha inicio:<br><input class="form-control" type="date" id="fechaFin"/></p>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<?php
write_footer();
