<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

$base = new base();
$DaoOfertas = new DaoOfertas();
$DaoAlumnos = new DaoAlumnos();
$DaoEspecialidades = new DaoEspecialidades();
$DaoOrientaciones = new DaoOrientaciones();
$DaoOfertasAlumno = new DaoOfertasAlumno();
$DaoGrados = new DaoGrados();
$DaoCiclos = new DaoCiclos();
$DaoMaterias = new DaoMaterias();
$DaoDocentes = new DaoDocentes();
$DaoGrupos = new DaoGrupos();
$DaoMateriasCicloAlumno = new DaoMateriasCicloAlumno();
$DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
$DaoTurnos = new DaoTurnos();
$DaoEvaluacionDocente = new DaoEvaluacionDocente();
$DaoPagosCiclo= new DaoPagosCiclo();

$ciclo = $DaoCiclos->getActual();
links_head("Rentabilidad | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                   <h1><i class="fa fa-usd" aria-hidden="true"></i> Rentabilidad</h1>
                </div>
                <div class="box-filter-reportes">
                    <ul class="row" style="list-style: none; font-size: 20px;">
                        <li class="col-md-6 weather-grids widget-shadow" onclick="mostrar_filtro()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-filter"></i> Filtros</div></li>
                        <li class="col-md-6 weather-grids weather-right widget-shadow states-last" onclick="download_excel()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-download"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td style="width: 80px">CLAVE GRUPAL</td>
                                <td>Ciclo</td>
                                <td>MATERIA</td>
                                <td>Orientaci&oacute;n</td>
                                <td>TURNO</td>
                                <td style="text-align: center;">CAPACIDAD</td>
                                <td>Docente</td>
                                <td style="text-align: center;">Alumnos</td>
                                <td style="text-align: center;">Ingreso/mes</td>
                                <td style="text-align: center;">Egreso/mes</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($ciclo->getId()) {
                                foreach ($DaoGrupos->showGruposByCiclo($ciclo->getId()) as $k => $v) {
                                    $mat = $DaoMaterias->show($v->getId_mat());
                                    $mat_esp = $DaoMateriasEspecialidad->show($v->getId_mat_esp());

                                    $NombreMat = $mat->getNombre();
                                    if (strlen($mat_esp->getNombreDiferente()) > 0) {
                                        $NombreMat = $mat_esp->getNombreDiferente();
                                    }

                                    $tur = $DaoTurnos->show($v->getTurno());
                                    $turno = $tur->getNombre();

                                    $nombre_ori = "";
                                    if ($v->getId_ori() > 0) {
                                        $ori = $DaoOrientaciones->show($v->getId_ori());
                                        $nombre_ori = $ori->getNombre();
                                    }

                                    $ingresos = 0;
                                    $TotalAlumnos = count($DaoGrupos->getAlumnosGrupo($v->getId()));
                                    foreach ($DaoGrupos->getAlumnosBYGrupo($v->getId()) as $k2 => $v2) {
                                        $pagoCiclo=$DaoPagosCiclo->getPagoByIdCicloAlumn($v2['Id_ciclo_alum']);
                                        if($pagoCiclo->getId()>0){
                                           $totalMaterias = count($DaoMateriasCicloAlumno->getMateriasByIdOfeAlumnAndIdCiclo($v2['Id_ofe_alum'], $ciclo->getId())) . "<br>";
                                           $ingresos+=round($pagoCiclo->getMensualidad() - $pagoCiclo->getDescuento() - $pagoCiclo->getDescuento_beca()) / $totalMaterias;   
                                        }
                                    }

                                    $egreso = 0;
                                    $horas_trabajadas = 0;
                                    $Id_oferta = 0;
                                    $Nombre_docen = "";
                                    $resp = $DaoGrupos->getDocenteGrupo($v->getId());
                                    if ($resp['Id_docente'] > 0) {
                                        $query_Horario_docente = " SELECT 
                                                     Horario_docente.*,
                                                     Grupos.Clave,Grupos.Id_mat_esp,
                                                     Materias_especialidades.Id_esp,
                                                     especialidades_ulm.Id_ofe
                                                from Horario_docente 
                                                JOIN Grupos ON Horario_docente.Id_grupo=Grupos.Id_grupo
                                                JOIN Materias_especialidades ON  Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
                                                JOIN especialidades_ulm ON Materias_especialidades.Id_esp=especialidades_ulm.Id_esp
                                                WHERE Horario_docente.Id_docente=" . $resp['Id_docente'] . " AND Horario_docente.Id_ciclo=" . $ciclo->getId() . " AND Horario_docente.Id_grupo=" . $v->getId();
                                        foreach ($base->advanced_query($query_Horario_docente) as $k6 => $v6) {
                                            $Id_oferta = $v6['Id_ofe'];
                                            //Insertamos solo los dias que da el profesor
                                            if ($v6['Lunes'] == 1) {
                                                $horas_trabajadas++;
                                            }
                                            if ($v6['Martes'] == 1) {
                                                $horas_trabajadas++;
                                            }
                                            if ($v6['Miercoles'] == 1) {
                                                $horas_trabajadas++;
                                            }
                                            if ($v6['Jueves'] == 1) {
                                                $horas_trabajadas++;
                                            }
                                            if ($v6['Viernes'] == 1) {
                                                $horas_trabajadas++;
                                            }
                                            if ($v6['Sabado'] == 1) {
                                                $horas_trabajadas++;
                                            }
                                            if ($v6['Domingo'] == 1) {
                                                $horas_trabajadas++;
                                            }
                                        }

                                        //Es por mes
                                        $horas_trabajadas = ($horas_trabajadas * 4);
                                        $docen = $DaoDocentes->show($resp['Id_docente']);
                                        $Nombre_docen = $docen->getNombre_docen() . " " . $docen->getApellidoP_docen() . " " . $docen->getApellidoM_docen();
                                        $eva = $DaoEvaluacionDocente->getInfoEvaluacion($docen->getId(), $Id_oferta);
                                        $egreso = $horas_trabajadas * $eva['PagoHora'];
                                    }
                                    $classPrimer = "";
                                    if ($ingresos < $egreso) {
                                        $classPrimer = 'class="pink"';
                                    } elseif (($ingresos / 2) < $egreso) {
                                        $classPrimer = 'class="yellow"';
                                    }
                                    //rojo si no se alcaza a pagar al profe
                                    //amarillo si no contribulle gastos fijos
                                    ?>
                                    <tr <?php echo $classPrimer; ?>>
                                        <td><?php echo $v->getClave() ?> </td>
                                        <td><?php echo $ciclo->getClave() ?> </td>
                                        <td style="width: 120px;"><?php echo $NombreMat; ?></td>
                                        <td><?php echo $nombre_ori; ?></td>
                                        <td><?php echo $turno; ?></td>
                                        <td style="text-align: center;"><?php echo $v->getCapacidad() ?></td>
                                        <td><?php echo $Nombre_docen ?></td>
                                        <td style="color:black;font-weight: bold;text-align: center;"><?php echo $TotalAlumnos; ?></td>
                                        <td style="text-align: center;"><b>$<?php echo number_format($ingresos, 2); ?></b></td>
                                        <td style="text-align: center;"><b>$<?php echo number_format($egreso, 2); ?></b></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="boxUlBuscador col-md-6">
            <p>Docente<br><input type="search"  class="buscarFiltro form-control" onkeyup="buscarDocent()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p class="col-md-3">Ciclo<br>
            <select id="ciclo" class="form-control">
                <option value="0"></option>
                <?php
                foreach ($DaoCiclos->showAll() as $k => $v) {
                    ?>
                    <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave() ?></option>
                    <?php
                }
                ?>
            </select>
        </p>
        <p class="col-md-3">Turno<br>
            <select id="turno" class="form-control">
                <option value="0"></option>
                <?php
                foreach ($DaoTurnos->getTurnos() as $turno) {
                    ?>
                    <option value="<?php echo $turno->getId() ?>" ><?php echo $turno->getNombre() ?></option>
                    <?php
                }
                ?>
            </select>
        </p>
    </div>
    <div class="row">
        <p class="col-md-4">Oferta<br>
            <select id="oferta" class="form-control" onchange="update_curso_box_curso()">
                <option value="0"></option>
                <?php
                foreach ($DaoOfertas->showAll() as $k => $v) {
                    ?>
                    <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
                    <?php
                }
                ?>
            </select>
        </p>
        <p class="col-md-4">Especialidad:<br>
            <select id="curso" class="form-control" onchange="update_materias()">
                <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
        <p class="col-md-4">Materia:<br>
            <select id="lista_materias" class="form-control">
                <option value="0"></option>
            </select>
        </p>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<?php
write_footer();
