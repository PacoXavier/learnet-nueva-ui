<?php
require_once('estandares/includes.php');
if(!isset($perm['48'])){
  header('Location: home.php');
}
require_once('clases/DaoDocentes.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoHoras.php');
$DaoDocentes= new DaoDocentes();
$DaoCiclos= new DaoCiclos();
$DaoHoras= new DaoHoras();
$cicloActual=$DaoCiclos->getActual($_usu->getId_plantel());

links_head("Horario de exámen | ULM");
?>
<link rel="stylesheet" href="js/fullcalendar/lib/cupertino/jquery-ui.min.css">
<link rel='stylesheet' href='js/fullcalendar/fullcalendar.css' />
<link href="js/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">
<?php
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-clock-o"></i> Horario de exámenes</h1>
                </div>
                <div id="box_uno">
                    <ul class="form">
                            <li>Ciclo<br><select id="ciclo" onchange="updateCalendar()">
                                    <option value="0">Selecciona el ciclo</option>
                                    <?php
                                        foreach($DaoCiclos->showAll() as $ciclo){
                                         ?>
                                    <option value="<?php echo $ciclo->getId() ?>" <?php if($cicloActual->getId()==$ciclo->getId()){ ?> selected="selected" <?php }?>><?php echo $ciclo->getClave() ?></option>
                                          <?php
                                          }
                                          ?>
                                    </select>
                            </li> 
                    </ul>
                    <div class="box-filter-reportes">
                        <ul>
                            <li><div class="box inhabiles"></div>Días inhábiles</li>
                            <li><div class="box disponibles"></div>Período exámen</li>
                            <li><div class="box vacaciones"></div>Período vacaciones</li>
                            <li><div class="box asignadas"></div>Exámenes</li>
                        </ul>
                    </div>
                </div>
                <div id="mascara_tabla">
                    <div id="calendar" class="fc fc-ltr fc-unthemed"></div>
                </div>
            </div>
        </td>
    </tr>
</table>
<?php
write_footer();
?>

<script src="js/fullcalendar/lib/moment.min.js"></script>
<!--<script src="js/fullcalendar/lib/jquery.min.js"></script>-->
<script src="js/fullcalendar/fullcalendar.min.js"></script>
<script src='js/fullcalendar/fullcalendar.js'></script>
<script src='js/fullcalendar/lang/es.js'></script>

<!--
http://jonthornton.github.io/jquery-timepicker/
http://jonthornton.github.io/Datepair.js/
-->


<script type="text/javascript" src="js/timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/jquery.timepicker.css">
<script type="text/javascript" src="js/timepicker/lib/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/lib/bootstrap-datepicker.css">
<script type="text/javascript" src="js/timepicker/lib/site.js"></script>
<script type="text/javascript" src="js/Datepair/dist/datepair.js"></script>
<script type="text/javascript" src="js/Datepair/dist/jquery.datepair.js"></script>
