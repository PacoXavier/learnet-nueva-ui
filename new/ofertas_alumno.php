<?php
require_once('estandares/includes.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoPagosCiclo.php');
require_once('clases/DaoCiclosAlumno.php');
require_once('clases/DaoCiclos.php');

$DaoOfertasAlumno = new DaoOfertasAlumno();
$DaoOfertas = new DaoOfertas();
$DaoAlumnos = new DaoAlumnos();
$DaoEspecialidades = new DaoEspecialidades();
$DaoOrientaciones = new DaoOrientaciones();
$DaoPagosCiclo = new DaoPagosCiclo();
$DaoCiclosAlumno = new DaoCiclosAlumno();
$DaoCiclos = new DaoCiclos();

links_head("Ofertas del alumno | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1>Ofertas del alumno</h1>
                </div>
                <div class="seccion">
                    <ul class="widget filtros list-design form">
                        <li style="width: 600px; display: flex; align-items: center; justify-content: center">Buscar<br><input class="form-control" style="margin-left: 10px;" type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o matricula"/>

                        </li>

                    </ul>
                    <div> <ul id="buscador_int" class="list-design"></ul></div>
                </div>
                <?php
                if ($_REQUEST['id'] > 0) {
                    $alum = $DaoAlumnos->show($_REQUEST['id']);
                    ?>
                    <div class="seccion panel-body widget-shadow tables" id="list_ofertas">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>Matricula</td>
                                    <td class="normal"><?php echo $alum->getMatricula() ?></td>
                                    <td>Nombre</td>
                                    <td colspan="6" class="normal"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
                                </tr>
                                <tr>
                                    <td>Nivel</td>
                                    <td>Carrera</td>
                                    <td>Orientaci&oacute;n</td>
                                    <td>Beca</td>
                                    <td>Precio</td>
                                    <td>Inscripción</td>
                                    <td>Pagos</td>
                                    <td>Mensualidad</td>
                                    <td>Acciones</td>
                                </tr>

                            </thead>
                            <tbody>
                                <?php
                                if (count($DaoOfertasAlumno->getOfertasAlumno($alum->getId())) > 0) {
                                    foreach ($DaoOfertasAlumno->getOfertasAlumno($alum->getId()) as $k => $v) {

                                        $nombre_ori = "";
                                        $oferta = $DaoOfertas->show($v->getId_ofe());
                                        $esp = $DaoEspecialidades->show($v->getId_esp());
                                        if ($v->getId_ori() > 0) {
                                            $ori = $DaoOrientaciones->show($v->getId_ori());
                                            $nombre_ori = $ori->getNombre();
                                        }

                                        $PrimerCicloAlumno = $DaoCiclosAlumno->getPrimerCicloOferta($v->getId());
                                        $UltimoCicloAlumno = $DaoCiclosAlumno->getLastCicloOferta($v->getId());

                                        $NumPagosPrimerCiclo = count($DaoPagosCiclo->getCargosCiclo($PrimerCicloAlumno->getId()));
                                        $NumPagosUltimoCiclo = count($DaoPagosCiclo->getCargosCiclo($UltimoCicloAlumno->getId()));

                                        if ($NumPagosPrimerCiclo > 0) {
                                            $Id_ciclo_alum = $PrimerCicloAlumno->getId();
                                        } elseif ($NumPagosUltimoCiclo > 0) {
                                            $Id_ciclo_alum = $UltimoCicloAlumno->getId();
                                        } else {
                                            $Id_ciclo_alum = 0;
                                        }

                                        if ($Id_ciclo_alum > 0) {
                                            $Precio_curso = ($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos();
                                            ?>
                                            <tr>
                                                <td><?php echo $oferta->getNombre_oferta(); ?></td>
                                                <td style="width: 200px;"><?php echo $esp->getNombre_esp() . " " . $nombre_ori; ?></td>
                                                <td><?php echo $nombre_ori; ?></td>
                                                <td><?php echo number_format($UltimoCicloAlumno->getPorcentaje_beca(), 2) ?> %</td>
                                                <td><?php echo "$" . number_format($esp->getPrecio_curso(), 2) ?></td>
                                                <td><?php echo "$" . number_format($esp->getInscripcion_curso(), 2) ?></td>
                                                <td><?php echo $esp->getNum_pagos() ?></td>
                                                <td><?php echo "$" . number_format($Precio_curso, 2) ?></td>
                                                <td style="width:110px;">
                                                    <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                                                    <div class="box-buttom">
                                                        <?php
                                                        if (isset($perm['35'])) {
                                                            if ($oferta->getTipoOferta() == 1) {
                                                                ?>
                                                                <a href="pago.php?id=<?php echo $v->getId() ?>" target="_blank"><button>Cobranza</button></a><br>
                                                                <?php
                                                            } elseif ($oferta->getTipoOferta() == 2) {
                                                                ?>
                                                                <a href="cargos_curso.php?id=<?php echo $v->getId() ?>" target="_blank"><button>Cobranza</button></a><br>
                                                                <?php
                                                            }
                                                        }
                                                        //Si tiene ya generados los cargos o es opcion de pagos por materia debera de por lo menos a ver pagado la inscripcion
                                                        if (($NumPagosPrimerCiclo > 1 || $NumPagosUltimoCiclo > 1) || ($v->getOpcionPago() == 1 && $DaoPagosCiclo->verificarPagoInscripcion($v->getId()) == 1)) {
                                                            if (isset($perm['58'])) {
                                                                ?>
                                                                <a href="asignar_materias.php?id=<?php echo $v->getId() ?>" target="_blank"><button>Asignar materias</button></a><br>
                                                                <?php
                                                            }
                                                            if (isset($perm['63'])) {
                                                                ?>
                                                                <a href="acreditar_materias.php?id=<?php echo $v->getId() ?>" target="_blank"><button>Acreditar materias</button></a>
                                                                <?php
                                                            }
                                                            if (isset($perm['65'])) {
                                                                ?>
                                                                <!--<a href="pagos_adelantados.php?id=<?php echo $v->getId() ?>" target="_blank"><button>Adelantar pago</button></a>-->
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
            </div>
        </td>
<!--        <td id="column_two ">-->
<!--            <div id="box_menus ">-->
<!--                --><?php
//                require_once 'estandares/menu_derecho.php';
//                ?>
<!--                <ul>-->
<!--                    <li><a href="interesados.php" class="link">Interesados </a></li>-->
<!--                    <li><a href="alumnos.php" class="link">Alumnos </a></li>-->
<!--                </ul>-->
<!--            </div>-->
<!--        </td>-->
    </tr>
</table>
<?php
write_footer();
