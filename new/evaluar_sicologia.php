<?php
require_once('estandares/includes.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoCategoriasSicologia.php');
require_once('clases/DaoCamposSicologia.php');
require_once('clases/DaoAnalisisSicologico.php');

$DaoCategoriasSicologia= new DaoCategoriasSicologia();
$DaoCamposSicologia= new DaoCamposSicologia();
$DaoAnalisisSicologico= new DaoAnalisisSicologico();
$DaoAlumnos= new DaoAlumnos();
links_head("Evaluar sicologia | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
  <?php
  if ($_REQUEST['id'] > 0) {
      $alum=$DaoAlumnos->show($_REQUEST['id']);
  ?>
  <tr>
    <td id="column_one">
      <div class="fondo">
        <div id="box_top">
          <div class="foto_alumno" 
          <?php if (strlen($alum->getImg_alum()) > 0) { ?> 
                 style="background-image:url(files/<?php echo $alum->getImg_alum() ?>.jpg) <?php } ?>">
          </div>
            <h1><?php echo ucwords(strtolower($alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM())) ?></h1>
        </div>
        <?php
        foreach($DaoCategoriasSicologia->showAll() as $k=>$v){
        ?>
        <div class="seccion">
              <h2><?php echo $v->getNombre()?></h2>
              <span class="linea"></span>
              <ul class="form">
                  <?php 
                  foreach($DaoCamposSicologia->showByCategoria($v->getId())as $k2=>$v2){
                      $campo=$DaoAnalisisSicologico->showByAlumCampo($_REQUEST['id'],$v2->getId());
                  ?>
                  <li><?php echo $v2->getNombre()?><br><input type="number" id="<?php echo $v2->getId()?>" class="campos" value="<?php echo $campo->getValor_camp()?>"/></li>
                  <?php
                  }
                  ?>
              </ul>
        </div>
        <?php
        }
        ?>
         <button id="button_ins" onclick="save_eva()">Guardar</button>
      </div>
    </td>
    <td id="column_two">
      <div id="box_menus">
        <table id="usu_login">
          <tr>
            <td><div class="nom_usu"><div id="img_usu" 
              <?php if (strlen($usu['Img_usu']) > 0) { ?> style="background-image:url(files/<?php echo $usu['Img_usu'] ?>.jpg) <?php } ?>"></div>
              <?php echo $usu['Nombre_usu'] . " " . $usu['ApellidoP_usu'] . " " . $usu['ApellidoM_usu'] ?>
               <img src="images/linea_uno.png" alt="linea_uno" width="185" height="5" class="lineas"/></div>
            </td>
            <td>
              <div class="opcion">
                <a href="perfil.php?id=<?php echo $usu['Id_usu'] ?>">
                  <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Mi perfil</a>
                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/>
              </div>
            </td>
            <td><div class="opcion">
                <a href="logout.php">
                  <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Salir</a>
                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/></div>
            </td>
          </tr>
        </table>
        <h2><img src="images/flecha_blue.png" alt="flecha_blue" width="6" height="11" />Opciones Alumnado</h2>
        <ul>
          <li><a href="graficas.php?id=<?php echo $alum->getId()?>" class="link">Graficas</a></li>
          <li><a href="analisis_sicologico.php" class="link">Regresar</a></li>
        </ul>
      </div>
    </td>
  </tr>
  <?php
  ?>
  <input type="hidden" id="Id_alum" value="<?php echo $alum->getId() ?>"/>
  <?php
  }
  ?>
</table>
<?php
write_footer();
?>
