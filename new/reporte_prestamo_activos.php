<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoActivos.php');
require_once('clases/DaoHistorialPrestamoActivo.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoCiclos.php');

$base = new base();
$DaoActivos = new DaoActivos();
$DaoHistorialPrestamoActivo = new DaoHistorialPrestamoActivo();
$DaoAlumnos = new DaoAlumnos();
$DaoDocentes = new DaoDocentes();
$DaoUsuarios = new DaoUsuarios();

$DaoCiclos = new DaoCiclos();
$ciclo = $DaoCiclos->getActual();

links_head("Historial de prestamo | ULM");
?>
<link rel="stylesheet" media="print" href="css/reporte_prestamo_activo_print.css?v=2011.6.16.18.36"> 
<?php
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-history"></i> Historial de prestamo de activos</h1>
                </div>
                <div class="box-filter-reportes">
                    <ul class="row list-design">
                        <li class="col-md-4 widget" onclick="mostrar_filtro()"><div class="stats-left"><i class="fa fa-filter"></i> Filtros</div></li>
                        <li class="col-md-4 widget states-mdl" onclick="download_excel()"><div class="stats-left"><i class="fa fa-download"></i> Descargar</div></li>
                        <li class="col-md-4 widget states-last" onclick="imprimir()"><div class="stats-left"><i class="fa fa-download"></i> Imprimir</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Código</td>
                                <td>Nombre</td>
                                <td>Usuario</td>
                                <td>Solicitante</td>
                                <td>Fecha de prestamo</td>
                                <td>Fecha de entrega</td>
                                <td>fecha de recibido</td>
                                <td>Comentarios</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($DaoHistorialPrestamoActivo->getPrestamosActivo() as $historial) {
                                $activo = $DaoActivos->show($historial->getId_activo());
                                $usuEntrega = $DaoUsuarios->show($historial->getUsu_entrega());

                                if ($historial->getTipo_usu() == "usu") {
                                    $usuRecibe = $DaoUsuarios->show($historial->getUsu_recibe());
                                    $Nombreusu = $usuRecibe->getNombre_usu() . " " . $usuRecibe->getApellidoP_usu() . " " . $usuRecibe->getApellidoM_usu();
                                } elseif ($historial->getTipo_usu() == "docen") {
                                    $usuRecibe = $DaoDocentes->show($historial->getUsu_recibe());
                                    $Nombreusu = $usuRecibe->getNombre_docen() . " " . $usuRecibe->getApellidoP_docen() . " " . $usuRecibe->getApellidoM_docen();
                                } elseif ($historial->getTipo_usu() == "alum") {
                                    $usuRecibe = $DaoAlumnos->show($historial->getUsu_recibe());
                                    $Nombreusu = $usuRecibe->getNombre() . " " . $usuRecibe->getApellidoP() . " " . $usuRecibe->getApellidoM();
                                }
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $activo->getCodigo() ?></td>
                                    <td style="width: 115px;"><?php echo $activo->getNombre() ?></td>
                                    <td><?php echo $usuEntrega->getNombre_usu() . " " . $usuEntrega->getApellidoP_usu() . " " . $usuEntrega->getApellidoM_usu(); ?></td>
                                    <td><?php echo $Nombreusu; ?></td>
                                    <td><?php echo $historial->getFecha_entrega(); ?></td>
                                    <td><?php echo $historial->getFecha_devolucion(); ?></td>
                                    <td><?php echo $historial->getFecha_entregado(); ?></td>
                                    <td><?php echo $historial->getComentarios(); ?></td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="boxUlBuscador col-md-3">
            <p>Código<br><input type="search"  class="buscarFiltro form-control" onkeyup="buscarActivo(this)" id="activo"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p class="col-md-3">Fecha inicio de prestamo<br><input class="form-control" type="date" id="fecha_ini"/>
        <p class="col-md-3">Fecha fin de prestamo<br><input class="form-control" type="date" id="fecha_fin"/>
        <div class="boxUlBuscador col-md-3">
            <p>Usuario<br><input type="search"  class="buscarFiltro form-control" onkeyup="buscarUsu(this)" id="usuario"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<?php
write_footer();



