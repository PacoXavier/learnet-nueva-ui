<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoCiclosAlumno.php');
require_once('clases/DaoRecargos.php');
require_once('clases/DaoPagosCiclo.php');
require_once('clases/DaoAnalisisSicologico.php');

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoCiclosAlumno= new DaoCiclosAlumno();
$DaoRecargos= new DaoRecargos();
$DaoPagosCiclo= new DaoPagosCiclo();
$DaoAnalisisSicologico= new DaoAnalisisSicologico();

links_head("Análisis Psicológico | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-bar-chart"></i> Análisis Psicológico</h1>
                    <span class="spanfiltros" style="margin-top: 15px;" onclick="mostrar_filtro()">Mostrar filtros</span>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            
                            <tr>
                                <td>#</td>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td>Orientaci&oacute;n</td>
                                <td style="text-align: center;">Fecha de evaluaci&Oacute;n</td>
                                <td style="text-align: center;">Estatus</td>
                                <!--<td  class="email_checkbox"><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>-->
                                <td>Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                             $count=1;
                             $query = "SELECT * FROM inscripciones_ulm 
                             JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
                             WHERE tipo=1  AND inscripciones_ulm.Id_plantel=".$usu['Id_plantel']."  AND Activo_oferta=1 AND Baja_ofe IS NULL ORDER BY Id_ins";
                             foreach($base->advanced_query($query) as $k=>$v){
                                       $alum = $DaoAlumnos->show($v['Id_ins']);
                                       $nombre_ori="";

                                       $oferta = $DaoOfertas->show($v['Id_ofe']);
                                       $esp = $DaoEspecialidades->show($v['Id_esp']);
                                       if ($v['Id_ori'] > 0) {
                                          $ori = $DaoOrientaciones->show($v['Id_ori']);
                                          $nombre_ori = $ori->getNombre();
                                        }
                                        $class="";
                                        $status="";
                                        $fecha="";
                                        $existe=$DaoAnalisisSicologico->showByAlum($alum->getId());
                                        if($existe->getId()>0){
                                           $fecha=$existe->getDateCreated(); 
                                           $status="Evaluado";
                                           $class="evaluado";
                                        }
                                      ?>
                                      <tr id_alum="<?php echo $alum->getId();?>" id_ofe_alum="<?php echo $v['Id_ofe_alum'];?>">
                                        <td><?php echo $count;?></td>
                                        <td><a href="alumno.php?id=<?php echo $alum->getId();?>"><?php echo $alum->getMatricula() ?></a></td>
                                        <td style="width: 115px;"><a href="alumno.php?id=<?php echo $alum->getId();?>"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></a></td>
                                        <td><?php echo $oferta->getNombre_oferta(); ?></td>
                                        <td><?php echo $esp->getNombre_esp(); ?></td>
                                        <td><?php echo $nombre_ori; ?></td>
                                        <td style="text-align: center;"><?php echo $fecha?></td>
                                        <td style="text-align: center;" class="<?php echo $class ?>"><?php echo $status;?></td>
                                        <!--<td><input type="checkbox"> </td>-->
                                        <td style="width:110px;">
                                            <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span> 
                                            <div class="box-buttom">
                                                <a href="evaluar_sicologia.php?id=<?php echo $alum->getId();?>"><button>Evaluar</button></a>
                                                <a href="graficas.php?id=<?php echo $alum->getId();?>" target="_blank"><button>Graficas</button></a>
                                            </div>
                                        </td>
                                      </tr>
                                      <?php
                                      $count++;
                                }
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="boxUlBuscador col-md-4">
            <p>Buscar<br><input type="search" class="form-control" class="buscarFiltro" onkeyup="buscarAlum()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <div class="form-group col-md-4">
            <p>Oferta<br>
                <select id="oferta" class="form-control" onchange="update_curso_box_curso()">
                  <option value="0"></option>
                  <?php
                  foreach($DaoOfertas->showAll() as $k=>$v){
                  ?>
                      <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
                  <?php
                  }
                  ?>
                </select>
            </p>
        </div>
        <div class="form-group col-md-4">
            <p>Especialidad:<br>
                <select id="curso" class="form-control" onchange="update_orientacion_box_curso()">
                  <option value="0"></option>
                </select>
            </p>
        </div>
        <div id="box_orientacion"></div>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<?php
write_footer();



