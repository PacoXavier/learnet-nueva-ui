<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

links_head("Sin grupo | ULM");
write_head_body();
write_body();

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoTurnos= new DaoTurnos();
$DaoCiclos= new DaoCiclos();
$ciclo=$DaoCiclos->getActual();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Alumnos sin grupo</h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul class="row" style="list-style: none; font-size: 20px;">
                        <li class="col-md-6 weather-grids widget-shadow" onclick="mostrar_filtro()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-filter" style="margin-right: 5px;"></i> Filtros</div></li>
                        <li class="col-md-6 weather-grids weather-right widget-shadow states-last" onclick="download_excel()"><div class="stats-right" style="width:100%; height: 100px; display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-download" style="margin-right: 5px;"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td>Materia</td>
                                <td>Turno</td>
                                <td>Opción de pago</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                             foreach ($DaoAlumnos->getAlumnos() as $k=>$v){
                                 
                                       $nombre_ori="";
                                       $oferta = $DaoOfertas->show($v['Id_ofe']);
                                       $esp = $DaoEspecialidades->show($v['Id_esp']);
                                       if ($v['Id_ori'] > 0) {
                                          $ori = $DaoOrientaciones->show($v['Id_ori']);
                                          $nombre_ori = $ori->getNombre();
                                        }
                                        $opcion="Plan por materias"; 
                                        if($v['Opcion_pago']==2){
                                          $opcion="Plan completo";  
                                        }

                                        $tur = $DaoTurnos->show($v['Turno']);
                                        $t=$tur->getNombre();
            
                                        $query = "SELECT materias_uml.Nombre,Materias_especialidades.NombreDiferente 
                                               FROM ciclos_alum_ulm 
                                        JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
                                        JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                                        JOIN materias_uml ON Materias_especialidades.Id_mat=materias_uml.Id_mat
                                        WHERE ciclos_alum_ulm.Id_ciclo=".$ciclo->getId()."  AND Id_ofe_alum=".$v['Id_ofe_alum']." AND materias_ciclo_ulm.Id_grupo IS NULL";
                                         foreach($base->advanced_query($query) as $k=>$v2){
                                                 $NombreMat=$v2['Nombre'];
                                                 if(strlen($v2['NombreDiferente'])>0){
                                                    $NombreMat=$v2['NombreDiferente']; 
                                                 }

                                      ?>
                                              <tr id_alum="<?php echo $v['Id_ins'];?>">
                                                <td><?php echo $v['Matricula'] ?></td>
                                                <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                                                <td><?php echo $oferta->getNombre_oferta(); ?></td>
                                                <td><?php echo $esp->getNombre_esp(); ?></td>
                                                <td><?php echo $nombre_ori; ?></td>
                                                <td><?php echo $NombreMat; ?></td>
                                                <td><?php echo $t; ?></td>
                                                <td><?php echo $opcion; ?></td>
                                              </tr>
                                              <?php
                                        }
                                    }
                                  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="boxUlBuscador col-md-4">
            <p>Alumno<br><input type="search"  class="buscarFiltro" onkeyup="buscarAlum()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p class="col-md-4">Oferta<br>
                <select id="oferta" onchange="update_curso_box_curso()">
                  <option value="0"></option>
                  <?php
                  foreach($DaoOfertas->showAll() as $k=>$v){
                  ?>
                      <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
                  <?php
                  }
                  ?>
                </select>
        </p>
        <p class="col-md-4">Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso()">
              <option value="0"></option>
            </select>
        </p>
    </div>
    <div class="row">
        <div class="col-md-3" id="box_orientacion"></div>
        <p class="col-md-3">Ciclo<br>
          <select id="ciclo">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p class="col-md-3">Opci&oacute;n de pago:<br>
            <select id="opcion">
              <option value="0"></option>
              <option value="1">POR MATERIAS</option>
              <option value="2">PLAN COMPLETO</option>
            </select>
        </p>
        <p class="col-md-3">Turno<br>
            <select id="turno">
                <option value="0"></option>
                <?php
                foreach($DaoTurnos->getTurnos() as $turno){
                    ?>
                    <option value="<?php echo $turno->getId()?>" ><?php echo $turno->getNombre()?></option>
                <?php
                }
                ?>
            </select>
        </p>
        <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
            <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
        </div>
</div>
<?php
write_footer();
