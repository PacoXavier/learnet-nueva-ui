<?php
require_once('estandares/includes.php');
if(!isset($perm['68'])){
  header('Location: home.php');
}
links_head("Capturar calificaciones  | ULM");
write_head_body();
write_body();
?>

<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-list-alt" aria-hidden="true"></i> Capturar calificaciones</h1>
                </div>
                <span class="spanfiltros" onclick="mostrar_filtro()">Mostrar filtros</span>
                <div id="mascara_tabla">

                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h1>Filtros</h1>
    <div class="boxUlBuscador">
        <p>Alumno<br><input type="search"  class="buscarFiltro" onkeyup="buscarAlum()" placeholder="Nombre"/></p>
        <ul class="Ulbuscador"></ul>
    </div>
    <p>Oferta<br>
          <select id="Id_ofe_alum">
          <option value="0"></option>
        </select>
    </p>
    <p><button onclick="filtro()">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();
