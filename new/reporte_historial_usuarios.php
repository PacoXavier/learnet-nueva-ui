<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoCiclosAlumno.php');
require_once('clases/DaoRecargos.php');
require_once('clases/DaoHistorialUsuario.php');
require_once('clases/DaoCiclos.php');

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoCiclosAlumno= new DaoCiclosAlumno();
$DaoHistorialUsuario= new DaoHistorialUsuario();
$DaoCiclos=new DaoCiclos();

links_head("Historial de usuarios | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-history"></i> Historial de usuarios </h1>
                </div>
                <div class="box-filter-reportes" style="margin-bottom: 30px">
                    <ul class="row" style="list-style: none; font-size: 20px;">
                        <li class="col-md-6 weather-grids widget-shadow" onclick="mostrar_filtro()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-filter" style="margin-right: 5px;"></i> Filtros</div></li>
                        <li class="col-md-6 weather-grids weather-right widget-shadow states-last" onclick="download_excel()"><div class="stats-right" style="width:100%; height: 100px; display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-download" style="margin-right: 5px;"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            
                            <tr>
                                <td>#</td>
                                <td>Ciclo</td>
                                <td>Usuario</td>
                                <td>Categoría</td>
                                <td style="width: 150px">Fecha</td>
                                <td>Movimiento realizado</td>
                            </tr>
                        </thead>
                        <tbody>
                       
                            <?php
                             $count=1;
                             foreach ($DaoHistorialUsuario->showAll() as $k=>$v){
                                $_usu=$DaoUsuarios->show($v->getId_usu());
                                $ciclo=$DaoCiclos->show($v->getId_ciclo());
                               ?>
                                 <tr>
                                         <td><?php echo $count; ?></td>
                                          <td><?php echo $ciclo->getClave()?></td>
                                         <td><a href="usuario.php?id=<?php echo $_usu->getId(); ?>"> <?php echo $_usu->getNombre_usu() . " " . $_usu->getApellidoP_usu() . " " . $_usu->getApellidoM_usu(); ?></a></td>
                                         <td><?php echo $v->getCategoria() ?></a></td>
                                         <td><?php echo $v->getDateCreated(); ?></td>
                                         <td><?php echo $v->getTexto(); ?></td>
                                 </tr>
                              <?php
                                 $count++;
                             }
                             ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="boxUlBuscador col-xs-6">
            <p>Usuario<br><input type="search"  class="buscarFiltro form-control" onkeyup="buscarUsu()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p class="col-xs-6">Categoria<br>
          <select class="form-control" id="categoria">
              <option></option>
              <?php
              $query="SELECT * FROM HistorialUsuario GROUP BY Categoria";
              foreach($DaoHistorialUsuario->advanced_query($query) as $k=>$v){
              ?>
                 <option value="<?php echo $v->getCategoria() ?>"><?php echo $v->getCategoria(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
    </div>
    <div class="row">
        <p class="col-md-4">Ciclo<br>
          <select id="ciclo" class="form-control">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p class="col-md-4">Fecha inicio<br><input class="form-control" type="date"  id="fecha_ini"/></p>
        <p class="col-md-4">Fecha fin<br><input class="form-control" type="date"  id="fecha_fin"/></p>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>

<?php
write_footer();



