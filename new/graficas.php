<?php
require_once('estandares/includes.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoCategoriasSicologia.php');
require_once('clases/DaoCamposSicologia.php');
require_once('clases/DaoAnalisisSicologico.php');

$DaoCategoriasSicologia= new DaoCategoriasSicologia();
$DaoCamposSicologia= new DaoCamposSicologia();
$DaoAnalisisSicologico= new DaoAnalisisSicologico();
$DaoAlumnos= new DaoAlumnos();
links_head("Graficas | ULM");
write_head_body();
write_body();
?>
<script src="js/Chart.js-master/Chart.js"></script>
<table id="tabla">
  <?php
  if ($_REQUEST['id'] > 0) {
      $alum=$DaoAlumnos->show($_REQUEST['id']);
  ?>
  <tr>
    <td id="column_one">
      <div class="fondo">
        <div id="box_top">
            <h1><i class="fa fa-bar-chart"></i> <?php echo ucwords(strtolower($alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM())) ?></h1>
        </div>
          <ul id="list-graficas">
        <?php
        $count=0;
        foreach($DaoCategoriasSicologia->showAll() as $k=>$v){
        ?>
              <li>
                <div class="seccion">
                      <h2><?php echo $v->getNombre()?></h2>
                      <p style="font-size:14px">Valor maximo: <b><?php echo $v->getRangoMax()?></b></p>
                      <span class="linea"></span>
                      <canvas id="myChart_<?php echo $count;?>" width="550" height="400"></canvas>
                </div>
              </li>
        <?php
        $count++;
        }
        ?>
          </ul>
      </div>
    </td>
  </tr>
  <?php
    ?>
  <input type="hidden" id="Id_alum" value="<?php echo $alum->getId() ?>"/>
  <?php
  }
  ?>
</table>
<?php
write_footer();
?>

