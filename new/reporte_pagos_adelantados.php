<?php
require_once('estandares/includes.php');
require_once('clases/DaoMetodosPago.php');
require_once('clases/DaoPagosAdelantados.php');
require_once('clases/DaoPaquetesDescuentos.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoPagos.php');
require_once('clases/DaoPagosCiclo.php');
require_once('clases/DaoCiclosAlumno.php');
require_once('clases/modelos/Alumnos.php');
require_once('clases/modelos/Ciclos.php');
require_once('clases/modelos/Pagos.php');
require_once('clases/modelos/PagosCiclo.php');
require_once('clases/modelos/Usuarios.php');
require_once('clases/modelos/PagosAdelantados.php');
require_once('clases/modelos/MetodosPago.php');
require_once('clases/modelos/PaquetesDescuentos.php');

$DaoAlumnos= new DaoAlumnos();
$DaoCiclos= new DaoCiclos();
$DaoPagos= new DaoPagos();
$DaoPagosCiclo= new DaoPagosCiclo();
$DaoCiclosAlumno= new DaoCiclosAlumno();
$DaoPagosAdelantados= new DaoPagosAdelantados();
$DaoUsuarios= new DaoUsuarios();
$DaoMetodosPago= new DaoMetodosPago();
$DaoPaquetesDescuentos= new DaoPaquetesDescuentos();

links_head("Pagos Adelantados | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Pagos Adelantados</h1>
                    <span class="spanfiltros" onclick="mostrar_filtro()">Mostrar filtros</span>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                    <td>Folio</td>
                                    <td>Matricula</td>
                                    <td>Alumno</td>
                                    <td>Fecha</td>
                                    <td>Monto recibido</td>
                                    <td>Saldo</td>
                                    <td>Folios</td>
                                    <td>M&eacute;todo de pago</td>
                                    <td>Usar en</td>
                                    <td>Paq. Descuento</td>
                                    <td>Usuario</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                           //foreach($DaoAlumnos->showAllAlumnos() as $k=>$v){
                                $exits_pago= array();
                                foreach($DaoPagosAdelantados->showAll() as $k2=>$v2){
                                    $alum=$DaoAlumnos->show($v2->getId_alum());
                                    $usu_acre = $DaoUsuarios->show($v2->getId_usu());
                                    $ciclo=$DaoCiclos->show($v2->getId_ciclo_usar());
                                    if($v2->getId_met_pago()>0){
                                       $metodoPago=$DaoMetodosPago->show($v2->getId_met_pago());
                                       $NombreMetodo=$metodoPago->getNombre();
                                    }else{
                                       $NombreMetodo="Cambio de plan";   
                                    }

                                    $Paq_des="";
                                    if($v2->getId_paq_des()>0){
                                        $Des=$DaoPaquetesDescuentos->show($v2->getId_paq_des());
                                        $Paq_des =$Des->getNombre()." ".$Des->getPorcentaje()." %";
                                    }

                                    
                                    $Ids_pp= array();
                                    $Ids_pp=explode(",", $v2->getIds_pagos());
                                    $Monto=0;
                                    $cadena="";
                                    $arrayPP=array();
                                    foreach($Ids_pp as $v3){
                                        if(!in_array($v3, $exits_pago)){
                                            if($v3>0){
                                            $resp=$DaoPagos->show($v3);
                                            $Monto+=$resp->getMonto_pp();
                                            $cadena.=$v3."=".$resp->getMonto_pp()."<br>";
                                            array_push($exits_pago, $v3);
                                            //array_push($arrayPP, $v3);
                                            }
                                        }
                                    } 
                                    $cadena.="<b>".$Monto."</b>";
                                    //$actualizar=implode(",", $arrayPP);
                                    //$pagoAdelantado=$DaoPagosAdelantados->show($v2->getId());
                                    //$pagoAdelantado->setIds_pagos($actualizar);
                                    //$DaoPagosAdelantados->update($pagoAdelantado);
                                    
                                   ?>
                                    <tr>
                                       <td><?php echo $v2->getId()?></td>
                                        <td><?php echo $alum->getMatricula()?></td>
                                        <td><?php echo $alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM()?></td>
                                        <td><?php echo $v2->getFecha_pago()?></td>
                                        <td>$<?php echo number_format($v2->getMonto_pago(),2)?></td>
                                        <td>$<?php echo number_format($v2->getSaldo(),2)?></td>
                                        <td <?php if($Monto==$v2->getMonto_pago()){?> style="background: greenyellow;"<?php } ?>>
                                            <?php echo $cadena;?>
                                        </td>
                                        <td><?php echo $NombreMetodo ?></td>
                                        <td><?php echo $ciclo->getClave()?></td>
                                        <td><?php echo $Paq_des?></td>
                                        <td><?php echo $usu_acre->getNombre_usu() . " " . $usu_acre->getApellidoP_usu() . " " . $usu_acre->getApellidoM_usu()?></td>
                                    </tr>
                                    <?php
                                }
                            //}
                                  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
        <div class="boxUlBuscador">
            <p>Buscar<br><input type="search"  class="buscarFiltro" onkeyup="buscarAlum()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p>Fecha inicio<br><input type="date" id="fecha_ini"/>
        <p>Fecha fin<br><input type="date" id="fecha_fin"/>
	<p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              /*
              $class_ofertas= new class_ofertas();
              foreach($class_ofertas->get_ofertas() as $k=>$v){
              ?>
                  <option value="<?php echo $v['Id_oferta'] ?>"> <?php echo $v['Nombre_oferta'] ?> </option>
              <?php
              }
               * 
               */
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso()">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();
