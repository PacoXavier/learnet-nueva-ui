<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoMediosEnterar.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoMateriasEspecialidad.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoMotivosBaja.php');

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoGrados= new DaoGrados();
$DaoMediosEnterar= new DaoMediosEnterar();
$DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
$DaoMaterias= new DaoMaterias();
$DaoMotivosBaja= new DaoMotivosBaja();


links_head("Bajas | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-times"></i> Bajas</h1>
                </div>
                <div class="box-filter-reportes">
                    <ul class="row list-design">
                        <li class="col-md-4 widget"  onclick="mostrar_box_masivo()"><div class="stats-left"><i class="fa fa-magic" aria-hidden="true"></i> Reactivar</div></li>
                        <li class="col-md-4 widget states-mdl" onclick="mostrar_filtro()"><div class="stats-left"><i class="fa fa-filter"></i> Filtrar</div></li>
                        <li class="col-md-4 widget states-last" onclick="download_excel()"><div class="stats-left"><i class="fa fa-download"></i> Descargar</div></li>
                    </ul>
                </div>
                
                <div id="mascara_tabla" class="panel-body widget-shadow tables">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td>Grado:</td>
                                <td>Fecha de baja</td>
                                <td>Motivo</td>
                                <td style="width:75px;">Comentarios</td>
                                <td class="td-center"><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                                <td class="td-center">Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                            //Se mostran alumnos duplicados si es que tienen mas dos ofertas
                                $count=0;
                                foreach ($DaoAlumnos->getAlumnosBajas() as $k=>$v){
                                       $nombre_ori="";
                                       $oferta = $DaoOfertas->show($v['Id_ofe']);
                                       $esp = $DaoEspecialidades->show($v['Id_esp']);
                                       if ($v['Id_ori'] > 0) {
                                          $ori = $DaoOrientaciones->show($v['Id_ori']);
                                          $nombre_ori = $ori->getNombre();
                                        }
                                        $opcion="Plan por materias"; 
                                        if($v['Opcion_pago']==2){
                                          $opcion="Plan completo";  
                                        }
                                        $grado = $DaoOfertasAlumno->getLastGradoOferta($v['Id_ofe_alum']);
                                      ?>
                                        <tr id_alum="<?php echo $v['Id_ins'];?>" num-tr="<?php echo $count;?>" id-ofe-alum="<?php echo $v['Id_ofe_alum']?>">
                                            <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $v['Matricula'] ?></td>
                                            <td style="width: 115px;" onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                                            <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $oferta->getNombre_oferta(); ?></td>
                                            <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $esp->getNombre_esp(); ?></td>
                                            <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $nombre_ori; ?></td>
                                            <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $grado['Grado'] ?></td>
                                            <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $base->formatFecha($v['Baja_ofe']) ?></td>
                                            <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $v['Nombre']; ?></td>
                                            <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $v['Comentario_baja']; ?></td>
                                            <td class="td-center"><input type="checkbox"> </td>
                                            <td class="td-center">
                                                <a href="solicitud_baja.php?id_ofe_alum=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button><i class="fa fa-file-o"></i> Solicitud</button></a>
                                                <?php
                                                if($oferta->getTipoOferta()==1){
                                                    ?>
                                                     <button onclick="mostrarCicloReactivar(<?php echo $v['Id_ofe_alum'] ?>)">Reactivar</button>
                                                     <a href="pago.php?id=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button>Cobranza</button></a>
                                                    <?php
                                                }elseif($oferta->getTipoOferta()==2){
                                                    ?>
                                                     <button onclick="mostrarReactivarSinCiclo(<?php echo $v['Id_ofe_alum'] ?>)">Reactivar</button>
                                                     <a href="cargos_curso.php?id=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button>Cobranza</button></a>
                                                    <?php   
                                                }
                                                ?>
                                            </td>
                                          </tr>
                                          <?php
                                          $count++;
                                  }
                                  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <div class="boxUlBuscador col-md-4">
            <p>Buscar<br><input type="search"  class="buscarFiltro form-control" onkeyup="buscarAlum()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p class="col-md-4">Fecha inicio<br><input class="form-control" type="date" id="fecha_ini"/></p>
        <p class="col-md-4">Fecha fin<br><input class="form-control" type="date" id="fecha_fin"/></p>
    </div>
    <div class="row">
        <p class="col-md-3">Motivo de la baja<br>
                <select id="motivos" class="form-control">
                    <option value="0"></option>
                    <?php
                        foreach ($DaoMotivosBaja->showAll() as $k => $v) {
                            ?>
                            <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre() ?></option>
                            <?php
                        }
                    ?>
           </select>
        </p>
        <p class="col-md-3">Oferta<br>
                <select id="oferta" class="form-control" onchange="update_curso_box_curso()">
                  <option value="0"></option>
                  <?php
                  foreach($DaoOfertas->showAll() as $k=>$v){
                  ?>
                      <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre_oferta() ?> </option>
                  <?php
                  }
                  ?>
                </select>
        </p>
        <p class="col-md-3">Especialidad:<br>
            <select id="curso" class="form-control" onchange="update_orientacion_box_curso()">
              <option value="0"></option>
            </select>
        </p>
        <div class="col-md-3" id="box_orientacion"></div>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<?php
write_footer();

