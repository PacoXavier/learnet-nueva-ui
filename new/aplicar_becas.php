<?php
require_once('estandares/includes.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoGrupos.php');
require_once('clases/DaoPermutasClase.php');
require_once('clases/DaoHoras.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/modelos/PermutasClase.php');
require_once('clases/modelos/base.php');

$DaoAlumnos= new DaoAlumnos();
$DaoCiclos= new DaoCiclos();
$DaoUsuarios= new DaoUsuarios();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoEspecialidades= new DaoEspecialidades();


links_head("Aplicar becas | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-asterisk"></i> Aplicar beca</h1>
                </div>
                <div class="seccion">
                    <ul class="form list-design row">
                        <?php
                        $nombre="";
                        if ($_REQUEST['id'] > 0) {
                            $alumno = $DaoAlumnos->show($_REQUEST['id']);
                            $nombre=$alumno->getNombre()." ".$alumno->getApellidoP()." ".$alumno->getApellidoM();
                        }
                        ?>
                        <li class="col-xs-6 weather-grids widget-shadow"><div class="stats-left" style="color: #fff">Buscar Alumno<br><input class="form-control" type="text" id="email_int" style="color: #000; margin-left: 5px;" onkeyup="buscar_int()" value="<?php echo $nombre ?>"/></div>
                            <ul id="buscador_int"></ul>
                        </li>
                        <?php
                        if ($_REQUEST['id'] > 0) {
                        ?>
                        <li class="col-xs-6 weather-grids weather-right widget-shadow states-last">
                            <div class="stats-left" style="color: #fff">Especialidad<br>
                            <select class="form-control" id="OfertaAlumno" style="color: #000 margin-left: 5px;" onchange="getCiclosBeca()">
                                <option value="0">Selecciona una especialidad</option>
                              <?php
                              foreach ($DaoOfertasAlumno->getOfertasAlumno($_REQUEST['id']) as $k => $v) {
                                  $esp = $DaoEspecialidades->show($v->getId_esp());
                              ?>
                                <option value="<?php echo $v->getId() ?>"><?php echo $esp->getNombre_esp() ?></option>
                              <?php
                              }
                              ?>

                            </select></div></li>
                            <?php
                         }
                        ?>
                    </ul>
                    <div id="box-info"></div>
                </div>
        </td>
<!--        <td id="column_two">-->
<!--            <div id="box_menus">-->
<!--                --><?php
//                require_once 'estandares/menu_derecho.php';
//                ?>
<!--                <ul>-->
<!--                </ul>-->
<!--            </div>-->
<!--        </td>-->
    </tr>
</table>
<input type="hidden" id="Id_alum" value="<?php echo $_REQUEST['id']; ?>"/>
<div class="box-dias-ciclo"></div>
<?php
write_footer();


