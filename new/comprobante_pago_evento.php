<?php

if ($_REQUEST['id'] > 0) {
    require_once('estandares/Fpdf/fpdf.php');
    require_once('estandares/Fpdf/fpdi.php');
    require_once("estandares/QRcode/qrlib.php");
    require_once('clases/DaoEventos.php');
    require_once('clases/DaoCiclos.php');
    require_once('clases/DaoAulas.php');
    require_once('clases/DaoPagos.php');
    require_once('clases/DaoMetodosPago.php');
    require_once('clases/modelos/Aulas.php');
    require_once('clases/modelos/Ciclos.php');
    require_once('clases/modelos/Eventos.php');
    require_once('clases/modelos/Pagos.php');
    require_once('clases/modelos/MetodosPago.php');
    require_once('estandares/cantidad_letra.php');
    require_once('clases/DaoPlanteles.php');
    require_once('clases/DaoUsuarios.php');

    $DaoPlanteles = new DaoPlanteles();
    $DaoUsuarios = new DaoUsuarios();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $plantel = $DaoPlanteles->show($usu->getId_plantel());
    if (strlen($plantel->getFormato_pago_evento()) > 0) {
        $DaoPagos = new DaoPagos();
        $pago = $DaoPagos->show($_REQUEST['id']);

        if ($pago->getId_evento() > 0) {

            $DaoEventos = new DaoEventos();
            $evento = $DaoEventos->show($pago->getId_evento());

            $DaoMetodosPago = new DaoMetodosPago();
            $Met = $DaoMetodosPago->show($pago->getMetodo_pago());

            $DaoAulas = new DaoAulas();
            $Aula = $DaoAulas->show($evento->getId_salon());

// initiate FPDI 
            $pdf = new FPDI();

// add a page 
            $pdf->AddPage();

// set the sourcefile 
            //$pdf->setSourceFile('estandares/reciboevento.pdf');
            
            $pdf->setSourceFile('files/'.$plantel->getFormato_pago_evento().".pdf");

// import page 1 
            $tplIdx = $pdf->importPage(1);

// use the imported page and place it at point 10,10 with a width of 100 mm 
            $pdf->useTemplate($tplIdx);

// now write some text above the imported page 
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->SetTextColor(0, 0, 0);

            $pdf->SetXY(179, 4);
            $pdf->Write(10, utf8_decode(date('Y-m-d')));

            $pdf->SetXY(77, 61);
            $pdf->Write(10, mb_strtoupper($pago->getNombrePaga()));


            $pdf->SetXY(179, 12);
            $pdf->Write(10, $pago->getFolio());

            $pdf->SetXY(77, 93);
            $pdf->MultiCell(90, 3, mb_strtoupper(utf8_decode($pago->getConcepto())), 0, 'L');
            $pdf->SetXY(77, 100);
            $pdf->Write(10, mb_strtoupper(utf8_decode($pago->getFecha_pp() . " Comentarios: " . $pago->getComentarios())));

            $pdf->SetXY(77, 113);
            $pdf->Write(10, mb_strtoupper(utf8_decode($Met->getNombre())));
            $pdf->SetXY(77, 74);
            $pdf->Write(10, "$" . number_format($pago->getMonto_pp(), 2));
            $pdf->SetXY(77, 79);
            $pdf->Write(10, strtoupper(num2letras($pago->getMonto_pp(), 2)));

// add QRcode
//texto a encliptar                                       //Ruta donde se guarda la imagen
            QRcode::png('?folio=' . $pago->getId() . '&cantidad=' . number_format($pago->getMonto_pp(), 2), 'files/test.png');
            $pdf->Image("files/test.png", 23, 27, 33, 33);

            $pdf->Output('recibo' . $pago->getId() . '.pdf', 'I');
        } else {
            header("Location: home.php");
        }
    }
} else {
    header("Location: home.php");
}