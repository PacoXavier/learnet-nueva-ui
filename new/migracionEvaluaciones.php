<?php
require_once('estandares/includes.php');
require_once('require_daos.php');
$base= new base();
$DaoDocentes = new DaoDocentes();
$DaoCiclos = new DaoCiclos();
$DaoEvaluacionDocente = new DaoEvaluacionDocente();
$DaoCategoriasEvaluacion = new DaoCategoriasEvaluacion();
$DaoCamposCategoria = new DaoCamposCategoria();
$DaoCategoriasPago = new DaoCategoriasPago();
$DaoResultadosEvaluacion= new DaoResultadosEvaluacion();
$DaoPuntosPorCategoriaEvaluadaDocente= new DaoPuntosPorCategoriaEvaluadaDocente();

$query="SELECT * FROM Evaluacion_docente ORDER BY Id_eva ASC";
foreach($base->advanced_query($query) as $evaluacion){
        //Expediente
        $puntosA=0;
        $id_cam=1;
        $puntos="0";
        $puntos=$evaluacion['CV']>0?1.6:0;
        $puntosA+=$puntos;
        $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
        if($resultado->getId()>0){
           $resultado->setId_camp($id_cam);
           $resultado->setId_eva($evaluacion['Id_eva']);
           $resultado->setPuntos($puntos);
           $DaoResultadosEvaluacion->update($resultado);
        }else{
          $resultado= new ResultadosEvaluacion();
          $resultado->setId_camp($id_cam);
          $resultado->setId_eva($evaluacion['Id_eva']);
          $resultado->setPuntos($puntos);
          $DaoResultadosEvaluacion->add($resultado);
        }
        
        $id_cam=2;
        $puntos="0";
        $puntos=$evaluacion['IFE']>0?1.6:0;
        $puntosA+=$puntos;
        $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
        if($resultado->getId()>0){
           $resultado->setId_camp($id_cam);
           $resultado->setId_eva($evaluacion['Id_eva']);
           $resultado->setPuntos($puntos);
           $DaoResultadosEvaluacion->update($resultado);
        }else{
          $resultado= new ResultadosEvaluacion();
          $resultado->setId_camp($id_cam);
          $resultado->setId_eva($evaluacion['Id_eva']);
          $resultado->setPuntos($puntos);
          $DaoResultadosEvaluacion->add($resultado);
        }
        
        $id_cam=4;
        $puntos="0";
        $puntos=$evaluacion['RFC']>0?1.6:0;
        $puntosA+=$puntos;
        $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
        if($resultado->getId()>0){
           $resultado->setId_camp($id_cam);
           $resultado->setId_eva($evaluacion['Id_eva']);
           $resultado->setPuntos($puntos);
           $DaoResultadosEvaluacion->update($resultado);
        }else{
          $resultado= new ResultadosEvaluacion();
          $resultado->setId_camp($id_cam);
          $resultado->setId_eva($evaluacion['Id_eva']);
          $resultado->setPuntos($puntos);
          $DaoResultadosEvaluacion->add($resultado);
        }
        
        
        $id_cam=5;
        $puntos="0";
        $puntos=$evaluacion['CURP']>0?1.6:0;
        $puntosA+=$puntos;
        $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
        if($resultado->getId()>0){
           $resultado->setId_camp($id_cam);
           $resultado->setId_eva($evaluacion['Id_eva']);
           $resultado->setPuntos($puntos);
           $DaoResultadosEvaluacion->update($resultado);
        }else{
          $resultado= new ResultadosEvaluacion();
          $resultado->setId_camp($id_cam);
          $resultado->setId_eva($evaluacion['Id_eva']);
          $resultado->setPuntos($puntos);
          $DaoResultadosEvaluacion->add($resultado);
        }
        
        $id_cam=6;
        $puntos="0";
        $puntos=$evaluacion['Fotos']>0?1.6:0;
        $puntosA+=$puntos;
        $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
        if($resultado->getId()>0){
           $resultado->setId_camp($id_cam);
           $resultado->setId_eva($evaluacion['Id_eva']);
           $resultado->setPuntos($puntos);
           $DaoResultadosEvaluacion->update($resultado);
        }else{
          $resultado= new ResultadosEvaluacion();
          $resultado->setId_camp($id_cam);
          $resultado->setId_eva($evaluacion['Id_eva']);
          $resultado->setPuntos($puntos);
          $DaoResultadosEvaluacion->add($resultado);
        }
        
        $id_cam=7;
        $puntos="0";
        $puntos=$evaluacion['Comp_domicilio']>0?1.6:0;
        $puntosA+=$puntos;
        $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
        if($resultado->getId()>0){
           $resultado->setId_camp($id_cam);
           $resultado->setId_eva($evaluacion['Id_eva']);
           $resultado->setPuntos($puntos);
           $DaoResultadosEvaluacion->update($resultado);
        }else{
          $resultado= new ResultadosEvaluacion();
          $resultado->setId_camp($id_cam);
          $resultado->setId_eva($evaluacion['Id_eva']);
          $resultado->setPuntos($puntos);
          $DaoResultadosEvaluacion->add($resultado);
        }
        
        $id_cam=8;
        $puntos="0";
        $puntos=$evaluacion['Comp_estudios']>0?10:0;
        $puntosA+=$puntos;
        $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
        if($resultado->getId()>0){
           $resultado->setId_camp($id_cam);
           $resultado->setId_eva($evaluacion['Id_eva']);
           $resultado->setPuntos($puntos);
           $DaoResultadosEvaluacion->update($resultado);
        }else{
          $resultado= new ResultadosEvaluacion();
          $resultado->setId_camp($id_cam);
          $resultado->setId_eva($evaluacion['Id_eva']);
          $resultado->setPuntos($puntos);
          $DaoResultadosEvaluacion->add($resultado);
        }
        
        $id_cam=9;
        $puntos="0";
        $puntos=$evaluacion['Tecnico_val']>0?10:0;
        $puntosA+=$puntos;
        $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
        if($resultado->getId()>0){
           $resultado->setId_camp($id_cam);
           $resultado->setId_eva($evaluacion['Id_eva']);
           $resultado->setPuntos($puntos);
           $DaoResultadosEvaluacion->update($resultado);
        }else{
          $resultado= new ResultadosEvaluacion();
          $resultado->setId_camp($id_cam);
          $resultado->setId_eva($evaluacion['Id_eva']);
          $resultado->setPuntos($puntos);
          $DaoResultadosEvaluacion->add($resultado);
        }

         
    //Desempenio
    $puntosB=0;
    $id_cam=3;
    $puntos=$evaluacion['Planeaciones']>0?$evaluacion['Planeaciones']:0;
    $puntosB+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=10;
    $puntos=$evaluacion['Asistencias']>0?$evaluacion['Asistencias']:0;
    $puntosB+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=11;
    $puntos=$evaluacion['Puntualidad']>0?$evaluacion['Puntualidad']:0;
    $puntosB+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=12;
    $puntos=$evaluacion['Encuestas']>0?$evaluacion['Encuestas']:0;
    $puntosB+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=13;
    $puntos=$evaluacion['Eva_int']>0?$evaluacion['Eva_int']:0;
    $puntosB+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=14;
    $puntos=$evaluacion['Juntas']>0?$evaluacion['Juntas']:0;
    $puntosB+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }

    //Capacitacion
    $puntosC=0;
    $id_cam=15;
    $puntos=$evaluacion['Titulo_cedula']>0?20:0;
    $puntosC+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=16;
    $puntos=$evaluacion['Equivalencia']>0?10:0;
    $puntosC+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=17;
    $puntos=$evaluacion['Maestria']>0?30:0;
    $puntosC+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=18;
    $puntos=$evaluacion['Cap_int']>0?$evaluacion['Cap_int']:0;
    $puntosC+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=19;
    $puntos=$evaluacion['Cap_ext']>0?$evaluacion['Cap_ext']:0;
    $puntosC+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=20;
    $puntos=$evaluacion['Cursos_afin']>0?$evaluacion['Cursos_afin']:0;
    $puntosC+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }

    //Cualitativos
    $puntosD=0;
    $id_cam=21;
    $puntos=$evaluacion['Libros']>0?$evaluacion['Libros']:0;
    $puntosD+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=22;
    $puntos=$evaluacion['Discografia']>0?$evaluacion['Discografia']:0;
    $puntosD+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=23;
    $puntos=$evaluacion['Ponente']>0?$evaluacion['Ponente']:0;
    $puntosD+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=24;
    $puntos=$evaluacion['Capacitador_externo']>0?$evaluacion['Capacitador_externo']:0;
    $puntosD+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=25;
    $puntos=$evaluacion['Inv_academico']>0?$evaluacion['Inv_academico']:0;
    $puntosD+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    $id_cam=26;
    $puntos=$evaluacion['presentaciones_profecionales']>0?$evaluacion['presentaciones_profecionales']:0;
    $puntosD+=$puntos;
    $resultado=$DaoResultadosEvaluacion->getCampoByEvaluacion($evaluacion['Id_eva'],$id_cam);
    if($resultado->getId()>0){
       $resultado->setId_camp($id_cam);
       $resultado->setId_eva($evaluacion['Id_eva']);
       $resultado->setPuntos($puntos);
       $DaoResultadosEvaluacion->update($resultado);
    }else{
      $resultado= new ResultadosEvaluacion();
      $resultado->setId_camp($id_cam);
      $resultado->setId_eva($evaluacion['Id_eva']);
      $resultado->setPuntos($puntos);
      $DaoResultadosEvaluacion->add($resultado);
    }
    
    
    //Total de cada categoria A
    $nivelTotales=$DaoPuntosPorCategoriaEvaluadaDocente->getPuntosPorCategoriaEvaluacion($evaluacion['Id_eva'],$evaluacion['Profesor_a']);
    if($nivelTotales->getId()>0){
        $nivelTotales->setPuntos_por_nivel($puntosA);  
        $DaoPuntosPorCategoriaEvaluadaDocente->update($nivelTotales);   
    }else{
        $nivelTotales= new PuntosPorCategoriaEvaluadaDocente();
        $nivelTotales->setId_eva($evaluacion['Id_eva']);
        $nivelTotales->setId_nivel($evaluacion['Profesor_a']);
        $nivelTotales->setPuntos_por_nivel($puntosA);  
        $DaoPuntosPorCategoriaEvaluadaDocente->add($nivelTotales);
    }

    //Total de cada categoria B
    $nivelTotales=$DaoPuntosPorCategoriaEvaluadaDocente->getPuntosPorCategoriaEvaluacion($evaluacion['Id_eva'],$evaluacion['Profesor_b']);
    if($nivelTotales->getId()>0){
        $nivelTotales->setPuntos_por_nivel($puntosB);  
        $DaoPuntosPorCategoriaEvaluadaDocente->update($nivelTotales);   
    }else{
        $nivelTotales= new PuntosPorCategoriaEvaluadaDocente();
        $nivelTotales->setId_eva($evaluacion['Id_eva']);
        $nivelTotales->setId_nivel($evaluacion['Profesor_b']);
        $nivelTotales->setPuntos_por_nivel($puntosB);  
        $DaoPuntosPorCategoriaEvaluadaDocente->add($nivelTotales);
    }

    //Total de cada categoria C
    $nivelTotales=$DaoPuntosPorCategoriaEvaluadaDocente->getPuntosPorCategoriaEvaluacion($evaluacion['Id_eva'],$evaluacion['Profesor_c']);
    if($nivelTotales->getId()>0){
        $nivelTotales->setPuntos_por_nivel($puntosC);  
        $DaoPuntosPorCategoriaEvaluadaDocente->update($nivelTotales);   
    }else{
        $nivelTotales= new PuntosPorCategoriaEvaluadaDocente();
        $nivelTotales->setId_eva($evaluacion['Id_eva']);
        $nivelTotales->setId_nivel($evaluacion['Profesor_c']);
        $nivelTotales->setPuntos_por_nivel($puntosC);  
        $DaoPuntosPorCategoriaEvaluadaDocente->add($nivelTotales);
    }
    
    //Total de cada categoria D
    $nivelTotales=$DaoPuntosPorCategoriaEvaluadaDocente->getPuntosPorCategoriaEvaluacion($evaluacion['Id_eva'],$evaluacion['Profesor_d']);
    if($nivelTotales->getId()>0){
        $nivelTotales->setPuntos_por_nivel($puntosD);  
        $DaoPuntosPorCategoriaEvaluadaDocente->update($nivelTotales);   
    }else{
        $nivelTotales= new PuntosPorCategoriaEvaluadaDocente();
        $nivelTotales->setId_eva($evaluacion['Id_eva']);
        $nivelTotales->setId_nivel($evaluacion['Profesor_d']);
        $nivelTotales->setPuntos_por_nivel($puntosD);  
        $DaoPuntosPorCategoriaEvaluadaDocente->add($nivelTotales);
    }
}



?>
<p>Listo!</p>