<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="filtro"){
    $base= new base();
    $DaoHoras= new DaoHoras();
    $DaoGrupos= new DaoGrupos();
    $DaoAulas= new DaoAulas();
    $DaoDocentes= new DaoDocentes();
    $DaoHorarioExamenGrupo= new DaoHorarioExamenGrupo();
    $DaoMaterias= new DaoMaterias();
    $DaoCiclos= new DaoCiclos();
    $DaoPeriodosExamenes= new DaoPeriodosExamenes();
    $DaoUsuarios= new DaoUsuarios();
    $DaoGrupoExamen= new DaoGrupoExamen();
    $DaoDocenteExamen= new DaoDocenteExamen();

    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $ciclo=$DaoCiclos->getActual($usu->getId_plantel());

    $query="";
    if($_POST['Id_docen']>0){
        $query="AND DocenteExamen.Id_docen=".$_POST['Id_docen'];
    }
    if($_POST['Id_grupo']>0){
        $query=$query." AND GrupoExamen.IdGrupo=".$_POST['Id_grupo'];
    }
    if($_POST['Id_materia']>0){
        $query=$query." AND Grupos.Id_mat=".$_POST['Id_materia'];
    }
    
    
    if($_POST['Id_aula']>0){
        $query=$query." AND HorarioExamenGrupo.Id_aula=".$_POST['Id_aula'];
    }
    if($_POST['Id_ciclo']>0){
        $Id_ciclo=$_POST['Id_ciclo'];
    }else{
        $Id_ciclo=$ciclo->getId();
    }
    
    if(strlen($_POST['Fecha_ini'])>0 && $_POST['Fecha_fin']==null){
        $query=$query." AND FechaAplicacion>='".$_POST['Fecha_ini']."'";
    }elseif(strlen($_POST['Fecha_fin'])>0 && $_POST['Fecha_ini']==null){
        $query=$query." AND FechaAplicacion<='".$_POST['Fecha_fin']."'";
    }elseif(strlen($_POST['Fecha_ini'])>0 && strlen($_POST['Fecha_fin'])>0){
        $query=$query." AND (FechaAplicacion>='".$_POST['Fecha_ini']."' AND FechaAplicacion<='".$_POST['Fecha_fin']."')";
    }
    $count=1;
    $query= "SELECT HorarioExamenGrupo.* FROM HorarioExamenGrupo
             JOIN GrupoExamen ON HorarioExamenGrupo.Id_horario=GrupoExamen.IdHorarioExamen
             JOIN DocenteExamen ON HorarioExamenGrupo.Id_horario=DocenteExamen.IdHorarioExamen
             JOIN Grupos ON GrupoExamen.IdGrupo=Grupos.Id_grupo
             JOIN materias_uml ON Grupos.Id_mat=materias_uml.Id_mat
             WHERE HorarioExamenGrupo.Id_ciclo=".$Id_ciclo." ".$query;
    foreach($DaoHorarioExamenGrupo->advanced_query($query) as $examen){     
         $per=$DaoPeriodosExamenes->show($examen->getId_periodoExamen());
         $grupos=array();
         $docentes=array();
         foreach($DaoGrupoExamen->getGruposExamenHorario($examen->getId()) as $ObjGrupo){
                   $grupo=$DaoGrupos->show($ObjGrupo->getIdGrupo());
                   $clave=$DaoGrupos->covertirCadena($grupo->getClave(), 1);
                   array_push($grupos, $clave);
                   $materia=$DaoMaterias->show($grupo->getId_mat());
         } 

         foreach($DaoDocenteExamen->getDocentesExamenHorario($examen->getId()) as $ObjDocente){
               $docente=$DaoDocentes->show($ObjDocente->getId_docen());
               $docenteNombre=$DaoDocentes->covertirCadena($docente->getNombre_docen()." ".$docente->getApellidoP_docen(),1);
               array_push($docentes, $docenteNombre);
         }
         $materia=$DaoMaterias->show($grupo->getId_mat());
         $start=  substr($examen->getStart(),strpos($examen->getStart(), "T")+1);
         $end=  substr($examen->getEnd(),strpos($examen->getEnd(), "T")+1);
         $aula=$DaoAulas->show($examen->getId_aula());
         $c=$DaoCiclos->show($examen->getId_ciclo());
     ?>
             <tr>
               <td><?php echo $count;?></td>
               <td><?php echo implode(", ", $docentes)?></td>
               <td><?php echo implode(", ", $grupos)?></td>
               <td><?php echo $DaoDocentes->covertirCadena($materia->getNombre(),2)?></td>
               <td><?php echo $DaoDocentes->covertirCadena($aula->getNombre_aula(),2)?></td>
               <td class="td-center"><?php echo $examen->getFechaAplicacion() ?></td>
               <td class="td-center"><?php echo $start; ?></td>
               <td class="td-center"><?php echo $end; ?></td>
               <td class="td-center"><?php echo $c->getClave(); ?></td>
               <td><?php echo $per->getNombre(); ?></td>
             </tr>
             <?php
             $count++;
    }
}

if($_GET['action']=="download_excel"){
    
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Docente');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Grupo');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Materia');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Aula');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Fecha de exámen');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Hora inicio');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Hora Fin');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Ciclo');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Período de exámen');
                                            
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','J') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    $DaoHoras= new DaoHoras();
    $DaoGrupos= new DaoGrupos();
    $DaoAulas= new DaoAulas();
    $DaoDocentes= new DaoDocentes();
    $DaoHorarioExamenGrupo= new DaoHorarioExamenGrupo();
    $DaoMaterias= new DaoMaterias();
    $DaoCiclos= new DaoCiclos();
    $DaoPeriodosExamenes= new DaoPeriodosExamenes();
    $DaoUsuarios= new DaoUsuarios();
    $DaoGrupoExamen= new DaoGrupoExamen();
    $DaoDocenteExamen= new DaoDocenteExamen();

    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $ciclo=$DaoCiclos->getActual($usu->getId_plantel());

    $query="";
    if($_GET['Id_docen']>0){
        $query="AND DocenteExamen.Id_docen=".$_GET['Id_docen'];
    }
    if($_GET['Id_grupo']>0){
        $query=$query." AND GrupoExamen.IdGrupo=".$_GET['Id_grupo'];
    }
    if($_GET['Id_materia']>0){
        $query=$query." AND Grupos.Id_mat=".$_GET['Id_materia'];
    }
    
    
    if($_GET['Id_aula']>0){
        $query=$query." AND HorarioExamenGrupo.Id_aula=".$_GET['Id_aula'];
    }
    if($_GET['Id_ciclo']>0){
        $Id_ciclo=$_GET['Id_ciclo'];
    }else{
        $Id_ciclo=$ciclo->getId();
    }
    
    if(strlen($_GET['Fecha_ini'])>0 && $_GET['Fecha_fin']==null){
        $query=$query." AND FechaAplicacion>='".$_GET['Fecha_ini']."'";
    }elseif(strlen($_GET['Fecha_fin'])>0 && $_GET['Fecha_ini']==null){
        $query=$query." AND FechaAplicacion<='".$_GET['Fecha_fin']."'";
    }elseif(strlen($_GET['Fecha_ini'])>0 && strlen($_GET['Fecha_fin'])>0){
        $query=$query." AND (FechaAplicacion>='".$_GET['Fecha_ini']."' AND FechaAplicacion<='".$_GET['Fecha_fin']."')";
    }
    
    
   $count=1;
    $query= "SELECT HorarioExamenGrupo.* FROM HorarioExamenGrupo
             JOIN GrupoExamen ON HorarioExamenGrupo.Id_horario=GrupoExamen.IdHorarioExamen
             JOIN DocenteExamen ON HorarioExamenGrupo.Id_horario=DocenteExamen.IdHorarioExamen
             JOIN Grupos ON GrupoExamen.IdGrupo=Grupos.Id_grupo
             JOIN materias_uml ON Grupos.Id_mat=materias_uml.Id_mat
             WHERE HorarioExamenGrupo.Id_ciclo=".$Id_ciclo." ".$query;
    foreach($DaoHorarioExamenGrupo->advanced_query($query) as $examen){     
         $per=$DaoPeriodosExamenes->show($examen->getId_periodoExamen());
         $grupos=array();
         $docentes=array();
         foreach($DaoGrupoExamen->getGruposExamenHorario($examen->getId()) as $ObjGrupo){
                   $grupo=$DaoGrupos->show($ObjGrupo->getIdGrupo());
                   $clave=$DaoGrupos->covertirCadena($grupo->getClave(), 1);
                   array_push($grupos, $clave);
                   $materia=$DaoMaterias->show($grupo->getId_mat());
         } 

         foreach($DaoDocenteExamen->getDocentesExamenHorario($examen->getId()) as $ObjDocente){
               $docente=$DaoDocentes->show($ObjDocente->getId_docen());
               $docenteNombre=$DaoDocentes->covertirCadena($docente->getNombre_docen()." ".$docente->getApellidoP_docen(),1);
               array_push($docentes, $docenteNombre);
         }
         $materia=$DaoMaterias->show($grupo->getId_mat());
         $start=  substr($examen->getStart(),strpos($examen->getStart(), "T")+1);
         $end=  substr($examen->getEnd(),strpos($examen->getEnd(), "T")+1);
         $aula=$DaoAulas->show($examen->getId_aula());
         $c=$DaoCiclos->show($examen->getId_ciclo());
         
        $count++;
        $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
        $objPHPExcel->getActiveSheet()->setCellValue("B$count", implode(", ", $docentes));
        $objPHPExcel->getActiveSheet()->setCellValue("C$count", implode(", ", $grupos));
        $objPHPExcel->getActiveSheet()->setCellValue("D$count", $DaoDocentes->covertirCadena($materia->getNombre(),2));
        $objPHPExcel->getActiveSheet()->setCellValue("E$count", $DaoDocentes->covertirCadena($aula->getNombre_aula(),2));
        $objPHPExcel->getActiveSheet()->setCellValue("F$count", $examen->getFechaAplicacion());
        $objPHPExcel->getActiveSheet()->setCellValue("G$count", $start);
        $objPHPExcel->getActiveSheet()->setCellValue("H$count", $end);
        $objPHPExcel->getActiveSheet()->setCellValue("I$count", $c->getClave());
        $objPHPExcel->getActiveSheet()->setCellValue("J$count", $per->getNombre());

    }
               
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=Exámenes-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');

}