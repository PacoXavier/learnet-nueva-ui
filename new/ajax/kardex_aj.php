<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 


if ($_POST['action'] == "buscarAlum") {
    $DaoAlumnos = new DaoAlumnos();
    foreach ($DaoAlumnos->buscarAlumnosAltasYBajas($_POST['buscar']) as $k => $v) {
        ?>
        <li id_alum="<?php echo $v->getId(); ?>" onclick="getOfertas(<?php echo $v->getId(); ?>)"><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li>
        <?php
    }
}


if ($_POST['action'] == "getOfertas") {
    ?>
    <option value="0"></option>
    <?php
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    foreach ($DaoOfertasAlumno->getOfertasAlumnoAltasYBajas($_POST['Id_alum']) as $OfertaAlumno) {
        $nombre_ori = "";
        $oferta = $DaoOfertas->show($OfertaAlumno->getId_ofe());
        $esp = $DaoEspecialidades->show($OfertaAlumno->getId_esp());
        if ($OfertaAlumno->getId_ori() > 0) {
            $ori = $DaoOrientaciones->show($OfertaAlumno->getId_ori());
            $nombre_ori = $ori->getNombre();
        }
        ?>
        <option value="<?php echo $OfertaAlumno->getId() ?>"><?php echo $oferta->getNombre_oferta() . " " . $esp->getNombre_esp() . " " . $nombre_ori; ?></option>
        <?php
    }
}


if($_POST['action']=="filtro"){
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoAlumnos = new DaoAlumnos();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoCiclos = new DaoCiclos();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoGrados= new DaoGrados();
    $DaoMateriasAcreditadas= new DaoMateriasAcreditadas();
    $base= new base();
    $nombre_ori="";
    $alum = $DaoAlumnos->show($_POST['Id_alumn']);
    $ofertaAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $oferta = $DaoOfertas->show($ofertaAlumno->getId_ofe());
    $esp = $DaoEspecialidades->show($ofertaAlumno->getId_esp());
    if ($ofertaAlumno->getId_ori() > 0) {
      $ori = $DaoOrientaciones->show($ofertaAlumno->getId_ori());
      $nombre_ori = $ori->getNombre();
    }

    $Opcion_pago="";
    if($ofertaAlumno->getOpcionPago()==1){
        $Opcion_pago="Plan por materias";
    }else{
        $Opcion_pago="Plan completo";
    }
    ?>
    <table class="table">
        <thead>
        <tr>
           <td colspan="10" style="text-align: center; font-size: 12px;">Datos del estudiante</td>
        </tr>
        <tr>
            <td>Matricula</td>
            <td class="normal"><?php echo $alum->getMatricula() ?></td>
            <td>Nombre</td>
            <td colspan="3" class="normal"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
            <td>Opci&oacute;n de pago</td>
            <td class="normal"><?php echo $Opcion_pago; ?></td>
        </tr>
        <tr>
             <td>Carrera</td>
             <td colspan="3" class="normal"><?php echo $esp->getNombre_esp(); ?></td>
             <td>Orientaci&oacute;n</td>
             <td class="normal"><?php echo $nombre_ori; ?></td>
             <td>Nivel</td>
             <td colspan="3" class="normal"><?php echo $oferta->getNombre_oferta(); ?></td>
        </tr>
        </thead>
    </table>
    <?php
    foreach($DaoCiclosAlumno->getCiclosOferta($_POST['Id_ofe_alum']) as $cicloOferta){
          $ClaveCiclo="";
          if($cicloOferta->getId_ciclo()>0){
              $ciclo=$DaoCiclos->show($cicloOferta->getId_ciclo());
              $ClaveCiclo=$ciclo->getClave();
          }
        ?>
        <table class="table">
          <thead>
              <tr>
                    <td colspan="8" style="text-align: center; font-size: 12px;">Calendario <?php echo $ClaveCiclo?></td>
                </tr>
                <tr>
                    <td>Clave</td>
                    <td>Nombre de la materia</td>
                    <td style="text-align: center">Tipo cal.</td>
                    <td style="text-align: center">Grado</td>
                    <td style="text-align: center">Calificaci&oacute;n</td>
                    <td style="text-align: center">Extraordinario</td>
                    <td style="text-align: center">Tipo mat.</td>
                    <td style="text-align: center">Nc</td>
                </tr>
           </thead>
            <tbody>
                <?php

                    foreach($DaoMateriasAcreditadas->getMateriasAcreditadasByOfeAlumno($_POST['Id_ofe_alum']) as $acreditada) {
                            $mat_esp=$DaoMateriasEspecialidad->show($acreditada->getId_mat_esp());
                            $mat=$DaoMaterias->show($mat_esp->getId_mat());
                            $grado=$DaoGrados->show($mat_esp->getGrado_mat());

                            $NombreMat=$mat->getNombre();
                            if(strlen($mat_esp->getNombreDiferente())>0){
                                $NombreMat=$mat_esp->getNombreDiferente(); 
                            }
                            ?>
                           <tr>
                                <td><?php echo $mat->getClave_mat() ?> </td>
                                <td style="width: 407px"><?php echo $NombreMat ?></td>
                                <td style="text-align: center;">Acreditada (AC)</td>
                                <td style="text-align: center;"><?php echo $grado->getGrado() ?></td>
                                <td style="text-align: center;">AC</td>
                                <td style="text-align: center;">AC</td>
                                <td style="text-align: center;">AC</td>
                                <td style="text-align: center;"><?php echo $mat->getCreditos() ?></td>
                            </tr>
                                <?php
                }     

                foreach ($DaoMateriasCicloAlumno->getMateriasCicloAlumno($cicloOferta->getId()) as $materiaCiclo) {
                        $mat_esp=$DaoMateriasEspecialidad->show($materiaCiclo->getId_mat_esp());
                        $mat=$DaoMaterias->show($mat_esp->getId_mat());
                        $grado=$DaoGrados->show($mat_esp->getGrado_mat());
                        $tipoMat="";
                        if($materiaCiclo->getTipo()==1){
                           $tipoMat="Normal"; 
                        }elseif($materiaCiclo->getTipo()==2){
                           $tipoMat="Extra";  
                        }

                        $NombreMat=$mat->getNombre();
                        if(strlen($mat_esp->getNombreDiferente())>0){
                            $NombreMat=$mat_esp->getNombreDiferente(); 
                        }
                         if($materiaCiclo->getActivo()==1){
                            
                            //Por si pusieron nomenclatura AC, CP, etc
                            $CalTotalParciales=$base->getCalificacion($materiaCiclo->getCalTotalParciales());
                            $CalExtraordinario=$base->getCalificacion($materiaCiclo->getCalExtraordinario());
                        ?>
                          <tr id-ciclo-mat="<?php echo $materiaCiclo->getId()?>">
                                <td><?php echo $mat->getClave_mat() ?> </td>
                                <td style="width: 407px"><?php echo $NombreMat?></td>
                                <td style="text-align: center;">ORDINARIO (OE)</td>
                                <td style="text-align: center;"><?php echo $grado->getGrado() ?></td>
                                <td style="text-align: center;"><input type="text" value="<?php echo $CalTotalParciales;?>" disabled="disabled"  class="CalTotalParciales"/></td>
                                <td style="text-align: center;"><input type="text" value="<?php echo $CalExtraordinario;?>" disabled="disabled" class="CalExtraordinario"/></td>
                                <td style="text-align: center;"><?php echo $tipoMat;?></td>
                                <td style="text-align: center;"><?php echo $mat->getCreditos() ?></td>
                            </tr>
                        <?php
                         }else{
                             ?>
                           <tr id-ciclo-mat="<?php echo $materiaCiclo->getId()?>">
                                <td><?php echo $mat->getClave_mat() ?> </td>
                                <td style="width: 407px"><?php echo $NombreMat ?></td>
                                <td style="text-align: center;">BAJA</td>
                                <td style="text-align: center;"><?php echo $grado->getGrado()  ?></td>
                                <td style="text-align: center;">BAJA</td>
                                <td style="text-align: center;">BAJA</td>
                                <td style="text-align: center;"><?php echo $tipoMat;?></td>
                                <td style="text-align: center;"><?php echo $mat->getCreditos() ?></td>
                            </tr>
                            <?php
                         }
                }
                ?>
            </tbody>
        </table>
        <?php
        }

}
