<?php
require_once('activate_error.php');
require_once('../require_daos.php');


if ($_POST['action'] == "mostrar_horario") {
    update_page($_POST['Id_docente'], $_POST['Id_ciclo']);
}

function update_page($Id_docente, $Id_ciclo) {
    $DaoHoras = new DaoHoras();
    $DaoUsuarios= new DaoUsuarios();
    $base = new base();
    $DaoDiasPlantel = new DaoDiasPlantel();

    $paramDias = array();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query = "SELECT * FROM Horas LEFT JOIN
      (SELECT Lunes,Martes,Miercoles,Jueves,Viernes,Sabado,Domingo,Id_docente,Id_hora as Id_hora_dispo
      FROM Disponibilidad_docente WHERE Id_docente=" . $Id_docente . " AND Id_ciclo=" . $Id_ciclo . ") as Disponibilidad  
      ON Horas.Id_hora= Disponibilidad.Id_hora_dispo ORDER BY Id_hora ASC";
    foreach ($base->advanced_query($query) as $row_Horas) {

        foreach ($DaoDiasPlantel->getJoinDiasPlantel($usu->getId_plantel()) as $dia) {
            $paramDias[$dia['Id_dia']]['disp_docente'] = "";
            $paramDias[$dia['Id_dia']]['nombre_materia'] = "";
            $paramDias[$dia['Id_dia']]['aula_materia'] = "";
            $paramDias[$dia['Id_dia']]['Id_grupo'] = "";
            $paramDias[$dia['Id_dia']]['Id_mat'] = "";
            $paramDias[$dia['Id_dia']]['Id_aula'] = "";
            $paramDias[$dia['Id_dia']]['clave'] = "";
            $paramDias[$dia['Id_dia']]['disponibilidad'] = "";
        }
        
        if ($row_Horas['Lunes'] == 1) {
            $paramDias[1]['disponibilidad'] = "disponibilidad";
        }
        if ($row_Horas['Martes'] == 1) {
            $paramDias[2]['disponibilidad'] = "disponibilidad";
        }
        if ($row_Horas['Miercoles'] == 1) {
            $paramDias[3]['disponibilidad'] = "disponibilidad";
        }
        if ($row_Horas['Jueves'] == 1) {
            $paramDias[4]['disponibilidad'] = "disponibilidad";
        }
        if ($row_Horas['Viernes'] == 1) {
            $paramDias[5]['disponibilidad'] = "disponibilidad";
        }
        if ($row_Horas['Sabado'] == 1) {
            $paramDias[6]['disponibilidad'] = "disponibilidad";
        }
        if ($row_Horas['Domingo'] == 1) {
            $paramDias[7]['disponibilidad'] = "disponibilidad";
        }
        //Horas ulitizadas por el docente
        $query = "SELECT HD.*,Gp.Clave,Gp.Id_mat,Materias.Nombre,aulas.Nombre_aula,aulas.Clave_aula FROM Horario_docente as HD 
            JOIN Grupos as Gp ON HD.Id_grupo=Gp.Id_grupo 
            JOIN materias_uml as Materias ON Gp.Id_mat= Materias.Id_mat
            JOIN aulas ON aulas.Id_aula= HD.Aula
            WHERE Hora= " . $row_Horas['Id_hora'] . " AND HD.Id_ciclo=" . $Id_ciclo . " AND Id_docente=" . $Id_docente;
        foreach ($base->advanced_query($query) as $row_Horario_docente) {

            if ($row_Horario_docente['Lunes'] == 1) {
                $paramDias[1]['disp_docente'] = 'horas_docente_asignadas';
                $paramDias[1]['nombre_materia'] = $row_Horario_docente['Nombre'];
                $paramDias[1]['aula_materia'] = $row_Horario_docente['Nombre_aula'];
                $paramDias[1]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                $paramDias[1]['Id_mat'] = $row_Horario_docente['Id_mat'];
                $paramDias[1]['Id_aula'] = $row_Horario_docente['Aula'];
                $paramDias[1]['clave'] = $row_Horario_docente['Clave'];
            }
            if ($row_Horario_docente['Martes'] == 1) {
                $paramDias[2]['disp_docente'] = 'horas_docente_asignadas';
                $paramDias[2]['nombre_materia'] = $row_Horario_docente['Nombre'];
                $paramDias[2]['aula_materia'] = $row_Horario_docente['Nombre_aula'];
                $paramDias[2]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                $paramDias[2]['Id_mat'] = $row_Horario_docente['Id_mat'];
                $paramDias[2]['Id_aula'] = $row_Horario_docente['Aula'];
                $paramDias[2]['clave'] = $row_Horario_docente['Clave'];
            }
            if ($row_Horario_docente['Miercoles'] == 1) {
                $paramDias[3]['disp_docente'] = 'horas_docente_asignadas';
                $paramDias[3]['nombre_materia'] = $row_Horario_docente['Nombre'];
                $paramDias[3]['aula_materia'] = $row_Horario_docente['Nombre_aula'];
                $paramDias[3]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                $paramDias[3]['Id_mat'] = $row_Horario_docente['Id_mat'];
                $paramDias[3]['Id_aula'] = $row_Horario_docente['Aula'];
                $paramDias[3]['clave'] = $row_Horario_docente['Clave'];
            }
            if ($row_Horario_docente['Jueves'] == 1) {
                $paramDias[4]['disp_docente'] = 'horas_docente_asignadas';
                $paramDias[4]['nombre_materia'] = $row_Horario_docente['Nombre'];
                $paramDias[4]['aula_materia'] = $row_Horario_docente['Nombre_aula'];
                $paramDias[4]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                $paramDias[4]['Id_mat'] = $row_Horario_docente['Id_mat'];
                $paramDias[4]['Id_aula'] = $row_Horario_docente['Aula'];
                $paramDias[4]['clave'] = $row_Horario_docente['Clave'];
            }
            if ($row_Horario_docente['Viernes'] == 1) {
                $paramDias[5]['disp_docente'] = 'horas_docente_asignadas';
                $paramDias[5]['nombre_materia'] = $row_Horario_docente['Nombre'];
                $paramDias[5]['aula_materia'] = $row_Horario_docente['Nombre_aula'];
                $paramDias[5]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                $paramDias[5]['Id_mat'] = $row_Horario_docente['Id_mat'];
                $paramDias[5]['Id_aula'] = $row_Horario_docente['Aula'];
                $paramDias[5]['clave'] = $row_Horario_docente['Clave'];
            }
            if ($row_Horario_docente['Sabado'] == 1) {
                $paramDias[6]['disp_docente'] = 'horas_docente_asignadas';
                $paramDias[6]['nombre_materia'] = $row_Horario_docente['Nombre'];
                $paramDias[6]['aula_materia'] = $row_Horario_docente['Nombre_aula'];
                $paramDias[6]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                $paramDias[6]['Id_mat'] = $row_Horario_docente['Id_mat'];
                $paramDias[6]['Id_aula'] = $row_Horario_docente['Aula'];
                $paramDias[6]['clave'] = $row_Horario_docente['Clave'];
            }

            if ($row_Horario_docente['Domingo'] == 1) {
                $paramDias[7]['disp_docente'] = 'horas_docente_asignadas';
                $paramDias[7]['nombre_materia'] = $row_Horario_docente['Nombre'];
                $paramDias[7]['aula_materia'] = $row_Horario_docente['Nombre_aula'];
                $paramDias[7]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                $paramDias[7]['Id_mat'] = $row_Horario_docente['Id_mat'];
                $paramDias[7]['Id_aula'] = $row_Horario_docente['Aula'];
                $paramDias[7]['clave'] = $row_Horario_docente['Clave'];
            }
        }
        ?>
        <tr id_hora="<?php echo $row_Horas['Id_hora'] ?>">
            <td><?php echo $row_Horas['Texto_hora'] ?></td>
            <?php
            foreach ($DaoDiasPlantel->getJoinDiasPlantel($usu->getId_plantel()) as $dia) {
                $id = $dia['Id_dia'];
                $onclick="";
                if(isset($paramDias[$id]['Id_grupo']) && $paramDias[$id]['Id_grupo']>0){
                   $onclick='onclick="mostrarCalificacionesGrupo('.$paramDias[$id]["Id_grupo"].')"'; 
                }
                ?>
                <td class="<?php echo $paramDias[$id]['disponibilidad'] . " " . $paramDias[$id]['disp_docente'] ?>" <?php echo $onclick;?>><span><?php echo $paramDias[$id]['nombre_materia']; ?></span><br><?php echo $paramDias[$id]['aula_materia'] ?><br><span><?php echo $paramDias[$id]['clave']; ?></span></td>
                <?php
            }
            ?>
        </tr>
        <?php
    }
}
