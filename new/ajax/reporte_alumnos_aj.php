<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if(isset($_POST['action']) && $_POST['action']=="filtro"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoMediosEnterar= new DaoMediosEnterar();
    $DaoUsuarios= new DaoUsuarios();
    $DaoTurnos= new DaoTurnos();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query="";

    if(isset($_POST['Id_ofe']) && $_POST['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_POST['Id_ofe'];
    }
    if(isset($_POST['Id_esp']) && $_POST['Id_esp']>0){
        $query=$query." AND Id_esp=".$_POST['Id_esp'];
    }
    if(isset($_POST['Id_ori']) && $_POST['Id_ori']>0){
        $query=$query." AND Id_ori=".$_POST['Id_ori'];
    }
    if(isset($_POST['Opcion_pago']) && $_POST['Opcion_pago']>0){
        $query=$query." AND Opcion_pago=".$_POST['Opcion_pago'];
    }
    if(isset($_POST['Turno']) && $_POST['Turno']>0){
        $query=$query." AND ofertas_alumno.Turno=".$_POST['Turno'];
    }
    
    $count=1;
    $query="SELECT * FROM inscripciones_ulm 
        JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
        WHERE tipo=1  
              AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()." 
              AND Activo_oferta=1 
              AND Baja_ofe IS NULL 
              AND FechaCapturaEgreso IS NULL
              AND IdCicloEgreso IS NULL
              AND IdUsuEgreso IS NULL ".$query." 
        ORDER BY Id_ins DESC";
    $consulta=$base->advanced_query($query);
    $row_consulta = $consulta->fetch_assoc();
    $totalRows_consulta= $consulta->num_rows;
     if($totalRows_consulta>0){
       do{
           $nombre_ori="";
           $oferta = $DaoOfertas->show($row_consulta['Id_ofe']);
           $esp = $DaoEspecialidades->show($row_consulta['Id_esp']);
           if ($row_consulta['Id_ori'] > 0) {
              $ori = $DaoOrientaciones->show($row_consulta['Id_ori']);
              $nombre_ori = $ori->getNombre();
            }
            $opcion="Plan por materias"; 
            if($row_consulta['Opcion_pago']==2){
              $opcion="Plan completo";  
            }
            
           $grado = $DaoOfertasAlumno->getLastGradoOferta($row_consulta['Id_ofe_alum']);
           $Grado=$DaoGrados->show($grado['Id_grado_ofe']);
            $MedioEnt="";
            if($row_consulta['Id_medio_ent']>0){
              $medio=$DaoMediosEnterar->show($row_consulta['Id_medio_ent']);
              $MedioEnt=$medio->getMedio();
            }
                                
            $Medio_aux=$row_consulta['Id_medio_ent'];
            if(isset($_POST['Id_medio']) && $_POST['Id_medio']>0){
                $Medio_aux=$_POST['Id_medio'];
            }
            
            $Grado_aux=$Grado->getId();
            if(isset($_POST['Id_grado']) && $_POST['Id_grado']>0){
                $Grado_aux=$_POST['Id_grado'];
            }

             $tur = $DaoTurnos->show($row_consulta['Turno']);
             $turno=$tur->getNombre();
            
            if($Grado->getId()==$Grado_aux && $row_consulta['Id_medio_ent']==$Medio_aux){
              ?>
                      <tr id_alum="<?php echo $row_consulta['Id_ins'];?>" id_ofe_alum="<?php echo $row_consulta['Id_ofe_alum'];?>">
                            <td><?php echo $count;?></td>
                            <td><?php echo $row_consulta['Matricula'] ?></td>
                            <td style="width: 115px;"><?php echo $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins'] ?></td>
                            <td><?php echo $row_consulta['Email_ins']?></td>
                            <td><?php echo $oferta->getNombre_oferta(); ?></td>
                            <td><?php echo $esp->getNombre_esp(); ?></td>
                            <td><?php echo $nombre_ori; ?></td>
                            <td><?php echo $Grado->getGrado(); ?></td>
                            <td><?php echo $opcion; ?></td>
                            <td><?php echo $row_consulta['Fecha_ins']; ?></td>
                            <td><?php echo $MedioEnt; ?></td>
                            <td><?php echo $turno;?></td>
                            <td><input type="checkbox"> </td>
                            <td>
                                   <?php
                                    if($oferta->getTipoOferta()==1){
                                        ?>
                                         <a href="pago.php?id=<?php echo $row_consulta['Id_ofe_alum']?>" target="_blank"><button>Pagos</button></a>
                                        <?php
                                    }elseif($oferta->getTipoOferta()==2){
                                        ?>
                                         <a href="cargos_curso.php?id=<?php echo $row_consulta['Id_ofe_alum']?>" target="_blank"><button>Pagos</button></a>
                                        <?php   
                                    }
                                    ?>
                            </td>
                      </tr>
                      <?php
                      $count++;
               }
            }while( $row_consulta = $consulta->fetch_assoc());
    }
}




if(isset($_GET['action']) && $_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'MATRICULA');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'EMAIL');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'OFERTA');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'CARRERA');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'ORIENTACIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'GRADO');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'OPCIÓN DE PAGO');
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'FECHA. INTE');
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'SE ENTERO POR');
    $objPHPExcel->getActiveSheet()->setCellValue('L1', 'TURNO');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','L') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoMediosEnterar= new DaoMediosEnterar();
    $DaoUsuarios= new DaoUsuarios();
    $DaoTurnos= new DaoTurnos();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query="";

    if(isset($_GET['Id_ofe']) && $_GET['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_GET['Id_ofe'];
    }
    if(isset($_GET['Id_esp']) && $_GET['Id_esp']>0){
        $query=$query." AND Id_esp=".$_GET['Id_esp'];
    }
    if(isset( $_GET['Id_ori']) && $_GET['Id_ori']>0){
        $query=$query." AND Id_ori=".$_GET['Id_ori'];
    }
    if(isset($_GET['Opcion_pago']) && $_GET['Opcion_pago']>0){
        $query=$query." AND Opcion_pago=".$_GET['Opcion_pago'];
    }
    if(isset($_GET['Turno']) && $_GET['Turno']>0){
        $query=$query." AND ofertas_alumno.Turno=".$_GET['Turno'];
    }
    
    $count=1;
    $query="SELECT * FROM inscripciones_ulm 
        JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
        WHERE tipo=1  
              AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()." 
              AND Activo_oferta=1 
              AND Baja_ofe IS NULL 
              AND FechaCapturaEgreso IS NULL
              AND IdCicloEgreso IS NULL
              AND IdUsuEgreso IS NULL ".$query." 
        ORDER BY Id_ins DESC";
    $consulta=$base->advanced_query($query);
    $row_consulta = $consulta->fetch_assoc();
    $totalRows_consulta= $consulta->num_rows;
     if($totalRows_consulta>0){
       do{
           $nombre_ori="";
           $oferta = $DaoOfertas->show($row_consulta['Id_ofe']);
           $esp = $DaoEspecialidades->show($row_consulta['Id_esp']);
           if ($row_consulta['Id_ori'] > 0) {
              $ori = $DaoOrientaciones->show($row_consulta['Id_ori']);
              $nombre_ori = $ori->getNombre();
            }
            $opcion="Plan por materias"; 
            if($row_consulta['Opcion_pago']==2){
              $opcion="Plan completo";  
            }
            
           $grado = $DaoOfertasAlumno->getLastGradoOferta($row_consulta['Id_ofe_alum']);
           $Grado=$DaoGrados->show($grado['Id_grado_ofe']);

            $MedioEnt="";
            if($row_consulta['Id_medio_ent']>0){
              $medio=$DaoMediosEnterar->show($row_consulta['Id_medio_ent']);
              $MedioEnt=$medio->getMedio();
            }
                                
            $Medio_aux=$row_consulta['Id_medio_ent'];
            if(isset($_GET['Id_medio']) && $_GET['Id_medio']>0){
                $Medio_aux=$_GET['Id_medio'];
            }
            
            $Grado_aux=$Grado->getId();
            if(isset($_GET['Id_grado']) && $_GET['Id_grado']>0){
                $Grado_aux=$_GET['Id_grado'];
            }

            $tur = $DaoTurnos->show($row_consulta['Turno']);
            $turno=$tur->getNombre();
            
            if($Grado->getId()==$Grado_aux && $row_consulta['Id_medio_ent']==$Medio_aux){
                $count++;
                $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
                $objPHPExcel->getActiveSheet()->setCellValue("B$count", $row_consulta['Matricula']);
                $objPHPExcel->getActiveSheet()->setCellValue("C$count", $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins']);
                $objPHPExcel->getActiveSheet()->setCellValue("D$count", $row_consulta['Email_ins']);
                $objPHPExcel->getActiveSheet()->setCellValue("E$count", $oferta->getNombre_oferta());
                $objPHPExcel->getActiveSheet()->setCellValue("F$count", $esp->getNombre_esp());
                $objPHPExcel->getActiveSheet()->setCellValue("G$count", $nombre_ori);
                $objPHPExcel->getActiveSheet()->setCellValue("H$count", $Grado->getGrado());
                $objPHPExcel->getActiveSheet()->setCellValue("I$count", $opcion);
                $objPHPExcel->getActiveSheet()->setCellValue("J$count", $row_consulta['Fecha_ins']);
                $objPHPExcel->getActiveSheet()->setCellValue("K$count", $MedioEnt);
                $objPHPExcel->getActiveSheet()->setCellValue("L$count", $turno);
               }
            }while($row_consulta = $consulta->fetch_assoc());
    }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=Alumnos-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}




