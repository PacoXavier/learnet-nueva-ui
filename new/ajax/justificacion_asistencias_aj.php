<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="buscar_int"){
    $DaoAlumnos= new DaoAlumnos();
    foreach($DaoAlumnos->buscarAlumno($_POST['buscar']) as $k=>$v){
      ?>
             <a href="justificacion_asistencias.php?id=<?php echo $v->getId()?>">
                    <li><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li>
            </a>
      <?php      
    }
    
}



if($_POST['action']=="mostrar_box_justificacion"){
    $DaoJustificaciones= new DaoJustificaciones();
    $Dia_justificado="";
    $Comentarios="";
    $HoraInicio="";
    $HoraFin="";
    $Id_jus=0;
    if(isset($_POST['Id_jus']) && $_POST['Id_jus']>0){
      $just=$DaoJustificaciones->show($_POST['Id_jus']);
      $Id_jus=$just->getId();
      $Dia_justificado=$just->getDia_justificado();
      $Comentarios=$just->getComentarios();
      $HoraInicio=$just->getHoraIni();
      $HoraFin=$just->getHoraFin();
    }
    ?>
    <div class="box-emergente-form box-justificacion">
      <h1>Justificar falta</h1>
      <p>Fecha a justificar<br>                                   
          <select id="list_fechas">
                <option value="">Selecciona una fecha</option>
                <?php
                //Muestra las faltas que tiene el alumno en cierto ciclo
                foreach($DaoJustificaciones->getFaltasAlumnoCiclo($_POST['Id_ciclo'],$_POST['Id_alum'])as $v){
                ?>
                <option value="<?php echo $v['Fecha_asis']?>" <?php if($Dia_justificado==$v['Fecha_asis']){?> selected="selected" <?php } ?>><?php echo $v['Fecha_asis']?></option>
                <?php
                }
                ?>
          </select>
      </p>
      <p>Hora de inicio<br><input type="text" id="horaInicio" class="time start" value="<?php echo $HoraInicio;?>"/></p>
      <p>Hora de fin<br><input type="text" id="horaFin" class="time end" value="<?php echo $HoraFin;?>"/></p>
      <p>Comentario:<br><textarea id="comentario"><?php echo $Comentarios;?></textarea></p>
      <button onclick="add_justificacion(<?php echo $Id_jus;?>)">Guardar</button>
      <button onclick="ocultar_error_layer()">Cancelar</button>
</div>   
<?php
}


if($_POST['action']=="mostrar_justificaciones"){
    update_page($_POST['Id_alum'],$_POST['Id_ciclo']);
}


if($_POST['action']=="add_justificacion"){
    
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $DaoJustificaciones= new DaoJustificaciones();
    $alum=$DaoAlumnos->show($_POST['Id_alum']);
    
    if($_POST['Id_jus']>0){
   
    $justificacion=$DaoJustificaciones->show($_POST['Id_jus']);
    $TextoHistorial="Edita fecha de justificación para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM()." de ".$justificacion->getDia_justificado()." a ".$_POST['Fecha'];
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Justificar clases");

    }else{
        $Justificaciones= new Justificaciones();
        $Justificaciones->setId_alum($alum->getId());
        $Justificaciones->setDate(date('Y-m-d H:i:s'));
        $Justificaciones->setDia_justificado($_POST['Fecha']);
        $Justificaciones->setHoraFin($_POST['HoraFin']);
        $Justificaciones->setHoraIni($_POST['HoraInicio']);
        $Justificaciones->setId_ciclo($_POST['Id_ciclo']);
        $Justificaciones->setId_usu($_COOKIE['admin/Id_usu']);
        $Justificaciones->setComentarios($_POST['Comentarios']);
        
        $DaoJustificaciones->add($Justificaciones);
    
        $TextoHistorial="Añade justificación para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", día ".$_POST['Fecha'];
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Justificar clases");
    }
    update_page($_POST['Id_alum'],$_POST['Id_ciclo']);
}




function update_page($Id_alum,$Id_ciclo) {
    $DaoAlumnos= new DaoAlumnos();
    $DaoCiclos= new DaoCiclos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoJustificaciones= new DaoJustificaciones();
    
    $_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-check-square-o"></i> Justificar faltas</h1>
                </div>
                <div class="seccion">
                    <div id="box_uno">
                        <?php
                        if ($Id_alum > 0) {
                            $alum = $DaoAlumnos->show($Id_alum);
                            $nombre = $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM();
                        }
                        ?>
                        <ul class="form">
                            <li>Alumno<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o clave" value="<?php echo $nombre ?>"/>
                                <ul id="buscador_int"></ul>
                            </li><br>
                            <?php
                            if ($Id_alum> 0) {
                                ?>
                                <li>Ciclo<br><select id="ciclo" onchange="mostrar_justificaciones()">
                                        <option value="0">Selecciona el ciclo</option>
                                        <?php
                                            foreach($DaoCiclos->getCiclosFuturos() as $ciclo){
                                             ?>
                                              <option value="<?php echo $ciclo->getId() ?>" <?php if($ciclo->getId()==$Id_ciclo){ ?> selected="selected" <?php } ?>><?php echo $ciclo->getClave() ?></option>
                                              <?php
                                              }
                                        ?>
                                        </select>
                                </li> 
                            <?php
                            }
                            ?>
                    </div>
                    <table class="table">
                            <thead>
                                <tr>
                                <td>#</td>
                                <td>Dia justificado</td>
                                <td>Usuario</td>
                                <td>Fecha de captura</td>
                                <td>Hora inicio</td>
                                <td>Hora fin</td>
                                <td>Comentarios</td>
                                <td>Acciones</td>
                                </tr>
                            </thead>
                            <tbody>
                             <?php  
                             $count=1;
                             foreach($DaoJustificaciones->getJustificacionesByCicloAlum($Id_alum,$Id_ciclo) as $just){
                                 $u = $DaoUsuarios->show($just->getId_usu());
                             ?>
                                    <tr id_jus="<?php echo $just->getId()?>">
                                        <td><?php echo $count?></td>
                                        <td><?php echo $just->getDia_justificado()?></td>
                                        <td><?php echo $u->getNombre_usu() . " " . $u->getApellidoP_usu() . " " . $u->getApellidoM_usu()?></td>
                                        <td><?php echo $just->getDate()?></td>
                                        <td><?php echo $just->getHoraIni()?></td>
                                        <td><?php echo $just->getHoraFin()?></td>
                                        <td><?php echo $just->getComentarios()?></td>
                                        <td><button onclick="delete_justi(<?php echo $just->getId()?>)">Eliminar</button></td>
                                    </tr>
                                  <?php
                                  $count++;
                                 }
                             ?>
                            </tbody>
                        </table>
                </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                 require_once '../estandares/menu_derecho.php';
                ?>
                   <ul>
                        <li><span onclick="mostrar_box_justificacion()">Justificar falta </span></li>
                </ul>
            </div>
        </td>
    </tr>  
    <?php
}




if($_POST['action']=="delete_justi"){
    $DaoJustificaciones= new DaoJustificaciones();
    $justificacion=$DaoJustificaciones->show($_POST['Id_jus']);
    
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $alum=$DaoAlumnos->show($_POST['Id_alum']);     
    $TextoHistorial="Elimina dia justificado ".$justificacion->getDia_justificado()." para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Justificar clases");
    
    $DaoJustificaciones->delete($_POST['Id_jus']);
    update_page($_POST['Id_alum'],$_POST['Id_ciclo']);
}





