<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="buscar"){
    ?>
    <li><b style="color: black;">Alumnos</b></li>
    <?php 
    $DaoAlumnos= new DaoAlumnos();
    foreach($DaoAlumnos->buscarAlumno($_POST['buscar']) as $k=>$v){
      ?>
        <li id-data="<?php echo $v->getId();?>" tipo="alum"><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li>
      <?php      
    }
    ?>
    <li><b style="color: black;">Usuarios</b></li>
    <?php 
    $DaoUsuarios= new DaoUsuarios();
    foreach($DaoUsuarios->buscarUsuario($_POST['buscar']) as $k=>$v){
      ?>
       <li id-data="<?php echo $v->getId();?>" tipo="usu"><?php echo $v->getNombre_usu()." ".$v->getApellidoP_usu()." ".$v->getApellidoM_usu() ?></li>
     <?php      
    }
    ?>
    <li><b style="color: black;">Docentes</b></li>
    <?php 
    $DaoDocentes= new DaoDocentes();
    foreach($DaoDocentes->buscarDocente($_POST['buscar']) as $k=>$v){
      ?>
       <li id-data="<?php echo $v->getId();?>" tipo="docen"><?php echo $v->getNombre_docen()." ".$v->getApellidoP_docen()." ".$v->getApellidoM_docen() ?></li>
      <?php      
    }
}


if($_POST['action']=="filtro"){

    $base = new base();
    $DaoOfertas = new DaoOfertas();
    $DaoAlumnos = new DaoAlumnos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoGrados = new DaoGrados();
    $DaoDocentes= new DaoDocentes();
    $DaoCiclos= new DaoCiclos();
    $DaoAmonestaciones= new DaoAmonestaciones();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $ciclo=$DaoCiclos->getActual();
    $Id_ciclo=$ciclo->getId();
    if(isset($_POST['Id_ciclo']) && $_POST['Id_ciclo']>0){
       $Id_ciclo=$_POST['Id_ciclo'];
    }
     $query=" AND Id_ciclo=".$Id_ciclo;
     
    if(isset($_POST['Id_data']) && $_POST['Id_data']>0){
        $query=$query." AND Id_recibe=".$_POST['Id_data']." AND Tipo_recibe='".$_POST['Tipo']."'";
    }

    
    $count=1;
    foreach($DaoAmonestaciones->showAll($query) as $k=>$v){
     $cantidad=0;
     $ciclo= $DaoCiclos->show($v->getId_ciclo());
    
     if($v->getTipo_recibe()=="usu"){

         $tipoRecibe="Usuario";
         $tipo="usu";
         $usu_recibe = $DaoUsuarios->show($v->getId_recibe());
         $Id=$usu_recibe->getId();
         $ClaveRecibe=$usu_recibe->getClave_usu();
         $NombreRecibe=$usu_recibe->getNombre_usu()." ".$usu_recibe->getApellidoP_usu()." ".$usu_recibe->getApellidoM_usu();
         $cantidad=$DaoAmonestaciones->getCantidadAmonestaciones($Id,"usu");
     }elseif($v->getTipo_recibe()=="docen"){
         $tipoRecibe="Docente";
         $tipo="docen";
         $doc=$DaoDocentes->show($v->getId_recibe());
         $Id=$doc->getId();
         $ClaveRecibe=$doc->getClave_docen();
         $NombreRecibe=$doc->getNombre_docen()." ".$doc->getApellidoP_docen()." ".$doc->getApellidoM_docen();
         $cantidad=$DaoAmonestaciones->getCantidadAmonestaciones($Id,"docen");
     }elseif($v->getTipo_recibe()=="alum"){
         $tipoRecibe="Alumno";
         $tipo="alum";
         $alum=$DaoAlumnos->show($v->getId_recibe());
         $Id=$alum->getId();
         $ClaveRecibe=$alum->getMatricula();
         $NombreRecibe=$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
         $cantidad=$DaoAmonestaciones->getCantidadAmonestaciones($Id,"alum");
     }

     if($v->getTipo_reporta()=="usu"){
         $usu_reporta = $DaoUsuarios->show($v->getId_reporta());
         $ClaveReporta=$usu_reporta->getClave_usu();
         $NombreReporta=$usu_reporta->getNombre_usu()." ".$usu_reporta->getApellidoP_usu()." ".$usu_reporta->getApellidoM_usu();
     }elseif($v->getTipo_reporta()=="docen"){
         $doc=$DaoDocentes->show($v->getId_reporta());
         $ClaveReporta=$doc->getClave_docen();
         $NombreReporta=$doc->getNombre_docen()." ".$doc->getApellidoP_docen()." ".$doc->getApellidoM_docen();
     }

      $style='class=""';
      if($cantidad>=3){
          $style='class="pink"';
      }
    ?>
      <tr <?php echo $style;?>>
            <td><?php echo $count;?></td>
            <td><?php echo $ciclo->getClave()?></td>
            <td><?php echo $tipoRecibe?></td>
            <td><?php echo $ClaveRecibe?></td>
            <td><?php echo $NombreRecibe?></td>
            <td><?php echo $v->getFecha_rep()?></td>
            <td><?php echo $NombreReporta?></td>
            <td><?php echo $v->getMotivo()?></td>
            <td style="text-align: center;"><?php echo $cantidad;?></td>
            <td><a href="amolestaciones.php?id=<?php echo $Id?>&tipo=<?php echo $tipo ?>" target="_blank"><button>Amonestaciones</button></a></td>
        </tr>
      <?php

      $count++;
     }

}




if(isset($_GET['action']) && $_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'CICLO');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'TIPO');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'CLAVE');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'RECIBE');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'FECHA');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'REPORTA');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'MOTIVO');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'NUM. REPORTES');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','I') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    $DaoOfertas = new DaoOfertas();
    $DaoAlumnos = new DaoAlumnos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoGrados = new DaoGrados();
    $DaoDocentes= new DaoDocentes();
    $DaoCiclos= new DaoCiclos();
    $DaoAmonestaciones= new DaoAmonestaciones();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $ciclo=$DaoCiclos->getActual();
    $Id_ciclo=$ciclo->getId();
    if($_GET['Id_ciclo']>0){
       $Id_ciclo=$_GET['Id_ciclo'];
    }
     $query=" AND Id_ciclo=".$Id_ciclo;
     
    if($_GET['Id_data']>0){
        $query=$query." AND Id_recibe=".$_GET['Id_data']." AND Tipo_recibe='".$_GET['Tipo']."'";
    }

    
    $count=1;
    foreach($DaoAmonestaciones->showAll($query) as $k=>$v){
     $cantidad=0;
     $ciclo= $DaoCiclos->show($v->getId_ciclo());
     if($v->getTipo_recibe()=="usu"){

         $tipoRecibe="Usuario";
         $tipo="usu";
         $usu_recibe = $DaoUsuarios->show($v->getId_recibe());
         $Id=$usu_recibe->getId();
         $ClaveRecibe=$usu_recibe->getClave_usu();
         $NombreRecibe=$usu_recibe->getNombre_usu()." ".$usu_recibe->getApellidoP_usu()." ".$usu_recibe->getApellidoM_usu();
         $cantidad=$DaoAmonestaciones->getCantidadAmonestaciones($Id,"usu");
     }elseif($v->getTipo_recibe()=="docen"){
         $tipoRecibe="Docente";
         $tipo="docen";
         $doc=$DaoDocentes->show($v->getId_recibe());
         $Id=$doc->getId();
         $ClaveRecibe=$doc->getClave_docen();
         $NombreRecibe=$doc->getNombre_docen()." ".$doc->getApellidoP_docen()." ".$doc->getApellidoM_docen();
         $cantidad=$DaoAmonestaciones->getCantidadAmonestaciones($Id,"docen");
     }elseif($v->getTipo_recibe()=="alum"){
         $tipoRecibe="Alumno";
         $tipo="alum";
         $alum=$DaoAlumnos->show($v->getId_recibe());
         $Id=$doc->getId();
         $ClaveRecibe=$alum->getMatricula();
         $NombreRecibe=$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
         $cantidad=$DaoAmonestaciones->getCantidadAmonestaciones($Id,"alum");
     }

     if($v->getTipo_reporta()=="usu"){
         $usu_reporta = $DaoUsuarios->show($v->getId_reporta());
         $ClaveReporta=$usu_reporta->getClave_usu();
         $NombreReporta=$usu_reporta->getNombre_usu()." ".$usu_reporta->getApellidoP_usu()." ".$usu_reporta->getApellidoM_usu();
     }elseif($v->getTipo_reporta()=="docen"){
         $doc=$DaoDocentes->show($v->getId_reporta());
         $ClaveReporta=$doc->getClave_docen();
         $NombreReporta=$doc->getNombre_docen()." ".$doc->getApellidoP_docen()." ".$doc->getApellidoM_docen();
     }

      $style='class=""';
      if($cantidad>=3){
          $style='class="pink"';
      }
        $count++;
        $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
        $objPHPExcel->getActiveSheet()->setCellValue("B$count", $ciclo->getClave());
        $objPHPExcel->getActiveSheet()->setCellValue("C$count", $tipoRecibe);
        $objPHPExcel->getActiveSheet()->setCellValue("D$count", $ClaveRecibe);
        $objPHPExcel->getActiveSheet()->setCellValue("E$count", $NombreRecibe);
        $objPHPExcel->getActiveSheet()->setCellValue("F$count", $v->getFecha_rep());
        $objPHPExcel->getActiveSheet()->setCellValue("G$count", $NombreReporta);
        $objPHPExcel->getActiveSheet()->setCellValue("H$count", $v->getMotivo());
        $objPHPExcel->getActiveSheet()->setCellValue("I$count", $cantidad);

     }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=Amonestaciones-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}






