<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 


if (isset($_POST['action']) && $_POST['action'] == "buscarAlum") {
    $DaoAlumnos = new DaoAlumnos();
    foreach ($DaoAlumnos->buscarAlumnosAltasYBajas($_POST['buscar']) as $k => $v) {
        ?>
        <li id_alum="<?php echo $v->getId(); ?>"><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li>
        <?php
    }
}


if(isset($_POST['action']) && $_POST['action']=="filtro"){
    $query="";
    if(isset($_POST['Id_alum']) && $_POST['Id_alum']>0){
        $query=$query." AND Id_ins=".$_POST['Id_alum'];
    }
    if(strlen($_POST['fecha_ini'])>0 && $_POST['fecha_fin']==null){
        $query=" AND Baja_ofe>='".$_POST['fecha_ini']."'";
    }elseif(strlen($_POST['fecha_fin'])>0 && $_POST['fecha_ini']==null){
        $query=" AND Baja_ofe<='".$_POST['fecha_fin']."'";
    }elseif(strlen($_POST['fecha_ini'])>0 && strlen($_POST['fecha_fin'])>0){
        $query=" AND (Baja_ofe>='".$_POST['fecha_ini']."' AND Baja_ofe<='".$_POST['fecha_fin']."')";
    }
    if(isset($_POST['Id_motivo']) && $_POST['Id_motivo']>0){
        $query=$query." AND ofertas_alumno.Id_mot_baja=".$_POST['Id_motivo'];
    }
    if(isset($_POST['Id_oferta']) &&  $_POST['Id_oferta']>0){
        $query=$query." AND Id_ofe=".$_POST['Id_oferta'];
    }
    if(isset($_POST['Id_esp']) &&  $_POST['Id_esp']>0){
        $query=$query." AND Id_esp=".$_POST['Id_esp'];
    }
    if(isset($_POST['Id_ori']) &&  $_POST['Id_ori']>0){
        $query=$query." AND Id_ori=".$_POST['Id_ori'];
    }
    
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoMediosEnterar= new DaoMediosEnterar();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoMaterias= new DaoMaterias();
    $DaoMotivosBaja= new DaoMotivosBaja();
    $count=0;
    foreach ($DaoAlumnos->getAlumnosBajas($query,null,null) as $k=>$v){
           $nombre_ori="";
           $oferta = $DaoOfertas->show($v['Id_ofe']);
           $esp = $DaoEspecialidades->show($v['Id_esp']);
           if ($v['Id_ori'] > 0) {
              $ori = $DaoOrientaciones->show($v['Id_ori']);
              $nombre_ori = $ori->getNombre();
            }
            $opcion="Plan por materias"; 
            if($v['Opcion_pago']==2){
              $opcion="Plan completo";  
            }
            $grado = $DaoOfertasAlumno->getLastGradoOferta($v['Id_ofe_alum']);
            $gr="";
            if(isset($grado['Grado'])){
               $gr= $grado['Grado'];
            }
          ?>
            <tr id_alum="<?php echo $v['Id_ins'];?>" num-tr="<?php echo $count;?>" id-ofe-alum="<?php echo $v['Id_ofe_alum']?>">
                <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $v['Matricula'] ?></td>
                <td style="width: 115px;" onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $oferta->getNombre_oferta(); ?></td>
                <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $esp->getNombre_esp(); ?></td>
                <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $nombre_ori; ?></td>
                <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $gr ?></td>
                <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $base->formatFecha($v['Baja_ofe']) ?></td>
                <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $v['Nombre']; ?></td>
                <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $v['Comentario_baja']; ?></td>
                <td class="td-center"><input type="checkbox"> </td>
                <td class="td-center">
                    <a href="solicitud_baja.php?id_ofe_alum=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button><i class="fa fa-file-o"></i> Solicitud</button></a>
                    <?php
                    if($oferta->getTipoOferta()==1){
                        ?>
                         <button onclick="mostrarCicloReactivar(<?php echo $v['Id_ofe_alum'] ?>)">Reactivar</button>
                         <a href="pago.php?id=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button>Cobranza</button></a>
                        <?php
                    }elseif($oferta->getTipoOferta()==2){
                        ?>
                         <a href="cargos_curso.php?id=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button>Cobranza</button></a>
                        <?php   
                    }
                    ?>
                </td>
              </tr>
              <?php
              $count++;
    }
}




if(isset($_GET['action']) && $_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'MATRICULA');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'OFERTA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'CARRERA');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'ORIENTACIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'GRADO');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'FECHA DE BAJA');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'MOTIVO');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'COMENTARIOS');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','J') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    $query="";
    if($_GET['Id_alum']>0){
        $query=$query." AND Id_ins=".$_GET['Id_alum'];
    }
    if(strlen($_GET['fecha_ini'])>0 && $_GET['fecha_fin']==null){
        $query=" AND Baja_ofe>='".$_POST['fecha_ini']."'";
    }elseif(strlen($_GET['fecha_fin'])>0 && $_GET['fecha_ini']==null){
        $query=" AND Baja_ofe<='".$_GET['fecha_fin']."'";
    }elseif(strlen($_GET['fecha_ini'])>0 && strlen($_GET['fecha_fin'])>0){
        $query=" AND (Baja_ofe>='".$_GET['fecha_ini']."' AND Baja_ofe<='".$_GET['fecha_fin']."')";
    }
    if($_GET['Id_motivo']>0){
        $query=$query." AND ofertas_alumno.Id_mot_baja=".$_GET['Id_motivo'];
    }
    if($_GET['Id_oferta']>0){
        $query=$query." AND Id_ofe=".$_GET['Id_oferta'];
    }
    if($_GET['Id_esp']>0){
        $query=$query." AND Id_esp=".$_GET['Id_esp'];
    }
    if($_GET['Id_ori']>0){
        $query=$query." AND Id_ori=".$_GET['Id_ori'];
    }

    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoMediosEnterar= new DaoMediosEnterar();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoMaterias= new DaoMaterias();
    $DaoMotivosBaja= new DaoMotivosBaja();
    $count=1;
    foreach ($DaoAlumnos->getAlumnosBajas($query,null,null) as $k=>$v){
           $nombre_ori="";
           $oferta = $DaoOfertas->show($v['Id_ofe']);
           $esp = $DaoEspecialidades->show($v['Id_esp']);
           if ($v['Id_ori'] > 0) {
              $ori = $DaoOrientaciones->show($v['Id_ori']);
              $nombre_ori = $ori->getNombre();
            }
            $opcion="Plan por materias"; 
            if($v['Opcion_pago']==2){
              $opcion="Plan completo";  
            }
             $grado = $DaoOfertasAlumno->getLastGradoOferta($v['Id_ofe_alum']);
            $count++;
            $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
            $objPHPExcel->getActiveSheet()->setCellValue("B$count", $v['Matricula']);
            $objPHPExcel->getActiveSheet()->setCellValue("C$count", $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins']);
            $objPHPExcel->getActiveSheet()->setCellValue("D$count", $oferta->getNombre_oferta());
            $objPHPExcel->getActiveSheet()->setCellValue("E$count", $esp->getNombre_esp());
            $objPHPExcel->getActiveSheet()->setCellValue("F$count", $nombre_ori);
            $objPHPExcel->getActiveSheet()->setCellValue("G$count", $grado['Grado']);
            $objPHPExcel->getActiveSheet()->setCellValue("H$count", $base->formatFecha($v['Baja_ofe']));
            $objPHPExcel->getActiveSheet()->setCellValue("I$count", $v['Nombre']);
            $objPHPExcel->getActiveSheet()->setCellValue("J$count", $v['Comentario_baja']);
      }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=Bajas-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}


if(isset($_POST['action']) && $_POST['action']=="mostrarCicloReactivar"){
    $DaoCiclos = new DaoCiclos();
    ?>
     <div class="box-emergente-form">
        <h1>Reactivar alumno</h1>
        <p>Ciclo<br>
        <select id="ciclo">
          <option value="0"></option>
          <?php
          foreach($DaoCiclos->getCiclosFuturos() as $k=>$v){
          ?>
              <option value="<?php echo $v->getId() ?>"> <?php echo $v->getClave() ?> </option>
          <?php
          }
          ?>
        </select>
      </p>
      <p><input type="checkbox"  id="generarMensualiadades" checked="checked"/> Generar mensualidades</p>
      <!--<p><input type="checkbox" id="asignarMaterias" checked="checked"/> Asignar materias</p>-->
      <button onclick="reactivarOferta(<?php echo $_POST['Id_ofe_alum'];?>)">Reactivar</button>
      <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>   
    <?php
}

if(isset($_POST['action']) && $_POST['action']=="reactivarOferta"){
    //Pueden reactivar a un alumno en el mismo ciclo dado de baja o en uno diferente
    $base= new base();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $DaoOfertas= new DaoOfertas();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoCiclos= new DaoCiclos();  
    $DaoGrados= new DaoGrados();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $params=$DaoParametrosPlantel->getParametrosPlantelArray();
    
    //Obtenemos los datos del ciclo
    $cicloNuevo=$DaoCiclos->show($_POST['Id_ciclo']);
    $fecha_pago = $cicloNuevo->getFecha_ini();
    $anio=date("Y",strtotime($cicloNuevo->getFecha_ini()));
    $mes=date("m",strtotime($cicloNuevo->getFecha_ini()));
        
    $Id_ciclo_nuevo=$cicloNuevo->getId();
    
    $ofeAlum=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $ofe=$DaoOfertas->show($ofeAlum->getId_ofe());
    //Si la oferta es por ciclos
    if($ofe->getTipoOferta()==1){
        $esp = $DaoEspecialidades->show($ofeAlum->getId_esp());

        $ofeAlum->setActivo(1);
        $ofeAlum->setBaja_ofe(NULL);
        $ofeAlum->setId_mot_baja(NULL);
        $ofeAlum->setComentario_baja(NULL);
        $DaoOfertasAlumno->update($ofeAlum);

        //Ver si no tiene mensualidades generadas en el ciclo de reactivacion
        //Porque puede que lo reactiven en el mismo ciclo
        $cicloOfertaAlumno=$DaoCiclosAlumno->getCicloOferta($ofeAlum->getId(), $Id_ciclo_nuevo);

        //si no existe un ciclo del alumno en Id_ciclo y Id_ofe_alum
        //entonces insertamos las mensualidades

        if($_POST['generarMensualiadades']==1 && $cicloOfertaAlumno->getId()==null){

            //Tomar el grado en que se quedo el alumno en el ciclo que se dio de baja  
            $UltimoCicloAlumno=$DaoCiclosAlumno->getLastCicloOferta($ofeAlum->getId());
            $grado = $DaoGrados->show($UltimoCicloAlumno->getId_grado());
            $Id_grado_ofe = $grado->getId();
            $SiguienteGrado = $grado->getGrado();

            //Verificamos si existe el nuevo grado
            $query = "SELECT * FROM grados_ulm WHERE Id_esp=" . $ofeAlum->getId_esp() . " AND Grado=" . $SiguienteGrado;
            foreach ($DaoGrados->advanced_query($query) as $gradoOferta) {
                $Id_grado_ofe = $gradoOferta->getId();
            }

            $CiclosAlumno = new CiclosAlumno();
            $CiclosAlumno->setId_ciclo($Id_ciclo_nuevo);
            $CiclosAlumno->setId_grado($Id_grado_ofe);
            $CiclosAlumno->setId_ofe_alum($ofeAlum->getId());
            $Id_ciclo_alum = $DaoCiclosAlumno->add($CiclosAlumno);

            $ciclos=$base->getCiclosTipoInscripcion($esp->getTipo_insc(), $esp->getTipo_ciclos());

            $grado_actual = $SiguienteGrado;
            $modulo = $grado_actual % $ciclos;

            if (($esp->getTipo_insc() == 2 && (($esp->getTipo_ciclos() == 1 || $esp->getTipo_ciclos() == 4) && $modulo == 0) || (($esp->getTipo_ciclos() == 2 || $esp->getTipo_ciclos() == 3) && $modulo == 1)) || ($esp->getTipo_insc() == 1 && $modulo == 1) || ($esp->getTipo_insc() == 3 && $grado_actual == 1)) {
                //Insertamos el cargo de la inscripcion
                $PagosCiclo = new PagosCiclo();
                $PagosCiclo->setConcepto("Inscripción");
                $PagosCiclo->setFecha_pago($fecha_pago);
                $PagosCiclo->setMensualidad($esp->getInscripcion_curso());
                $PagosCiclo->setId_ciclo_alum($Id_ciclo_alum);
                $PagosCiclo->setId_alum($ofeAlum->getId_alum());
                $PagosCiclo->setTipo_pago("pago");
                $DaoPagosCiclo->add($PagosCiclo);
            }
          //Si el tipo de pago es plan completo
          if ($ofeAlum->getOpcionPago() == 2) {
            //Insertamos las mensualidades
            for ($i = 1; $i <= $esp->getNum_pagos(); $i++) {
                //Se generan los cargos apartir del mes de inicio del ciclo
                //en el dia que se dio de alta en parametros y si no por default el dia 1
                $d=1;
                if(isset($params['DiaInicioMensualidades']) && strlen($params['DiaInicioMensualidades'])>0){
                   $d=$params['DiaInicioMensualidades'];
                }
                $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes + $i, $d, $anio));

                $Precio_curso = ($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos();
                $PagosCiclo = new PagosCiclo();
                $PagosCiclo->setConcepto("Mensualidad");
                $PagosCiclo->setFecha_pago($base->get_day_pago($fecha_pago, $Id_ciclo_nuevo));
                $PagosCiclo->setMensualidad($Precio_curso);
                $PagosCiclo->setId_ciclo_alum($Id_ciclo_alum);
                $PagosCiclo->setId_alum($ofeAlum->getId_alum());
                $PagosCiclo->setTipo_pago("pago");
                $PagosCiclo->setTipoRel("alum");
                $PagosCiclo->setIdRel($ofeAlum->getId_alum());
                $DaoPagosCiclo->add($PagosCiclo);
            }
          }
        }

        $alum=$DaoAlumnos->show($ofeAlum->getId_alum());


        //Crear Notificacion 
        $TextoHistorial="Reactivo al alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ".$ofe->getNombre_oferta().", ".$esp->getNombre_esp();
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Reporte de bajas");
    }
}



if(isset($_POST['action']) && $_POST['action']=="mostrar_box_masivo"){
    $DaoCiclos = new DaoCiclos();
    ?>
     <div class="box-emergente-form">
        <h1>Reactivar alumno</h1>
        <p>Ciclo<br>
        <select id="ciclo">
          <option value="0"></option>
          <?php
          foreach($DaoCiclos->getCiclosFuturos() as $k=>$v){
          ?>
              <option value="<?php echo $v->getId() ?>"> <?php echo $v->getClave() ?> </option>
          <?php
          }
          ?>
        </select>
      </p>
      <p><input type="checkbox"  id="generarMensualiadades" checked="checked"/> Generar mensualidades</p>
      <button onclick="confirmarReactivacion()">Reactivar</button>
      <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>   
    <?php
}

function alumnosBajas(){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $count=0;
   foreach ($DaoAlumnos->getAlumnosBajas() as $k=>$v){
      $nombre_ori="";
      $oferta = $DaoOfertas->show($v['Id_ofe']);
      $esp = $DaoEspecialidades->show($v['Id_esp']);
      if ($v['Id_ori'] > 0) {
         $ori = $DaoOrientaciones->show($v['Id_ori']);
         $nombre_ori = $ori->getNombre();
       }
       $opcion="Plan por materias"; 
       if($v['Opcion_pago']==2){
         $opcion="Plan completo";  
       }
     ?>
       <tr id_alum="<?php echo $v['Id_ins'];?>" num-tr="<?php echo $count;?>" id-ofe-alum="<?php echo $v['Id_ofe_alum']?>">
           <td><?php echo $v['Matricula'] ?></td>
           <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
           <td><?php echo $oferta->getNombre_oferta(); ?></td>
           <td><?php echo $esp->getNombre_esp(); ?></td>
           <td><?php echo $nombre_ori; ?></td>
           <td><?php echo $base->formatFecha($v['Baja_ofe']) ?></td>
           <td><?php echo $v['Nombre']; ?></td>
           <td><?php echo $v['Comentario_baja']; ?></td>
           <td class="td-center"><input type="checkbox"> </td>
           <td class="td-center">
                <a href="solicitud_baja.php?id_ofe_alum=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button><i class="fa fa-file-o"></i> Solicitud</button></a>
                <button onclick="mostrarCicloReactivar(<?php echo $v['Id_ofe_alum'] ?>)">Reactivar</button>
                <?php
                if($oferta->getTipoOferta()==1){
                    ?>
                     <a href="pago.php?id=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button>Cobranza</button></a>
                    <?php
                }elseif($oferta->getTipoOferta()==2){
                    ?>
                     <a href="cargos_curso.php?id=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button>Cobranza</button></a>
                    <?php   
                }
                ?>
           </td>
         </tr>
         <?php
         $count++;
   }   
}

if(isset($_POST['action']) && $_POST['action']=="getAlumnosBajas"){
    alumnosBajas();
}





if (isset($_POST['action']) && $_POST['action'] == "mostrarReactivarSinCiclo") {
    $DaoTurnos= new DaoTurnos();
?>
      <div class="box-emergente-form">
        <h1>Reactivar alumno</h1>       
        <p>Turno<br>
            <select id="turno" onchange="getCursosEspecialidad(<?php echo $_POST['Id_ofe_alum'];?>)">
                <option value="0"></option>
                <?php
                foreach($DaoTurnos->getTurnos() as $turno){
                    ?>
                    <option value="<?php echo $turno->getId()?>"><?php echo $turno->getNombre()?></option>
                <?php
                }
                ?>
            </select>
        </p>
        <p>Fecha del curso<br>
            <select id="cursos-especialidad"></select>
        </p>
        <p><input type="checkbox"  id="generarMensualiadades" checked="checked"/> Generar mensualidades</p>
      <p><button onclick="confirmarReactivacionSinCiclo(<?php echo $_POST['Id_ofe_alum'];?>)">Reactivar</button>
         <button onclick = "ocultar_error_layer()">Cancelar</button></p>
</div>
<?php
}

if (isset($_POST['action']) && $_POST['action'] == "getCursosEspecialidad") {
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $ofertaAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    ?>
    <option value="0"></option>
    <?php
    $DaoCursosEspecialidad= new DaoCursosEspecialidad();
    foreach ($DaoCursosEspecialidad->getCursosEspecialidad($ofertaAlumno->getId_esp(), true,$ofertaAlumno->getTurno()) as $curso) {
        $fechaIni = "Fecha abierta";
        if (strlen($curso->getFechaInicio()) > 0 && strlen($curso->getFechaFin()) > 0) {
            $fechaIni = "Inicio: " . $curso->getFechaInicio() . " - Fin: " . $curso->getFechaFin();
        }
        ?>
        <option value="<?php echo $curso->getId() ?>"><?php echo $fechaIni ?> </option>
        <?php
    }
}

if($_POST['action']=="confirmarReactivacionSinCiclo"){
    //Pueden reactivar a un alumno en el mismo ciclo dado de baja o en uno diferente
    $base= new base();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $DaoOfertas= new DaoOfertas();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoCiclos= new DaoCiclos();  
    $DaoGrados= new DaoGrados();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoCursosEspecialidad= new DaoCursosEspecialidad();
    
    //Obtemos los datos de la oferta del alumno
    $ofeAlum=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $esp = $DaoEspecialidades->show($ofeAlum->getId_esp());
    
    //Reactivamos al alumno
    $ofeAlum->setActivo(1);
    $ofeAlum->setBaja_ofe(NULL);
    $ofeAlum->setId_mot_baja(NULL);
    $ofeAlum->setComentario_baja(NULL);
    $DaoOfertasAlumno->update($ofeAlum);
     
    if($_POST['generarMensualidades']==1){
        //Tomar el grado en que se quedo el alumno en el ciclo que se dio de baja  
        $UltimoCicloAlumno=$DaoCiclosAlumno->getLastCicloOferta($ofeAlum->getId());
        $grado = $DaoGrados->show($UltimoCicloAlumno->getId_grado());
        $Id_grado_ofe = $grado->getId();
        $SiguienteGrado = $grado->getGrado();
        
        //Verificamos si existe el nuevo grado
        $query = "SELECT * FROM grados_ulm WHERE Id_esp=" . $ofeAlum->getId_esp() . " AND Grado=" . $SiguienteGrado;
        foreach ($DaoGrados->advanced_query($query) as $gradoOferta) {
            $Id_grado_ofe = $gradoOferta->getId();
        }
  
        //Insertamos el nuevo ciclo del alumno
        $CiclosAlumno = new CiclosAlumno();
        $CiclosAlumno->setId_ciclo(0);
        $CiclosAlumno->setId_grado($Id_grado_ofe);
        $CiclosAlumno->setId_ofe_alum($ofeAlum->getId());
        $Id_ciclo_alum = $DaoCiclosAlumno->add($CiclosAlumno);

        //Insertamos primer cargo por inscripcion
        $fecha_pago=$DaoCursosEspecialidad->getFechaPagoInscripcion($ofeAlum->getId_curso_esp());
        $PagosCiclo = new PagosCiclo();
        $PagosCiclo->setConcepto("Inscripción");
        $PagosCiclo->setFecha_pago($fecha_pago);
        $PagosCiclo->setMensualidad($esp->getInscripcion_curso());
        $PagosCiclo->setId_ciclo_alum($Id_ciclo_alum);
        $PagosCiclo->setId_alum($ofeAlum->getId_alum());
        $PagosCiclo->setTipo_pago('pago');
        $PagosCiclo->setTipoRel('alum');
        $PagosCiclo->setIdRel($ofeAlum->getId_alum());
        $DaoPagosCiclo->add($PagosCiclo);

      //Si el tipo de pago es plan completo
      if ($ofeAlum->getOpcionPago() == 2 && $ofeAlum->getId_curso_esp()>0) {
        //Si la oferta es sin ciclos
        //Se generan los pagos apartir de la fecha segun fue configurado el curso
        $fecha_pago=$DaoCursosEspecialidad->getFechaPago($ofeAlum->getId_curso_esp());
        $anio = date("Y", strtotime($fecha_pago));
        $mes = date("m", strtotime($fecha_pago));
        $day= date("d", strtotime($fecha_pago));

        //Generar mensualidades
        for ($i = 1; $i <= $esp->getNum_pagos(); $i++) {
            $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes +$i, $day , $anio));

            $Precio_curso = ($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos();
            $PagosCiclo = new PagosCiclo();
            $PagosCiclo->setConcepto("Mensualidad");
            $PagosCiclo->setFecha_pago($fecha_pago);
            $PagosCiclo->setMensualidad($Precio_curso);
            $PagosCiclo->setId_ciclo_alum($Id_ciclo_alum);
            $PagosCiclo->setId_alum($ofeAlum->getId_alum());
            $PagosCiclo->setTipo_pago("pago");
            $PagosCiclo->setTipoRel("alum");
            $PagosCiclo->setIdRel($ofeAlum->getId_alum());
            $DaoPagosCiclo->add($PagosCiclo);
        }
      }
    }

    //Obteneomos los datos del alumno
    $alum=$DaoAlumnos->show($ofeAlum->getId_alum());
    $ofe=$DaoOfertas->show($ofeAlum->getId_ofe());
    
    //Crear Notificacion 
    $TextoHistorial="Reactivo al alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ".$ofe->getNombre_oferta().", ".$esp->getNombre_esp();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Reporte de bajas");
}
