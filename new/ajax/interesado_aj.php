<?php
require_once('activate_error.php');
require_once('../require_daos.php');

if (isset($_POST['action']) && $_POST['action'] == "save_inte") {
    $DaoUsuarios = new DaoUsuarios();
    $DaoDirecciones = new DaoDirecciones();
    $DaoAlumnos = new DaoAlumnos();
    $DaoLogsInscripciones = new DaoLogsInscripciones();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoCiclos = new DaoCiclos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoGrados = new DaoGrados();
    $DaoOfertas= new DaoOfertas();
    $DaoCursosEspecialidad= new DaoCursosEspecialidad();
    $DaoTutores= new DaoTutores();

    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $resp = array();

    if (isset($_POST['Id_alumn']) && $_POST['Id_alumn'] > 0) {
        $alum = $DaoAlumnos->show($_POST['Id_alumn']);
        $alum->setNombre($_POST['nombre_int']);
        $alum->setApellidoP($_POST['apellidoP_int']);
        $alum->setApellidoM($_POST['apellidoM_int']);
        $alum->setEmail($_POST['email_int']);
        $alum->setCel($_POST['cel_int']);
        $alum->setId_medio_ent($_POST['medio']);
        $alum->setSeguimiento_ins($_POST['prioridad']);
        $alum->setMedioContacto($_POST['medio_contacto']);
        $alum->setComentarios($_POST['comentarios']);
        $DaoAlumnos->update($alum);

        $resp['Id'] = $_POST['Id_alumn'];
        $Id_int = $_POST['Id_alumn'];

        //Captura de historial
        $TextoHistorial = "Actualiza los datos del interesado " . $_POST['nombre_int'] . " " . $_POST['apellidoP_int'] . " " . $_POST['apellidoM_int'];
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Interesados");
    } else {

        //tipo 0=interesado, 1= alumno, 2=egresado
        $alum = new Alumnos();
        $alum->setNombre($_POST['nombre_int']);
        $alum->setApellidoP($_POST['apellidoP_int']);
        $alum->setApellidoM($_POST['apellidoM_int']);
        $alum->setEmail($_POST['email_int']);
        $alum->setCel($_POST['cel_int']);
        $alum->setId_medio_ent($_POST['medio']);
        $alum->setSeguimiento_ins($_POST['prioridad']);
        $alum->setMedioContacto($_POST['medio_contacto']);
        $alum->setComentarios($_POST['comentarios']);
        $alum->setFecha_ins(date('Y-m-d'));
        $alum->setTipo(0);
        $alum->setRefencia_pago(123244354565665767);
        $alum->setId_plantel($usu->getId_plantel());
        $alum->setId_usu_capturo($usu->getId());
        $id = $DaoAlumnos->add($alum);

        $resp['Id'] = $id;
        $Id_int = $id;

        //Captura de historial
        $TextoHistorial = "Captura del nuevo interesado " . $_POST['nombre_int'] . " " . $_POST['apellidoP_int'] . " " . $_POST['apellidoM_int'];
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Interesados");
    }

    $id_dir = 0;
    if (isset($_POST['Id_dir']) && $_POST['Id_dir'] > 0) {
        $Direcciones = $DaoDirecciones->show($_POST['Id_dir']);
        $Direcciones->setCiudad_dir($_POST['ciudad_int']);
        $DaoDirecciones->update($Direcciones);
        $id_dir = $_POST['Id_dir'];
    } else {
        if (strlen($_POST['ciudad_int']) > 0) {
            $Direcciones = new Direcciones();
            $Direcciones->setCalle_dir("---");
            $Direcciones->setNumExt_dir("---");
            $Direcciones->setCiudad_dir($_POST['ciudad_int']);
            $Direcciones->setIdRel_dir($Id_int);
            $Direcciones->setTipoRel_dir('alum');
            $id_dir = $DaoDirecciones->add($Direcciones);
        }
    }
    if ($id_dir > 0) {
        $a = $DaoAlumnos->show($Id_int);
        $a->setId_dir($id_dir);
        $DaoAlumnos->update($a);
    }

    //Ofertas del interesado
    if (isset($_POST['ofertas']) && count($_POST['ofertas']) > 0) {
        foreach ($_POST['ofertas'] as $k => $v) { 

            $oferta=$DaoOfertas->show($v['Id_oferta']);
            $esp = $DaoEspecialidades->show($v['Id_esp']);
            $grado = $DaoGrados->show($v['Id_grado']);

            //Si ya existe una oferta actualizamos
            if (isset($v['Id_ofe_alum']) && $v['Id_ofe_alum'] > 0) {

                $ofertaAlumno = $DaoOfertasAlumno->show($v['Id_ofe_alum']);
                $ofertaAlumno->setId_ofe($v['Id_oferta']);
                $ofertaAlumno->setId_esp($v['Id_esp']);
                $ofertaAlumno->setOpcionPago($v['opcion']);
                $ofertaAlumno->setTurno($v['Turno']);
                if(isset($v['Id_ori']) && $v['Id_ori']>0){
                  $ofertaAlumno->setId_ori($v['Id_ori']);
                }
                if(isset($v['Id_curso_esp']) && $v['Id_curso_esp']>0){
                  $ofertaAlumno->setId_curso_esp($v['Id_curso_esp']);
                }
                $DaoOfertasAlumno->update($ofertaAlumno);

                //1= por ciclos, 2= sin ciclos
                if($oferta->getTipoOferta()==1){
                    $ciclo = $DaoCiclos->show($v['Id_ciclo']);
                    $fecha_pago = $ciclo->getFecha_ini();
                }else{
                    $fecha_pago=$DaoCursosEspecialidad->getFechaPagoInscripcion($v['Id_curso_esp']);
                }
                
                //Actualizar primer ciclo del alumno
                $CiclosAlumno = $DaoCiclosAlumno->show($v['Id_ciclo_alum']);
                $CiclosAlumno->setId_grado($v['Id_grado']);
                if(isset($v['Id_ciclo']) && $v['Id_ciclo']>0){
                       $CiclosAlumno->setId_ciclo($v['Id_ciclo']);
                }else{
                       $CiclosAlumno->setId_ciclo(0);
                }
                $DaoCiclosAlumno->update($CiclosAlumno);

                //Actualizar el pago de inscripcion
                $PagosCiclo = $DaoPagosCiclo->show($v['Id_pago_ciclo']);
                $PagosCiclo->setFecha_pago($fecha_pago);
                $PagosCiclo->setMensualidad($esp->getInscripcion_curso());
                $DaoPagosCiclo->update($PagosCiclo);  

            } else {

                //Validamos si ya existe la oferta y si no entonces insertamos la nueva
                $query = "SELECT * FROM ofertas_alumno WHERE Id_ofe=" . $v['Id_oferta'] . " AND Id_alum= " . $Id_int . " AND Id_esp=" . $v['Id_esp'];
                if ((isset($v['Id_ori']) && $v['Id_ori'] > 0) && isset($v['Id_curso_esp']) && $v['Id_curso_esp'] > 0) {
                    $query = "SELECT * FROM ofertas_alumno WHERE Id_ofe=" . $v['Id_oferta'] . " AND Id_alum= " . $Id_int . " AND Id_esp=" . $v['Id_esp'] . " AND Id_ori=" . $v['Id_ori']." AND Id_curso_esp=" . $v['Id_curso_esp'];
                }elseif (isset($v['Id_ori']) && $v['Id_ori'] > 0) {
                    $query = "SELECT * FROM ofertas_alumno WHERE Id_ofe=" . $v['Id_oferta'] . " AND Id_alum= " . $Id_int . " AND Id_esp=" . $v['Id_esp'] . " AND Id_ori=" . $v['Id_ori'];
                }elseif(isset($v['Id_curso_esp']) && $v['Id_curso_esp'] > 0){
                    $query = "SELECT * FROM ofertas_alumno WHERE Id_ofe=" . $v['Id_oferta'] . " AND Id_alum= " . $Id_int . " AND Id_esp=" . $v['Id_esp'] . " AND Id_curso_esp=" . $v['Id_curso_esp'];
                }
                $existeOferta = $DaoOfertasAlumno->existeQuery($query);
                if ($existeOferta->getId() == 0) {

                    //Insertamos la oferta del alumno
                    $OfertasAlumno = new OfertasAlumno();
                    $OfertasAlumno->setId_ofe($v['Id_oferta']);
                    $OfertasAlumno->setId_alum($Id_int);
                    $OfertasAlumno->setId_esp($v['Id_esp']);
                    $OfertasAlumno->setOpcionPago($v['opcion']);
                    $OfertasAlumno->setActivo(1);
                    $OfertasAlumno->setTurno($v['Turno']);
                    $OfertasAlumno->setAlta_ofe(date('Y-m-d'));
                    if(isset($v['Id_ori']) && $v['Id_ori']>0){
                      $OfertasAlumno->setId_ori($v['Id_ori']);
                    }
                    if(isset($v['Id_curso_esp']) && $v['Id_curso_esp']>0){
                      $OfertasAlumno->setId_curso_esp($v['Id_curso_esp']);
                    }
                    $id_oferta_alumno = $DaoOfertasAlumno->add($OfertasAlumno);

                    //1= por ciclos, 2= sin ciclos
                    if($oferta->getTipoOferta()==1){
                        $ciclo = $DaoCiclos->show($v['Id_ciclo']);
                        $fecha_pago = $ciclo->getFecha_ini();
                    }else{
                        $fecha_pago=$DaoCursosEspecialidad->getFechaPagoInscripcion($v['Id_curso_esp']);
                    }

                    //Insertamos el primer ciclo del alumno
                    $CiclosAlumno = new CiclosAlumno();
                    $CiclosAlumno->setId_grado($v['Id_grado']);
                    $CiclosAlumno->setId_ofe_alum($id_oferta_alumno);
                    if(isset($v['Id_ciclo']) && $v['Id_ciclo']>0){
                       $CiclosAlumno->setId_ciclo($v['Id_ciclo']);
                    }else{
                       $CiclosAlumno->setId_ciclo(0);
                     }
                    $id_ciclo_alumno = $DaoCiclosAlumno->add($CiclosAlumno);

                    //Insertamos primer cargo por inscripcion
                    $PagosCiclo = new PagosCiclo();
                    $PagosCiclo->setConcepto("Inscripción");
                    $PagosCiclo->setFecha_pago($fecha_pago);
                    $PagosCiclo->setMensualidad($esp->getInscripcion_curso());
                    $PagosCiclo->setId_ciclo_alum($id_ciclo_alumno);
                    $PagosCiclo->setId_alum($Id_int);
                    $PagosCiclo->setTipo_pago('pago');
                    $PagosCiclo->setTipoRel('alum');
                    $PagosCiclo->setIdRel($Id_int);
                    $DaoPagosCiclo->add($PagosCiclo);
                        
                    
                }
            }
        }
    }
    
    //Contactar el dia
    if (strlen($_POST['contactar']) > 0) {
        $existeDia = $DaoLogsInscripciones->getLogIntDia($Id_int, $_POST['contactar']);
        if ($existeDia->getId() == 0) {
            $LogsInscripciones = new LogsInscripciones();
            $LogsInscripciones->setId_usu($_COOKIE['admin/Id_usu']);
            $LogsInscripciones->setFecha_log(date('Y-m-d H:i:s'));
            $LogsInscripciones->setComen_log("Contactar");
            $LogsInscripciones->setIdrel_log($Id_int);
            $LogsInscripciones->setTipo_relLog('inte');
            $LogsInscripciones->setDia_contactar($_POST['contactar']);
            $DaoLogsInscripciones->add($LogsInscripciones);
        }
    }
    
    //Tutores
    if (isset($_POST['tutores']) && count($_POST['tutores']) > 0) {
        foreach($_POST['tutores'] as $tutor){
             if($tutor['id']>0){
                 $Tutores= $DaoTutores->show($tutor['id']);
                 $Tutores->setParentesco($tutor['parantesco']);
                 $Tutores->setNombre($tutor['nombre']);
                 $Tutores->setApellidoP($tutor['apellidoP']);
                 $Tutores->setApellidoM($tutor['apellidoM']);
                 $Tutores->setTel($tutor['tel']);
                 $Tutores->setCel($tutor['cel']);
                 $Tutores->setEmail($tutor['emal']);
                 $DaoTutores->update($Tutores);  
                 $Id_tutor=$tutor['id'];
                 addDireccionTutor($tutor,$Id_tutor);
                 
             }else{
                 $Tutores= new Tutores();
                 $Tutores->setParentesco($tutor['parantesco']);
                 $Tutores->setNombre($tutor['nombre']);
                 $Tutores->setApellidoP($tutor['apellidoP']);
                 $Tutores->setApellidoM($tutor['apellidoM']);
                 $Tutores->setTel($tutor['tel']);
                 $Tutores->setCel($tutor['cel']);
                 $Tutores->setEmail($tutor['emal']);
                 $Tutores->setId_ins($Id_int);
                 $Id_tutor=$DaoTutores->add($Tutores);
                 
                 addDireccionTutor($tutor,$Id_tutor);
             }
        }
    }
    
    
    echo json_encode($resp);
}

function addDireccionTutor($tutor,$Id_tutor){
   $DaoDirecciones= new DaoDirecciones();
    if($tutor['id_dir']>0){
        $Direcciones= $DaoDirecciones->show($tutor['id_dir']);
        $Direcciones->setCalle_dir($tutor['calle']);
        $Direcciones->setNumExt_dir($tutor['numExt']);
        $Direcciones->setNumInt_dir($tutor['numInt']);
        $Direcciones->setColonia_dir($tutor['colonia']);
        $Direcciones->setCiudad_dir($tutor['ciudad']);
        $Direcciones->setEstado_dir($tutor['estado']);
        $Direcciones->setCp_dir($tutor['cp']);
        $DaoDirecciones->update($Direcciones);
    }else{
        $Direcciones= new Direcciones();
        $Direcciones->setCalle_dir($tutor['calle']);
        $Direcciones->setNumExt_dir($tutor['numExt']);
        $Direcciones->setNumInt_dir($tutor['numInt']);
        $Direcciones->setColonia_dir($tutor['colonia']);
        $Direcciones->setCiudad_dir($tutor['ciudad']);
        $Direcciones->setEstado_dir($tutor['estado']);
        $Direcciones->setCp_dir($tutor['cp']);
        $Direcciones->setIdRel_dir($Id_tutor);
        $Direcciones->setTipoRel_dir("tutor");
        if(strlen($tutor['calle'])>0 && strlen($tutor['numExt'])>0 && strlen($tutor['ciudad'])>0){
           $DaoDirecciones->add($Direcciones);  
        }
    }
}

function delete_ofe_alum($Id_ofe_alum, $Id_alumn) {
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoRecargos = new DaoRecargos();
    $DaoPagos = new DaoPagos();
    $DaoArchivosAlumno = new DaoArchivosAlumno();

    //Obtenemos los ciclos de laoferta delalumno
    foreach ($DaoCiclosAlumno->getCiclosOferta($Id_ofe_alum) as $cicloAlumno) {
        //Obtenemos los pagos delciclo
        $query = "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=" . $cicloAlumno->getId();
        foreach ($DaoPagosCiclo->advanced_query($query) as $pagoCiclo) {
            //Elliminamos los recargos del pago
            $DaoRecargos->deleteRecargosPago($pagoCiclo->getId());
            //Elliminamos los pagos_pagos
            $DaoPagos->deletePagosCargo($pagoCiclo->getId());
        }
        $DaoPagosCiclo->deleteTrue($cicloAlumno->getId());
    }
    //Eliminamos los ciclos de laoferta del alumno
    $DaoCiclosAlumno->deleteCiclosAlumnoByIdOfe($Id_ofe_alum);

    //Eliminamos la oferta del alumno 
    $DaoOfertasAlumno->deleteTrue($Id_ofe_alum);

    //Eliminamos los archivos delalumno
    $DaoArchivosAlumno->deleteFilesByOfertaAlumno($Id_ofe_alum, $Id_alumn);
    update_page($Id_alumn);
}

function update_page($Id) {
    $DaoMediosEnterar = new DaoMediosEnterar();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoCiclos = new DaoCiclos();
    $DaoGrados = new DaoGrados();
    $DaoAlumnos = new DaoAlumnos();
    $DaoDirecciones = new DaoDirecciones();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoMediosContacto = new DaoMediosContacto();
    $DaoLogsInscripciones = new DaoLogsInscripciones();
    $DaoUsuarios = new DaoUsuarios();
    $DaoCursosEspecialidad= new DaoCursosEspecialidad();
    $DaoTurnos= new DaoTurnos();
    $DaoTutores= new DaoTutores();
    $perm = array();
    $_usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    foreach ($DaoUsuarios->getPermisosUsuario($_usu->getId()) as $k => $v) {
        $perm[$v['Id_per']] = 1;
    }

    $titulo = "Nuevo interesado";
    $id_dir = 0;
    $id = 0;
    $ciudad = "";
    $email = "";
    $nombre = "";
    $apellidoP = "";
    $apellidoM = "";
    $edad = "";
    $tel = "";
    $cel = "";
    $comentarios = "";
    $id_med_ent = "";
    $seguimiento = "";
    $id_medio_contacto = "";

    if (isset($Id) && $Id > 0) {
        $id = $Id;
        $int = $DaoAlumnos->show($id);
        $titulo = $int->getNombre() . " " . $int->getApellidoP() . " " . $int->getApellidoM();
        $email = $int->getEmail();
        $nombre = $int->getNombre();
        $apellidoP = $int->getApellidoP();
        $apellidoM = $int->getApellidoM();
        $edad = $int->getEdad();
        $tel = $int->getTel();
        $cel = $int->getCel();
        $comentarios = $int->getComentarios();
        $id_med_ent = $int->getId_medio_ent();
        $seguimiento = $int->getSeguimiento_ins();
        $id_medio_contacto = $int->getMedioContacto();

        if ($int->getId_dir() > 0) {
            $dir = $DaoDirecciones->show($int->getId_dir());
            $id_dir = $dir->getId();
            $ciudad = $dir->getCiudad_dir();
        }

        $log = $DaoLogsInscripciones->getLogInt($id);
        if ($log->getId() > 0) {
            $diaContactar = $log->getDia_contactar();
        } else {
            $diaContactar = $semana = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d') + 7, date('Y')));
        }
    } else {
        $diaContactar = $semana = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d') + 7, date('Y')));
    }
    ?>
    <table id="tabla">
        <tr>
            <td id="column_one">
                <div class="fondo">
                    <div id="box_top">
                        <h1><i class="fa fa-user" aria-hidden="true"></i> <?php echo ucwords(strtolower($titulo)); ?></h1>
                    </div>
                    <div class="seccion">
                        <h2>Datos Personales</h2>
                        <span class="linea"></span>
                        <ul class="form">
                            <li><span class="requerido">*</span>Email<br><input type="email" id="email_int" value="<?php echo $email ?>" onkeyup="buscar_int()"/>
                                <ul id="buscador_int"></ul>
                            </li>
                            <li><span class="requerido">*</span>Nombre<br><input type="text" id="nombre_int" value="<?php echo $nombre ?>"/></li>
                            <li><span class="requerido">*</span>Apellido Paterno<br><input type="text" id="apellidoP_int" value="<?php echo $apellidoP ?>"/></li>
                            <li>Apellido Materno<br><input type="text" id="apellidoM_int" value="<?php echo $apellidoM ?>"/></li>
                            <li>Tel&eacute;fono Movil<br><input type="tel" id="cel_int" value="<?php echo $cel ?>"/></li>
                            <li>Medio de Contacto<br>
                                <select id="medio_contacto">
                                    <option value="0"></option>
                                    <?php
                                    foreach ($DaoMediosContacto->getMedios() as $medio) {
                                        ?>
                                        <option value="<?php echo $medio->getId() ?>" <?php if ($id_medio_contacto == $medio->getId()) { ?> selected="selected"<?php } ?>><?php echo $medio->getNombre_medio() ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </li>
                            <?php
                            if (strlen($ciudad) > 0) {
                                $ciudad = 'value="' . $ciudad . '"';
                            }
                            ?>
                            <li>Ciudad<br><input type="tel" id="ciudad_int" <?php echo $ciudad; ?> onkeyup="buscar_ciudad()"/>
                                <ul id="buscador_ciudad"></ul>
                            </li>
                            <li>Contactar el d&iacute;a<br>
                                <input type="date" value="<?php echo $diaContactar; ?>" id="contactar"/>
                            </li>
                            <li>Comentarios:<br><textarea id="comentarios"><?php echo $comentarios ?></textarea></li>
                        </ul>
                    </div>
                    <div class="seccion">
                        <h2>Datos de inter&eacute;s</h2>
                        <span class="linea"></span>
                        <ul class="form">
                            <li><span class="requerido">*</span>Medio por el que se enteró<br>
                                <select id="medio">
                                    <option value="0"></option>
                                    <?php
                                    foreach ($DaoMediosEnterar->showAll() as $k2 => $v2) {
                                        ?>
                                        <option value="<?php echo $v2->getId() ?>" <?php if ($id_med_ent == $v2->getId()) { ?> selected="selected"<?php } ?>><?php echo $v2->getMedio() ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </li>
                            <li><span class="requerido">*</span>Prioridad<br>
                                <select id="prioridad">
                                    <option value="0"></option>
                                    <option value="1" <?php if ($seguimiento == 1) { ?> selected="selected"<?php } ?>>Alta</option>
                                    <option value="2" <?php if ($seguimiento == 2) { ?> selected="selected"<?php } ?>>Media</option>
                                    <option value="3" <?php if ($seguimiento == 3) { ?> selected="selected"<?php } ?>>Baja</option>
                                    <option value="4" <?php if ($seguimiento == 4) { ?> selected="selected"<?php } ?>>Nula</option>
                                </select>
                            </li>
                        </ul>
                    </div>
                    <div class="seccion">
                        <h2>Datos del padre o tutor</h2>
                        <span class="linea"></span>
                        <div id="list_tutores">
                            <?php
                            $countTu=0;
                            if (isset($id) && $id > 0) {
                                foreach($DaoTutores->getTutoresAlumno($id) as $tutor){
                                    $id_dir=0;
                                    $dir=$DaoDirecciones->getDireccionPorTipoRel($tutor->getId(),'tutor');
                                    if($dir->getId()>0){
                                      $id_dir= $dir->getId(); 
                                    }
                                ?>
                                    <ul class="form" id-tutor="<?php echo $tutor->getId();?>" id-dir="<?php echo $id_dir?>">
                                        <li>Parentesco<br><input type="text" class="parentesco-tutor" value="<?php echo $tutor->getParentesco();?>"/></li>
                                        <li>Nombre<br><input type="text" class="nombre-tutor" value="<?php echo $tutor->getNombre();?>"/></li>
                                        <li>Apellido paterno<br><input type="text" class="apellidoP-tutor" value="<?php echo $tutor->getApellidoP();?>"/></li>
                                        <li>Apellido materno<br><input type="text" class="apellidoM-tutor" value="<?php echo $tutor->getApellidoM();?>"/></li>
                                        <li>Correo electrónico<br><input type="text" class="email-tutor" value="<?php echo $tutor->getEmail();?>"/></li>
                                        <li>Teléfono<br><input type="text" class="tel-tutor" value="<?php echo $tutor->getTel();?>"/></li>
                                        <li>Celular<br><input type="text" class="cel-tutor" value="<?php echo $tutor->getCel();?>"/></li>
                                        <li>Calle<br><input type="text" class="calle-tutor" value="<?php echo $dir->getCalle_dir();?>"/></li>
                                        <li>Núm ext.<br><input type="text" class="numext-tutor" value="<?php echo $dir->getNumExt_dir();?>"/></li>
                                        <li>Núm int.<br><input type="text" class="numint-tutor" value="<?php echo $dir->getNumInt_dir();?>"/></li>
                                        <li>Colonia<br><input type="text" class="colonia-tutor" value="<?php echo $dir->getColonia_dir();?>"/></li>
                                        <li>Ciudad<br><input type="text" class="ciudad-tutor" value="<?php echo $dir->getCiudad_dir();?>"/></li>
                                        <li>Estado<br><input type="text" class="estado-tutor" value="<?php echo $dir->getEstado_dir();?>"/></li>
                                        <li>Código postal<br><input type="text" class="cp-tutor" value="<?php echo $dir->getCp_dir();?>"/></li>
                                        <li><button type="button" class="btns btns-primary btns-sm delete-tutor" onclick="delete_tutor(<?php echo $tutor->getId() ?>,<?php echo $id_dir?>)">Eliminar tutor</button></li>
                                    </ul>
                                <?php
                                $countTu++;
                                }
                            }
                            if($countTu==0){
                            ?>
                                <ul class="form" id-tutor="0" id-dir="0">
                                    <li>Parentesco<br><input type="text" class="parentesco-tutor"/></li>
                                    <li>Nombre<br><input type="text" class="nombre-tutor"/></li>
                                    <li>Apellido paterno<br><input type="text" class="apellidoP-tutor"/></li>
                                    <li>Apellido materno<br><input type="text" class="apellidoM-tutor"/></li>
                                    <li>Correo electrónico<br><input type="text" class="email-tutor"/></li>
                                    <li>Teléfono<br><input type="text" class="tel-tutor"/></li>
                                    <li>Celular<br><input type="text" class="cel-tutor"/></li>
                                    <li>Calle<br><input type="text" class="calle-tutor"/></li>
                                    <li>Núm ext.<br><input type="text" class="numext-tutor"/></li>
                                    <li>Núm int.<br><input type="text" class="numint-tutor"/></li>
                                    <li>Colonia<br><input type="text" class="colonia-tutor"/></li>
                                    <li>Ciudad<br><input type="text" class="ciudad-tutor"/></li>
                                    <li>Estado<br><input type="text" class="estado-tutor"/></li>
                                    <li>Código postal<br><input type="text" class="cp-tutor"/></li>
                                </ul>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="seccion">
                        <h2>&Aacute;reas de inter&eacute;s</h2>
                        <span class="linea"></span>
                        <div id="list_ofertas">
                        <?php
                        $count_ofertas = 0;
                        if (isset($id) && $id > 0) {
                            
                            foreach ($DaoOfertasAlumno->getOfertasAlumno($id) as $ofeAlum) {
                                $of = $DaoOfertas->show($ofeAlum->getId_ofe());

                                //1= por ciclos, 2= sin ciclos
                                if ($of->getTipoOferta() == 1) {
                                    $primerCiclo = $DaoCiclosAlumno->getPrimerCicloOferta($ofeAlum->getId());
                                    $pago = $DaoPagosCiclo->getPagoIdCicloAlum($primerCiclo->getId());
                                    ?>
                                    <ul class="form" Id_ofe_alum="<?php echo $ofeAlum->getId() ?>" Id_ciclo_alum="<?php echo $primerCiclo->getId() ?>" Id_pago_ciclo="<?php echo $pago->getId() ?>">
                                        <li><span class="requerido">*</span>Oferta de inter&eacute;s<br>            
                                           <select class="oferta" onchange="getTipociclo(this)">
                                                <option value="0"></option>
                                                <?php
                                                foreach ($DaoOfertas->showAll() as $ofe) {
                                                    ?>
                                                    <option value="<?php echo $ofe->getId() ?>" <?php if ($ofe->getId() == $ofeAlum->getId_ofe()) { ?> selected="selected" <?php } ?>> <?php echo $ofe->getNombre_oferta() ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                        <li><span class="requerido">*</span>Especialidad<br>            
                                            <select class="curso" onchange="getOrientacionesEspecialidad(this)">
                                                <option value="0"></option>
                                                <?php
                                                foreach ($DaoEspecialidades->getEspecialidadesOferta($ofeAlum->getId_ofe()) as $esp) {
                                                    ?>
                                                    <option value="<?php echo $esp->getId() ?>" <?php if ($esp->getId() == $ofeAlum->getId_esp()) { ?> selected="selected" <?php } ?>><?php echo $esp->getNombre_esp() ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                        <li><div class="box_orientacion">
                                                <?php
                                                if (count($DaoOrientaciones->getOrientacionesEspecialidad($ofeAlum->getId_esp())) > 0) {
                                                    ?>
                                                    <span class="requerido">*</span>Orientaci&oacute;n<br>
                                                    <select class="orientacion">
                                                        <option value="0"></option>
                                                        <?php
                                                        foreach ($DaoOrientaciones->getOrientacionesEspecialidad($ofeAlum->getId_esp()) as $ori) {
                                                            ?>
                                                            <option value="<?php echo $ori->getId() ?>" <?php if ($ori->getId() == $ofeAlum->getId_ori()) { ?> selected="selected" <?php } ?>> <?php echo $ori->getNombre() ?> </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <?php
                                                }
                                                ?>
                                            </div></li>
                                        <li><span class="requerido">*</span>Grado<br>            
                                            <select class="grado">
                                                <option value="0"></option>
                                                <?php
                                                foreach ($DaoGrados->getGradosEspecialidad($ofeAlum->getId_esp()) as $grado) {
                                                    ?>
                                                    <option value="<?php echo $grado->getId() ?>" <?php if ($grado->getId() == $primerCiclo->getId_grado()) { ?> selected="selected" <?php } ?>> <?php echo $grado->getGrado() ?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                        <li><span class="requerido">*</span>Ciclo<br>
                                            <select class="ciclo">
                                                <option value="0"></option>
                                                <?php
                                                foreach ($DaoCiclos->showAll() as $cicloFut) {
                                                    ?>
                                                    <option value="<?php echo $cicloFut->getId() ?>" <?php if ($cicloFut->getId() == $primerCiclo->getId_ciclo()) { ?> selected="selected" <?php } ?>><?php echo $cicloFut->getClave() ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                        <li><span class="requerido">*</span>Opci&oacute;n de pago<br>
                                            <select class="opcionPago">
                                                <option value="0"></option>
                                                <option value="1" <?php if ($ofeAlum->getOpcionPago() == 1) { ?> selected="selected" <?php } ?>>Materias</option>
                                                <option value="2" <?php if ($ofeAlum->getOpcionPago() == 2) { ?> selected="selected" <?php } ?>>Plan Completo</option>
                                            </select>
                                        </li>
                                        <li><span class="requerido">*</span>Turno<br>
                                            <select class="turno">
                                                <option value="0"></option>
                                                <?php
                                                foreach($DaoTurnos->getTurnos() as $turno){
                                                    ?>
                                                    <option value="<?php echo $turno->getId()?>" <?php if ($ofeAlum->getTurno() == $turno->getId()) { ?> selected="selected" <?php } ?>><?php echo $turno->getNombre()?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </li><br>
                                        <li><button type="button" class="btns btns-primary btns-sm" onclick="delete_ofe_alum(<?php echo $ofeAlum->getId() ?>)">Eliminar oferta</button></li>
                                    </ul>
                                    <?php
                                } elseif ($of->getTipoOferta() == 2) {
                                    $primerCiclo = $DaoCiclosAlumno->getPrimerCicloOferta($ofeAlum->getId());
                                    $pago = $DaoPagosCiclo->getPagoIdCicloAlum($primerCiclo->getId());
                                    ?>
                                    <ul class="form" Id_ofe_alum="<?php echo $ofeAlum->getId() ?>" Id_ciclo_alum="<?php echo $primerCiclo->getId() ?>" Id_pago_ciclo="<?php echo $pago->getId() ?>">
                                        <li><span class="requerido">*</span>Oferta de inter&eacute;s<br>            
                                            <select class="oferta" onchange="getTipociclo(this)">
                                                <option value="0"></option>
                                                <?php
                                                foreach ($DaoOfertas->showAll() as $ofe) {
                                                    ?>
                                                    <option value="<?php echo $ofe->getId() ?>" <?php if ($ofe->getId() == $ofeAlum->getId_ofe()) { ?> selected="selected" <?php } ?>> <?php echo $ofe->getNombre_oferta() ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                        <li><span class="requerido">*</span>Especialidad<br>            
                                            <select class="curso" onchange="getOrientacionesEspecialidad(this)">
                                                <option value="0"></option>
                                                <?php
                                                foreach ($DaoEspecialidades->getEspecialidadesOferta($ofeAlum->getId_ofe()) as $esp) {
                                                    ?>
                                                    <option value="<?php echo $esp->getId() ?>" <?php if ($esp->getId() == $ofeAlum->getId_esp()) { ?> selected="selected" <?php } ?>><?php echo $esp->getNombre_esp() ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                       <li><span class="requerido">*</span>Turno<br>
                                            <select class="turno" onchange="getCursosEspecialidad(this)">
                                                <option value="0"></option>
                                                <?php
                                                foreach($DaoTurnos->getTurnos() as $turno){
                                                    ?>
                                                    <option value="<?php echo $turno->getId()?>" <?php if ($ofeAlum->getTurno() == $turno->getId()) { ?> selected="selected" <?php } ?>><?php echo $turno->getNombre()?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                        <li>
                                            <span class="requerido">*</span>Fecha del curso<br>
                                            <select class="cursos-especialidad">
                                                    <option value="0"></option>
                                                    <?php
                                                    foreach ($DaoCursosEspecialidad->getCursosEspecialidad($ofeAlum->getId_esp()) as $curso) {
                                                        $fechaIni = "Fecha abierta";
                                                        if (strlen($curso->getFechaInicio()) > 0 && strlen($curso->getFechaFin()) > 0) {
                                                            $fechaIni = "Inicio: " . $curso->getFechaInicio() . " - Fin: " . $curso->getFechaFin();
                                                        }
                                                        ?>
                                                        <option value="<?php echo $curso->getId() ?>" <?php if ($curso->getId() == $ofeAlum->getId_curso_esp()) { ?> selected="selected" <?php } ?>><?php echo $fechaIni ?> </option>
                                                        <?php
                                                    }
                                                    ?>
                                            </select>
                                        </li>
                                        <li>
                                            <div class="box_orientacion">
                                             <?php
                                                if (count($DaoOrientaciones->getOrientacionesEspecialidad($ofeAlum->getId_esp())) > 0) {
                                                    ?>
                                                    <span class="requerido">*</span>Orientaci&oacute;n<br>
                                                    <select class="orientacion">
                                                        <option value="0"></option>
                                                        <?php
                                                        foreach ($DaoOrientaciones->getOrientacionesEspecialidad($ofeAlum->getId_esp()) as $ori) {
                                                            ?>
                                                            <option value="<?php echo $ori->getId() ?>" <?php if ($ori->getId() == $ofeAlum->getId_ori()) { ?> selected="selected" <?php } ?>> <?php echo $ori->getNombre() ?> </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </li>
                                        <li><span class="requerido">*</span>Grado<br>            
                                            <select class="grado">
                                                <option value="0"></option>
                                                <?php
                                                foreach ($DaoGrados->getGradosEspecialidad($ofeAlum->getId_esp()) as $grado) {
                                                    ?>
                                                    <option value="<?php echo $grado->getId() ?>" <?php if ($grado->getId() == $primerCiclo->getId_grado()) { ?> selected="selected" <?php } ?>> <?php echo $grado->getGrado() ?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </li>
                                        <li><span class="requerido">*</span>Opci&oacute;n de pago<br>
                                            <select class="opcionPago">
                                                <option value="0"></option>
                                                <option value="1" <?php if ($ofeAlum->getOpcionPago() == 1) { ?> selected="selected" <?php } ?>>Materias</option>
                                                <option value="2" <?php if ($ofeAlum->getOpcionPago() == 2) { ?> selected="selected" <?php } ?>>Plan Completo</option>
                                            </select>
                                        </li>
                                        <li><button type="button" class="btns btns-primary btns-sm" onclick="delete_ofe_alum(<?php echo $ofeAlum->getId() ?>)">Eliminar oferta</button></li>
                                    </ul>
                                    <?php
                                }
                                $count_ofertas++;
                            }
                        }
                        if($count_ofertas==0){
                         ?>
                           <ul class="form">
                            <li><span class="requerido">*</span>Oferta de inter&eacute;s<br>            
                                <select class="oferta" onchange="getTipociclo(this)">
                                    <option value="0"></option>
                                    <?php
                                    foreach ($DaoOfertas->showAll() as $oferta) {
                                        ?>
                                        <option value="<?php echo $oferta->getId() ?>"> <?php echo $oferta->getNombre_oferta() ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </li>
                            <li><span class="requerido">*</span>Especialidad<br>            
                                <select class="curso" onchange="getOrientacionesEspecialidad(this), getCursosEspecialidad(this)">
                                    <option value="0"></option>
                                </select>
                            </li>
                            <li><div class="box_orientacion"></div></li>
                            <li><span class="requerido">*</span>Grado<br>            
                                <select class="grado">
                                    <option value="0"></option>
                                </select>
                            </li>
                            <li class="box-ciclo"><span class="requerido">*</span>Ciclo<br>
                                <select class="ciclo">
                                    <option value="0"></option>
                                    <?php
                                    foreach ($DaoCiclos->getCiclosFuturos() as $k => $v) {
                                        ?>
                                        <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave() ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </li>
                            <li><span class="requerido">*</span>Opci&oacute;n de pago<br>
                                <select class="opcionPago">
                                    <option value="0"></option>
                                    <option value="1">Materias</option>
                                    <option value="2">Plan Completo</option>
                                </select>
                            </li>
                            <li><span class="requerido">*</span>Turno<br>
                                <select class="turno">
                                    <option value="0"></option>
                                    <?php
                                    foreach($DaoTurnos->getTurnos() as $turno){
                                        ?>
                                        <option value="<?php echo $turno->getId()?>"><?php echo $turno->getNombre()?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </li>
                        </ul>
                        <?php
                        }
                        ?>
                        </div>
                    </div>
                    <button type="button" class="btns btns-primary btns-lg" id="button_ins" onclick="save_inte()">Guardar</button>
                </div>
            </td>
            <td id="column_two">
                <div id="box_menus">
                    <?php
                    require_once '../estandares/menu_derecho.php';
                    ?>
                    <ul>
                        <li><a href="interesado.php" class="link">Nuevo interesado</a></li>
                        <li><span onclick="mostrarBoxTutor()">Añadir tutor </span></li>
                        <?php
                        if (isset($id) && $id > 0) {
                            ?>
                            <li><span onclick="mostrar_box_comentario(<?php echo $id ?>, 'update')">Capturar seguimiento</span></li>
                            <li><span onclick="add_oferta()">Añadir oferta </span></li>
                            <?php
                            if ($count_ofertas > 0) {
                                if (isset($perm['31'])) {
                                    ?>
                                    <li><span onclick="box_inscripcion()">Inscribir</span></li>
                                    <?php
                                }
                            }
                        }
                        ?>

                        <li><a href="interesados.php">Regresar</a></li>
                    </ul>
                </div>
            </td>
        </tr>
    </table>
    <input type="hidden" id="Id_alumn" value="<?php echo $id ?>"/>
    <input type="hidden" id="Id_dir" value="<?php echo $id_dir ?>"/>
    <?php
}

if (isset($_POST['action']) && $_POST['action'] == "mostrar_box_comentario") {
    $DaoHistorialSeguimiento = new DaoHistorialSeguimiento();
    $DaoUsuarios = new DaoUsuarios();
    ?>
    <div id="box_emergente" class="box_seguimiento">
        <h1><i class="fa fa-history"></i> Historial de seguimiento</h1>
        <ul id="list-historia">
            <?php
            $count = 0;
            foreach ($DaoHistorialSeguimiento->getHistorialSeguimiento('inte', $_POST['Id_int']) as $historial) {
                $usu = $DaoUsuarios->show($historial->getId_usu());
                ?>
                <li>
                    <p><b>Fecha:</b> <?php echo $historial->getFecha_log() ?></p>
                    <p><b>Usuario:</b> <?php echo $usu->getNombre_usu() . " " . $usu->getApellidoP_usu() . " " . $usu->getApellidoM_usu(); ?></p>
                    <?php
                    if (strlen($historial->getTipoSeguimiento()) > 0) {
                        if ($historial->getTipoSeguimiento() == "email") {
                            $tipo = "Email";
                        } elseif ($historial->getTipoSeguimiento() == "tel") {
                            $tipo = "Teléfono";
                        }
                        ?>
                        <p><b>Contactado via:</b> <?php echo $tipo; ?></p>
                        <?php
                    }
                    ?>
                    <p><b>Comentarios:</b> <?php echo $historial->getComen_log() ?></p>
                </li>
                <?php
                $count++;
            }
            ?>
        </ul>
        <p>Contactar el dia:<br><input type="date" id="contactar"/></p>
        <?php
        if ($count > 0) {
            ?>
            <p>Contactado via:<br>
                <select id="tipo-seg">
                    <option value="0"></option>
                    <option value="1">Email</option>
                    <option value="2">Teléfono</option>
                </select>
            </p>
            <?php
        }
        ?>
        <p>Comentarios:<br><textarea id="coment"></textarea></p>
        <button onclick="capturar_seguimiento(<?php echo $_POST['Id_int'] ?>)">Guardar</button><button onclick="ocultar_error_layer()">Cerrar</button>
    </div>
    <?php
}


if (isset($_POST['action']) && $_POST['action'] == "capturar_seguimiento") {
    $tipo = "";
    if (isset($_POST['tipo']) && $_POST['tipo'] == "1") {
        $tipo = "email";
    } elseif (isset($_POST['tipo']) && $_POST['tipo'] == "2") {
        $tipo = "tel";
    }
    $DaoHistorialSeguimiento = new DaoHistorialSeguimiento();
    $HistorialSeguimiento = new HistorialSeguimiento();
    $HistorialSeguimiento->setComen_log($_POST['coment']);
    $HistorialSeguimiento->setDia_contactar($_POST['contactar']);
    $HistorialSeguimiento->setFecha_log(date('Y-m-d H:i:s'));
    $HistorialSeguimiento->setId_usu($_COOKIE['admin/Id_usu']);
    $HistorialSeguimiento->setTipoSeguimiento($tipo);
    $HistorialSeguimiento->setTipo_relLog('inte');
    $HistorialSeguimiento->setIdrel_log($_POST['Id_int']);
    $DaoHistorialSeguimiento->add($HistorialSeguimiento);

    //Captura de historial
    $DaoUsuarios = new DaoUsuarios();
    $DaoAlumnos = new DaoAlumnos();
    $alum = $DaoAlumnos->show($_POST['Id_int']);
    $TextoHistorial = "Se registra seguimiento para " . $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() . ", " . $_POST['coment'];
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Interesados");
}


if (isset($_POST['action']) && $_POST['action'] == "buscar_int") {
    $DaoUsuarios = new DaoUsuarios();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $DaoAlumnos = new DaoAlumnos();
    $query = "SELECT * FROM inscripciones_ulm WHERE Email_ins LIKE '%" . $_POST['buscar'] . "%' AND Id_plantel=" . $usu->getId_plantel();
    foreach ($DaoAlumnos->advanced_query($query) as $alum) {
        if ($alum->getTipo() == 1) {
            ?>
            <li><a href="alumno.php?id=<?php echo $alum->getId() ?>"><?php echo $alum->getEmail() ?></a></li>
            <?php
        } elseif ($alum->getTipo() == 0) {
            ?>
            <li><a href="interesado.php?id=<?php echo $alum->getId() ?>"><?php echo $alum->getEmail() ?></a></li>
            <?php
        }
    }
}



if (isset($_POST['action']) && $_POST['action'] == "add_oferta") {
    $DaoOfertas = new DaoOfertas();
    $DaoCiclos = new DaoCiclos();
    $DaoTurnos= new DaoTurnos();
    ?>
    <ul class="form">
        <li><span class="requerido">*</span>Oferta de inter&eacute;s<br>            
            <select class="oferta" onchange="getTipociclo(this)">
                <option value="0"></option>
                <?php
                foreach ($DaoOfertas->showAll() as $oferta) {
                    ?>
                    <option value="<?php echo $oferta->getId() ?>"> <?php echo $oferta->getNombre_oferta() ?></option>
                    <?php
                }
                ?>
            </select>
        </li>
        <li><span class="requerido">*</span>Especialidad<br>            
            <select class="curso" onchange="getOrientacionesEspecialidad(this)">
                <option value="0"></option>
            </select>
        </li>
        <li><div class="box_orientacion"></div></li>
        <li><span class="requerido">*</span>Grado<br>            
            <select class="grado">
                <option value="0"></option>
            </select>
        </li>
        <li><span class="requerido">*</span>Ciclo<br>
            <select class="ciclo">
                <option value="0"></option>
                <?php
                foreach ($DaoCiclos->getCiclosFuturos() as $ciclo) {
                    ?>
                    <option value="<?php echo $ciclo->getId() ?>"><?php echo $ciclo->getClave() ?></option>
                    <?php
                }
                ?>
            </select>
        </li>
        <li><span class="requerido">*</span>Opci&oacute;n de pago<br>
            <select class="opcionPago">
                <option value="0"></option>
                <option value="1">Materias</option>
                <option value="2">Plan Completo</option>
            </select>
        </li>
        <li><span class="requerido">*</span>Turno<br>
            <select class="turno">
                <option value="0"></option>
                <?php
                foreach($DaoTurnos->getTurnos() as $turno){
                    ?>
                    <option value="<?php echo $turno->getId()?>"><?php echo $turno->getNombre()?></option>
                <?php
                }
                ?>
            </select>
        </li>
    </ul>
    <?php
}


if (isset($_POST['action']) && $_POST['action'] == "getEspecialidades") {
    $DaoEspecialidades = new DaoEspecialidades();
    ?>
    <option value="0"></option>
    <?php
    foreach ($DaoEspecialidades->getEspecialidadesOferta($_POST['Id_oferta']) as $esp) {
        ?>
        <option value="<?php echo $esp->getId() ?>"> <?php echo $esp->getNombre_esp() ?> </option>
        <?php
    }
}




if (isset($_POST['action']) && $_POST['action'] == "getOrientacionesEspecialidad") {
    $DaoOrientaciones = new DaoOrientaciones();
    if (count($DaoOrientaciones->getOrientacionesEspecialidad($_POST['Id_esp'])) > 0) {
        ?>
        Orientaci&oacute;n<br>
        <select class="orientacion">
            <option value="0"></option>
            <?php
            foreach ($DaoOrientaciones->getOrientacionesEspecialidad($_POST['Id_esp']) as $k => $v) {
                ?>
                <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre() ?> </option>
                <?php
            }
            ?>
        </select>
        <?php
    }
}


if (isset($_POST['action']) && $_POST['action'] == "getGrado") {
    $DaoGrados = new DaoGrados();
    ?>
    <option value="0"></option>
    <?php
    foreach ($DaoGrados->getGradosEspecialidad($_POST['Id_esp']) as $k => $v) {
        ?>
        <option value="<?php echo $v->getId() ?>"> <?php echo $v->getGrado() ?> </option>
        <?php
    }
}



if (isset($_POST['action']) && $_POST['action'] == "preguntar_inscripcion") {
    $DaoAlumnos = new DaoAlumnos();
    $int = $DaoAlumnos->show($_POST['Id_int']);
    ?>
    <div id="box_emergente">
        <h1>&#191;Deseas inscribir como alumno a<br>  <?php echo strtoupper($int->getNombre() . " " . $int->getApellidoP() . " " . $int->getApellidoM()) ?> &#63</h1>
        <p><button id="no_insc">No</button><button onclick="verificar_pago_inscripcion(<?php echo $_POST['Id_ofe_alum']; ?>, <?php echo $_POST['Id_ciclo']; ?>, <?php echo $_POST['Id_esp']; ?>)">Si</button></p>
    </div>
    <?php
}


if (isset($_POST['action']) && $_POST['action'] == "delete_ofe_alum") {
    delete_ofe_alum($_POST['Id_ofe_alum'], $_POST['Id_alum']);
}



if (isset($_POST['action']) && $_POST['action'] == "buscar_ciudad") {
    $DaoDirecciones = new DaoDirecciones();
    $query = "SELECT DISTINCT  Ciudad_dir FROM direcciones_uml WHERE Ciudad_dir LIKE '%" . $_POST['buscar'] . "%'";
    foreach ($DaoDirecciones->advanced_query($query) as $direccion) {
        ?>
        <li><?php echo $direccion->getCiudad_dir() ?></li>
        <?php
    }
}



if ($_POST['action'] == "box_inscripcion") {
    $DaoAlumnos = new DaoAlumnos();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoCursosEspecialidad= new DaoCursosEspecialidad();
    
    $int = $DaoAlumnos->show($_POST['Id_alumn']);
    ?>
    <div class="box-emergente-form" id="box-inscripcion">
        <h1>Ofertas del alumno</h1>
        <table class="table" id="box-ofertas-ins">
            <thead>
                <tr>
                    <td>Núm.</td>
                    <td>Oferta</td>
                    <td>Especialidad</td>
                    <td>Orientaci&oacute;n</td>
                    <td>Curso</td>
                    <td class="td-center">Inscribir</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $count = 1;
                $existeOfertaPagada = 0;
                
                foreach ($DaoOfertasAlumno->getOfertasAlumno($_POST['Id_alumn']) as $ofertaAlumno) {
                    $oferta = $DaoOfertas->show($ofertaAlumno->getId_ofe());
                    $esp = $DaoEspecialidades->show($ofertaAlumno->getId_esp());
                    
                    $nombre_ori = "";
                    if ($ofertaAlumno->getId_ori() > 0) {
                        $ori = $DaoOrientaciones->show($ofertaAlumno->getId_ori());
                        $nombre_ori = $ori->getNombre();
                    }
                    
                    if($oferta->getTipoOferta()==1){
                        $ExistePagoInscripcion = $DaoPagosCiclo->verificarPagoInscripcion($ofertaAlumno->getId());
                        $primerCiclo = $DaoCiclosAlumno->getPrimerCicloOferta($ofertaAlumno->getId());
                        ?>
                <tr Id_ofe_alum="<?php echo $ofertaAlumno->getId(); ?>" Id_ciclo="<?php echo $primerCiclo->getId_ciclo() ?>" Id_esp="<?php echo $esp->getId(); ?>" pago_inscripcion="<?php echo $ExistePagoInscripcion; ?>" id-ciclo-alum="<?php echo $primerCiclo->getId()?>">
                            <td><?php echo $count; ?></td>
                            <td><?php echo $oferta->getNombre_oferta(); ?></td>
                            <td><?php echo $esp->getNombre_esp() ?></td>
                            <td><?php echo $nombre_ori ?></td>
                            <td></td>
                            <?php
                            if ($ExistePagoInscripcion == 1) {
                                $existeOfertaPagada=1;
                                ?>
                                <td class="td-center"><input type="checkbox" class="inscribir"/></td>
                                <?php
                            } else {
                                ?>
                                <td class="td-center">
                                    <button onclick="generar_pdf_insc(<?php echo $ofertaAlumno->getId_alum(); ?>,<?php echo $ofertaAlumno->getId(); ?>)">Formato de pago</button>
                                    <button onclick="pagar(<?php echo $ofertaAlumno->getId() ?>)">Pagar</button>
                                </td>
                                <?php
                            }
                            ?>
                            </tr>
                        <?php
                    }elseif($oferta->getTipoOferta()==2){
                        $ExistePagoInscripcion = $DaoPagosCiclo->verificarPagoInscripcion($ofertaAlumno->getId());
                        $primerCiclo = $DaoCiclosAlumno->getPrimerCicloOferta($ofertaAlumno->getId());
                        $curso=$DaoCursosEspecialidad->show($ofertaAlumno->getId_curso_esp());
                        $fechaIni = "Fecha abierta";
                        if (strlen($curso->getFechaInicio()) > 0 && strlen($curso->getFechaFin()) > 0) {
                            $fechaIni = "Inicio: " . $curso->getFechaInicio() . " - Fin: " . $curso->getFechaFin();
                        }
                        ?>
                        <tr Id_ofe_alum="<?php echo $ofertaAlumno->getId(); ?>" Id_ciclo="<?php echo $primerCiclo->getId_ciclo() ?>" Id_esp="<?php echo $esp->getId(); ?>" pago_inscripcion="<?php echo $ExistePagoInscripcion; ?>" id-ciclo-alum="<?php echo $primerCiclo->getId()?>">
                            <td><?php echo $count; ?></td>
                            <td><?php echo $oferta->getNombre_oferta(); ?></td>
                            <td><?php echo $esp->getNombre_esp() ?></td>
                            <td><?php echo $nombre_ori ?></td>
                            <td><?php echo $fechaIni;?></td>
                            <?php
                            if ($ExistePagoInscripcion == 1) {
                                $existeOfertaPagada=1;
                                ?>
                                <td class="td-center"><input type="checkbox" class="inscribir"/></td>
                                <?php
                            } else {
                                ?>
                                <td class="td-center">
                                    <button onclick="generar_pdf_insc(<?php echo $ofertaAlumno->getId_alum(); ?>,<?php echo $ofertaAlumno->getId(); ?>)">Formato de pago</button>
                                    <button onclick="pagarCurso(<?php echo $ofertaAlumno->getId() ?>)">Pagar</button>
                                </td>
                                <?php
                            }
                            ?>
                            </tr>  
                            <?php
                    }
                    $count++;
                }
                ?>
            </tbody>
        </table>
        <p><button onclick="ocultar_error_layer()">Cancelar</button>
            <?php
            if ($existeOfertaPagada == 1) {
                ?>
                <button onclick="inscribir()">Inscribir</button>
                <?php
            }
            ?>
        </p>
    </div>
    <?php
}


if ($_POST['action'] == "inscribir") {
    //Generamos la matricula del alumno
    //Tomando como base el primer digito es del plantel al que pertence
    //y los restantes son la ultima matricula existente
    $base = new base();
    $DaoUsuarios = new DaoUsuarios();
    $DaoAlumnos = new DaoAlumnos();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoOfertas = new DaoOfertas();
    $DaoCiclos = new DaoCiclos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoPlanteles = new DaoPlanteles();
    $DaoDirecciones = new DaoDirecciones();
    $DaoMateriasCicloAlumno = new DaoMateriasCicloAlumno();
    $DaoMaterias = new DaoMaterias();
    $DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
    $DaoCursosEspecialidad=new DaoCursosEspecialidad();
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $params=$DaoParametrosPlantel->getParametrosPlantelArray();
    
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $alum = $DaoAlumnos->show($_POST['Id_int']);

    //Esto es porque se migraron los alumnos los cuales ya tenian matricula
    if ($alum->getMatricula() != null) {
        $matricula = $alum->getMatricula();
    } else {
        $matricula = $base->generar_matricula($usu->getId_plantel());
    }
    $referancia_pago = $base->generar_referenciaPago($matricula);
    $key = $base->generarKey(20);

    $Alumnos = $DaoAlumnos->show($_POST['Id_int']);
    $Alumnos->setAlta_alum(date('Y-m-d H:i:s'));
    $Alumnos->setActivo_alum(1);
    $Alumnos->setTipo(1);
    $Alumnos->setMatricula($matricula);
    $Alumnos->setRefencia_pago($referancia_pago);
    $Alumnos->setRecoverPass($key);
    $DaoAlumnos->update($Alumnos);

    foreach ($_POST['ofertas'] as $k => $v) {

        if ($v['pago_inscripcion'] == 1) {
            //Metodo de pago plan completo
            $ofertaAlumno = $DaoOfertasAlumno->show($v['Id_ofe_alum']);
            $oferta = $DaoOfertas->show($ofertaAlumno->getId_ofe());
            if ($ofertaAlumno->getOpcionPago() == 2) {

                //$cicloAlumno = $DaoCiclosAlumno->getCicloOferta($v['Id_ofe_alum'], $v['Id_ciclo']);
                $cicloAlumno= $DaoCiclosAlumno->show($v['Id_ciclo_alumno']);
                $esp = $DaoEspecialidades->show($v['Id_esp']);
                
                if($oferta->getTipoOferta()==1){
                    //Se generan los pagos apartir de la fecha de inicio del ciclo
                    //Generar cargos para las mensualidades
                    $ciclo = $DaoCiclos->show($cicloAlumno->getId_ciclo());
                    $fecha_pago = $ciclo->getFecha_ini();
                    $anio = date("Y", strtotime($fecha_pago));
                    $mes = date("m", strtotime($fecha_pago));
                    
                    //Generar mensualidades
                    for ($i = 1; $i <= $esp->getNum_pagos(); $i++) {
                        $d=1;
                        if(isset($params['DiaInicioMensualidades']) && strlen($params['DiaInicioMensualidades'])>0){
                           $d=$params['DiaInicioMensualidades'];
                        }
                        $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes + $i, $d, $anio));
                        
                        $Precio_curso = ($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos();
                        $PagosCiclo = new PagosCiclo();
                        $PagosCiclo->setConcepto("Mensualidad");
                        $PagosCiclo->setFecha_pago($base->get_day_pago($fecha_pago, $v['Id_ciclo']));
                        $PagosCiclo->setMensualidad($Precio_curso);
                        $PagosCiclo->setId_ciclo_alum($cicloAlumno->getId());
                        $PagosCiclo->setId_alum($_POST['Id_int']);
                        $PagosCiclo->setTipo_pago("pago");
                        $PagosCiclo->setTipoRel("alum");
                        $PagosCiclo->setIdRel($_POST['Id_int']);
                        $DaoPagosCiclo->add($PagosCiclo);
                    }
                }elseif($oferta->getTipoOferta()==2){
                    $fecha_pago=$DaoCursosEspecialidad->getFechaPago($ofertaAlumno->getId_curso_esp());
                    $anio = date("Y", strtotime($fecha_pago));
                    $mes = date("m", strtotime($fecha_pago));
                    $day= date("d", strtotime($fecha_pago));
                    
                    //Generar mensualidades
                    for ($i = 1; $i <= $esp->getNum_pagos(); $i++) {
                        $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes +$i, $day , $anio));
                        
                        $Precio_curso = ($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos();
                        $PagosCiclo = new PagosCiclo();
                        $PagosCiclo->setConcepto("Mensualidad");
                        $PagosCiclo->setFecha_pago($fecha_pago);
                        $PagosCiclo->setMensualidad($Precio_curso);
                        $PagosCiclo->setId_ciclo_alum($cicloAlumno->getId());
                        $PagosCiclo->setId_alum($_POST['Id_int']);
                        $PagosCiclo->setTipo_pago("pago");
                        $PagosCiclo->setTipoRel("alum");
                        $PagosCiclo->setIdRel($_POST['Id_int']);
                        $DaoPagosCiclo->add($PagosCiclo);
                    }
                }

                //Cargar materias que cursara en el ciclo
                $query = "SELECT * FROM Materias_especialidades 
                                   JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat
                          WHERE Id_esp=" . $v['Id_esp'] . " AND Grado_mat=" . $cicloAlumno->getId_grado() . " AND Activo_mat_esp=1 AND Activo_mat=1";
                foreach ($DaoMateriasEspecialidad->advanced_query($query) as $matEsp) {
                    $query = "SELECT * FROM materias_ciclo_ulm WHERE Id_ciclo_alum=" . $cicloAlumno->getId() . " AND Id_mat_esp=" . $matEsp->getId() . " AND Id_alum=" . $_POST['Id_int'];
                    $materia = $DaoMaterias->existeQuery($query);
                    if ($materia->getId() == 0) {
                        $MateriasCicloAlumno = new MateriasCicloAlumno();
                        $MateriasCicloAlumno->setId_ciclo_alum($cicloAlumno->getId());
                        $MateriasCicloAlumno->setId_mat_esp($matEsp->getId());
                        $MateriasCicloAlumno->setTipo(1);
                        $MateriasCicloAlumno->setActivo(1);
                        $MateriasCicloAlumno->setId_alum($_POST['Id_int']);
                        $DaoMateriasCicloAlumno->add($MateriasCicloAlumno);
                    }
                }
            }
            //Captura de historial
            $TextoHistorial = "Inscribe al interesado " . $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() . ", en " . $oferta->getNombre_oferta();
            $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Interesados");
    
        } else {
            //Eliminanos las ofertas que no le interezaron
            delete_ofe_alum($v['Id_ofe_alum'], $_POST['Id_int']);
        }
    }

    //Enviar email de bienvenida
    $planteles = $DaoPlanteles->show($alum->getId_plantel());
    $subject = "¡Bienvenido al sistema escolar " . $planteles->getAbreviatura() . "!";

    $Mensaje = '<p style="margin-bottom: 20px;">Para ingresar al sistema escolar, deberas establecer tu contrase&ntilde;a en la siguiente liga:</p>
              <p style="margin-bottom: 20px;"><a href="http://www.' . $planteles->getDominio() . '/alumno/welcome.php?key=' . $key . '">http://www.' . $planteles->getDominio() . '/alumno/welcome.php?key=' . $key . '</a></p>';

    if (strlen($alum->getEmail()) > 0) {
        $arrayData = array();
        $arrayData['Asunto'] = $subject;
        $arrayData['Mensaje'] = $Mensaje;
        $arrayData['Destinatarios'] = array();

        //Destinatario
        $Destinatario = array();
        $Destinatario['email'] = $alum->getEmail();
        //$Destinatario['email']= "christian310332@gmail.com";
        $Destinatario['name'] = $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM();
        array_push($arrayData['Destinatarios'], $Destinatario);

        //$base->send_email($arrayData) . "<br>";
    }
}

/*
  if(isset($_POST['action']) &&  $_POST['action']=="mostrar_acciones"){
  $DaoOfertasAlumno= new DaoOfertasAlumno();
  $DaoEspecialidades= new DaoEspecialidades();
  $DaoOfertas= new  DaoOfertas();
  ?>
  <div id="box_emergente" class="box_acciones">
  <h1>Acciones</h1>
  <table class="table" id="tabla_acciones">
  <thead>
  <tr>
  <td>Oferta</td>
  <td>Exámen de Admisi&oacute;n</td>
  <td>Cita sesi&oacute;n informativa</td>
  <td>Clase de muestra</td>
  </tr>
  </thead>
  <tbody>
  <?php
  foreach($DaoOfertasAlumno->getOfertasAlumno($_POST['Id']) as $ofertaAlumno) {
  $oferta = $DaoOfertas->show($ofertaAlumno->getId_ofe());
  $esp = $DaoEspecialidades->show($ofertaAlumno->getId_esp());
  ?>
  <tr id-ofe-alum="<?php echo $ofertaAlumno->getId()?>">
  <td><?php echo  $oferta->getNombre_oferta(); ?><br><?php echo $esp->getNombre_esp(); ?></td>
  <td style="text-align: center;"><input type="checkbox" value="1" class="examen"/></td>
  <td style="text-align: center;"><input type="date" class="cita_sesion"/></td>
  <td style="text-align: center;"><input type="date" class="clase_muestra"/></td>
  </tr>
  <?php
  }
  ?>
  </tbody>
  </table>
  <p><button onclick="ejecutar_accion(<?php echo $_POST['Id']?>)">Aceptar</button>
  <button onclick="ocultar_error_layer()">Cancelar</button></p>
  </div>
  <?php
  }

  if($_POST['action']=="ejecutar_accion"){
  $class_usuarios= new class_usuarios($_COOKIE['admin/Id_usu']);
  $usu=$class_usuarios->get_usu();

  $class_interesados = new class_interesados($_POST['Id_int']);
  $int=$class_interesados->get_interesado();
  foreach($_POST['acciones'] as $k=>$v){
  $ofe_alum=$class_interesados->get_oferta_alumno($v['Id_ofe_alumn']);
  $class_ofertas= new class_ofertas();
  $oferta=$class_ofertas->get_oferta($ofe_alum['Id_ofe']);
  $esp = $class_ofertas->get_especialidad($ofe_alum['Id_esp']);
  $nombre_ori="";
  if ($ofe_alum['Id_ori'] > 0) {
  $ori = $class_ofertas->get_orientacion($ofe_alum['Id_ori']);
  $nombre_ori = $ori['Nombre_ori'];
  }

  $costo=null;
  if($v['tipo']==1){
  $costo=200;
  //Examen
  $asunto="Examen de admisión";
  $mensaje="Fecha: ".date('Y-m-d')."<br><br>
  Se ha confirmado un examen de admisión para el aspirante:<br><br>

  <b>".$int['Nombre_ins']." ".$int['ApellidoP_ins']." ".$int['ApellidoM_ins']."</b><br><br>
  Oferta académica: ".$oferta['Nombre_oferta']."<br>
  Especialidad: ".$esp['Nombre_esp']."<br>
  Orientación: ".$nombre_ori."<br>

  Confirmado por: ".$usu['Nombre_usu']." ".$usu['ApellidoP_usu']." ".$usu['ApellidoM_usu'];

  $destinatarios=array();
  $dest=array();
  $dest['nombre']="Oscar arturo muro ramos";
  $dest['email']="oscar.muro@ulm.mx";
  array_push($destinatarios,$dest);
  $dest=array();
  $dest['nombre']="Geronimo gonzalez";
  $dest['email']="geronimo.gonzalez@ulm.mx";
  array_push($destinatarios,$dest);
  $dest=array();
  $dest['nombre']="Jose salgado";
  $dest['email']="jose.salgado@ulm.mx";
  array_push($destinatarios,$dest);
  $dest=array();
  $dest['nombre']="Marien castro";
  $dest['email']="marien.castro@ulm.mx";
  array_push($destinatarios,$dest);

  }elseif($v['tipo']==2){
  $costo=null;
  //Sesion informativa
  $asunto="Sesión informativa";
  $mensaje="Fecha: ".date('Y-m-d')."<br><br>
  Se ha confirmado asistencia para sesión informativa el dia <b>". $v['fecha_sesion']."</b> para el aspirante:<br><br>

  <b>".$int['Nombre_ins']." ".$int['ApellidoP_ins']." ".$int['ApellidoM_ins']."</b><br><br>
  Oferta académica: ".$oferta['Nombre_oferta']."<br>
  Especialidad: ".$esp['Nombre_esp']."<br>
  Orientación: ".$nombre_ori."<br>

  Confirmado por: ".$usu['Nombre_usu']." ".$usu['ApellidoP_usu']." ".$usu['ApellidoM_usu'];

  $destinatarios=array();
  $dest=array();
  $dest['nombre']="Oscar arturo muro ramos";
  $dest['email']="oscar.muro@ulm.mx";
  array_push($destinatarios,$dest);
  $dest=array();
  $dest['nombre']="Geronimo gonzalez";
  $dest['email']="geronimo.gonzalez@ulm.mx";
  array_push($destinatarios,$dest);
  $dest=array();
  $dest['nombre']="Jose salgado";
  $dest['email']="jose.salgado@ulm.mx";
  array_push($destinatarios,$dest);
  $dest=array();
  $dest['nombre']="Belinda gracia";
  $dest['email']="belinda.gracia@ulm.mx";
  array_push($destinatarios,$dest);

  }elseif($v['tipo']==3){
  $costo=200;

  //Clase de muestra
  $asunto="Clase de Muestra";
  $mensaje="Fecha: ".date('Y-m-d')."<br><br>
  Se ha confirmado la programación de una Clase de Muestra  el dia <b>". $v['fecha_sesion']."</b> para el aspirante:<br><br>

  <b>".$int['Nombre_ins']." ".$int['ApellidoP_ins']." ".$int['ApellidoM_ins']."</b><br><br>
  Oferta académica: ".$oferta['Nombre_oferta']."<br>
  Especialidad: ".$esp['Nombre_esp']."<br>
  Orientación: ".$nombre_ori."<br>

  Confirmado por: ".$usu['Nombre_usu']." ".$usu['ApellidoP_usu']." ".$usu['ApellidoM_usu'];

  $destinatarios=array();
  $dest=array();
  $dest['nombre']="Oscar arturo muro ramos";
  $dest['email']="oscar.muro@ulm.mx";
  array_push($destinatarios,$dest);
  $dest=array();
  $dest['nombre']="Geronimo gonzalez";
  $dest['email']="geronimo.gonzalez@ulm.mx";
  array_push($destinatarios,$dest);
  $dest=array();
  $dest['nombre']="Jose salgado";
  $dest['email']="jose.salgado@ulm.mx";
  array_push($destinatarios,$dest);
  $dest=array();
  $dest['nombre']="Belinda gracia";
  $dest['email']="belinda.gracia@ulm.mx";
  array_push($destinatarios,$dest);
  $dest=array();
  $dest['nombre']="Marien castro";
  $dest['email']="marien.castro@ulm.mx";
  array_push($destinatarios,$dest);
  $dest=array();
  $dest['nombre']="Control escolar";
  $dest['email']="controlescolar@ulm.mx";
  array_push($destinatarios,$dest);

  }

  $sql_nueva_entrada = sprintf("INSERT INTO Acciones_interesado (Id_int, TipoAccion, Id_ofe_alum,CostoAccion,FechaAccion,FechaSession,Usuario,Plantel) VALUES (%s,%s,%s,%s,%s,%s,%s,%s);",
  GetSQLValueString($_POST['Id_int'], "int"),
  GetSQLValueString($v['tipo'] , "int"),
  GetSQLValueString($v['Id_ofe_alumn'] , "int"),
  GetSQLValueString($costo , "double"),
  GetSQLValueString(date('Y-m-d H:i:s') , "date"),
  GetSQLValueString($v['fecha_sesion'] , "date"),
  GetSQLValueString($usu['Id_usu'] , "int"),
  GetSQLValueString($usu['Id_plantel'] , "int"));
  $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

  send_email($asunto,$mensaje,$nombre,$destinatarios);
  }
  }


  function send_email($asunto,$mensaje,$nombre,$destinatarios){
  global $database_cnn, $cnn;

  $texto_html=' <html>
  <head></head>
  <body style="background-color: #F8F8F8;padding-top: 40px;" bgcolor="#F8F8F8">
  <div id="container" style="width: 600px; background-color: white; font-family: arial; font-size: 13px; margin: auto;">
  <div id="boxtext" style="width: 540px; padding: 30px;min-height: 300px;">
  <img src="http://www.ulm.mx/admin_ce/images/logoulm.png" ><h1 style="font-size: 30px; font-family: arial; color: #606060; font-weight: 100; margin-top: 20px;">'.$asunto.'</h1>
  <p style="margin-bottom: 20px;">'.$mensaje.'</p>
  </div>
  <div id="footer" style="background-color: #606060; color: white; font-size: 13px; width: 540px; padding: 30px;padding-top: 20px;padding-bottom: 20px;">
  <table>
  <tr>
  <td style="vertical-align: top; padding-right: 5px;" valign="top">
  <div id="text_footer" style="width: 226px; font-size: 13px; color: white; font-family: arial;">
  <p style="margin-bottom: 10px;">Tel&eacute;fonos:<br> (0133) 36 16 27 69 | 01800 841 9955 </p>
  <p style="margin-bottom: 10px;">Domicilio: <br>Av. Vallarta No. 1543 Col. Americana Guadalajara, Jalisco</p>
  </div>
  </td>
  </tr></table>
  </div>
  </div>
  <p class="footer" style="font-family: arial; font-size: 10px; color: black; width: 600px; margin: 5px auto auto;">2014 ULM Universidad Libre de M&uacute;sica</p>
  </body>
  </html>';

  $texto_txt='
 * *******************************
  '.$asunto.'
 * *******************************

 * *********************
  Mensaje
 * *********************


  Mensaje: '.$mensaje.'



  Teléfonos:  (0133) 36 16 27 69 | 01800 841 9955
  Domicilio:  Av. Vallarta No. 1543 Col. Americana Guadalajara, Jalisco

  2014 ULM Universidad Libre de Musica';

  $arrayData= array();
  $arrayData['Texto']=$texto_txt;
  $arrayData['Html']=$texto_html;
  $arrayData['Asunto']=$asunto;
  $arrayData['Destinatarios']=array();

  foreach($destinatarios as $k=>$v){
  $Data= array();
  $Data['email']= $v['email'];
  $Data['name']= $v['nombre'];
  array_push($arrayData['Destinatarios'], $Data);
  }

  $class_bdd= new class_bdd();
  $class_bdd->send_email($arrayData);
  }
 *                             
 */

if ($_POST['action'] == "getTipoCiclo") {
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoCiclos = new DaoCiclos();
    $DaoTurnos= new  DaoTurnos();
    $ofe = $DaoOfertas->show($_POST['Id_ofe']);

    //1=plan por ciclos 
    //2=plan sin ciclos
    if ($ofe->getTipoOferta() == 1) {
        ?>
        <li><span class="requerido">*</span>Oferta de inter&eacute;s<br>            
            <select class="oferta" onchange="getTipociclo(this)">
                <option value="0"></option>
                <?php
                foreach ($DaoOfertas->showAll() as $oferta) {
                    ?>
                    <option value="<?php echo $oferta->getId() ?>" <?php if ($_POST['Id_ofe'] == $oferta->getId()) { ?> selected="selected" <?php } ?>> <?php echo $oferta->getNombre_oferta() ?></option>
                    <?php
                }
                ?>
            </select>
        </li>
        <li><span class="requerido">*</span>Especialidad<br>            
            <select class="curso" onchange="getOrientacionesEspecialidad(this)">
                <option value="0"></option>
                <?php
                foreach ($DaoEspecialidades->getEspecialidadesOferta($_POST['Id_ofe']) as $esp) {
                    ?>
                    <option value="<?php echo $esp->getId() ?>"><?php echo $esp->getNombre_esp() ?></option>
                    <?php
                }
                ?>
            </select>
        </li>
        <li><div class="box_orientacion"></div></li>
        <li><span class="requerido">*</span>Grado<br>            
            <select class="grado">
                <option value="0"></option>
            </select>
        </li>
        <li><span class="requerido">*</span>Ciclo<br>
            <select class="ciclo">
                <option value="0"></option>
                <?php
                foreach ($DaoCiclos->getCiclosFuturos() as $ciclo) {
                    ?>
                    <option value="<?php echo $ciclo->getId() ?>"><?php echo $ciclo->getClave() ?></option>
                    <?php
                }
                ?>
            </select>
        </li>
        <li><span class="requerido">*</span>Opci&oacute;n de pago<br>
            <select class="opcionPago">
                <option value="0"></option>
                <option value="1">Materias</option>
                <option value="2">Plan Completo</option>
            </select>
        </li>
        <li><span class="requerido">*</span>Turno<br>
            <select class="turno">
                <option value="0"></option>
                <?php
                foreach($DaoTurnos->getTurnos() as $turno){
                    ?>
                    <option value="<?php echo $turno->getId()?>"><?php echo $turno->getNombre()?></option>
                <?php
                }
                ?>
            </select>
        </li>
        <?php
    } else {
        ?>
        <li><span class="requerido">*</span>Oferta de inter&eacute;s<br>            
            <select class="oferta" onchange="getTipociclo(this)">
                <option value="0"></option>
                <?php
                foreach ($DaoOfertas->showAll() as $oferta) {
                    ?>
                    <option value="<?php echo $oferta->getId() ?>" <?php if ($_POST['Id_ofe'] == $oferta->getId()) { ?> selected="selected" <?php } ?>> <?php echo $oferta->getNombre_oferta() ?></option>
                    <?php
                }
                ?>
            </select>
        </li>
        <li><span class="requerido">*</span>Especialidad<br>            
            <select class="curso" onchange="getOrientacionesEspecialidad(this)">
                <option value="0"></option>
                <?php
                foreach ($DaoEspecialidades->getEspecialidadesOferta($_POST['Id_ofe']) as $esp) {
                    ?>
                    <option value="<?php echo $esp->getId() ?>"><?php echo $esp->getNombre_esp() ?></option>
                    <?php
                }
                ?>
            </select>
        </li>
        <li><span class="requerido">*</span>Turno<br>
            <select class="turno" onchange="getCursosEspecialidad(this)">
                <option value="0"></option>
                <?php
                foreach($DaoTurnos->getTurnos() as $turno){
                    ?>
                    <option value="<?php echo $turno->getId()?>"><?php echo $turno->getNombre()?></option>
                <?php
                }
                ?>
            </select>
        </li>
        <li>
            <span class="requerido">*</span>Fecha del curso<br>
            <select class="cursos-especialidad">

            </select>
        </li>
        <li><div class="box_orientacion"></div></li>
        <li><span class="requerido">*</span>Grado<br>            
            <select class="grado">
                <option value="0"></option>
            </select>
        </li>
        <li><span class="requerido">*</span>Opci&oacute;n de pago<br>
            <select class="opcionPago">
                <option value="0"></option>
                <option value="1">Materias</option>
                <option value="2">Plan Completo</option>
            </select>
        </li>
        <?php
    }
}

if (isset($_POST['action']) && $_POST['action'] == "getCursosEspecialidad") {
    ?>
    <option value="0"></option>
    <?php
    $DaoCursosEspecialidad= new DaoCursosEspecialidad();
    foreach ($DaoCursosEspecialidad->getCursosEspecialidad($_POST['Id_esp'], true,$_POST['Turno']) as $curso) {
        $fechaIni = "Fecha abierta";
        if (strlen($curso->getFechaInicio()) > 0 && strlen($curso->getFechaFin()) > 0) {
            $fechaIni = "Inicio: " . $curso->getFechaInicio() . " - Fin: " . $curso->getFechaFin();
        }
        ?>
        <option value="<?php echo $curso->getId() ?>"><?php echo $fechaIni ?> </option>
        <?php
    }
}


if ($_POST['action'] == "mostrarBoxTutor") {
    ?>
    <ul class="form" id-tutor="0" id-dir="0">
        <li>Parentesco<br><input type="text" class="parentesco-tutor"/></li>
        <li>Nombre<br><input type="text" class="nombre-tutor"/></li>
        <li>Apellido paterno<br><input type="text" class="apellidoP-tutor"/></li>
        <li>Apellido materno<br><input type="text" class="apellidoM-tutor"/></li>
        <li>Correo electrónico<br><input type="text" class="email-tutor"/></li>
        <li>Teléfono<br><input type="text" class="tel-tutor"/></li>
        <li>Celular<br><input type="text" class="cel-tutor"/></li>
        <li>Calle<br><input type="text" class="calle-tutor"/></li>
        <li>Núm ext.<br><input type="text" class="numext-tutor"/></li>
        <li>Núm int.<br><input type="text" class="numint-tutor"/></li>
        <li>Colonia<br><input type="text" class="colonia-tutor"/></li>
        <li>Ciudad<br><input type="text" class="ciudad-tutor"/></li>
        <li>Estado<br><input type="text" class="estado-tutor"/></li>
        <li>Código postal<br><input type="text" class="cp-tutor"/></li>
    </ul>
    <?php
}

if($_POST['action']=="delete_tutor"){
    $DaoTutores= new DaoTutores();
    $DaoDirecciones= new DaoDirecciones();
    $DaoTutores->delete($_POST['id_tutor']);
    $DaoDirecciones->delete($_POST['id_dir']);
    update_page($_POST['id_alumn']);
}