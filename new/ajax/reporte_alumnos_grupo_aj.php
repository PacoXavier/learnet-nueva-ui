<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if ($_POST['action'] == "buscarAlum") {
    $DaoAlumnos = new DaoAlumnos();
    foreach ($DaoAlumnos->buscarAlumnosAltasYBajas($_POST['buscar']) as $k => $v) {
        ?>
        <li id_alum="<?php echo $v->getId(); ?>" onclick="getOfertas(<?php echo $v->getId(); ?>)"><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li>
        <?php
    }
}


if($_POST['action']=="filtro"){
    $DaoAlumnos= new DaoAlumnos();
    $DaoGrupos= new DaoGrupos();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    
    $query="";
    if($_POST['Id_alum']>0){
        $query=$query." AND Id_ins=".$_POST['Id_alum'];
    }

    $grupo=$DaoGrupos->show($_POST['Id_grupo']);
    $mat = $DaoMaterias->show($grupo->getId_mat());
    $mat_esp=$DaoMateriasEspecialidad->show($grupo->getId_mat_esp());

    $NombreMat=$mat->getNombre();
    if(strlen($mat_esp->getNombreDiferente())>0){
        $NombreMat=$mat_esp->getNombreDiferente(); 
    }
    $turno="";
    if($grupo->getTurno()==1){
       $turno="MATUTINO";
    }if($grupo->getTurno()==2){
       $turno="VESPERTINO";
    }if($grupo->getTurno()==3){
       $turno="NOCTURNO";
    }
    foreach($DaoGrupos->getAlumnosBYGrupo($_POST['Id_grupo']) as $k=>$v){
       $alumn=$DaoAlumnos->show($v['Id_ins']);
       $asis=$DaoGrupos->getAsistenciasGrupoAlum($grupo->getId(),$v['Id_ins']);
       $total= $DaoGrupos->porcentajeAsistencias($asis['Asistencias'],$asis['Justificaciones'],$asis['Faltas']);
       
        $colorCalTotalParciales="red";
        if($v['CalTotalParciales']>=$mat->getPromedio_min()){
            $colorCalTotalParciales="green";
        }
        $colorCalExtraordinario="red";
        if($v['CalExtraordinario']>=$mat->getPromedio_min()){
            $colorCalExtraordinario="green";
        }
        $colorCalEspecial="red";
        if($v['CalEspecial']>=$mat->getPromedio_min()){
            $colorCalEspecial="green";
        }
       $status="D";
       if($total<80){
           $status="SD";
        }

        $Asis_aux=$asis['Asistencias'];
        if($_POST['Asis']>0){
          $Asis_aux=$_POST['Asis'];
        }
        $Falt_aux=$asis['Faltas'];
        if($_POST['Fal']>0){
          $Falt_aux=$_POST['Fal'];
        }
        $Just_aux=$asis['Justificaciones'];
        if($_POST['Just']>0){
          $Just_aux=$_POST['Just'];
        }
        $Status_aux=$status;
        if($_POST['Estatus']>0){
           $Status_aux="SD";
           if($_POST['Estatus']==1){
              $Status_aux="D";
           }
        }

    if($asis['Asistencias']==$Asis_aux && $asis['Faltas']==$Falt_aux && $asis['Justificaciones']==$Just_aux && $status==$Status_aux){
    ?>
    <tr>
        <td><?php echo $grupo->getClave()?> </td>
        <td style="width: 120px;"><?php echo $NombreMat?></td>
        <td><?php echo $turno;?></td>
        <td><?php echo $alumn->getMatricula()?></td>
        <td><?php echo $alumn->getNombre()." ".$alumn->getApellidoP()." ".$alumn->getApellidoM()?></td>
        <td style="text-align: center;color: red;"><?php echo $asis['Faltas'];?></td>
        <td style="text-align: center;color: green;"><?php echo $asis['Asistencias']; ?></td>
        <td style="text-align: center; color: black;"><?php echo $asis['Justificaciones']; ?></td>
        <td style="text-align: center; color: black;"><?php echo number_format($total, 2) ?>%</td>
        <td <?php if($total<80){?> class="reprobado" <?php }else{ ?> class="aprobado"  <?php }?>>
            <?php
            if($total<80){
            ?>
            <span>SD</span>
            <?php
            }else{
            ?>
            <span>D</span>
            <?php
            }
            ?>
        </td>
        <td style="text-align: center;color: <?php echo $colorCalTotalParciales;?>;"><?php echo $DaoAlumnos->redonderCalificacion($v['CalTotalParciales'])?></td>
        <td style="text-align: center;color: <?php echo $colorCalExtraordinario;?>;"><?php echo $DaoAlumnos->redonderCalificacion($v['CalExtraordinario'])?></td>
        <td style="text-align: center;color: <?php echo $colorCalEspecial;?>;"><?php echo $DaoAlumnos->redonderCalificacion($v['CalEspecial'])?></td>
    </tr>
    <?php
    }
    }                              
}



