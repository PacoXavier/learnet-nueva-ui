<?php
require_once('activate_error.php');
require_once('../clases/DaoUsuarios.php');
require_once('../clases/DaoCiclos.php');
require_once('../clases/DaoMaterias.php');
require_once('../clases/DaoOfertas.php');
require_once('../clases/modelos/Ofertas.php');
require_once('../clases/modelos/base.php');
require_once('../clases/DaoOrientaciones.php');
require_once('../clases/modelos/Orientaciones.php');
require_once('../clases/DaoEspecialidades.php');
require_once('../clases/modelos/Especialidades.php');

if ($_POST['action'] == "new_subnivel") {
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $Id_esp = 0;
    $Id_ori = 0;
    $NombreOri = "";
    if ($_POST['Id_ori'] > 0) {
        $ori = $DaoOrientaciones->show($_POST['Id_ori']);
        $esp = $DaoEspecialidades->show($ori->getId_esp());
        $Id_esp = $esp->getId();
        $Id_ori = $ori->getId();
        $NombreOri = $ori->getNombre();
    }
    ?>
    <div class="box-emergente-form">
        <h1>Nueva orientaci&oacute;n</h1>
        <p>Especialidad<br>
            <select id="nivel" >
                <option></option>
                <?php
                foreach ($DaoEspecialidades->getEspecialidadesOferta($_POST['Id_oferta']) as $especialidad) {
                    ?>
                    <option value="<?php echo $especialidad->getId() ?>" <?php if ($especialidad->getId() == $Id_esp) { ?>  selected="selected" <?php } ?>>
                        <?php echo $especialidad->getNombre_esp() ?></option>
                    <?php
                }
                ?>
            </select>
        </p>
        <p>Nombre<br><input type="text" value="<?php echo $NombreOri ?>" id="nombre_subnivel"/></p>

        <button onclick="save_subnivel(<?php echo $Id_ori; ?>)">Guardar</button>
        <button onclick="ocultar_error_layer()">Cerrar</button>
    </div>	
    <?php
}


if ($_POST['action'] == "save_subnivel") {
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoUsuarios = new DaoUsuarios();
    if ($_POST['Id_ori'] > 0) {
        $Orientaciones = $DaoOrientaciones->show($_POST['Id_ori']);

        //Historial
        if ($Orientaciones->getNombre() != $_POST['Nombre_nivel']) {
            $TextoHistorial = "Actualizo el nombre para la orientación " . $Orientaciones->getNombre() . " a " . $_POST['nombre_subnivel'];
            $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Ofertas académicas");
        }

        $Orientaciones->setId_esp($_POST['Id_esp']);
        $Orientaciones->setNombre($_POST['nombre_subnivel']);
        $DaoOrientaciones->update($Orientaciones);
        update_page($_POST['Id_oferta']);
    } else {
        $Orientaciones = new Orientaciones();
        $Orientaciones->setId_esp($_POST['Id_esp']);
        $Orientaciones->setNombre($_POST['nombre_subnivel']);
        $Orientaciones->setActivo(1);
        $DaoOrientaciones->add($Orientaciones);

        $TextoHistorial = "Añadio nueva orientación " . $_POST['nombre_subnivel'];
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Ofertas académicas");
        update_page($_POST['Id_oferta']);
    }
}

if ($_POST['action'] == "get_especialdades") {
    ?>
    <option value="0"></option>
    <?php
    $DaoEspecialidades = new DaoEspecialidades();
    foreach ($DaoEspecialidades->getEspecialidadesOferta($_POST['Id_oferta']) as $k => $v) {
        ?>
        <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre_esp(); ?> ee</option>
        <?php
    }
}

function update_page($Id_ofe) {
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();

    $oferta = $DaoOfertas->show($Id_ofe);
    ?>
    <div class="fondo">
        <div class="box_top">
            <h1><i class="fa fa-cog" aria-hidden="true"></i> Especialidades <?php echo $oferta->getNombre_oferta() ?></h1>
        </div>
        <div class="seccion">						
            <table class="table">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Nombre especialidad</td>
                        <td>Orientaci&oacute;n</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $count = 1;
                    foreach ($DaoEspecialidades->getEspecialidadesOferta($Id_ofe) as $k => $v) {
                        ?>
                        <tr>
                            <td><?php echo $count; ?></td>
                            <td>
                                <table>
                                    <tr>
                                        <td><?php echo $v->getNombre_esp() ?></td>
                                        <td style="text-align: center;width: 10%;">
                                            <a href="curso.php?id=<?php echo $v->getId() ?>">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>

                                            <i class="fa fa-trash" onclick="delete_esp(<?php echo $v->getId() ?>)"></i>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table class="orientaciones">
                                    <?php
                                    foreach ($DaoOrientaciones->getOrientacionesEspecialidad($v->getId()) as $k2 => $v2) {
                                        ?>
                                        <tr>
                                            <td><?php echo $v2->getNombre() ?></td>
                                            <td>
                                                <i class="fa fa-pencil-square-o" onclick="new_subnivel(<?php echo $v2->getId() ?>)"></i>
                                                <i class="fa fa-trash" onclick="delete_sub(<?php echo $v2->getId() ?>)"></i>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </td>
                        </tr>
                        <?php
                        $count++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>	
    <?php
}

if ($_POST['action'] == "delete_esp") {
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoUsuarios = new DaoUsuarios();
    $esp = $DaoEspecialidades->show($_POST['Id_esp']);

    $TextoHistorial = "Elimino la especialidad " . $esp->getNombre_esp();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Ofertas académicas");

    $DaoEspecialidades->delete($_POST['Id_esp']);
    update_page($_POST['Id_oferta']);
}


if ($_POST['action'] == "delete_sub") {

    $DaoOrientaciones = new DaoOrientaciones();
    $DaoUsuarios = new DaoUsuarios();
    $ori = $DaoOrientaciones->show($_POST['Id_ori']);

    $TextoHistorial = "Elimino la orientación " . $ori->getNombre();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Ofertas académicas");

    $DaoOrientaciones->delete($_POST['Id_ori']);
    update_page($_POST['Id_oferta']);
}



