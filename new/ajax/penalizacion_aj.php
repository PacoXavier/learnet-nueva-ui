<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="buscar"){
    $DaoDocentes= new DaoDocentes();
    foreach($DaoDocentes->buscarDocente($_POST['buscar']) as $k=>$v){
      ?>
      <a href="penalizacion.php?id=<?php echo $v->getId()?>">
       <li id-data="<?php echo $v->getId();?>"><?php echo $v->getNombre_docen()." ".$v->getApellidoP_docen()." ".$v->getApellidoM_docen() ?></li>
      </a>
      <?php      
    }
}


if($_POST['action']=="mostrar"){
    update_page($_POST['Id_docen'],$_POST['Id_ciclo']);
}

function update_page($Id_docen,$Id_ciclo) {
    $DaoPenalizacionDocente= new DaoPenalizacionDocente();
    $DaoDocentes= new DaoDocentes();
    $DaoCiclos= new DaoCiclos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoGrupos= new DaoGrupos();
    $DaoHoras= new DaoHoras();
    
    $ciclo=$DaoCiclos->show($Id_ciclo);
    $_usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-exclamation"></i> Incidencias</h1>
                </div>
                <div class="seccion">
                    <div id="box_uno">
                        <?php
                        if ($Id_docen > 0) {
                            $docen=$DaoDocentes->show($Id_docen);
                            $nombre = $docen->getNombre_docen()." ".$docen->getApellidoP_docen()." ".$docen->getApellidoM_docen();
                        }
                        ?>
                        <ul class="form">
                            <li>Docente<br><input type="text" id="email_int"  onkeyup="buscar()" placeholder="Nombre o clave" value="<?php echo $nombre ?>"/>
                                <ul id="buscador_int"></ul>
                            </li><br>
                            <?php
                            if ($Id_docen> 0) {
                                ?>
                                <li>Ciclo<br><select id="ciclo" onchange="mostrar()">
                                        <option value="0">Selecciona el ciclo</option>
                                        <?php
                                            foreach($DaoCiclos->showAll() as $ciclo){
                                             ?>
                                                <option value="<?php echo $ciclo->getId() ?>" <?php if($Id_ciclo==$ciclo->getId()){?> selected="selected" <?php }?> ><?php echo $ciclo->getClave() ?></option>
                                              <?php
                                              }
                                              ?>
                                        </select>
                                </li> 
                            <?php
                            }
                            ?>
                    </div>
                    <table class="table">
                            <thead>
                                <tr>
                                <td>#</td>
                                <td>Día penalizado</td>
                                <td>Grupo</td>
                                <td>Sesion</td>
                                <td>Usuario</td>
                                <td>Fecha de captura</td>
                                <td>Comentarios</td>
                                <td>Acciones</td>
                                </tr>
                            </thead>
                            <tbody>
                             <?php  
                             $count=1;
                             foreach($DaoPenalizacionDocente->getPenalizacionesDocente($Id_docen,$Id_ciclo) as $pena){
                                 $grupo = $DaoGrupos->show($pena->getGrupo());
                                 $hora=$DaoHoras->show($pena->getHora());
                                 $u = $DaoUsuarios->show($pena->getId_usu_created());
                             ?>
                                    <tr id_jus="<?php echo $pena->getId()?>">
                                        <td><?php echo $count?></td>
                                        <td><?php echo $pena->getFecha()?></td>
                                        <td><?php echo $grupo->getClave()?></td>
                                        <td><?php echo $hora->getTexto_hora()?></td>
                                        <td><?php echo $u->getNombre_usu() . " " . $u->getApellidoP_usu() . " " . $u->getApellidoM_usu()?></td>
                                        <td><?php echo $pena->getDateCreated()?></td>
                                        <td><?php echo $pena->getComentarios()?></td>
                                        <td><button onclick="deletePena(<?php echo $pena->getId()?>)">Eliminar</button></td>
                                    </tr>
                                  <?php
                                  $count++;
                                 }
                             ?>
                            </tbody>
                        </table>
                </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                    <?php
                    require_once '../estandares/menu_derecho.php';
                    ?>
                   <ul>
                        <li><span onclick="mostrar_box()">Capturar incidencia </span></li>
                </ul>
            </div>
        </td>
    </tr>  
    
    <input type="hidden" id="FechaIniCiclo" value="<?php echo $ciclo->getFecha_ini(); ?>"/>
    <input type="hidden" id="FechaFinCiclo" value="<?php echo $ciclo->getFecha_fin(); ?>"/>
    
    <?php
}


if($_POST['action']=="deletePena"){
    $DaoPenalizacionDocente= new DaoPenalizacionDocente();
    $penalizacion=$DaoPenalizacionDocente->show($_POST['Id']);
    
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $docen=$DaoDocentes->show($_POST['Id_docen']);     
    $TextoHistorial="Elimina día penalizado ".$penalizacion->getFecha()." para el docente ".$docen->getNombre_docen()." ".$docen->getApellidoP_docen()." ".$docen->getApellidoM_docen();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Penalizaciones");
    
    $DaoPenalizacionDocente->delete($_POST['Id']);
    update_page($_POST['Id_docen'],$_POST['Id_ciclo']);
}

if($_POST['action']=="mostrar_box"){
    $DaoPenalizacionDocente= new DaoPenalizacionDocente();
    $DaoDocentes= new DaoDocentes();
    $DaoCiclos= new DaoCiclos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoGrupos= new DaoGrupos();
    $DaoHoras= new DaoHoras();
    $DaoHorarioDocente= new DaoHorarioDocente();
    $comentarios="";
    if($_POST['Id_pena']>0){
      $pena=$DaoPenalizacionDocente->show($_POST['Id_pena']);
      $comentarios=$pena->getComentarios();
    }
    ?>
    <div class="box-emergente-form">
      <h1>Incidencia</h1>
      <p>Grupo<br><select id="grupos" onchange="getDiasClase()">
              <option value="0"></option>
              <?php
              foreach($DaoHorarioDocente->getGruposDocenteCiclo($_POST['Id_docen'],$_POST['Id_ciclo']) as $grupo){
                  $g=$DaoGrupos->show($grupo->getId_grupo());
                  ?>
                  <option value="<?php echo $g->getId()?>"><?php echo $g->getClave()?></option>
                  <?php
              }
              ?>
              
          </select>
      </p>
      <p>Fecha<br><input type="text" class="datepicker"  id="fecha-permuta"/></p> 
      <p>Sesion<br> 
            <select id="hora">
                <option value="0"></option>
            <?php
            foreach($DaoHoras->showAll() as $hora){
                    ?>
                    <option value="<?php echo $hora->getId() ?>"> <?php echo $hora->getTexto_hora(); ?></option>
                    <?php   
            }
            ?>
            </select>
        </p>  
      <p>Comentario:<br><textarea id="comentario"><?php echo $comentarios;?></textarea></p>
      <button onclick="add_pena(<?php echo $_POST['Id_pena'];?>)">Guardar</button>
      <button onclick="ocultar_error_layer()">Cancelar</button>
</div>   
<?php
}


if ($_POST['action'] == "getDiasClase") {
    $DaoCiclos = new DaoCiclos();
    $DaoGrupos = new DaoGrupos();

    $resp = $DaoGrupos->getDiasClaseGrupo($_POST['Id_grupo'], $_POST['Id_ciclo']);
    foreach ($resp['dias'] as $dia) {
        ?>
        <input type="hidden" value="<?php echo $dia ?>" class="dias_ciclo"/>
        <?php
    }
}

if ($_POST['action'] == "getHorasClase") {
    $DaoCiclos = new DaoCiclos();
    $DaoGrupos = new DaoGrupos();
    $ciclo = $DaoCiclos->getActual();
    if ($ciclo->getId() > 0) {
        $resp = $DaoGrupos->getDiasClaseGrupo($_POST['Id_grupo'], $ciclo->getId());
        $dia = date("N", strtotime($_POST['Fecha']));
        if ($dia == 1) {
            $letraDia = "L";
        } elseif ($dia == 2) {
            $letraDia = "M";
        }if ($dia == 3) {
            $letraDia = "I";
        }if ($dia == 4) {
            $letraDia = "J";
        }if ($dia == 5) {
            $letraDia = "V";
        }if ($dia == 6) {
            $letraDia = "S";
        }
        ?>
        <option value="0"></option>
        <?php
        foreach ($resp['horas'][$letraDia] as $k => $v) {
            ?>
            <option value="<?php echo $v['Id'] ?>"> <?php echo $v['Hora']; ?></option>
            <?php
        }
    }
}


if($_POST['action']=="add_pena"){

    $DaoUsuarios= new DaoUsuarios();
    $DaoPenalizacionDocente= new DaoPenalizacionDocente();
    $DaoDocentes= new DaoDocentes();
    $DaoCiclos= new DaoCiclos();
    
    $docen=$DaoDocentes->show($_POST['Id_docen']);
    if($_POST['Id_pena']>0){
   
    $pena=$DaoPenalizacionDocente->show($_POST['Id_pena']);
    $TextoHistorial="Edita fecha de penalización para el docente ".$docen->getNombre_docen()." ".$docen->getApellidoP_docen()." ".$docen->getApellidoM_docen()." de ".$pena->getFecha()." a ".$_POST['Fecha'];
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Penalizaciones");

    }else{
        $PenalizacionDocente= new PenalizacionDocente();
        $PenalizacionDocente->setComentarios($_POST['Comentarios']);
        $PenalizacionDocente->setDateCreated(date('Y-m-d H:i:s'));
        $PenalizacionDocente->setFecha($_POST['Fecha']);
        $PenalizacionDocente->setGrupo($_POST['Id_grupo']);
        $PenalizacionDocente->setHora($_POST['Hora']);
        $PenalizacionDocente->setId_ciclo($_POST['Id_ciclo']);
        $PenalizacionDocente->setId_docen($_POST['Id_docen']);
        $PenalizacionDocente->setId_usu_created($_COOKIE['admin/Id_usu']);
        
        $DaoPenalizacionDocente->add($PenalizacionDocente);
    
        $TextoHistorial="Añade penalización para el docente ".$docen->getNombre_docen()." ".$docen->getApellidoP_docen()." ".$docen->getApellidoM_docen().", día ".$_POST['Fecha'];
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Penalizaciones");
    }
    update_page($_POST['Id_docen'],$_POST['Id_ciclo']);
     
}