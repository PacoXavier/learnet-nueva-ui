<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 


if ($_POST['action'] == "mostrar_horario") {
   update_page($_POST['Id_ofe_alum'],$_POST['Id_ciclo']);
}

//$Id_grupo adaptarlo
function update_page($Id_ofe_alum,$Id_ciclo) {
    $DaoHoras= new DaoHoras();
    $DaoUsuarios= new DaoUsuarios();
    $DaoDiasPlantel= new DaoDiasPlantel();
    $base= new base();
    
    $paramDias= array();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $query  = "SELECT * FROM Horas ORDER BY Id_hora ASC";           
    foreach($base->advanced_query($query) as $row_Horas){

       foreach($DaoDiasPlantel->getJoinDiasPlantel($usu->getId_plantel()) as $dia){
                $paramDias[$dia['Id_dia']]['disp_docente']="";
                $paramDias[$dia['Id_dia']]['nombre_materia']="";
                $paramDias[$dia['Id_dia']]['nombre_docen']=""; 
                $paramDias[$dia['Id_dia']]['Id_grupo']="";
                $paramDias[$dia['Id_dia']]['Id_mat']="";
                $paramDias[$dia['Id_dia']]['Id_aula']="";
                $paramDias[$dia['Id_dia']]['clave']="";  
                $paramDias[$dia['Id_dia']]['nombre_aula']=""; 
        }
                    
        $query="SELECT ciclos_alum_ulm.*
                            FROM ofertas_alumno
                            JOIN (SELECT Horario_docente.*,
                                   Grupos.Clave,Grupos.Id_mat,
                                   materias_uml.Nombre,
                                   Docentes.Nombre_docen,Docentes.ApellidoP_docen,Docentes.ApellidoM_docen,
                                   aulas.Nombre_aula,
                                   ciclos_alum_ulm.Id_ofe_alum
                                   FROM ciclos_alum_ulm 
                                     JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum 
                                     JOIN Grupos ON materias_ciclo_ulm.Id_grupo=Grupos.Id_grupo 
                                     JOIN materias_uml ON Grupos.Id_mat= materias_uml.Id_mat
                                     JOIN Horario_docente ON Grupos.Id_grupo=Horario_docente.Id_grupo
                                     JOIN Docentes ON Horario_docente.Id_docente= Docentes.Id_docen
                                     JOIN aulas ON aulas.Id_aula= Horario_docente.Aula
                                  WHERE ciclos_alum_ulm.Id_ciclo=".$Id_ciclo." 
                                        AND materias_ciclo_ulm.Id_grupo IS NOT NULL 
                                        AND Hora=".$row_Horas['Id_hora']."
                                  ) as ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum

                            WHERE ofertas_alumno.Id_ofe_alum=".$Id_ofe_alum;
           foreach($base->advanced_query($query) as $row_Horario_docente){

                if($row_Horario_docente['Lunes']==1){ 
                    $paramDias[1]['disp_docente']='class="horas_docente_asignadas"'; 
                    $paramDias[1]['nombre_materia']=$row_Horario_docente['Nombre'];
                    $paramDias[1]['nombre_docen']=$row_Horario_docente['Nombre_docen'];
                    $paramDias[1]['Id_grupo']=$row_Horario_docente['Id_grupo'];
                    $paramDias[1]['Id_mat']=$row_Horario_docente['Id_mat'];
                    $paramDias[1]['Id_aula']=$row_Horario_docente['Aula'];
                    $paramDias[1]['clave']=$row_Horario_docente['Clave'];  
                    $paramDias[1]['nombre_aula']=$row_Horario_docente['Nombre_aula']; 
                }

              if($row_Horario_docente['Martes']==1){ 
                    $paramDias[2]['disp_docente']='class="horas_docente_asignadas"'; 
                    $paramDias[2]['nombre_materia']=$row_Horario_docente['Nombre'];
                    $paramDias[2]['nombre_docen']=$row_Horario_docente['Nombre_docen'];
                    $paramDias[2]['Id_grupo']=$row_Horario_docente['Id_grupo'];
                    $paramDias[2]['Id_mat']=$row_Horario_docente['Id_mat'];
                    $paramDias[2]['Id_aula']=$row_Horario_docente['Aula'];
                    $paramDias[2]['clave']=$row_Horario_docente['Clave'];  
                    $paramDias[2]['nombre_aula']=$row_Horario_docente['Nombre_aula']; 
              }
              if($row_Horario_docente['Miercoles']==1){ 
                    $paramDias[3]['disp_docente']='class="horas_docente_asignadas"'; 
                    $paramDias[3]['nombre_materia']=$row_Horario_docente['Nombre'];
                    $paramDias[3]['nombre_docen']=$row_Horario_docente['Nombre_docen'];
                    $paramDias[3]['Id_grupo']=$row_Horario_docente['Id_grupo'];
                    $paramDias[3]['Id_mat']=$row_Horario_docente['Id_mat'];
                    $paramDias[3]['Id_aula']=$row_Horario_docente['Aula'];
                    $paramDias[3]['clave']=$row_Horario_docente['Clave'];  
                    $paramDias[3]['nombre_aula']=$row_Horario_docente['Nombre_aula'];  
              }
              if($row_Horario_docente['Jueves']==1){ 
                    $paramDias[4]['disp_docente']='class="horas_docente_asignadas"'; 
                    $paramDias[4]['nombre_materia']=$row_Horario_docente['Nombre'];
                    $paramDias[4]['nombre_docen']=$row_Horario_docente['Nombre_docen'];
                    $paramDias[4]['Id_grupo']=$row_Horario_docente['Id_grupo'];
                    $paramDias[4]['Id_mat']=$row_Horario_docente['Id_mat'];
                    $paramDias[4]['Id_aula']=$row_Horario_docente['Aula'];
                    $paramDias[4]['clave']=$row_Horario_docente['Clave'];  
                    $paramDias[4]['nombre_aula']=$row_Horario_docente['Nombre_aula']; 
              }
              if($row_Horario_docente['Viernes']==1){ 
                    $paramDias[5]['disp_docente']='class="horas_docente_asignadas"'; 
                    $paramDias[5]['nombre_materia']=$row_Horario_docente['Nombre'];
                    $paramDias[5]['nombre_docen']=$row_Horario_docente['Nombre_docen'];
                    $paramDias[5]['Id_grupo']=$row_Horario_docente['Id_grupo'];
                    $paramDias[5]['Id_mat']=$row_Horario_docente['Id_mat'];
                    $paramDias[5]['Id_aula']=$row_Horario_docente['Aula'];
                    $paramDias[5]['clave']=$row_Horario_docente['Clave'];  
                    $paramDias[5]['nombre_aula']=$row_Horario_docente['Nombre_aula']; 
              }
              if($row_Horario_docente['Sabado']==1){ 
                    $paramDias[6]['disp_docente']='class="horas_docente_asignadas"'; 
                    $paramDias[6]['nombre_materia']=$row_Horario_docente['Nombre'];
                    $paramDias[6]['nombre_docen']=$row_Horario_docente['Nombre_docen'];
                    $paramDias[6]['Id_grupo']=$row_Horario_docente['Id_grupo'];
                    $paramDias[6]['Id_mat']=$row_Horario_docente['Id_mat'];
                    $paramDias[6]['Id_aula']=$row_Horario_docente['Aula'];
                    $paramDias[6]['clave']=$row_Horario_docente['Clave'];  
                    $paramDias[6]['nombre_aula']=$row_Horario_docente['Nombre_aula'];  
              }
              
              if($row_Horario_docente['Domingo']==1){ 
                    $paramDias[7]['disp_docente']='class="horas_docente_asignadas"'; 
                    $paramDias[7]['nombre_materia']=$row_Horario_docente['Nombre'];
                    $paramDias[7]['nombre_docen']=$row_Horario_docente['Nombre_docen'];
                    $paramDias[7]['Id_grupo']=$row_Horario_docente['Id_grupo'];
                    $paramDias[7]['Id_mat']=$row_Horario_docente['Id_mat'];
                    $paramDias[7]['Id_aula']=$row_Horario_docente['Aula'];
                    $paramDias[7]['clave']=$row_Horario_docente['Clave'];  
                    $paramDias[7]['nombre_aula']=$row_Horario_docente['Nombre_aula']; 
              }

          }
           
     ?>
              <tr id_hora="<?php echo $row_Horas['Id_hora']?>">
                    <td><?php echo $row_Horas['Texto_hora']?></td>
                    <?php
                    foreach($DaoDiasPlantel->getJoinDiasPlantel($usu->getId_plantel()) as $dia){
                        $id=$dia['Id_dia'];
                        $onclick="";
                        if(isset($paramDias[$id]['Id_grupo']) && $paramDias[$id]['Id_grupo']>0){
                            $onclick='onclick="mostrarCalificacionesGrupo('.$paramDias[$id]["Id_grupo"].')"'; 
                        }
                        ?>
                          <td <?php echo $paramDias[$id]['disp_docente']?> <?php echo $onclick;?>>
                              <span><?php echo $paramDias[$id]['nombre_materia']?></span>
                              <br><?php echo $paramDias[$id]['nombre_aula']?><br><span>
                                  <?php echo $paramDias[$id]['clave']?></span><br>
                                  <?php echo $paramDias[$id]['nombre_docen'];?></td>
                        <?php
                    }
                    ?>
              </tr>
            <?php
     }
}



if ($_POST['action'] == "buscarAlum") {
    $DaoAlumnos = new DaoAlumnos();
    foreach ($DaoAlumnos->buscarAlumno($_POST['buscar']) as $k => $v) {
        ?>
        <li id_alum="<?php echo $v->getId(); ?>" onclick="getOfertas(<?php echo $v->getId(); ?>)"><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li>
        <?php
    }
}


if ($_POST['action'] == "getOfertas") {
    ?>
    <option value="0"></option>
    <?php
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    foreach ($DaoOfertasAlumno->getOfertasAlumnoAltasYBajas($_POST['Id_alum']) as $OfertaAlumno) {
        $nombre_ori = "";
        $oferta = $DaoOfertas->show($OfertaAlumno->getId_ofe());
        $esp = $DaoEspecialidades->show($OfertaAlumno->getId_esp());
        if ($OfertaAlumno->getId_ori() > 0) {
            $ori = $DaoOrientaciones->show($OfertaAlumno->getId_ori());
            $nombre_ori = $ori->getNombre();
        }
        ?>
        <option value="<?php echo $OfertaAlumno->getId() ?>"><?php echo $oferta->getNombre_oferta() . " " . $esp->getNombre_esp() . " " . $nombre_ori; ?></option>
        <?php
    }
}
