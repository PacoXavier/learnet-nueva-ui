<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="buscar_int"){
    $DaoDocentes= new DaoDocentes();
    foreach($DaoDocentes->buscarDocente($_POST['buscar']) as $k=>$v){
        ?>
           <a href="evaluaciones_docente.php?id=<?php echo $v->getId()?>">
                <li><?php echo $v->getNombre_docen()." ".$v->getApellidoP_docen()." ".$v->getApellidoM_docen()?></li>
           </a>
      <?php
    }
  
}

if($_POST['action']=="delete_evaluacion"){
    $DaoCategoriasPago= new DaoCategoriasPago();
    $DaoDocentes= new DaoDocentes();
    $DaoCiclos= new DaoCiclos();
    $DaoEvaluacionDocente= new DaoEvaluacionDocente();
    $DaoPuntosPorCategoriaEvaluadaDocente= new DaoPuntosPorCategoriaEvaluadaDocente();
    $DaoNivelesPago= new DaoNivelesPago();
    $DaoResultadosEvaluacion= new DaoResultadosEvaluacion();
    $Id_docente=0;

    $DaoEvaluacionDocente->delete($_POST['Id_eva']);
    $DaoPuntosPorCategoriaEvaluadaDocente->deletePorEvaluacion($_POST['Id_eva']);
    $DaoResultadosEvaluacion->deleteCamposPorEvaluacion($_POST['Id_eva']);

    $count = 1;
    $docente = $DaoDocentes->show($_POST['Id_docente']);
    $nombre=$docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . " " . $docente->getApellidoM_docen();
    foreach ($DaoEvaluacionDocente->getEvaluacionesDocentes($docente->getId()) as $eva) {
        $ciclo = $DaoCiclos->show($eva->getId_ciclo());
        ?>
        <tr>
            <td><?php echo $count ?></td>
            <td><?php echo $ciclo->getClave() ?></td>
            <td><?php echo $eva->getDateCreated(); ?></td>
            <td><?php echo $nombre ?></td>
            <td><?php echo $eva->getPuntosTotales(); ?></td>
            <?php
            foreach ($DaoCategoriasPago->getCategoriasPago() as $cat) {
                $resul=$DaoPuntosPorCategoriaEvaluadaDocente->getNivelPorCategoria($eva->getId(),$cat->getId());
                $nombreNIvel="";
                if($resul->getId()>0){
                   $nivel=$DaoNivelesPago->show($resul->getId_nivel());
                   $nombreNIvel=$nivel->getNombre_nivel();
                }
               ?>
                 <td><?php echo $nombreNIvel;?></td>
               <?php
            }
            ?>
            <td><button onclick="editar_evaluacion(<?php echo $eva->getId(); ?>)">Editar</button><button onclick="delete_evaluacion(<?php echo $eva->getId() ?>)">Eliminar</button></td>
        </tr>
        <?php
        $count++;
    }
}
