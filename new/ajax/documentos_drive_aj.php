<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="getAccessToken"){
    //Cuando se usa el api de javascript no se pone redire_uri en google console,solo es para el php 
    $base=new base();
    $DaoDocumentosDrive= new DaoDocumentosDrive();
    $DaoPlanteles= new DaoPlanteles();
    $DaoUsuarios= new DaoUsuarios();
    $DaoParametros= new DaoParametros();
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $plantel=$DaoPlanteles->show($usu->getId_plantel());
    
    //Parametros del plantel configurados
    $params=$DaoParametrosPlantel->getParametrosPlantelArray();
    if ($DaoParametrosPlantel->checkGoogleTokenDrivePlantel($params) !== true) {
       $params = $DaoParametrosPlantel->getParametrosPlantelArray();
    }

    if(strlen($_POST['code'])>0){
        $url="https://www.googleapis.com/oauth2/v4/token";
        $data="code=".$_POST['code']."&client_id=".$params['ClientIdGoogle']."&client_secret=".$params['ClientSecretGoogle']."&redirect_uri=postmessage&grant_type=authorization_code";

        $cc = new cURL(); 
        $return= $cc->post($url,$data); 
        $return=substr($return,strpos($return, "{"));

        $resultado=json_decode($return,true);
        $access_token=$resultado["access_token"];
        $refresh_token=$resultado["refresh_token"];
        
        //Verificamos si existe el parametro en el plantel
        //22=AccessTokenGoogleDrive
        $ParametrosPlantel=$DaoParametrosPlantel->existeParametro(22);
        if($ParametrosPlantel->getId()>0){
            $ParametrosPlantel->setValor($access_token);
            $DaoParametrosPlantel->update($ParametrosPlantel);
        }else{
            $ParametrosPlantel= new ParametrosPlantel();
            $ParametrosPlantel->setId_param(22);
            $ParametrosPlantel->setId_plantel($usu->getId_plantel());
            $ParametrosPlantel->setValor($access_token);
            $DaoParametrosPlantel->add($ParametrosPlantel);
        } 
        
        //Si existe el refresh lo insertamos
        if(strlen($refresh_token)>0){
            //Verificamos si existe el parametro en el plantel
            //23=RefreshTokenGoogleDrive
            $ParametrosPlantel=$DaoParametrosPlantel->existeParametro(23);
            if($ParametrosPlantel->getId()>0){
                $ParametrosPlantel->setValor($refresh_token);
                $DaoParametrosPlantel->update($ParametrosPlantel);
            }else{
                $ParametrosPlantel= new ParametrosPlantel();
                $ParametrosPlantel->setId_param(23);
                $ParametrosPlantel->setId_plantel($usu->getId_plantel());
                $ParametrosPlantel->setValor($refresh_token);
                $DaoParametrosPlantel->add($ParametrosPlantel);
            } 
        
            $params=$DaoParametrosPlantel->getParametrosPlantelArray();
            //Creamos carpeta de documentos ulm
            $head=array();
            $head[0]="Authorization: Bearer ".$params['AccessTokenGoogleDrive'];
            $head[1]="Content-Type: application/json";
            $head[2]="X-JavaScript-User-Agent: Google APIs Explorer";

            $data= array();
            $data['title']="Documentos ".$plantel->getAbreviatura();
            $data['mimeType']="application/vnd.google-apps.folder";

            $cc = new cURL($head);
            $url="https://www.googleapis.com/drive/v2/files";
            $return =$cc->post($url,json_encode($data));
            $return = substr($return, strpos($return, "{"));
            $return = substr($return, 0, strripos($return, "}") + 1);
            $return = json_decode($return, true);

            $idCarpetaPrincipal=$return['id'];
            $urlDive=$return['alternateLink'];
            
            $DocumentosDrive= new DocumentosDrive();
            $DocumentosDrive->setNombre("Documentos ".$plantel->getAbreviatura());
            $DocumentosDrive->setTipoDoc("Carpeta");
            $DocumentosDrive->setTipoUsuario("");
            $DocumentosDrive->setUID_drive($idCarpetaPrincipal);
            $DocumentosDrive->setURL($urlDive);
            $DocumentosDrive->setDateCreated(date('Y-m-d H:i:s'));
            $DocumentosDrive->setId_plantel($usu->getId_plantel());
            $DocumentosDrive->setId_usu($usu->getId());
            $DocumentosDrive->setId_parent(0);
            $IdFolder=$DaoDocumentosDrive->add($DocumentosDrive);
            
            $data= array();
            $data['role']="reader";
            $data['type']="anyone";

            $cc = new cURL($head);
            $url="https://www.googleapis.com/drive/v2/files/".$idCarpetaPrincipal."/permissions";
            $return =$cc->post($url,json_encode($data));
            $return = substr($return, strpos($return, "{"));
            $return = substr($return, 0, strripos($return, "}") + 1);
            $return = json_decode($return, true);
            
            /************************************************************************/
            
            //Carpeta alumnos
            $data= array();
            $data['title']="1-Alumnos";
            $data['parents']=array();
            $data['parents'][0]['id']=$idCarpetaPrincipal;
            $data['mimeType']="application/vnd.google-apps.folder";

            $cc = new cURL($head);
            $url="https://www.googleapis.com/drive/v2/files";
            $return =$cc->post($url,json_encode($data));
            $return = substr($return, strpos($return, "{"));
            $return = substr($return, 0, strripos($return, "}") + 1);
            $return = json_decode($return, true);
            $id=$return['id'];
            $url=$return['alternateLink'];
            
            $DocumentosDrive= new DocumentosDrive();
            $DocumentosDrive->setNombre("1-Alumnos");
            $DocumentosDrive->setTipoDoc("Carpeta");
            $DocumentosDrive->setTipoUsuario("alumno");
            $DocumentosDrive->setUID_drive($id);
            $DocumentosDrive->setURL($url);
            $DocumentosDrive->setDateCreated(date('Y-m-d H:i:s'));
            $DocumentosDrive->setId_plantel($usu->getId_plantel());
            $DocumentosDrive->setId_usu($usu->getId());
            $DocumentosDrive->setId_parent($IdFolder);
            $DaoDocumentosDrive->add($DocumentosDrive);
            
            $data= array();
            $data['role']="reader";
            $data['type']="anyone";

            $cc = new cURL($head);
            $url="https://www.googleapis.com/drive/v2/files/".$id."/permissions";
            $return =$cc->post($url,json_encode($data));
            $return = substr($return, strpos($return, "{"));
            $return = substr($return, 0, strripos($return, "}") + 1);
            $return = json_decode($return, true);
            
            /************************************************************************/

            //Carpeta docentes
            $data= array();
            $data['title']="2-Docentes";
            $data['parents']=array();
            $data['parents'][0]['id']=$idCarpetaPrincipal;
            $data['mimeType']="application/vnd.google-apps.folder";

            $cc = new cURL($head);
            $url="https://www.googleapis.com/drive/v2/files";
            $return =$cc->post($url,json_encode($data));
            $return = substr($return, strpos($return, "{"));
            $return = substr($return, 0, strripos($return, "}") + 1);
            $return = json_decode($return, true);
            $id=$return['id'];
            $url=$return['alternateLink'];
            
            $DocumentosDrive= new DocumentosDrive();
            $DocumentosDrive->setNombre("2-Docentes");
            $DocumentosDrive->setTipoDoc("Carpeta");
            $DocumentosDrive->setTipoUsuario("docente");
            $DocumentosDrive->setUID_drive($id);
            $DocumentosDrive->setURL($url);
            $DocumentosDrive->setDateCreated(date('Y-m-d H:i:s'));
            $DocumentosDrive->setId_plantel($usu->getId_plantel());
            $DocumentosDrive->setId_usu($usu->getId());
            $DocumentosDrive->setId_parent($IdFolder);
            $DaoDocumentosDrive->add($DocumentosDrive);
            
            $data= array();
            $data['role']="reader";
            $data['type']="anyone";

            $cc = new cURL($head);
            $url="https://www.googleapis.com/drive/v2/files/".$id."/permissions";
            $return =$cc->post($url,json_encode($data));
            $return = substr($return, strpos($return, "{"));
            $return = substr($return, 0, strripos($return, "}") + 1);
            $return = json_decode($return, true);
            /************************************************************************/

            //Carpeta usuarios
            $data= array();
            $data['title']="3-Usuarios";
            $data['parents']=array();
            $data['parents'][0]['id']=$idCarpetaPrincipal;
            $data['mimeType']="application/vnd.google-apps.folder";

            $cc = new cURL($head);
            $url="https://www.googleapis.com/drive/v2/files";
            $return =$cc->post($url,json_encode($data));
            $return = substr($return, strpos($return, "{"));
            $return = substr($return, 0, strripos($return, "}") + 1);
            $return = json_decode($return, true);
            $id=$return['id'];
            $url=$return['alternateLink'];
            
            $DocumentosDrive= new DocumentosDrive();
            $DocumentosDrive->setNombre("3-Usuarios");
            $DocumentosDrive->setTipoDoc("Carpeta");
            $DocumentosDrive->setTipoUsuario("usuario");
            $DocumentosDrive->setUID_drive($id);
            $DocumentosDrive->setURL($url);
            $DocumentosDrive->setDateCreated(date('Y-m-d H:i:s'));
            $DocumentosDrive->setId_plantel($usu->getId_plantel());
            $DocumentosDrive->setId_usu($usu->getId());
            $DocumentosDrive->setId_parent($IdFolder);
            $DaoDocumentosDrive->add($DocumentosDrive);
            
            $data= array();
            $data['role']="reader";
            $data['type']="anyone";

            $cc = new cURL($head);
            $url="https://www.googleapis.com/drive/v2/files/".$id."/permissions";
            $return =$cc->post($url,json_encode($data));
            $return = substr($return, strpos($return, "{"));
            $return = substr($return, 0, strripos($return, "}") + 1);
            $return = json_decode($return, true);
        }
    }   
}


if($_POST['action']=="desconectar"){
    //Parametros del plantel configurados
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $params=$DaoParametrosPlantel->getParametrosPlantelArray();
    if ($DaoParametrosPlantel->checkGoogleTokenDrivePlantel($params) !== true) {
       $params = $DaoParametrosPlantel->getParametrosPlantelArray();
    }
    if(isset($params['AccessTokenGoogleDrive'])){
        $cc = new cURL(); 
        $url="https://accounts.google.com/o/oauth2/revoke?token=".$params['AccessTokenGoogleDrive'];
        $return =$cc->get($url);

        //Verificamos si existe el parametro en el plantel
        //22=AccessTokenGoogleDrive
        $ParametrosPlantel=$DaoParametrosPlantel->existeParametro(22);
        if($ParametrosPlantel->getId()>0){
            $ParametrosPlantel->setValor('');
            $DaoParametrosPlantel->update($ParametrosPlantel);
        }

        //23=RefreshTokenGoogleDrive
        $ParametrosPlantel=$DaoParametrosPlantel->existeParametro(23);
        if($ParametrosPlantel->getId()>0){
            $ParametrosPlantel->setValor('');
            $DaoParametrosPlantel->update($ParametrosPlantel);
        }
        
        $DaoDocumentosDrive= new DaoDocumentosDrive();
        foreach($DaoDocumentosDrive->getDocumentosPlantel() as $file){
            $DaoDocumentosDrive->delete($file->getId());
        }
    }
}


if($_POST['action']=="mostrarBoxNuevo"){
?>
<div class="box-emergente-form">
	<h1>Nuevo documento</h1>
        <p>Nombre<br><input type="text" id="nombre"/></p>
        <p>Tipo de documento<br>
            <select id="tipo-doc">
                  <option value="0"></option>
                  <option value="1">Carpeta</option>
                  <option value="2">Documento</option>
                  <option value="3">Hoja de cálculo</option>
                  <option value="4">Presentación</option>
            </select>
        </p>
        <p><button onclick="saveDocumento(this)">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
</div>
<?php
}


if($_POST['action']=="saveDocumento"){
    
    $DaoUsuarios= new DaoUsuarios();
    $DaoParametros= new DaoParametros();
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $DaoDocumentosDrive= new DaoDocumentosDrive();
    $params=$DaoParametrosPlantel->getParametrosPlantelArray();
    if ($DaoParametrosPlantel->checkGoogleTokenDrivePlantel($params) !== true) {
       $params = $DaoParametrosPlantel->getParametrosPlantelArray();
    }
    
    $folderParent=$DaoDocumentosDrive->show($_POST['id_parent']);
    $tipoFIle="";
    if($_POST['tipoDoc']==1){
       $tipoFIle="application/vnd.google-apps.folder"; 
       $tipoFIleBdd="Carpeta";
    }elseif($_POST['tipoDoc']==2){
      $tipoFIle="application/vnd.google-apps.document";   
      $tipoFIleBdd="Documento";
    }elseif($_POST['tipoDoc']==3){
      $tipoFIle="application/vnd.google-apps.spreadsheet";   
      $tipoFIleBdd="Excel";
    }elseif($_POST['tipoDoc']==4){
      $tipoFIle="application/vnd.google-apps.presentation"; 
      $tipoFIleBdd="PowerPoint";
    }
    //Nuevo documento
    $data= array();
    $data['title']=$_POST['nombre'];
    $data['parents']=array();
    $data['parents'][0]['id']=$folderParent->getUID_drive();
    $data['mimeType']=$tipoFIle;

    $head=array();
    $head[0]="Authorization: Bearer ".$params['AccessTokenGoogleDrive'];
    $head[1]="Content-Type: application/json";
    $head[2]="X-JavaScript-User-Agent: Google APIs Explorer";
            
    $cc = new cURL($head);
    $url="https://www.googleapis.com/drive/v2/files";
    $return =$cc->post($url,json_encode($data));
    $return = substr($return, strpos($return, "{"));
    $return = substr($return, 0, strripos($return, "}") + 1);
    $return = json_decode($return, true);
    $id=$return['id'];
    $url=$return['alternateLink'];
    
    $DocumentosDrive= new DocumentosDrive();
    $DocumentosDrive->setNombre($_POST['nombre']);
    $DocumentosDrive->setTipoDoc($tipoFIleBdd);
    $DocumentosDrive->setTipoUsuario($_POST['tipo_usu']);
    $DocumentosDrive->setUID_drive($id);
    $DocumentosDrive->setURL($url);
    $DocumentosDrive->setDateCreated(date('Y-m-d H:i:s'));
    $DocumentosDrive->setId_plantel($usu->getId_plantel());
    $DocumentosDrive->setId_usu($usu->getId());
    $DocumentosDrive->setId_parent($folderParent->getId());
    $DaoDocumentosDrive->add($DocumentosDrive);
}


if($_POST['action']=="editNameFile"){
 
    $DaoParametrosPlantel = new DaoParametrosPlantel();
    $params = $DaoParametrosPlantel->getParametrosPlantelArray();

    $base = new base();
    if ($DaoParametrosPlantel->checkGoogleTokenDrivePlantel($params) !== true) {
        $params = $DaoParametrosPlantel->getParametrosPlantelArray();
    }

    $head = array();
    $head[0] = "Authorization: Bearer " . $params['AccessTokenGoogleDrive'];
    $head[1] = "Content-Type: application/json";

    $data = array();
    $data['id'] = $_POST['Id_file'];
    $data['title'] = $_POST['Name'];

    $cod= array();
    array_push($cod, json_encode($data));
    //http://tools.ietf.org/html/rfc6902

    $url = "https://www.googleapis.com/drive/v2/files/".$_POST['Id_file'];
    $user_info = $base->simpleCurl("PUT", $head, $url, $cod);
    $user_info = json_decode($user_info,true);

    if(strlen($user_info['id'])>0){
        $DaoDocumentosDrive=new DaoDocumentosDrive();
        $file=$DaoDocumentosDrive->showByKey($_POST['Id_file']);
        $file->setNombre($_POST['Name']);
        $DaoDocumentosDrive->update($file);
    }
}



if ($_GET['action'] == "uploadFile") {

    if (count($_FILES) > 0) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {

            $mimeType=$_FILES["file"]['type'];
            $NameFile=$_FILES["file"]["name"];
            move_uploaded_file($_FILES["file"]["tmp_name"], "files/documentoDrive");
            $tipo_usu=$_FILES["file"]["tipo_usu"];
            $id_parent=$_FILES["file"]["id_parent"];
            
            
        }

    } elseif (isset($_GET['action'])) {

        if ($_FILES["file"]["error"] > 0) {

            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {

           if (isset($_GET['base64'])) {
                // If the browser does not support sendAsBinary ()
                $content = base64_decode(file_get_contents('php://input'));
            } else {
                $content = file_get_contents('php://input');
            }

            $mimeType=$_GET["TypeFile"];
            $NameFile=$_GET["FileName"];
            file_put_contents('files/documentoDrive', $content);
            $tipo_usu=$_GET["tipo_usu"];
            $id_parent=$_GET["id_parent"];
        }
    }
    
    
    $DaoUsuarios= new DaoUsuarios();
    $DaoParametros= new DaoParametros();
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $DaoDocumentosDrive= new DaoDocumentosDrive();
    $params=$DaoParametrosPlantel->getParametrosPlantelArray();
    if ($DaoParametrosPlantel->checkGoogleTokenDrivePlantel($params) !== true) {
       $params = $DaoParametrosPlantel->getParametrosPlantelArray();
    }
    $folderParent=$DaoDocumentosDrive->show($id_parent);
    
   //Ruta temporal del archivo
   $ruta="files/documentoDrive";
   $contenidoFile=file_get_contents($ruta);  

    //$data['convert']=true;
    $dataFile=array();
    $dataFile['title']=$NameFile;
    $dataFile['parents']=array();
    $dataFile['parents'][0]['id']=$folderParent->getUID_drive();

    $boundary="foo_bar_baz";
    $related = "--$boundary\r\n";
    $related .="Content-Type: application/json; charset=UTF-8\r\n";
    $related .="\r\n" . json_encode($dataFile) . "\r\n";
    $related .= "--$boundary\r\n";
    $related .= "Content-Type: $mimeType\r\n";
    $related .= "Content-Transfer-Encoding: base64\r\n";
    $related .= "\r\n" . base64_encode($contenidoFile) . "\r\n";
    $related .= "--$boundary--";
    $postBody = $related;
    
    $head=array();
    $head[0]="Authorization: Bearer ".$params['AccessTokenGoogleDrive'];
    $head[1]='Content-Type: multipart/related; boundary='.$boundary;
    $head[2]="Content-Length: ".strlen($postBody);
     
    $cc = new cURL($head);
    $url="https://www.googleapis.com/upload/drive/v2/files?uploadType=multipart";
    
    $return =$cc->post($url, $postBody);
    $return = substr($return, strpos($return, "{"));
    $return = substr($return, 0, strripos($return, "}") + 1);
    $return = json_decode($return, true);
    $id=$return['id'];
    $url=$return['alternateLink'];
    
    if(strlen($url)>0){
        $tipoFIleBdd="";
        if($return['mimeType']=="application/excel"){
          $tipoFIleBdd="Excel";
        }elseif($return['mimeType']=="application/x-mspowerpoint" || $return['mimeType']=="application/powerpoint"){
          $tipoFIleBdd="PowerPoint";  
        }elseif($return['mimeType']=="application/msword"){
          $tipoFIleBdd="Documento";  
        }elseif($return['mimeType']=="application/zip" || $return['mimeType']=="multipart/x-zip" || $return['mimeType']=="application/x-compressed"){
          $tipoFIleBdd="Zip";  
        }else{
          $tipoFIleBdd="Desconocido";  
        }
        
        //Nuevo documento en la bdd
        $DocumentosDrive= new DocumentosDrive();
        $DocumentosDrive->setNombre($NameFile);
        $DocumentosDrive->setTipoDoc($tipoFIleBdd);
        $DocumentosDrive->setTipoUsuario($tipo_usu);
        $DocumentosDrive->setUID_drive($id);
        $DocumentosDrive->setURL($url);
        $DocumentosDrive->setDateCreated(date('Y-m-d H:i:s'));
        $DocumentosDrive->setId_plantel($usu->getId_plantel());
        $DocumentosDrive->setId_usu($usu->getId());
        $DocumentosDrive->setId_parent($folderParent->getId());
        $DaoDocumentosDrive->add($DocumentosDrive);
    }

    //Eliminamos el archivo temporal
    unlink($ruta);
}

if($_POST['action']=="deleteFile"){
    $DaoParametrosPlantel = new DaoParametrosPlantel();
    $params = $DaoParametrosPlantel->getParametrosPlantelArray();

    $base = new base();
    if ($DaoParametrosPlantel->checkGoogleTokenDrivePlantel($params) !== true) {
        $params = $DaoParametrosPlantel->getParametrosPlantelArray();
    }
    $DaoDocumentosDrive=new DaoDocumentosDrive();
    $file=$DaoDocumentosDrive->show($_POST['Id_file']);
    
    $head = array();
    $head[0] = "Authorization: Bearer " . $params['AccessTokenGoogleDrive'];
    $head[1] = "Content-Type: application/json";

    $data = array();
    $url = "https://www.googleapis.com/drive/v2/files/".$file->getUID_drive();
    $user_info = $base->simpleCurl("DELETE", $head, $url, $cod);
    $user_info = json_decode($user_info,true);
    $DaoDocumentosDrive->delete($file->getId());
}


/*
POST /feeds/api/users/default/uploads HTTP/1.1
Host: uploads.gdata.youtube.com
Authorization: Bearer ACCESS_TOKEN
GData-Version: 2
X-GData-Key: key=adf15ee97731bca89da876c...a8dc
Slug: video-test.mp4
Content-Type: multipart/related; boundary="f93dcbA3"
Content-Length: 1941255
Connection: close

--f93dcbA3
Content-Type: application/atom+xml; charset=UTF-8

<?xml version="1.0"?>
<entry xmlns="http://www.w3.org/2005/Atom"
  xmlns:media="http://search.yahoo.com/mrss/"
  xmlns:yt="http://gdata.youtube.com/schemas/2007">
  <media:group>
    <media:title type="plain">Bad Wedding Toast</media:title>
    <media:description type="plain">
      I gave a bad toast at my friend's wedding.
    </media:description>
    <media:category
      scheme="http://gdata.youtube.com/schemas/2007/categories.cat">People
    </media:category>
    <media:keywords>toast, wedding</media:keywords>
  </media:group>
</entry>
--f93dcbA3
Content-Type: video/mp4
Content-Transfer-Encoding: binary

<Binary File Data>
--f93dcbA3--
 * 
 * 
 * 
POST /feeds/api/users/default/uploads HTTP/1.1
Host: uploads.gdata.youtube.com
Authorization: Bearer ACCESS_TOKEN
GData-Version: 2
X-GData-Key: key=DEVELOPER_KEY
Slug: VIDEO_FILENAME
Content-Type: multipart/related; boundary="BOUNDARY_STRING"
Content-Length: CONTENT_LENGTH
Connection: close

--<boundary_string>
Content-Type: application/atom+xml; charset=UTF-8

API_XML_request
--BOUNDARY_STRING
Content-Type: VIDEO_CONTENT_TYPE
Content-Transfer-Encoding: binary

<Binary File Data>
--BOUNDARY_STRING--
*/