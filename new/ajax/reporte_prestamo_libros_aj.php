<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if ($_POST['action'] == "filtro") {
    $base = new base();
    $DaoLibros = new DaoLibros();
    $DaoHistorialPrestamoLibro = new DaoHistorialPrestamoLibro();
    $DaoAlumnos = new DaoAlumnos();
    $DaoDocentes = new DaoDocentes();
    $DaoUsuarios = new DaoUsuarios();
    $DaoCiclos = new DaoCiclos();
    $ciclo = $DaoCiclos->getActual();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $query = "";

    if ($_POST['Id_libro'] > 0) {
        $query =" AND Id_libro=" . $_POST['Id_libro'];
    }
    
    
    if ($_POST['Id_usuario'] > 0) {
        $query = $query ." AND Usu_entrega=" . $_POST['Id_usuario'];
    }
    
    if(strlen($_POST['Fecha_ini'])>0 && $_POST['Fecha_fin']==null){
        $query=$query." AND Fecha_entrega>='".$_POST['Fecha_ini']."'";
    }elseif(strlen($_POST['Fecha_fin'])>0 && $_POST['Fecha_ini']==null){
        $query=$query." AND Fecha_entrega<='".$_POST['Fecha_fin']."'";
    }elseif(strlen($_POST['Fecha_ini'])>0 && strlen($_POST['Fecha_fin'])>0){
        $query=$query." AND (Fecha_entrega>='".$_POST['Fecha_ini']."' AND Fecha_entrega<='".$_POST['Fecha_fin']."')";
    }
    $prestados="";
    if(strlen($_POST['Id_libro'])==0 && strlen($_POST['Id_usuario'] )==0 && strlen($_POST['Fecha_ini'] )==0 && strlen($_POST['Fecha_fin'] )==0){
        $prestados=" AND Fecha_entregado IS NULL";
    }
    
    
    $count = 1;
    $queryx = "SELECT * FROM Historial_libro_prestamo 
             JOIN Libros ON Historial_libro_prestamo.Id_libro=Libros.Id_lib
             WHERE Id_plantel=".$usu->getId_plantel()."  ".$query." ".$prestados." ORDER BY Fecha_devolucion ASC";
    foreach($DaoHistorialPrestamoLibro->advanced_query($queryx) as $historial) {
        $libro=$DaoLibros->show($historial->getId_libro());
        $usuEntrega = $DaoUsuarios->show($historial->getUsu_entrega());
        
        if($historial->getTipo_usu()=="usu"){
            $usuRecibe=$DaoUsuarios->show($historial->getUsu_recibe());
            $Nombreusu=$usuRecibe->getNombre_usu()." ".$usuRecibe->getApellidoP_usu()." ".$usuRecibe->getApellidoM_usu();
        }elseif($historial->getTipo_usu()=="docen"){
            $usuRecibe=$DaoDocentes->show($historial->getUsu_recibe());
            $Nombreusu=$usuRecibe->getNombre_docen()." ".$usuRecibe->getApellidoP_docen()." ".$usuRecibe->getApellidoM_docen();
        }elseif($historial->getTipo_usu()=="alum"){
            $usuRecibe=$DaoAlumnos->show($historial->getUsu_recibe());
            $Nombreusu=$usuRecibe->getNombre()." ".$usuRecibe->getApellidoP()." ".$usuRecibe->getApellidoM();     
        }
        ?>
        <tr>
            <td><?php echo $count; ?></td>
            <td><?php echo $libro->getCodigo() ?></td>
            <td style="width: 115px;"><?php echo $libro->getTitulo() ?></td>
            <td><?php echo $usuEntrega->getNombre_usu()." ".$usuEntrega->getApellidoP_usu()." ".$usuEntrega->getApellidoM_usu(); ?></td>
            <td><?php echo $Nombreusu; ?></td>
            <td><?php echo $historial->getFecha_entrega(); ?></td>
            <td><?php echo $historial->getFecha_devolucion(); ?></td>
            <td><?php echo $historial->getFecha_entregado(); ?></td>
            <td><?php echo $historial->getComentarios(); ?></td>
        </tr>
        <?php
        $count++;
    }
}



if($_GET['action']=="download_excel"){
    $base = new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Código');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'USUARIO');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'SOLICITANTE');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'FECHA DE PRESTAMO');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'FECHA DE ENTREGA');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'FECHA DE RECIBIDO');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'COMENTARIOS');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','I') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    $DaoLibros = new DaoLibros();
    $DaoHistorialPrestamoLibro = new DaoHistorialPrestamoLibro();
    $DaoAlumnos = new DaoAlumnos();
    $DaoDocentes = new DaoDocentes();
    $DaoCiclos = new DaoCiclos();
    $ciclo = $DaoCiclos->getActual();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query = "";

    if ($_GET['Id_libro'] > 0) {
        $query =" AND Id_libro=" . $_GET['Id_libro'];
    }
    
    
    if ($_GET['Id_usuario'] > 0) {
        $query = $query ." AND Usu_entrega=" . $_GET['Id_usuario'];
    }
    
    if(strlen($_GET['Fecha_ini'])>0 && $_GET['Fecha_fin']==null){
        $query=$query." AND Fecha_entrega>='".$_GET['Fecha_ini']."'";
    }elseif(strlen($_GET['Fecha_fin'])>0 && $_GET['Fecha_ini']==null){
        $query=$query." AND Fecha_entrega<='".$_GET['Fecha_fin']."'";
    }elseif(strlen($_GET['Fecha_ini'])>0 && strlen($_GET['Fecha_fin'])>0){
        $query=$query." AND (Fecha_entrega>='".$_GET['Fecha_ini']."' AND Fecha_entrega<='".$_GET['Fecha_fin']."')";
    }
    $prestados="";
    if(strlen($_GET['Id_libro'])==0 && strlen($_GET['Id_usuario'] )==0 && strlen($_GET['Fecha_ini'] )==0 && strlen($_GET['Fecha_fin'] )==0){
        $prestados=" AND Fecha_entregado IS NULL";
    }
    
    $a=1;
    $count=1;
    $queryx = "SELECT * FROM Historial_libro_prestamo 
             JOIN Libros ON Historial_libro_prestamo.Id_libro=Libros.Id_lib
             WHERE Id_plantel=".$usu->getId_plantel()."  ".$query." ".$prestados." ORDER BY Fecha_devolucion ASC";
    foreach($DaoHistorialPrestamoLibro->advanced_query($queryx) as $historial) {
        $libro=$DaoLibros->show($historial->getId_libro());
        $usuEntrega = $DaoUsuarios->show($historial->getUsu_entrega());
        
        if($historial->getTipo_usu()=="usu"){
            $usuRecibe=$DaoUsuarios->show($historial->getUsu_recibe());
            $Nombreusu=$usuRecibe->getNombre_usu()." ".$usuRecibe->getApellidoP_usu()." ".$usuRecibe->getApellidoM_usu();
        }elseif($historial->getTipo_usu()=="docen"){
            $usuRecibe=$DaoDocentes->show($historial->getUsu_recibe());
            $Nombreusu=$usuRecibe->getNombre_docen()." ".$usuRecibe->getApellidoP_docen()." ".$usuRecibe->getApellidoM_docen();
        }elseif($historial->getTipo_usu()=="alum"){
            $usuRecibe=$DaoAlumnos->show($historial->getUsu_recibe());
            $Nombreusu=$usuRecibe->getNombre()." ".$usuRecibe->getApellidoP()." ".$usuRecibe->getApellidoM();     
        }
        
        $count++;
        $objPHPExcel->getActiveSheet()->setCellValue("A$count", $a);
        $objPHPExcel->getActiveSheet()->setCellValue("B$count", $libro->getCodigo());
        $objPHPExcel->getActiveSheet()->setCellValue("C$count", $libro->getTitulo());
        $objPHPExcel->getActiveSheet()->setCellValue("D$count", $usuEntrega->getNombre_usu()." ".$usuEntrega->getApellidoP_usu()." ".$usuEntrega->getApellidoM_usu());
        $objPHPExcel->getActiveSheet()->setCellValue("E$count", $Nombreusu);
        $objPHPExcel->getActiveSheet()->setCellValue("F$count", $historial->getFecha_entrega());
        $objPHPExcel->getActiveSheet()->setCellValue("G$count", $historial->getFecha_devolucion());
        $objPHPExcel->getActiveSheet()->setCellValue("H$count", $historial->getFecha_entregado());
        $objPHPExcel->getActiveSheet()->setCellValue("I$count", $historial->getComentarios());
        $a++;
    }
    

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=PrestamoLibro-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}




