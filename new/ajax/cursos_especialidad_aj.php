<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if (isset($_POST['action']) && $_POST['action'] == "updatePage") {
    getCursos($_POST['id_esp']);
}


if (isset($_POST['action']) && $_POST['action'] == "mostrarBoxModal") {
    $id = 0;
    $Text = "Nuevo";
    $clave = "";
    $fechaInicio = "";
    $fechaFin = "";
    $tipoPago = "";
    $numeroHoras = "";
    $numeroSesiones = "";
    $diaEspecifico = "";
    $Idturno="";
    $DaoTurnos= new DaoTurnos();
    if (isset($_POST['id_curso']) && $_POST['id_curso'] > 0) {
        $Text = "Editar";
        $DaoCursosEspecialidad = new DaoCursosEspecialidad();

        $curso = $DaoCursosEspecialidad->show($_POST['id_curso']);
        $id = $curso->getId();
        $clave = $curso->getClave();
        $fechaInicio = $curso->getFechaInicio();
        $fechaFin = $curso->getFechaFin();
        $tipoPago = $curso->getTipoPago();
        $numeroHoras = $curso->getNum_horas();
        $numeroSesiones = $curso->getNum_sesiones();
        $diaEspecifico = $curso->getDiaPago();
        $Idturno=$curso->getTurno();
    }
    ?>
    <div class="box-emergente-form">
        <h1><?php echo $Text ?> curso</h1>
        <p>Clave<br><input type="text"  id="clave" value="<?php echo $clave ?>"/></p>
        <p>Turno<br>
            <select id="turno">
                <option value="0"></option>
                <?php
                foreach($DaoTurnos->getTurnos() as $turno){
                    ?>
                    <option value="<?php echo $turno->getId()?>" <?php if ($Idturno == $turno->getId()) { ?> selected="selected" <?php } ?>><?php echo $turno->getNombre()?></option>
                <?php
                }
                ?>
            </select>
        </p>
        <p>Fecha de inicio<br><input type="date"  id="fechaIni" value="<?php echo $fechaInicio ?>"/></p>
        <p>Fecha de fin<br><input type="date"  id="fechaFin" value="<?php echo $fechaFin ?>"/></p>
        <p>Generar cargos a partir del<br>
            <select id="tipo-pago" onchange="mostrarBoxDia()">
                <option value="0"></option>
                <option value="1" <?php if ($tipoPago == "1") { ?> selected="selected" <?php } ?>>Día de inicio del curso</option>
                <option value="2" <?php if ($tipoPago == "2") { ?> selected="selected" <?php } ?>>Día especifico</option>
                <option value="3" <?php if ($tipoPago == "3") { ?> selected="selected" <?php } ?>>Día de inscripción del alumno</option>
            </select>   </p>
        <div id="box-dia-especifico">
            <?php
            if (strlen($diaEspecifico) > 0) {
                ?>
                <p>Día especifico<br><input type="number"  id="diaEspecifico" value="<?php echo $diaEspecifico ?>"/></p>
                <?php
            }
            ?>
        </div>
        <p>Número de Horas<br><input type="number"  id="numHoras" value="<?php echo $numeroHoras ?>"/></p>
        <p>Número de sesiones<br><input type="number"  id="numSesiones" value="<?php echo $numeroSesiones ?>"/></p>
        <button onclick="saveCurso(<?php echo $id ?>)">Guardar</button>
        <button onclick="ocultar_error_layer()">Cerrar</button>
    </div>
    <?php
}

if (isset($_POST['action']) && $_POST['action'] == "mostrarBoxDia") {
    if ($_POST['tipoPago'] == "2") {
        ?>
        <p>Día especifico<br><input type="text"  id="diaEspecifico"/></p>
        <?php
    }
}


if (isset($_POST['action']) && $_POST['action'] == "saveCurso") {

    $DaoCursosEspecialidad = new DaoCursosEspecialidad();
    $DaoUsuarios = new DaoUsuarios();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    if (isset($_POST['id_curso']) && $_POST['id_curso'] > 0) {
        $curso = $DaoCursosEspecialidad->show($_POST['id_curso']);
        $curso->setClave($_POST['clave']);
        $curso->setFechaInicio($_POST['fechaIni']);
        $curso->setFechaFin($_POST['fechaFin']);
        $curso->setNum_horas($_POST['numHoras']);
        $curso->setNum_sesiones($_POST['numSesiones']);
        $curso->setTipoPago($_POST['tipoPago']);
        $curso->setDiaPago($_POST['diaEspecifico']);
        $curso->setTurno($_POST['turno']);
        $DaoCursosEspecialidad->update($curso);
    } else {
        $curso = new CursosEspecialidad();
        $curso->setId_esp($_POST['id_esp']);
        $curso->setId_usu_capura($usu->getId());
        $curso->setDateCreated(date('Y-m-d H:i:s'));
        $curso->setClave($_POST['clave']);
        $curso->setFechaInicio($_POST['fechaIni']);
        $curso->setFechaFin($_POST['fechaFin']);
        $curso->setNum_horas($_POST['numHoras']);
        $curso->setNum_sesiones($_POST['numSesiones']);
        $curso->setTipoPago($_POST['tipoPago']);
        $curso->setTurno($_POST['turno']);
        if(isset($_POST['diaEspecifico'])){
           $curso->setDiaPago($_POST['diaEspecifico']);
        }
        
        $curso->setActivo(1);
        $DaoCursosEspecialidad->add($curso);
    }
}


if (isset($_POST['action']) && $_POST['action'] == "deleteCurso") {
    $DaoCursosEspecialidad = new DaoCursosEspecialidad();
    $DaoCursosEspecialidad->delete($_POST['id_curso']);
}

function getCursos($Id_esp) {
    $count = 1;
    $DaoTurnos= new DaoTurnos();
    $DaoCursosEspecialidad = new DaoCursosEspecialidad();
    foreach ($DaoCursosEspecialidad->getCursosEspecialidad($Id_esp) as $curso) {
        if ($curso->getTipoPago() == 1) {
            $tipoPago = "Día de inicio del curso";
        } elseif ($curso->getTipoPago() == 2) {
            $tipoPago = "Día especifico";
        } elseif ($curso->getTipoPago() == 3) {
            $tipoPago = "Día de inscripción del alumno";
        }
        
        $tur = $DaoTurnos->show($curso->getTurno());
        $turno=$tur->getNombre();
        ?>
        <tr>
            <td><?php echo $count ?></td>
            <td><?php echo $curso->getClave() ?></td>
            <td class="td-center"><?php echo $curso->getDateCreated() ?></td>
            <td class="td-center"><?php echo $curso->getFechaInicio() ?></td>
            <td class="td-center"><?php echo $curso->getFechaFin() ?></td>
            <td class="td-center"><?php echo $tipoPago ?></td>
            <td class="td-center"><?php echo $curso->getDiaPago() ?></td>
            <td class="td-center"><?php echo $curso->getNum_horas() ?></td>
            <td class="td-center"><?php echo $curso->getNum_sesiones() ?></td>
            <td class="td-center"><?php echo $turno ?></td>
            <td class="td-center">
                <i class="fa fa-pencil-square-o" onclick="mostrarBoxModal(<?php echo $curso->getId() ?>)" title="Editar"></i>
                <i class="fa fa-trash" onclick="deleteCurso(<?php echo $curso->getId() ?>)" title="Eliminar"></i>
            </td>
        </tr>
        <?php
        $count++;
    }
}
