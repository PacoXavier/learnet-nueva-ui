<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if ($_POST['action'] == "filtro") {

    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoAlumnos = new DaoAlumnos();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoCiclos = new DaoCiclos();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoGrupos= new DaoGrupos();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoTurnos= new DaoTurnos();
    $base= new base();
    $nombre_ori="";
    $oferta_alumno = $DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $alum = $DaoAlumnos->show($oferta_alumno->getId_alum());
    $oferta = $DaoOfertas->show($oferta_alumno->getId_ofe());
    $esp = $DaoEspecialidades->show($oferta_alumno->getId_esp());
    if ($oferta_alumno->getId_ori() > 0) {
        $ori = $DaoOrientaciones->show($oferta_alumno->getId_ori());
        $nombre_ori = $ori->getNombre();
    }

    $id_alum = $_POST['Id_alumn'];

    $query = "";
    if ($_POST['Id_ciclo'] > 0) {
        $query = " AND Id_ciclo=" . $_POST['Id_ciclo'];
    }

    if ($oferta_alumno->getOpcionPago() == 1) {
        $Opcion_pago = "Plan por materias";
    } else {
        $Opcion_pago = "Plan completo";
    }
    ?>
    <table class="table">
        <thead>
            <tr>
                <td>Matricula</td>
                <td class="normal"><?php echo $alum->getMatricula() ?></td>
                <td>Nombre</td>
                <td colspan="3" class="normal"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
                <td>Opci&oacute;n de pago</td>
                <td class="normal" colspan="4"><?php echo $Opcion_pago; ?></td>
            </tr>
            <tr>
                <td>Carrera</td>
                <td colspan="3" class="normal"><?php echo $esp->getNombre_esp(); ?></td>
                <td>Orientaci&oacute;n</td>
                <td class="normal"><?php echo $nombre_ori; ?></td>
                <td>Nivel</td>
                <td colspan="4" class="normal"><?php echo $oferta->getNombre_oferta(); ?></td>
            </tr>
            <tr>
                <td>Ciclo</td>
                <td style="width: 80px">CLAVE GRUPAL</td>
                <td style="width: 100px">MATERIA</td>
                <td>TURNO</td>
                <td>Faltas</td>
                <td>Asistencias</td>
                <td>Justificaciones</td>
                <td>Porcentaje</td>
                <td>Calificaci&oacute;n</td>
                <td>Evaluaci&oacute;n<br> Extraordinaria</td>
                <td>Evaluaci&oacute;n<br> Especial</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $countMat = 0;
            $totalCal = 0;
            $query = "SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=" . $_POST['Id_ofe_alum'] . " " . $query;
            foreach ($DaoCiclosAlumno->advanced_query($query) as $cicloAlumno) {
                $ciclo = $DaoCiclos->show($cicloAlumno->getId_ciclo());
                foreach ($DaoMateriasCicloAlumno->getMateriasCicloAlumno($cicloAlumno->getId()) as $materiaCiclo) {

                    $total = "";
                    $turno = "";
                    $clave="";
                    $asis=array();
                    $asis['Faltas']=0;
                    $asis['Asistencias']=0;
                    $asis['Justificaciones']=0;
                    if ($materiaCiclo->getId_grupo() > 0) {
                        $asis=$DaoGrupos->getAsistenciasGrupoAlum($materiaCiclo->getId_grupo(), $id_alum);
                        $total = $DaoGrupos->porcentajeAsistencias($asis['Asistencias'], $asis['Justificaciones'], $asis['Faltas']);
                        $total = number_format($total, 2) . " %";
                        
                        $grupo= $DaoGrupos->show($materiaCiclo->getId_grupo());
                        $clave=$grupo->getClave();
                        $tur = $DaoTurnos->show($grupo->getTurno());
                        $turno=$tur->getNombre();
                    }

                    $mat_esp = $DaoMateriasEspecialidad->show($materiaCiclo->getId_mat_esp());
                    $mat = $DaoMaterias->show($mat_esp->getId_mat());

                    $colorCalTotalParciales = "red";
                    if ($materiaCiclo->getCalTotalParciales() > $mat->getPromedio_min()) {
                        $colorCalTotalParciales = "green";
                    }
                    $colorCalExtraordinario = "red";
                    if ($materiaCiclo->getCalExtraordinario() > $mat->getPromedio_min()) {
                        $colorCalExtraordinario = "green";
                    }
                    $colorCalEspecial = "red";
                    if ($materiaCiclo->getCalEspecial() > $mat->getPromedio_min()) {
                        $colorCalEspecial = "green";
                    }
                    
                    if ($materiaCiclo->getCalEspecial() > 0) {
                        $totalCal+=$DaoMaterias->redonderCalificacion($materiaCiclo->getCalEspecial());
                    }elseif($materiaCiclo->getCalExtraordinario()>0){
                        $totalCal+=$DaoMaterias->redonderCalificacion($materiaCiclo->getCalExtraordinario());
                    }else{
                        $totalCal+=$DaoMaterias->redonderCalificacion($materiaCiclo->getCalTotalParciales());
                    }

                    $NombreMat = $mat->getNombre();
                    if (strlen($mat_esp->getNombreDiferente()) > 0) {
                        $NombreMat = $mat_esp->getNombreDiferente();
                    }

                    if ($materiaCiclo->getCalTotalParciales() != null || $materiaCiclo->getCalExtraordinario() != null || $materiaCiclo->getCalEspecial() != null) {
                        $countMat++;
                    }
                    
                    //Por si pusieron nomenclatura AC, CP, etc
                    $CalTotalParciales=$base->getCalificacion($materiaCiclo->getCalTotalParciales());
                    $CalExtraordinario=$base->getCalificacion($materiaCiclo->getCalExtraordinario());
                            
                    ?>
                    <tr>
                        <td><?php echo $ciclo->getClave() ?></td>
                        <td><?php echo $clave ?> </td>
                        <td style="width: 200px;"><?php echo $NombreMat ?></td>
                        <td><?php echo $turno; ?></td>
                        <td style="text-align: center;color: red;"><?php echo $asis['Faltas']; ?></td>
                        <td style="text-align: center;color: green;"><?php echo $asis['Asistencias']; ?></td>
                        <td style="text-align: center; color: black;"><?php echo $asis['Justificaciones']; ?></td>
                        <td style="text-align: center; color: black;"><?php echo $total; ?></td>
                        <td style="text-align: center; color: <?php echo $colorCalTotalParciales; ?>;"><?php echo $CalTotalParciales ?></td>
                        <td style="text-align: center; color: <?php echo $colorCalExtraordinario; ?>;"><?php echo $CalExtraordinario ?></td>
                        <td style="text-align: center; color: <?php echo $colorCalEspecial; ?>;"><?php echo $DaoAlumnos->redonderCalificacion($materiaCiclo->getCalEspecial()) ?></td>
                    </tr>
            <?php
        }
    }
    if($countMat>0){
      $totalCal = $totalCal / $countMat;  
    }else{
      $totalCal=0;  
    }
    ?>
        </tbody>
    </table>
    <table class="table">
        <tbody>
            <tr>
                <td colspan="8"></td>
                <td><b>Promedio:</b></td>
                <td><b><?php echo $DaoAlumnos->redonderCalificacion($totalCal); ?></b></td>
            </tr>
        </tbody>
    </table>
    <?php
}



if ($_POST['action'] == "mostrar_box_email") {
    ?>
    <div id="box_emergente">
        <h1>Enviar calificaciones</h1>
        <p><input type="text" placeholder="Destinatarios" id="destinatarios"/></p>
        <p><textarea placeholder="Comentarios" id="mensaje"></textarea></p>
        <p><button onclick="send_email(this)">Enviar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
    </div>
    <?php
}


if ($_POST['action'] == "buscarAlum") {
    $DaoAlumnos = new DaoAlumnos();
    foreach ($DaoAlumnos->buscarAlumnosAltasYBajas($_POST['buscar']) as $k => $v) {
        ?>
        <li id_alum="<?php echo $v->getId(); ?>" onclick="getOfertas(<?php echo $v->getId(); ?>)"><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li>
        <?php
    }
}


if ($_POST['action'] == "getOfertas") {
    ?>
    <option value="0"></option>
    <?php
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    foreach ($DaoOfertasAlumno->getOfertasAlumnoAltasYBajas($_POST['Id_alum']) as $OfertaAlumno) {
        $nombre_ori = "";
        $oferta = $DaoOfertas->show($OfertaAlumno->getId_ofe());
        $esp = $DaoEspecialidades->show($OfertaAlumno->getId_esp());
        if ($OfertaAlumno->getId_ori() > 0) {
            $ori = $DaoOrientaciones->show($OfertaAlumno->getId_ori());
            $nombre_ori = $ori->getNombre();
        }
        ?>
        <option value="<?php echo $OfertaAlumno->getId() ?>"><?php echo $oferta->getNombre_oferta() . " " . $esp->getNombre_esp() . " " . $nombre_ori; ?></option>
        <?php
    }
}

if ($_POST['action'] == "getCiclosOferta") {
    ?>
    <option value="0"></option>
    <?php
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoCiclos = new DaoCiclos();
    foreach ($DaoCiclosAlumno->getCiclosOferta($_POST['Id_ofe_alum']) as $cicloAlumno) {
        $ciclo = $DaoCiclos->show($cicloAlumno->getId_ciclo());
        ?>
        <option value="<?php echo $ciclo->getId() ?>"><?php echo $ciclo->getClave() ?></option>
        <?php
    }
}