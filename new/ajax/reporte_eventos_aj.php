<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 
require_once('../estandares/Fpdf/fpdf.php'); 
require_once('../estandares/Fpdf/fpdi.php'); 
require_once("../estandares/QRcode/qrlib.php");
require_once('../estandares/cantidad_letra.php'); 

if($_POST['action']=="filtro"){
    $DaoEventos= new DaoEventos();
    $DaoCiclos= new DaoCiclos();
    $DaoAulas= new DaoAulas();
    $DaoUsuarios= new DaoUsuarios();
    $DaoPagos= new DaoPagos();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $query="";
    if($_POST['Id_salon']>0){
        $query="AND Id_salon=".$_POST['Id_salon'];
    }
    if($_POST['Id_ciclo']>0){
        $query=$query." AND Id_ciclo=".$_POST['Id_ciclo'];
    }
    
    if(strlen($_POST['Fecha_ini'])>0 && $_POST['Fecha_fin']==null){
        $query=$query." AND Start>='".$_POST['Fecha_ini']."'";
    }elseif(strlen($_POST['Fecha_fin'])>0 && $_POST['Fecha_ini']==null){
        $query=$query." AND Start<='".$_POST['Fecha_fin']."'";
    }elseif(strlen($_POST['Fecha_ini'])>0 && strlen($_POST['Fecha_fin'])>0){
        $query=$query." AND (Start>='".$_POST['Fecha_ini']."' AND Start<='".$_POST['Fecha_fin']."')";
    }
    
    $count=1;
    $query= "SELECT * FROM Eventos WHERE Activo=1  AND Id_plantel=".$usu->getId_plantel()." ".$query." ORDER BY Id_event DESC";
    foreach($DaoEventos->advanced_query($query) as $k=>$v){     
        $ciclo=$DaoCiclos->show($v->getId_ciclo());
        $aula=$DaoAulas->show($v->getId_salon());

        $Start=date('Y-m-d', strtotime($v->getStart()));
        $HoraInicio=date('H:i:s', strtotime($v->getStart()));
        $HoraFin=date('H:i:s', strtotime($v->getEnd()));

        $TotalPagado=0;
        foreach($DaoPagos->getPagosEvento($v->getId()) as $k2=>$v2){
            $TotalPagado+=$v2->getMonto_pp();
        }
    ?>
    <tr>
        <td><?php echo $count?> </td>
        <td><?php echo $v->getNombre()?> </td>
        <td><?php echo $ciclo->getClave()?> </td>
        <td><?php echo $aula->getClave_aula()." - ".$aula->getNombre_aula() ?></td>
        <td><?php echo $Start;?></td>
        <td><?php echo $HoraInicio;?></td>
        <td><?php echo $HoraFin?></td>
        <td><?php echo $v->getComentarios()?></td>
        <td><b>$<?php echo number_format($v->getCosto(),2);?></b></td>
        <td><b>$<?php echo number_format($TotalPagado,2);?></b></td>
        <td style="text-align: center;" class="menu">
             <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar opciones</span>
             <div class="box-buttom">
                 <?php
                 if($v->getCosto()>$TotalPagado){
                     ?>
                     <button onclick="mostrar_box_pagar(<?php echo $v->getId()?>)">Pagar</button>
                 <?php
                 }
                 ?>
                 <button onclick="mostrar_historial(<?php echo $v->getId()?>)">Historial</button>
             </div>
        </td>
    </tr>
    <?php
    $count++;
    }
    
}


if($_POST['action']=="mostrar_box_pagar"){
    $DaoMetodosPago= new DaoMetodosPago();
?>
<div id ="box_emergente" class="pagar">
    <h1>Pagar evento</h1>
    <div>
        <p>Cantidad recibida<br><input type="text" id="Cantidad"/></p>
        <p>Fecha de pago<br><input type="date" id="FechaPago"/></p>
        <p>M&eacute;todo de pago<br>                                   
          <select id="Id_met">
               <option value="0"></option>
                <?php
                    foreach ($DaoMetodosPago->showAll() as $k => $v) {
                        ?>
                        <option value="<?php echo $v->getId()?>"><?php echo $v->getNombre() ?></option>
                        <?php
                    }
                ?>
              </select>
        </p>
        <p>Nombre<br><input type="text" id="Nombre"/></p>
        <p>Email<br><input type="text" id="Email"/></p>
        <p>Comentarios<br><textarea id="comentarios"></textarea></p>
        <button onclick="pagar(<?php echo $_POST['Id_pago'] ?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cancelar</button>
    </div>
</div>
    <?php
}

if($_POST['action']=="pagar"){
    $DaoEventos= new DaoEventos();
    $eve=$DaoEventos->show($_POST['Id_event']);
    $DaoAulas= new DaoAulas();
    $aula=$DaoAulas->show($eve->getId_salon());
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $DaoPagos= new DaoPagos();
    $Folio=$DaoPagos->generarFolio();
    $Pagos= new Pagos();
    $Pagos->setMonto_pp($_POST['Cantidad']);
    $Pagos->setId_usu($_COOKIE['admin/Id_usu']);
    $Pagos->setFecha_pp($_POST['FechaPago']);
    $Pagos->setMetodo_pago($_POST['Id_met']);
    $Pagos->setFecha_captura(date('Y-m-d H:i:s'));
    $Pagos->setComentarios($_POST['Comentarios']);
    $Pagos->setId_evento($_POST['Id_event']);
    $Pagos->setNombrePaga($_POST['Nombre']);
    $Pagos->setEmailPaga($_POST['Email']);
    $Pagos->setConcepto("Renta de ".$aula->getNombre_aula());
    $Pagos->setId_plantel($usu->getId_plantel());
    $Pagos->setActivo(1);
    $Pagos->setFolio($Folio);
    $DaoPagos->add($Pagos);
    updateEventos();
}


function updateEventos(){
    $count=1;
    $DaoEventos= new DaoEventos();
    $DaoCiclos= new DaoCiclos();
    $DaoAulas= new DaoAulas();
    $DaoPagos= new DaoPagos();
    foreach($DaoEventos->showAll() as $k=>$v){
        $ciclo=$DaoCiclos->show($v->getId_ciclo());
        $aula=$DaoAulas->show($v->getId_salon());

        $Start=date('Y-m-d', strtotime($v->getStart()));
        $HoraInicio=date('H:i:s', strtotime($v->getStart()));
        $HoraFin=date('H:i:s', strtotime($v->getEnd()));

        $TotalPagado=0;
        foreach($DaoPagos->getPagosEvento($v->getId()) as $k2=>$v2){
            $TotalPagado+=$v2->getMonto_pp();
        }
    ?>
    <tr>
        <td><?php echo $count?> </td>
        <td><?php echo $v->getNombre()?> </td>
        <td><?php echo $ciclo->getClave()?> </td>
        <td><?php echo $aula->getClave_aula()." - ".$aula->getNombre_aula() ?></td>
        <td><?php echo $Start;?></td>
        <td><?php echo $HoraInicio;?></td>
        <td><?php echo $HoraFin?></td>
        <td><?php echo $v->getComentarios()?></td>
        <td><b>$<?php echo number_format($v->getCosto(),2);?></b></td>
        <td><b>$<?php echo number_format($TotalPagado,2);?></b></td>
        <td style="text-align: center;" class="menu">
             <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar opciones</span>
             <div class="box-buttom">
                 <?php
                 if($v->getCosto()>$TotalPagado){
                     ?>
                     <button onclick="mostrar_box_pagar(<?php echo $v->getId()?>)">Pagar</button>
                 <?php
                 }
                 ?>
                 <button onclick="mostrar_historial(<?php echo $v->getId()?>)">Historial</button>
             </div>
        </td>
    </tr>
    <?php
    $count++;
    }
    
}

if($_POST['action']=="mostrar_historial"){
    $DaoMetodosPago= new DaoMetodosPago();
    $DaoUsuarios= new DaoUsuarios();
    $DaoPagos= new DaoPagos();

?>
<div id ="box_emergente" class="historial">
    <h1>Historial de pago</h1>
    <table class="table">
        <thead>
            <tr>
                <td>Folio</td>
                <td>Monto</td>
                <td>Fecha captura</td>
                <td>Met&oacute; de pago</td>
                <td>Fecha de dep&oacute;sito</td>
                <td>Usuario</td>
                <td>Acciones</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $total=0;
             foreach($DaoPagos->getPagosEvento($_POST['Id_evento']) as $k2=>$v2){
                 $met=$DaoMetodosPago->show($v2->getMetodo_pago());
                 $usu=$DaoUsuarios->show($v2->getId_usu());
                 $total+=$v2->getMonto_pp();
                 ?>
            <tr>
                <td><?php echo $v2->getId()?></td>
                <td>$<?php echo number_format($v2->getMonto_pp(),2)?></td>
                <td><?php echo $v2->getFecha_captura()?></td>
                <td><?php echo $met->getNombre()?></td>
                <td><?php echo $v2->getFecha_pp()?></td>
                <td><?php echo $usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();?></td>
                <td>
                    <button onclick="mostrar_recibo(<?php echo $v2->getId()?>)">Recibo</button>
                </td>
            </tr>
            <?php

              }
            ?>
            <tr>
                <td colspan="7"></td>
            </tr>
            <tr>
                <td><b>Total</b></td>
                <td colspan="6"><b>$<?php echo number_format($total,2);?></b></td>
            </tr>
        </tbody>
    </table>
    <button onclick="ocultar_error_layer()">Cerrar</button>
</div>
    <?php
}


/*
if($_POST['action']=="send_recibo"){
    // initiate FPDI 
    $pdf = new FPDI(); 

    // add a page 
    $pdf->AddPage(); 
    // set the sourcefile 
    $pdf->setSourceFile('estandares/recibo.pdf'); 

    // import page 1 
    $tplIdx = $pdf->importPage(1); 

    // use the imported page and place it at point 10,10 with a width of 100 mm 
    $pdf->useTemplate($tplIdx); 

    //Id_ofe_alum
    $class_interesados = new class_interesados($_POST['id']);
    $alum=$class_interesados->get_interesado();

    // now write some text above the imported page 
    $pdf->SetFont('Arial','B',8); 
    $pdf->SetTextColor(0,0,0); 

    $pdf->SetXY(77,61); 
    $pdf->Write(10, utf8_decode(strtoupper($alum['Nombre_ins']." ".$alum['ApellidoP_ins']." ".$alum['ApellidoM_ins']))); 

    $pdf->SetXY(77,69.5); 
    $pdf->Write(10, utf8_decode($alum['Matricula']));  

    $subtotal=0;
    $query_Pagos_pagos = "SELECT * FROM Pagos_pagos JOIN Pagos_ciclo ON Pagos_pagos.Id_pago_ciclo=Pagos_ciclo.Id_pago_ciclo WHERE Id_pp=".$_POST['id_pp'];
    $Pagos_pagos= mysql_query($query_Pagos_pagos, $cnn) or die(mysql_error());
    $row_Pagos_pagos = mysql_fetch_array($Pagos_pagos);
    $totalRows_Pagos_pagos= mysql_num_rows($Pagos_pagos);
    if ($totalRows_Pagos_pagos > 0) {

        $pdf->SetXY(179,12); 
        $pdf->Write(10, utf8_decode($row_Pagos_pagos['Id_pp'])); 

        $pdf->SetXY(77,105); 
        $pdf->Write(10, utf8_decode($row_Pagos_pagos['Fecha_pp'])); 

        $class_metodos_pago= new class_metodos_pago($row_Pagos_pagos['metodo_pago']);
        $metodoPago=$class_metodos_pago->get_metodo();

        $Ciclo_oferta=$class_interesados->get_ciclo_oferta($row_Pagos_pagos['Id_ciclo_alum']);
        $Oferta_alum=$class_interesados->get_oferta_alumno($Ciclo_oferta['Id_ofe_alum']);

        $class_ofertas= new class_ofertas($Oferta_alum['Id_ofe']);

        $ofe=$class_ofertas->get_oferta();
        $esp=$class_ofertas->get_especialidad($Oferta_alum['Id_esp']);
        if($Oferta_alum['Id_ori']>0){
        $ori=$class_ofertas->get_orientacion($Oferta_alum['Id_ori']);
        }
        $query_ciclo = "SELECT * FROM  ciclos_ulm WHERE Id_ciclo=" . $_POST['id_ciclo'];
        $ciclo= mysql_query($query_ciclo, $cnn) or die(mysql_error());
        $row_ciclo = mysql_fetch_array($ciclo);

        $pdf->SetXY(77,95); 
        $pdf->MultiCell(90,3 ,utf8_decode(mb_strtoupper($row_Pagos_pagos['Concepto']."/ ".$ofe['Nombre_oferta']." - ".$esp['Nombre_esp']." ".$ori['Nombre_ori']." / ".$row_ciclo['Clave'])),0,'L'); 
        $pdf->SetXY(77,117); 
        $pdf->Write(10, utf8_decode(mb_strtoupper($metodoPago['Nombre']))); 
        $pdf->SetXY(77,78); 
        $pdf->Write(10,"$".  number_format($row_Pagos_pagos['Monto_pp'],2)); 
        $pdf->SetXY(77,83.5); 
        $pdf->Write(10,strtoupper(num2letras($row_Pagos_pagos['Monto_pp'],2))); 
        $pdf->SetXY(179,4); 
        $pdf->Write(10, "CICLO ".$row_ciclo['Clave']." ".utf8_decode(date('Y-m-d'))); 
        
            // add QRcode
    //texto a encliptar                                       //Ruta donde se guarda la imagen
    QRcode::png('?matricula='.$alum['Matricula'].'&folio='.$row_Pagos_pagos['Id_pp'].'&cantidad='.number_format($row_Pagos_pagos['Monto_pp'],2), 'files/test.png');
    $pdf->Image("files/test.png", 23 , 27, 33 ,33);    
    }

    $content=$pdf->Output('', 'S'); 

    //Enviar email por medio de mandrill
    $arrayParams = array();
    $arrayParams['key'] = "wtchUd5oZJnac6Hy2bRZpA";
    $arrayParams['message']['html'] = '';
    $arrayParams['message']['text'] = '';
    $arrayParams['message']['subject'] = "Comprobante de pago ".date('Y-m-d');
    $arrayParams['message']['from_email'] = 'noreply@ulm.mx';
    $arrayParams['message']['from_name'] = 'ULM';
    $arrayParams['message']['to']=array();

    $arrayTo=array();
    $arrayTo["email"]=$alum['Email_ins'];
    //$arrayTo["email"]="christian310332@gmail.com";
    $arrayTo["name"]=$alum['Nombre_ins']." ".$alum['ApellidoP_ins']." ".$alum['ApellidoM_ins'];
    array_push($arrayParams['message']['to'], $arrayTo);
    
    $arrayTo=array();
    $arrayTo["email"]="recibos@ulm.mx";
    $arrayTo["name"]="ULM";
    array_push($arrayParams['message']['to'], $arrayTo);

    $array_global_merge_vars = array();
    $array_global_merge_vars["name"] = 'example name';
    $array_global_merge_vars["content"] = 'example content';

    $arrayParams['message']['global_merge_vars'] = array();
    array_push($arrayParams['message']['global_merge_vars'], $array_global_merge_vars);

    $array_merge_vars = array();
    $array_merge_vars["rcpt"] = 'example rcpt';

    $array_merge_vars['vars'] = array();
    $array_vars_attr = array();
    $array_vars_attr['name'] = 'example name';
    $array_vars_attr['content'] = 'example content';
    array_push($array_merge_vars['vars'], $array_vars_attr);

    $arrayParams['message']['merge_vars'] = array();
    array_push($arrayParams['message']['merge_vars'], $array_merge_vars);

    $arrayParams['message']['tags'] = array();
    array_push($arrayParams['message']['tags'], "example tags[]");

    $arrayParams['message']['google_analytics_domains'] = array();
    array_push($arrayParams['message']['google_analytics_domains'], "...");

    $arrayParams['message']['google_analytics_campaign'] = "...";
    $arrayParams['message']['metadata'] = array();
    array_push($arrayParams['message']['metadata'], "...");

    $array_recipient_metadata = array();
    $array_recipient_metadata["rcpt"] = 'example rcpt';

    $array_recipient_metadata['values'] = array();
    array_push($array_recipient_metadata['values'], "...");

    $arrayParams['message']['recipient_metadata'] = array();
    array_push($arrayParams['message']['recipient_metadata'], $array_recipient_metadata);
   
    $arrayParams['message']['attachments'] = array();
    //$file = file_get_contents("attachments/".$row_Attachments['Llave_atta'].".ulm");
    $arrarAttrAttachments = array();
    $arrarAttrAttachments['type'] = "application/pdf";
    $arrarAttrAttachments['name'] = "Comprobante de pago ".date('Y-m-d');
    $arrarAttrAttachments['content'] = base64_encode($content);
    array_push($arrayParams['message']['attachments'], $arrarAttrAttachments);

    $arrayParams['message']['async'] = '...';
    //create a new cURL resource
    $cc = new cURL();
    $url = "https://mandrillapp.com/api/1.0/messages/send.json";
    $data = $cc->post($url, json_encode($arrayParams));

    //Leer cadena json recibida
    //La cadena recibida tienes que cortarla asta que apetezca puro json
    $data = substr($data, strpos($data, "{"));
    $data = substr($data, 0, strpos($data, "}") + 1);
    $arr = json_decode($data, true);
    //$arr['email']; 
    //$arr['status']; 
    //$arr['_id']; 
    //
    echo json_encode($arr);
}
 * 
 */


if($_GET['action']=="download_excel"){
    
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'CICLOS');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'AULA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'FECHA');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'HORA INICIO');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'HORA FIN');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'COMENTARIOS');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'COSTO RENTA');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'PAGADO');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','J') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    $DaoEventos= new DaoEventos();
    $DaoCiclos= new DaoCiclos();
    $DaoAulas= new DaoAulas();
    $DaoUsuarios= new DaoUsuarios();
    $DaoPagos= new DaoPagos();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $query="";
    if($_GET['Id_salon']>0){
        $query="AND Id_salon=".$_GET['Id_salon'];
    }
    if($_GET['Id_ciclo']>0){
        $query=$query." AND Id_ciclo=".$_GET['Id_ciclo'];
    }
    
    if(strlen($_GET['Fecha_ini'])>0 && $_GET['Fecha_fin']==null){
        $query=$query." AND Start>='".$_GET['Fecha_ini']."'";
    }elseif(strlen($_GET['Fecha_fin'])>0 && $_GET['Fecha_ini']==null){
        $query=$query." AND Start<='".$_GET['Fecha_fin']."'";
    }elseif(strlen($_GET['Fecha_ini'])>0 && strlen($_GET['Fecha_fin'])>0){
        $query=$query." AND (Start>='".$_GET['Fecha_ini']."' AND Start<='".$_GET['Fecha_fin']."')";
    }
    
    $count=1;
    $query= "SELECT * FROM Eventos WHERE Activo=1  AND Id_plantel=".$usu->getId_plantel()." ".$query." ORDER BY Id_event DESC";
    foreach($DaoEventos->advanced_query($query) as $k=>$v){     
        $ciclo=$DaoCiclos->show($v->getId_ciclo());
        $aula=$DaoAulas->show($v->getId_salon());

        $Start=date('Y-m-d', strtotime($v->getStart()));
        $HoraInicio=date('H:i:s', strtotime($v->getStart()));
        $HoraFin=date('H:i:s', strtotime($v->getEnd()));

        $TotalPagado=0;
        foreach($DaoPagos->getPagosEvento($v->getId()) as $k2=>$v2){
            $TotalPagado+=$v2->getMonto_pp();
        }
        
        $count++;
        $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
        $objPHPExcel->getActiveSheet()->setCellValue("B$count", $v->getNombre());
        $objPHPExcel->getActiveSheet()->setCellValue("C$count", $ciclo->getClave());
        $objPHPExcel->getActiveSheet()->setCellValue("D$count", $aula->getClave_aula()." - ".$aula->getNombre_aula());
        $objPHPExcel->getActiveSheet()->setCellValue("E$count", $Start);
        $objPHPExcel->getActiveSheet()->setCellValue("F$count", $HoraInicio);
        $objPHPExcel->getActiveSheet()->setCellValue("G$count", $HoraFin);
        $objPHPExcel->getActiveSheet()->setCellValue("H$count", $v->getComentarios());
        $objPHPExcel->getActiveSheet()->setCellValue("I$count", "$".number_format($v->getCosto(),2));
        $objPHPExcel->getActiveSheet()->setCellValue("J$count", "$".number_format($TotalPagado,2));
    }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=Eventos-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');

}
