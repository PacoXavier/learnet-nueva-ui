<?php
require_once('activate_error.php');
require_once('../require_daos.php');
require_once('../barcodegen/class/BCGFontFile.php');
require_once('../barcodegen/class/BCGColor.php');
require_once('../barcodegen/class/BCGDrawing.php');
require_once('../barcodegen/class/BCGcode39.barcode.php');

if ($_POST['action'] == "generar_credencial") {
    $DaoDocentes = new DaoDocentes();
    $docente = $DaoDocentes->show($_POST['Id_docente']);

    if (file_exists("../files/" . $docente->getImg_docen() . ".jpg")) {
        //+++++++++++++++++++++++++++++++++++++++++++++++
        //Parte del frente de la credencial
        //++++++++++++++++++++++++++++++++++++++++++++++++
        // Crear instancias de imágenes
        $destino = imagecreatefromjpeg("../estandares/Credenciales/Id_frente.jpg");
        $origen = imagecreatefromjpeg("../files/" . $docente->getImg_docen() . ".jpg");

        //Colocar color al texto
        $color_texto = imagecolorallocate($destino, 255, 255, 255);

        //ruta del archivo ttf
        $fuente = "../estandares/Fonts/RobotoCondensed-Regular.ttf";

        //Definimos el tamaño de la fuente
        $tamanoFuente = 40;
        $Texto = mb_strtoupper($docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . " " . $docente->getApellidoM_docen(), "utf-8");

        //Centramos el texto en la imagen
        $cajatexto = imagettfbbox($tamanoFuente, 0, $fuente, mb_strtoupper($Texto, "utf-8"));
        $x = ceil((1013 - $cajatexto[2]) / 2) - 3;

        if ($x <= 0) {
            $tamanoFuente = 22;
            $cajatexto = imagettfbbox($tamanoFuente, 0, $fuente, mb_strtoupper($Texto, "utf-8"));
            $x = ceil((1013 - $cajatexto[2]) / 2) - 3;
        }
        imagettftext($destino, $tamanoFuente, 0, $x, 500, $color_texto, $fuente, mb_strtoupper($Texto, "utf-8"));
        //ruta del archivo ttf
        $fuente = "../estandares/Fonts/RobotoCondensed-Regular.ttf";

        //Definimos el tamaño de la fuente
        $tamanoFuente = 35;
        $Texto = strtolower("Profesor");
        //Centramos el texto en la imagen
        $cajatexto = imagettfbbox($tamanoFuente, 0, $fuente, ucwords($Texto));
        $x = ceil((1013 - $cajatexto[2]) / 2) - 3;
        imagettftext($destino, $tamanoFuente, 0, $x, 557, $color_texto, $fuente, strtoupper($Texto));

        // Copiar y fusionar
        imagecopymerge($destino, $origen, 105, 128, 0, 0, 300, 300, 100);

        //Imprimir y liberar memoria
        //header('Content-Type: image/jpg');
        $Img_frente = "docente_" . $docente->getClave_docen() . "_frente.jpg";
        $im = imagejpeg($destino, $Img_frente, 100);

        //Agregamos la imagen al zip
        $zip = new ZipArchive();
        $zip->open("credenciales_docentes.zip", ZipArchive::CREATE);
        $zip->addFile($Img_frente, $Img_frente);
        $zip->close();

        imagedestroy($destino);
        imagedestroy($origen);

        //+++++++++++++++++++++++++++++++++++++++++++++++
        //Parte del posterior de la credencial
        //++++++++++++++++++++++++++++++++++++++++++++++++
        //Generar imagen del codigo de barras
        // Loading Font
        $font = new BCGFontFile('../estandares/Fonts/RobotoCondensed-Regular.ttf', 25);

        // Don't forget to sanitize user inputs
        $text = $docente->getClave_docen();

        // The arguments are R, G, B for color.
        $color_black = new BCGColor(0, 0, 0);
        $color_white = new BCGColor(255, 255, 255);

        $drawException = null;
        try {
            $code = new BCGcode39();
            $code->setScale(3); // Resolution
            $code->setThickness(20); // Thickness
            $code->setForegroundColor($color_black); // Color of bars
            $code->setBackgroundColor($color_white); // Color of spaces
            $code->setFont($font); // Font (or 0)
            $code->parse($text); // Text
        } catch (Exception $exception) {
            $drawException = $exception;
        }

        //Here is the list of the arguments
        //1 - Filename (empty : display on screen)
        //2 - Background color 
        $drawing = new BCGDrawing('codigoBarras_docen.jpg', $color_white);
        if ($drawException) {
            $drawing->drawException($drawException);
        } else {
            $drawing->setBarcode($code);
            $drawing->draw();
        }
        //Header that says it is an image (remove it if you save the barcode to a file)
        //header('Content-Type: image/png');
        //header('Content-Disposition: inline; filename="barcode.png"');
        // Draw (or save) the image into PNG format.
        $drawing->finish(BCGDrawing::IMG_FORMAT_JPEG);
        //Licenciatura
        $destino = imagecreatefromjpeg("../estandares/Credenciales/Id_p1.jpg");
        $origen = imagecreatefromjpeg("codigoBarras_docen.jpg");

        //Colocar color al texto
        $color_texto = imagecolorallocate($destino, 0, 0, 0);

        //ruta del archivo ttf
        $fuente = "../estandares/Fonts/RobotoCondensed-Regular.ttf";

        //Definimos el tamaño de la fuente
        $tamanoFuente = 25;

        //Actualizamos la fecha de la ultima actualizacion
        $hoy = date('Y-m-d');

        $docente->setUltimaGeneracion_credencial($hoy);
        $DaoDocentes->update($docente);

        $Vigencia = date("d/m/Y", mktime(0, 0, 0, date('m'), date('d'), date('Y') + 1));

        $Texto = "Vigencia: " . $Vigencia;
        imagettftext($destino, $tamanoFuente, 0, 98, 560, $color_texto, $fuente, ucwords($Texto));

        //Obtenemos el tamanio del codigo de barras
        $picsize = getimagesize("codigoBarras_docen.jpg");

        $source_x = $picsize[0] * 1;
        $source_y = $picsize[1] * 1;

        // Copiar y fusionar
        imagecopymerge($destino, $origen, 690, 515, 0, 0, $source_x, $source_y, 100);

        //Imprimir y liberar memoria
        //header('Content-Type: image/jpg');
        $Img_atras = "docente_" . $docente->getClave_docen() . "_atras.jpg";
        $im = imagejpeg($destino, $Img_atras, 100);

        //Agregamos la imagen al zip
        $zip = new ZipArchive();
        $zip->open("credenciales_docentes.zip", ZipArchive::CREATE);
        $zip->addFile($Img_atras, $Img_atras);
        $zip->close();

        imagedestroy($destino);
        imagedestroy($origen);

        //Borramos las imagenes creadas
        unlink("codigoBarras_docen.jpg");
        unlink($Img_frente);
        unlink($Img_atras);

        $vigencia = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d'), date('Y') + 1));
        $hoy = date("Y-m-d");
        $style = "text-align:center;background: lightgreen;";
        $Status = "Vigente";
        if ($hoy > $vigencia) {
            $style = "text-align:center;background: pink;";
            $Status = "Renovar";
        }
        $resp = array();
        $resp['UltimaRenovacion'] = $hoy;
        $resp['Vigencia'] = $vigencia;
        $resp['Style'] = $style;
        $resp['Status'] = $Status;
        echo json_encode($resp);
    }
}


if ($_POST['action'] == "mostrar_layer_generando") {
    ?>
    <div id ="box_emergente" class="grupos">
        <h1>Credenciales generadas <br>
            <span id="alumnosCAmbiados"><span></h1>
    </div>
    <?php
}

if ($_POST['action'] == "buscarDocen") {
    $DaoDocentes = new DaoDocentes();
    foreach ($DaoDocentes->buscarDocente($_POST['buscar']) as $k => $v) {
        ?>
            <li id_docen="<?php echo $v->getId(); ?>"><?php echo $v->getNombre_docen() . " " . $v->getApellidoP_docen() . " " . $v->getApellidoM_docen() ?></li>
            <?php
    }
}


if ($_POST['action'] == "filtro") {
    $base = new base();
    $Daousuarios = new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $usu = $class_usuarios->show($_COOKIE['admin/Id_usu']);

    $query = "";
    if ($_POST['Id_docen'] > 0) {
        $query = $query . " AND Id_docen=" . $_POST['Id_docen'];
    }

    $q = "SELECT * FROM Docentes 
                   WHERE Baja_docen IS NULL  
          AND Id_plantel=" . $usu->getI . "  " . $query . " ORDER BY Id_docen DESC";
    foreach ($base->advanced_query($q) as $row_inscripciones_ulm) {

        if (strlen($row_inscripciones_ulm['Img_docen']) > 0) {
            $Status_aux = "";
            $vigencia = "";
            if (strlen($row_inscripciones_ulm['UltimaGeneracion_credencial']) > 0) {
                $anio = date("Y", strtotime($row_inscripciones_ulm['UltimaGeneracion_credencial']));
                $mes = date("m", strtotime($row_inscripciones_ulm['UltimaGeneracion_credencial']));
                $dia = date("d", strtotime($row_inscripciones_ulm['UltimaGeneracion_credencial']));
                $vigencia = date("Y-m-d", mktime(0, 0, 0, $mes, $dia, $anio + 1));
            }
            $hoy = date("Y-m-d");
            $style = "text-align:center;background: lightgreen;";
            $Status = "Vigente";
            if ($hoy > $vigencia) {
                $style = "text-align:center;background: pink;";
                $Status = "Renovar";
            }
            $Status_aux = $Status;
            if ($_POST['Status'] == 1) {
                $Status_aux = "Vigente";
            } elseif ($_POST['Status'] == 2) {
                $Status_aux = "Renovar";
            }
            if ($Status == $Status_aux) {
                ?>
                <tr id_docen="<?php echo $row_inscripciones_ulm['Id_docen']; ?>">
                    <td><?php echo $row_inscripciones_ulm['Clave_docen'] ?></td>
                    <td><?php echo $row_inscripciones_ulm['Nombre_docen'] . " " . $row_inscripciones_ulm['ApellidoP_docen'] . " " . $row_inscripciones_ulm['ApellidoM_docen'] ?></td>
                    <td style="text-align:center;"><?php echo $row_inscripciones_ulm['UltimaGeneracion_credencial'] ?></td>
                    <td style="text-align:center;"><?php echo $vigencia; ?></td>
                    <td style="text-align:center;"><input type="checkbox"> </td>
                    <td style="text-align:center;<?php echo $style ?>"><span><?php echo $Status ?></span></td>
                </tr>
            <?php
            }
        }
    }
}


