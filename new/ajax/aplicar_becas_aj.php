<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if ($_POST['action'] == "buscar_int") {
    $DaoAlumnos = new DaoAlumnos();
    foreach ($DaoAlumnos->buscarAlumno($_POST['buscar']) as $k => $v) {
        ?>
        <a href="aplicar_becas.php?id=<?php echo $v->getId() ?>">
            <li><?php echo $v->getMatricula() . " - " . $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li>
        </a>
        <?php
    }
}

if($_POST['action']=="box_beca"){
  $DaoBecas= new DaoBecas();
  $DaoCiclosAlumno= new DaoCiclosAlumno();
  $DaoCiclos= new DaoCiclos();
  $DaoOfertasAlumno= new DaoOfertasAlumno();
  $ofertaAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
?>
<div class="box-emergente-form">
	<h1>Aplicar Beca</h1>
        <p>Tipo<br>
            <select id="tipo_beca" onchange="updatePorcentajeBeca()">
                  <option value="0"></option>
                  <?php
                  foreach($DaoBecas->showAll() as $beca){
                      ?>
                       <option value="<?php echo $beca->getId()?>"><?php echo $beca->getNombre()?></option>
                      <?php
                  }
                  ?>
            </select>
        </p>
        
        <p>Porcentaje<br><input type="text" id="porcentaje"/></p>
        <p>Ciclo<br>
          <select id="ciclo_alum">
               <option value="0"></option>
                <?php
                 $query="SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=".$_POST['Id_ofe_alum']." 
                            AND Id_ciclo IN(
                                            SELECT Id_ciclo FROM ciclos_alum_ulm WHERE Id_ofe_alum=".$_POST['Id_ofe_alum']." AND Porcentaje_beca IS NULL
                           ) ORDER BY Id_ciclo_alum DESC";
                foreach($DaoCiclosAlumno->advanced_query($query) as $cicloAlumno){
                        $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());
                  ?>
               <option value="<?php echo $cicloAlumno->getId()?>"><?php echo $ciclo->getClave()?></option>
                 <?php
                }
                ?>
           </select>
        </p>
        
        <p><button onclick="validarExistenciaMensualidades(<?php echo $ofertaAlumno->getId_alum();?>)">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
</div>
<?php
}



if($_POST['action']=="updatePorcentajeBeca"){
    $DaoBecas= new DaoBecas();
    $beca=$DaoBecas->show($_POST['Id_beca']);
    echo number_format($beca->getMonto(),0);
}

if($_POST['action']=="getCalificacionMin"){
    $DaoBecas= new DaoBecas();
    $beca=$DaoBecas->show($_POST['Id_beca']);
    echo number_format($beca->getCalificacionMin(),0);
}


if($_POST['action']=="aplicar_beca"){   
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoBecas= new DaoBecas();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoCiclos= new DaoCiclos();
    $base= new base();
    
    $cicloActual=$DaoCiclos->getActual();
    $beca=$DaoBecas->show($_POST['Tipo_beca']);
    $ofertasAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $Last_ciclo=$DaoOfertasAlumno->getLastCicloOferta($_POST['Id_ofe_alum']);
    $calTotal=0;
    $countMat=0;
    
    $query = "SELECT * FROM ciclos_alum_ulm 
    JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
    WHERE Id_ofe_alum=".$_POST['Id_ofe_alum']." AND Id_ciclo<".$Last_ciclo['Id_ciclo']."  AND materias_ciclo_ulm.Activo=1";
    foreach($base->advanced_query($query) as $row_materias){
             $mat_esp=$DaoMateriasEspecialidad->show($row_materias['Id_mat_esp']);
             $mat=$DaoMaterias->show($mat_esp->getId_mat());

             if($row_materias['CalTotalParciales']>=$mat->getPromedio_min()){
                $calTotal+=$row_materias['CalTotalParciales'];
             }elseif($row_materias['CalExtraordinario']>=$mat->getPromedio_min()){
                 $calTotal+=$row_materias['CalExtraordinario'];
             }elseif($row_materias['CalEspecial']>=$mat->getPromedio_min()){
                 $calTotal+=$row_materias['CalEspecial'];
             }
             $countMat++;
     }
     
     $calTotal=$calTotal/$countMat;
     
     //Se aplica la beca si el alumno tiene un promedio mayor al requerido por la beca
     //O si el alumno es de primer ingreso
     $num=count($DaoCiclosAlumno->getCiclosOferta($_POST['Id_ofe_alum']));
     
     if($calTotal>=$beca->getCalificacionMin() || $num==1){
            //En realidad actualizar la oferta del alumno esta de mas
            $ofertasAlumno->setBeca($_POST['porcentaje']);
            $ofertasAlumno->setTipo_beca($_POST['Tipo_beca']);
            $DaoOfertasAlumno->update($ofertasAlumno);
             
            //Aplicamos la beca al ciclo
            $cicloAlumno=$DaoCiclosAlumno->show($_POST['Id_ciclo_alum']);
            $cicloAlumno->setTipo_beca($_POST['Tipo_beca']);
            $cicloAlumno->setPorcentaje_beca($_POST['porcentaje']);
            $cicloAlumno->setIdUsuCapBeca($_COOKIE['admin/Id_usu']);
            $cicloAlumno->setDiaCapBeca(date('Y-m-d H:i:s'));
            $DaoCiclosAlumno->update($cicloAlumno);
            
            //Aplicamos la beca a los cargos del ciclo
            //Se agrega campo descuento_beca para que se grabe el descuento
            // por si algun dia cambian de porcetaje el tipo de beca este no afecte al porcentaje
            // de beca que tenia anteriormente
            $query = "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=" . $_POST['Id_ciclo_alum']." AND Id_alum=".$_POST['Id_alum']." AND Concepto='Mensualidad' AND Cantidad_pagada IS NULL AND DeadCargo IS NULL AND  DeadCargoUsu IS NULL";
            foreach($DaoPagosCiclo->advanced_query($query) as $cargo){
                $montoDescuento=($cargo->getMensualidad()*$_POST['porcentaje'])/100;
                $Pago=$DaoPagosCiclo->show($cargo->getId());
                $Pago->setDescuento_beca($montoDescuento);
                $DaoPagosCiclo->update($Pago);
            }

            //Crear historial
            $DaoUsuarios= new DaoUsuarios();
            $DaoAlumnos= new DaoAlumnos();
            $DaoCiclos= new DaoCiclos();

            $alum=$DaoAlumnos->show($_POST['Id_alum']);
            $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());

            $TextoHistorial="Añade beca del ".number_format($_POST['porcentaje'],0)."% al alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ciclo ".$ciclo->getClave();
            $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Becas");

            update_page($_POST['Id_alum'], $_POST['Id_ofe_alum']);
     }else{
        echo number_format($calTotal,2);
     }
}

if ($_POST['action'] == "getCiclosBeca") {
    update_page($_POST['Id_alum'], $_POST['Id_ofe_alum']);
}





function update_page($Id_alum, $Id_ofe_alum) {
    $DaoAlumnos= new DaoAlumnos();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoCiclos = new DaoCiclos();
    $DaoUsuarios = new DaoUsuarios();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoBecas= new DaoBecas();

    $_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-asterisk"></i> Aplicar beca</h1>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <?php
                        $nombre = "";
                        if ($Id_alum > 0) {
                            $alumno = $DaoAlumnos->show($Id_alum);
                            $nombre=$alumno->getNombre()." ".$alumno->getApellidoP()." ".$alumno->getApellidoM();
                        }
                        ?>
                        <li>Buscar Alumno<br><input type="text" id="email_int"  onkeyup="buscar_int()" value="<?php echo $nombre ?>"/>
                            <ul id="buscador_int"></ul>
                        </li><br>
                        <li>Especialidad<br>
                            <select id="OfertaAlumno" onchange="getCiclosBeca()">
                                <option value="0">Selecciona una especialidad</option>
                              <?php
                              foreach ($DaoOfertasAlumno->getOfertasAlumno($Id_alum) as $k => $v) {
                                  $esp = $DaoEspecialidades->show($v->getId_esp());
                              ?>
                                <option value="<?php echo $v->getId() ?>" <?php if($v->getId()==$Id_ofe_alum){ ?> selected="selected" <?php } ?>><?php echo $esp->getNombre_esp() ?></option>
                              <?php
                              }
                              ?>

                            </select></li>
                            </select>
                        </li>
                    </ul>
                    <div id="box-info">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td class="td-center">Ciclo</td>
                                    <td class="td-center">Fecha de captura</td>
                                    <td>Usuario</td>
                                    <td class="td-center">Tipo de beca</td>
                                    <td class="td-center">Porcentaje</td>
                                    <td class="td-center">Acciones</td>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php
                                $count = 1;
                                $query="SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=".$Id_ofe_alum." AND Porcentaje_beca>0 ORDER BY Id_ciclo_alum DESC";
                                foreach ($DaoCiclosAlumno->advanced_query($query) as $cicloAlumno) {
                                    $ciclo = $DaoCiclos->show($cicloAlumno->getId_ciclo());
                                    $nombre="";
                                    if($cicloAlumno->getIdUsuCapBeca()>0){
                                      $usuCaptura=$DaoUsuarios->show($cicloAlumno->getIdUsuCapBeca());
                                      $nombre=$usuCaptura->getNombre_usu()." ".$usuCaptura->getApellidoP_usu()." ".$usuCaptura->getApellidoM_usu();
                                    }
                                    $beca=$DaoBecas->show($cicloAlumno->getTipo_beca());
                                    ?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td class="td-center"><?php echo $ciclo->getClave(); ?></td>
                                         <td class="td-center"><?php echo $cicloAlumno->getDiaCapBeca() ?></td>
                                        <td><?php echo $nombre?></td>
                                        <td class="td-center"><?php echo $beca->getNombre()?></td>
                                        <td class="td-center"><?php echo number_format($cicloAlumno->getPorcentaje_beca(),0) ?> %</td>
                                        <td class="td-center">
                                            <a href="kardex.php?ida=<?php echo $Id_alum?>&ido=<?php echo $Id_ofe_alum?>" target="_blank"><button>Kardex</button></a>
                                            <?php
                                            if(file_exists("../files/".$beca->getContrato().".pdf")){
                                                ?>
                                                  <a href="files/<?php echo $beca->getContrato()?>.pdf" target="_blank"><button>Contrato beca</button></a>
                                            <?php
                                            }
                                            ?>
                                            <button onclick="deleteBeca(<?php echo $cicloAlumno->getId()?>)">Eliminar</button>
                                        </td>
                                    </tr>
                                    <?php
                                    $count++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once '../estandares/menu_derecho.php';
                ?>
                <ul>
                    <?php
                    if ($Id_alum > 0) {
                        ?>
                        <li><span onclick="box_beca(<?php echo $Id_ofe_alum;?>)">Aplicar beca </span></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </td>
    </tr>
    <?php
}






if ($_POST['action'] == "deleteBeca") {
    $DaoAlumnos= new DaoAlumnos();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoCiclos = new DaoCiclos();
    $DaoUsuarios = new DaoUsuarios();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoBecas= new DaoBecas();
    $DaoPagosCiclo= new DaoPagosCiclo();

    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $cicloAlumno=$DaoCiclosAlumno->show($_POST['Id_ciclo_alum']);
    $beca=$DaoBecas->show($cicloAlumno->getTipo_beca());
    $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());
    $ofertaAlumno=$DaoOfertasAlumno->show($cicloAlumno->getId_ofe_alum());
    $alum=$DaoAlumnos->show($ofertaAlumno->getId_alum());

    //Eliminar los montos de la beca
    $query = "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=" . $cicloAlumno->getId()." AND Id_alum=".$alum->getId()." AND Concepto='Mensualidad' AND DeadCargo IS NULL AND  DeadCargoUsu IS NULL";
    foreach($DaoPagosCiclo->advanced_query($query) as $cargo){
        $montoDescuento=($cargo->getMensualidad()*$beca->getMonto())/100;
        $Pago=$DaoPagosCiclo->show($cargo->getId());
        $Pago->setDescuento_beca(NULL);
        $DaoPagosCiclo->update($Pago);
    }

    //Historial
    $TextoHistorial = "Elimino beca del ".number_format($beca->getMonto(),0)."% del ciclo ".$ciclo->getClave()." para  " . $alum->getNombre() . " " . $alum->getApellidoP(). " " . $alum->getApellidoM();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Becas");

    $cicloAlumno->setDiaCapBeca(NULL);
    $cicloAlumno->setIdUsuCapBeca(NULL);
    $cicloAlumno->setPorcentaje_beca(NULL);
    $cicloAlumno->setTipo_beca(NULL);
    $cicloAlumno->setMontoBeca(NULL);
    $DaoCiclosAlumno->update($cicloAlumno);
    
    
    $ofertaAlumno->setBeca(NULL);
    $ofertaAlumno->setTipo_beca(NULL);
    $DaoOfertasAlumno->update($ofertaAlumno);

    update_page($alum->getId(), $ofertaAlumno->getId());
}

if($_POST['action']=="validarExistenciaMensualidades"){
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclos= new DaoCiclos();  
    
    $count=0;
    foreach($DaoPagosCiclo->getCargosCiclo($_POST['Id_ciclo_alum']) as $cargo){
        if($cargo->getConcepto()=="Mensualidad"){
           $count++;
        }
    }
    echo $count;
}