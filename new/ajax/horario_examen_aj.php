<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="getIdPer"){
    $Id_per=0;
    $DaoPeriodosExamenes= new DaoPeriodosExamenes();
    foreach($DaoPeriodosExamenes->getPeriodosExamenCiclo($_POST['Id_ciclo']) as $perExamen){
           $resp=$DaoPeriodosExamenes->getDiasRango($perExamen->getFechaIni(),$perExamen->getFechaFin());
           foreach($resp as $dia){
               if($dia==$_POST['Fecha']){
                  $Id_per= $perExamen->getId();
               }
           }
    }
    echo $Id_per;
}

if($_POST['action']=="getExamenesDocente"){
    $resp['Examenes']=array();
    $resp['Ciclo'] = array();
    $resp['DiasFestivos'] = array();
    $resp['PeriodoExamen'] = array();
    $resp['PeriodoVacaciones'] = array();
        
    if($_POST['Id_ciclo']>0){
        $DaoUsuarios= new DaoUsuarios();
        $DaoCiclos = new DaoCiclos();
        $DaoDiasInhabiles = new DaoDiasInhabiles();
        $DaoHorarioExamenGrupo= new DaoHorarioExamenGrupo();
        $DaoPeriodosExamenes= new DaoPeriodosExamenes();
        $DaoPeriodosVacaciones= new DaoPeriodosVacaciones();
        $DaoMaterias= new DaoMaterias();
        $DaoGrupos= new DaoGrupos();
        $DaoGrupoExamen= new DaoGrupoExamen();
        $cicloActual = $DaoCiclos->show($_POST['Id_ciclo']);

        //Obtener el periodo que abaarca el ciclo actual
        $c = array();
        $c['FechaIni'] = $cicloActual->getFecha_ini();
        $c['FechaFin'] = $DaoPeriodosExamenes->getDiaMasDias($cicloActual->getFecha_fin(),1);
        array_push($resp['Ciclo'], $c);

        //Obtener los dias festivos
        foreach ($DaoDiasInhabiles->getDiasInhabilesCiclo($cicloActual->getId()) as $dia) {
            $d = array();
            $d['FechaIni'] = $dia->getFecha_inh();
            array_push($resp['DiasFestivos'], $d);
        }
        
        //Periodo de examenes
        foreach($DaoPeriodosExamenes->getPeriodosExamenCiclo($cicloActual->getId()) as $per){
                //Añadir mas 1 dia en fecha fin porque el plugin no marca correctamente los dias
                $t = array();
                $t['Id'] = $per->getId();
                $t['Titulo'] = "";
                $t['FechaIni'] = $per->getFechaIni();
                $t['FechaFin'] = $DaoPeriodosExamenes->getDiaMasDias($per->getFechaFin(),1);
                $t['Dias']=$DaoPeriodosExamenes->getDiasRango($per->getFechaIni(),$per->getFechaFin());
                 array_push($resp['PeriodoExamen'], $t);

        }

        //Periodo de vacaciones
        foreach($DaoPeriodosVacaciones->getPeriodosVacacionesCiclo($cicloActual->getId()) as $per){
                $t = array();
                $t['Id'] = $per->getId();
                $t['Titulo'] = $per->getNombre();
                $t['FechaIni'] = $per->getFechaIni();
                $t['FechaFin'] = $DaoPeriodosExamenes->getDiaMasDias($per->getFechaFin(),1);
                array_push($resp['PeriodoVacaciones'], $t);

        }
 
        foreach($DaoHorarioExamenGrupo->getExamenesCiclo($_POST['Id_ciclo']) as $examen){
            
            $arrayGrupos=array();
            foreach($DaoGrupoExamen->getGruposExamenHorario($examen->getId()) as $ObjGrupo){
                   $grupo=$DaoGrupos->show($ObjGrupo->getIdGrupo());
                   $cadena=$DaoGrupos->covertirCadena($grupo->getClave(), 1);
                   array_push($arrayGrupos, $cadena);
            }       
            
            //$materia=$DaoMaterias->show($grupo->getId_mat());
            $evento=array();
            $evento['Id']=$examen->getId();
            $evento['Nombre']="Exámen, grupo(s) ".implode(",", $arrayGrupos);
            $evento['Start']=$examen->getStart();
            $evento['End']=$examen->getEnd();
            array_push($resp['Examenes'], $evento);
        }

  
    }
    echo json_encode($resp);
}

if($_POST['action']=="mostrar_box_evento"){
    $DaoAulas= new DaoAulas();
    $DaoHorarioExamenGrupo= new DaoHorarioExamenGrupo();
    $DaoGrupos= new DaoGrupos();
    $DaoGrupoExamen= new DaoGrupoExamen();
    $DaoDocenteExamen= new DaoDocenteExamen();
    $DaoDocentes= new DaoDocentes();
   if($_POST['Id_examen']>0){
      $event=$DaoHorarioExamenGrupo->show($_POST['Id_examen']); 
      $Start=$_POST['FechaInicio'];
      $HoraInicio=$_POST['HoraInicio'];
      $HoraFin=$_POST['HoraFin'];
      $Id_salon=$event->getId_aula();
      $Id_examen=$_POST['Id_examen'];
      $Id_per=$event->getId_periodoExamen();
   }else{
      $Nombre="";
      $Start=$_POST['Start'];
      $HoraInicio="";
      $HoraFin="";
      $Id_salon="";
      $Id_examen=0;
      $Id_per=$_POST['Id_per'];
   }
?>
<div class="eventos box-emergente-form">
	<h1>Exámen</h1>
        <div class="boxUlBuscador">
            <p>Grupo<br><input type="text"  id="Id_grupo" class="buscarFiltro" onkeyup="buscarGruposCiclo(this)" placeholder="Clave"/> <i class="fa fa-plus" aria-hidden="true" onclick="addGrupoExamen()"></i></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <ul id="lista-grupos" class="listas">
               <?php
                foreach($DaoGrupoExamen->getGruposExamenHorario($Id_examen) as $ObjGrupo){
                       $grupo=$DaoGrupos->show($ObjGrupo->getIdGrupo());
                       ?>
                <li id-data="<?php echo $grupo->getId()?>"><?php echo $DaoGrupos->covertirCadena($grupo->getClave(), 2)?> <i class="fa fa-trash-o" aria-hidden="true" onclick="deleteNodo(this)"></i></li>
                       <?php
                }
                ?>
        </ul>
        <div class="boxUlBuscador">
            <p>Docente<br><input type="text"  id="Id_docen" class="buscarFiltro" onkeyup="buscarDocent(this)" placeholder="Clave"/> <i class="fa fa-plus" aria-hidden="true" onclick="addDocenteExamen()"></i></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <ul id="lista-docentes" class="listas">
                <?php
                 foreach($DaoDocenteExamen->getDocentesExamenHorario($Id_examen) as $ObjDocente){
                       $docente=$DaoDocentes->show($ObjDocente->getId_docen());
                       ?>
                          <li id-data="<?php echo $docente->getId()?>"><?php echo $DaoDocentes->covertirCadena($docente->getNombre_docen()." ".$docente->getApellidoP_docen(),2)?> <i class="fa fa-trash-o" aria-hidden="true" onclick="deleteNodo(this)"></i></li>
                       <?php
                }
                ?>
        </ul>
        <p>Aula<br>
           <select id="aula">
              <option value="0"></option>
              	<?php
	            foreach($DaoAulas->showAll() as $aula){
	               ?>
                          <option value="<?php echo $aula->getId()?>" <?php if($Id_salon==$aula->getId()){?>  selected="selected" <?php } ?>><?php echo $aula->getClave_aula()." - ".$aula->getNombre_aula() ?></option>
	                <?php
	            }
                ?>
            </select>
        </p>
        <p>Fecha<br><input type="text" value="<?php echo $Start?>" id="start" readonly="readonly"/></p>
        <p>Hora de inicio<br><input type="text" id="horaInicio" class="time start" value="<?php echo $HoraInicio;?>"/></p>
        <p>Hora de fin<br><input type="text" id="horaFin" class="time end" value="<?php echo $HoraFin;?>"/></p>
        <p>
            <button onclick="verificarDisponibilidad(<?php echo $Id_examen?>)">Guardar</button>
            <?php
              if($Id_examen>0){
            ?>
              <button onclick="delete_examen(<?php echo $Id_examen?>)">Eliminar</button>
            <?php
              }
              ?>
              <button onclick="ocultar_error_layer()">Cancelar</button>
        </p>
        <input type="hidden" id="Id_per_examen" value="<?php echo $Id_per?>"/>
</div>
<?php	
}

if($_POST['action']=="delete_examen"){
    $DaoHorarioExamenGrupo= new DaoHorarioExamenGrupo();
    $DaoGrupoExamen= new DaoGrupoExamen();
    $DaoDocenteExamen= new DaoDocenteExamen();
    
    $DaoGrupoExamen->deleteGruposExamen($_POST['Id_evento']);
    $DaoDocenteExamen->deleteDocentesExamen($_POST['Id_evento']);
    $DaoHorarioExamenGrupo->delete($_POST['Id_evento']);
}


//Hacer buscador donde solo semuestren los grupos 
//que no hayan sido asignados en el periodo de examenes y que Id_docen!=docente actual
if ($_POST['action'] == "buscarGruposCiclo") {
    $DaoUsuarios= new DaoUsuarios();
    $DaoMaterias= new DaoMaterias();
    $DaoGrupos= new DaoGrupos();
    $query="SELECT * FROM Grupos 
            WHERE Grupos.Id_grupo NOT IN(SELECT IdGrupo FROM HorarioExamenGrupo
                                         JOIN GrupoExamen ON HorarioExamenGrupo.Id_horario=GrupoExamen.IdHorarioExamen
                                         WHERE Id_periodoExamen=".$_POST['Id_per_examen'].") 
            AND Grupos.Id_ciclo=".$_POST['Id_ciclo']."
            AND Grupos.Clave LIKE '%".$_POST['buscar']."%'
            AND Grupos.Activo_grupo=1";
    foreach($DaoGrupos->advanced_query($query) as $k=>$v){
        $mat=$DaoMaterias->show($v->getId_mat());
      ?>
       <li id-data="<?php echo $v->getId();?>"><?php echo $v->getClave()."-".$mat->getNombre()?></li>
      <?php      
    }
}


if($_POST['action']=="buscarDocent"){
    if(strlen($_POST['buscar'])>=4){
        $DaoDocentes= new DaoDocentes();
        foreach($DaoDocentes->buscarDocente($_POST['buscar']) as $k=>$v){
          ?>
              <li id-data="<?php echo $v->getId();?>"><?php echo $v->getNombre_docen()." ".$v->getApellidoP_docen()." ".$v->getApellidoM_docen() ?></li>
          <?php      
        }
    }
}


if($_POST['action']=="verificarDisponibilidad"){
    $resp=array();
    $DaoHoras= new DaoHoras();
    $DaoGrupos= new DaoGrupos();
    $DaoDocentes= new DaoDocentes();
    $DaoHorarioExamenGrupo= new DaoHorarioExamenGrupo();
    $DaoDocenteExamen= new DaoDocenteExamen();
    $DaoGrupoExamen= new DaoGrupoExamen();
    $IdHoraIni=  substr($_POST['HoraInicio'], 0,2);
    $IdHoraFin=  substr($_POST['HoraFin'], 0,2);
    
    $HoraIni=$DaoHoras->getTextoHora($IdHoraIni);
    $HoraFin=$DaoHoras->getTextoHora($IdHoraFin);
    
    //Validar que el aula no este ocupada el dia y la hora
    $respOut=$DaoHorarioExamenGrupo->getDisponibilidadSalon($_POST['Id_aula'],$HoraIni->getId(),$HoraFin->getId(),$_POST['start']);
    if($_POST['Id_examen']>0 && $_POST['Id_examen']==$respOut->getId()){
       //Obtener solo los grupos nuevos
       //Obtener solo los docentes nuevos
        $arrayNewGrupos=array();
        $arrayNewDocentes=array();

       foreach($_POST['Grupos'] as $Id_grupo){
           $grupoExamen=$DaoGrupoExamen->getGrupoExamenByHorario($Id_grupo,$_POST['Id_examen']);
           if($grupoExamen->getId()==0){
               array_push($arrayNewGrupos, $Id_grupo);
           }
       }
       foreach($_POST['Docentes'] as $Id_docente){
           $docenteExamen=$DaoDocenteExamen->getDocenteExamenByHorario($Id_docente,$_POST['Id_examen']);
           if($docenteExamen->getId()==0){
               array_push($arrayNewDocentes, $Id_docente);
           }
       }
       $resp=validarHorario($arrayNewGrupos,$arrayNewDocentes,$_POST['Id_per_examen'],$_POST['start'],$HoraIni->getId(),$HoraFin->getId());
    }else{
        
        //Validar que el aula no este ocupada el dia y la hora
        $respOut=$DaoHorarioExamenGrupo->getDisponibilidadSalon($_POST['Id_aula'],$HoraIni->getId(),$HoraFin->getId(),$_POST['start']);
        if($respOut->getId()>0){
            $claveGrupo="";
            $docenteNombre="";
            foreach($DaoGrupoExamen->getGruposExamenHorario($respOut->getId()) as $ObjGrupo){
                   $grupo=$DaoGrupos->show($ObjGrupo->getIdGrupo());
                   $clave=$DaoGrupos->covertirCadena($grupo->getClave(), 1);
                   $claveGrupo.="   -".$clave."\n";
            } 
            
            foreach($DaoDocenteExamen->getDocentesExamenHorario($respOut->getId()) as $ObjDocente){
                   $docente=$DaoDocentes->show($ObjDocente->getId_docen());
                   $docenteNombre.="    -".$DaoDocentes->covertirCadena($docente->getNombre_docen()." ".$docente->getApellidoP_docen(),1)."\n";
            }
            
            $resp['status']=1;
            $resp['error']="El aula ya se encuentra asignada para impartir exámen\n\nGrupo(s) \n".$claveGrupo."\nDocente(s) \n".$docenteNombre;
           
        }else{
             $resp=validarHorario($_POST['Grupos'],$_POST['Docentes'],$_POST['Id_per_examen'],$_POST['start'],$HoraIni->getId(),$HoraFin->getId());
        }
       
    }
    echo json_encode($resp);
}

function validarHorario($Grupos,$Docentes,$Id_per_examen,$start,$HoraIni,$HoraFin){
     $cadena="";
     $resp=array();
     $resp['status']=0;
     $resp['error']="";
     $DaoHorarioExamenGrupo= new DaoHorarioExamenGrupo();
     $DaoGrupoExamen= new DaoGrupoExamen();
     $DaoDocenteExamen= new DaoDocenteExamen();
     $DaoDocentes=new DaoDocentes();
     $DaoGrupos= new DaoGrupos();
 
     foreach($Grupos as $Id_grupo){
         //Validar que el grupo no haya sido asignado ya
         $Id_horario=$DaoHorarioExamenGrupo->getStatusGrupo($Id_grupo,$Id_per_examen);
         
         if($Id_horario>0){
            $grupo=$DaoGrupos->show($Id_grupo);
            foreach($DaoDocenteExamen->getDocentesExamenHorario($Id_horario) as $doc){ 
                    $docen=$DaoDocentes->show($doc->getId_docen());
                    $resp['status']=1;
                    $cadena.="El grupo ".strtoupper($grupo->getClave())." ya se encuentra asignada para el docente ".$DaoDocentes->covertirCadena($docen->getNombre_docen()." ".$docen->getApellidoP_docen(),2)."\n";
            }
         }else{
             foreach ($Docentes as $Id_docente){
                 //Validar que el docente tenga disponibilidad el dia y la hora
                 $respOut=$DaoHorarioExamenGrupo->validarDisponibilidadDocente($start,$Id_docente,$HoraIni,$HoraFin);
                 if($respOut->getId()>0){
                    $GrupoExamen=$DaoGrupoExamen->getGrupoExamenHorario($respOut->getId());
                    $grupo=$DaoGrupos->show($GrupoExamen->getIdGrupo());
                    if($grupo->getId()!=$Id_grupo){
                      $docen=$DaoDocentes->show($Id_docente);
                      $resp['status']=1;
                      $cadena.=$DaoDocentes->covertirCadena($docen->getNombre_docen()." ".$docen->getApellidoP_docen(),2)." tiene programado exámen entre las sesiones seleccionadas para el grupo ".strtoupper ($grupo->getClave())."\n";  
                    }
                 }
             }
         }
     }
     $resp['error']=$cadena;
     return $resp; 
}

if($_POST['action']=="save_examen"){   
   
    $DaoHorarioExamenGrupo= new DaoHorarioExamenGrupo();
    $DaoHoras= new DaoHoras();
    $DaoDocenteExamen= new DaoDocenteExamen();
    $DaoGrupoExamen= new DaoGrupoExamen();
    
    $start=$_POST['start']."T".$_POST['HoraInicio'];
    $end=$_POST['start']."T".$_POST['HoraFin'];
    
    $IdHoraIni=  substr($_POST['HoraInicio'], 0,2);
    $IdHoraFin=  substr($_POST['HoraFin'], 0,2);
    
    $HoraIni=$DaoHoras->getTextoHora($IdHoraIni);
    $HoraFin=$DaoHoras->getTextoHora($IdHoraFin);

   if($_POST['Id_examen']>0){
        $HorarioExamenGrupo=$DaoHorarioExamenGrupo->show($_POST['Id_examen']);
        $HorarioExamenGrupo->setId_aula($_POST['Id_aula']);
        $HorarioExamenGrupo->setStart($start);
        $HorarioExamenGrupo->setEnd($end);
        $HorarioExamenGrupo->setFechaAplicacion($start);
        $HorarioExamenGrupo->setIdHoraIni($HoraIni->getId());
        $HorarioExamenGrupo->setIdHoraFin($HoraFin->getId());
        $HorarioExamenGrupo->setId_ciclo($_POST['Id_ciclo']);
        $HorarioExamenGrupo->setId_periodoExamen($_POST['Id_per_examen']);
        $DaoHorarioExamenGrupo->update($HorarioExamenGrupo);
        $Id_horario=$_POST['Id_examen'];
   }else{
       $HorarioExamenGrupo= new HorarioExamenGrupo();
        $HorarioExamenGrupo->setId_aula($_POST['Id_aula']);
        $HorarioExamenGrupo->setStart($start);
        $HorarioExamenGrupo->setEnd($end);
        $HorarioExamenGrupo->setFechaAplicacion($start);
        $HorarioExamenGrupo->setDateCreated(date('Y-m-d H:i:s'));
        $HorarioExamenGrupo->setId_usu($_COOKIE['admin/Id_usu']);
        $HorarioExamenGrupo->setIdHoraIni($HoraIni->getId());
        $HorarioExamenGrupo->setIdHoraFin($HoraFin->getId());
        $HorarioExamenGrupo->setId_ciclo($_POST['Id_ciclo']);
        $HorarioExamenGrupo->setId_periodoExamen($_POST['Id_per_examen']);
        $Id_horario=$DaoHorarioExamenGrupo->add($HorarioExamenGrupo);
        
   }
   
   $DaoGrupoExamen->deleteGruposExamen($Id_horario);
   $DaoDocenteExamen->deleteDocentesExamen($Id_horario);
   
    foreach($_POST['Grupos'] as $Id_grupo){
           $GrupoExamen= new GrupoExamen();
           $GrupoExamen->setIdGrupo($Id_grupo);
           $GrupoExamen->setIdHorarioExamen($Id_horario);
           $DaoGrupoExamen->add($GrupoExamen);
    }

    foreach($_POST['Docentes'] as $Id_docen){
           $DocenteExamen= new DocenteExamen();
           $DocenteExamen->setId_docen($Id_docen);
           $DocenteExamen->setIdHorarioExamen($Id_horario);
           $DaoDocenteExamen->add($DocenteExamen);
    }
}




/*
if($_POST['action']=="verificarDisponibilidad"){
    $resp=array();
    $resp['status']=0;
    $resp['error']="";
    $DaoHoras= new DaoHoras();
    $DaoGrupos= new DaoGrupos();
    $DaoDocentes= new DaoDocentes();
    $DaoHorarioExamenGrupo= new DaoHorarioExamenGrupo();
    $IdHoraIni=  substr($_POST['HoraInicio'], 0,2);
    $IdHoraFin=  substr($_POST['HoraFin'], 0,2);
    
    $HoraIni=$DaoHoras->getTextoHora($IdHoraIni);
    $HoraFin=$DaoHoras->getTextoHora($IdHoraFin);
    
     //Validar que el aula no este ocupada el dia y la hora
     $respOut=$DaoHorarioExamenGrupo->validarDisponibilidadDiaHoraSalon($_POST['Id_aula'],$HoraIni->getId(),$HoraFin->getId(),$_POST['start'],$_POST['Id_docen']);
     if($respOut->getId()>0){
        $grupo=$DaoGrupos->show($respOut->getId_grupo());
        $docen=$DaoDocentes->show($respOut->getId_docen());
        $resp['status']=1;
        $resp['error']="El aula ya se encuentra asignada para el exámen del grupo ".strtoupper ($grupo->getClave()).", docente ".$DaoDocentes->covertirCadena($docen->getNombre_docen()." ".$docen->getApellidoP_docen(),2);;
     }else{   
         //Validar que el grupo no haya sido asignado a otro docente
         $respOut=$DaoHorarioExamenGrupo->validarGrupo($_POST['Id_grupo'],$_POST['Id_docen'],$_POST['Id_per_examen']);
         if($respOut->getId()>0){
            $docen=$DaoDocentes->show($respOut->getId_docen());
            $resp['status']=1;
            $resp['error']="El grupo ya se encuentra asignada para el docente ".$DaoDocentes->covertirCadena($docen->getNombre_docen()." ".$docen->getApellidoP_docen(),2);
         }else{
                  //Validar que el docente tenga disponibilidad el dia y la hora
                 $respOut=$DaoHorarioExamenGrupo->validarDisponibilidadDocente($_POST['start'],$_POST['Id_docen'],$HoraIni->getId(),$HoraFin->getId());
                 if($respOut->getId()>0){
                    $grupo=$DaoGrupos->show($respOut->getId_grupo());
                    if($grupo->getId()!=$_POST['Id_grupo']){
                      $resp['status']=1;
                      $resp['error']="El docente tiene programado un exámen entre las horas seleccionadas para el grupo ".strtoupper ($grupo->getClave());  
                    }
                 }else{
                     
                 }
         }
     }
     echo json_encode($resp);
}
*/