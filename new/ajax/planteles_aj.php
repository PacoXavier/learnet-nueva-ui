<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="delete_plantel"){
    $DaoUsuarios= new DaoUsuarios();
    $DaoPlanteles= new DaoPlanteles();
    $DaoOfertasPlantel= new DaoOfertasPlantel();
    $DaoDirecciones= new DaoDirecciones();
    
    $plantel=$DaoPlanteles->show($_POST['Id_plantel']); 
    $plantel->setActivo_plantel(0);
    $DaoPlanteles->update($plantel);
  
    //Crear historial
    $TextoHistorial="Elimina el plantel ".$plantel->getNombre_plantel();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Planteles");
    
    $count=1;
    foreach ($DaoPlanteles->showAll() as $k => $v) {
        $getCiudad_dir="";
        $getEstado_dir="";
        $plantel = $DaoPlanteles->show($v->getId());
        if ($plantel->getId_dir_plantel() > 0) {
            $dir = $DaoDirecciones->show($plantel->getId_dir_plantel());
            $getCalle_dir=$dir->getCalle_dir();
            $getNumExt_dir=$dir->getNumExt_dir();
            $getNumInt_dir=$dir->getNumInt_dir();
            $getColonia_dir=$dir->getColonia_dir();
            $getCiudad_dir=$dir->getCiudad_dir();
            $getEstado_dir=$dir->getEstado_dir();
            $getCp_dir=$dir->getCp_dir();
            $Id_dir=$plantel->getId_dir_plantel();
         }
        ?>
        <tr>
            <td><?php echo $count ?></td>
            <td><?php echo $v->getNombre_plantel() ?></td>
            <td><?php echo $getCiudad_dir;?></td>
            <td><?php echo $getEstado_dir;?></td>
            <td class="td-center"><?php echo count($DaoOfertasPlantel->getOfertasPlantel($v->getId()))?></td>
            <td class="td-center">
                <button onclick="mostrar(<?php echo $v->getId() ?>)">Editar</button>
                <button onclick="delete_plantel(<?php echo $v->getId() ?>)">Eliminar</button>
            </td>
        </tr>
        <?php
        $count++;
    }
}