<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 


if($_POST['action']=="buscar_int"){
    $DaoDocentes= new DaoDocentes();
    foreach($DaoDocentes->buscarDocente($_POST['buscar']) as $k=>$v){
      ?>
        <a href="horario_docente.php?id=<?php echo $v->getId() ?>">
            <li><?php echo $v->getNombre_docen()." ".$v->getApellidoP_docen()." ".$v->getApellidoM_docen() ?></li>
        </a>
      <?php      
    }
}


if ($_POST['action'] == "save_horario_materia") {
    $DaoHorarioDocente= new DaoHorarioDocente();
    $DaoHorarioDocente->deleteGrupoDocente($_POST['Id_grupo'], $_POST['Id_aula'], $_POST['Id_ciclo'], $_POST['Id_docente']);

    foreach ($_POST['Horas_materia'] as $k => $v) {
        //Validamos que alguien mas no tenga clase en ese salon los dias que seleccionamos
        //esto se hace por si de casualidad dos usuarios crean un horario en una misma aula el mismo dia a la misma hora
        //al mismo tiempo
        $ban=0;
        if($v['lunes']==1){
            $query="SELECT * FROM Horario_docente WHERE  Aula=".$_POST['Id_aula']." AND Hora=".$v['hora']." AND Lunes=1 AND Id_ciclo=".$_POST['Id_ciclo']." AND Id_docente!=".$_POST['Id_docente'];
            $horario=$DaoHorarioDocente->getHorarioQuery($query);
            if($horario->getId()>0){
               $ban=1;
            }
        }
        if($v['martes']==1){
            $query="SELECT * FROM Horario_docente WHERE  Aula=".$_POST['Id_aula']." AND Hora=".$v['hora']." AND Martes=1 AND Id_ciclo=".$_POST['Id_ciclo']." AND Id_docente!=".$_POST['Id_docente'];
            $horario=$DaoHorarioDocente->getHorarioQuery($query);
            if($horario->getId()>0){
               $ban=1;
            }
        }
        if($v['miercoles']==1){
            $query="SELECT * FROM Horario_docente WHERE  Aula=".$_POST['Id_aula']." AND Hora=".$v['hora']." AND Miercoles=1 AND Id_ciclo=".$_POST['Id_ciclo']." AND Id_docente!=".$_POST['Id_docente'];
            $horario=$DaoHorarioDocente->getHorarioQuery($query);
            if($horario->getId()>0){
               $ban=1;
            }
        }
        if($v['jueves']==1){
            $query="SELECT * FROM Horario_docente WHERE  Aula=".$_POST['Id_aula']." AND Hora=".$v['hora']." AND Jueves=1 AND Id_ciclo=".$_POST['Id_ciclo']." AND Id_docente!=".$_POST['Id_docente'];
            $horario=$DaoHorarioDocente->getHorarioQuery($query);
            if($horario->getId()>0){
               $ban=1;
            }
        }
        if($v['viernes']==1){
            $query="SELECT * FROM Horario_docente WHERE  Aula=".$_POST['Id_aula']." AND Hora=".$v['hora']." AND Viernes=1 AND Id_ciclo=".$_POST['Id_ciclo']." AND Id_docente!=".$_POST['Id_docente'];
            $horario=$DaoHorarioDocente->getHorarioQuery($query);
            if($horario->getId()>0){
               $ban=1;
            }
        }
        if($v['sabado']==1){
            $query="SELECT * FROM Horario_docente WHERE  Aula=".$_POST['Id_aula']." AND Hora=".$v['hora']." AND Sabado=1 AND Id_ciclo=".$_POST['Id_ciclo']." AND Id_docente!=".$_POST['Id_docente'];
            $horario=$DaoHorarioDocente->getHorarioQuery($query);
            if($horario->getId()>0){
               $ban=1;
            }
        }
        if($v['domingo']==1){
            $query="SELECT * FROM Horario_docente WHERE  Aula=".$_POST['Id_aula']." AND Hora=".$v['hora']." AND Domingo=1 AND Id_ciclo=".$_POST['Id_ciclo']." AND Id_docente!=".$_POST['Id_docente'];
            $horario=$DaoHorarioDocente->getHorarioQuery($query);
            if($horario->getId()>0){
               $ban=1;
            }
        }
        
        if($ban==0){
            $HorarioDocente= new HorarioDocente();
            $HorarioDocente->setId_grupo($_POST['Id_grupo']);
            $HorarioDocente->setAula($_POST['Id_aula']);
            $HorarioDocente->setHora($v['hora']);
            $HorarioDocente->setLunes($v['lunes']);
            $HorarioDocente->setMartes($v['martes']);
            $HorarioDocente->setMiercoles($v['miercoles']);
            $HorarioDocente->setJueves($v['jueves']);
            $HorarioDocente->setViernes($v['viernes']);
            $HorarioDocente->setSabado($v['sabado']);
            $HorarioDocente->setDomingo($v['domingo']);
            $HorarioDocente->setId_ciclo($_POST['Id_ciclo']);
            $HorarioDocente->setId_docente($_POST['Id_docente']);
            $DaoHorarioDocente->add($HorarioDocente);
        }
        
    }
    mostrar_horario($_POST['Id_docente'],$_POST['Id_ciclo']);
}


if ($_POST['action'] == "show_horario_aula") {
   update_page($_POST['Id_docente'], $_POST['Id_grupo'], $_POST['Id_aula'], $_POST['Id_mat'], $_POST['Id_ciclo']);
}


function update_page($Id_docente, $Id_grupo, $Id_aula,$Id_mat,$Id_ciclo) {
    $base= new base();
    $DaoDocentes= new DaoDocentes();
    $DaoHoras= new DaoHoras();
    $DaoCiclos= new DaoCiclos();
    $DaoDiasPlantel= new DaoDiasPlantel();
    $DaoTurnos= new DaoTurnos();
    $DaoGrupos= new DaoGrupos();
    $DaoAulas= new DaoAulas();
    $DaoUsuarios= new DaoUsuarios();
    
    $_usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $DaoMaterias= new DaoMaterias();
    $matD=$DaoMaterias->show($Id_mat);
    ?>
    <div id="box_uno">
        <ul class="form">
            <li>Ciclo<br>
                <select id="ciclo" <?php if ($Id_docente > 0) { ?> onchange="get_materias_ciclo_docente()" <?php }?>>
                    <option value="0">Selecciona el ciclo</option>
                    <?php
                        foreach($DaoCiclos->showAll() as $ciclo){
                         ?>
                             <option value="<?php echo $ciclo->getId() ?>" <?php if($Id_ciclo==$ciclo->getId()){?> selected="selected" <?php }?> ><?php echo $ciclo->getClave() ?></option>
                          <?php
                          }
                          ?>
                    </select>
            </li>
            <li>Materia<br><select id="list_materias" <?php if ($Id_docente > 0) { ?> onchange="get_grupos_ciclo_docente()" <?php } ?>>
                <option value="0">Selecciona la materia</option>
                    <?php
                    if($Id_mat>0){
                         $query  = "SELECT * FROM Materias_docentes 
                                        JOIN materias_uml ON materias_uml.Id_mat =Materias_docentes.Id_materia
                                        JOIN ciclos_ulm ON ciclos_ulm.Id_ciclo=Materias_docentes.Id_ciclo  
                                    WHERE Id_profesor=".$Id_docente."  AND Materias_docentes.Id_ciclo=".$Id_ciclo;
                         foreach($base->advanced_query($query) as $row_Materias_docentes){
                              ?>
                              <option value="<?php echo $row_Materias_docentes['Id_mat']?>" 
                                  <?php if($Id_mat==$row_Materias_docentes['Id_mat']){ ?> selected="selected" <?php }?> >
                                      <?php echo $row_Materias_docentes['Clave_mat']." - ".$row_Materias_docentes['Nombre']?>
                              </option>
                              <?php
                         }
                    }
                    ?>
            </select>
            <li>Grupos<br>
                <select id="list_grupos" <?php if ($Id_docente > 0) { ?> onchange="show_horario_aula()" <?php } ?>>
                    <option value="0">Selecciona el grupo</option>
                    <?php
                    if($Id_grupo>0){
                      foreach($DaoGrupos->getGruposByMateria($Id_mat,$Id_ciclo) as $grupo){
                                $turno = $DaoTurnos->show($grupo->getTurno());
                          ?>
                               <option value="<?php echo $grupo->getId()?>"
                               <?php if($Id_grupo==$grupo->getId()){ ?> selected="selected" <?php }?> >
                                   <?php echo $grupo->getClave()."-".$turno->getNombre()?>
                               </option>
                          <?php
                      }
                    }
                    ?>
                </select>
            </li>
            <li>Aula<br><select id="list_aulas" <?php if ($Id_docente > 0) { ?>onchange="show_horario_aula()" <?php } ?>>
                    <option value="0">Selecciona el aula</option>
                    <?php
                    foreach($DaoAulas->showAll() as $aula){
                            ?>
                            <option value="<?php echo $aula->getId(); ?>" <?php if ($Id_aula == $aula->getId()) { ?> selected="selected" <?php } ?> ><?php echo $aula->getNombre_aula() ?></option>
                            <?php
                    }
                    ?>
                </select>
            </li>
        </ul>     
        <div id="box_colores">
            <ul>
                <li><div id="horario_profesor"> </div>Sesiones asignadas al docente</li>
                <li><div id="sin_desponibilidad"> </div>Sesiones no disponibles </li>
                <li><div id="horario_salon"> </div>Salon sin disponibilidad </li>
                <?php
                if($Id_grupo>0){
                ?>
                <li style="color: red;cursor: pointer;"><span style="cursor: pointer;" onclick="deleteGrupo()"><i class="fa fa-trash"></i> Eliminar grupo</span> </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </div>
    <div id="box_dos">
        <table id="list_usu">
            <thead>
                <tr>
                    <td>Sesiones</td>
                    <?php
                    foreach($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia){
                        ?>
                          <td><?php echo $dia['Nombre']?></td>
                        <?php
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
    <?php
    //Seleccionamos las horas diponibles del profesor
    $paramDias= array();
    $countHorasPorMateria=0;
    $query = "SELECT * FROM Horas 
                    LEFT JOIN (
                               SELECT Lunes,Martes,Miercoles,Jueves,Viernes,Sabado,Domingo,Id_docente,Id_hora as Id_hora_dispo 
                                     FROM Disponibilidad_docente 
                               WHERE Id_docente=" . $Id_docente . " AND Id_ciclo=".$Id_ciclo."
                               ) as Disponibilidad ON Horas.Id_hora= Disponibilidad.Id_hora_dispo 
                    ORDER BY Horas.Id_hora ASC";
    foreach($base->advanced_query($query) as $row_Horas){
            
            foreach($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia) {
                    $paramDias[$dia['Id_dia']]['disp_docente'] = "";
                    $paramDias[$dia['Id_dia']]['nombre_materia'] = "";
                    $paramDias[$dia['Id_dia']]['aula_materia'] = "";
                    $paramDias[$dia['Id_dia']]['Id_grupo'] = "";
                    $paramDias[$dia['Id_dia']]['Id_mat'] = "";
                    $paramDias[$dia['Id_dia']]['Id_aula'] = "";
                    $paramDias[$dia['Id_dia']]['clave'] = "";
                    $paramDias[$dia['Id_dia']]['Nodisponibilidad'] = "";
                    $paramDias[$dia['Id_dia']]['Disabled'] = "";
                    $paramDias[$dia['Id_dia']]['DiaUtilizado'] = "";
                    $paramDias[$dia['Id_dia']]['CheckedCheckboxMat'] = "";
                    $paramDias[$dia['Id_dia']]['DisabledCheckBoxDocen'] = "";
                    $paramDias[$dia['Id_dia']]['DisponibilidadAula'] = "";
                    $paramDias[$dia['Id_dia']]['DisabledAula'] = "";
            }
            
            //Horas no disponibles del docente en rojo
            //Desabilitar checkbox
            if ($row_Horas['Lunes'] == 0) {
                $paramDias[1]['Nodisponibilidad'] = 'class="sin_desponibilidad"';
                $paramDias[1]['Disabled'] = 'disabled="disabled"';  
            }
            if ($row_Horas['Martes'] == 0) {
                $paramDias[2]['Nodisponibilidad'] = 'class="sin_desponibilidad"';
                $paramDias[2]['Disabled'] = 'disabled="disabled"';
            }
            if ($row_Horas['Miercoles'] == 0) {
                $paramDias[3]['Nodisponibilidad'] = 'class="sin_desponibilidad"';
                $paramDias[3]['Disabled'] = 'disabled="disabled"';
            }
            if ($row_Horas['Jueves'] == 0) {
                $paramDias[4]['Nodisponibilidad'] = 'class="sin_desponibilidad"';
                $paramDias[4]['Disabled'] = 'disabled="disabled"';
            }
            if ($row_Horas['Viernes'] == 0) {
                $paramDias[5]['Nodisponibilidad'] = 'class="sin_desponibilidad"';
                $paramDias[5]['Disabled'] = 'disabled="disabled"';
            }
            if ($row_Horas['Sabado'] == 0) {
                $paramDias[6]['Nodisponibilidad'] = 'class="sin_desponibilidad"';
                $paramDias[6]['Disabled'] = 'disabled="disabled"';
            }
            if ($row_Horas['Domingo'] == 0) {
                $paramDias[7]['Nodisponibilidad'] = 'class="sin_desponibilidad"';
                $paramDias[7]['Disabled'] = 'disabled="disabled"';
            }
                                
            //Horas ulitizadas por el docente
            $query = "SELECT * FROM Horario_docente WHERE Hora= " . $row_Horas['Id_hora']." AND  Id_ciclo=".$Id_ciclo." AND Id_docente=".$Id_docente;
            foreach($base->advanced_query($query) as $row_Horario_docente){
                    //$disp_docente_lunes= verde
                    if ($row_Horario_docente['Lunes'] == 1) {
                        $paramDias[1]['DiaUtilizado'] = 'class="horas_docente_asignadas"';
                        $paramDias[1]['CheckedCheckboxMat'] = 'checked="checked"';
                        if ($row_Horario_docente['Aula'] != $Id_aula || $row_Horario_docente['Id_grupo'] != $Id_grupo) {
                            $paramDias[1]['DisabledCheckBoxDocen'] = 'disabled="disabled"';
                            
                        }
                        //Contamos cuntas horas a utilizado por grupo o materia(ya que tiene restriccion la materia en numerode horas)
                        if($row_Horario_docente['Id_grupo']==$Id_grupo){
                            $countHorasPorMateria++;
                        }
                    }
                    if ($row_Horario_docente['Martes'] == 1) {
                        $paramDias[2]['DiaUtilizado'] = 'class="horas_docente_asignadas"';
                        $paramDias[2]['CheckedCheckboxMat'] = 'checked="checked"';
                        if ($row_Horario_docente['Aula'] != $Id_aula || $row_Horario_docente['Id_grupo'] != $Id_grupo) {
                            $paramDias[2]['DisabledCheckBoxDocen'] = 'disabled="disabled"';
                        }
                        //Contamos cuntas horas a utilizado por grupo o materia(ya que tiene restriccion la materia en numerode horas)
                        if($row_Horario_docente['Id_grupo']==$Id_grupo){
                            $countHorasPorMateria++;
                        }
                    }
                    if ($row_Horario_docente['Miercoles'] == 1) {
                        $paramDias[3]['DiaUtilizado'] = 'class="horas_docente_asignadas"';
                        $paramDias[3]['CheckedCheckboxMat'] = 'checked="checked"';
                        if ($row_Horario_docente['Aula'] != $Id_aula || $row_Horario_docente['Id_grupo'] != $Id_grupo) {
                           $paramDias[3]['DisabledCheckBoxDocen'] = 'disabled="disabled"';
                        }
                        //Contamos cuntas horas a utilizado por grupo o materia(ya que tiene restriccion la materia en numerode horas)
                        if($row_Horario_docente['Id_grupo']==$Id_grupo){
                            $countHorasPorMateria++;
                        }
                    }
                    if ($row_Horario_docente['Jueves'] == 1) {
                        $paramDias[4]['DiaUtilizado'] = 'class="horas_docente_asignadas"';
                        $paramDias[4]['CheckedCheckboxMat'] = 'checked="checked"';
                        if ($row_Horario_docente['Aula'] != $Id_aula || $row_Horario_docente['Id_grupo'] != $Id_grupo) {
                            $paramDias[4]['DisabledCheckBoxDocen'] = 'disabled="disabled"';
                        }
                         //Contamos cuntas horas a utilizado por grupo o materia(ya que tiene restriccion la materia en numerode horas)
                        if($row_Horario_docente['Id_grupo']==$Id_grupo){
                            $countHorasPorMateria++;
                        }
                    }
                    if ($row_Horario_docente['Viernes'] == 1) {
                        $paramDias[5]['DiaUtilizado'] = 'class="horas_docente_asignadas"';
                        $paramDias[5]['CheckedCheckboxMat'] = 'checked="checked"';
                        if ($row_Horario_docente['Aula'] != $Id_aula || $row_Horario_docente['Id_grupo'] != $Id_grupo) {
                            $paramDias[5]['DisabledCheckBoxDocen'] = 'disabled="disabled"';
                        }
                        //Contamos cuntas horas a utilizado por grupo o materia(ya que tiene restriccion la materia en numerode horas)
                        if($row_Horario_docente['Id_grupo']==$Id_grupo){
                            $countHorasPorMateria++;
                        }
                    }
                    if ($row_Horario_docente['Sabado'] == 1) {
                        $paramDias[6]['DiaUtilizado'] = 'class="horas_docente_asignadas"';
                        $paramDias[6]['CheckedCheckboxMat'] = 'checked="checked"';
                        if ($row_Horario_docente['Aula'] != $Id_aula || $row_Horario_docente['Id_grupo'] != $Id_grupo) {
                            $paramDias[6]['DisabledCheckBoxDocen'] = 'disabled="disabled"';
                        }
                        //Contamos cuntas horas a utilizado por grupo o materia(ya que tiene restriccion la materia en numerode horas)
                        if($row_Horario_docente['Id_grupo']==$Id_grupo){
                            $countHorasPorMateria++;
                        }
                    }
                    if ($row_Horario_docente['Domingo'] == 1) {
                        $paramDias[7]['DiaUtilizado'] = 'class="horas_docente_asignadas"';
                        $paramDias[7]['CheckedCheckboxMat'] = 'checked="checked"';
                        if ($row_Horario_docente['Aula'] != $Id_aula || $row_Horario_docente['Id_grupo'] != $Id_grupo) {
                            $paramDias[7]['DisabledCheckBoxDocen'] = 'disabled="disabled"';
                        }
                        //Contamos cuntas horas a utilizado por grupo o materia(ya que tiene restriccion la materia en numerode horas)
                        if($row_Horario_docente['Id_grupo']==$Id_grupo){
                            $countHorasPorMateria++;
                        }
                    }
            }
            
            //Obtenemos las horas en las que ya ha sido ocupada el aula 
            //Por otras materias que no son del profesor
            $query = "SELECT * FROM Horario_docente WHERE Hora= " . $row_Horas['Id_hora'] . " AND  Aula =" . $Id_aula . " AND Id_ciclo=".$Id_ciclo." AND Id_docente!=".$Id_docente;
            foreach($base->advanced_query($query) as $row_Horario_aula){
                    if ($row_Horario_aula['Lunes'] == 1 ) {
                        $paramDias[1]['DisponibilidadAula'] = 'class="horas_salon"';
                        if(strlen($paramDias[1]['CheckedCheckboxMat'])==0){
                            $paramDias[1]['DisabledAula'] = 'disabled="disabled"';
                        }
                    }
                    if ($row_Horario_aula['Martes'] == 1) {
                        $paramDias[2]['DisponibilidadAula'] = 'class="horas_salon"';
                        if(strlen($paramDias[2]['CheckedCheckboxMat'])==0){
                           $paramDias[2]['DisabledAula'] = 'disabled="disabled"';
                        }
                    }
                    if ($row_Horario_aula['Miercoles'] == 1) {
                        $paramDias[3]['DisponibilidadAula'] = 'class="horas_salon"';
                        if(strlen($paramDias[3]['CheckedCheckboxMat'])==0){
                           $paramDias[3]['DisabledAula'] = 'disabled="disabled"';
                        }
                    }
                    if ($row_Horario_aula['Jueves'] == 1) {
                        $paramDias[4]['DisponibilidadAula'] = 'class="horas_salon"';
                        if(strlen($paramDias[4]['CheckedCheckboxMat'])==0){
                          $paramDias[4]['DisabledAula'] = 'disabled="disabled"';
                        }
                    }
                    if ($row_Horario_aula['Viernes'] == 1) {
                        $paramDias[5]['DisponibilidadAula'] = 'class="horas_salon"';
                        if(strlen($paramDias[5]['CheckedCheckboxMat'])==0){
                           $paramDias[5]['DisabledAula'] = 'disabled="disabled"';
                        }
                    }
                    if ($row_Horario_aula['Sabado'] == 1) {
                        $paramDias[6]['DisponibilidadAula'] = 'class="horas_salon"';
                        if(strlen($paramDias[6]['CheckedCheckboxMat'])==0){
                           $paramDias[6]['DisabledAula'] = 'disabled="disabled"';
                        }
                    }
                    if ($row_Horario_aula['Domingo'] == 1) {
                        $paramDias[7]['DisponibilidadAula'] = 'class="horas_salon"';
                        if(strlen($paramDias[7]['CheckedCheckboxMat'])==0){
                           $paramDias[7]['DisabledAula'] = 'disabled="disabled"';
                        }
                    }
            }
            //Prioridad para el bloqueo de horas
            //1.en rojo horas no asignadas en disponibilidad
            //2.en verde las horas que ya fueron asignadas
            //3.en gris las horas en las que ya esta ocupado el salon
             ?>   
                <tr id_hora="<?php echo $row_Horas['Id_hora'] ?>">
                    <td><?php echo $row_Horas['Texto_hora'] ?></td>
                    <?php
                    foreach ($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia) {
                        $id = $dia['Id_dia'];
                        ?>
                        <td <?php echo $paramDias[$id]['Nodisponibilidad'] . " " . $paramDias[$id]['DiaUtilizado'] . " " . $paramDias[$id]['DisponibilidadAula'] ?> >
                            <input type="checkbox" class="<?php echo $dia['Abreviacion']?>" <?php echo $paramDias[$id]['Disabled'] . " " . $paramDias[$id]['CheckedCheckboxMat'] . " " . $paramDias[$id]['DisabledCheckBoxDocen'] . " " . $paramDias[$id]['DisabledAula'] ?> onchange="validarHorasChecadas(this)"/>
                        </td>
                        <?php
                    }
                   ?>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>
<p style="padding-bottom: 0px;"><button onclick="save_horario_materia()">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
<input type="hidden" id="horasPorSemanaMateria" value="<?php echo $matD->getHoras_porSemana()?>"/>
<input type="hidden" id="countHorasPorMateria" value="<?php echo $countHorasPorMateria?>"/>
    <?php
}

if($_POST['action']=="get_materias_ciclo_docente"){
    ?>
     <option value="0">Selecciona la materia</option> 
     <?php
     $base= new base();
     $query  = "SELECT * FROM Materias_docentes 
                    JOIN materias_uml ON materias_uml.Id_mat =Materias_docentes.Id_materia
                    JOIN ciclos_ulm ON ciclos_ulm.Id_ciclo=Materias_docentes.Id_ciclo  
                WHERE Id_profesor=".$_POST['Id_docente']."  AND Materias_docentes.Id_ciclo=".$_POST['Id_ciclo'];
     foreach($base->advanced_query($query) as $row_Materias_docentes){
        ?>
          <option value="<?php echo $row_Materias_docentes['Id_mat']?>"><?php echo $row_Materias_docentes['Clave_mat']." - ".$row_Materias_docentes['Nombre']?></option>
        <?php
 
     }
}

if($_POST['action']=="get_grupos_ciclo_docente"){
    ?>
     <option value="0">Selecciona el grupo</option> 
    <?php
    //Seleccionamos las grupos que no han sido asigandos a algun docente
    // y ademas que son a los que pertenecen las materias asignadas al docente
    $base= new base();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $query  = "SELECT * FROM Grupos 
                    WHERE Id_mat=".$_POST['Id_mat']." 
               AND Id_plantel=".$usu->getId_plantel()." 
               AND Id_ciclo=".$_POST['Id_ciclo']." 
               AND Id_grupo NOT IN (
                                     SELECT Id_grupo 
                                            FROM Horario_docente 
                                     WHERE Id_docente !=".$_POST['Id_docente']." 
                                           AND Id_ciclo=".$_POST['Id_ciclo']."
                                   ) 
               AND Activo_grupo=1 
               ORDER BY Id_grupo DESC";
    foreach($base->advanced_query($query) as $row_Grupos){
       ?>
       <option value="<?php echo $row_Grupos['Id_grupo']?>"><?php echo $row_Grupos['Clave']?></option>
       <?php
    }
}


if($_POST['action']=="mostrar_box_horario"){
    ?>
       <div id="box_emergente" class="box_horario">
           <?php
          update_page($_POST['Id_docente'], $_POST['Id_grupo'], $_POST['Id_aula'], $_POST['Id_mat'], $_POST['Id_ciclo']);
           ?>
       </div>
       <?php
}


if($_POST['action']=="mostrar_horario"){
  mostrar_horario($_POST['Id_docente'],$_POST['Id_ciclo']);
}

function mostrar_horario($Id_docente,$Id_ciclo){
    $base= new base();
    $DaoDocentes= new DaoDocentes();
    $DaoCiclos= new DaoCiclos();
    $DaoDiasPlantel= new DaoDiasPlantel();
    $DaoUsuarios= new DaoUsuarios();
    
    $_usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    ?>
    <tr>
    <td id="column_one">
        <div class="fondo">
            <div id="box_top">
                <h1>Horario del docente</h1>
            </div>
            <div class="seccion" id="box_info_horario">
                <div id="box_uno">
                    <?php
                    if ($Id_docente > 0) {
                        $docente = $DaoDocentes->show($Id_docente);
                        $nombre = $docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . " " . $docente->getApellidoM_docen();
                    }
                    ?>
                    <ul class="form">
                        <li>Docente<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o clave" value="<?php echo $nombre ?>"/>
                            <ul id="buscador_int"></ul>
                        </li>
                        <?php
                        if ($Id_docente > 0) {
                            ?>
                            <li>Ciclo<br><select id="ciclo" onchange="mostrar_horario()">
                                    <option value="0">Selecciona el ciclo</option>
                                    <?php
                                        foreach($DaoCiclos->showAll() as $ciclo){
                                         ?>
                                            <option value="<?php echo $ciclo->getId() ?>" <?php if($Id_ciclo==$ciclo->getId()){?> selected="selected" <?php }?> ><?php echo $ciclo->getClave() ?></option>
                                          <?php
                                          }
                                          ?>
                                    </select>
                            </li> 
                        <?php
                        }
                        ?>
                </div>
                <table id="tabla_horario">
                        <thead>
                            <tr>
                                <td>Sesiones</td>
                                <?php
                                foreach($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia){
                                    ?>
                                      <td><?php echo $dia['Nombre']?></td>
                                    <?php
                                }
                                ?>
                            </tr>
                        </thead>
                            <?php
                            //Seleccionamos las horas diponibles del profesor
                            $paramDias = array();
                            $query  = "SELECT * FROM Horas 
                                         LEFT JOIN (
                                                     SELECT Lunes,Martes,Miercoles,Jueves,Viernes,Sabado,Domingo,Id_docente,Id_hora as Id_hora_dispo
                                                            FROM Disponibilidad_docente 
                                                     WHERE Id_docente=".$Id_docente." AND Id_ciclo=".$Id_ciclo."
                                                   ) as Disponibilidad  
                                      ON Horas.Id_hora= Disponibilidad.Id_hora_dispo 
                                      ORDER BY Horas.Id_hora ASC";
                            foreach($base->advanced_query($query) as $row_Horas){

                                foreach($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia) {
                                        $paramDias[$dia['Id_dia']]['disp_docente'] = "";
                                        $paramDias[$dia['Id_dia']]['nombre_materia'] = "";
                                        $paramDias[$dia['Id_dia']]['aula_materia'] = "";
                                        $paramDias[$dia['Id_dia']]['Id_grupo'] = "";
                                        $paramDias[$dia['Id_dia']]['Id_mat'] = "";
                                        $paramDias[$dia['Id_dia']]['Id_aula'] = "";
                                        $paramDias[$dia['Id_dia']]['clave'] = "";
                                        $paramDias[$dia['Id_dia']]['disponibilidad'] = "";
                                }

                                if ($row_Horas['Lunes'] == 1) {
                                    $paramDias[1]['disponibilidad'] = "disponibilidad";
                                }
                                if ($row_Horas['Martes'] == 1) {
                                    $paramDias[2]['disponibilidad'] = "disponibilidad";
                                }
                                if ($row_Horas['Miercoles'] == 1) {
                                    $paramDias[3]['disponibilidad'] = "disponibilidad";
                                }
                                if ($row_Horas['Jueves'] == 1) {
                                    $paramDias[4]['disponibilidad'] = "disponibilidad";
                                }
                                if ($row_Horas['Viernes'] == 1) {
                                    $paramDias[5]['disponibilidad'] = "disponibilidad";
                                }
                                if ($row_Horas['Sabado'] == 1) {
                                    $paramDias[6]['disponibilidad'] = "disponibilidad";
                                }
                                if ($row_Horas['Domingo'] == 1) {
                                    $paramDias[7]['disponibilidad'] = "disponibilidad";
                                }
        
                                  //Horas ulitizadas por el docente
                                  $query="SELECT HD.*,Gp.Clave,Gp.Id_mat,Materias.Nombre,aulas.Nombre_aula,aulas.Clave_aula FROM Horario_docente as HD 
                                   JOIN Grupos as Gp ON HD.Id_grupo=Gp.Id_grupo 
                                   JOIN materias_uml as Materias ON Gp.Id_mat= Materias.Id_mat
                                   JOIN aulas ON aulas.Id_aula= HD.Aula
                                   WHERE Hora= ".$row_Horas['Id_hora']." AND HD.Id_ciclo=".$Id_ciclo." AND Id_docente=".$Id_docente;
                                  foreach($base->advanced_query($query) as $row_Horario_docente){

                                        if ($row_Horario_docente['Lunes'] == 1) {
                                            $paramDias[1]['disp_docente'] = 'horas_docente_asignadas';
                                            $paramDias[1]['nombre_materia'] = $row_Horario_docente['Nombre'];
                                            $paramDias[1]['aula_materia'] = $row_Horario_docente['Nombre_aula'];
                                            $paramDias[1]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                                            $paramDias[1]['Id_mat'] = $row_Horario_docente['Id_mat'];
                                            $paramDias[1]['Id_aula'] = $row_Horario_docente['Aula'];
                                            $paramDias[1]['clave'] = $row_Horario_docente['Clave'];
                                        }
                                        if ($row_Horario_docente['Martes'] == 1) {
                                            $paramDias[2]['disp_docente'] = 'horas_docente_asignadas';
                                            $paramDias[2]['nombre_materia'] = $row_Horario_docente['Nombre'];
                                            $paramDias[2]['aula_materia'] = $row_Horario_docente['Nombre_aula'];
                                            $paramDias[2]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                                            $paramDias[2]['Id_mat'] = $row_Horario_docente['Id_mat'];
                                            $paramDias[2]['Id_aula'] = $row_Horario_docente['Aula'];
                                            $paramDias[2]['clave'] = $row_Horario_docente['Clave'];
                                        }
                                        if ($row_Horario_docente['Miercoles'] == 1) {
                                            $paramDias[3]['disp_docente'] = 'horas_docente_asignadas';
                                            $paramDias[3]['nombre_materia'] = $row_Horario_docente['Nombre'];
                                            $paramDias[3]['aula_materia'] = $row_Horario_docente['Nombre_aula'];
                                            $paramDias[3]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                                            $paramDias[3]['Id_mat'] = $row_Horario_docente['Id_mat'];
                                            $paramDias[3]['Id_aula'] = $row_Horario_docente['Aula'];
                                            $paramDias[3]['clave'] = $row_Horario_docente['Clave'];
                                        }
                                        if ($row_Horario_docente['Jueves'] == 1) {
                                            $paramDias[4]['disp_docente'] = 'horas_docente_asignadas';
                                            $paramDias[4]['nombre_materia'] = $row_Horario_docente['Nombre'];
                                            $paramDias[4]['aula_materia'] = $row_Horario_docente['Nombre_aula'];
                                            $paramDias[4]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                                            $paramDias[4]['Id_mat'] = $row_Horario_docente['Id_mat'];
                                            $paramDias[4]['Id_aula'] = $row_Horario_docente['Aula'];
                                            $paramDias[4]['clave'] = $row_Horario_docente['Clave'];
                                        }
                                        if ($row_Horario_docente['Viernes'] == 1) {
                                            $paramDias[5]['disp_docente'] = 'horas_docente_asignadas';
                                            $paramDias[5]['nombre_materia'] = $row_Horario_docente['Nombre'];
                                            $paramDias[5]['aula_materia'] = $row_Horario_docente['Nombre_aula'];
                                            $paramDias[5]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                                            $paramDias[5]['Id_mat'] = $row_Horario_docente['Id_mat'];
                                            $paramDias[5]['Id_aula'] = $row_Horario_docente['Aula'];
                                            $paramDias[5]['clave'] = $row_Horario_docente['Clave'];
                                        }
                                        if ($row_Horario_docente['Sabado'] == 1) {
                                            $paramDias[6]['disp_docente'] = 'horas_docente_asignadas';
                                            $paramDias[6]['nombre_materia'] = $row_Horario_docente['Nombre'];
                                            $paramDias[6]['aula_materia'] = $row_Horario_docente['Nombre_aula'];
                                            $paramDias[6]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                                            $paramDias[6]['Id_mat'] = $row_Horario_docente['Id_mat'];
                                            $paramDias[6]['Id_aula'] = $row_Horario_docente['Aula'];
                                            $paramDias[6]['clave'] = $row_Horario_docente['Clave'];
                                        }

                                        if ($row_Horario_docente['Domingo'] == 1) {
                                            $paramDias[7]['disp_docente'] = 'horas_docente_asignadas';
                                            $paramDias[7]['nombre_materia'] = $row_Horario_docente['Nombre'];
                                            $paramDias[7]['aula_materia'] = $row_Horario_docente['Nombre_aula'];
                                            $paramDias[7]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                                            $paramDias[7]['Id_mat'] = $row_Horario_docente['Id_mat'];
                                            $paramDias[7]['Id_aula'] = $row_Horario_docente['Aula'];
                                            $paramDias[7]['clave'] = $row_Horario_docente['Clave'];
                                        }
                                 }
                            ?>
                                     <tr id_hora="<?php echo $row_Horas['Id_hora']?>">
                                           <td><?php echo $row_Horas['Texto_hora']?></td>
                                           <?php
                                            foreach ($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia) {
                                                $id = $dia['Id_dia'];
                                                ?>
                                                <td class="<?php echo $paramDias[$id]['disponibilidad']." ".$paramDias[$id]['disp_docente']?>" <?php if(strlen($paramDias[$id]['nombre_materia'])>0){ ?> onclick="mostrar_box_horario(<?php echo $paramDias[$id]['Id_grupo']?>,<?php echo $paramDias[$id]['Id_mat'];?>,<?php echo $paramDias[$id]['Id_aula'];?>,<?php echo $Id_ciclo?>)" <?php } ?> >
                                                    <span><?php echo $paramDias[$id]['nombre_materia'];?></span><br><?php echo $paramDias[$id]['aula_materia']?><br>
                                                    <span><?php echo $paramDias[$id]['clave'];?></span>
                                                </td>
                                                <?php
                                            }
                                           ?>
                                     </tr>
                                   <?php
                            }
                            ?>
                      </tbody>
                    </table>
                </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once '../estandares/menu_derecho.php';
                ?>
                <ul>
                    <?php
                    if ($Id_docente > 0) {
                        ?>
                        <li><span onclick="mostrar_box_horario(0,0,0,0)">Asignar grupo </span></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </td>
    </tr>
    <?php
}


if ($_POST['action'] == "deleteGrupo") {
    //Si existen alumno activos asignados al grupo, no se podra eliminar
    $base= new base();
    $query = "SELECT ofertas_alumno.*,Id_grupo FROM materias_ciclo_ulm
    JOIN ciclos_alum_ulm ON materias_ciclo_ulm.Id_ciclo_alum=ciclos_alum_ulm.Id_ciclo_alum
    JOIN ofertas_alumno ON ciclos_alum_ulm.Id_ofe_alum=ofertas_alumno.Id_ofe_alum
    WHERE Id_grupo=".$_POST['Id_grupo']." AND Activo_oferta=1";
    $totalRows_Grupos  = $base->getTotalRows($query);
    if($totalRows_Grupos==0){
        $DaoHorarioDocente= new DaoHorarioDocente();
        $DaoHorarioDocente->deleteGrupoDocente($_POST['Id_grupo'], $_POST['Id_aula'], $_POST['Id_ciclo'], $_POST['Id_docente']);
        mostrar_horario($_POST['Id_docente'],$_POST['Id_ciclo']);
    }else{
       echo "Actualmente el grupo se encuentra con ".$totalRows_Grupos." alumnos asignados\nEliminelos para borrarlo.";
    }
}
