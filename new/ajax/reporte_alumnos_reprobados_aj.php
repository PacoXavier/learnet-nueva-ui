<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="filtro"){

    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoMediosEnterar= new DaoMediosEnterar();
    $DaoCiclos= new DaoCiclos();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoGrupos= new DaoGrupos();
    $DaoDocentes= new DaoDocentes();
    $DaoMaterias= new DaoMaterias();
    $DaoBecas = new DaoBecas();

    $ciclo=$DaoCiclos->getActual();

    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $queryx="";

    $Id_ciclo=$ciclo->getId();
    if($_POST['Id_ciclo']>0){
       $Id_ciclo=$_POST['Id_ciclo'];
    }
    
    if($_POST['Id_docente']>0){
        $queryx=$queryx." AND Docentes.Id_docen=".$_POST['Id_docente'];
    } 
    if($_POST['Id_grupo']>0){
        $queryx=$queryx." AND Grupos.Id_grupo=".$_POST['Id_grupo'];
    } 
    if($_POST['Calificacion']>0){
        $queryx=$queryx." AND materias_ciclo_ulm.CalTotalParciales=".$_POST['Calificacion'];
    }   
    
    
    
    /*
    if($_POST['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_POST['Id_ofe'];
    }
    if($_POST['Id_esp']>0){
        $query=$query." AND Id_esp=".$_POST['Id_esp'];
    }
    if($_POST['Id_ori']>0){
        $query=$query." AND Id_ori=".$_POST['Id_ori'];
    }
   */
    

    $count=1;
    $query='
        SELECT 
                ciclos_ulm.Clave AS ClaveCiclo,
                ciclos_alum_ulm.Id_ciclo,
                Grupos.Clave,Grupos.Turno,
                CASE 
                     WHEN Grupos.Turno=1 THEN "MATUTINO" 
                     WHEN Grupos.Turno=2 THEN "VESPERTINO" 
                     WHEN Grupos.Turno=3 THEN "NOCTURNO" 
                END AS Turno,
                materias_uml.Nombre,materias_uml.Promedio_min,
                materias_ciclo_ulm.CalEspecial, materias_ciclo_ulm.CalExtraordinario, ROUND(materias_ciclo_ulm.CalTotalParciales) AS CalificacionTotalParciales,
                inscripciones_ulm.Matricula,inscripciones_ulm.Nombre_ins,inscripciones_ulm.ApellidoP_ins, inscripciones_ulm.ApellidoM_ins,inscripciones_ulm.Id_plantel,inscripciones_ulm.Id_ins,
                Docentes.Nombre_docen, Docentes.ApellidoP_docen,Docentes.ApellidoM_docen
                FROM ciclos_alum_ulm 
         JOIN ciclos_ulm ON ciclos_alum_ulm.Id_ciclo=ciclos_ulm.Id_ciclo
         JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
         JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
         JOIN materias_uml ON Materias_especialidades.Id_mat=materias_uml.Id_mat
         JOIN inscripciones_ulm ON materias_ciclo_ulm.Id_alum=inscripciones_ulm.Id_ins
         JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
         JOIN Grupos ON materias_ciclo_ulm.Id_grupo=Grupos.Id_grupo
         JOIN Horario_docente ON Grupos.Id_grupo=Horario_docente.Id_grupo
         JOIN Docentes ON Horario_docente.Id_docente=Docentes.Id_docen
         WHERE ciclos_alum_ulm.Id_ciclo='.$Id_ciclo.'
               AND materias_ciclo_ulm.Activo=1
               AND inscripciones_ulm.Id_plantel='.$usu->getId_plantel().'
               AND inscripciones_ulm.tipo=1 
               AND ofertas_alumno.Activo_oferta=1 
               AND ofertas_alumno.Baja_ofe IS NULL
               AND ofertas_alumno.FechaCapturaEgreso IS NULL
               AND ofertas_alumno.IdCicloEgreso IS NULL
               AND ofertas_alumno.IdUsuEgreso IS NULL
               AND Round(materias_ciclo_ulm.CalTotalParciales) < materias_uml.Promedio_min
               '.$queryx.'
         GROUP BY Grupos.Clave,inscripciones_ulm.Id_ins';

    foreach ($base->advanced_query($query) as $k=>$v){

         ?>
                 <tr id_alum="<?php echo $v['Id_ins'];?>">
                   <td><?php echo $count;?></td>
                   <td><?php echo $v['ClaveCiclo'] ?></td>
                   <td><?php echo $v['Clave'] ?></td>
                   <td><?php echo $v['Turno'] ?></td>
                   <td><?php echo $v['Nombre_docen'] . " " . $v['ApellidoP_docen'] . " " . $v['ApellidoM_docen'] ?></td>
                   <td><?php echo $v['Nombre'] ?></td>
                   <td><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                   <td class="td-center"><?php echo $v['Promedio_min'] ?></td>
                   <td class="td-center"><?php echo $v['CalificacionTotalParciales'] ?></td>
                   <td class="td-center"><input type="checkbox"> </td>
                 </tr>
                 <?php
                 $count++;
        }
}




if($_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'CICLO');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'GRUPO');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'TURNO');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'DOCENTE');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'MATERIA');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'ALUMNO');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'PROMEDIO MÍNIMO');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'CALIFICACIÓN OBTENIDA');
 							   
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','I') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoMediosEnterar= new DaoMediosEnterar();
    $DaoCiclos= new DaoCiclos();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoGrupos= new DaoGrupos();
    $DaoDocentes= new DaoDocentes();
    $DaoMaterias= new DaoMaterias();

    $ciclo=$DaoCiclos->getActual();

    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $queryx="";

    $Id_ciclo=$ciclo->getId();
    if($_GET['Id_ciclo']>0){
       $Id_ciclo=$_GET['Id_ciclo'];
    }
    
    if($_GET['Id_docente']>0){
        $queryx=$queryx." AND Docentes.Id_docen=".$_GET['Id_docente'];
    } 
    if($_GET['Id_grupo']>0){
        $queryx=$queryx." AND Grupos.Id_grupo=".$_GET['Id_grupo'];
    } 
    if($_GET['Calificacion']>0){
        $queryx=$queryx." AND materias_ciclo_ulm.CalTotalParciales=".$_GET['Calificacion'];
    }   
    
    $count=1;
    $query='
        SELECT 
                ciclos_ulm.Clave AS ClaveCiclo,
                ciclos_alum_ulm.Id_ciclo,
                Grupos.Clave,Grupos.Turno,
                CASE 
                     WHEN Grupos.Turno=1 THEN "MATUTINO" 
                     WHEN Grupos.Turno=2 THEN "VESPERTINO" 
                     WHEN Grupos.Turno=3 THEN "NOCTURNO" 
                END AS Turno,
                materias_uml.Nombre,materias_uml.Promedio_min,
                materias_ciclo_ulm.CalEspecial, materias_ciclo_ulm.CalExtraordinario, ROUND(materias_ciclo_ulm.CalTotalParciales) AS CalificacionTotalParciales,
                inscripciones_ulm.Matricula,inscripciones_ulm.Nombre_ins,inscripciones_ulm.ApellidoP_ins, inscripciones_ulm.ApellidoM_ins,inscripciones_ulm.Id_plantel,inscripciones_ulm.Id_ins,
                Docentes.Nombre_docen, Docentes.ApellidoP_docen,Docentes.ApellidoM_docen
                FROM ciclos_alum_ulm 
         JOIN ciclos_ulm ON ciclos_alum_ulm.Id_ciclo=ciclos_ulm.Id_ciclo
         JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
         JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
         JOIN materias_uml ON Materias_especialidades.Id_mat=materias_uml.Id_mat
         JOIN inscripciones_ulm ON materias_ciclo_ulm.Id_alum=inscripciones_ulm.Id_ins
         JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
         JOIN Grupos ON materias_ciclo_ulm.Id_grupo=Grupos.Id_grupo
         JOIN Horario_docente ON Grupos.Id_grupo=Horario_docente.Id_grupo
         JOIN Docentes ON Horario_docente.Id_docente=Docentes.Id_docen
         WHERE ciclos_alum_ulm.Id_ciclo='.$Id_ciclo.'
               AND materias_ciclo_ulm.Activo=1
               AND inscripciones_ulm.Id_plantel='.$usu->getId_plantel().'
               AND inscripciones_ulm.tipo=1 
               AND ofertas_alumno.Activo_oferta=1 
               AND ofertas_alumno.Baja_ofe IS NULL
               AND ofertas_alumno.FechaCapturaEgreso IS NULL
               AND ofertas_alumno.IdCicloEgreso IS NULL
               AND ofertas_alumno.IdUsuEgreso IS NULL
               AND Round(materias_ciclo_ulm.CalTotalParciales) < materias_uml.Promedio_min
               '.$queryx.'
         GROUP BY Grupos.Clave,inscripciones_ulm.Id_ins';

    foreach ($base->advanced_query($query) as $k=>$v){

                    $count++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
                    $objPHPExcel->getActiveSheet()->setCellValue("B$count", $v['ClaveCiclo']);
                    $objPHPExcel->getActiveSheet()->setCellValue("C$count", $v['Clave']);
                    $objPHPExcel->getActiveSheet()->setCellValue("D$count", $v['Turno']);
                    $objPHPExcel->getActiveSheet()->setCellValue("E$count", $v['Nombre_docen'] . " " . $v['ApellidoP_docen'] . " " . $v['ApellidoM_docen']);
                    $objPHPExcel->getActiveSheet()->setCellValue("F$count", $v['Nombre']);
                    $objPHPExcel->getActiveSheet()->setCellValue("G$count", $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins']);
                    $objPHPExcel->getActiveSheet()->setCellValue("H$count", $v['Promedio_min']);
                    $objPHPExcel->getActiveSheet()->setCellValue("I$count", $v['CalificacionTotalParciales']);
        }  


    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=AlumnosReprobados-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}




