<?php
require_once('activate_error.php');
require_once('../require_daos.php');

if($_POST['action']=="filtro"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoCiclos= new DaoCiclos();
    $DaoMaterias= new DaoMaterias();
    $DaoDocentes= new DaoDocentes();
    $DaoGrupos= new DaoGrupos();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoTurnos= new DaoTurnos();
    $DaoEvaluacionDocente = new DaoEvaluacionDocente();
    $DaoPagosCiclo= new DaoPagosCiclo();
  
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $ciclo=$DaoCiclos->getActual();

    $query=" Grupos.Id_ciclo=".$ciclo->getId();
    $ClaveCiclo=$ciclo->getClave();
    if($_POST['Id_ciclo']>0){
        $query="  Grupos.Id_ciclo=".$_POST['Id_ciclo'];
        $ciclo=$DaoCiclos->show($_POST['Id_ciclo']);
    }

    if($_POST['Id_turno']>0){
        $query=$query." AND Turno=".$_POST['Id_turno'];
    }
    if($_POST['Id_mat']>0){
        $query=$query." AND Grupos.Id_mat=".$_POST['Id_mat'];
    }
    
    if($_POST['Id_ofe']>0){
        $query=$query." AND especialidades_ulm.Id_ofe=".$_POST['Id_ofe'];
    }
    
    if($_POST['Id_esp']>0){
        $query=$query." AND especialidades_ulm.Id_esp=".$_POST['Id_esp'];
    }
    if($_POST['Id_turno']>0){
        $query=$query." AND Turno=".$_POST['Id_turno'];
    }
    $query_Grupos  = "SELECT * , Grupos.Id_grupo AS Grupo, especialidades_ulm.Id_ofe ,especialidades_ulm.Id_esp FROM Grupos 
            LEFT JOIN Materias_especialidades ON Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
            LEFT JOIN especialidades_ulm ON Materias_especialidades.Id_esp=especialidades_ulm.Id_esp
            LEFT JOIN Horario_docente ON Grupos.Id_grupo=Horario_docente.Id_grupo
            LEFT JOIN Docentes ON Horario_docente.Id_docente=Docentes.Id_docen
            WHERE ".$query." AND Grupos.Id_plantel=".$usu->getId_plantel()." AND Activo_grupo=1 GROUP BY Grupos.Id_grupo ORDER BY Grupos.Id_grupo DESC";
    foreach($base->advanced_query($query_Grupos) as $k=>$v){
        $mat = $DaoMaterias->show($v['Id_mat']);
        $mat_esp=$DaoMateriasEspecialidad->show($v['Id_mat_esp']);

        $NombreMat=$mat->getNombre();
        if(strlen($mat_esp->getNombreDiferente())>0){
            $NombreMat=$mat_esp->getNombreDiferente(); 
        }     
        
        $tur = $DaoTurnos->show($v['Turno']);
        $turno=$tur->getNombre();
                                        
        $nombre_ori="";
        if($v['Id_ori']>0){
          $ori = $DaoOrientaciones->show($v['Id_ori']);
          $nombre_ori=$ori->getNombre();
        }


         $ingresos=0;
         $TotalAlumnos=count($DaoGrupos->getAlumnosGrupo($v['Grupo']));
         foreach($DaoGrupos->getAlumnosBYGrupo($v['Grupo']) as $k2=>$v2){
            $pagoCiclo=$DaoPagosCiclo->getPagoByIdCicloAlumn($v2['Id_ciclo_alum']);
            if($pagoCiclo->getId()>0){
               $totalMaterias = count($DaoMateriasCicloAlumno->getMateriasByIdOfeAlumnAndIdCiclo($v2['Id_ofe_alum'], $ciclo->getId())) . "<br>";
               $ingresos+=round($pagoCiclo->getMensualidad() - $pagoCiclo->getDescuento() - $pagoCiclo->getDescuento_beca()) / $totalMaterias;   
            }
         }

         $egreso=0;
         $horas_trabajadas=0;
         $Id_oferta=0;
         $Nombre_docen="";
         $resp=$DaoGrupos->getDocenteGrupo($v['Grupo']);
         if($resp['Id_docente']>0){    
            $query_Horario_docente = " SELECT 
                     Horario_docente.*,
                     Grupos.Clave,Grupos.Id_mat_esp,
                     Materias_especialidades.Id_esp,
                     especialidades_ulm.Id_ofe
                from Horario_docente 
                JOIN Grupos ON Horario_docente.Id_grupo=Grupos.Id_grupo
                JOIN Materias_especialidades ON  Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
                JOIN especialidades_ulm ON Materias_especialidades.Id_esp=especialidades_ulm.Id_esp
                WHERE Horario_docente.Id_docente=".$resp['Id_docente']." AND Horario_docente.Id_ciclo=".$ciclo->getId()." AND Horario_docente.Id_grupo=".$v['Grupo'];
            foreach($base->advanced_query($query_Horario_docente) as $k6=>$v6 ){
                    $Id_oferta=$v6['Id_ofe'];
                    //Insertamos solo los dias que da el profesor
                    if($v6['Lunes']==1){
                        $horas_trabajadas++;
                    }
                    if($v6['Martes']==1){
                        $horas_trabajadas++;
                    }
                    if($v6['Miercoles']==1){
                        $horas_trabajadas++;
                    }
                    if($v6['Jueves']==1){
                        $horas_trabajadas++;
                    }
                    if($v6['Viernes']==1){
                       $horas_trabajadas++;
                    }
                    if($v6['Sabado']==1){
                       $horas_trabajadas++;
                    }
                    if ($v6['Domingo'] == 1) {
                        $horas_trabajadas++;
                    }
            }

            //Es por mes
            $horas_trabajadas=($horas_trabajadas*4);
            $docen=$DaoDocentes->show($resp['Id_docente']);
            $Nombre_docen=$docen->getNombre_docen()." ".$docen->getApellidoP_docen()." ".$docen->getApellidoM_docen();
            $eva = $DaoEvaluacionDocente->getInfoEvaluacion($docen->getId(), $Id_oferta);
            $egreso = $horas_trabajadas * $eva['PagoHora'];
         }
         $classPrimer="";
         if($ingresos<$egreso){
            $classPrimer='class="pink"'; 
         }elseif(($ingresos/2)<$egreso){
            $classPrimer='class="yellow"';
         }
         //rojo si no se alcaza a pagar al profe
         //amarillo si no contribulle gastos fijos
        ?>
        <tr <?php echo $classPrimer;?>>
            <td><?php echo $v['Clave']?> </td>
            <td><?php echo $ciclo->getClave()?> </td>
            <td style="width: 120px;"><?php echo $NombreMat;?></td>
            <td><?php echo $nombre_ori;?></td>
            <td><?php echo $turno;?></td>
            <td style="text-align: center;"><?php echo $v['Capacidad']?></td>
            <td><?php echo $Nombre_docen?></td>
            <td style="color:black;font-weight: bold;text-align: center;"><?php echo $TotalAlumnos;?></td>
            <td style="text-align: center;"><b>$<?php echo number_format($ingresos,2);?></b></td>
            <td style="text-align: center;"><b>$<?php echo number_format($egreso,2);?></b></td>
        </tr>
        <?php
}
}


if($_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'CLAVE GRUPAL');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'CICLO');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'MATERIA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'ORIENTACIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'TURNO');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'CAPACIDAD');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'DOCENTE');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'ALUMNOS');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'INGRESO/MES');
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'EGRESO/MES');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','K') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoCiclos= new DaoCiclos();
    $DaoMaterias= new DaoMaterias();
    $DaoDocentes= new DaoDocentes();
    $DaoGrupos= new DaoGrupos();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoTurnos= new DaoTurnos();
    $DaoEvaluacionDocente = new DaoEvaluacionDocente();
    $DaoPagosCiclo= new DaoPagosCiclo();

    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $ciclo=$DaoCiclos->getActual();

    $query=" Grupos.Id_ciclo=".$ciclo->getId();
    $ClaveCiclo=$ciclo->getClave();
    if($_GET['Id_ciclo']>0){
        $query="  Grupos.Id_ciclo=".$_GET['Id_ciclo'];
        $ciclo=$DaoCiclos->show($_GET['Id_ciclo']);
    }

    if($_GET['Id_turno']>0){
        $query=$query." AND Turno=".$_GET['Id_turno'];
    }
    if($_GET['Id_mat']>0){
        $query=$query." AND Grupos.Id_mat=".$_GET['Id_mat'];
    }
    
    if($_GET['Id_ofe']>0){
        $query=$query." AND especialidades_ulm.Id_ofe=".$_GET['Id_ofe'];
    }
    
    if($_GET['Id_esp']>0){
        $query=$query." AND especialidades_ulm.Id_esp=".$_GET['Id_esp'];
    }
    if($_GET['Id_turno']>0){
        $query=$query." AND Turno=".$_GET['Id_turno'];
    }
    $count=1;
    $query_Grupos  = "SELECT * , Grupos.Id_grupo AS Grupo, especialidades_ulm.Id_ofe ,especialidades_ulm.Id_esp FROM Grupos 
            LEFT JOIN Materias_especialidades ON Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
            LEFT JOIN especialidades_ulm ON Materias_especialidades.Id_esp=especialidades_ulm.Id_esp
            LEFT JOIN Horario_docente ON Grupos.Id_grupo=Horario_docente.Id_grupo
            LEFT JOIN Docentes ON Horario_docente.Id_docente=Docentes.Id_docen
            WHERE ".$query." AND Grupos.Id_plantel=".$usu->getId_plantel()." AND Activo_grupo=1 GROUP BY Grupos.Id_grupo ORDER BY Grupos.Id_grupo DESC";
    foreach($base->advanced_query($query_Grupos) as $k=>$v){
        $mat = $DaoMaterias->show($v['Id_mat']);
        $mat_esp=$DaoMateriasEspecialidad->show($v['Id_mat_esp']);

        $NombreMat=$mat->getNombre();
        if(strlen($mat_esp->getNombreDiferente())>0){
            $NombreMat=$mat_esp->getNombreDiferente(); 
        }     

        $tur = $DaoTurnos->show($v['Turno']);
        $turno=$tur->getNombre();
        
        $nombre_ori="";
        if($v['Id_ori']>0){
          $ori = $DaoOrientaciones->show($v['Id_ori']);
          $nombre_ori=$ori->getNombre();
        }


         $ingresos=0;
         $TotalAlumnos=count($DaoGrupos->getAlumnosGrupo($v['Grupo']));
         foreach($DaoGrupos->getAlumnosBYGrupo($v['Grupo']) as $k2=>$v2){
                $pagoCiclo=$DaoPagosCiclo->getPagoByIdCicloAlumn($v2['Id_ciclo_alum']);
                if($pagoCiclo->getId()>0){
                   $totalMaterias = count($DaoMateriasCicloAlumno->getMateriasByIdOfeAlumnAndIdCiclo($v2['Id_ofe_alum'], $ciclo->getId())) . "<br>";
                   $ingresos+=round($pagoCiclo->getMensualidad() - $pagoCiclo->getDescuento() - $pagoCiclo->getDescuento_beca()) / $totalMaterias;   
                }
         }

         $egreso=0;
         $horas_trabajadas=0;
         $Id_oferta=0;
         $Nombre_docen="";
         $resp=$DaoGrupos->getDocenteGrupo($v['Grupo']);
         if($resp['Id_docente']>0){    
            $query_Horario_docente = " SELECT 
                     Horario_docente.*,
                     Grupos.Clave,Grupos.Id_mat_esp,
                     Materias_especialidades.Id_esp,
                     especialidades_ulm.Id_ofe
                from Horario_docente 
                JOIN Grupos ON Horario_docente.Id_grupo=Grupos.Id_grupo
                JOIN Materias_especialidades ON  Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
                JOIN especialidades_ulm ON Materias_especialidades.Id_esp=especialidades_ulm.Id_esp
                WHERE Horario_docente.Id_docente=".$resp['Id_docente']." AND Horario_docente.Id_ciclo=".$ciclo->getId()." AND Horario_docente.Id_grupo=".$v['Grupo'];
            foreach($base->advanced_query($query_Horario_docente) as $k6=>$v6 ){
                    $Id_oferta=$v6['Id_ofe'];
                    //Insertamos solo los dias que da el profesor
                    if($v6['Lunes']==1){
                        $horas_trabajadas++;
                    }
                    if($v6['Martes']==1){
                        $horas_trabajadas++;
                    }
                    if($v6['Miercoles']==1){
                        $horas_trabajadas++;
                    }
                    if($v6['Jueves']==1){
                        $horas_trabajadas++;
                    }
                    if($v6['Viernes']==1){
                       $horas_trabajadas++;
                    }
                    if($v6['Sabado']==1){
                       $horas_trabajadas++;
                    }
                    if ($v6['Domingo'] == 1) {
                        $horas_trabajadas++;
                    }
            }

            //Es por mes
            $horas_trabajadas=($horas_trabajadas*4);
            $docen=$DaoDocentes->show($resp['Id_docente']);
            $Nombre_docen=$docen->getNombre_docen()." ".$docen->getApellidoP_docen()." ".$docen->getApellidoM_docen();
            $eva = $DaoEvaluacionDocente->getInfoEvaluacion($docen->getId(), $Id_oferta);
            $egreso = $horas_trabajadas * $eva['PagoHora'];
         }
         $classPrimer="";
         if($ingresos<$egreso){
            $classPrimer='class="pink"'; 
         }elseif(($ingresos/2)<$egreso){
            $classPrimer='class="yellow"';
         }
         //rojo si no se alcaza a pagar al profe
         //amarillo si no contribulle gastos fijos
         
        $count++;
        $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
        $objPHPExcel->getActiveSheet()->setCellValue("B$count", $v['Clave']);
        $objPHPExcel->getActiveSheet()->setCellValue("C$count", $ciclo->getClave());
        $objPHPExcel->getActiveSheet()->setCellValue("D$count", $NombreMat);
        $objPHPExcel->getActiveSheet()->setCellValue("E$count", $nombre_ori);
        $objPHPExcel->getActiveSheet()->setCellValue("F$count", $turno);
        $objPHPExcel->getActiveSheet()->setCellValue("G$count", $v['Capacidad']);
        $objPHPExcel->getActiveSheet()->setCellValue("H$count", $Nombre_docen);
        $objPHPExcel->getActiveSheet()->setCellValue("I$count", $TotalAlumnos);
        $objPHPExcel->getActiveSheet()->setCellValue("J$count", "$".number_format($ingresos,2));
        $objPHPExcel->getActiveSheet()->setCellValue("K$count", "$".number_format($egreso,2));
    }
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=Rentabilidad-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}



