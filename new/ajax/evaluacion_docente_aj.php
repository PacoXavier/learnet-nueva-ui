<?php
require_once('activate_error.php');
require_once('../require_daos.php');

if($_POST['action']=="updatePuntosTotales"){
    ?>
        <li><span>Puntos:   <b id="puntos_totales"><?php echo round($_POST['puntosTotales']); ?> </b></span></li>
    <?php
    $DaoCategoriasPago= new DaoCategoriasPago();
    $DaoNivelesPago= new DaoNivelesPago();
    $Data=array();
    foreach ($DaoCategoriasPago->getCategoriasPago() as $cat) {
        $Id_nivel="";
        $Nombre_nivel="";
        foreach ($DaoNivelesPago->getNivelesCategoria($cat->getId()) as $nivel) {
                if(strlen($nivel->getPuntosMin())>0 && $_POST['puntosTotales']>=$nivel->getPuntosMin() && strlen($nivel->getPuntosMax())>0 && $_POST['puntosTotales']<=$nivel->getPuntosMax()){
                   $Id_nivel=$nivel->getId();
                   $Nombre_nivel=$nivel->getNombre_nivel();
                }elseif(strlen($nivel->getPuntosMin())>0 && $_POST['puntosTotales']>=$nivel->getPuntosMin() && strlen($nivel->getPuntosMax())==0){
                   $Id_nivel=$nivel->getId();
                   $Nombre_nivel=$nivel->getNombre_nivel();
                }
        }
        ?>
        <li><span><?php echo $cat->getNombre_tipo() ?>   <b><?php echo $Nombre_nivel; ?></b></span><input type="hidden" class="resultado_nivel_por_categoria"  value="<?php echo $Id_nivel; ?>"/></li>
        <?php
    }
    if ($_POST['Id_docente'] > 0) {
        ?>
        <li><a href="evaluaciones_docente.php?id=<?php echo $_POST['Id_docente']; ?>">Regresar</a></li>
        <?php
    }
}



if($_POST['action']=="save_evaluacion"){
    $DaoEvaluacionDocente= new DaoEvaluacionDocente();
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $DaoCiclos= new DaoCiclos();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $ciclo=$DaoCiclos->show($_POST['Id_ciclo']);
    $doc=$DaoDocentes->show($_POST['Id_docente'] );
    if($_POST['Id_eva']>0){
        $EvaluacionDocente=$DaoEvaluacionDocente->show($_POST['Id_eva']);
        $EvaluacionDocente->setComentarios($_POST['Comentarios']);
        $EvaluacionDocente->setId_usu_evaluador($usu->getId());
        $EvaluacionDocente->setPuntosTotales($_POST['puntosTotales']);
        $DaoEvaluacionDocente->update($EvaluacionDocente);
        
        //Historial
        $TextoHistorial = "Actualiza evaluación ".$ciclo->getClave()." para " . $doc->getNombre_docen(). " " . $doc->getApellidoP_docen() . " " . $doc->getApellidoM_docen();
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Evaluación docente");

        $Id_eva=$_POST['Id_eva'];
        $resp['tipo']="update";
        $resp['id']=$Id_eva;
    }else{
        $EvaluacionDocente= new EvaluacionDocente();
        $EvaluacionDocente->setDateCreated(date('Y-m-d'));
        $EvaluacionDocente->setComentarios($_POST['Comentarios']);
        $EvaluacionDocente->setId_ciclo($_POST['Id_ciclo']);
        $EvaluacionDocente->setId_doc($_POST['Id_docente']);
        $EvaluacionDocente->setId_usu_evaluador($usu->getId());
        $EvaluacionDocente->setPuntosTotales($_POST['puntosTotales']);
        $Id_eva=$DaoEvaluacionDocente->add($EvaluacionDocente);
        
        $resp['tipo']="new";
        $resp['id']=$Id_eva;

        //Historial
        $TextoHistorial = "Captura evaluación ".$ciclo->getClave()." para " . $doc->getNombre_docen(). " " . $doc->getApellidoP_docen() . " " . $doc->getApellidoM_docen();
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Evaluación docente");
    }
   
    //Insertamos los valores de los campos evaluados
    $DaoResultadosEvaluacion= new DaoResultadosEvaluacion();
    $DaoResultadosEvaluacion->deleteCamposPorEvaluacion($Id_eva);
    foreach($_POST['CategoriasEvaluadas'] as $cat){
        foreach($cat['inputs'] as $input){
            $ResultadosEvaluacion= new ResultadosEvaluacion();
            $ResultadosEvaluacion->setId_camp($input['id']);
            $ResultadosEvaluacion->setId_eva($Id_eva);
            $ResultadosEvaluacion->setPuntos($input['punto']);
            $DaoResultadosEvaluacion->add($ResultadosEvaluacion);      
        }
    }
    
    $DaoPuntosPorCategoriaEvaluadaDocente=new DaoPuntosPorCategoriaEvaluadaDocente();
    $DaoPuntosPorCategoriaEvaluadaDocente->deletePorEvaluacion($Id_eva);
    foreach($_POST['NivelesObtenidos'] as $nivel){
        $PuntosPorCategoriaEvaluadaDocente= new PuntosPorCategoriaEvaluadaDocente();
        $PuntosPorCategoriaEvaluadaDocente->setId_eva($Id_eva);
        $PuntosPorCategoriaEvaluadaDocente->setId_nivel($nivel['id']);
        $DaoPuntosPorCategoriaEvaluadaDocente->add($PuntosPorCategoriaEvaluadaDocente);
    }
    echo json_encode($resp);
}


