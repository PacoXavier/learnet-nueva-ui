<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 


if($_POST['action']=="buscarUsuarioPrestamo"){
    $base= new base();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
  ?>
      <li><span>Usuarios</span></li>
      <?php
        $query = "SELECT *, CONCAT(IFNULL(Nombre_usu,''),IFNULL(ApellidoP_usu,''),IFNULL(ApellidoM_usu,'')) AS Buscar  
        FROM usuarios_ulm HAVING Buscar LIKE '%".$_POST['buscar']."%' AND Baja_usu IS NULL AND Id_plantel=".$usu['Id_plantel'];	
        foreach($base->advanced_query($query) as $row_BuscarUsu){
          ?>
               <li id_usu="<?php echo $row_BuscarUsu['Id_usu']?>" tipo_usu="usu"><?php echo $row_BuscarUsu['Clave_usu']." - ".$row_BuscarUsu['Nombre_usu']."  ".$row_BuscarUsu['ApellidoP_usu']."  ".$row_BuscarUsu['ApellidoM_usu']?></li>
          <?php
        }
        ?>
      <li><span>Docentes</span></li>
       <?php
        $query = "SELECT *, CONCAT(IFNULL(Nombre_docen,''),IFNULL(ApellidoP_docen,''),IFNULL(ApellidoM_docen,'')) AS Buscar  
          FROM Docentes HAVING Buscar LIKE '%".$_POST['buscar']."%' AND Baja_docen IS NULL AND Id_plantel=".$usu['Id_plantel'];	
        foreach($base->advanced_query($query) as $row_BuscarDocen){
          ?>
           <li id_usu="<?php echo $row_BuscarDocen['Id_docen']?>" tipo_usu="docen"><?php echo $row_BuscarDocen['Clave_docen']." - ".$row_BuscarDocen['Nombre_docen']." ".$row_BuscarDocen['ApellidoP_docen']." ".$row_BuscarDocen['ApellidoM_docen']?></li>
          <?php
         }
         ?>
      <li><span>Alumnos</span></li>
       <?php
       $query = "SELECT *, CONCAT(IFNULL(Nombre_ins,''),IFNULL(ApellidoP_ins,''),IFNULL(ApellidoM_ins,'')) AS Buscar  
      FROM inscripciones_ulm  LEFT JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
      HAVING Buscar LIKE '%".$_POST['buscar']."%' AND tipo=1 AND Activo_alum=1 AND Id_plantel=".$usu['Id_plantel']." ORDER BY Id_ins DESC";	
      foreach($base->advanced_query($query) as $row_BusquedaPro){
        ?>
            <li id_usu="<?php echo $row_BusquedaPro['Id_ins']?>" tipo_usu="alum"><?php echo $row_BusquedaPro['Matricula']." - ".$row_BusquedaPro['Nombre_ins'] . " " . $row_BusquedaPro['ApellidoP_ins'] . " " . $row_BusquedaPro['ApellidoM_ins']?></li>
        <?php

      }
}


if($_POST['action']=="buscar_int"){
    $base= new base();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query = "SELECT * FROM Activos  WHERE Nombre  LIKE  '%".$_POST['buscar']."%' OR Modelo LIKE '%".$_POST['buscar']."%' AND Baja_activo IS NULL AND Id_plantel=".$usu->getId_plantel()." LIMIT 20";
    foreach($base->advanced_query($query) as $row_Activos){
        ?>
           <a href="prestamo_activos.php?id=<?php echo $row_Activos['Id_activo']?>">
                <li><?php echo $row_Activos['Modelo']." - ".$row_Activos['Nombre']?></li>
           </a>
        <?php
    }
}



if ($_POST['action']=="box_prestamo"){
?>
<div id="box_emergente"><h1>Capturar prestamo</h1>
    <span class="linea"></span>
       <p>Solicitante <br><input type="text" id="usuario_recibe" onkeyup="buscarUsuarioPrestamo()"/></p>
        <ul id="buscador_prestamo"></ul>
        <p>Fecha de devoluci&oacute;n<br><input type="date" id="fecha_devolucion"/></p>
        <p style="padding-bottom: 20px;">Estado de entrega<br>
            <textarea id="Estado_entrega"></textarea>
        </p>
       <p><button onclick="save_prestamo()">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
</div>
<?php
}

if ($_POST['action']=="box_recepcion"){
    $DaoActivos= new DaoActivos();
    $DaoDocentes= new DaoDocentes();
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $DaoHistorialPrestamoActivo= new DaoHistorialPrestamoActivo();

    $activo = $DaoActivos->show($_POST['Id_activo']);

    $codigo="";
    $nombre="";
    if($activo->getDisponible()==0){
         $hist=$DaoHistorialPrestamoActivo->getLastHistoriaActivo($_POST['Id_activo']);
         if($hist->getId()>0){
             $Id_hist=$hist->getId();
             $Fecha_entrega=$hist->getFecha_entrega();
             $Fecha_devolucion=$hist->getFecha_devolucion();

             if ($hist->getTipo_usu() == "usu" && $hist->getUsu_recibe()>0) {
                 $usuRecibe = $DaoUsuarios->show($hist->getUsu_recibe());
                 $nombre = $usuRecibe->getNombre_usu() . " " . $usuRecibe->getApellidoP_usu() . " " . $usuRecibe->getApellidoM_usu();
                 $tipo_usu = "Usuario";
                 $codigo = $usuRecibe->getClave_usu();
             } elseif ($hist->getTipo_usu() == "docen" && $hist->getUsu_recibe()>0) {
                 $usuRecibe = $DaoDocentes->show($hist->getUsu_recibe());
                 $nombre = $usuRecibe->getNombre_docen() . " " . $usuRecibe->getApellidoP_docen() . " " . $usuRecibe->getApellidoM_docen();
                 $tipo_usu = "Docente";
                 $codigo = $usuRecibe->getClave_docen();
             } elseif ($hist->getTipo_usu() == "alum" && $hist->getUsu_recibe()>0) {
                 $usuRecibe= $DaoAlumnos->show($historial->getUsu_recibe());
                 $nombre = $usuRecibe->getNombre() . " " . $usuRecibe->getApellidoP() . " " . $usuRecibe->getApellidoM();
                 $tipo_usu = "Alumno";
                 $codigo = $usuRecibe->getMatricula();
             }
         }
     }
?>
<div id="box_emergente"><h1>Capturar prestamo</h1>
    <span class="linea"></span>
      <p>C&oacute;digo<br><input type="text" disabled="disabled" value="<?php echo $codigo?>"/></p>
       <p>Solicitante<br><input type="text" disabled="disabled" value="<?php echo strtoupper($nombre)?>"/></p>
        <p style="padding-bottom: 20px;">Estado de  entrega<br><textarea id="Estado_entrega"></textarea></p>
       <p><button onclick="save_recepcion()">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
</div>
<?php
}



if ($_POST['action'] == "save_prestamo") {
    $DaoActivos= new DaoActivos();
    $DaoHistorialPrestamoActivo= new DaoHistorialPrestamoActivo();

    $activo=$DaoActivos->show($_POST['Id_activo']);
    $activo->setDisponible(0);
    $DaoActivos->update($activo);
    
    $HistorialPrestamoActivo= new HistorialPrestamoActivo();
    $HistorialPrestamoActivo->setUsu_entrega($_COOKIE['admin/Id_usu']);
    $HistorialPrestamoActivo->setUsu_recibe($_POST['usuario_recibe']);
    $HistorialPrestamoActivo->setFecha_entrega(date('Y-m-d'));
    $HistorialPrestamoActivo->setFecha_devolucion($_POST['fecha_devolucion']);
    $HistorialPrestamoActivo->setEstado_entrega($_POST['Estado_entrega']);
    $HistorialPrestamoActivo->setId_activo($_POST['Id_activo']);
    $HistorialPrestamoActivo->setTipo_usu($_POST['tipo']);
    $DaoHistorialPrestamoActivo->add($HistorialPrestamoActivo);
           
    $DaoUsuarios= new DaoUsuarios();
    $TextoHistorial = "Presto el activo ".$activo->getCodigo()." - ".$activo->getNombre(); 
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Préstamo de activos"); 
    
    update_page($_POST['Id_activo']);
}



if ($_POST['action'] == "save_recepcion") {
    $DaoActivos= new DaoActivos();
    $DaoHistorialPrestamoActivo= new DaoHistorialPrestamoActivo();

    $activo=$DaoActivos->show($_POST['Id_activo']);
    $activo->setDisponible(1);
    $DaoActivos->update($activo);
    
    $hist=$DaoHistorialPrestamoActivo->show($_POST['Id_prestamo']);
    $hist->setEstado_recibe($_POST['Estado_entrega']);
    $hist->setFecha_entregado(date('Y-m-d'));
    $DaoHistorialPrestamoActivo->update($hist);
           
    $DaoUsuarios= new DaoUsuarios();
    $TextoHistorial = "Recibió el activo ".$activo->getCodigo()." - ".$activo->getNombre(); 
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Préstamo de activos"); 
    
    update_page($_POST['Id_activo']);
}



function update_page($Id_activo){
    $DaoUsuarios= new DaoUsuarios();
    $DaoActivos= new DaoActivos();
    $DaoDocentes= new DaoDocentes();
    $DaoAlumnos= new DaoAlumnos();
    $DaoHistorialPrestamoActivo= new DaoHistorialPrestamoActivo();

    $Id_hist=0;
    $Fecha_entrega="";
    $Fecha_devolucion="";
    $disponible="";

    $_usu->$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    ?>
      <table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-keyboard-o"></i> Préstamo activos</h1>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <li>Buscar Activo<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o modelo"/>
                            <ul id="buscador_int"></ul>
                        </li>
                    </ul>
                </div>
                <?php
                if (isset($Id_activo) && $Id_activo > 0) {
                    $activo = $DaoActivos->show($Id_activo);
                    $Id_activo=$activo->getId();
                    
                    if ($activo->getDisponible() == 1) {
                        $disponible=1;
                        $disponibilidad = "Disponible";
                        $style = 'style="color:green;"';
                    } else {
                        $disponible=0;
                        $disponibilidad = "No Disponible";
                        $style = 'style="color:red;"';

                        $hist=$DaoHistorialPrestamoActivo->getLastHistoriaActivo($Id_activo);
                        if($hist->getId()>0){
                            $Id_hist=$hist->getId();
                            $Fecha_entrega=$hist->getFecha_entrega();
                            $Fecha_devolucion=$hist->getFecha_devolucion();
                            
                            if ($hist->getTipo_usu() == "usu" && $hist->getUsu_recibe()>0) {
                                $usuRecibe = $DaoUsuarios->show($hist->getUsu_recibe());
                                $nombre = $usuRecibe->getNombre_usu() . " " . $usuRecibe->getApellidoP_usu() . " " . $usuRecibe->getApellidoM_usu();
                                $tipo_usu = "Usuario";
                                $codigo = $usuRecibe->getClave_usu();
                            } elseif ($hist->getTipo_usu() == "docen" && $hist->getUsu_recibe()>0) {
                                $usuRecibe = $DaoDocentes->show($hist->getUsu_recibe());
                                $nombre = $usuRecibe->getNombre_docen() . " " . $usuRecibe->getApellidoP_docen() . " " . $usuRecibe->getApellidoM_docen();
                                $tipo_usu = "Docente";
                                $codigo = $usuRecibe->getClave_docen();
                            } elseif ($hist->getTipo_usu() == "alum" && $hist->getUsu_recibe()>0) {
                                $usuRecibe= $DaoAlumnos->show($historial->getUsu_recibe());
                                $nombre = $usuRecibe->getNombre() . " " . $usuRecibe->getApellidoP() . " " . $usuRecibe->getApellidoM();
                                $tipo_usu = "Alumno";
                                $codigo = $usuRecibe->getMatricula();
                            }
                        }
                    }
                }
                ?>
                <div class="seccion">
                    <h2>Datos del activo</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Modelo<br><input type="text" value="<?php echo $activo->getModelo() ?>" id="modelo" disabled="disabled" /></li>
                        <li>Nombre<br><input type="text" value="<?php echo $activo->getNombre() ?>" id="nombre" disabled="disabled"/></li><br>
                        <li>Fecha de prestamo<br><input type="date" value="<?php echo $Fecha_entrega ?>" id="Fecha_devolucion" disabled="disabled"/></li>
                        <li>Fecha de devoluci&oacute;n<br><input type="date" id="Fecha_devolucion" value="<?php echo $Fecha_devolucion ?>" disabled="disabled"/></li><br>
                        <li>Disponibilidad<br><input type="text" id="Estado" value="<?php echo $disponibilidad ?>" disabled="disabled" <?php echo $style; ?>/></li>
                    </ul>
                </div>
                <?php
                if (isset($Id_activo) &&  $Id_activo> 0 && $disponible == 0) {
                    ?>
                    <div class="seccion">
                        <h2>Datos del usuario actual</h2>
                        <span class="linea"></span>
                        <ul class="form">
                            <li>Tipo<br><input type="text" id="Nombre_recibe" value="<?php echo $tipo_usu; ?>" disabled="disabled"/></li>
                            <li>C&oacute;digo <br><input type="text" id="Nombre_recibe" value="<?php echo $codigo; ?>" disabled="disabled"/></li>
                            <li>Nombre<br><input type="text" id="Nombre_recibe" value="<?php echo $nombre; ?>" disabled="disabled"/></li>
                        </ul>
                    </div>
                    <?php
                }
                ?>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once '../estandares/menu_derecho.php';
                ?>
                <ul>
                    <?php
                    if (isset($Id_activo) && $Id_activo > 0) {
                        if ($disponible == 1) {
                            ?>
                            <li><span onclick="box_prestamo()">Capturar prestamo </span></li>
                            <?php
                        } else {
                            ?>
                            <li><span onclick="box_recepcion()">Capturar recepci&oacute;n </span></li>
                            <?php
                        }
                        ?>
                        <li><a href="historial_prestamo_activo.php?id=<?php echo $Id_activo; ?>" target="_blank">Historial de prestamos</a></li>
                        <li><a href="historial_activo.php?id=<?php echo $Id_activo; ?>" target="_blank">Historial de mantenimiento</a></li>
                        <?php
                    }
                    ?>

                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_activo" value="<?php echo $Id_activo; ?>"/>
<input type="hidden" id="Id_prestamo" value="<?php echo $Id_hist; ?>"/>
    <?php
}




