<?php
require_once('activate_error.php');
require_once('../require_daos.php');

if ($_POST['action'] == "buscar_int") {
    $DaoDocentes = new DaoDocentes();
    foreach ($DaoDocentes->buscarDocente($_POST['buscar']) as $k => $v) {
        ?>
        <a href="ligar_materias.php?id=<?php echo $v->getId() ?>">
            <li><?php echo $v->getClave_docen() . " - " . $v->getNombre_docen() . " " . $v->getApellidoP_docen() . " " . $v->getApellidoM_docen() ?></li>
        </a>
        <?php
    }
}



if ($_POST['action'] == "get_mat_ciclo") {
    update_ciclo($_POST['Id_docente'], $_POST['Id_ciclo']);
}


if ($_POST['action'] == "mostrar_box_materias") {
    ?>
    <div class="box-emergente-form">
        <h1>Asignar materia</h1>
        <p>Ciclo<br>     
            <select id="list_ciclos">
                <option value="0">Selecciona un ciclo</option>
                <?php
                $DaoCiclos = new DaoCiclos();
                foreach ($DaoCiclos->getCiclosFuturos() as $k => $v) {
                    ?>
                    <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave() ?></option>
                    <?php
                }
                ?>
            </select></p>
        <p>Oferta<br>                                   
            <select id="oferta" onchange="update_curso_box_curso(this)">
                <option value="0">Selecciona una oferta</option>
                <?php
                $DaoOfertas = new DaoOfertas();
                foreach ($DaoOfertas->showAll() as $k => $v) {
                    ?>
                    <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
                    <?php
                }
                ?>
            </select></p>
        <p>Especialidad<br>                                   
            <select id="curso" onchange="update_materias(this)">
                <option value="0">Selecciona una especialidad</option>
            </select></p>
        <p>Materia<br>                                   
            <select id="lista_materias" onchange="verificar_orientaciones(this)">
                <option value="0">Selecciona una materia</option>
            </select></p>
        <div id="box_orientaciones"></div>
        <button onclick="add_mat()">Guardar</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
    <?php
}


if ($_POST['action'] == "add_mat") {
    $q="";
    if(isset($_POST['Id_orientacion']) && $_POST['Id_orientacion']>0){
      $q=" AND Id_orientacion=".$_POST['Id_orientacion'];
    }
    $base = new base();
    $query = "SELECT * FROM Materias_docentes WHERE Id_profesor=" . $_POST['Id_docente'] . " AND Id_ciclo=" . $_POST['Id_ciclo'] . "  AND Id_materia=" . $_POST['Id_mate']." ".$q;
    $totalRows_consulta = $base->getTotalRows($query);
    if ($totalRows_consulta == 0) {

        $DaoMateriasDocente = new DaoMateriasDocente();
        $MateriasDocente = new MateriasDocente();
        $MateriasDocente->setId_materia($_POST['Id_mate']);
        $MateriasDocente->setId_ciclo($_POST['Id_ciclo']);
        $MateriasDocente->setId_orientacion($_POST['Id_orientacion']);
        $MateriasDocente->setId_profesor($_POST['Id_docente']);
        $DaoMateriasDocente->add($MateriasDocente);

        $DaoUsuarios = new DaoUsuarios();
        $DaoDocentes = new DaoDocentes();
        $DaoCiclos = new DaoCiclos();
        $DaoMaterias = new DaoMaterias();

        $ciclo = $DaoCiclos->show($_POST['Id_ciclo']);
        $doc = $DaoDocentes->show($_POST['Id_docente']);
        $mat = $DaoMaterias->show($_POST['Id_mate']);
        //Historial
        $TextoHistorial = "Asigna " . $mat->getNombre() . " a " . $doc->getNombre_docen() . " " . $doc->getApellidoP_docen() . " " . $doc->getApellidoM_docen() . ",ciclo " . $ciclo->getClave();
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Asignar materias docente");
        update_ciclo($_POST['Id_docente'], $_POST['Id_ciclo']);
    }
}

function update_ciclo($Id_doc, $Id_ciclo) {
    $base = new base();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoDocentes = new DaoDocentes();
    $DacoCiclos = new DaoCiclos();
    $docente = $DaoDocentes->show($Id_doc);
    ?>
    <h2>Materias asignadas</h2>
    <h2 style="font-size: 15px;margin-bottom: 15px;"><?php echo $docente->getClave_docen() . "  - " . $docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . " " . $docente->getApellidoM_docen() ?></h2>
    <h2 style="font-size: 15px;">Ciclo<br>
        <select id="Id_ciclo" onchange="get_mat_ciclo()">
            <option value="0">Selecciona un ciclo</option>
            <?php
            foreach ($DacoCiclos->showAll() as $ciclo) {
                ?>
                <option value="<?php echo $ciclo->getId() ?>" <?php if ($ciclo->getId() == $Id_ciclo) { ?> selected="selected" <?php } ?>><?php echo $ciclo->getClave() ?></option>
                <?php
            }
            ?>
        </select>
    </h2>
    <span class="linea"></span>
    <table class="table">
        <thead>
            <tr>
                <td>Ciclo</td>
                <td>Clave</td>
                <td>Nombre</td>
                <td>Orientaci&oacute;n</td>
                <td>Acciones</td>
            </tr>
        </thead>
        <tbody>
    <?php
    $query = "SELECT * FROM Materias_docentes 
                        JOIN materias_uml ON materias_uml.Id_mat =Materias_docentes.Id_materia
                        JOIN ciclos_ulm ON ciclos_ulm.Id_ciclo=Materias_docentes.Id_ciclo  
             WHERE Id_profesor=" . $Id_doc . "  AND Materias_docentes.Id_ciclo=" . $Id_ciclo . " 
             ORDER BY Id_matp DESC";
    foreach ($base->advanced_query($query) as $row_Materias_docentes) {
        $nombre_ori = "";
        $Id_ori = "";
        if ($row_Materias_docentes['Id_orientacion'] > 0) {
            $orientacion = $DaoOrientaciones->show($row_Materias_docentes['Id_orientacion']);
            $nombre_ori = $orientacion->getNombre();
            $Id_ori = $orientacion->getId();
        }
        ?>
                <tr>
                    <td><?php echo $row_Materias_docentes['Clave'] ?></td>
                    <td><?php echo $row_Materias_docentes['Clave_mat'] ?></td>
                    <td><?php echo $row_Materias_docentes['Nombre'] ?></td>
                    <td><?php echo $nombre_ori ?></td>
                    <td><button onclick="delete_mat_doc(<?php echo $row_Materias_docentes['Id_matp'] ?>)">Eliminar</button></td> 
                </tr>
        <?php
    }
    ?>
        </tbody>
    </table>
    <?php
}

if ($_POST['action'] == "delete_mat_doc") {
    $DaoMateriasDocente = new DaoMateriasDocente();
    $mateasignada = $DaoMateriasDocente->show($_POST['Id_matp']);

    $DaoUsuarios = new DaoUsuarios();
    $DaoDocentes = new DaoDocentes();
    $DaoCiclos = new DaoCiclos();
    $DaoMaterias = new DaoMaterias();

    $mat = $DaoMaterias->show($mateasignada->getId_materia());
    $ciclo = $DaoCiclos->show($mateasignada->getId_ciclo());
    $doc = $DaoDocentes->show($_POST['Id_docente']);

    //Historial
    $TextoHistorial = "Elimino la materia " . $mat->getNombre() . " para " . $doc->getNombre_docen() . " " . $doc->getApellidoP_docen() . " " . $doc->getApellidoM_docen() . ",ciclo " . $ciclo->getClave();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Asignar materias docente");

    $DaoMateriasDocente->delete($_POST['Id_matp']);
    update_ciclo($_POST['Id_docente'], $_POST['Id_ciclo']);
}


if ($_POST['action'] == "buscar_materias") {
    $base= new base();
    $DaoOrientaciones = new DaoOrientaciones();
    ?>
    <table class="table">
        <thead>
            <tr>
                <td>Clave</td>
                <td>Nombre</td>
                <td>Orientaci&oacute;n</td>
                <td style="text-align: center;"><input type="checkbox" id="all_materias" onchange="marcar_materias()"/></td>
            </tr>
        </thead>
        <tbody>
    <?php
    $query ="SELECT * FROM Materias_docentes 
                JOIN materias_uml ON materias_uml.Id_mat =Materias_docentes.Id_materia
                JOIN ciclos_ulm ON ciclos_ulm.Id_ciclo=Materias_docentes.Id_ciclo  
            WHERE Id_profesor=" . $_POST['Id_docen'] . "  AND Materias_docentes.Id_ciclo=" . $_POST['Id_ciclo'] . " ORDER BY Id_matp DESC";
    foreach($base->advanced_query($query) as $row_Materias_docentes){
            $nombre_ori = "";
            $Id_ori = "";
            if ($row_Materias_docentes['Id_orientacion'] > 0) {
                $orientacion = $DaoOrientaciones->show($row_Materias_docentes['Id_orientacion']);
                $nombre_ori = $orientacion->getNombre();
                $Id_ori = $orientacion->getId();
            }
            ?>
                    <tr id-mat="<?php echo $row_Materias_docentes['Id_materia'] ?>" id-ori="<?php echo $Id_ori ?>">
                        <td><?php echo $row_Materias_docentes['Clave_mat'] ?></td>
                        <td><?php echo $row_Materias_docentes['Nombre'] ?></td>
                        <td><?php echo $nombre_ori; ?></td>
                        <td style="text-align: center;"><input type="checkbox" class="materias"/></td> 
                    </tr>
            <?php
    }
    ?>
        </tbody>
    </table>
<?php
}

if ($_POST['action'] == "add_mat_ciclo") {
    $DaoMateriasDocente = new DaoMateriasDocente();
    $DaoUsuarios = new DaoUsuarios();
    $DaoDocentes = new DaoDocentes();
    $DaoCiclos = new DaoCiclos();
    $DaoMaterias = new DaoMaterias();
    $DaoMateriasDocente->deleteByCicloDocente($_POST['Id_ciclo'], $_POST['Id_docente']);
    foreach ($_POST['Materias'] as $k => $v) {
        $MateriasDocente = new MateriasDocente();
        $MateriasDocente->setId_materia($v['Id_mat']);
        $MateriasDocente->setId_ciclo($_POST['Id_ciclo']);
        $MateriasDocente->setId_orientacion($v['Id_ori']);
        $MateriasDocente->setId_profesor($_POST['Id_docente']);
        $DaoMateriasDocente->add($MateriasDocente);

        $ciclo = $DaoCiclos->show($_POST['Id_ciclo']);
        $doc = $DaoDocentes->show($_POST['Id_docente']);
        $mat = $DaoMaterias->show($v['Id_mat']);

        //Historial
        $TextoHistorial = "Asigna " . $mat->getNombre() . " a " . $doc->getNombre_docen() . " " . $doc->getApellidoP_docen() . " " . $doc->getApellidoM_docen() . ",ciclo " . $ciclo->getClave();
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Asignar materias docente");
    }
    update_ciclo($_POST['Id_docente'], $_POST['Id_ciclo']);
}

if ($_POST['action'] == "mostrar_box_materias_ciclo") {
   $DaoCiclos = new DaoCiclos();
?>
    <div class="box-emergente-form">
        <h1>Asignar materias</h1>
        <p>Ciclo<br>     
            <select id="Id_ciclo_actual">
                <option value="0">Selecciona un ciclo</option>
                <?php
                foreach ($DaoCiclos->getCiclosFuturos() as $k => $v) {
                    ?>
                    <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave() ?></option>
                    <?php
                }
                ?>
            </select>
        </p>
        <p>Ciclo a buscar<br>     
            <select id="Id_ciclo_buscar" onchange="buscar_materias()">
                <option value="0">Selecciona un ciclo</option>
                <?php
                foreach ($DaoCiclos->showAll() as $k => $v) {
                    ?>
                    <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave() ?></option>
                    <?php
                }
                ?>
            </select>
        </p>
        <div id="box_materias_ciclo"></div>
        <button onclick="add_mat_ciclo()">Guardar</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
    <?php
}


if ($_POST['action'] == "verificar_orientaciones") {
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoOrientaciones= new DaoOrientaciones();
    $mat_esp = $DaoMateriasEspecialidad->show($_POST['Id_mat_esp']);
    if ($mat_esp->getTiene_orientacion() == 1) {
        ?>
        <p>Orientaci&oacute;n<br>                                   
            <select id="orientaciones">
                <option value="0">Selecciona una orientaci&oacute;n</option>
                <?php
                foreach ($DaoOrientaciones->getOrientacionesEspecialidad($mat_esp->getId_esp()) as $ori) {
                ?>
                    <option value="<?php echo $ori->getId() ?>"> <?php echo $ori->getNombre() ?> </option>
                    <?php
                }
                ?>
            </select>
        </p>
<?php
    }
}

/*
if($_POST['action']=="validar_si_asignada_a_grupo"){
    $DaoHorarioDocente= new DaoHorarioDocente();
    $query="SELECT * FROM Horario_docente WHERE Id_docente=".$_POST['Id_docente']." AND Id_ciclo=".$_POST['Id_ciclo']." GROUP BY Id_grupo";
    $DaoHorarioDocente->advanced_query($query);
}
 */