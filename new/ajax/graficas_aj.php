<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 


function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
    return random_color_part() . random_color_part() . random_color_part();
}

if($_POST['action']=="getValores"){
    $DaoCategoriasSicologia= new DaoCategoriasSicologia();
    $DaoCamposSicologia= new DaoCamposSicologia();
    $DaoAnalisisSicologico= new DaoAnalisisSicologico();
    $DaoAlumnos= new DaoAlumnos(); 
    
    $categorias=array();
    foreach($DaoCategoriasSicologia->showAll() as $k=>$v){
          $cat=array();
          $cat['Nombre']=$v->getNombre();
          $cat['Campos']=array();
          foreach($DaoCamposSicologia->showByCategoria($v->getId())as $k2=>$v2){
              $campo=$DaoAnalisisSicologico->showByAlumCampo($_POST['Id_alum'],$v2->getId());
              $valor=0;
              if(strlen($campo->getValor_camp())>0){
                 $valor= $campo->getValor_camp();
              }
              
              $campo=array();
              $campo['Nombre']=$v2->getNombre();
              $campo['Valor']=$valor;
              $campo['Color']=random_color();
              array_push($cat['Campos'], $campo);
              
          }  
          array_push($categorias, $cat);
    }
    echo json_encode($categorias);
}
