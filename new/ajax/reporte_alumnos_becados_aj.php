<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if ($_POST['action'] == "filtro") {
    $base = new base();
    $DaoOfertas = new DaoOfertas();
    $DaoAlumnos = new DaoAlumnos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoGrados = new DaoGrados();
    $DaoMediosEnterar = new DaoMediosEnterar();
    $DaoMaterias = new DaoMaterias();
    $DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
    $DaoBecas = new DaoBecas();
    $DaoCiclos = new DaoCiclos();
    $DaoUsuarios = new DaoUsuarios();
    $DaoTurnos= new DaoTurnos();
    
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $q = "";

    if ($_POST['Id_ofe'] > 0) {
        $q = $q . " AND ofertas_alumno.Id_ofe=" . $_POST['Id_ofe'];
    }
    if ($_POST['Id_esp'] > 0) {
        $q = $q . " AND ofertas_alumno.Id_esp=" . $_POST['Id_esp'];
    }
    if ($_POST['Id_ori'] > 0) {
        $q = $q . " AND ofertas_alumno.Id_ori=" . $_POST['Id_ori'];
    }
    if ($_POST['Opcion_pago'] > 0) {
        $q = $q . " AND ofertas_alumno.Opcion_pago=" . $_POST['Opcion_pago'];
    }
    
    if($_POST['Id_ciclo'] >0){
       $q = $q . " AND ciclos_alum_ulm.Id_ciclo=" . $_POST['Id_ciclo']; 
    }
    
    if($_POST['Turno'] >0){
       $q = $q . " AND ofertas_alumno.Turno=" . $_POST['Turno']; 
    }
    
    $count = 1;
    $query = "SELECT * FROM ciclos_alum_ulm 
        JOIN ofertas_alumno ON ciclos_alum_ulm.Id_ofe_alum=ofertas_alumno.Id_ofe_alum
        JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
        JOIN ciclos_ulm ON ciclos_alum_ulm.Id_ciclo=ciclos_ulm.Id_ciclo
    WHERE inscripciones_ulm.tipo=1  
          AND inscripciones_ulm.Id_plantel=" . $usu->getId_plantel() . " 
          AND ofertas_alumno.Activo_oferta=1 
          AND ofertas_alumno.Baja_ofe IS NULL
          AND ofertas_alumno.FechaCapturaEgreso IS NULL
          AND ofertas_alumno.IdCicloEgreso IS NULL
          AND ofertas_alumno.IdUsuEgreso IS NULL
          AND ciclos_alum_ulm.Porcentaje_beca >0 ".$q." ORDER BY ciclos_alum_ulm.Id_ciclo DESC";
    $becados = $base->advanced_query($query);
    $row_becados = $becados->fetch_assoc();
    $totalRows_becados= $becados->num_rows;
    if ($totalRows_becados) {
        do {
            $nombre_ori = "";
            $oferta = $DaoOfertas->show($row_becados['Id_ofe']);
            $esp = $DaoEspecialidades->show($row_becados['Id_esp']);
            if ($row_becados['Id_ori'] > 0) {
                $ori = $DaoOrientaciones->show($row_becados['Id_ori']);
                $nombre_ori = $ori->getNombre();
            }
            $opcion = "Plan por materias";
            if ($row_becados['Opcion_pago'] == 2) {
                $opcion = "Plan completo";
            }
            $tur = $DaoTurnos->show($row_becados['Turno']);
            $t=$tur->getNombre();

            $nombreBeca="";
            if($row_becados['Tipo_beca']>0){
               $beca = $DaoBecas->show($row_becados['Tipo_beca']);
               $nombreBeca=$beca->getNombre();   
            }
                                        
            $Grado = $DaoGrados->show($row_becados['Id_grado']);
            $calTotal = 0;
            $countMat = 0;
            $query = "
            SELECT * FROM ciclos_alum_ulm 
            JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
            WHERE Id_ofe_alum=" . $row_becados['Id_ofe_alum'] . " AND Id_ciclo<" . $row_becados['Id_ciclo'] . "  AND materias_ciclo_ulm.Activo=1";
                foreach ($base->advanced_query($query) as $k2 => $v2) {
                    $mat_esp = $DaoMateriasEspecialidad->show($v2['Id_mat_esp']);
                    $mat = $DaoMaterias->show($mat_esp->getId_mat());

                    if (strlen($v2['CalTotalParciales']) > 0) {
                        $calTotal+=$v2['CalTotalParciales'];
                    } elseif (strlen($v2['CalExtraordinario']) > 0) {
                        $calTotal+=$v2['CalExtraordinario'];
                    } elseif (strlen($v2['CalEspecial']) > 0) {
                        $calTotal+=$v2['CalEspecial'];
                    }
                    $countMat++;
                }
                $calTotal = $calTotal / $countMat;
                $pink = "";
                if ($calTotal < $beca->getCalificacionMin()) {
                    $pink = "pink";
                }
                ?>
                <tr id_alum="<?php echo $row_becados['Id_ins']; ?>" class="<?php echo $pink . " " . $calTotal . " " . $countMat ?>">
                    <td><?php echo $count; ?></td>
                    <td><?php echo $row_becados['Matricula'] ?></td>
                    <td style="width: 115px;"><?php echo $row_becados['Nombre_ins'] . " " . $row_becados['ApellidoP_ins'] . " " . $row_becados['ApellidoM_ins'] ?></td>
                    <td><?php echo $oferta->getNombre_oferta() ?></td>
                    <td><?php echo $esp->getNombre_esp(); ?></td>
                    <td><?php echo $nombre_ori; ?></td>
                    <td><?php echo $Grado->getGrado(); ?></td>
                    <td><?php echo $opcion; ?></td>
                    <td style="text-align:center;"><?php echo $row_becados['Porcentaje_beca']; ?>%</td>
                    <td><?php echo $nombreBeca; ?></td>
                    <td style="text-align:center;"><b><?php echo number_format($calTotal, 2); ?></b></td>
                    <td style="text-align:center;"><?php echo $row_becados['Clave'] ?></td>
                    <td style="text-align:center;"><?php echo $t ?></td>
                    <td style="text-align:center;"><input type="checkbox"> </td>
                </tr>
                <?php
                $count++;

        } while ($row_becados = $becados->fetch_assoc());
    }
}


if ($_GET['action'] == "download_excel") {
    $base = new base();
    $DaoTurnos= new DaoTurnos();
    $gris = "ACB9CA";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'MATRICULA');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'OFERTA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'CARRERA');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'ORIENTACIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'GRADO');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'OPCIÓN DE PAGO');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'BECA');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'TIPO');
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'PROMEDIO');
    $objPHPExcel->getActiveSheet()->setCellValue('L1', 'CICLO');
    $objPHPExcel->getActiveSheet()->setCellValue('M1', 'TURNO');

    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');

    //Establece el width automatico de la celda 
    foreach ($base->xrange('A', 'M') as $columnID) {
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }

    $base = new base();
    $DaoOfertas = new DaoOfertas();
    $DaoAlumnos = new DaoAlumnos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoGrados = new DaoGrados();
    $DaoMediosEnterar = new DaoMediosEnterar();
    $DaoMaterias = new DaoMaterias();
    $DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
    $DaoBecas = new DaoBecas();
    $DaoCiclos = new DaoCiclos();
    $DaoUsuarios = new DaoUsuarios();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $q = "";

    if ($_GET['Id_ofe'] > 0) {
        $q = $q . " AND ofertas_alumno.Id_ofe=" . $_GET['Id_ofe'];
    }
    if ($_GET['Id_esp'] > 0) {
        $q = $q . " AND ofertas_alumno.Id_esp=" . $_GET['Id_esp'];
    }
    if ($_GET['Id_ori'] > 0) {
        $q = $q . " AND ofertas_alumno.Id_ori=" . $_GET['Id_ori'];
    }
    if ($_GET['Opcion_pago'] > 0) {
        $q = $q . " AND ofertas_alumno.Opcion_pago=" . $_GET['Opcion_pago'];
    }
    
    if($_GET['Id_ciclo'] >0){
       $q = $q . " AND ciclos_alum_ulm.Id_ciclo=" . $_GET['Id_ciclo']; 
    }
    
    if($_GET['Turno'] >0){
       $q = $q . " AND ofertas_alumno.Turno=" . $_GET['Turno']; 
    }

$count = 1;
    $query = "SELECT * FROM ciclos_alum_ulm 
        JOIN ofertas_alumno ON ciclos_alum_ulm.Id_ofe_alum=ofertas_alumno.Id_ofe_alum
        JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
        JOIN ciclos_ulm ON ciclos_alum_ulm.Id_ciclo=ciclos_ulm.Id_ciclo
    WHERE inscripciones_ulm.tipo=1  
          AND inscripciones_ulm.Id_plantel=" . $usu->getId_plantel() . " 
          AND ofertas_alumno.Activo_oferta=1 
          AND ofertas_alumno.Baja_ofe IS NULL
          AND ofertas_alumno.FechaCapturaEgreso IS NULL
          AND ofertas_alumno.IdCicloEgreso IS NULL
          AND ofertas_alumno.IdUsuEgreso IS NULL
          AND ciclos_alum_ulm.Porcentaje_beca >0 ".$q." ORDER BY ciclos_alum_ulm.Id_ciclo DESC";
    $becados = $base->advanced_query($query);
    $row_becados = $becados->fetch_assoc();
    $totalRows_becados= $becados->num_rows;
    if ($totalRows_becados) {
        do {
            $nombre_ori = "";
            $oferta = $DaoOfertas->show($row_becados['Id_ofe']);
            $esp = $DaoEspecialidades->show($row_becados['Id_esp']);
            if ($row_becados['Id_ori'] > 0) {
                $ori = $DaoOrientaciones->show($row_becados['Id_ori']);
                $nombre_ori = $ori->getNombre();
            }
            $opcion = "Plan por materias";
            if ($row_becados['Opcion_pago'] == 2) {
                $opcion = "Plan completo";
            }
            $tur = $DaoTurnos->show($row_becados['Turno']);
            $t=$tur->getNombre();

            $nombreBeca="";
            if($row_becados['Tipo_beca']>0){
               $beca = $DaoBecas->show($row_becados['Tipo_beca']);
               $nombreBeca=$beca->getNombre();   
            }
            
            $Grado = $DaoGrados->show($row_becados['Id_grado']);
            $calTotal = 0;
            $countMat = 0;
            $query = "
            SELECT * FROM ciclos_alum_ulm 
            JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
            WHERE Id_ofe_alum=" . $row_becados['Id_ofe_alum'] . " AND Id_ciclo<" . $row_becados['Id_ciclo'] . "  AND materias_ciclo_ulm.Activo=1";
                foreach ($base->advanced_query($query) as $k2 => $v2) {
                    $mat_esp = $DaoMateriasEspecialidad->show($v2['Id_mat_esp']);
                    $mat = $DaoMaterias->show($mat_esp->getId_mat());

                    if (strlen($v2['CalTotalParciales']) > 0) {
                        $calTotal+=$v2['CalTotalParciales'];
                    } elseif (strlen($v2['CalExtraordinario']) > 0) {
                        $calTotal+=$v2['CalExtraordinario'];
                    } elseif (strlen($v2['CalEspecial']) > 0) {
                        $calTotal+=$v2['CalEspecial'];
                    }
                    $countMat++;
                }
                $calTotal = $calTotal / $countMat;
                $pink = "";
                if ($calTotal < $beca->getCalificacionMin()) {
                    $pink = "pink";
                }

            $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
            $objPHPExcel->getActiveSheet()->setCellValue("B$count", $row_becados['Matricula']);
            $objPHPExcel->getActiveSheet()->setCellValue("C$count", $row_becados['Nombre_ins'] . " " . $row_becadosv['ApellidoP_ins'] . " " . $row_becados['ApellidoM_ins']);
            $objPHPExcel->getActiveSheet()->setCellValue("D$count", $oferta->getNombre_oferta());
            $objPHPExcel->getActiveSheet()->setCellValue("E$count", $esp->getNombre_esp());
            $objPHPExcel->getActiveSheet()->setCellValue("F$count", $nombre_ori);
            $objPHPExcel->getActiveSheet()->setCellValue("G$count", $Grado->getGrado());
            $objPHPExcel->getActiveSheet()->setCellValue("H$count", $opcion);
            $objPHPExcel->getActiveSheet()->setCellValue("I$count", $row_becados['Porcentaje_beca']);
            $objPHPExcel->getActiveSheet()->setCellValue("J$count", $nombreBeca);
            $objPHPExcel->getActiveSheet()->setCellValue("K$count", number_format($calTotal, 2));
            $objPHPExcel->getActiveSheet()->setCellValue("L$count", $row_becados['Clave']);
            $objPHPExcel->getActiveSheet()->setCellValue("M$count", $t);
            $count++;

        } while ($row_becados = $becados->fetch_assoc());
       }
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=AlumnosBecados-" . date('Y-m-d_H:i:s') . ".xlsx");
    echo $objWriter->save('php://output');
}




