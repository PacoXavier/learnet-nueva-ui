<?php
require_once('activate_error.php');
require_once('../require_daos.php');
require_once('../barcodegen/class/BCGFontFile.php');
require_once('../barcodegen/class/BCGColor.php');
require_once('../barcodegen/class/BCGDrawing.php');
require_once('../barcodegen/class/BCGcode39.barcode.php');



if ($_POST['action'] == "buscarLibro") {
    $base = new base();
    $DaoLibros = new DaoLibros();
    $DaoUsuarios = new DaoUsuarios();
    $DaoAulas= new DaoAulas();
    $DaoCategoriasLibros= new DaoCategoriasLibros();
    $count=1;
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $query = "SELECT *
	FROM Libros WHERE  (Titulo LIKE '%".$_POST['buscar']."%' OR Autor LIKE '%".$_POST['buscar']."%') AND Id_plantel=" . $usu->getId_plantel() . " ORDER BY Id_lib DESC LIMIT 20";
    foreach ($base->advanced_query($query) as $row_BusquedaPro) {
        $libro=$DaoLibros->show($row_BusquedaPro['Id_lib']);
        if ($libro->getDisponible() == 1) {
            $disponibilidad = "Disponible";
            $style = 'style="color:green;"';
        } else {
            $disponibilidad = "No Disponible";
            $style = 'style="color:red;"';
        }

        $aula = $DaoAulas->show($libro->getId_aula());
        $cat = $DaoCategoriasLibros->show($libro->getId_cat());

        $status = "Activo";
        $color = "color:green;";
        if(strlen($libro->getBaja_libro())>0){
            $status = "Baja";
            $color = "color:red;";
        }
        ?>
        <tr>
            <td onclick="mostrar(<?php echo $libro->getId(); ?>)"><?php echo $count; ?></td>
            <td style="text-align: center;" onclick="mostrar(<?php echo $libro->getId(); ?>)"><?php echo $libro->getCodigo() ?></td>
            <td onclick="mostrar(<?php echo $libro->getId(); ?>)"><?php echo $libro->getTitulo(); ?></td>
            <td onclick="mostrar(<?php echo $libro->getId(); ?>)"><?php echo $libro->getAutor() ?></td>
            <td><?php echo $cat->getNombre(); ?></td>
            <td><?php echo $libro->getFecha_adq() ?></td>
            <td style="text-align:center;"><?php echo $aula->getClave_aula(); ?></td>
            <td <?php echo $style; ?>><?php echo $disponibilidad; ?></td>
            <td style="text-align:center;<?php echo $color ?>"><?php echo $status; ?></td>
            <td>
                <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                <div class="box-buttom">
                    <button onclick="delete_libro(<?php echo $libro->getId(); ?>)">Eliminar</button><br>
                    <button onclick="generar_etiqueta(<?php echo $libro->getId(); ?>)">Etiqueta</button><br>
                    <a href="historial_prestamo_libro.php?id=<?php echo $libro->getId(); ?>" target="_blank"><button>Hist. Pres</button></a>
                </div>
            </td>
        </tr>
        <?php
        $count++;
    }
}


if ($_POST['action'] == "delete_libro") {
    $DaoLibros = new DaoLibros();
    $libro = $DaoLibros->show($_POST['Id_lib']);
    $libro->setBaja_libro(date('Y-m-d H:i:s'));
    $DaoLibros->update($libro);

    $DaoUsuarios = new DaoUsuarios();
    $TextoHistorial = "Elimino el libro " . $libro->getTitulo();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Libros");
    update_page();
}

function update_page() {
    $DaoLibros = new DaoLibros();
    $DaoAulas = new DaoAulas();
    $DaoCategoriasLibros = new DaoCategoriasLibros();
    $count = 1;
    foreach ($DaoLibros->showAll() as $libro) {
        if ($libro->getDisponible() == 1) {
            $disponibilidad = "Disponible";
            $style = 'style="color:green;"';
        } else {
            $disponibilidad = "No Disponible";
            $style = 'style="color:red;"';
        }

        $aula = $DaoAulas->show($libro->getId_aula());
        $cat = $DaoCategoriasLibros->show($libro->getId_cat());

        $status = "Activo";
        $color = "color:green;";
        if(strlen($libro->getBaja_libro())>0){
            $status = "Baja";
            $color = "color:red;";
        }
        ?>
        <tr>
            <td onclick="mostrar(<?php echo $libro->getId(); ?>)"><?php echo $count; ?></td>
            <td style="text-align: center;" onclick="mostrar(<?php echo $libro->getId(); ?>)"><?php echo $libro->getCodigo() ?></td>
            <td onclick="mostrar(<?php echo $libro->getId(); ?>)"><?php echo $libro->getTitulo(); ?></td>
            <td onclick="mostrar(<?php echo $libro->getId(); ?>)"><?php echo $libro->getAutor() ?></td>
            <td><?php echo $cat->getNombre(); ?></td>
            <td><?php echo $libro->getFecha_adq() ?></td>
            <td style="text-align:center;"><?php echo $aula->getClave_aula(); ?></td>
            <td <?php echo $style; ?>><?php echo $disponibilidad; ?></td>
            <td style="text-align:center;<?php echo $color ?>"><?php echo $status; ?></td>
            <td>
                <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                <div class="box-buttom">
                    <button onclick="delete_libro(<?php echo $libro->getId(); ?>)">Eliminar</button><br>
                    <button onclick="generar_etiqueta(<?php echo $libro->getId(); ?>)">Etiqueta</button><br>
                    <a href="historial_prestamo_libro.php?id=<?php echo $libro->getId(); ?>" target="_blank"><button>Hist. Pres</button></a>
                </div>
            </td>
        </tr>
        <?php
        $count++;
    }
}

if ($_POST['action'] == "generar_etiqueta") {
    $DaoUsuarios = new DaoUsuarios();
    $DaoPlanteles = new DaoPlanteles();
    $DaoLibros = new DaoLibros();

    $libro = $DaoLibros->show($_POST['Id_lib']);
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $plantel = $DaoPlanteles->show($usu->getId_plantel());
    if (strlen($plantel->getEtiquetaLibro()) > 0) {
        //+++++++++++++++++++++++++++++++++++++++++++++++
        //Parte del posterior de la credencial
        //++++++++++++ ++++++++++++++++++++++++++++++++++++
        //Generar imagen del codigo de barras
        //Generar imagen del codigo de barras
        // Loading Font
        $font = new BCGFontFile('../barcodegen/font/Arial.ttf', 9);
        $text = $libro->getCodigo();

        // The arguments are R, G, B for color.
        $color_black = new BCGColor(0, 0, 0);
        $color_white = new BCGColor(255, 255, 255);

        $drawException = null;
        try {
            $code = new BCGcode39();
            $code->setScale(2); // Resolution
            $code->setThickness(12); // Thickness
            $code->setForegroundColor($color_black); // Color of bars
            $code->setBackgroundColor($color_white); // Color of spaces
            $code->setFont($font); // Font (or 0)
            $code->parse($text); // Text
        } catch (Exception $exception) {
            $drawException = $exception;
        }

        //Here is the list of the arguments
        //1 - Filename (empty : display on screen)
        //2 - Background color 
        $drawing = new BCGDrawing('codigoBarras.jpg', $color_white);
        if ($drawException) {
            $drawing->drawException($drawException);
        } else {
            $drawing->setBarcode($code);
            $drawing->draw();
        }

        //Header that says it is an image (remove it if you save the barcode to a file)
        //header('Content-Type: image/png');
        //header('Content-Disposition: inline; filename="barcode.png"');
        // Draw (or save) the image into PNG format.
        $drawing->finish(BCGDrawing::IMG_FORMAT_JPEG);

        $destino = imagecreatefromjpeg("../files/" . $plantel->getEtiquetaLibro() . ".jpg");
        $origen = imagecreatefromjpeg('codigoBarras.jpg');

        //Colocar color al texto
        $color_texto = imagecolorallocate($destino, 0, 0, 0);

        //ruta del archivo ttf
        $fuente = "../estandares/Arial/arial.ttf";

        //Definimos el tamaño de la fuente
        $tamanoFuente = 12;

        //Obtenemos el tamanio del codigo de barras
        $picsize = getimagesize('codigoBarras.jpg');

        $source_x = $picsize[0] * 1;
        $source_y = $picsize[1] * 1;

        // Copiar y fusionar
        imagecopymerge($destino, $origen, 55, 17, 0, 0, $source_x, $source_y, 100);

        //Imprimir y liberar memoria
        //header('Content-Type: image/jpg');
        $Img_atras = "etiquetaLibro.jpg";
        $im = imagejpeg($destino, $Img_atras, 100);

        //Agregamos la imagen al zip
        $zip = new ZipArchive();
        $zip->open("etiqueta_libro.zip", ZipArchive::CREATE);
        $zip->addFile($Img_atras, $Img_atras);
        $zip->close();

        imagedestroy($destino);
        imagedestroy($origen);

        //Borramos las imagenes creadas
        unlink($Img_atras);
        unlink('codigoBarras.jpg');
    }
}