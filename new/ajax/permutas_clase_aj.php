<?php
require_once('activate_error.php');
require_once('../require_daos.php');

if ($_POST['action'] == "buscar_int") {
    $DaoDocentes = new DaoDocentes();
    foreach ($DaoDocentes->buscarDocente($_POST['buscar']) as $k => $v) {
        ?>
        <a href="permutas_clase.php?id=<?php echo $v->getId() ?>">
            <li><?php echo $v->getClave_docen() . " - " . $v->getNombre_docen() . " " . $v->getApellidoP_docen() . " " . $v->getApellidoM_docen() ?></li>
        </a>
        <?php
    }
}

if ($_POST['action'] == "mostrar_box_permutas") {
    ?>
    <div class="box-emergente-form">
        <h1>Permutar clase</h1>
        <div class="boxUlBuscador" id="box-grupo">
            <p>Grupo<br><input type="text" onkeyup="buscarGruposCiclo(this)" placeholder="Nombre" id="grupo"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p>Fecha de la clase<br><input type="text" class="datepicker"  id="fecha-no-cubierta"/></p> 
        <p>Fecha de reposición<br><input type="text" class="datepicker" id="fecha-permuta"/></p> 
        <p>Sesion<br> 
            <select id="hora">
                <option value="0"></option>
            <?php
            $DaoHoras= new DaoHoras();
            foreach($DaoHoras->showAll() as $hora){
                    ?>
                    <option value="<?php echo $hora->getId() ?>"> <?php echo $hora->getTexto_hora(); ?></option>
                    <?php   
            }
            ?>
            </select>
        </p>     
         <p>Aula<br>
           <select id="aula">
              <option value="0"></option>
              	<?php
                $DaoAulas= new DaoAulas();
                foreach($DaoAulas->showAll() as $aula){
                 ?>
                   <option value="<?php echo $aula->getId()?>"><?php echo $aula->getClave_aula()." - ".$aula->getNombre_aula() ?></option>
                 <?php   
                }
                ?>
            </select>
        </p>
        <p>Comentario<br><textarea id="Comentarios"></textarea></p>   
        <button onclick="verificarDisponibilidad()">Guardar</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
    <?php
}



if ($_POST['action'] == "getDiasClase") {
    $DaoCiclos = new DaoCiclos();
    $DaoGrupos = new DaoGrupos();
    $ciclo = $DaoCiclos->getActual();
    if ($ciclo->getId() > 0) {
        $resp = $DaoGrupos->getDiasClaseGrupo($_POST['Id_grupo'], $ciclo->getId());
        foreach ($resp['dias'] as $dia) {
            ?>
            <input type="hidden" value="<?php echo $dia ?>" class="dias_ciclo"/>
            <?php
        }
    }
}

if ($_POST['action'] == "getHorasClase") {
    $DaoCiclos = new DaoCiclos();
    $DaoGrupos = new DaoGrupos();
    $ciclo = $DaoCiclos->getActual();
    if ($ciclo->getId() > 0) {
        $resp = $DaoGrupos->getDiasClaseGrupo($_POST['Id_grupo'], $ciclo->getId());
        $dia = date("N", strtotime($_POST['Fecha']));
        if ($dia == 1) {
            $letraDia = "L";
        } elseif ($dia == 2) {
            $letraDia = "M";
        }if ($dia == 3) {
            $letraDia = "I";
        }if ($dia == 4) {
            $letraDia = "J";
        }if ($dia == 5) {
            $letraDia = "V";
        }if ($dia == 6) {
            $letraDia = "S";
        }if ($dia == 7) {
            $letraDia = "D";
        }
        ?>
        <option value="0"></option>
        <?php
        foreach ($resp['horas'][$letraDia] as $k => $v) {
            ?>
            <option value="<?php echo $v['Id'] ?>"> <?php echo $v['Hora']; ?></option>
            <?php
        }
    }
}




if ($_POST['action'] == "add_permuta") {
    $DaoAsistencias= new DaoAsistencias();

    $existe=$DaoAsistencias->existAsistencia($_POST['Id_grupo'], $_POST['FechaPermuta']);
    if($existe==0){ 
        $base = new base();
        $query = "SELECT * FROM PermutasClase WHERE Id_docente_asignado=" . $_POST['Id_doc'] . " AND Id_ciclo=" . $_POST['Id_ciclo'] . "  AND Dia='" . $_POST['FechaPermuta'] . "' AND Id_hora=" . $_POST['Hora'];
        $consulta = $base->advanced_query($query);
        $row_consulta = $consulta->fetch_assoc();
        $totalRows_consulta = $consulta->num_rows;
        if ($totalRows_consulta == 0) {

            $DaoPermutaClase = new DaoPermutasClase();
            $PermutasClase = new PermutasClase();
            $PermutasClase->setId_usu($_COOKIE['admin/Id_usu']);
            $PermutasClase->setId_grupo($_POST['Id_grupo']);
            $PermutasClase->setId_docente_asignado($_POST['Id_doc']);
            $PermutasClase->setId_ciclo($_POST['Id_ciclo']);
            $PermutasClase->setDiaNoCubierto($_POST['FechaNoCubierta']);
            $PermutasClase->setDia($_POST['FechaPermuta']);
            $PermutasClase->setId_hora($_POST['Hora']);
            $PermutasClase->setDateCreated(date('Y-m-d H:i:s'));
            $PermutasClase->setComentario($_POST['Comentarios']);
            $PermutasClase->setId_aula($_POST['Id_aula']);
            $DaoPermutaClase->add($PermutasClase);

            $DaoUsuarios = new DaoUsuarios();
            $DaoDocentes = new DaoDocentes();
            $DaoGrupos = new DaoGrupos();
            $DaoHoras = new DaoHoras();

            $doc = $DaoDocentes->show($_POST['Id_doc']);
            $grupo = $DaoGrupos->show($_POST['Id_grupo']);
            $hora = $DaoHoras->show($_POST['Hora']);

            //Crear Notificacion para el docente
            $DaoNotificacionesUsuario= new DaoNotificacionesUsuario();
            $NotificacionesUsuario= new NotificacionesUsuario();
            $textoNot="Permuta de clase asignada<br>Grupo:".$grupo->getClave()."<br>Día: ".$_POST['FechaPermuta']."<br>Hora: ".$hora->getTexto_hora();

            $NotificacionesUsuario->setTitulo("Permuta de clase");
            $NotificacionesUsuario->setTexto($textoNot);
            $NotificacionesUsuario->setDateCreated(date('Y-m-d H:i:s'));
            $NotificacionesUsuario->setIdRel($doc->getId());
            $NotificacionesUsuario->setTipoRel('docente');
            $DaoNotificacionesUsuario->add($NotificacionesUsuario);

            //Historial
            $TextoHistorial = "Añade permuta de clase para  " . $doc->getNombre_docen() . " " . $doc->getApellidoP_docen() . " " . $doc->getApellidoM_docen() . ",grupo " . $grupo->getClave() . ", Hora " . $hora->getTexto_hora();
            $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Permutas de clase");
        }
         update_page($_POST['Id_doc'], $_POST['Id_ciclo']);
    }
}

function update_page($Id_doc, $Id_ciclo) {
    $DaoCiclos = new DaoCiclos();
    $DaoDocentes = new DaoDocentes();
    $DaoPermutaClase = new DaoPermutasClase();
    $DaoGrupos = new DaoGrupos();
    $DaoHoras = new DaoHoras();
    $DaoHorarioDocente = new DaoHorarioDocente();
    $DaoUsuarios= new DaoUsuarios();
    $DaoAulas= new DaoAulas();
    
    $_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-random"></i> Permuta de clases</h1>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <?php
                        $nombre = "";
                        if ($Id_doc > 0) {
                            $docente = $DaoDocentes->show($Id_doc);
                            $nombre = $docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . " " . $docente->getApellidoM_docen();
                        }
                        ?>
                        <li>Buscar Docente<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o clave" value="<?php echo $nombre ?>"/>
                            <ul id="buscador_int"></ul>
                        </li><br>
                        <li>Ciclo<br>
                            <select id="Id_ciclo" onchange="getPermutas()">
                                <option value="0">Selecciona un ciclo</option>
                                <?php
                                foreach ($DaoCiclos->showAll() as $k => $v) {
                                    ?>
                                     <option value="<?php echo $v->getId() ?>" <?php if($v->getId()==$Id_ciclo){ ?> selected="selected" <?php } ?>><?php echo $v->getClave() ?></option>
                                    <?php
                                }
                                ?>

                            </select>
                        </li>
                    </ul>
                    <div id="box-info">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Fecha de captura</td>
                                    <td>Ciclo</td>
                                    <td>Grupo</td>
                                    <td>Fecha de la clase</td>
                                    <td>Fecha de permuta</td>
                                    <td>Sesion</td>
                                    <td>Aula</td>
                                    <td>Docente del grupo</td>
                                    <td>Docente con permuta</td>
                                    <td>Comentario</td>
                                    <td>Acciones</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                foreach ($DaoPermutaClase->getPermutasDocenteByCiclo($Id_doc, $Id_ciclo) as $k => $v) {
                                    $ciclo = $DaoCiclos->show($v->getId_ciclo());
                                    $grupo = $DaoGrupos->show($v->getId_grupo());
                                    $hora = $DaoHoras->show($v->getId_hora());
                                    $aula="";
                                    if(strlen($v->getId_aula())>0){
                                        $a=$DaoAulas->show($v->getId_aula());
                                        $aula=$a->getClave_aula();
                                    }
                                    //Obtener el docente original del grupo permutado
                                    $nombreDocenteGrupo="";
                                    $horario=$DaoHorarioDocente->getHorarioByGrupoCiclo($v->getId_grupo(),$Id_ciclo);
                                    $Id_docen= $horario->getId_docente();
                                    if($Id_docen>0){
                                      $docenteGrupo=$DaoDocentes->show($Id_docen);  
                                      $nombreDocenteGrupo=$docenteGrupo->getNombre_docen() . " " . $docenteGrupo->getApellidoP_docen() . " " . $docenteGrupo->getApellidoM_docen();
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td><?php echo $v->getDateCreated() ?></td>
                                        <td><?php echo $ciclo->getClave(); ?></td>
                                        <td><?php echo $grupo->getClave() ?></td>
                                        <td><?php echo $v->getDiaNoCubierto() ?></td>
                                        <td><?php echo $v->getDia() ?></td>
                                        <td><?php echo $hora->getTexto_hora() ?></td>
                                        <td><?php echo $aula ?></td>
                                        <td><?php echo $nombreDocenteGrupo?></td>
                                        <td><?php echo $docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . " " . $docente->getApellidoM_docen() ?></td>
                                        <td><?php echo $v->getComentario() ?></td>
                                        <td><button onclick="validarSiExistenAsistencias(<?php echo $v->getId() ?>)">ELIMINAR</button></td>
                                    </tr>
                                    <?php
                                    $count++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once '../estandares/menu_derecho.php';
                ?>
                <ul>
                    <?php
                    if ($Id_doc > 0) {
                        ?>
                        <li><span onclick="mostrar_box_permutas()">Realizar permuta </span></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </td>
    </tr>
    <?php
}

if ($_POST['action'] == "getPermutas") {
    update_page($_POST['Id_doc'], $_POST['Id_ciclo']);
}


if ($_POST['action'] == "validarSiExistenAsistencias") {
    $DaoAsistencias= new DaoAsistencias();
    $DaoPermutaClase = new DaoPermutasClase();
    $permuta = $DaoPermutaClase->show($_POST['Id_per']); 
    
    $resp=0;
    $existe=$DaoAsistencias->existAsistencia($permuta->getId_grupo(), $permuta->getDia());
    if($existe>0){
        $resp=1;
    }
    echo $resp;
}

if ($_POST['action'] == "deletePermuta") {
    $DaoAsistencias= new DaoAsistencias();
    $DaoPermutaClase = new DaoPermutasClase();
    $permuta = $DaoPermutaClase->show($_POST['Id_per']);

    $DaoUsuarios = new DaoUsuarios();
    $DaoDocentes = new DaoDocentes();
    $DaoGrupos = new DaoGrupos();
    $DaoHoras = new DaoHoras();

    $doc = $DaoDocentes->show($permuta->getId_docente_asignado());
    $grupo = $DaoGrupos->show($permuta->getId_grupo());
    $hora = $DaoHoras->show($permuta->getId_hora());

    $resp=$DaoAsistencias->deleteAsistencias($permuta->getId_grupo(),$permuta->getId_ciclo(),$permuta->getDia());
    if($resp==true){
        //Historial
        $TextoHistorial = "Elimino la clase de permuta para  " . $doc->getNombre_docen() . " " . $doc->getApellidoP_docen() . " " . $doc->getApellidoM_docen() . ",grupo " . $grupo->getClave() . ", Hora " . $hora->getTexto_hora();
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Permutas de clase");

        $DaoPermutaClase->delete($_POST['Id_per']);

        update_page($_POST['Id_doc'], $_POST['Id_ciclo']);    
    }

}


if($_POST['action']=="verificarDisponibilidad"){
    $resp=array();
    $base= new base();
    $DaoHoras= new DaoHoras();
    $DaoAulas= new DaoAulas();
    $DaoHorarioDocente= new DaoHorarioDocente();
    $DaoDocentes= new DaoDocentes();
    $DaoGrupos= new DaoGrupos();

    $dia=date("N",strtotime($_POST['FechaPermuta']));  
    $queryDia="";
    if($dia==1){
           $queryDia="AND Lunes=1";
    }elseif($dia==2){
           $queryDia="AND Martes=1";
    }elseif($dia==3){
           $queryDia="AND Miercoles=1"; 
    }elseif($dia==4){
           $queryDia="AND Jueves=1"; 
    }elseif($dia==5){
           $queryDia="AND Viernes=1"; 
    }elseif($dia==6){
           $queryDia="AND Sabado=1"; 
    }elseif($dia==7){
           $queryDia="AND Domingo=1"; 
    }
    
    $resp['Ban']=0;
    $texto="";
    $query = "SELECT * FROM Horario_docente WHERE Id_ciclo=".$_POST['Id_ciclo']." AND Aula=".$_POST['Id_aula']." AND Hora=".$_POST['Id_Hora']." ".$queryDia."  ORDER BY Hora ASC";
    foreach($base->advanced_query($query) as $row_Horario_docente){
           $resp['Ban']=1;
           $doc=$DaoDocentes->show($row_Horario_docente['Id_docente']);
           $grupo=$DaoGrupos->show($row_Horario_docente['Id_grupo']);
           $hora=$DaoHoras->show($row_Horario_docente['Hora']);
           $texto.="-".mb_strtoupper($doc->getNombre_docen()." ".$doc->getApellidoP_docen()." ".$doc->getApellidoM_docen())."\n Grupo ".mb_strtoupper($grupo->getClave()).", Horario de ".$hora->getTexto_hora()."\n\n ";
    }
    $resp['Texto']="Salón ocupado por: \n\n".$texto." \n\n¿Desea continuar?";
    echo json_encode($resp);
}



