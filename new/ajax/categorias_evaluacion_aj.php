<?php
require_once('activate_error.php');
require_once('../require_daos.php');

if ($_POST['action'] == "mostrarCategoria") {
    $DaoCategoriasEvaluacion = new DaoCategoriasEvaluacion();
    $nombre = "";
    $id_cat = 0;
    if (isset($_POST['Id_cat']) && $_POST['Id_cat'] > 0) {
        $categoria = $DaoCategoriasEvaluacion->show($_POST['Id_cat']);
        $nombre = $categoria->getNombre();
        $id_cat = $categoria->getId();
    }
    ?>
    <div class="box-emergente-form">
        <h1>Categoría</h1>
        <p>Nombre<br><input type="text" value="<?php echo $nombre ?>" id="nombre-cat"/></p>
        <p>
            <button onclick="saveCategoria(<?php echo $id_cat ?>)">Guardar</button>
            <button onclick="ocultar_error_layer()">Cancelar</button>
        </p>
    </div>
    <?php
}

if($_POST['action']=="saveCategoria"){
    $DaoCategoriasEvaluacion = new DaoCategoriasEvaluacion();
    $DaoUsuarios = new DaoUsuarios();
    $_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    if($_POST['Id_cat']>0){
        $Categoria=$DaoCategoriasEvaluacion->show($_POST['Id_cat']);
        $Categoria->setNombre($_POST['nombre']);
        $DaoCategoriasEvaluacion->update($Categoria);
        $Id_cat=$_POST['Id_cat'];
    }else{
        $Categoria = new CategoriasEvaluacion();
        $Categoria->setNombre($_POST['nombre']);
        $Categoria->setDatecCeated(date('Y-m-d'));
        $Categoria->setId_usu($_usu->getId());
        $Categoria->setActivo(1);
        $Categoria->setId_plantel($_usu->getId_plantel());
        $Id_cat=$DaoCategoriasEvaluacion->add($Categoria);   
    }
}


if($_POST['action']=="updatePage"){
   updatePage();
}

if($_POST['action']=="eliminarCategoria"){
    $DaoCategoriasEvaluacion = new DaoCategoriasEvaluacion();
    $DaoCategoriasEvaluacion->delete($_POST['Id_cat']);
}


if($_POST['action']=="eliminarCampo"){
    $DaoCamposCategoria= new DaoCamposCategoria();
    $DaoCamposCategoria->delete($_POST['Id_campo']);
}


if ($_POST['action'] == "mostrarCampo") {
    $DaoCategoriasEvaluacion = new DaoCategoriasEvaluacion();
    $DaoCamposCategoria = new DaoCamposCategoria();

    $id_campo = 0;
    $id_cat=0;
    $nombre = "";
    $valor="";
    $TipoCamp="";
    $checked="";
    if (isset($_POST['Id_campo']) && $_POST['Id_campo'] > 0) {
        $campo = $DaoCamposCategoria->show($_POST['Id_campo']);
        $nombre = $campo->getNombre();
        $valor=$campo->getValor();
        $TipoCamp=$campo->getTipo_camp();
        $id_campo = $campo->getId();
        $id_cat=$campo->getId_cat();
        $checked='disabled="disabled"';
    }
    ?>
    <div class="box-emergente-form">
        <h1>Campo</h1>
        <p>Categoría<br>
            <select id="select-categorias" <?php echo $checked?>>
                <option value="0"></option>
            <?php
             foreach($DaoCategoriasEvaluacion->getCategoriasEvaluacion() as $cat){
               ?>
                <option value="<?php echo $cat->getId();?>" <?php if($id_cat==$cat->getId()){ ?> selected="selected" <?php } ?>><?php echo $cat->getNombre();?></option>
                <?php
             }
            ?>
            </select>
        </p>
        <p>Nombre<br><input type="text" value="<?php echo $nombre ?>" id="nombre"/></p>
        <p>Puntaje máximo<br><input type="text" value="<?php echo $valor ?>" id="valor"/></p>
        <p>Tipo de campo<br>
            <select id="tipo-campo">
                <option value="0"></option>
                <option value="1" <?php if($TipoCamp=="1"){ ?> selected="selected" <?php } ?> >Caja de texto</option>
                <option value="2" <?php if($TipoCamp=="2"){ ?> selected="selected" <?php } ?> >Caja checkbox</option>
            </select>
        </p>
        <p>
            <button onclick="saveCampo(<?php echo $id_campo ?>)">Guardar</button>
            <button onclick="ocultar_error_layer()">Cancelar</button>
        </p>
    </div>
    <?php
}




function updatePage() {
$DaoCategoriasEvaluacion = new DaoCategoriasEvaluacion();
$DaoCamposCategoria = new DaoCamposCategoria();
$DaoUsuarios = new DaoUsuarios();
$_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
?>
    <tr>
    <td id="column_one">
        <div class="fondo">
            <div id="box_top">
                <h1><i class="fa fa-list-ul" aria-hidden="true"></i> Categorías de evaluación</h1>
            </div>
            <?php
            foreach ($DaoCategoriasEvaluacion->getCategoriasEvaluacion() as $cat) {
                ?>
                <div class="seccion" id_tipo="<?php echo $cat->getId() ?>">
                    <h2>Categoría</h2>
                    <p style="margin-bottom: 20px;">Nombre categoría<br>
                        <input type="text" value="<?php echo $cat->getNombre() ?>" class="nombre-cat" disabled="disabled"/>
                        <i class="fa fa-pencil-square-o" aria-hidden="true" title="Editar" onclick="mostrarCategoria(<?php echo $cat->getId()?>)"></i>
                        <i class="fa fa-trash" aria-hidden="true" title="Eliminar" onclick="eliminarCategoria(<?php echo $cat->getId()?>)"></i>
                    </p>
                    <table id="main-niveles">
                        <tbody>
                            <tr>
                                <td>
                                    <h2>Campos de la categoría</h2>
                                    <?php
                                    foreach ($DaoCamposCategoria->getCamposCategoria($cat->getId()) as $campo) {
                                        ?>
                                        <ul class="form" id_nivel="<?php echo $campo->getId() ?>">
                                            <li>Nombre del campo<br><input type="text" value="<?php echo $campo->getNombre(); ?>" class="nombre" disabled="disabled"/></li>
                                            <li>Valor máximo<br><input type="text" value="<?php echo $campo->getValor(); ?>" class="puntaje_profesor" disabled="disabled"/></li>
                                            <li>Tipo de campo<br>           
                                                <select disabled="disabled">
                                                    <option value="0"></option>
                                                    <option value="1" <?php if($campo->getTipo_camp()=="1"){ ?> selected="selected" <?php } ?> >Caja de texto</option>
                                                    <option value="2" <?php if($campo->getTipo_camp()=="2"){ ?> selected="selected" <?php } ?> >Caja checkbox</option>
                                                    <option value="3" <?php if($campo->getTipo_camp()=="3"){ ?> selected="selected" <?php } ?> >Caja radio</option>
                                                </select>
                                            </li>
                                            <li>
                                                <i class="fa fa-pencil-square-o" aria-hidden="true" title="Editar" onclick="mostrarCampo(<?php echo $campo->getId()?>)"></i>
                                                <i class="fa fa-trash" aria-hidden="true" title="Eliminar" onclick="eliminarCampo(<?php echo $campo->getId()?>)"></i>
                                            </li>
                                            <br>
                                        </ul>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?php
            }
            ?>
        </div>
    </td>
    <td id="column_two">
        <div id="box_menus">
            <?php
            require_once '../estandares/menu_derecho.php';
            ?>
            <ul> 
                <li class="link" onclick="mostrarCategoria(0)">Agregar categoría</li>
                <li class="link" onclick="mostrarCampo(0)">Agregar campo</li>
            </ul>
        </div> 
    </td>
</tr>
    <?php
}


if($_POST['action']=="saveCampo"){
    $DaoCamposCategoria = new DaoCamposCategoria();
    if($_POST['Id_campo']>0){
       $Campo=$DaoCamposCategoria->show($_POST['Id_campo']);
       $Campo->setNombre($_POST['nombre']);
       $Campo->setValor($_POST['valor']);
       $Campo->setTipo_camp($_POST['tipocampo']);
       $Campo->setId_cat($_POST['id_cat']);
       $DaoCamposCategoria->update($Campo);
    }else{
       $Campo= new CamposCategoria();
       $Campo->setNombre($_POST['nombre']);
       $Campo->setValor($_POST['valor']);
       $Campo->setTipo_camp($_POST['tipocampo']);
       $Campo->setId_cat($_POST['id_cat']);
       $Campo->setActivo(1);
       $DaoCamposCategoria->add($Campo);
    }
}
