<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 


if($_POST['action']=="buscarAlum"){
        $base= new base();
        $DaoUsuarios= new DaoUsuarios();
        $_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        
	$query = "SELECT *
	FROM inscripciones_ulm 
        JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
        WHERE
	(Nombre_ins LIKE '%".$_POST['buscar']."%' 
        OR ApellidoP_ins LIKE '%".str_replace(' ','',$_POST['buscar'])."%' 
        OR ApellidoM_ins LIKE '%".str_replace(' ','',$_POST['buscar'])."%' 
        OR Matricula LIKE '%".str_replace(' ','',$_POST['buscar'])."%')
        AND tipo=1 
        AND Activo_alum=1 
        AND Id_plantel=".$_usu->getId_plantel()." 
        AND Baja_ofe IS NULL 
        AND FechaCapturaEgreso IS NULL
        AND IdCicloEgreso IS NULL 
        AND IdUsuEgreso IS NULL     
        ORDER BY Id_ins DESC LIMIT 20";	               
        foreach($base->advanced_query($query) as $row_BusquedaPro){
	  ?>
             <tr id_alum="<?php echo $row_BusquedaPro['Id_ins'];?>">
                  <td onclick="mostrar(<?php echo $row_BusquedaPro['Id_ins']; ?>)"><?php echo $row_BusquedaPro['Matricula']; ?></td>
                  <td style="width: 115px;"><?php echo $row_BusquedaPro['Nombre_ins'] . " " . $row_BusquedaPro['ApellidoP_ins'] . " " . $row_BusquedaPro['ApellidoM_ins'] ?></td>
                  <td><a href="mailto:<?php echo $row_BusquedaPro['Email_ins']; ?>"><?php echo $row_BusquedaPro['Email_ins']; ?></a></td>
                  <td><?php echo $base->formatFecha($row_BusquedaPro['Alta_alum']) ?></td>
                  <td> <input type="checkbox"></td>
              </tr>
                
	  <?php
	}
}


if($_POST['action']=="mostrar_box_email"){
?>
<div id="box_emergente" class="send-email">
      <h1><i class="fa fa-envelope"></i> Enviar email</h1>
      <p><input type="text" placeholder="Asunto" id="asunto"/></p>
      <ul id="tipo-email">
          <li><input type="checkbox" id="cAlumno"/> Alumnos</li>
          <li><input type="checkbox" id="cPadres"/> Padres o Tutores</li>
      </ul>
      <div id="box-box">
          <div id="box-mensaje">
              <textarea class="ckeditor" name="editor1"></textarea>
          </div>
      </div>
      <ul id="list_archivos">
          <?php
           $DaoAttachments= new DaoAttachments();
            foreach ($DaoAttachments->getAttachmentsUsu($_COOKIE['admin/Id_usu'],"usu") as $k=>$v){
                ?>
                <li><?php echo $v->getNombre_atta()?> <i class="fa fa-trash-o" onclick="delete_attac(<?php echo $v->getId()?>)"></i></li>     
                <?php
            }
          ?>          
      </ul>
      <p><button onclick="send_email(this)" id="boton-email">Enviar</button><button onclick="ocultar_error_layer()">Cerrar</button></p>
      <span id="buttonAdjuntar" onclick="mostrarFinder()"><i class="fa fa-paperclip" style="font-size: 19px;"></i></span>
</div>
<?php	
}


if($_POST['action']=="send_email"){
   $DaoUsuarios= new DaoUsuarios();
   $DaoPlanteles= new DaoPlanteles();
   $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
   $plantel=$DaoPlanteles->show($usu->getId_plantel());
   
   foreach($_POST['alumnos'] as $v){
     $DaoAlumnos= new DaoAlumnos();
     $alum=$DaoAlumnos->show($v);
     if(strlen($alum->getEmail())>0){
       $base = new base();
       $arrayData= array();
       $arrayData['Asunto']=$_POST['asunto'];
       $arrayData['Mensaje']=$_POST['mensaje'];
       $arrayData['IdRel']=$_COOKIE['admin/Id_usu'];
       $arrayData['TipoRel']="usu";
       $arrayData['email-usuario']= $plantel->getEmail();
       $arrayData['nombre-usuario']=$DaoPlanteles->covertirCadena($plantel->getNombre_plantel(),2);
       $arrayData['replyTo']=$usu->getEmail_usu();
       
       if($_POST['alum']==1){
           $arrayData['Destinatarios']=array();
           $Data= array();
           $Data['email']= $alum->getEmail();
           $Data['name']= $alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
           array_push($arrayData['Destinatarios'], $Data);
           $base->send_email($arrayData);
       }
       
        if($_POST['padre']==1){
           $arrayData['Destinatarios']=array();
           $Data= array();
           $Data['email']= $alum->getEmailTutor();
           $Data['name']= $alum->getNombreTutor();
           array_push($arrayData['Destinatarios'], $Data);
           $base->send_email($arrayData);
       }
     }
   }
}



