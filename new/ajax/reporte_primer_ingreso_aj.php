<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if(isset($_POST['action']) && $_POST['action']=="filtro"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoMediosEnterar= new DaoMediosEnterar();
    $DaoCiclos= new DaoCiclos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoTurnos= new DaoTurnos();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $query="";

    if(isset($_POST['Id_ofe']) && $_POST['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_POST['Id_ofe'];
    }
    if(isset($_POST['Id_esp']) && $_POST['Id_esp']>0){
        $query=$query." AND Id_esp=".$_POST['Id_esp'];
    }
    if(isset($_POST['Id_ori']) && $_POST['Id_ori']>0){
        $query=$query." AND Id_ori=".$_POST['Id_ori'];
    }
    if(isset($_POST['Opcion_pago']) && $_POST['Opcion_pago']>0){
        $query=$query." AND Opcion_pago=".$_POST['Opcion_pago'];
    }
    if(isset($_POST['Id_usu']) && $_POST['Id_usu']>0){
        $query=$query." AND Id_usu_capturo=".$_POST['Id_usu'];
    }
    
    $Tipo="Interesado";
    if($_POST['Tipo']==1){
        $Tipo="Alumno";
    }
    
    $Id_ciclo=0;
    $ciclo=$DaoCiclos->show($Id_ciclo);
    if($_POST['Id_ciclo']>0){
        $ciclo=$DaoCiclos->show($_POST['Id_ciclo']);
        $Id_ciclo=$_POST['Id_ciclo'];
    }
    
    if(strlen($_POST['Fecha_ini'])>0 && $_POST['Fecha_fin']==null){
        $query=$query." AND Fecha_ins>='".$_POST['Fecha_ini']."'";
        if($_POST['Id_ciclo']>0){
            $ciclo=$DaoCiclos->getCicloPorFecha($_POST['Fecha_ini']);
            $Id_ciclo=$ciclo->getId();
        }
    }elseif(strlen($_POST['Fecha_fin'])>0 && $_POST['Fecha_ini']==null){
        $query=$query." AND Fecha_ins<='".$_POST['Fecha_fin']."'";
        if($_POST['Id_ciclo']>0){
            $ciclo=$DaoCiclos->getCicloPorFecha($_POST['Fecha_fin']);
            $Id_ciclo=$ciclo->getId();
        }
    }elseif(strlen($_POST['Fecha_ini'])>0 && strlen($_POST['Fecha_fin'])>0){
        $query=$query." AND (Fecha_ins>='".$_POST['Fecha_ini']."' AND Fecha_ins<='".$_POST['Fecha_fin']."')";
        if($_POST['Id_ciclo']>0){
            $ciclo=$DaoCiclos->getCicloPorFecha($_POST['Fecha_ini']);
            $Id_ciclo=$ciclo->getId();
        }
    }

    $count=1;
    $query="SELECT * FROM inscripciones_ulm 
        JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
        LEFT JOIN usuarios_ulm ON usuarios_ulm.Id_usu=inscripciones_ulm.Id_usu_capturo
        WHERE tipo=".$_POST['Tipo']."  
              AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()." 
              AND Activo_oferta=1 
              AND Baja_ofe IS NULL 
              AND FechaCapturaEgreso IS NULL
              AND IdCicloEgreso IS NULL
              AND IdUsuEgreso IS NULL ".$query." 
        ORDER BY Id_ins DESC";
    $consulta=$base->advanced_query($query);
    $row_consulta = $consulta->fetch_assoc();
    $totalRows_consulta= $consulta->num_rows;
     if($totalRows_consulta>0){
       do{
           
            $nombre_ori="";
            $oferta = $DaoOfertas->show($row_consulta['Id_ofe']);
            $esp = $DaoEspecialidades->show($row_consulta['Id_esp']);
            if ($row_consulta['Id_ori'] > 0) {
               $ori = $DaoOrientaciones->show($row_consulta['Id_ori']);
               $nombre_ori = $ori->getNombre();
             }
             $opcion="Plan por materias"; 
             if($row_consulta['Opcion_pago']==2){
               $opcion="Plan completo";  
             }

             //$Grado=$DaoOfertasAlumno->getLastCicloOfertaSinCiclo($row_consulta['Id_ofe_alum']);
             //$Grado=$DaoGrados->show($Grado['Id_grado']);

             $MedioEnt="";
             if($row_consulta['Id_medio_ent']>0){
               $medio=$DaoMediosEnterar->show($row_consulta['Id_medio_ent']);
               $MedioEnt=$medio->getMedio();
             }

            $Medio_aux=$row_consulta['Id_medio_ent'];
            if($_POST['Id_medio']>0){
                $Medio_aux=$_POST['Id_medio'];
            }
            
            $Turno_aux=$row_consulta['Turno'];
            if($_POST['Turno']>0){
                $Turno_aux=$_POST['Turno'];
            }
            
            $tur = $DaoTurnos->show($row_consulta['Turno']);
            $t=$tur->getNombre();
            
            $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=" . $row_consulta['Id_ofe_alum']." ORDER BY Id_ciclo_alum  ASC";
            $consulta_dos=$base->advanced_query($query_ciclos_alum_ulm);
            $row_consulta_dos = $consulta_dos->fetch_assoc();
            $totalRows_consulta_dos= $consulta_dos->num_rows;
            $id_ciclo_aux=0;
            if($Id_ciclo>0){
               $id_ciclo_aux=$row_consulta_dos['Id_ciclo'];
            }
            if($row_consulta['Id_medio_ent']==$Medio_aux && $id_ciclo_aux==$Id_ciclo && $row_consulta['Turno']==$Turno_aux){
             ?>
                      <tr id_alum="<?php echo $row_consulta['Id_ins'];?>" id_ofe_alum="<?php echo $row_consulta['Id_ofe_alum'];?>">
                        <td><?php echo $count ?></td>
                        <td><?php echo $Tipo;?></td>
                        <td><?php echo $ciclo->getClave();?></td>
                        <td><?php echo $row_consulta['Matricula'] ?></td>
                        <td style="width: 115px;"><?php echo $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins'] ?></td>
                        <td><?php echo $row_consulta['Tel_ins'] ?></td>
                        <td><?php echo $row_consulta['Cel_ins'] ?></td>
                        <td><?php echo $row_consulta['Email_ins'] ?></td>
                        <td><?php echo $oferta->getNombre_oferta(); ?></td>
                        <td><?php echo $esp->getNombre_esp(); ?></td>
                        <td><?php echo $nombre_ori; ?></td>
                        <td><?php echo $t;?></td>
                        <td><?php echo $row_consulta['Nombre_usu']." ".$row_consulta['ApellidoP_usu']." ".$row_consulta['ApellidoM_usu'];?></td>
                        <td><?php echo $opcion; ?></td>
                        <td><?php echo $MedioEnt; ?></td>
                        <td><input type="checkbox"> </td>
                      </tr>
                      <?php
                      $count++;
                }
        }while($row_consulta = $consulta->fetch_assoc());
    }
}




if(isset($_GET['action']) && $_GET['action']=="download_excel"){
    $base= new base();
    $DaoTurnos= new DaoTurnos();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'TIPO');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'CICLO');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'MATRICULA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'OFERTA');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'CARRERA');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'ORIENTACIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'OPCIÓN DE PAGO');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'SE ENTERO POR');
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'TEL.');
    $objPHPExcel->getActiveSheet()->setCellValue('L1', 'CEL.');
    $objPHPExcel->getActiveSheet()->setCellValue('M1', 'EMAIL');
    $objPHPExcel->getActiveSheet()->setCellValue('N1', 'TURNO');
    $objPHPExcel->getActiveSheet()->setCellValue('O1', 'USUARIO');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','O') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoMediosEnterar= new DaoMediosEnterar();
    $DaoCiclos= new DaoCiclos();
    $DaoUsuarios= new DaoUsuarios();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $query="";

    if($_GET['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_GET['Id_ofe'];
    }
    if($_GET['Id_esp']>0){
        $query=$query." AND Id_esp=".$_GET['Id_esp'];
    }
    if($_GET['Id_ori']>0){
        $query=$query." AND Id_ori=".$_GET['Id_ori'];
    }
    if($_GET['Opcion_pago']>0){
        $query=$query." AND Opcion_pago=".$_GET['Opcion_pago'];
    }
    
    if(isset($_GET['Id_usu']) && $_GET['Id_usu']>0){
        $query=$query." AND Id_usu_capturo=".$_GET['Id_usu'];
    }
    $Tipo="Interesado";
    if($_GET['Tipo']==1){
        $Tipo="Alumno";
    }
    
    $Id_ciclo=0;
    $ciclo=$DaoCiclos->show($Id_ciclo);
    if($_GET['Id_ciclo']>0){
        $ciclo=$DaoCiclos->show($_GET['Id_ciclo']);
        $Id_ciclo=$_GET['Id_ciclo'];
    }
    
    if(strlen($_GET['Fecha_ini'])>0 && $_GET['Fecha_fin']==null){
        $query=$query." AND Fecha_ins>='".$_GET['Fecha_ini']."'";
        if($_GET['Id_ciclo']>0){
            $ciclo=$DaoCiclos->getCicloPorFecha($_GET['Fecha_ini']);
            $Id_ciclo=$ciclo->getId();
        }
    }elseif(strlen($_GET['Fecha_fin'])>0 && $_GET['Fecha_ini']==null){
        $query=$query." AND Fecha_ins<='".$_GET['Fecha_fin']."'";
        if($_GET['Id_ciclo']>0){
            $ciclo=$DaoCiclos->getCicloPorFecha($_GET['Fecha_fin']);
            $Id_ciclo=$ciclo->getId();
        }
    }elseif(strlen($_GET['Fecha_ini'])>0 && strlen($_GET['Fecha_fin'])>0){
        $query=$query." AND (Fecha_ins>='".$_GET['Fecha_ini']."' AND Fecha_ins<='".$_GET['Fecha_fin']."')";
        if($_GET['Id_ciclo']>0){
            $ciclo=$DaoCiclos->getCicloPorFecha($_GET['Fecha_ini']);
            $Id_ciclo=$ciclo->getId();
        }
    }

    $count=1;
    $query="SELECT * FROM inscripciones_ulm 
        JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
        LEFT JOIN usuarios_ulm ON usuarios_ulm.Id_usu=inscripciones_ulm.Id_usu_capturo
        WHERE tipo=".$_GET['Tipo']."  
              AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()." 
              AND Activo_oferta=1 
              AND Baja_ofe IS NULL 
              AND FechaCapturaEgreso IS NULL
              AND IdCicloEgreso IS NULL
              AND IdUsuEgreso IS NULL ".$query." 
        ORDER BY Id_ins DESC";
    $consulta=$base->advanced_query($query);
    $row_consulta = $consulta->fetch_assoc();
    $totalRows_consulta= $consulta->num_rows;
     if($totalRows_consulta>0){
       do{
           
            $nombre_ori="";
            $oferta = $DaoOfertas->show($row_consulta['Id_ofe']);
            $esp = $DaoEspecialidades->show($row_consulta['Id_esp']);
            if ($row_consulta['Id_ori'] > 0) {
               $ori = $DaoOrientaciones->show($row_consulta['Id_ori']);
               $nombre_ori = $ori->getNombre();
             }
             $opcion="Plan por materias"; 
             if($row_consulta['Opcion_pago']==2){
               $opcion="Plan completo";  
             }

             //$Grado=$DaoOfertasAlumno->getLastCicloOfertaSinCiclo($row_consulta['Id_ofe_alum']);
             //$Grado=$DaoGrados->show($Grado['Id_grado']);

             $MedioEnt="";
             if($row_consulta['Id_medio_ent']>0){
               $medio=$DaoMediosEnterar->show($row_consulta['Id_medio_ent']);
               $MedioEnt=$medio->getMedio();
             }

            $Medio_aux=$row_consulta['Id_medio_ent'];
            if($_GET['Id_medio']>0){
                $Medio_aux=$_GET['Id_medio'];
            }
            
            $Turno_aux=$row_consulta['Turno'];
            if($_GET['Turno']>0){
                $Turno_aux=$_GET['Turno'];
            }
            
            $tur = $DaoTurnos->show($row_consulta['Turno']);
            $t=$tur->getNombre();
            
            $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=" . $row_consulta['Id_ofe_alum']." ORDER BY Id_ciclo_alum  ASC";
            $consulta_dos=$base->advanced_query($query_ciclos_alum_ulm);
            $row_consulta_dos = $consulta_dos->fetch_assoc();
            $totalRows_consulta_dos= $consulta_dos->num_rows;
            //echo "Id_medio:".$row_consulta['Id_medio_ent']."==".$Medio_aux." && ".$row_consulta_dos['Id_ciclo']."==".$_GET['Id_ciclo']."&&". $row_consulta['Turno']."==".$Turno_aux;
            $id_ciclo_aux=0;
            if($Id_ciclo>0){
               $id_ciclo_aux=$row_consulta_dos['Id_ciclo'];
            }
           if($row_consulta['Id_medio_ent']==$Medio_aux && $id_ciclo_aux==$Id_ciclo && $row_consulta['Turno']==$Turno_aux){
                $count++;
                $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
                $objPHPExcel->getActiveSheet()->setCellValue("B$count", $Tipo);
                $objPHPExcel->getActiveSheet()->setCellValue("C$count", $ciclo->getClave());
                $objPHPExcel->getActiveSheet()->setCellValue("D$count", $row_consulta['Matricula']);
                $objPHPExcel->getActiveSheet()->setCellValue("E$count", $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins']);
                $objPHPExcel->getActiveSheet()->setCellValue("F$count", $oferta->getNombre_oferta());
                $objPHPExcel->getActiveSheet()->setCellValue("G$count", $esp->getNombre_esp());
                $objPHPExcel->getActiveSheet()->setCellValue("H$count", $nombre_ori);
                $objPHPExcel->getActiveSheet()->setCellValue("I$count", $opcion);
                $objPHPExcel->getActiveSheet()->setCellValue("J$count", $MedioEnt);
                $objPHPExcel->getActiveSheet()->setCellValue("K$count", $row_consulta['Tel_ins']);
                $objPHPExcel->getActiveSheet()->setCellValue("L$count", $row_consulta['Cel_ins']);
                $objPHPExcel->getActiveSheet()->setCellValue("M$count", $row_consulta['Email_ins']);
                $objPHPExcel->getActiveSheet()->setCellValue("N$count", $t);
                $objPHPExcel->getActiveSheet()->setCellValue("N$count", $row_consulta['Nombre_usu']." ".$row_consulta['ApellidoP_usu']." ".$row_consulta['ApellidoM_usu']);
                
           }
        }while($row_consulta = $consulta->fetch_assoc());
    }


    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=Ingreso-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}

