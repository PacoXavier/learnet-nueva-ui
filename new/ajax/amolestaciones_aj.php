<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="buscar_int"){
    ?>
    <li><b style="color: black;">Alumnos</b></li>
    <?php 
    $DaoAlumnos= new DaoAlumnos();
    foreach($DaoAlumnos->buscarAlumno($_POST['buscar']) as $k=>$v){
      ?>
    <a href="amolestaciones.php?id=<?php echo $v->getId()?>&tipo=alum"> <li id-data="<?php echo $v->getId();?>" tipo_usu="alum"><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li></a>
      <?php      
    }
    ?>
    <li><b style="color: black;">Usuarios</b></li>
    <?php 
    $DaoUsuarios= new DaoUsuarios();
    foreach($DaoUsuarios->buscarUsuario($_POST['buscar']) as $k=>$v){
      ?>
    <a href="amolestaciones.php?id=<?php echo $v->getId()?>&tipo=usu"> <li id-data="<?php echo $v->getId();?>" tipo_usu="usu"><?php echo $v->getNombre_usu()." ".$v->getApellidoP_usu()." ".$v->getApellidoM_usu() ?></li></a>
     <?php      
    }
    ?>
    <li><b style="color: black;">Docentes</b></li>
    <?php 
    $DaoDocentes= new DaoDocentes();
    foreach($DaoDocentes->buscarDocente($_POST['buscar']) as $k=>$v){
      ?>
    <a href="amolestaciones.php?id=<?php echo $v->getId()?>&tipo=docen"> <li id-data="<?php echo $v->getId();?>" tipo_usu="docen"><?php echo $v->getNombre_docen()." ".$v->getApellidoP_docen()." ".$v->getApellidoM_docen() ?></li></a>
      <?php      
    }
}



if($_POST['action']=="mostrar_box_amolestacion"){
    $DaoCiclos= new DaoCiclos();
    ?>
    <div id="box_emergente" class="box_materias">
      <h1><i class="fa fa-exclamation-circle"></i> Amonestar</h1>
      <p>Ciclo<br>
      <select id="Id_ciclo">
        <option value="0"></option>
        <?php
            foreach($DaoCiclos->getCiclosFuturos() as $k=>$v){
             ?>
                <option value="<?php echo $v->getId() ?>" <?php if($Id_ciclo==$v->getId()){?> selected="selected" <?php }?> ><?php echo $v->getClave() ?></option>
            <?php
              }
            ?>
        </select>
      </p>
      <p>Fecha del reporte<br><input type="date" id="fecha_reporte"/></p>
      <p>Reporta<br><input type="text"  class="buscarFiltro" onkeyup="buscarReporta()" placeholder="Nombre"/></p>
      <ul class="UlbuscadorReporta"></ul>
      <p>Motivo<br><textarea id="motivo"></textarea></p>
      <button onclick="add_amolestacion(<?php echo $_POST['Id_rep'];?>)">Guardar</button>
      <button onclick="ocultar_error_layer()">Cancelar</button>
</div>   
<?php
}



if($_POST['action']=="add_amolestacion"){
    $DaoAmonestaciones= new DaoAmonestaciones();
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos = new DaoAlumnos();
    $DaoDocentes = new DaoDocentes();
    $DaoCiclos = new DaoCiclos();
    if($_POST['Id_rep']>0){
        /*
        $Amonestaciones=$DaoAmonestaciones->show($_POST['Id_rep']);
        $Amonestaciones->setFecha_rep($Fecha_rep);
        $Amonestaciones->setMotivo($_POST['Motivo']);
        $Amonestaciones->setId_ciclo($_POST['Id_ciclo']); 
        $Amonestaciones->setId_recibe($_POST['Id_amolestado']);
        $Amonestaciones->setTipo_recibe($_POST['Tipo_amolestado']);
        $Amonestaciones->setId_reporta($_POST['Id_amolesta']);
        $Amonestaciones->setTipo_reporta($_POST['Tipo_amolesta']);
        $DaoAmonestaciones->update($Amonestaciones);
         */
    }else{
        $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        $Amonestaciones= new Amonestaciones();
        $Amonestaciones->setFechaCaptura(date('Y-m-d H:i:s'));
        $Amonestaciones->setFecha_rep($_POST['Fecha_rep']);
        $Amonestaciones->setId_ciclo($_POST['Id_ciclo']);
        $Amonestaciones->setId_plantel($usu->getId_plantel());
        $Amonestaciones->setId_recibe($_POST['Id_amolestado']);
        $Amonestaciones->setTipo_recibe($_POST['Tipo_amolestado']);
        $Amonestaciones->setId_reporta($_POST['Id_amolesta']);
        $Amonestaciones->setTipo_reporta($_POST['Tipo_amolesta']);
        $Amonestaciones->setId_usu($_COOKIE['admin/Id_usu']);
        $Amonestaciones->setMotivo($_POST['Motivo']);
        $DaoAmonestaciones->add($Amonestaciones);
     
        
        if ($_POST['Id_amolestado'] > 0 && $_POST['Tipo_amolestado'] == "alum") {
            $alum = $DaoAlumnos->show($_POST['Id_amolestado']);
            $NombreRecibe = $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM();
        } elseif ($_POST['Id_amolestado'] > 0 && $_POST['Tipo_amolestado'] == "usu") {
            $usu_recibe = $DaoUsuarios->show($_POST['Id_amolestado']);
            $NombreRecibe = $usu_recibe->getNombre_usu() . " " . $usu_recibe->getApellidoP_usu() . " " . $usu_recibe->getApellidoM_usu();
        } elseif ($_POST['Id_amolestado'] > 0 && $_POST['Tipo_amolestado'] == "docen") {
            $doc = $DaoDocentes->show($_POST['Id_amolestado']);
            $NombreRecibe = $doc->getNombre_docen() . " " . $doc->getApellidoP_docen() . " " . $doc->getApellidoM_docen();
        }
   
        $TextoHistorial="Añade amonestación para ".$NombreRecibe." el día ".$_POST['Fecha_rep'];
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Amonestaciones");
    
    }
    update_page($_POST['Id_amolestado'],$_POST['Tipo_amolestado']);

}




function update_page($Id_amolestado,$Tipo_amolestado) {
    $base = new base();
    $DaoAlumnos = new DaoAlumnos();
    $DaoDocentes = new DaoDocentes();
    $DaoCiclos = new DaoCiclos();
    $DaoAmonestaciones = new DaoAmonestaciones();
    $DaoUsuarios= new DaoUsuarios();
    $_usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-exclamation-circle"></i> Amonestaciones</h1>
                </div>
                <div class="seccion">
                    <div id="box_uno">
                        <?php
                        if ($Id_amolestado > 0 && $Tipo_amolestado == "alum") {
                            $tipoRecibe = "Alumno";
                            $tipo = "alum";
                            $alum = $DaoAlumnos->show($Id_amolestado);
                            $Id = $alum->getId();
                            $ClaveRecibe = $alum->getMatricula();
                            $NombreRecibe = $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM();
                            $cantidad = $DaoAmonestaciones->getCantidadAmonestaciones($Id, "alum");
                        } elseif ($Id_amolestado > 0 && $Tipo_amolestado == "usu") {
                            $tipoRecibe = "Usuario";
                            $tipo = "usu";
                            $usu_recibe = $DaoUsuarios->show($Id_amolestado);
                            $Id = $usu_recibe->getId();
                            $ClaveRecibe = $usu_recibe->getClave_usu();
                            $NombreRecibe = $usu_recibe->getNombre_usu() . " " . $usu_recibe->getApellidoP_usu() . " " . $usu_recibe->getApellidoM_usu();
                            $cantidad = $DaoAmonestaciones->getCantidadAmonestaciones($Id, "usu");
                        } elseif ($Id_amolestado > 0 && $Tipo_amolestado == "docen") {
                            $tipoRecibe = "Docente";
                            $tipo = "docen";
                            $doc = $DaoDocentes->show($Id_amolestado);
                            $Id = $doc->getId();
                            $ClaveRecibe = $doc->getClave_docen();
                            $NombreRecibe = $doc->getNombre_docen() . " " . $doc->getApellidoP_docen() . " " . $doc->getApellidoM_docen();
                            $cantidad = $DaoAmonestaciones->getCantidadAmonestaciones($Id, "docen");
                        }
                        ?>
                        <ul class="form">
                            <li>Nombre<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o clave" value="<?php echo $NombreRecibe ?>"/>
                                <ul id="buscador_int"></ul>
                            </li>
                        </ul>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td colspan="7" style="text-align: center;font-size: 11px;">Amonestaciones</td>
                                </tr>
                                <tr>
                                    <td>#</td>
                                    <td>Ciclo</td>
                                    <td style="width:60px;">Fecha</td>
                                    <td>Usuario reporta</td>
                                    <td style="width:225px;">Motivo</td>
                                    <td>Usuario recibe</td>
                                    <td>Acciones</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                                $count = 1;
                                foreach ($DaoAmonestaciones->getAmonestacionesReportado($Id_amolestado, $Tipo_amolestado) as $k => $v) {
                                    $u = $DaoUsuarios->show($v->getId_usu());
                                    $ciclo = $DaoCiclos->show($v->getId_ciclo());

                                    if ($v->getTipo_reporta() == "usu") {
                                        $usu_reporta = $DaoUsuarios->show($v->getId_reporta());
                                        $ClaveReporta = $usu_reporta->getClave_usu();
                                        $NombreReporta = $usu_reporta->getNombre_usu() . " " . $usu_reporta->getApellidoP_usu() . " " . $usu_reporta->getApellidoM_usu();
                                    } elseif ($v->getTipo_reporta() == "docen") {
                                        $doc = $DaoDocentes->show($v->getId_reporta());
                                        $ClaveReporta = $doc->getClave_docen();
                                        $NombreReporta = $doc->getNombre_docen() . " " . $doc->getApellidoP_docen() . " " . $doc->getApellidoM_docen();
                                    }
                                    ?>
                                    <tr id_rep="<?php echo $v->getId() ?>">
                                        <td><?php echo $count ?></td>
                                        <td><?php echo $ciclo->getClave() ?></td>
                                        <td><?php echo $v->getFecha_rep() ?></td>
                                        <td><?php echo $NombreReporta ?></td>
                                        <td><?php echo $v->getMotivo() ?></td>
                                        <td><?php echo $u->getNombre_usu() . " " . $u->getApellidoP_usu() . " " . $u->getApellidoM_usu() ?></td>
                                        <td style="width:110px;">
                                            <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                                            <div class="box-buttom">
                                                <button onclick="delete_rep(<?php echo $v->getId() ?>)">Eliminar</button>
                                                <a href="acta_administrativa.php?id=<?php echo $v->getId() ?>&tipo=<?php echo $Tipo_amolestado ?>" target="_blank"><button>Formato</button></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $count++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once '../estandares/menu_derecho.php';
                ?>
                <ul>
                    <?php
                    if ($Id_amolestado > 0) {
                        ?>
                        <li><span onclick="mostrar_box_amolestacion()">Levantar amonestaci&oacute;n </span></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </td>
    </tr>
    <?php
}


if($_POST['action']=="delete_rep"){
    $DaoAmonestaciones= new DaoAmonestaciones();
    $DaoUsuarios= new DaoUsuarios();
    $amonestacion=$DaoAmonestaciones->show($_POST['Id_rep']);
    
    if ($amonestacion->getId_recibe() > 0 && $amonestacion->getTipo_recibe() == "alum") {
        $alum = $DaoAlumnos->show($amonestacion->getId_recibe());
        $NombreRecibe = $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM();
    } elseif ($amonestacion->getId_recibe() > 0 && $amonestacion->getTipo_recibe() == "usu") {
        $usu_recibe = $DaoUsuarios->show($amonestacion->getId_recibe());
        $NombreRecibe = $usu_recibe->getNombre_usu() . " " . $usu_recibe->getApellidoP_usu() . " " . $usu_recibe->getApellidoM_usu();
    } elseif ($amonestacion->getId_recibe() > 0 && $amonestacion->getTipo_recibe() == "docen") {
        $doc = $DaoDocentes->show($amonestacion->getId_recibe());
        $NombreRecibe = $doc->getNombre_docen() . " " . $doc->getApellidoP_docen() . " " . $doc->getApellidoM_docen();
    }


    $TextoHistorial="Elimina amonestación para ".$NombreRecibe." del día ".$amonestacion->getFecha_rep();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Amonestaciones");
        
    $DaoAmonestaciones->delete($_POST['Id_rep']); 
    update_page($_POST['Id_amolestado'],$_POST['Tipo_amolestado']);
}




if($_POST['action']=="buscarReporta"){
    ?>
    <li><b style="color: black;">Usuarios</b></li>
    <?php 
    $DaoUsuarios= new DaoUsuarios();
    foreach($DaoUsuarios->buscarUsuario($_POST['buscar']) as $k=>$v){
      ?>
       <li id-data="<?php echo $v->getId();?>" tipo_usu="usu"><?php echo $v->getNombre_usu()." ".$v->getApellidoP_usu()." ".$v->getApellidoM_usu() ?></li>
     <?php      
    }
    ?>
    <li><b style="color: black;">Docentes</b></li>
    <?php 
    $DaoDocentes= new DaoDocentes();
    foreach($DaoDocentes->buscarDocente($_POST['buscar']) as $k=>$v){
      ?>
       <li id-data="<?php echo $v->getId();?>" tipo_usu="docen"><?php echo $v->getNombre_docen()." ".$v->getApellidoP_docen()." ".$v->getApellidoM_docen() ?></li>
      <?php      
    }
}




