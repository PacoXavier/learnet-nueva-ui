<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="filtro"){
    
    $base= new base();
    $DaoHistorialUsuario= new DaoHistorialUsuario();
    $DaoCiclos=new DaoCiclos();
    $ciclo=$DaoCiclos->getActual();
    
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $queryd="";

    if(strlen($_POST['Categoria'])>0){
        $queryd=$queryd." AND Categoria='".$_POST['Categoria']."'";
    }
    if($_POST['Id_usu']>0){
        $queryd=$queryd." AND Id_usu=".$_POST['Id_usu'];
    }
    
    
    if($_POST['Id_ciclo']>0){
        $queryd=$queryd." AND Id_ciclo=".$_POST['Id_ciclo'];
    }else{
        if(strlen($_POST['Fecha_ini'])>0 && $_POST['Fecha_fin']==null){
            $queryd=$queryd." AND DateCreated>='".$_POST['Fecha_ini']."'";
        }elseif(strlen($_POST['Fecha_fin'])>0 && $_POST['Fecha_ini']==null){
            $queryd=$queryd." AND DateCreated<='".$_POST['Fecha_fin']."'";
        }elseif(strlen($_POST['Fecha_ini'])>0 && strlen($_POST['Fecha_fin'])>0){
            $queryd=$queryd." AND (DateCreated>='".$_POST['Fecha_ini']."' AND DateCreated<='".$_POST['Fecha_fin']."')";
        }else{
           $Id_ciclo=$ciclo->getId();
           $queryd=$queryd." AND Id_ciclo=".$Id_ciclo; 
        }
    }
    
    //echo $queryd;
    
    $count=1;
    $query="SELECT * FROM HistorialUsuario WHERE Id_plantel=".$usu->getId_plantel()." ".$queryd." ORDER BY Id DESC";
    foreach ($DaoHistorialUsuario->advanced_query($query) as $k=>$v){
        $_usu=$DaoUsuarios->show($v->getId_usu());
        $ciclo=$DaoCiclos->show($v->getId_ciclo());
       ?>
         <tr>
            <td><?php echo $count; ?></td>
            <td><?php echo $ciclo->getClave()?></td>
            <td><a href="usuario.php?id=<?php echo $_usu->getId(); ?>"> <?php echo $_usu->getNombre_usu() . " " . $_usu->getApellidoP_usu() . " " . $_usu->getApellidoM_usu(); ?></a></td>
            <td><?php echo $v->getCategoria() ?></a></td>
            <td><?php echo $v->getDateCreated(); ?></td>
            <td><?php echo $v->getTexto(); ?></td>
         </tr>
      <?php
         $count++;
    }
}


if($_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'CICLO');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'USUARIO');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'CATEGORÍA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'FECHA');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'MOVIMIENTO REALIZADO');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','F') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    
    $DaoHistorialUsuario= new DaoHistorialUsuario();
    $DaoCiclos=new DaoCiclos();
    $ciclo=$DaoCiclos->getActual();
    
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query="";

    if(strlen($_GET['Categoria'])>0){
        $query=$query." AND Categoria='".$_GET['Categoria']."'";
    }
    if($_GET['Id_usu']>0){
        $query=$query." AND Id_usu=".$_GET['Id_usu'];
    }
    
    
    $Id_ciclo=$ciclo->getId();
    if($_GET['Id_ciclo']>0){
        $Id_ciclo=$_GET['Id_ciclo'];
    }
    $count=1;
    $query="SELECT * FROM HistorialUsuario WHERE Id_plantel=".$usu->getId_plantel()." AND Id_ciclo=".$Id_ciclo." ".$query;
    foreach ($DaoHistorialUsuario->advanced_query($query) as $k=>$v){
        $_usu=$DaoUsuarios->show($v->getId_usu());
        $ciclo=$DaoCiclos->show($v->getId_ciclo());
        $count++;
        $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
        $objPHPExcel->getActiveSheet()->setCellValue("B$count", $ciclo->getClave());
        $objPHPExcel->getActiveSheet()->setCellValue("C$count", $_usu->getNombre_usu() . " " . $_usu->getApellidoP_usu() . " " . $_usu->getApellidoM_usu());
        $objPHPExcel->getActiveSheet()->setCellValue("D$count", $v->getCategoria());
        $objPHPExcel->getActiveSheet()->setCellValue("E$count", $v->getDateCreated());
        $objPHPExcel->getActiveSheet()->setCellValue("F$count", $v->getTexto());
    }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=HistorialUsuarios-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}



