<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="buscarAlum"){
    $DaoAlumnos= new DaoAlumnos();
    foreach($DaoAlumnos->buscarAlumno($_POST['buscar']) as $k=>$v){
      ?>
        <li id_alum="<?php echo $v->getId();?>"><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li>
      <?php      
    }
}





if($_POST['action']=="filtro"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoRecargos= new DaoRecargos();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoUsuarios= new DaoUsuarios();
    $DaoAnalisisSicologico= new DaoAnalisisSicologico();

    $query="";
    if($_POST['Id_alum']>0){
        $query="AND Id_ins=".$_POST['Id_alum'];
    }

    if($_POST['Id_oferta']>0){
        $query=$query." AND Id_ofe=".$_POST['Id_oferta'];
    }
    if($_POST['Id_esp']>0){
        $query=$query." AND Id_esp=".$_POST['Id_esp'];
    }
    if($_POST['Id_ori']>0){
        $query=$query." AND Id_ori=".$_POST['Id_ori'];
    }
    $DaoUsuarios = new DaoUsuarios();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
   $query = "SELECT * FROM inscripciones_ulm 
            JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
            WHERE tipo=1  AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()."  AND Activo_oferta=1 AND Baja_ofe IS NULL ".$query." ORDER BY Id_ins";
     $count=1;
     foreach($base->advanced_query($query) as $k=>$v){
           $alum = $DaoAlumnos->show($v['Id_ins']);
           $nombre_ori="";

           $oferta = $DaoOfertas->show($v['Id_ofe']);
           $esp = $DaoEspecialidades->show($v['Id_esp']);
           if ($v['Id_ori'] > 0) {
              $ori = $DaoOrientaciones->show($v['Id_ori']);
              $nombre_ori = $ori->getNombre();
            }
            $class="";
            $status="";
            $fecha="";
            $existe=$DaoAnalisisSicologico->showByAlum($alum->getId());
            if($existe->getId()>0){
               $fecha=$existe->getDateCreated(); 
               $status="Evaluado";
               $class="evaluado";
            }
              ?>
              <tr id_alum="<?php echo $alum->getId();?>" id_ofe_alum="<?php echo $v['Id_ofe_alum'];?>">
                <td><?php echo $count;?></td>
                <td><a href="alumno.php?id=<?php echo $alum->getId();?>"><?php echo $alum->getMatricula() ?></a></td>
                <td style="width: 115px;"><a href="alumno.php?id=<?php echo $alum->getId();?>"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></a></td>
                <td><?php echo $oferta->getNombre_oferta(); ?></td>
                <td><?php echo $esp->getNombre_esp(); ?></td>
                <td><?php echo $nombre_ori; ?></td>
                <td style="text-align: center;"><?php echo $fecha?></td>
                <td style="text-align: center;" class="<?php echo $class ?>"><?php echo $status;?></td>
                <!--<td><input type="checkbox"> </td>-->
                <td style="width:110px;">
                    <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span> 
                    <div class="box-buttom">
                        <a href="evaluar_sicologia.php?id=<?php echo $alum->getId();?>"><button>Evaluar</button></a>
                        <a href="graficas.php?id=<?php echo $alum->getId();?>" target="_blank"><button>Graficas</button></a>
                    </div>
                </td>
              </tr>
              <?php
              $count++; 
        }
}


