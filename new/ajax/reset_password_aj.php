<?php

require_once('activate_error.php');
require_once('../require_daos.php');


if ($_POST['action'] == "reset_password") {
    $base = new base();
    $respuesta = array();

    $DaoUsuarios = new DaoUsuarios();
    $DaoPlanteles = new DaoPlanteles();
    
    $query = "SELECT * FROM usuarios_ulm WHERE Recover_usu='" . $_POST['key'] . "'";
    $usuario = $DaoUsuarios->existQuery($query);
    if ($usuario->getId() > 0) {
        $usuario->setPass_usu($base->hashPassword($_POST['pass']));
        $usuario->setRecover_usu(NULL);
        $DaoUsuarios->update($usuario);

        $email = $usuario->getEmail_usu();
        $nombre = $usuario->getNombre_usu() . " " . $usuario->getApellidoP_usu() . " " . $usuario->getApellidoM_usu();
        $plantel = $DaoPlanteles->show($usuario->getId_plantel());

        $direccion = "";
        $DaoDirecciones = new DaoDirecciones();
        if ($plantel->getId_dir_plantel() > 0) {
            $dir = $DaoDirecciones->show($plantel->getId_dir_plantel());
            $direccion = $dir->getCalle_dir() . ' No. ' . $dir->getNumExt_dir() . ' Col. ' . $dir->getColonia_dir() . ' ' . $dir->getCiudad_dir() . ', ' . $dir->getEstado_dir();
        }

        $imagen = "";
        if (strlen($plantel->getId_img_logo()) > 0) {
            $imagen = 'src="http://www.' . $plantel->getDominio() . '/admin_ce/files/' . $plantel->getId_img_logo() . '.jpg"';
        }

        //Enviar email
        $texto_html = ' <html>
	      <head></head>
	      <body style="background-color: #F8F8F8;padding-top: 40px;" bgcolor="#F8F8F8">
	                      <div id="container" style="width: 600px; background-color: white; font-family: arial; font-size: 13px; margin: auto;">
	                           <div id="boxtext" style="width: 540px; padding: 30px;min-height: 300px;">
	                           <img ' . $imagen . ' >
	                           <h1 style="font-size: 25px; font-family: arial; color: #606060; font-weight: 100; margin-top: 20px; margin-bottom: 40px;">Contraseña actualizada</h1>
	                               <p style="margin-bottom: 20px;">Tu contraseña para el sitio ' . $plantel->getDominio() . ', fue actualizada, este correo es sólo para que estés notificado del cambio realizado, 
                                    no necesitas realizar ninguna acción.</p>
						           <p style="margin-bottom: 20px;">Si tu no has realizado el cambio en tu contraseña, por favor ponte en contacto con nosotros.</p>
	
	                           </div>
	                          <div id="footer" style="background-color: #606060; color: white; font-size: 13px; width: 540px; padding: 30px;padding-top: 20px;padding-bottom: 20px;">
	                       <table>
	                                <tr>
	                                       <td style="vertical-align: top; padding-right: 5px;" valign="top">
	                                              <div id="text_footer" style="width: 226px; font-size: 13px; color: white; font-family: arial;">
	                                                      <p style="margin-bottom: 10px;">Tel&eacute;fonos:<br> ' . $plantel->getTel() . ' </p>
	                                                      <p style="margin-bottom: 10px;">Domicilio: <br>' . $direccion . '</p>
	                                               </div>
	                                       </td>
	                               </tr></table>
	      </div>
	                    </div>
	                     <p class="footer" style="font-family: arial; font-size: 10px; color: black; width: 600px; margin: 5px auto auto;">' . date('Y') . ' ' . $plantel->getNombre_plantel() . '</p>
	              </body>
	      </html>';

        $texto_txt = '
********************************
Contraseña actualizada
********************************

**********************
Mensaje
**********************

Tu contraseña para el sitio de ' . $plantel->getDominio() . ', fue actualizada, este correo es sólo para que estés notificado 
del cambio realizado, no necesitas realizar ninguna acción.

Si tu no has realizado el cambio en tu contraseña, por favor ponte en contacto con nosotros.



Teléfonos:  ' . $plantel->getTel() . ' 
Domicilio:  ' . $direccion . '

' . date('Y') . ' ' . $plantel->getNombre_plantel();

        $arrayData = array();
        $arrayData['Asunto'] = 'Contraseña actualizada';
        $arrayData['Html'] = $texto_html;
        $arrayData['Texto'] = $texto_txt;
        $arrayData['Id_plantel'] = $plantel->getId();

        $arrayData['Destinatarios'] = array();

        $Data['email'] = $email;
        $Data['name'] = $nombre;
        array_push($arrayData['Destinatarios'], $Data);

        echo $base->send_email($arrayData);
    }else{
        $respuesta['status']="error";
        echo json_encode($respuesta);
    }
}



