<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="mostrar_box_actividad"){
    $DaoListaActividades= new DaoListaActividades();
    
    $nombre="";
    $descripcion="";
    if($_POST['Id_actividad']>0){
       $actividad= $DaoListaActividades->show($_POST['Id_actividad']);
       $nombre=$actividad->getNombre();
       $descripcion=$actividad->getComentarios();
    }
?>
<div id="box_emergente" class="actividades">
	<h1>Actividad</h1>
        <p>Nombre de la actividad<br><input type="text" value="<?php echo $nombre?>" id="nombre"/></p>
        <p>Descripci&oacute;n<br><textarea id="comentarios"><?php echo $descripcion?></textarea></p>
        <p>
            <button onclick="save_actividad(<?php echo $_POST['Id_actividad']?>)">Guardar</button>
            <button onclick="ocultar_error_layer()">Cancelar</button>
        </p>
</div>
<?php	
}


if($_POST['action']=="save_actividad"){
    $DaoListaActividades= new DaoListaActividades();
    $DaoUsuarios= new DaoUsuarios();
    if($_POST['Id_actividad']>0){
        $actividad= $DaoListaActividades->show($_POST['Id_actividad']);
        $actividad->setNombre($_POST['Nombre']);
        $actividad->setComentarios($_POST['Comentarios']);
        $DaoListaActividades->update($actividad);
    }else{
        $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        $ListaActividades= new ListaActividades();
        $ListaActividades->setNombre($_POST['Nombre']);
        $ListaActividades->setComentarios($_POST['Comentarios']);
        $ListaActividades->setDateCreated(date('Y-m-d H:i:s'));
        $ListaActividades->setId_usu($_COOKIE['admin/Id_usu']);
        $ListaActividades->setId_plantel($usu->getId_plantel());
        $ListaActividades->setOrder($DaoListaActividades->nextOrder());
        $DaoListaActividades->add($ListaActividades);
    }
    actividadesAbiertas();
}


if($_POST['action']=="delete_actividad"){
    $DaoListaActividades= new DaoListaActividades();
    $DaoListaActividades->delete($_POST['Id_actividad']);
 
    $DaoTareasActividad= new DaoTareasActividad();
    foreach($DaoTareasActividad->getTareasActividad($_POST['Id_actividad']) as $k=>$v){
        $DaoTareasActividad->delete($v->getId());
    }
    actividadesAbiertas();
}




if($_POST['action']=="saveOrdenTareas"){
	$count=1;
        $DaoTareasActividad= new DaoTareasActividad();
	foreach($_POST['Tareas'] as $k => $v){
            $tarea=$DaoTareasActividad->show($v['id_tarea']);
            $tarea->setOrder($count);
            $DaoTareasActividad->update($tarea);
	   $count++;
	}
	exit();
}

if($_POST['action']=="saveOrdenActividades"){
	$count=1;
        $DaoListaActividades= new DaoListaActividades();
	foreach($_POST['Actividades'] as $k => $v){
            $act=$DaoListaActividades->show($v['id_act']);
            $act->setOrder($count);
            $DaoListaActividades->update($act);
	   $count++;
	}
	exit();
}



if($_POST['action']=="mostrar_box_tarea"){
    $DaoTareasActividad= new DaoTareasActividad();
    $DaoUsuarios= new DaoUsuarios();
    $DaoDependencias= new DaoDependencias();
    $nombre="";
    $fechaIni="";
    $fechFin="";
    $descripcion="";
    $prioridad="";
    $Id_usu="";
    
    if($_POST['Id_tarea']>0){
       $tarea= $DaoTareasActividad->show($_POST['Id_tarea']);
       $usuario=$DaoUsuarios->show($tarea->getId_usuRespon());
       $nombre=$usuario->getNombre_usu()." ".$usuario->getApellidoP_usu()." ".$usuario->getApellidoM_usu();
       $descripcion=$tarea->getComentario();
       $fechaIni=$tarea->getStartDate();
       $fechFin=$tarea->getEndDate();
       $prioridad=$tarea->getPrioridad();
       $Id_usu=$tarea->getId_usuRespon();
    }
?>
<div id="box_emergente" class="box-tareas-emergente">        
  <ul>
    <li><a href="#tabs-1">General</a></li>
    <li><a href="#tabs-2">Dependencias <i class="fa fa-level-up"></i></a></li>
  </ul>
  <div id="tabs-1">
        <div class="box-usu">
            <p>Usuario asignado<br><input type="text" value="<?php echo $nombre?>" id="nombre-usu" onkeyup="buscarUsuario()" id-usu="<?php echo $Id_usu;?>"/></p>
            <ul id="list-usuarios"></ul>
        </div>
        <p>Fecha de inicio<br><input type="date" value="<?php echo $fechaIni?>" id="fechaIni"/></p>
        <p>Fecha de fin<br><input type="date" value="<?php echo $fechFin?>" id="fechaFin"/></p>
        <p>Prioridad<br>
            <select id="priodidad">
                <option value=""></option>
                <option value="1" <?php if($prioridad=="ninguna"){ ?> selected="selected" <?php } ?>>ninguna</option>
                <option value="2" <?php if($prioridad=="baja"){ ?> selected="selected" <?php } ?>>baja</option>
                <option value="3" <?php if($prioridad=="media"){ ?> selected="selected" <?php } ?>>media</option>
                <option value="4" <?php if($prioridad=="alta"){ ?> selected="selected" <?php } ?>>alta</option>
            </select>
        </p>
        <p>Descripci&oacute;n<br><textarea id="comentarios"><?php echo $descripcion?></textarea></p>
  </div>
  <div id="tabs-2">
      <table>
          <tr>
              <td>
                  <p>Actividades</p>
                  <ul id="act-tab">
                      <?php
                        $Id_usuCookie=$_COOKIE['admin/Id_usu'];
                        $TipoUsu=$DaoUsuarios->show($Id_usuCookie);
                        $DaoListaActividades= new DaoListaActividades();
                        foreach($DaoListaActividades->getActividadesAbiertas() as $k=>$v){ 
                            $usuario=$DaoUsuarios->show($v->getId_usu());
                            $count=1;
                            $Id_usuAux=0;
                            foreach($DaoTareasActividad->getTareasActividad($v->getId()) as $k2=>$v2){
                                if(($v2->getId_usuRespon()==$Id_usuCookie) || ($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                                    $count++; 
                                    $Id_usuAux=1;
                                }
                            }
                            if($Id_usuAux==1){
                        ?>
                              <li onclick="getTareas(<?php echo $v->getId()?>,this)"><i class="fa fa-list-ul"></i> <?php echo $v->getNombre();?> <span>(<?php echo $count?>)</span></li>
                       <?php
                            }
                       }
                       ?>   
                            
                  </ul>
              </td>
              <td>
                  <p>Tareas</p>
                      <?php
                        $TipoUsu=$DaoUsuarios->show($Id_usuCookie);
                        $DaoListaActividades= new DaoListaActividades();
                        foreach($DaoListaActividades->getActividadesAbiertas() as $k=>$v){ 
                            $usuario=$DaoUsuarios->show($v->getId_usu());
                            ?>
                             <ul class="tareas-tab" list-act="<?php echo $v->getId()?>">
                            <?php
                            if($_POST['Id_tarea']>0){
                                foreach($DaoTareasActividad->getTareasActividad($v->getId()) as $k2=>$v2){
                                    if(($v2->getId_usuRespon()==$Id_usuCookie) || ($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                                        $depe=$DaoDependencias->showByTareadependencia($_POST['Id_tarea'],$v2->getId());
                                        if($_POST['Id_tarea']!=$v2->getId()){
                                        ?>
                                       <li><input type="checkbox" value="<?php echo $v2->getId();?>" <?php if($depe->getId()>0){?> checked="checked" <?php }?>/> <?php echo $v2->getComentario();?></li>
                                     <?php
                                        }
                                    }
                                }
                            }else{
                                
                                foreach($DaoTareasActividad->getTareasActividad($v->getId()) as $k2=>$v2){
                                    if(($v2->getId_usuRespon()==$Id_usuCookie) || ($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                                        
                                        ?>
                                        <li><input type="checkbox" value="<?php echo $v2->getId();?>" /> <?php echo $v2->getComentario();?></li>
                                     <?php
                                    }
                                }    
                            }
                            ?>
                            </ul>
                            <?php
                       }
                       ?>
              </td>
          </tr>
      </table>
  </div>   
        <p>
            <button onclick="save_tarea(<?php echo $_POST['Id_tarea']?>,<?php echo $_POST['Id_act']?>)">Guardar</button>
            <button onclick="ocultar_error_layer()">Cancelar</button>
        </p>
</div>
<?php	
}



if($_POST['action']=="buscarUsuario"){
    $base = new base();
    $DaoUsuarios = new DaoUsuarios();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query="SELECT *, CONCAT(IFNULL(Nombre_usu,''),IFNULL(ApellidoP_usu,''),IFNULL(ApellidoM_usu,'')) AS Buscar  
    FROM usuarios_ulm HAVING Buscar LIKE '%".$_POST['buscar']."%' AND Baja_usu IS NULL AND Id_plantel=".$usu->getId_plantel();	
    $consulta=$base->advanced_query($query);
    $row_consulta = $consulta->fetch_assoc();
    $totalRows_consulta= $consulta->num_rows;
    if($totalRows_consulta>0){
       do{
           ?>
          <li id-usu="<?php echo $row_consulta['Id_usu']?>"><?php echo $row_consulta['Nombre_usu']."  ".$row_consulta['ApellidoP_usu']."  ".$row_consulta['ApellidoM_usu']?></li>
        <?php
       }while($row_consulta = $consulta->fetch_assoc());  
    }
}



if($_POST['action']=="save_tarea"){
    $DaoTareasActividad= new DaoTareasActividad();
    $DaoDependencias= new DaoDependencias();
    $DaoUsuarios= new DaoUsuarios();
    $base = new base();
    $DaoPlanteles= new DaoPlanteles();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $plantel=$DaoPlanteles->show($usu->getId_plantel());
    
    if($_POST['prioridad']==1){
        $classPrioridad="ninguna"; 
     }elseif($_POST['prioridad']==2){
        $classPrioridad="baja";
     }elseif($_POST['prioridad']==3){
        $classPrioridad="media";
     }elseif($_POST['prioridad']==4){
        $classPrioridad="alta";
     }
    if($_POST['Id_tarea']>0){
        $tarea= $DaoTareasActividad->show($_POST['Id_tarea']);
        $tarea->setComentario($_POST['comentarios']);
        $tarea->setId_usuRespon($_POST['Id_usuRes']);
        $tarea->setStartDate($_POST['fechaIni']);
        $tarea->setEndDate($_POST['fechaFin']);
        $tarea->setPrioridad($classPrioridad);
        $DaoTareasActividad->update($tarea);
        $Id_tarea=$_POST['Id_tarea'];
        
        $titulo="Actualización de actividad";
        $mensaje='<p>Se ha actualizado la siguiente actividad.</p>
          <p><b>'.$_POST['comentarios'].'</b></p>
          <p>Para iniciar preciona <a href="http://www.'.$plantel->getDominio().'/admin_ce/actividades.php">aqui</a></p>';
            
    }else{
        $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        $tarea= new TareasActividad();
        $tarea->setDateCreated(date('Y-m-d H:i:s'));
        $tarea->setId_act($_POST['Id_act']);
        $tarea->setStatus(0);
        $tarea->setId_usuCreated($_COOKIE['admin/Id_usu']);
        $tarea->setOrder($DaoTareasActividad->nextOrder());
        $tarea->setComentario($_POST['comentarios']);
        $tarea->setId_usuRespon($_POST['Id_usuRes']);
        $tarea->setStartDate($_POST['fechaIni']);
        $tarea->setEndDate($_POST['fechaFin']);
        $tarea->setPrioridad($classPrioridad);
        $Id_tarea=$DaoTareasActividad->add($tarea);
        
        $titulo="Actividad nueva";
        $mensaje='<p>Tienes una nueva actividad asignada.</p>
          <p><b>'.$_POST['comentarios'].'</b></p>
          <p>Para iniciar preciona <a href="http://www.'.$plantel->getDominio().'/admin_ce/actividades.php">aqui</a></p>';
    }
    
    foreach($DaoDependencias->getDependenciasTarea($Id_tarea) as $k=>$v){
        $DaoDependencias->delete($v->getId());
    }
    
    foreach($_POST['dependencias'] as $v){
        $Dependencias= new Dependencias();
        $Dependencias->setId_tarea($Id_tarea);
        $Dependencias->setId_depe($v);
        $DaoDependencias->add($Dependencias);
    }
    
    $arrayTo=array();
    $arrayTo['Asunto']=$titulo;
    $arrayTo['Mensaje']=$mensaje;
    $arrayTo['Destinatarios']=array();
    
    $resp=$DaoUsuarios->show($_POST['Id_usuRes']);

    $Data= array();
    $Data['email']= $resp->getEmail_usu();
    $Data['name']= $resp->getNombre_usu()." ".$resp->getApellidoP_usu();
    array_push($arrayTo['Destinatarios'], $Data);
    $base->send_email($arrayTo);
  
}


if($_POST['action']=="getTareas"){
    $DaoListaActividades= new DaoListaActividades();
    $DaoTareasActividad= new DaoTareasActividad();
    $DaoUsuarios= new DaoUsuarios();

    $Id_usuCookie=$_COOKIE['admin/Id_usu'];
    $TipoUsu=$DaoUsuarios->show($Id_usuCookie);
    foreach($DaoTareasActividad->getTareasActividad($_POST['Id_act']) as $k=>$v){ 
        $Actividad=$DaoListaActividades->show($v->getId_act());
        if(($v->getId_usuRespon()==$Id_usuCookie) || ($Actividad->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
           ?>
                  <li><input type="checkbox" value="<?php echo $v->getId();?>"/> <?php echo $v->getComentario();?></li>

           <?php
        }
    } 
}


if($_POST['action']=="getTextoTooltip"){
    $DaoDependencias= new DaoDependencias();
    ?>
      <div class="xxxx">
          <h2>No puede ser iniciada</h2>
          <p>Necesita completar las siguientes tareas:</p>
          <ul id="list-tooltip-eme">
              <?php
              if($_POST['Id_tarea']>0){
                 $DaoTareasActividad= new DaoTareasActividad();
                  foreach($DaoDependencias->getDependenciasTarea($_POST['Id_tarea']) as $x=>$y){
                     $tarea=$DaoTareasActividad->show($y->getId_depe());
                     if($tarea->getStatus()==0){
                         ?>
                          <li><i class="fa fa-level-down"></i> <?php echo $tarea->getComentario()?></li>
              <?php
                     }   
                    }
              }
              ?>
          </ul>
      </div>
   <?php
}




function actividadesAbiertas(){
    $DaoListaActividades= new DaoListaActividades();
    $DaoUsuarios= new DaoUsuarios();
    $DaoTareasActividad= new DaoTareasActividad();
    $DaoDependencias= new DaoDependencias();
    $base= new base();
    
    $Id_usuCookie=$_COOKIE['admin/Id_usu'];
    $TipoUsu=$DaoUsuarios->show($Id_usuCookie);

    $UsuariosPermitidos= array();
    $query="SELECT * FROM usuarios_ulm WHERE Tipo_usu=1";
    foreach($DaoUsuarios->advanced_query($query) as $k=>$v){
        array_push($UsuariosPermitidos, $v->getId());
    }

    $count=1;
    foreach($DaoListaActividades->getActividadesAbiertas() as $k=>$v){ 
        $usuario=$DaoUsuarios->show($v->getId_usu());
        $Id_usuAux=0;
        $r=0;
        //Visualiza solo las actividades en las que esta relacionado
        foreach($DaoTareasActividad->getTareasActividad($v->getId()) as $k2=>$v2){
            if(($v2->getId_usuRespon()==$Id_usuCookie) || ($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                $Id_usuAux=1;
            }
            $r++;
        }
        if($Id_usuAux==1 || $r==0){
    ?>
        <li id-act="<?php echo $v->getId()?>" class="actividad-li">
            <table class="table">
                <thead>
                <tr>
                  <th>Actividad</th>
                  <th>Fecha Creada</th>
                  <th>Usuario</th>
                  <th>Opciones</th>
                </tr>
              </thead>
                <tbody>
                     <tr>
                        <td style="width: 21.875em;"><?php echo $v->getNombre();?></td>
                         <td><?php echo $v->getDateCreated()?></td>
                         <td><?php echo $usuario->getNombre_usu()." ".$usuario->getApellidoP_usu()?></td>
                         <td style="width: 6.250em;text-align: right;"> 
                            <?php
                             if(($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                            ?>
                               <i class="fa fa-pencil-square-o" onclick="mostrar_box_actividad(<?php echo $v->getId()?>)"></i>
                               <i class="fa fa-arrows"></i>
                               <i class="fa fa-times" onclick="delete_actividad(<?php echo $v->getId()?>)"></i>
                            <?php
                             }
                            ?>
                               <i class="fa fa-minus-square-o" onclick="mostrar_actividad(this)"></i>
                         </td>
                     </tr>
                </tbody>
            </table>

            <div class="box-tareas">
                <div class="comentario-act"><?php echo $v->getComentarios()?></div>
                <ul class="tareas">

                    <?php
                     foreach($DaoTareasActividad->getTareasActividad($v->getId()) as $k2=>$v2){
                         $usuario=$DaoUsuarios->show($v2->getId_usuRespon());
                         $classPrioridad="";
                        $textoPrioridad="";
                        if($v2->getPrioridad()=="alta"){
                           $classPrioridad="prioridad-red"; 
                           $textoPrioridad="Prioridad alta";
                        }elseif($v2->getPrioridad()=="media"){
                           $classPrioridad="prioridad-yellow";
                           $textoPrioridad="Prioridad media";
                        }elseif($v2->getPrioridad()=="baja"){
                           $classPrioridad="prioridad-green";
                           $textoPrioridad="Prioridad baja";
                        }elseif($v2->getPrioridad()=="ninguna"){
                           $classPrioridad="prioridad-none";
                           $textoPrioridad="Sin prioridad";
                        }

                         $fecha="";
                         if($v2->getStartDate()!=null && $v2->getEndDate()==null){
                            $fecha="(Inicia el ".$base->formatFecha($v2->getStartDate()).")";
                         }
                         if($v2->getStartDate()==null && $v2->getEndDate()!=null){
                            $fecha="(Termina el ".$base->formatFecha($v2->getEndDate()).")";
                         }

                         if($v2->getStartDate()!=null && $v2->getEndDate()!=null){
                            $fecha="(Inicia el ".$base->formatFecha($v2->getStartDate()).", termina el ".$base->formatFecha($v2->getEndDate()).")";
                         }
                         //Puede visializar solo:
                         //El usuario que creo la actividad
                         //El usuario al que le asignaron la actividad
                         //El que sea administrador 
                         //if(($v2->getId_usuRespon()==$Id_usuCookie) || ($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                             $dependencias=$DaoDependencias->getDependenciasRealizadas($v2->getId());
                             $totalDependencias=count($DaoDependencias->getDependenciasTarea($v2->getId()));
                        ?>
                        <li id-tarea="<?php echo $v2->getId()?>" id-act="<?php echo  $v->getId()?>">
                            <table class="table">
                                <tbody>
                                     <tr>
                                         <td style="width: 85px;padding-right: 0px">
                                            <?php
                                            $class="hidden-i";
                                            if(($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                                              $class="visible-i";
                                            }
                                            ?>
                                            <div class="box-i <?php echo $class;?>">
                                               <i class="fa fa-arrows"></i>
                                               <i class="fa fa-pencil-square-o" onclick="mostrar_box_tarea(<?php echo $v2->getId()?>,this,<?php echo $v->getId()?>)"></i>
                                               <i class="fa fa-times" onclick="delete_tarea(<?php echo $v2->getId()?>)"></i>
                                            </div>
                                             <div class="box-input" <?php if($dependencias==0 && $totalDependencias>0){?> title="<?php echo $textTitle?>" <?php }?> value="<?php echo $v2->getId()?>">
                                                 <input type="checkbox" <?php if($v2->getStatus()==1){ ?> checked="checked" <?php }?> 
                                                     <?php 
                                                     //El usuario puede visualizar todas las tareas aunque no sea el usuario asignado
                                                     //Pero solo podra dar click en las que este asignado
                                                     if(($dependencias==0 && $totalDependencias>0) || (($v2->getId_usuRespon()!=$Id_usuCookie &&  $v->getId_usu()!=$Id_usuCookie) && (!in_array($Id_usuCookie, $UsuariosPermitidos)))){
                                                     ?> 
                                                        disabled="disabled" 
                                                     <?php 
                                                     }
                                                     ?> 
                                                        onchange="tareaStatus(this)" 
                                                        data-id="<?php echo $v2->getId()?>"
                                                   >
                                             </div>
                                         </td>
                                         <td><div class="asignado"><?php echo $usuario->getNombre_usu()." ".$usuario->getApellidoP_usu()?></div>
                                             <div class="comentarios-tarea"><?php echo $v2->getComentario()?></div>
                                             <div class="mas">
                                                 <span onclick="mostrar_comentario(this)"> <?php echo strtolower("m&aacute;s")?> </span> 
                                                 <i class="fa fa-exclamation-circle <?php echo $classPrioridad?>" title="<?php echo $textoPrioridad;?>"></i>
                                                 <span class="fechas"><?php echo $fecha;?></span>
                                             </div>
                                         </td>
                                     </tr>

                                </tbody>
                            </table> 
                            <div class="comentarios-tarea-completo">
                                <p><?php echo $v2->getComentario()?></p>
                                <?php
                                  if(strlen($v2->getComentario_realizo())>0){
                                ?>
                                <br>
                                <p><b>¿C&oacute;mo se completo?</b></p> 
                                <p><?php echo $v2->getComentario_realizo()?></p>
                                <?php
                                  }
                                ?>
                            </div>
                        </li>
                    <?php
                         //}
                    }
                    if(($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                    ?>
                        <li><span class="button-tarea" onclick="mostrar_box_tarea(0,this,<?php echo $v->getId()?>)"><i class="fa fa-plus"></i> Añadir tarea</span></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </li>
   <?php
        }
   $count++; 
   }
}


function actividadesCerradas(){

    $DaoListaActividades= new DaoListaActividades();
    $DaoUsuarios= new DaoUsuarios();
    $DaoTareasActividad= new DaoTareasActividad();
    $DaoDependencias= new DaoDependencias();
    $base= new base();
    
    $Id_usuCookie=$_COOKIE['admin/Id_usu'];
    $TipoUsu=$DaoUsuarios->show($Id_usuCookie);

    $UsuariosPermitidos= array();
    $query="SELECT * FROM usuarios_ulm WHERE Tipo_usu=1";
    foreach($DaoUsuarios->advanced_query($query) as $k=>$v){
        array_push($UsuariosPermitidos, $v->getId());
    }

    $count=1;
    foreach($DaoListaActividades->getActividadesCerradas() as $k=>$v){ 
        $usuario=$DaoUsuarios->show($v->getId_usu());
        $Id_usuAux=0;
        $r=0;
        //Visualiza solo las actividades en las que esta relacionado
        foreach($DaoTareasActividad->getTareasActividad($v->getId()) as $k2=>$v2){
            if(($v2->getId_usuRespon()==$Id_usuCookie) || ($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                $Id_usuAux=1;
            }
            $r++;
        }
        if($Id_usuAux==1 || $r==0){
    ?>
        <li id-act="<?php echo $v->getId()?>" class="actividad-li">
            <table class="table">
                <thead>
                <tr>
                  <th>Actividad</th>
                  <th>Fecha Creada</th>
                  <th>Usuario</th>
                  <th>Opciones</th>
                </tr>
              </thead>
                <tbody>
                     <tr>
                        <td style="width: 21.875em;"><?php echo $v->getNombre();?></td>
                         <td><?php echo $v->getDateCreated()?></td>
                         <td><?php echo $usuario->getNombre_usu()." ".$usuario->getApellidoP_usu()?></td>
                         <td style="width: 6.250em;text-align: right;"> 
                            <?php
                             if(($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                            ?>
                               <i class="fa fa-pencil-square-o" onclick="mostrar_box_actividad(<?php echo $v->getId()?>)"></i>
                               <i class="fa fa-arrows"></i>
                               <i class="fa fa-times" onclick="delete_actividad(<?php echo $v->getId()?>)"></i>
                            <?php
                             }
                            ?>
                               <i class="fa fa-minus-square-o" onclick="mostrar_actividad(this)"></i>
                         </td>
                     </tr>
                </tbody>
            </table>

            <div class="box-tareas">
                <div class="comentario-act"><?php echo $v->getComentarios()?></div>
                <ul class="tareas">

                    <?php
                     foreach($DaoTareasActividad->getTareasActividad($v->getId()) as $k2=>$v2){
                         $usuario=$DaoUsuarios->show($v2->getId_usuRespon());
                         $classPrioridad="";
                        $textoPrioridad="";
                        if($v2->getPrioridad()=="alta"){
                           $classPrioridad="prioridad-red"; 
                           $textoPrioridad="Prioridad alta";
                        }elseif($v2->getPrioridad()=="media"){
                           $classPrioridad="prioridad-yellow";
                           $textoPrioridad="Prioridad media";
                        }elseif($v2->getPrioridad()=="baja"){
                           $classPrioridad="prioridad-green";
                           $textoPrioridad="Prioridad baja";
                        }elseif($v2->getPrioridad()=="ninguna"){
                           $classPrioridad="prioridad-none";
                           $textoPrioridad="Sin prioridad";
                        }

                         $fecha="";
                         if($v2->getStartDate()!=null && $v2->getEndDate()==null){
                            $fecha="(Inicia el ".$base->formatFecha($v2->getStartDate()).")";
                         }
                         if($v2->getStartDate()==null && $v2->getEndDate()!=null){
                            $fecha="(Termina el ".$base->formatFecha($v2->getEndDate()).")";
                         }

                         if($v2->getStartDate()!=null && $v2->getEndDate()!=null){
                            $fecha="(Inicia el ".$base->formatFecha($v2->getStartDate()).", termina el ".$base->formatFecha($v2->getEndDate()).")";
                         }
                         //Puede visializar solo:
                         //El usuario que creo la actividad
                         //El usuario al que le asignaron la actividad
                         //El que sea administrador 
                         //if(($v2->getId_usuRespon()==$Id_usuCookie) || ($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                             $dependencias=$DaoDependencias->getDependenciasRealizadas($v2->getId());
                             $totalDependencias=count($DaoDependencias->getDependenciasTarea($v2->getId()));
                        ?>
                        <li id-tarea="<?php echo $v2->getId()?>" id-act="<?php echo  $v->getId()?>">
                            <table class="table">
                                <tbody>
                                     <tr>
                                         <td style="width: 85px;padding-right: 0px">
                                            <?php
                                            $class="hidden-i";
                                            if(($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                                              $class="visible-i";
                                            }
                                            ?>
                                            <div class="box-i <?php echo $class;?>">
                                               <i class="fa fa-arrows"></i>
                                               <i class="fa fa-pencil-square-o" onclick="mostrar_box_tarea(<?php echo $v2->getId()?>,this,<?php echo $v->getId()?>)"></i>
                                               <i class="fa fa-times" onclick="delete_tarea(<?php echo $v2->getId()?>)"></i>
                                            </div>
                                             <div class="box-input" <?php if($dependencias==0 && $totalDependencias>0){?> title="<?php echo $textTitle?>" <?php }?> value="<?php echo $v2->getId()?>">
                                                 <input type="checkbox" <?php if($v2->getStatus()==1){ ?> checked="checked" <?php }?> 
                                                     <?php 
                                                     //El usuario puede visualizar todas las tareas aunque no sea el usuario asignado
                                                     //Pero solo podra dar click en las que este asignado
                                                     if(($dependencias==0 && $totalDependencias>0) || (($v2->getId_usuRespon()!=$Id_usuCookie &&  $v->getId_usu()!=$Id_usuCookie) && (!in_array($Id_usuCookie, $UsuariosPermitidos)))){
                                                     ?> 
                                                        disabled="disabled" 
                                                     <?php 
                                                     }
                                                     ?> 
                                                        onchange="tareaStatus(this)" 
                                                        data-id="<?php echo $v2->getId()?>"
                                                   >
                                             </div>
                                         </td>
                                         <td><div class="asignado"><?php echo $usuario->getNombre_usu()." ".$usuario->getApellidoP_usu()?></div>
                                             <div class="comentarios-tarea"><?php echo $v2->getComentario()?></div>
                                             <div class="mas">
                                                 <span onclick="mostrar_comentario(this)"> <?php echo strtolower("m&aacute;s")?> </span> 
                                                 <i class="fa fa-exclamation-circle <?php echo $classPrioridad?>" title="<?php echo $textoPrioridad;?>"></i>
                                                 <span class="fechas"><?php echo $fecha;?></span>
                                             </div>
                                         </td>
                                     </tr>

                                </tbody>
                            </table> 
                            <div class="comentarios-tarea-completo">
                                <p><?php echo $v2->getComentario()?></p>
                                <?php
                                  if(strlen($v2->getComentario_realizo())>0){
                                ?>
                                <br>
                                <p><b>¿C&oacute;mo se completo?</b></p> 
                                <p><?php echo $v2->getComentario_realizo()?></p>
                                <?php
                                  }
                                ?>
                            </div>
                        </li>
                    <?php
                         //}
                    }
                    if(($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                    ?>
                        <li><span class="button-tarea" onclick="mostrar_box_tarea(0,this,<?php echo $v->getId()?>)"><i class="fa fa-plus"></i> Añadir tarea</span></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </li>
   <?php
        }
   $count++; 
   }
}


if($_POST['action']=="mostrar_tareas_abiertas"){
    actividadesAbiertas();
}


if($_POST['action']=="mostrar_tareas_cerradas"){
    actividadesCerradas();
}

if($_POST['action']=="tareaStatus"){
    $DaoTareasActividad= new DaoTareasActividad();
    $DaoUsuarios= new DaoUsuarios();
    $base = new base();
    $DaoPlanteles= new DaoPlanteles();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $plantel=$DaoPlanteles->show($usu->getId_plantel());
    
    $Id=NULL;
    $Comentario=NULL;
    if($_POST['status']==1){
       $Id=$_COOKIE['admin/Id_usu'];
       $Comentario=$_POST['comentario'];
       
       //Se notificara via email a los usuarios que dependan de esta actividad 
       //para inicar su tarea
       $DaoDependencias= new DaoDependencias();
       foreach ($DaoDependencias->getTareasDependencia($_POST['id']) as $k=>$v){
                $tar=$DaoTareasActividad->show($v->getId_tarea());
                
                $base= new base();
                $arrayTo=array();
                
                $titulo="Actividad desbloqueada";
                $mensaje='<p>Ahora puedes inicar la siguiente actividad:</p>
                  <p><b>'.$tar->getComentario().'</b></p>
                  <p>Para iniciar preciona <a href="http://www.'.$plantel->getDominio().'/admin_ce/actividades.php">aqui</a></p>';
        
                $arrayTo['Asunto']=$titulo;
                $arrayTo['Mensaje']=$mensaje;
                $arrayTo['Destinatarios']=array();

                $resp=$DaoUsuarios->show($tar->getId_usuRespon());

                $Data= array();
                $Data['email']= $resp->getEmail_usu();
                $Data['name']= $resp->getNombre_usu()." ".$resp->getApellidoP_usu();
                array_push($arrayTo['Destinatarios'], $Data);
                $base->send_email($arrayTo);
       }
    }
    
   
   $tarea=$DaoTareasActividad->show($_POST['id']);
   $tarea->setStatus($_POST['status']);
   $tarea->setComentario_realizo($Comentario);
   $tarea->setId_usu_realizo($Id);
   $DaoTareasActividad->update($tarea);
   actividadesAbiertas();
   
}



if($_POST['action']=="delete_tarea"){
    $DaoTareasActividad= new DaoTareasActividad();
    $DaoTareasActividad->delete($_POST['Id_tarea']);
 
    $DaoDependencias= new DaoDependencias();
    foreach($DaoDependencias->getDependenciasTarea($_POST['Id_tarea']) as $k=>$v){
        $DaoDependencias->delete($v->getId());
    }
    actividadesAbiertas();
}




if($_POST['action']=="getActividadesAbiertas"){
    actividadesAbiertas();
}



if($_POST['action']=="updateIconTareas"){
  $DaoTareasActividad= new DaoTareasActividad();
  $z=count($DaoTareasActividad->getTareasActividadUserFaltantes($_COOKIE['admin/Id_usu']));
    if($z>0){
        ?>

            <i class="fa fa-bell" id="icon-lista-de-tareas"></i>
            <a href="actividades.php">
                <div id="box-count-tareas">
                     <?php echo $z?> 
                </div>
           </a>
   <?php
    }
}



