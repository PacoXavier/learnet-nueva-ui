<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 
require_once('../barcodegen/class/BCGFontFile.php');
require_once('../barcodegen/class/BCGColor.php');
require_once('../barcodegen/class/BCGDrawing.php');
require_once('../barcodegen/class/BCGcode39.barcode.php');


if($_POST['action']=="generar_credencial"){
    //Las imagenes deben de ser de 300 X 300 px
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();

    $alumn=$DaoAlumnos->show($_POST['Id_alum']);
    $oferta = $DaoOfertas->show($_POST['Id_ofe']);
    
    $ofertaAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $esp=$DaoEspecialidades->show($ofertaAlumno->getId_esp());
    $curp=$DaoAlumnos->show($_POST['Id_alum']);

    if(file_exists("../files/".$alumn->getImg_alum().".jpg")){
        //+++++++++++++++++++++++++++++++++++++++++++++++
          //Parte del frente de la credencial
        //++++++++++++++++++++++++++++++++++++++++++++++++

        // Crear instancias de imágenes
        //$destino = imagecreatefromjpeg("../estandares/Credenciales/Id_frente.jpg");

        //Carrera Tecnica
        if($esp->getId()==132 || $esp->getId()==133 || $esp->getId()==134 || $esp->getId()==135 || $esp->getId()==136){
           $destino = imagecreatefromjpeg("../estandares/Credenciales/Id_fct.jpg");
        }else{
        //Lic
           $destino = imagecreatefromjpeg("../estandares/Credenciales/Id_f.jpg"); 
        }
        $origen = imagecreatefromjpeg("../files/".$alumn->getImg_alum().".jpg");

        //Colocar color al texto
        $color_texto = imagecolorallocate($destino, 255, 255, 255);

        //ruta del archivo ttf
        $fuente = "../estandares/Fonts/RobotoCondensed-Regular.ttf"; 

        
        //Definimos el tamaño de la fuente
        $tamanoFuente=35;
        $Texto=mb_strtoupper($alumn->getNombre()." ".$alumn->getApellidoP()." ".$alumn->getApellidoM(),"utf-8");

        //Centramos el texto en la imagen
        $cajatexto = imagettfbbox($tamanoFuente, 0, $fuente, mb_strtoupper($Texto,"utf-8"));
        $x = ceil((1013 - $cajatexto[2]) / 2)-3;
        
        if($x<=0){
           $tamanoFuente=22; 
           $cajatexto = imagettfbbox($tamanoFuente, 0, $fuente, mb_strtoupper($Texto,"utf-8"));
           $x = ceil((1013 - $cajatexto[2]) / 2)-3;
        }
        imagettftext($destino, $tamanoFuente, 0, $x, 500, $color_texto, $fuente, mb_strtoupper($Texto,"utf-8"));
        //ruta del archivo ttf
        $fuente = "../estandares/Fonts/RobotoCondensed-Regular.ttf"; 

        //Definimos el tamaño de la fuente
        //$tamanoFuente=35;
        $tamanoFuente=20;
        //$Texto=strtolower($oferta->getNombre_oferta());
        $Texto=strtolower($esp->getNombre_esp());
        //Centramos el texto en la imagen
        $cajatexto = imagettfbbox($tamanoFuente, 0, $fuente, mb_strtoupper($Texto,"utf-8"));
        $x = ceil((1013 - $cajatexto[2]) / 2)-3;
        imagettftext($destino, $tamanoFuente, 0, $x, 557, $color_texto, $fuente, mb_strtoupper($Texto,"utf-8"));

        //Se agrega campo de Curp y Matricula 
        $tamanoFuente=12;
        $Texto=strtolower("Curp:".$alumn->getCurp_ins()."  "."Matricula:".$alumn->getMatricula());
        $cajatexto = imagettfbbox($tamanoFuente, 0, $fuente, mb_strtoupper($Texto,"utf-8"));
        $x = ceil((1013 - $cajatexto[2]) / 2)-3;
        imagettftext($destino, $tamanoFuente, 0, $x, 600, $color_texto, $fuente, mb_strtoupper($Texto,"utf-8"));


        // Copiar y fusionar
        imagecopymerge($destino, $origen, 105, 128, 0, 0, 300, 300, 100);

        //Imprimir y liberar memoria
        //header('Content-Type: image/jpg');
        $Img_frente="credencial_".$alumn->getMatricula()."_frente.jpg";
        $im=imagejpeg($destino,$Img_frente,100);

        //Agregamos la imagen al zip
        $zip = new ZipArchive();
        $zip->open("credenciales.zip",ZipArchive::CREATE);
        $zip->addFile($Img_frente,$Img_frente);
        $zip->close();

        imagedestroy($destino);
        imagedestroy($origen);

        //+++++++++++++++++++++++++++++++++++++++++++++++
          //Parte del posterior de la credencial
        //++++++++++++++++++++++++++++++++++++++++++++++++

        //Generar imagen del codigo de barras
        // Loading Font
        $font = new BCGFontFile('../estandares/Fonts/RobotoCondensed-Regular.ttf', 25);

        // Don't forget to sanitize user inputs
        $text = $alumn->getMatricula();

        // The arguments are R, G, B for color.
        $color_black = new BCGColor(0, 0, 0);
        $color_white = new BCGColor(255, 255, 255);

        $drawException = null;
        try {
            $code = new BCGcode39();
            $code->setScale(3); // Resolution
            $code->setThickness(20); // Thickness
            $code->setForegroundColor($color_black); // Color of bars
            $code->setBackgroundColor($color_white); // Color of spaces
            $code->setFont($font); // Font (or 0)
            $code->parse($text); // Text
        } catch(Exception $exception) {
            $drawException = $exception;
        }

        //Here is the list of the arguments
        //1 - Filename (empty : display on screen)
        //2 - Background color 
        $drawing = new BCGDrawing('codigoBarras.jpg', $color_white);
        if($drawException) {
            $drawing->drawException($drawException);
        } else {
            $drawing->setBarcode($code);
            $drawing->draw();
        }
        //Header that says it is an image (remove it if you save the barcode to a file)
        //header('Content-Type: image/png');
        //header('Content-Disposition: inline; filename="barcode.png"');
        // Draw (or save) the image into PNG format.
        $drawing->finish(BCGDrawing::IMG_FORMAT_JPEG);
        //Carrera Tecnica
        if($esp->getId()==132 || $esp->getId()==133 || $esp->getId()==134 || $esp->getId()==135 || $esp->getId()==136){
           $destino = imagecreatefromjpeg("../estandares/Credenciales/Id_ct.jpg");
        }else{
            //Lic
           $destino = imagecreatefromjpeg("../estandares/Credenciales/Id_p1.jpg"); 
        }
        $origen = imagecreatefromjpeg("codigoBarras.jpg");

        //Colocar color al texto
        $color_texto = imagecolorallocate($destino, 0, 0, 0);

        //ruta del archivo ttf
        $fuente = "../estandares/Fonts/RobotoCondensed-Regular.ttf"; 

        //Definimos el tamaño de la fuente
        $tamanoFuente=25;
        
       //Actualizamos la fecha de la ultima actualizacion
       $hoy=date('Y-m-d');
       $ofertaAlumno->setUltimaGeneracion_credencial($hoy);
       $DaoOfertasAlumno->update($ofertaAlumno);
        
        $Vigencia=date("d/m/Y",mktime(0, 0, 0, date('m'), date('d'), date('Y')+1));  
        
        $Texto="Vigencia: ".$Vigencia;
        imagettftext($destino, $tamanoFuente, 0, 110, 560, $color_texto, $fuente, ucwords($Texto));

        //Obtenemos el tamanio del codigo de barras
        $picsize=getimagesize("codigoBarras.jpg");

        $source_x  = $picsize[0]*1;
        $source_y  = $picsize[1]*1;

        // Copiar y fusionar
        imagecopymerge($destino, $origen, 620, 515, 0, 0, $source_x, $source_y, 100);

        //Imprimir y liberar memoria
        //header('Content-Type: image/jpg');
        $Img_atras="credencial_".$alumn->getMatricula()."_atras.jpg";
        $im=imagejpeg($destino,$Img_atras,100);

        //Agregamos la imagen al zip
        $zip = new ZipArchive();
        $zip->open("credenciales.zip",ZipArchive::CREATE);
        $zip->addFile($Img_atras,$Img_atras);
        $zip->close();

        imagedestroy($destino);
        imagedestroy($origen);

        //Borramos las imagenes creadas
        unlink("codigoBarras.jpg");
        unlink($Img_frente);
        unlink($Img_atras);
        
        $vigencia=date("Y-m-d",mktime(0, 0, 0, date('m'), date('d'), date('Y')+1));  
        $hoy=date("Y-m-d");
        $style="text-align:center;background: lightgreen;";
        $Status="Vigente";
        if($hoy>$vigencia){
            $style="text-align:center;background: pink;";
            $Status="Renovar";
        }
        $resp=array();
        $resp['UltimaRenovacion']=$hoy;
        $resp['Vigencia']=$vigencia;
        $resp['Style']=$style;
        $resp['Status']=$Status;
        echo json_encode($resp);
    }
}

if ($_POST['action'] == "mostrar_layer_generando") {
?>
<div id ="box_emergente" class="grupos">
    <h1>Credenciales generadas <br>
        <span id="alumnosCAmbiados"><span></h1>
</div>
<?php
}

if($_POST['action']=="buscarAlum"){
    $DaoAlumnos= new DaoAlumnos();
    foreach($DaoAlumnos->buscarAlumno($_POST['buscar']) as $k=>$v){
      ?>
        <li id_alum="<?php echo $v->getId();?>"><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li>
      <?php      
    }
}


if ($_POST['action'] == "update_curso") {
  ?>
  <option value="0"></option>
  <?php
  $DaoEspecialidades= new DaoEspecialidades();
  foreach ($DaoEspecialidades->getEspecialidadesOferta($_POST['Id_oferta']) as $k => $v) {
  ?>
  <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre_esp(); ?> </option>
  <?php
  }
}


if ($_POST['action'] == "update_orientacion_box_curso") {
  $DaoOrientaciones= new DaoOrientaciones();
  if(count($DaoOrientaciones->getOrientacionesEspecialidad($_POST['Id_esp']))>0){
  ?>
   <p>
  Orientaci&oacute;n<br>
  <select class="orientacion">
    <option value="0"></option>
  <?php
  foreach ($DaoOrientaciones->getOrientacionesEspecialidad($_POST['Id_esp']) as $k => $v) {
  ?>
    <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre() ?> </option>
  <?php
  }
  ?>
  </select>
  </p>
  <?php
   }
}        


if($_POST['action']=="filtro"){
    $query="";
    if($_POST['Id_alum']>0){
        $query=$query." AND Id_ins=".$_POST['Id_alum'];
    }
    if($_POST['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_POST['Id_ofe'];
    }
    if($_POST['Id_esp']>0){
        $query=$query." AND Id_esp=".$_POST['Id_esp'];
    }
    if($_POST['Id_ori']>0){
        $query=$query." AND Id_ori=".$_POST['Id_ori'];
    }

    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoUsuarios= new DaoUsuarios();
    $usu= $DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    //Se mostran alumnos duplicados si es que tienen mas dos ofertas
    $query = "SELECT * FROM inscripciones_ulm 
    JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
    WHERE tipo=1  AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()."  AND Activo_oferta=1 ".$query." AND Baja_ofe IS NULL AND IdCicloEgreso IS NULL ORDER BY Id_ins DESC";
    foreach($base->advanced_query($query) as $k=>$v){
            if(strlen($v['Img_alum'])>0 && $v['Id_ofe']!=14){
                  $alum = $DaoAlumnos->show($v['Id_ins']);

                  $Status_aux="";
                  $nombre_ori="";

                  $oferta = $DaoOfertas->show($v['Id_ofe']);
                  $esp = $DaoEspecialidades->show($v['Id_esp']);
                  if ($v['Id_ori'] > 0) {
                     $ori = $DaoOrientaciones->show($v['Id_ori']);
                     $nombre_ori = $ori->getNombre();
                   }

                   $vigencia="";
                   if(strlen($v['UltimaGeneracion_credencial'])>0){
                    $anio=date("Y",strtotime($v['UltimaGeneracion_credencial']));
                    $mes=date("m",strtotime($v['UltimaGeneracion_credencial']));
                    $dia=date("d",strtotime($v['UltimaGeneracion_credencial']));
                    $vigencia=date("Y-m-d",mktime(0, 0, 0, $mes, $dia, $anio+1));
                   }
                   $hoy=date("Y-m-d");
                   $style="background: lightgreen;";
                   $Status="Vigente";
                   if($hoy>$vigencia){
                       $style="background: pink;";
                       $Status="Renovar";
                   }

                   $Status_aux=$Status;
                   if($_POST['Status']==1){
                        $Status_aux="Vigente";
                    }elseif($_POST['Status']==2){
                        $Status_aux="Renovar";
                    }
                    if($Status==$Status_aux){
                 ?>
                         <tr id_alum="<?php echo $v['Id_ins'];?>" id_ofe_alum="<?php echo $v['Id_ofe_alum'];?>" id_ofe="<?php echo $v['Id_ofe']?>">
                           <td><?php echo $v['Matricula'] ?></td>
                           <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                           <td><?php echo $oferta->getNombre_oferta(); ?></td>
                           <td><?php echo $esp->getNombre_esp(); ?></td>
                           <td><?php echo $nombre_ori; ?></td>
                           <td style="text-align:center;"><?php echo $v['UltimaGeneracion_credencial']?></td>
                           <td style="text-align:center;"><?php echo $vigencia; ?></td>
                           <td style="text-align:center;"><input type="checkbox"> </td>
                           <td style="text-align:center;<?php echo $style?>"><span><?php echo $Status?></span></td>
                         </tr>
                         <?php
                    }
            }
    }
}

