<?php
require_once('estandares/includes.php');
require_once('require_daos.php');
$DaoAlumnos = new DaoAlumnos();
$DaoTutores = new DaoTutores();

foreach ($DaoAlumnos->showAllAlumnos() as $alum) {
    echo $alum->getId()."<br>";
    $Tutores = $DaoTutores->getTutorAlumno($alum->getId(),$alum->getNombreTutor(),$alum->getTelTutor(),$alum->getEmailTutor());
    if ($Tutores->getId() > 0) {
        $Tutores = $DaoTutores->show($Tutores->getId());
        $Tutores->setNombre($alum->getNombreTutor());
        $Tutores->setTel($alum->getTelTutor());
        $Tutores->setEmail($alum->getEmailTutor());
        $DaoTutores->update($Tutores);
    } else {
        $Tutores = new Tutores();
        $Tutores->setNombre($alum->getNombreTutor());
        $Tutores->setTel($alum->getTelTutor());
        $Tutores->setEmail($alum->getEmailTutor());
        $Tutores->setId_ins($alum->getId());
        $DaoTutores->add($Tutores);
    }
}
