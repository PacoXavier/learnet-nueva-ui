<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoAccionesInteresado.php');
require_once('clases/modelos/AccionesInteresado.php');
require_once('clases/modelos/Alumnos.php');
$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoUsuarios= new DaoUsuarios();
$DaoCiclos= new DaoCiclos();
links_head("Seguimientos | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Seguimientos</h1>
                </div>
                <div class="box-filter-reportes" style="margin-bottom: 30px">
                    <ul class="row" style="list-style: none; font-size: 20px;">
                        <li class="col-md-6 weather-grids widget-shadow" onclick="mostrar_filtro()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-filter" style="margin-right: 5px;"></i> Filtros</div></li>
                        <li class="col-md-6 weather-grids weather-right widget-shadow states-last" onclick="download_excel()"><div class="stats-right" style="width:100%; height: 100px; display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-download" style="margin-right: 5px;"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla" class="panel-body widget-shadow tables" style="margin-bottom: 15px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <td scope="row">#</td>
                                <td>Fecha</td>
                                <td>Usuario</td>
                                <td>Interesado</td>
                                <td>Comentarios</td>
                                <td>Contactar el día</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $count=1;
                                $query  = "SELECT logs_inscripciones_ulm.*,
                                usuarios_ulm.Nombre_usu,usuarios_ulm.ApellidoM_usu,usuarios_ulm.ApellidoP_usu,usuarios_ulm.Id_plantel,
                                inscripciones_ulm.Nombre_ins,inscripciones_ulm.ApellidoM_ins,inscripciones_ulm.ApellidoP_ins
                                FROM logs_inscripciones_ulm
                                JOIN usuarios_ulm ON logs_inscripciones_ulm.Id_usu=usuarios_ulm.Id_usu
                                JOIN inscripciones_ulm ON logs_inscripciones_ulm.Idrel_log=inscripciones_ulm.Id_ins
                                WHERE usuarios_ulm.Id_plantel=".$_usu->getId_plantel()." AND Tipo_relLog='inte' ORDER BY Fecha_log DESC LIMIT 100";
                                foreach($base->advanced_query($query) as $k=>$v){
                                    ?>
                                    <tr>
                                        <td scope="row"><?php echo $count?> </td>
                                        <td><?php echo $v['Fecha_log']?> </td>
                                        <td><?php echo $v['Nombre_usu']." ".$v['ApellidoP_usu']." ".$v['ApellidoM_usu'];?></td>
                                        <td><?php echo $v['Nombre_ins']." ".$v['ApellidoP_ins']." ".$v['ApellidoM_ins'];?></td>
                                        <td><?php echo $v['Comen_log'];?></td>
                                        <td><?php echo $v['Dia_contactar'];?></td>
                                        
                                    </tr>
                                    <?php
                                    $count++;
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h4 class="title">Filtros</h4>
    <div class="row">
        <div class="boxUlBuscador col-md-12">
            <p>Usuario<br><input class="form-control" type="search"  class="buscarFiltro" onkeyup="buscarUsu()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-3">
            <p>Fecha de inicio captura<br><input class="form-control" type="date"  id="fecha_ini"/></p>
        </div>
        <div class="form-group col-md-3">
            <p>Fecha de fin captura<br><input class="form-control" type="date"  id="fecha_fin"/></p>
        </div>
        <div class="form-group col-md-3">
            <p>Fecha de inicio próximo contacto<br><input type="date" class="form-control" id="fecha_ini_con"/></p>
        </div>
        <div class="form-group col-md-3">
            <p>Fecha de fin próximo contacto<br><input type="date" class="form-control" id="fecha_fin_con"/></p>
        </div>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<?php
write_footer();
