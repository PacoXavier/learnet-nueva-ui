<?php
error_reporting(E_ALL);
require_once('Connections/cnn.php');
require_once('estandares/class_interesados.php');
require_once('estandares/class_ofertas.php');
require_once("estandares/class_usuarios.php");
require_once('estandares/class_motivos_bajas.php');
require_once('estandares/class_medios_enterar.php');
require_once('estandares/class_bdd.php');
require_once('estandares/class_docentes.php');
require_once('estandares/class_grados.php');


require_once('clases/DaoMediosEnterar.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoHistorialSeguimiento.php');
require_once('clases/PHPExcel.php');
require_once('clases/modelos/HistorialSeguimiento.php');
require_once('clases/DaoPlanteles.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/modelos/base.php');
    
    
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');


if($_POST['action']=="buscarInt"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $query = "SELECT *,
        CONCAT(Nombre_ins,ApellidoP_ins,ApellidoM_ins) AS Buscar
    FROM inscripciones_ulm  
    LEFT JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
    WHERE  (Nombre_ins LIKE '%".str_replace(' ','',$_POST['buscar'])."%' 
    OR ApellidoP_ins LIKE '%".str_replace(' ','',$_POST['buscar'])."%' 
    OR ApellidoM_ins LIKE '%".str_replace(' ','',$_POST['buscar'])."%' 
    OR Matricula LIKE '%".str_replace(' ','',$_POST['buscar'])."%')
            AND tipo=0 
            AND Id_plantel=".$usu->getId_plantel()."  
            ORDER BY Id_ins DESC LIMIT 20";
    foreach($base->advanced_query($query) as $v ){
        if($v['Id_ofe']>0){
           $alum = $DaoAlumnos->show($v['Id_ins']);
           if($v['Id_ofe']>0){
           $nombre_ori="";

           $oferta = $DaoOfertas->show($v['Id_ofe']);
           $esp = $DaoEspecialidades->show($v['Id_esp']);
           if ($v['Id_ori'] > 0) {
              $ori = $DaoOrientaciones->show($v['Id_ori']);
              $nombre_ori = $ori->getNombre();
            }

            $Seguimiento_ins = "";
            if ($v['Seguimiento_ins'] == 1) {
              $Seguimiento_ins = "Alta";
            } elseif ($v['Seguimiento_ins'] == 2) {
              $Seguimiento_ins = "Media";
            } elseif ($v['Seguimiento_ins'] == 3) {
              $Seguimiento_ins = "Baja";
            } elseif ($v['Seguimiento_ins'] == 4) {
              $Seguimiento_ins = "NULA";
            }


             $query_log  = "SELECT * FROM logs_inscripciones_ulm WHERE Idrel_log=".$v['Id_ins']." AND Tipo_relLog='inte' ORDER BY Id_log DESC LIMIT 1";
             $log = mysql_query($query_log, $cnn) or die(mysql_error());
             $row_log = mysql_fetch_array($log);
             
            $color="";
            $Seguimiento_ins = "";
            if ($v['Seguimiento_ins'] == 1) {
              $Seguimiento_ins = "Alta";
              $color="green";
            } elseif ($v['Seguimiento_ins'] == 2) {
              $Seguimiento_ins = "Media";
              $color="yellow";
            } elseif ($v['Seguimiento_ins'] == 3) {
              $Seguimiento_ins = "Baja";
              $color="pink";
            } elseif ($v['Seguimiento_ins'] == 4) {
              $Seguimiento_ins = "NULA";
              $color="pink";
            }
           }
          ?>
                  <tr id_alum="<?php echo $alum->getId();?>" class="<?php echo $color;?>">
                     <td  onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getId(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)" style="width: 115px;"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $oferta->getNombre_oferta(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $esp->getNombre_esp(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $nombre_ori; ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getFecha_ins(); ?></td>
                    <td style="word-break: break-all;width: 130px;"><?php echo $alum->getEmail(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getCel(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)" class="prioridad"><?php echo $Seguimiento_ins; ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><div class="box_seguimiento"><?php echo $row_log['Comen_log']?></div></td>
                    <td><?php echo $row_log['Dia_contactar']; ?></td>
                    <td><input type="checkbox"> </td>
                    <td><button onclick="delete_int(<?php echo $alum->getId(); ?>)">Eliminar</button>
                        <button onclick="mostrar_box_comentario(<?php echo $alum->getId(); ?>)">Seguimiento</button>
                    </td>
                  </tr>
                  <?php
        }
       
    }
}


if($_POST['action']=="delete_int"){
  
    $class_interesados = new class_interesados($_POST['Id_int']);
    $int = $class_interesados->get_interesado();
    
    $sql_nueva_entrada = sprintf("DELETE FROM inscripciones_ulm WHERE Id_ins=%s",
            GetSQLValueString($_POST['Id_int'] , "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    
    $sql_nueva_entrada = sprintf("DELETE FROM direcciones_uml WHERE Id_dir=%s",
            GetSQLValueString($int['Id_dir'] , "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    
    $sql_nueva_entrada = sprintf("DELETE FROM logs_inscripciones_ulm WHERE Idrel_log=%s AND Tipo_relLog=%s",
            GetSQLValueString($_POST['Id_int'] , "int"),
            GetSQLValueString('inte' , "text"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

    foreach ($int['Ofertas'] as $k => $v) {
       $sql_nueva_entrada = sprintf("DELETE FROM ofertas_alumno WHERE Id_ofe_alum=%s", 
          GetSQLValueString($v['Id_ofe_alum'] , "int"));
          $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());  

       //Eliminamos los ciclos
       $query_ciclos_alum= "SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=".$v['Id_ofe_alum'];
       $ciclos_alum = mysql_query($query_ciclos_alum, $cnn) or die(mysql_error());
       $row_ciclos_alum= mysql_fetch_array($ciclos_alum);
       $totalRows_ciclos_alum= mysql_num_rows($ciclos_alum);
       if($totalRows_ciclos_alum>0){
           do{
             //Obtenemos los pagos del alumno del ciclo
             $query_Pagos_ciclo = "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=".$row_ciclos_alum['Id_ciclo_alum'];
             $Pagos_Pagos_ciclo = mysql_query($query_Pagos_ciclo, $cnn) or die(mysql_error());
             $row_Pagos_ciclo = mysql_fetch_array($Pagos_Pagos_ciclo);
             $totalRows_Pagos_ciclo= mysql_num_rows($Pagos_Pagos_ciclo);
              if ($totalRows_Pagos_ciclo > 0) {
                do{
                  //Elliminamos los recargos del pago
                  $sql_nueva_entrada = sprintf("DELETE FROM Recargos_pago WHERE Id_pago=%s", 
                  GetSQLValueString($row_Pagos_ciclo['Id_pago_ciclo'] , "int"));
                  $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

                  //Elliminamos los pagos_pagos
                  $sql_nueva_entrada = sprintf("DELETE FROM Pagos_pagos WHERE Id_pago_ciclo=%s", 
                  GetSQLValueString($row_Pagos_ciclo['Id_pago_ciclo'] , "int"));
                  $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

                  //Elliminamos elk seguimiento del pago
                  $sql_nueva_entrada = sprintf("DELETE FROM Seguimiento_pago WHERE Id_pago=%s", 
                  GetSQLValueString($row_Pagos_ciclo['Id_pago_ciclo'] , "int"));
                  $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

                }while($row_Pagos_ciclo = mysql_fetch_array($Pagos_Pagos_ciclo));
               } 

             $sql_nueva_entrada = sprintf("DELETE FROM Pagos_ciclo WHERE Id_ciclo_alum=%s", 
                  GetSQLValueString($row_ciclos_alum['Id_ciclo_alum'] , "int"));
                  $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error()); 
           }while($row_ciclos_alum= mysql_fetch_array($ciclos_alum));
       }
        $sql_nueva_entrada = sprintf("DELETE FROM ciclos_alum_ulm WHERE Id_ofe_alum=%s", 
          GetSQLValueString($v['Id_ofe_alum'] , "int"));
          $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());  

        $sql_nueva_entrada = sprintf("DELETE FROM archivos_alum_ulm WHERE Id_alum=%s AND Id_ofe_alum=%s", 
          GetSQLValueString($_POST['Id_int'] , "int"),
          GetSQLValueString($v['Id_ofe_alum'], "int"));
        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error()); 
    }
    update_interesados();
}


function update_interesados(){
    global $database_cnn, $cnn;
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    foreach($DaoAlumnos->getInteresados() as $k=>$v){
        if($v['Id_ofe']>0){
           $alum = $DaoAlumnos->show($v['Id_ins']);
           if($v['Id_ofe']>0){
           $nombre_ori="";

           $oferta = $DaoOfertas->show($v['Id_ofe']);
           $esp = $DaoEspecialidades->show($v['Id_esp']);
           if ($v['Id_ori'] > 0) {
              $ori = $DaoOrientaciones->show($v['Id_ori']);
              $nombre_ori = $ori->getNombre();
            }
            
            $color="";
            $Seguimiento_ins = "";
            if ($v['Seguimiento_ins'] == 1) {
              $Seguimiento_ins = "Alta";
              $color="green";
            } elseif ($v['Seguimiento_ins'] == 2) {
              $Seguimiento_ins = "Media";
              $color="yellow";
            } elseif ($v['Seguimiento_ins'] == 3) {
              $Seguimiento_ins = "Baja";
              $color="pink";
            } elseif ($v['Seguimiento_ins'] == 4) {
              $Seguimiento_ins = "NULA";
              $color="pink";
            }

             $query_log  = "SELECT * FROM logs_inscripciones_ulm WHERE Idrel_log=".$v['Id_ins']." AND Tipo_relLog='inte' ORDER BY Id_log DESC LIMIT 1";
             $log = mysql_query($query_log, $cnn) or die(mysql_error());
             $row_log = mysql_fetch_array($log);
           }
          ?>
                  <tr id_alum="<?php echo $alum->getId();?>" class="<?php echo $color;?>">
                     <td  onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getId(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)" style="width: 115px;"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $oferta->getNombre_oferta(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $esp->getNombre_esp(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $nombre_ori; ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getFecha_ins(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)" style="word-break: break-all;width: 130px;"><?php echo $alum->getEmail(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getCel(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)" class="prioridad"><?php echo $Seguimiento_ins; ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><div class="box_seguimiento"><?php echo $row_log['Comen_log']?></div></td>
                    <td><?php echo $row_log['Dia_contactar']; ?></td>
                    <td><input type="checkbox"> </td>
                    <td><button onclick="delete_int(<?php echo $alum->getId(); ?>)">Eliminar</button>
                        <button onclick="mostrar_box_comentario(<?php echo $alum->getId(); ?>)">Seguimiento</button>
                    </td>
                  </tr>
                  <?php
        }
    }
}



if($_POST['action']=="filtro"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $query="";

    if($_POST['Id_usu']>0){
        $query=$query." AND Id_usu_capturo=".$_POST['Id_usu'];
    }
    if($_POST['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_POST['Id_ofe'];
    }
    if($_POST['Id_esp']>0){
        $query=$query." AND Id_esp=".$_POST['Id_esp'];
    }
    if($_POST['Id_ori']>0){
        $query=$query." AND Id_ori=".$_POST['Id_ori'];
    }
    if(strlen($_POST['Fecha_ini'])>0 && $_POST['Fecha_fin']==null){
        $query=$query." AND Fecha_ins>='".$_POST['Fecha_ini']."'";
    }elseif(strlen($_POST['Fecha_fin'])>0 && $_POST['Fecha_ini']==null){
        $query=$query." AND Fecha_ins<='".$_POST['Fecha_fin']."'";
    }elseif(strlen($_POST['Fecha_ini'])>0 && strlen($_POST['Fecha_fin'])>0){
        $query=$query." AND (Fecha_ins>='".$_POST['Fecha_ini']."' AND Fecha_ins<='".$_POST['Fecha_fin']."')";
    }
    
    if($_POST['Prioridad']>0){
        $query=$query." AND Seguimiento_ins=".$_POST['Prioridad'];
    }
    
    
    foreach($DaoAlumnos->getInteresados($query,null) as $k=>$v){
        if($v['Id_ofe']>0){
           $alum = $DaoAlumnos->show($v['Id_ins']);
           if($v['Id_ofe']>0){
           $nombre_ori="";

           $oferta = $DaoOfertas->show($v['Id_ofe']);
           $esp = $DaoEspecialidades->show($v['Id_esp']);
           if ($v['Id_ori'] > 0) {
              $ori = $DaoOrientaciones->show($v['Id_ori']);
              $nombre_ori = $ori->getNombre();
            }

            $color="";
            $Seguimiento_ins = "";
            if ($v['Seguimiento_ins'] == 1) {
              $Seguimiento_ins = "Alta";
              $color="green";
            } elseif ($v['Seguimiento_ins'] == 2) {
              $Seguimiento_ins = "Media";
              $color="yellow";
            } elseif ($v['Seguimiento_ins'] == 3) {
              $Seguimiento_ins = "Baja";
              $color="pink";
            } elseif ($v['Seguimiento_ins'] == 4) {
              $Seguimiento_ins = "NULA";
              $color="pink";
            }
            
            $Medio_aux=$v['Id_medio_ent'];
            if($_POST['Id_medio']>0){
                $Medio_aux=$_POST['Id_medio'];
            }

             $query_log  = "SELECT * FROM logs_inscripciones_ulm WHERE Idrel_log=".$v['Id_ins']." AND Tipo_relLog='inte' ORDER BY Id_log DESC LIMIT 1";
             $log = mysql_query($query_log, $cnn) or die(mysql_error());
             $row_log = mysql_fetch_array($log);
             $totalRows_logs = mysql_num_rows($log);
             $segui=0;
             if($_POST['seguimiento']==1 || $_POST['seguimiento']==2){
                 if($totalRows_logs>0){
                    $segui=1; 
                 }elseif($totalRows_logs==0){
                    $segui=2; 
                 }
             }
             
           }
           if($v['Id_medio_ent']==$Medio_aux && $segui==$_POST['seguimiento']){
          ?>
                  <tr id_alum="<?php echo $alum->getId();?>" class="<?php echo $color;?>">
                     <td  onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getId(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)" style="width: 115px;"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $oferta->getNombre_oferta(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $esp->getNombre_esp(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $nombre_ori; ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getFecha_ins(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)" style="word-break: break-all;width: 130px;"><?php echo $alum->getEmail(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getCel(); ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)" class="prioridad"><?php echo $Seguimiento_ins; ?></td>
                    <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><div class="box_seguimiento"><?php echo $row_log['Comen_log']?></div></td>
                    <td class="contactar"><?php echo $row_log['Dia_contactar']; ?></td>
                    <td><input type="checkbox"> </td>
                    <td><button onclick="delete_int(<?php echo $alum->getId(); ?>)">Eliminar</button>
                        <button onclick="mostrar_box_comentario(<?php echo $alum->getId(); ?>)">Seguimiento</button>
                    </td>
                  </tr>
                  <?php
            }
        }
    }
}

if($_POST['action']=="mostrar_box_comentario"){
    $DaoHistorialSeguimiento=new DaoHistorialSeguimiento();
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $alum=$DaoAlumnos->show($_POST['Id_int']);
?>
<div id="box_emergente" class="box_seguimiento">
	<h1><i class="fa fa-history"></i> Historial de seguimiento</h1>
        <ul id="list-historia">
            <?php
            $count=0;
            foreach($DaoHistorialSeguimiento->getHistorialSeguimiento('inte', $_POST['Id_int']) as $historial){
                $usu=$DaoUsuarios->show($historial->getId_usu());
            ?>
              <li>
                  <p><b>Fecha:</b> <?php echo $historial->getFecha_log()?></p>
                  <p><b>Usuario:</b> <?php echo $usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();?></p>
                  <?php
                  if(strlen($historial->getTipoSeguimiento())>0){
                      if($historial->getTipoSeguimiento()=="email"){
                         $tipo="Email"; 
                      }elseif($historial->getTipoSeguimiento()=="tel"){
                         $tipo="Teléfono"; 
                      }
                  ?>
                     <p><b>Contactado via:</b> <?php echo $tipo;?></p>
                  <?php
                  }
                  ?>
                  <p><b>Comentarios:</b> <?php echo $historial->getComen_log()?></p>
              </li>
            <?php
            $count++;
            }
            ?>
        </ul>
        <p>Contactar el dia:<br><input type="date" id="contactar"/></p>
        <?php
        if($count>0){
        ?>
        <p>Contactado vía:<br>
            <select id="tipo-seg">
                <option value="0"></option>
                <option value="1">Email</option>
                <option value="2">Teléfono</option>
            </select>
        </p>
        <?php
        }
        ?>
        <p>Prioridad:<br>
            <select id="prioridad">
                <option value="0"></option>
                <option value="1" <?php if($alum->getSeguimiento_ins()==1){?> selected="selected" <?php } ?>>Alta</option>
                <option value="2" <?php if($alum->getSeguimiento_ins()==2){?> selected="selected" <?php } ?>>Media</option>
                <option value="3" <?php if($alum->getSeguimiento_ins()==3){?> selected="selected" <?php } ?>>Baja</option>
                <option value="4" <?php if($alum->getSeguimiento_ins()==4){?> selected="selected" <?php } ?>>Nula</option>
            </select>
        </p>
	<p>Comentarios:<br><textarea id="coment"></textarea></p>
    <button onclick="capturar_seguimiento(<?php echo $_POST['Id_int']?>)">Guardar</button><button onclick="ocultar_error_layer()">Cerrar</button>
</div>
<?php
}

if($_POST['action']=="capturar_seguimiento"){
    $tipo="";
    if($_POST['tipo']=="1"){
        $tipo="email";
    }elseif($_POST['tipo']=="2"){
        $tipo="tel";
    }
    $DaoHistorialSeguimiento= new DaoHistorialSeguimiento();
    $HistorialSeguimiento= new HistorialSeguimiento();
    $HistorialSeguimiento->setComen_log($_POST['coment']);
    $HistorialSeguimiento->setDia_contactar($_POST['contactar']);
    $HistorialSeguimiento->setFecha_log(date('Y-m-d H:i:s'));
    $HistorialSeguimiento->setId_usu($_COOKIE['admin/Id_usu']);
    $HistorialSeguimiento->setTipoSeguimiento($tipo);
    $HistorialSeguimiento->setTipo_relLog('inte');
    $HistorialSeguimiento->setIdrel_log($_POST['Id_int']);
    $DaoHistorialSeguimiento->add($HistorialSeguimiento);
    
    //Captura de historial
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $alum=$DaoAlumnos->show($_POST['Id_int']);
    $alum->setSeguimiento_ins($_POST['prioridad']);
    $DaoAlumnos->update($alum);
    $TextoHistorial="Se registra seguimiento para ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ".$_POST['coment'];
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Interesados");
    
    $color="";
    $Seguimiento_ins="";
    if ($_POST['prioridad'] == 1) {
      $Seguimiento_ins = "Alta";
      $color="green";
    } elseif ($_POST['prioridad'] == 2) {
      $Seguimiento_ins = "Media";
      $color="yellow";
    } elseif ($_POST['prioridad'] == 3) {
      $Seguimiento_ins = "Baja";
      $color="pink";
    } elseif ($_POST['prioridad'] == 4) {
      $Seguimiento_ins = "NULA";
      $color="pink";
    }
    $resp=array();
    $resp['comentario']=$_POST['coment'];
    $resp['contactar']=$_POST['contactar'];
    $resp['color']=$color;
    $resp['Prioridad']=$Seguimiento_ins;
    echo json_encode($resp);
}


if($_GET['action']=="download_excel"){
    $DaoHistorialSeguimiento=new DaoHistorialSeguimiento();
    $DaoUsuarios= new DaoUsuarios();
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'OFERTA');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'CARRERA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'ORIENTACIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'ALTA');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'EMAIL');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'TEL.');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'PRIORIDAD');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'CONTACTAR');
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'SE ENTERO POR:');
    $objPHPExcel->getActiveSheet()->setCellValue('L1', 'HISTORIAL DE SEGUIMIENTO');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','L') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoMediosEnterar= new DaoMediosEnterar();
    $query="";

    if($_GET['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_GET['Id_ofe'];
    }
    if($_GET['Id_esp']>0){
        $query=$query." AND Id_esp=".$_GET['Id_esp'];
    }
    if($_GET['Id_ori']>0){
        $query=$query." AND Id_ori=".$_GET['Id_ori'];
    }
    if(strlen($_GET['Fecha_ini'])>0 && $_GET['Fecha_fin']==null){
        $query=$query." AND Fecha_ins>='".$_GET['Fecha_ini']."'";
    }elseif(strlen($_GET['Fecha_fin'])>0 && $_GET['Fecha_ini']==null){
        $query=$query." AND Fecha_ins<='".$_GET['Fecha_fin']."'";
    }elseif(strlen($_GET['Fecha_ini'])>0 && strlen($_POST['Fecha_fin'])>0){
        $query=$query." AND (Fecha_ins>='".$_GET['Fecha_ini']."' AND Fecha_ins<='".$_GET['Fecha_fin']."')";
    }
    
    if($_GET['Prioridad']>0){
        $query=$query." AND Seguimiento_ins=".$_GET['Prioridad'];
    }
    
    
    $count=1;
    foreach($DaoAlumnos->getInteresados($query,null) as $k=>$v){
        $Cadena="";
        if($v['Id_ofe']>0){
           $alum = $DaoAlumnos->show($v['Id_ins']);
           if($v['Id_ofe']>0){
           $nombre_ori="";

           $oferta = $DaoOfertas->show($v['Id_ofe']);
           $esp = $DaoEspecialidades->show($v['Id_esp']);
           if ($v['Id_ori'] > 0) {
              $ori = $DaoOrientaciones->show($v['Id_ori']);
              $nombre_ori = $ori->getNombre();
            }

            $Seguimiento_ins = "";
            if ($v['Seguimiento_ins'] == 1) {
              $Seguimiento_ins = "Alta";
            } elseif ($v['Seguimiento_ins'] == 2) {
              $Seguimiento_ins = "Media";
            } elseif ($v['Seguimiento_ins'] == 3) {
              $Seguimiento_ins = "Baja";
            } elseif ($v['Seguimiento_ins'] == 4) {
              $Seguimiento_ins = "NULA";
            }
            
            $Medio_aux=$v['Id_medio_ent'];
            if($_GET['Id_medio']>0){
                $Medio_aux=$_GET['Id_medio'];
            }
            
            $nombreMedio="";
            if($Medio_aux>0){
                $medio= $DaoMediosEnterar->show($Medio_aux);
                $nombreMedio=$medio->getMedio();
            }
             
            
            $countH=1;
            foreach($DaoHistorialSeguimiento->getHistorialSeguimiento('inte', $v['Id_ins']) as $historial){
                 if(strlen($historial->getTipoSeguimiento())>0){
                      if($historial->getTipoSeguimiento()=="email"){
                         $tipo="Email"; 
                      }elseif($historial->getTipoSeguimiento()=="tel"){
                         $tipo="Teléfono"; 
                      }
                  }
                $usu=$DaoUsuarios->show($historial->getId_usu());
                $Cadena.="#".$countH."\r\n";
                $Cadena.="Fecha: ".$historial->getFecha_log()."\r\n";
                $Cadena.="Usuario: ".$usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu()."\r\n";
                $Cadena.="Contactado via: ".$tipo."\r\n";
                $Cadena.="Comentarios: ".$historial->getComen_log()."\r\n\n";
                $countH++;
            }
             
            $Conseguimiento=$countH;
            if($_GET['Conseguimiento']>0){
                $Conseguimiento=$_POST['Conseguimiento'];
            }
             
           }
           if($v['Id_medio_ent']==$Medio_aux && $countH>=$Conseguimiento){
                $count++;
                $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
                $objPHPExcel->getActiveSheet()->setCellValue("B$count", $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM());
                $objPHPExcel->getActiveSheet()->setCellValue("C$count", $oferta->getNombre_oferta());
                $objPHPExcel->getActiveSheet()->setCellValue("D$count", $esp->getNombre_esp());
                $objPHPExcel->getActiveSheet()->setCellValue("E$count", $nombre_ori);
                $objPHPExcel->getActiveSheet()->setCellValue("F$count", $alum->getFecha_ins());
                $objPHPExcel->getActiveSheet()->setCellValue("G$count", $alum->getEmail());
                $objPHPExcel->getActiveSheet()->setCellValue("H$count", $alum->getCel());
                $objPHPExcel->getActiveSheet()->setCellValue("I$count", $Seguimiento_ins);
                $objPHPExcel->getActiveSheet()->setCellValue("J$count", $row_log['Dia_contactar']);
                $objPHPExcel->getActiveSheet()->setCellValue("K$count", $nombreMedio);
                $objPHPExcel->getActiveSheet()->setCellValue("L$count", $Cadena);
                $objPHPExcel->getActiveSheet()->getStyle("L$count")->getAlignment()->setWrapText(true);

            }
        }
    }


    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=Interesados-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}




if($_POST['action']=="send_email"){
   $DaoUsuarios= new DaoUsuarios();
   $DaoPlanteles= new DaoPlanteles();
   $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
   $plantel=$DaoPlanteles->show($usu->getId_plantel());
     
   foreach($_POST['alumnos'] as $v){
     $DaoAlumnos= new DaoAlumnos();
     $alum=$DaoAlumnos->show($v);
     
     if(strlen($alum->getEmail())>0 && $alum->getSeguimiento_ins()!="4"){
       $base = new base();
       $arrayData= array();
       $arrayData['Asunto']=$_POST['asunto'];
       $arrayData['Mensaje']=$_POST['mensaje'];
       $arrayData['IdRel']=$_COOKIE['admin/Id_usu'];
       $arrayData['TipoRel']="usu";
       $arrayData['email-usuario']= $plantel->getEmail();
       $arrayData['nombre-usuario']=$DaoPlanteles->covertirCadena($plantel->getNombre_plantel(),2);
       $arrayData['replyTo']=$usu->getEmail_usu();
       $arrayData['Destinatarios']=array();
       
       $Data= array();
       $Data['email']= $alum->getEmail();
       //$Data['email']= "christian310332@gmail.com";
       $Data['name']= $alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
       
       array_push($arrayData['Destinatarios'], $Data);
       $base->send_email($arrayData);
     }
   }
}


if($_POST['action']=="mostrar_box_masivo"){
?>
<div class="box-emergente-form  seguimiento-masivo">
	<h1><i class="fa fa-keyboard-o" aria-hidden="true"></i> Seguimiento masivo</h1>
        <p>Prioridad:<br>
            <select id="prioridad">
                <option value="0"></option>
                <option value="1">Alta</option>
                <option value="2">Media</option>
                <option value="3">Baja</option>
                <option value="4">Nula</option>
            </select>
        </p>
        <p>Contactado vía:<br>
            <select id="tipo-seg">
                <option value="0"></option>
                <option value="1">Email</option>
                <option value="2">Teléfono</option>
            </select>
        </p>
	<p>Comentarios:<br><textarea id="coment"></textarea></p>
    <button onclick="capturar_seguimiento_masivo(this)">Guardar</button><button onclick="ocultar_error_layer()">Cerrar</button>
</div>
<?php
}


if($_POST['action']=="capturar_seguimiento_masivo"){
    $tipo="";
    if($_POST['tipo']=="1"){
        $tipo="email";
    }elseif($_POST['tipo']=="2"){
        $tipo="tel";
    }
    $DaoAlumnos= new DaoAlumnos();
    $DaoHistorialSeguimiento= new DaoHistorialSeguimiento();
    foreach($_POST['alumnos'] as $v){
        $alum=$DaoAlumnos->show($v);
        $alum->setSeguimiento_ins($_POST['prioridad']);
        $DaoAlumnos->update($alum);
        
        $HistorialSeguimiento= new HistorialSeguimiento();
        $HistorialSeguimiento->setComen_log($_POST['coment']);
        $HistorialSeguimiento->setDia_contactar('');
        $HistorialSeguimiento->setFecha_log(date('Y-m-d H:i:s'));
        $HistorialSeguimiento->setId_usu($_COOKIE['admin/Id_usu']);
        $HistorialSeguimiento->setTipoSeguimiento($tipo);
        $HistorialSeguimiento->setTipo_relLog('inte');
        $HistorialSeguimiento->setIdrel_log($alum->getId());
        $DaoHistorialSeguimiento->add($HistorialSeguimiento);
      }
    update_interesados();
}
