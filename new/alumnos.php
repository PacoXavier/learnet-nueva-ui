<?php
require_once('estandares/includes.php');
if(!isset($perm['41'])){
  header('Location: home.php');
}
require_once('clases/modelos/base.php');
require_once('clases/DaoAlumnos.php');

links_head("Alumnos | ULM");
write_head_body();
write_body();

$base= new base();
$DaoAlumnos= new DaoAlumnos();
?>

<table id="tabla">
  <tr>
    <td id="column_one">
      <div class="fondo">
        <div id="box_top">
          <h1><i class="fa fa-users" aria-hidden="true"></i> Alumnos</h1>
        </div>
        <ul class="form">
            <li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>
        </ul>
        <div id="mascara_tabla">
          <table class="table">
            <thead>
              <tr>
                <td>Matricula</td>
                <td>Nombre</td>
                <td>Email</td>
                <td>Fecha ingreso</td>
                <td><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
              </tr>
            </thead>
            <tbody>
              <?php
                 foreach($DaoAlumnos->getAlumnos() as $k=>$v){
                      ?>
                          <tr id_alum="<?php echo $v['Id_ins'];?>">
                            <td onclick="mostrar(<?php echo $v['Id_ins']; ?>)"><?php echo $v['Matricula']; ?></td>
                            <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                            <td><a href="mailto:<?php echo $v['Email_ins']; ?>"><?php echo $v['Email_ins']; ?></a></td>
                            <td><?php echo $base->formatFecha($v['Alta_alum']) ?></td>
                            <td><input type="checkbox"> </td>
                          </tr>
                       <?php
                 }
              ?>
            </tbody>
          </table>
        </div>
        <div id="box_button_mostrar"><span id="mostrar" onclick="mostrar_inte()">Mostrar m&aacute;s</span></div>
      </div>
    </td>
    <td id="column_two">
      <div id="box_menus">
        <table id="usu_login">
          <tr>
            <td><div class="nom_usu"><div id="img_usu" 
                                          <?php if (strlen($usu['Img_usu']) > 0) { ?> style="background-image:url(files/<?php echo $usu['Img_usu'] ?>.jpg) <?php } ?>"></div>
                                          <?php echo $usu['Nombre_usu'] . " " . $usu['ApellidoP_usu'] . " " . $usu['ApellidoM_usu'] ?>
                <img src="images/linea_uno.png" alt="linea_uno" width="185" height="5" class="lineas"/></div>
            </td>
            <td>
              <div class="opcion">
                <a href="perfil.php?id=<?php echo $usu['Id_usu'] ?>">
                  <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Mi perfil</a>
                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/>
              </div>
            </td>
            <td><div class="opcion">
                <a href="logout.php">
                  <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Salir</a>
                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/></div>
            </td>
          </tr>
        </table>
        <h2><img src="images/flecha_blue.png" alt="flecha_blue" width="6" height="11" />Opciones Alumnado</h2>
        <ul>
          <li><a href="interesado.php" class="link">Nuevo</a></li>
          <!--<li><span onclick="mostrar_box_busqueda()">Buscar</span></li>-->
          <li><span onclick="mostrar_reportes()">Reportes</span>
            <ul id="list_reportes">
              <li><a href="alumnos.php">Datos generales</a></li>
              <li><a href="reporte_alumnos_deudores.php">Deudores </a></li>
              <li><a href="interesados.php">Interesados </a></li>
              <li><a href="reporte_alumnos_bajas.php">Bajas </a></li>
              <li><a href="reporte_alumnos.php">Alumnos</a></li>
            </ul>
          </li>
           <li><span onclick="mostrar_box_email()">Enviar email</span></li>
        </ul>
      </div>
    </td>
  </tr>
</table>
<div id="box_buscador"></div>
<script src="js/ckeditor/ckeditor.js"></script>
<input type="file" id="files" name="files" multiple="">

<?php
write_footer();
