<?php 
/*
if($_REQUEST['id']>0){

require_once('Connections/cnn.php');
require_once('estandares/Fpdf/fpdf.php'); 
require_once('estandares/Fpdf/fpdi.php'); 
require_once('estandares/cantidad_letra.php'); 
require_once('estandares/class_interesados.php'); 
require_once('estandares/class_ofertas.php'); 
require_once('estandares/class_direcciones.php'); 

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');

// initiate FPDI 
$pdf = new FPDI(); 

// add a page 
$pdf->AddPage(); 

// set the sourcefile 
$pdf->setSourceFile('estandares/comprobante.pdf'); 

// import page 1 
$tplIdx = $pdf->importPage(1); 

// use the imported page and place it at point 10,10 with a width of 100 mm 
$pdf->useTemplate($tplIdx); 

//Id_ofe_alum
$class_interesados = new class_interesados($_REQUEST['id']);
$alum=$class_interesados->get_interesado();

// now write some text above the imported page 
$pdf->SetFont('Arial','B',8); 
$pdf->SetTextColor(0,0,0); 

$pdf->SetXY(28,12); 
$pdf->Write(10, utf8_decode($alum['Matricula'])); 
$pdf->SetXY(13,33.5); 
$pdf->Write(10, utf8_decode("Guadalajara, Jalisco, México, C.P.44160")); 
$pdf->SetXY(13,37.5); 
$pdf->Write(10, utf8_decode(date('Y-m-d H:i:s'))); 

$pdf->SetXY(168,12); 
$pdf->Write(10, utf8_decode("A - 279")); 

$pdf->SetXY(110,62.5); 
$pdf->Write(10, mb_strtoupper(utf8_decode("Nombre: ".$alum['Nombre_ins']." ".$alum['ApellidoP_ins']." ".$alum['ApellidoM_ins']))); 

$pdf->SetXY(110,69.5); 
$pdf->Write(10, utf8_decode("Matrícula: ".$alum['Matricula'])); 


$pdf->SetXY(147,94.5); 
$pdf->Write(10, utf8_decode("Banorte 2451")); 

$subtotal=0;
//Esto se genera cuando solo requieres el comprobante de pago
//En la primera inscripcion cuando el intersado pasa a ser alumno
if(!isset($_REQUEST['id_pago'] )&& !isset($_REQUEST['id_ciclo_alum'])){
    //Pagos
    $metodo_pago="";
    $i=1;
    foreach($alum['Ofertas'] as $k=>$v){
        $class_ofertas= new class_ofertas($v['Id_ofe']);
        $ofe=$class_ofertas->get_oferta();

        $esp=$class_ofertas->get_especialidad($v['Id_esp']);
        if($v['Id_ori']>0){
        $ori=$class_ofertas->get_orientacion($v['Id_ori']);
        }
        $query_ciclo_audeudo = "SELECT * FROM  ciclos_ulm WHERE Id_ciclo=" . $v['Ciclos_oferta'][0]['Id_ciclo'];
        $ciclo_audeudo= mysql_query($query_ciclo_audeudo, $cnn) or die(mysql_error());
        $row_ciclo_audeudo = mysql_fetch_array($ciclo_audeudo);


        $query_pago_inscripcion = "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=" .$v['Ciclos_oferta'][0]['Id_ciclo_alum']." AND Concepto='Inscripcion'";
        $pago_inscripcion= mysql_query($query_pago_inscripcion, $cnn) or die(mysql_error());
        $row_pago_inscripcion = mysql_fetch_array($pago_inscripcion);
        $totalRows_pago_inscripcion = mysql_num_rows($pago_inscripcion);
        if ($totalRows_pago_inscripcion > 0) {
            $pdf->SetXY(19,21.5); 
            $pdf->Write(10, utf8_decode($row_pago_inscripcion['Fecha_pagado'])); 
          do{
            $query_Pagos_pagos = "SELECT * FROM Pagos_pagos WHERE Id_pago_ciclo=".$row_pago_inscripcion['Id_pago_ciclo'];
            $Pagos_pagos= mysql_query($query_Pagos_pagos, $cnn) or die(mysql_error());
            $row_Pagos_pagos = mysql_fetch_array($Pagos_pagos);
            $totalRows_Pagos_pagos= mysql_num_rows($Pagos_pagos);
            if($totalRows_Pagos_pagos){
                do{
                    if($row_Pagos_pagos['metodo_pago']==1){
                      $metodo_pago="Tarjeta de crédito";
                    }elseif($row_Pagos_pagos['metodo_pago']==2){
                      $metodo_pago="Tarjeta de débito";  
                    }elseif($row_Pagos_pagos['metodo_pago']==3){
                      $metodo_pago="Efectivo";  
                    }elseif($row_Pagos_pagos['metodo_pago']==4){
                      $metodo_pago="Depósito";  
                    }elseif($row_Pagos_pagos['metodo_pago']==5){
                      $metodo_pago="Paypal";  
                    }elseif($row_Pagos_pagos['metodo_pago']==6){
                      $metodo_pago="Cheque";  
                    }
                }while($row_Pagos_pagos = mysql_fetch_array($Pagos_pagos));
            }
        
            $pdf->SetXY(13,115+($i*10)); 
            $pdf->Write(10,$i);
            $pdf->SetXY(30,118.5+($i*10)); 
            $pdf->MultiCell(50,3 ,mb_strtoupper(utf8_decode($row_pago_inscripcion['Concepto']."/ ".$ofe['Nombre_oferta']." - ".$esp['Nombre_esp']." ".$ori['Nombre_ori']." / ".$row_ciclo_audeudo['Clave'])),0,'L'); 
            $pdf->SetXY(90,115+($i*10)); 
            $pdf->Write(10, $row_pago_inscripcion['Fecha_pagado']); 
            $pdf->SetXY(170,115+($i*10)); 
            $pdf->Write(10, "$".  number_format($row_pago_inscripcion['Cantidad_Pagada'],2)); 
            $subtotal+=$row_pago_inscripcion['Cantidad_Pagada'];
            $i++;
          }while($row_pago_inscripcion = mysql_fetch_array($pago_inscripcion));
        }
    }
    $pdf->SetXY(55,94); 
    $pdf->Write(10, mb_strtoupper(utf8_decode($metodo_pago))); 
    //Este se utiliza cunado quieres ver un solo pago
}elseif($_REQUEST['id_pago']>0 && $_REQUEST['id_ciclo']>0){
    $metodo_pago="";
    $query_pago = "SELECT * FROM Pagos_ciclo WHERE Id_pago_ciclo=" .$_REQUEST['id_pago'];
    $pago= mysql_query($query_pago, $cnn) or die(mysql_error());
    $row_pago = mysql_fetch_array($pago);
    $totalRows_pago = mysql_num_rows($pago);
    if ($totalRows_pago > 0) {
        $pdf->SetXY(19,21.5); 
        $pdf->Write(10, utf8_decode($row_pago['Fecha_pagado'])); 
        
        $query_Pagos_pagos = "SELECT * FROM Pagos_pagos WHERE Id_pago_ciclo=".$_REQUEST['id_pago'];
        $Pagos_pagos= mysql_query($query_Pagos_pagos, $cnn) or die(mysql_error());
        $row_Pagos_pagos = mysql_fetch_array($Pagos_pagos);
        $totalRows_Pagos_pagos= mysql_num_rows($Pagos_pagos);
        if($totalRows_Pagos_pagos){
            do{
                if($row_Pagos_pagos['metodo_pago']==1){
                  $metodo_pago="Tarjeta de crédito";
                }elseif($row_Pagos_pagos['metodo_pago']==2){
                  $metodo_pago="Tarjeta de débito";  
                }elseif($row_Pagos_pagos['metodo_pago']==3){
                  $metodo_pago="Efectivo";  
                }elseif($row_Pagos_pagos['metodo_pago']==4){
                  $metodo_pago="Depósito";  
                }elseif($row_Pagos_pagos['metodo_pago']==5){
                  $metodo_pago="Paypal";  
                }elseif($row_Pagos_pagos['metodo_pago']==6){
                  $metodo_pago="Cheque";  
                }
            }while($row_Pagos_pagos = mysql_fetch_array($Pagos_pagos));
        }
        $pdf->SetXY(55,94); 
        $pdf->Write(10, utf8_decode($metodo_pago)); 
        
        $Ciclo_oferta=$class_interesados->get_ciclo_oferta($row_pago['Id_ciclo_alum']);
        $Oferta_alum=$class_interesados->get_oferta_alumno($Ciclo_oferta['Id_ofe_alum']);
     
        $class_ofertas= new class_ofertas($Oferta_alum['Id_ofe']);

        $ofe=$class_ofertas->get_oferta();
        $esp=$class_ofertas->get_especialidad($Oferta_alum['Id_esp']);
        if($Oferta_alum['Id_ori']>0){
        $ori=$class_ofertas->get_orientacion($Oferta_alum['Id_ori']);
        }

        $query_ciclo = "SELECT * FROM  ciclos_ulm WHERE Id_ciclo=" . $_REQUEST['id_ciclo'];
        $ciclo= mysql_query($query_ciclo, $cnn) or die(mysql_error());
        $row_ciclo = mysql_fetch_array($ciclo);
        
        //Monto del recargo
        $query_Recargos_pago = "SELECT SUM(Monto_recargo) as recargos FROM Recargos_pago WHERE Id_pago=" . $_REQUEST['id_pago'];
        $Pagos_Recargos_pago = mysql_query($query_Recargos_pago, $cnn) or die(mysql_error());
        $row_Recargos_pago = mysql_fetch_array($Pagos_Recargos_pago);
          
        $l=1;
        $i=1;   
        $pdf->SetXY(13,115); 
        $pdf->Write(10,$l);
        $pdf->SetXY(30,118.5); 
        $pdf->MultiCell(50,3 ,utf8_decode(strtoupper($row_pago['Concepto']."/ ".$ofe['Nombre_oferta']." - ".$esp['Nombre_esp']." ".$ori['Nombre_ori']." / ".$row_ciclo['Clave'])),0,'L'); 
        $pdf->SetXY(90,115); 
        $pdf->Write(10, $row_pago['Fecha_pagado']); 
        $pdf->SetXY(170,115); 
        //$mensualidad= $row_Pagos_ciclo['Mensualidad']-$row_Pagos_ciclo['Descuento']-$row_Pagos_ciclo['Descuento_beca'];
        $pdf->Write(10, "$".  number_format($row_pago['Cantidad_Pagada']-$row_Recargos_pago['recargos'],2)); 
        
         //Recargos
         //suma epagospago 
         //suma de los recargos

          $query_Recargos_pago = "SELECT * FROM Recargos_pago WHERE Id_pago=" . $_REQUEST['id_pago'];
          $Pagos_Recargos_pago = mysql_query($query_Recargos_pago, $cnn) or die(mysql_error());
          $row_Recargos_pago = mysql_fetch_array($Pagos_Recargos_pago);
          $totalRows_Recargos_pago = mysql_num_rows($Pagos_Recargos_pago);
          if ($totalRows_Recargos_pago > 0) {
            do {
                $l++;
                $pdf->SetXY(13,120+($i*10)); 
                $pdf->Write(10,$l);
                $pdf->SetXY(30,123.5+($i*10)); 
                $pdf->MultiCell(50,3 ,mb_strtoupper(mb_strtoupper("Recargo/ ".$row_Recargos_pago['Fecha_recargo']." / ".$row_ciclo['Clave'])),0,'L'); 
                $pdf->SetXY(170,120+($i*10)); 
                $pdf->Write(10, "$".  number_format($row_Recargos_pago['Monto_recargo'],2)); 
                $i++;
            } while ($row_Recargos_pago = mysql_fetch_array($Pagos_Recargos_pago));
          }
                  
        $subtotal+=$row_pago['Cantidad_Pagada']+$recargos;
    }
    //Este es para mostrar todos los pagos
}elseif($_REQUEST['id_ciclo_alum']>0 && $_REQUEST['id_ciclo']>0){
  $metodo_pago="";
  $query_Pagos_ciclo = "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=" . $_REQUEST['id_ciclo_alum'] . " ORDER BY Id_pago_ciclo ASC";
  $Pagos_ciclo = mysql_query($query_Pagos_ciclo, $cnn) or die(mysql_error());
  $row_Pagos_ciclo = mysql_fetch_array($Pagos_ciclo);
  $totalRows_Pagos_ciclo = mysql_num_rows($Pagos_ciclo);
  if ($totalRows_Pagos_ciclo > 0) {
      $pdf->SetXY(19,21.5); 
      $pdf->Write(10, utf8_decode($row_Pagos_ciclo['Fecha_pagado']));
        
    $i=1;
    $l=1;
    do {
        
        $query_Pagos_pagos = "SELECT * FROM Pagos_pagos WHERE Id_pago_ciclo=".$row_Pagos_ciclo['Id_pago_ciclo'];
        $Pagos_pagos= mysql_query($query_Pagos_pagos, $cnn) or die(mysql_error());
        $row_Pagos_pagos = mysql_fetch_array($Pagos_pagos);
        $totalRows_Pagos_pagos= mysql_num_rows($Pagos_pagos);
        if($totalRows_Pagos_pagos){
            do{
                if($row_Pagos_pagos['metodo_pago']==1){
                  $metodo_pago="Tarjeta de crédito";
                }elseif($row_Pagos_pagos['metodo_pago']==2){
                  $metodo_pago="Tarjeta de débito";  
                }elseif($row_Pagos_pagos['metodo_pago']==3){
                  $metodo_pago="Efectivo";  
                }elseif($row_Pagos_pagos['metodo_pago']==4){
                  $metodo_pago="Depósito";  
                }elseif($row_Pagos_pagos['metodo_pago']==5){
                  $metodo_pago="Paypal";  
                }elseif($row_Pagos_pagos['metodo_pago']==6){
                  $metodo_pago="Cheque";  
                }
            }while($row_Pagos_pagos = mysql_fetch_array($Pagos_pagos));
        }
        $pdf->SetXY(55,94); 
        $pdf->Write(10, mb_strtoupper(utf8_decode($metodo_pago))); 
        
        
        $Ciclo_oferta=$class_interesados->get_ciclo_oferta($row_Pagos_ciclo['Id_ciclo_alum']);
        $Oferta_alum=$class_interesados->get_oferta_alumno($Ciclo_oferta['Id_ofe_alum']);
     
        $class_ofertas= new class_ofertas($Oferta_alum['Id_ofe']);

        $ofe=$class_ofertas->get_oferta();
        $esp=$class_ofertas->get_especialidad($Oferta_alum['Id_esp']);
        if($Oferta_alum['Id_ori']>0){
        $ori=$class_ofertas->get_orientacion($Oferta_alum['Id_ori']);
        }

        $query_ciclo = "SELECT * FROM  ciclos_ulm WHERE Id_ciclo=" . $_REQUEST['id_ciclo'];
        $ciclo= mysql_query($query_ciclo, $cnn) or die(mysql_error());
        $row_ciclo = mysql_fetch_array($ciclo);
       
           
        
        $pdf->SetXY(13,100+($i*10)+$top_recargo); 
        $pdf->Write(10,$l);
        $pdf->SetXY(30,102.5+($i*10)+$top_recargo); 
        $pdf->MultiCell(50,3 ,mb_strtoupper(utf8_decode($row_Pagos_ciclo['Concepto']."/ ".$ofe['Nombre_oferta']." - ".$esp['Nombre_esp']." ".$ori['Nombre_ori']." / sdsds".$row_ciclo['Clave'])),0,'L'); 
        $pdf->SetXY(90,100+($i*10)+$top_recargo); 
        $pdf->Write(10, $row_Pagos_ciclo['Fecha_pagado']); 
        $pdf->SetXY(180,100+($i*10)+$top_recargo); 
        $pdf->Write(10, "$".  number_format($row_Pagos_ciclo['Cantidad_Pagada'],2)); 
        //Recargos
      
        $recargos = 0;
        $top_recargo=0;
        $query_Recargos_pago = "SELECT * FROM Recargos_pago WHERE Id_pago=" . $row_Pagos_ciclo['Id_pago_ciclo'];
        $Pagos_Recargos_pago = mysql_query($query_Recargos_pago, $cnn) or die(mysql_error());
        $row_Recargos_pago = mysql_fetch_array($Pagos_Recargos_pago);
        $totalRows_Recargos_pago = mysql_num_rows($Pagos_Recargos_pago);
        if ($totalRows_Recargos_pago > 0) {
            do {
$l++;
                $pdf->SetXY(13,115+($i+1)); 
                $pdf->Write(10,$l);
                $pdf->SetXY(30,118.5+($i+1)); 
                $pdf->MultiCell(50,3 ,mb_strtoupper(utf8_decode("Recargo/ ".$row_Recargos_pago['Fecha_recargo']." / ".$row_ciclo['Clave'])),0,'L'); 
                $pdf->SetXY(180,115+($i+1)); 
                $pdf->Write(10, "$".  number_format($row_Recargos_pago['Monto_recargo'],2)); 
                $recargos+=$row_Recargos_pago['Monto_recargo'];
                $top_recargo=3;
                
            } while ($row_Recargos_pago = mysql_fetch_array($Pagos_Recargos_pago));
         }
       
         $subtotal+=$row_Pagos_ciclo['Cantidad_Pagada']+$recargos;
         $i++;
         $l++;
    }while($row_Pagos_ciclo = mysql_fetch_array($Pagos_ciclo));
  }
}

//Subtotales
$iva=0;
$total=$subtotal+$iva;
$pdf->SetXY(180,187); 
$pdf->Write(10,"$".  number_format($subtotal,2)); 
$pdf->SetXY(180,193); 
$pdf->Write(10,"$".  number_format($iva,2)); 
$pdf->SetXY(180,199); 
$pdf->Write(10,"$".  number_format($total,2)); 

$pdf->SetXY(33,207.5); 
$pdf->Write(10,strtoupper(num2letras($total,2))); 


$pdf->Output('Comprobante_pago.pdf', 'I'); 
}else{
    //header("Location: interesados.php");
}
     *  */
