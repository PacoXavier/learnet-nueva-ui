<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoMediosEnterar.php');


$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoGrados= new DaoGrados();
$DaoMediosEnterar= new DaoMediosEnterar();
links_head("Datos del seguro | ULM");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-user"></i> Datos del seguro</h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul class="row" style="list-style: none; font-size: 20px;">
                        <li class="col-md-6 weather-grids widget-shadow" onclick="mostrar_filtro()"><div class="stats-left" style="width:100%; height: 100px;display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-filter" style="margin-right: 5px;"></i> Filtros</div></li>
                        <!--<li onclick="mostrar_box_email()"><i class="fa fa-envelope"></i> Email</li>-->
                        <li class="col-md-6 weather-grids weather-right widget-shadow states-last" onclick="download_excel()"><div class="stats-right" style="width:100%; height: 100px; display: flex; align-items: center;justify-content: center; color: #fff"><i class="fa fa-download" style="margin-right: 5px;"></i> Descargar</div></li>
                    </ul>
                </div>
                <div id="mascara_tabla"></div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h4 class="title">Filtros</h4>
    <div class="row">
        <p class="col-md-6">Tipo:<br>
            <select id="tipo" class="form-control" onchange="updateFiltros()">
              <option value="0"></option>
              <option value="1">Alumnos</option>
              <option value="2">Docentes</option>
            </select>
        </p>
    </div>
    <div class="box-data-alumnos row">
        <p class="col-md-3">Oferta<br>
            <select id="oferta" class="form-control" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
        </p>
        <p class="col-md-3">Especialidad:<br>
            <select id="curso" class="form-control" onchange="update_orientacion_box_curso();update_grados_ofe()">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion" class="col-md-3"></div>
        <p class="col-md-3">Opci&oacute;n de pago:<br>
            <select id="opcion" class="form-control">
              <option value="0"></option>
              <option value="1">POR MATERIAS</option>
              <option value="2">PLAN COMPLETO</option>
            </select>
        </p>
    </div>
    <div style="display: flex; justify-content: flex-end; margin-top: 15px; padding-right: 15px">
        <p><button class="btn btn-primary" style="margin-right: 10px;" onclick="filtro(this)">Buscar</button><button class="btn btn-default" onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
</div>
<script src="js/ckeditor/ckeditor.js"></script>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();
