<?php
require_once('activate_error.php');
require_once('../estandares/Fpdf/fpdf.php'); 
require_once('../estandares/Fpdf/fpdi.php'); 
require_once("../estandares/QRcode/qrlib.php");
require_once('../estandares/cantidad_letra.php'); 
require_once('../require_daos.php'); 


if($_POST['action']=="send_recibo"){
    //require_once('estandares/cURL.php');
    $base= new base();

    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoPagos = new DaoPagos();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoAlumnos = new DaoAlumnos();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoCiclos = new DaoCiclos();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoRecargos = new DaoRecargos();
    $DaoMetodosPago = new DaoMetodosPago();
    $DaoUsuarios= new DaoUsuarios();
    $DaoPlanteles= new DaoPlanteles();
    
    $alum = $DaoAlumnos->show($_REQUEST['id']);
    $pago = $DaoPagos->show($_REQUEST['id_pp']);
    $metodo = $DaoMetodosPago->show($pago->getMetodo_pago());
    $ciclo = $DaoCiclos->show($_REQUEST['id_ciclo']);

    $oferta_alumno = $DaoOfertasAlumno->show($pago->getId_ofe_alum());
    $oferta = $DaoOfertas->show($oferta_alumno->getId_ofe());
    $esp = $DaoEspecialidades->show($oferta_alumno->getId_esp());
    if ($oferta_alumno->getId_ori() > 0) {
        $ori = $DaoOrientaciones->show($oferta_alumno->getId_ori());
        $nombre_ori = $ori->getNombre();
    }

    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $plantel = $DaoPlanteles->show($usu->getId_plantel());
    
    if (strlen($plantel->getFormato_pago()) > 0) {

        // initiate FPDI 
            $pdf = new FPDI();

        // add a page 
            $pdf->AddPage();

        // set the sourcefile 
           $pdf->setSourceFile('../files/'.$plantel->getFormato_pago().".pdf");

        // import page 1 
            $tplIdx = $pdf->importPage(1);

        // use the imported page and place it at point 10,10 with a width of 100 mm 
            $pdf->useTemplate($tplIdx);

        // now write some text above the imported page 
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->SetTextColor(0, 0, 0);

            $pdf->SetXY(170, 4);
            $pdf->Write(10, $pago->getFecha_captura());

            $pdf->SetXY(77, 61);
            $pdf->Write(10, mb_strtoupper(utf8_decode($alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM())));

            $pdf->SetXY(77, 69.5);
            $pdf->Write(10, utf8_decode($alum->getMatricula()));

            $subtotal = 0;
            if ($pago->getId() > 0) {

                $usuarioCapturo=$DaoUsuarios->show($pago->getId_usu());
                $nombreUsuCapturo=$usuarioCapturo->getNombre_usu()." ".$usuarioCapturo->getApellidoP_usu()." ".$usuarioCapturo->getApellidoM_usu();
    
                $pdf->SetXY(170, 12);
                $pdf->Write(10, utf8_decode($pago->getFolio()));

                $pdf->SetXY(77, 95);
                $pdf->MultiCell(90, 3, mb_strtoupper(utf8_decode($pago->getConcepto() . " / " . $pago->getDetalleCarrera(). " / " . $ciclo->getClave())), 0, 'L');
                $pdf->SetXY(77, 97);
                $pdf->Write(10, mb_strtoupper(utf8_decode($pago->getComentarios())));

                $pdf->SetXY(77, 117);
                $pdf->Write(10, mb_strtoupper(utf8_decode($metodo->getNombre())));
                $pdf->SetXY(77, 78);
                $pdf->Write(10, "$" . number_format($pago->getMonto_pp(), 2));
                $pdf->SetXY(77, 83.5);
                $pdf->Write(10, strtoupper(num2letras($pago->getMonto_pp(), 2)));
                $pdf->SetXY(77, 105);
                $pdf->Write(10, "CICLO " . $ciclo->getClave() . " " . utf8_decode($pago->getFecha_pp()));
                
                $pdf->SetXY(77, 128);
                $pdf->Write(10, mb_strtoupper(utf8_decode($nombreUsuCapturo)));

                // add QRcode
                //texto a encliptar                                       //Ruta donde se guarda la imagen
                QRcode::png('?matricula=' . $alum->getMatricula() . '&folio=' . $pago->getFolio() . '&cantidad=' . number_format($pago->getMonto_pp(), 2), 'files/test.png');
                $pdf->Image("../files/test.png", 23, 27, 33, 33);
            }
            

            $content=$pdf->Output('', 'S'); 

            //Enviar email por medio de mandrill
            $KeySistema = $base->getParamPlantel('KeyMandrill',$plantel->getId());
            $arrayParams = array();
            $arrayParams['key'] = $KeySistema;
            $arrayParams['message']['html'] = '';
            $arrayParams['message']['text'] = '';
            $arrayParams['message']['subject'] = "Comprobante de pago #".$pago->getId()." ".date('Y -m-d');
            $arrayParams['message']['from_email'] = $plantel->getEmail();
            $arrayParams['message']['from_name'] = $plantel->getNombre_plantel();
            $arrayParams['message']['to']=array();

            $arrayTo=array();
            $arrayTo["email"]=$alum->getEmail();
            //$arrayTo["email"]="christian310332@gmail.com";
            $arrayTo["name"]=$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
            array_push($arrayParams['message']['to'], $arrayTo);
/*
            $arrayTo=array();
            $arrayTo["email"]="recibos@ulm.mx";
            $arrayTo["name"]=$plantel->getNombre_plantel();;
            array_push($arrayParams['message']['to'], $arrayTo);
 * 
 */


            $array_global_merge_vars = array();
            $array_global_merge_vars["name"] = 'example name';
            $array_global_merge_vars["content"] = 'example content';

            $arrayParams['message']['global_merge_vars'] = array();
            array_push($arrayParams['message']['global_merge_vars'], $array_global_merge_vars);

            $array_merge_vars = array();
            $array_merge_vars["rcpt"] = 'example rcpt';

            $array_merge_vars['vars'] = array();
            $array_vars_attr = array();
            $array_vars_attr['name'] = 'example name';
            $array_vars_attr['content'] = 'example content';
            array_push($array_merge_vars['vars'], $array_vars_attr);

            $arrayParams['message']['merge_vars'] = array();
            array_push($arrayParams['message']['merge_vars'], $array_merge_vars);

            $arrayParams['message']['tags'] = array();
            array_push($arrayParams['message']['tags'], "example tags[]");

            $arrayParams['message']['google_analytics_domains'] = array();
            array_push($arrayParams['message']['google_analytics_domains'], "...");

            $arrayParams['message']['google_analytics_campaign'] = "...";
            $arrayParams['message']['metadata'] = array();
            array_push($arrayParams['message']['metadata'], "...");

            $array_recipient_metadata = array();
            $array_recipient_metadata["rcpt"] = 'example rcpt';

            $array_recipient_metadata['values'] = array();
            array_push($array_recipient_metadata['values'], "...");

            $arrayParams['message']['recipient_metadata'] = array();
            array_push($arrayParams['message']['recipient_metadata'], $array_recipient_metadata);

            $arrayParams['message']['attachments'] = array();
            //$file = file_get_contents("attachments/".$row_Attachments['Llave_atta'].".ulm");
            $arrarAttrAttachments = array();
            $arrarAttrAttachments['type'] = "application/pdf";
            $arrarAttrAttachments['name'] = "Comprobante de pago ".date('Y-m-d');
            $arrarAttrAttachments['content'] = base64_encode($content);
            array_push($arrayParams['message']['attachments'], $arrarAttrAttachments);

            $arrayParams['message']['async'] = '...';
            //create a new cURL resource
            $cc = new cURL();
            $url = "https://mandrillapp.com/api/1.0/messages/send.json";

            $params= array();    
            array_push($params, json_encode($arrayParams));       
            $data=$base->simpleCurl("POST",false,$url,$params,false);

            //Leer cadena json recibida
            //La cadena recibida tienes que cortarla asta que apetezca puro json
            $data = substr($data, strpos($data, "{"));
            $data = substr($data, 0, strpos($data, "}") + 1);
            $arr = json_decode($data, true);

            echo json_encode($arr);
    }
}






if($_POST['action']=="send_email_bancario"){
        $base= new base();
        $DaoUsuarios= new DaoUsuarios();
        $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        
        $DaoPlanteles= new DaoPlanteles();
        $plantel=$DaoPlanteles->show($usu->getId_plantel());
        
        $DaoAlumnos= new DaoAlumnos();
        $alum=$DaoAlumnos->show($_POST['id']);
        
        $Banco="";
        $Beneficiario="";
        $Convenio="";
        $Transferencias="";
        $RFC="";
        $DaoDatosBancarios=new DaoDatosBancarios();
        $datosB=$DaoDatosBancarios->getDatosBancariosPlantel($plantel->getId());
        foreach($datosB as $dato){
            $Banco=$dato->getBanco();
            $Beneficiario=$dato->getBeneficiario();
            $Convenio=$dato->getConvenio();
            $Transferencias=$dato->getTrasferencias();
            $RFC=$dato->getRFC();   
        }
        

        $asunto="Datos bancarios";
        $mensaje="Fecha: ".date('Y-m-d')."<br><br>

        Estimado <b>".mb_strtoupper($alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM())."</b>:<br><br>

        Por este medio te hacemos llegar tú número de referencia, indispensable para que ".$plantel->getNombre_plantel()." pueda registrar tus pagos en tiempo.<br><br>

        Recuerda que este número es único e intransferible, ya que es la referencia a tu matrícula.<br><br>

        Banco: ".$Banco."<br>
        Beneficiario: ".$plantel->getNombre_plantel()."<br>
        Convenio: ".$Convenio."<br>
        Referencia: <b>".$alum->getRefencia_pago()."</b><br><br>

        Transferencias: ".$Transferencias."<br><br>
        RFC: ".$RFC."<br><br>

        Atentamente,<br><br>

        ".$plantel->getNombre_plantel();


  $arrayData= array();
  $arrayData['Asunto']=$asunto;
  $arrayData['Mensaje']=$mensaje;
  $arrayData['Destinatarios']=array();
  
  $Data= array();
  $Data['email']= $alum->getEmail();
  //$Data['email']= "christian310332@gmail.com";
  $Data['name']= mb_strtoupper($alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM());
  array_push($arrayData['Destinatarios'], $Data);

  echo $base->send_email($arrayData);
}





/******************************************/


if($_POST['action']=="save_monto"){   

    //Ya no se ocuparan los campos de la tabla Pagos_ciclo
    //Cantidad_Pagada,Fecha_pagado,tipo_pago(en duda) tampo(pero checar en reporte de recibos que no afecte)
     
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $DaoCiclos= new DaoCiclos();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoRecargos= new DaoRecargos();
    $DaoPagos= new DaoPagos();
    
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $ofertaAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $alum=$DaoAlumnos->show($ofertaAlumno->getId_alum());
    
    if($_POST['concepto']=="-2"){
       $concepto="Pago a cuenta";
       $Id_cargo=NULL;
       $Id_ciclo_a_usar=NULL;
    }elseif($_POST['concepto']=="-3"){
       $cicloUsar=$DaoCiclos->show($_POST['Id_ciclo_a_usar']);
       $concepto="Pago adelantado para el ciclo ".$cicloUsar->getClave();
       $Id_cargo=NULL;
       $Id_ciclo_a_usar=$_POST['Id_ciclo_a_usar'];
    }elseif($_POST['concepto']>0){
       $cargo=$DaoPagosCiclo->show($_POST['concepto']);
       $concepto=$cargo->getConcepto();
       $Id_cargo=$cargo->getId();
       $Id_ciclo_a_usar=NULL;
    }
    
    if($_POST['Id_pago']>0){
        $Pagos=$DaoPagos->show($_POST['Id_pago']);
        $Pagos->setId_pago_ciclo($Id_cargo);
        $Pagos->setMonto_pp($_POST['monto']);
        $Pagos->setId_usu($_COOKIE['admin/Id_usu']);
        $Pagos->setFecha_pp($_POST['Fecha_deposito']);
        $Pagos->setMetodo_pago($_POST['metodo_pago']);
        $Pagos->setFecha_captura(date('Y-m-d H:i:s'));
        $Pagos->setComentarios($_POST['Comentarios']);
        $Pagos->setConcepto($concepto);
        $Pagos->setId_ciclo_a_usar($Id_ciclo_a_usar);
        $DaoPagos->update($Pagos);  
        $Id_pago=$_POST['Id_pago'];
        //Crear historial
        $TextoHistorial="Edita pago de $". number_format($_POST['monto'],2)." por concepto de ".$concepto." el día ".date('Y-m-d H:i:s')." con número de folio #".$Id_pago.", para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Pagos");
    }else{
        $Folio=$DaoPagos->generarFolio();
        $usu_x=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        
        $DaoOfertasAlumno= new DaoOfertasAlumno();
        $DaoOfertas = new DaoOfertas();
        $DaoEspecialidades = new DaoEspecialidades();
        $DaoOrientaciones = new DaoOrientaciones();

         $ofertaAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
         $oferta=$DaoOfertas->show($ofertaAlumno->getId_ofe());
         $esp=$DaoEspecialidades->show($ofertaAlumno->getId_esp());
         $nombreOri="";
         if($ofertaAlumno->getId_ori()>0){
            $ori=$DaoOrientaciones->show($ofertaAlumno->getId_ori());
            $nombreOri="-".$ori->getNombre();
         }
         
         
        $Pagos=new Pagos();
        $Pagos->setId_pago_ciclo($Id_cargo);
        $Pagos->setMonto_pp($_POST['monto']);
        $Pagos->setId_usu($_COOKIE['admin/Id_usu']);
        $Pagos->setFecha_pp($_POST['Fecha_deposito']);
        $Pagos->setMetodo_pago($_POST['metodo_pago']);
        $Pagos->setFecha_captura(date('Y-m-d H:i:s'));
        $Pagos->setComentarios($_POST['Comentarios']);
        $Pagos->setConcepto($concepto);
        $Pagos->setId_ofe_alum($_POST['Id_ofe_alum']);
        $Pagos->setId_plantel($usu_x->getId_plantel());
        $Pagos->setActivo(1);
        $Pagos->setFolio($Folio);
        $Pagos->setTipoRel("alum");
        $Pagos->setIdRel($alum->getId());
        $Pagos->setDetalleCarrera(mb_strtoupper($oferta->getNombre_oferta()."-".$esp->getNombre_esp()." ".$nombreOri,"UTF-8"));
        $Pagos->setId_ciclo_a_usar($Id_ciclo_a_usar);
        $Id_pago=$DaoPagos->add($Pagos);  
        //Crear historial
        $TextoHistorial="Añade pago de $". number_format($_POST['monto'],2)." por concepto de ".$concepto." el día ".date('Y-m-d H:i:s')." con número de folio #".$Id_pago.", para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Pagos");
    }
    updatePagos($_POST['Id_ofe_alum']);
}





if($_POST['action']=="save_fecha_recargo"){
    $DaoRecargos= new DaoRecargos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $DaoCiclos= new DaoCiclos();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoPagosCiclo= new DaoPagosCiclo();
    
    $Recargos=$DaoRecargos->show($_POST['Id_recargo']);

    $cargo=$DaoPagosCiclo->show($Recargos->getId_pago());
    $alum=$DaoAlumnos->show($cargo->getId_alum());
    $cicloAlumno=$DaoCiclosAlumno->show($cargo->getId_ciclo_alum());
    $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());

    //Crear historial
    $TextoHistorial="Edita fecha de recargo de ".  $Recargos->getFecha_recargo() ." a ".$_POST['fecha_pago'].",para el cargo ".$cargo->getConcepto()." con número de folio #".$cargo->getId().", para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ciclo ".$ciclo->getClave();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Pagos");
    
    $Recargos->setFecha_recargo($_POST['fecha_pago']);
    $DaoRecargos->update($Recargos);
    updatePagos($_POST['id_ofe_alum']);
          
}


if($_POST['action']=="show_box_recargo"){
  ?>
    <div class="box-emergente-form box-recargos">
        <h1>Recargos</h1>
        <p>Fecha<br><input type="date" id="fecha_recargo"/></p>     
        <p>Monto<br><input type="number" id="monto" value="150"/></p> 
        
         <button onclick="save_recargo(<?php echo $_POST['Id_pago']?>,this)">Guardar</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
    <?php
}



if($_POST['action']=="save_recargo"){
    $DaoRecargos= new DaoRecargos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoMiscelaneos= new DaoMiscelaneos();
    $DaoAlumnos= new DaoAlumnos();
    $DaoCiclos= new DaoCiclos();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoPagosCiclo= new DaoPagosCiclo();
    
    $Recargos=new Recargos();
    $Recargos->setFecha_recargo($_POST['Fecha_recargo']);
    $Recargos->setId_pago($_POST['Id_pago']);
    $Recargos->setMonto_recargo($_POST['monto']);
    $DaoRecargos->add($Recargos);
    
    $cargo=$DaoPagosCiclo->show($_POST['Id_pago']);
    $alum=$DaoAlumnos->show($cargo->getId_alum());
    $cicloAlumno=$DaoCiclosAlumno->show($cargo->getId_ciclo_alum());
    $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());

    //Crear historial
    $TextoHistorial="Añade recargo de $".  number_format($_POST['monto'],2) .",para el cargo ".$cargo->getConcepto()." con número de folio #".$_POST['Id_pago'].", para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ciclo ".$ciclo->getClave();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Pagos");
    
    updatePagos($_POST['id_ofe_alum']);
}

if ($_POST['action']=="delete_pago"){
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoPagos= new DaoPagos();

    $pago=$DaoPagos->show($_POST['Id_pago']);
    $ofertaAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $alum=$DaoAlumnos->show($ofertaAlumno->getId_alum());

    //Crear historial
    $TextoHistorial="Elimino el pago por concepto de ".$pago->getConcepto()." con número de folio #".$pago->getId().", para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().",el día ".date('Y-m-d H:i:s');
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Pagos");
    
    $DaoPagos->delete($_POST['Id_pago']);

    updatePagos($_POST['Id_ofe_alum']); 
}



if ($_POST['action']=="delete_cargo"){
    $DaoUsuarios= new DaoUsuarios();
    $DaoMiscelaneos= new DaoMiscelaneos();
    $DaoAlumnos= new DaoAlumnos();
    $DaoCiclos= new DaoCiclos();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoRecargos= new DaoRecargos();

    $PagosCiclo=$DaoPagosCiclo->show($_POST['Id_pago_mis']);
    $texto="Elimino ".$PagosCiclo->getConcepto();
    if($PagosCiclo->getId_mis()>0){
       $miscelaneos= $DaoMiscelaneos->show($PagosCiclo->getId_mis());
       $texto="Elimino el miscelaneo ".$miscelaneos->getNombre_mis();
    }

    $cargo=$DaoPagosCiclo->show($PagosCiclo->getId());
    $alum=$DaoAlumnos->show($cargo->getId_alum());
    $cicloAlumno=$DaoCiclosAlumno->show($cargo->getId_ciclo_alum());
    $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());

    //Crear historial
    $TextoHistorial=$texto." con número de folio #".$PagosCiclo->getId().", para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ciclo ".$ciclo->getClave();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Pagos");
    
    //Actualizamos quien lo elimino
    $PagosCiclo->setDeadCargo(date('Y-m-d H:i:s'));
    $PagosCiclo->setDeadCargoUsu($_COOKIE['admin/Id_usu']);
    $DaoPagosCiclo->update($PagosCiclo);

     updatePagos($_POST['id_ofe_alum']);
}



if($_POST['action']=="delete_recargo"){
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $DaoCiclos= new DaoCiclos();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoRecargos= new DaoRecargos();
    
    $recargos=$DaoRecargos->show($_POST['Id_recargo']);
    $PagosCiclo=$DaoPagosCiclo->show($recargos->getId_pago());
    $cargo=$DaoPagosCiclo->show($PagosCiclo->getId());
    $alum=$DaoAlumnos->show($cargo->getId_alum());
    $cicloAlumno=$DaoCiclosAlumno->show($cargo->getId_ciclo_alum());
    $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());
    
    //Crear historial
    $TextoHistorial="Elimina recargo de $".  number_format($recargos->getMonto_recargo(), 2)." para el cargo con número de folio #".$PagosCiclo->getId().":  ".$cargo->getConcepto().", para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ciclo ".$ciclo->getClave();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Pagos");
    
    $DaoRecargos->delete($_POST['Id_recargo']);
    updatePagos($_POST['id_ofe_alum']);
}


if ($_POST['action'] == "show_miscelaneos") {
    $DaoMiscelaneos= new DaoMiscelaneos();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoCiclos= new DaoCiclos();
    $cicloActual=$DaoCiclos->getActual();
    ?>
    <div class="box-emergente-form box-miscelaneo">
        <h1>Miscelaneos</h1>
        <p>Concepto<br>
            <select onchange="select_miscelaneo()" id="list_mis">
              <option value="0"></option>
              	<?php     
	        foreach($DaoMiscelaneos->showAll() as $mis){
	               ?>
                       <option value="<?php  echo $mis->getId()?>"><?php echo $mis->getNombre_mis()?></option>
	                <?php
	            }
	        ?>
          <select>
          </p>     
        <p>Costo<br><input type="number"  id="costo_miscelaneo" readonly="readonly"/></p> 
        <p>Cantidad<br><input type="number" id="cantidad"/></p>   
        <p>Ciclo<br>
            <select onchange="select_miscelaneo()" id="ciclo-alumno">
              <option value="0"></option>
              	<?php     
                $query= "SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=" . $_POST['Id_ofe_alum'] . " ORDER BY Id_ciclo DESC";
	        foreach($DaoCiclosAlumno->advanced_query($query) as $cicloAlumno){
                        $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());
	               ?>
                         <option value="<?php  echo $cicloAlumno->getId()?>" <?php if($cicloActual->getId()==$ciclo->getId()){ ?> selected="selected" <?php } ?>><?php echo $ciclo->getClave()?></option>
	                <?php
	            }
	        ?> 
            <select>
        </p>  
        <button onclick="save_miscelaneo()">A&ntilde;adir cargo</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
    <?php
}



if($_POST['action']=="select_miscelaneo"){
    $DaoMiscelaneos= new DaoMiscelaneos();
    $mis=$DaoMiscelaneos->show($_POST['Id_mis']);
    echo $mis->getCosto_mis();
}


if($_POST['action']=="save_miscelaneo"){
    $DaoMiscelaneos= new DaoMiscelaneos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $DaoCiclos= new DaoCiclos();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoRecargos= new DaoRecargos();

    $miscelaneos=$DaoMiscelaneos->show($_POST['Id_mis']);
    $nombre_mis=  $miscelaneos->getNombre_mis();
    if(strpos($nombre_mis, "(")!==false){
      $nombre_mis=  substr($nombre_mis, 0,  strpos($nombre_mis, "("));
    }
    
    $Costo_mis=$miscelaneos->getCosto_mis();
    if($_POST['cantidad']>1){
        $texto_cant="(".$_POST['cantidad'].")";
        $Costo_mis=$miscelaneos->getCosto_mis()*$_POST['cantidad'];
    }
    $PagosCiclo= new PagosCiclo();
    $PagosCiclo->setConcepto($nombre_mis." ".$texto_cant);
    $PagosCiclo->setFecha_pago(date('Y-m-d'));
    $PagosCiclo->setMensualidad($Costo_mis);
    $PagosCiclo->setId_ciclo_alum($_POST['Id_ciclo_alum']);
    $PagosCiclo->setTipo_pago("miscelaneo");
    $PagosCiclo->setId_mis($_POST['Id_mis']);
    $PagosCiclo->setId_alum($_POST['id_alum']);
    $Id_pago=$DaoPagosCiclo->add($PagosCiclo); 

    $PagosCiclo=$DaoPagosCiclo->show($Id_pago);
    $cargo=$DaoPagosCiclo->show($PagosCiclo->getId());
    $alum=$DaoAlumnos->show($cargo->getId_alum());
    $cicloAlumno=$DaoCiclosAlumno->show($cargo->getId_ciclo_alum());
    $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());

    //Crear historial
    $TextoHistorial="Añade miscelaneo ".  $miscelaneos->getNombre_mis()." con número de folio #".$PagosCiclo->getId().", para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ciclo ".$ciclo->getClave();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Miscelaneos");
    
    updatePagos($_POST['Id_ofe_alum']);
}



if($_POST['action']=="mostrar_fecha_pago"){
  ?>   
   <div class="box-emergente-form box-feha-cargo">
        <h1>Editar fecha límite</h1>
        <p>Fecha<br><input type="date"  id="fecha_pago"/></p>     
        <button onclick="save_fecha(<?php echo $_POST['Id_pago']?>)">Guardar</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
<?php
}

if($_POST['action']=="mostrar_descuento"){
  ?>
    <div class="box-emergente-form box-feha-descuento">
        <h1>Editar descuento</h1>
        <p>Monto<br><input type="number"  id="descuento"/></p>    
        <button onclick="save_descuento(<?php echo $_POST['Id_pago']?>)">Guardar</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
<?php
}

if($_POST['action']=="mostrar_fecha_recargo"){
  ?>
    <div class="box-emergente-form box-fecha-recargo">
        <h1>Editar fecha</h1>
        <p>Fecha<br><input type="date"  id="fecha_pago"/></p>  
        <button onclick="save_fecha_recargo(<?php echo $_POST['Id_recargo']?>)">Guardar</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
<?php
}



if($_POST['action']=="save_fecha"){
  $DaoPagosCiclo= new DaoPagosCiclo();
  $PagosCiclo=$DaoPagosCiclo->show($_POST['Id_pago']);

  //Crear historial
  $DaoUsuarios= new DaoUsuarios();
  $DaoAlumnos= new DaoAlumnos();
  $DaoCiclos= new DaoCiclos();
  $DaoCiclosAlumno= new DaoCiclosAlumno();
  $cargo=$DaoPagosCiclo->show($_POST['Id_pago']);
  $alum=$DaoAlumnos->show($cargo->getId_alum());
  $cicloAlumno=$DaoCiclosAlumno->show($cargo->getId_ciclo_alum());
  $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());

  $TextoHistorial="Edita la fecha para el cargo con número de folio #".$_POST['Id_pago'].": ".$cargo->getConcepto()." de ".$cargo->getFecha_pago()." a ".$_POST['fecha_pago'].", para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ciclo ".$ciclo->getClave();
  $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Pagos");

  $PagosCiclo->setFecha_pago($_POST['fecha_pago']);
  $DaoPagosCiclo->update($PagosCiclo);

  updatePagos($_POST['id_ofe_alum']);
}



if($_POST['action']=="save_descuento"){
  $DaoPagosCiclo= new DaoPagosCiclo();
  $PagosCiclo=$DaoPagosCiclo->show($_POST['Id_pago']);

  //Crear historial
  $DaoUsuarios= new DaoUsuarios();
  $DaoAlumnos= new DaoAlumnos();
  $DaoCiclos= new DaoCiclos();
  $DaoCiclosAlumno= new DaoCiclosAlumno();
  $cargo=$DaoPagosCiclo->show($_POST['Id_pago']);
  $alum=$DaoAlumnos->show($cargo->getId_alum());
  $cicloAlumno=$DaoCiclosAlumno->show($cargo->getId_ciclo_alum());
  $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());
  
  $descuento=  number_format($cargo->getDescuento(),2,",",".");
  if($cargo->getDescuento()==null){
     $descuento=0; 
  }
  $nuevoDescuento=  number_format($_POST['descuento'], 2,",",".");

  $TextoHistorial="Edita el descuento para el cargo con número de folio #".$_POST['Id_pago'].":  ".$cargo->getConcepto()."  de $".$descuento." a $".$nuevoDescuento.", para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ciclo ".$ciclo->getClave();
  $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Descuentos");

  $PagosCiclo->setDescuento($_POST['descuento']);
  $PagosCiclo->setUsuCapturaDescuento($_COOKIE['admin/Id_usu']);
  $PagosCiclo->setFechaCapturaDescuento(date('Y-m-d H:i:s'));
  $DaoPagosCiclo->update($PagosCiclo);
   updatePagos($_POST['id_ofe_alum']);
}



function updatePagos($Id_ofe_alum){
    $DaoUsuarios= new DaoUsuarios();
    $_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $perm=array();   
    foreach($DaoUsuarios->getPermisosUsuario($_usu->getId()) as $k=>$v){
        $perm[$v['Id_per']]=1;
    }
    
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoPagos = new DaoPagos();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoAlumnos = new DaoAlumnos();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoCiclos = new DaoCiclos();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoRecargos = new DaoRecargos();
    $DaoMetodosPago = new DaoMetodosPago();
    $DaoPaquetesDescuentosAlumno= new DaoPaquetesDescuentosAlumno();

    $oferta_alumno = $DaoOfertasAlumno->show($Id_ofe_alum);
    $alum = $DaoAlumnos->show($oferta_alumno->getId_alum());
    $oferta = $DaoOfertas->show($oferta_alumno->getId_ofe());
    $esp = $DaoEspecialidades->show($oferta_alumno->getId_esp());
    if ($oferta_alumno->getId_ori() > 0) {
        $ori = $DaoOrientaciones->show($oferta_alumno->getId_ori());
        $nombre_ori = $ori->getNombre();
    }

    $primer_ciclo = $DaoOfertasAlumno->getFirstCicloOfertaSinCiclo($Id_ofe_alum);
    $ultimo_ciclo = $DaoOfertasAlumno->getLastCicloOfertaSinCiclo($Id_ofe_alum);

    $Opcion_pago = "";
    if ($oferta_alumno->getOpcionPago() == 1) {
        $Opcion_pago = "Plan por materias";
    } else {
        $Opcion_pago = "Plan completo";
    }

    $SaldoPagos = $DaoPagos->getTotalPagosACuenta($oferta_alumno->getId());
    ?>
   <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-university"></i> Estado de cuenta</h1>
                </div>
                <div class="seccion">
                    <table  id="info_alumn">
                        <tbody><tr><th style="padding-bottom: 15px;text-align: left;">
                                    <font size="3">DATOS DEL ESTUDIANTE</font>
                                </th></tr>
                            <tr><td>
                                    <table width="100%">
                                        <tbody><tr>
                                                <th ><font>C&oacute;digo:</font></th>
                                                <td><font><?php echo $alum->getMatricula() ?></font></td>
                                                <th ><font>Nombre:</font></th>
                                                <td colspan="5"><font><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></font></td>
                                            </tr>
                                            <tr>
                                                <th><font>Nivel:</font></th>
                                                <td><font><?php echo $oferta->getNombre_oferta(); ?></font></td>
                                                <th><font>Admisi&oacute;n:</font></th>
                                                <td><?php echo "" ?></font></td>
                                                <th><font>Último ciclo:</font></th>
                                                <td><font><?php echo ""; ?></font></td>
                                            </tr>
                                            <tr>
                                                <th><font>Carrera:</font></th>
                                                <td colspan="7"><font><?php echo $esp->getNombre_esp(); ?></font></td>
                                            </tr>
                                            <tr>
                                                <th><font>Orientaci&oacute;n:</font></th>
                                                <td><font color="navy" face="arial" size="2"><?php echo $nombre_ori; ?></font></td>
                                                <th><font>Opci&oacute;n de pago</font></th>
                                                <td><font color="navy" face="arial" size="2"><?php echo $Opcion_pago; ?></font></td>
                                            </tr>
                                        </tbody></table>
                                </td></tr>
                        </tbody>
                    </table>
                </div>
                <table id="estado-cuenta">
                    <tr>
                        <td id="seccion-cargos">
                            <?php
                            if (isset($perm['39'])) {
                                //Verificamos si ya se generaron los demas cargos
                                if (count($DaoPagosCiclo->getCargosCiclo($primer_ciclo['Id_ciclo_alum'])) > 1 || ($esp->getNum_pagos() == 1 && count($DaoPagosCiclo->getCargosCiclo($primer_ciclo['Id_ciclo_alum'])) >= 1)) {
                                    ?>
                                     <button id="add_pago_miscelaneo" class="boton-normal" onclick="show_miscelaneos()"><i class="fa fa-plus-square"></i> A&ntilde;adir miscelaneo</button>
                                     <button id="buttonBancario" class="boton-normal" onclick="send_email_bancario()"><i class="fa fa-paper-plane"></i> Enviar datos bancarios</button>
                                     <button id="buttonBancario" class="boton-normal" onclick="show_box_paquete_descuento()"><i class="fa fa-plus-square"></i> A&ntilde;adir paquete descuento</button>
                                    <?php
                                } else {
                                    $class = "red";
                                    if ($DaoPagosCiclo->verificarPagoInscripcion($Id_ofe_alum) == 1) {
                                        $class = "green";
                                    }
                                    if ($oferta_alumno->getOpcionPago() == 2) {
                                        $texto = "2.- Presionar bot&oacute;n inscribir para generar los cargos del per&iacute;odo";
                                    } else {
                                        $texto = "2.- Asignar las materias que cursara en el presente ciclo y presionar el bot&oacute;n de generar mensualidades";
                                    }
                                    ?>
                                    <p class="<?php echo $class; ?>">1.- Pagar cargo por inscripci&oacute;n</p>
                                    <p class="red"><?php echo $texto ?></p>
                                    <?php
                                }
                            }
                            $totalCargosOferta = 0;
                            $totalBecasOferta = 0;
                            $totalDescuentosOferta = 0;
                            $totalPaquetesDescuento=0;
                            $i = 0;
                            
                            foreach ($DaoCiclosAlumno->getCiclosOferta($oferta_alumno->getId()) as $cicloAlumno) {
                                $ciclo = $DaoCiclos->show($cicloAlumno->getId_ciclo());
                                
                                //Si el alumno tieneun paquete de descuento en el ciclo en tonces descontar a los cargos
                                $descuentoPaqueteCiclo=$DaoPaquetesDescuentosAlumno->getPaqueteDescuentoCicloAlumno($cicloAlumno->getId_ciclo(),$oferta_alumno->getId());
                                ?>
                                <table class="table" id-ciclo="<?php echo $cicloAlumno->getId_ciclo() ?>">
                                    <thead>
                                        <tr>
                                            <td colspan="11" class="clave-ciclo">Cargos del Ciclo <?php echo $ciclo->getClave() ?></td>
                                        </tr>
                                        <tr>
                                            <td class="td-center">#</td>
                                            <td>Concepto</td>
                                            <td class="td-center" width="65px;">Fecha límite</td>
                                            <td class="td-center">Monto</td>
                                            <td class="td-center">Descuento</td>
                                            <td class="td-center">Aplica Desct.</td>
                                            <td class="td-center">Paquete Desc. <?php echo $descuentoPaqueteCiclo->getPorcentaje()."%"?></td>
                                            <td class="td-center">Aplica Paquete Desc.</td>
                                            <td class="td-center">Beca <?php echo $cicloAlumno->getPorcentaje_beca(); ?> %</td>
                                            <td class="td-center">Cantidad pagada</td>
                                            <td class="td-center">Adeudo</td>
                                            <td class="td-center"></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $mensualidad = 0;
                                        $Descuento = 0;
                                        $Adeudo = 0;
                                        $miscelaneos = 0;
                                        $totalAdeudo = 0;
                                        $totalrecargos = 0;
                                        $totalmiscelaneos = 0;
                                        $totalBeca = 0;
                                        $totalPagado = 0;
                                        //Pagos que se usaran solo en el ciclo que se eligio
                                        if($DaoPagos->getTotalPagosAdelantados($cicloAlumno->getId_ciclo(),$alum->getId(),$Id_ofe_alum)>0){
                                           $pagoAdelantado=$DaoPagos->getTotalPagosAdelantados($cicloAlumno->getId_ciclo(), $alum->getId(),$Id_ofe_alum); 
                                           $SaldoPagos+=$pagoAdelantado;
                                        }
                                        foreach ($DaoPagosCiclo->getCargosCiclo($cicloAlumno->getId()) as $cargo) {
                                            //Recargos
                                            $recargos = 0;
                                            foreach ($DaoRecargos->getRecargosPago($cargo->getId()) as $recargo) {
                                                $recargos+=$recargo->getMonto_recargo();
                                            }
                                            //Miscelaneos
                                            if ($cargo->getTipo_pago() == "miscelaneo") {
                                                $miscelaneos = $cargo->getMensualidad();
                                            }
                                            
                                            $mensualidad+= $cargo->getMensualidad() + $recargos;
                                            $Descuento+=$cargo->getDescuento();
                                            
                                            //$TotalCargo = round($cargo->getMensualidad() - $cargo->getDescuento() - $cargo->getDescuento_beca() + $recargos);
                                            //Esta linea es para saber si el pago se dirigio a un cargo en especifico,o se fue directamente a cuenta
                                            //Es decir no se escogio ningun cargo y fue entonces uno adelantado
                                            $text="";
                                            $cantidadPagada=$DaoPagos->getTotalPagosCargo($cargo->getId());
                                            if($cantidadPagada>0){
                                               $text="Pagado por un cargo";
                                               $TotalCargo=round($cargo->getMensualidad() - $cargo->getDescuento() - $cargo->getDescuento_beca() + $recargos);
                                               
                                               //Si tiene algun descuento por paquete
                                               $decuentoPorPaquete=0;
                                               if($descuentoPaqueteCiclo->getId()>0 && $descuentoPaqueteCiclo->getPorcentaje()>0){
                                                    if($cargo->getTipo_pago()=="pago"){
                                                       $decuentoPorPaquete=($TotalCargo*$descuentoPaqueteCiclo->getPorcentaje())/100; 
                                                    }else{
                                                        $decuentoPorPaquete=0;
                                                    }
                                                    $totalPaquetesDescuento+=$decuentoPorPaquete;
                                               }
                                               $TotalCargo=$TotalCargo-$decuentoPorPaquete;
                                               $Deuda=$TotalCargo-$cantidadPagada;

                                               if($TotalCargo>0 && $SaldoPagos>0){
                                                  $MontoPagado = $TotalCargo;
                                                  $SaldoPagos=$SaldoPagos-$Deuda;
                                                  $Adeudo=0;
                                               }else{
                                                  $MontoPagado = $cantidadPagada;
                                                  $Adeudo = $Deuda;  
                                               }
                                           
                                               

                                            }else{
                                               //Esta linea espor si fue pago adelantado
                                               $text="Pago adelantado";
                                               $TotalCargo = round($cargo->getMensualidad() - $cargo->getDescuento() - $cargo->getDescuento_beca() + $recargos);
                                               //Si tiene algun descuento por paquete
                                               $decuentoPorPaquete=0;
                                               if($descuentoPaqueteCiclo->getId()>0 && $descuentoPaqueteCiclo->getPorcentaje()>0){
                                                    if($cargo->getTipo_pago()=="pago"){
                                                       $decuentoPorPaquete=($TotalCargo*$descuentoPaqueteCiclo->getPorcentaje())/100; 
                                                    }else{
                                                        $decuentoPorPaquete=0;
                                                    }
                                                    $totalPaquetesDescuento+=$decuentoPorPaquete;
                                               }
                                               $TotalCargo=$TotalCargo-$decuentoPorPaquete;
                                                if ($SaldoPagos >= $TotalCargo) {
                                                    $SaldoPagos = $SaldoPagos - $TotalCargo;
                                                    $MontoPagado=$TotalCargo;
                                                    $Adeudo = 0;
                                                } elseif ($SaldoPagos < $TotalCargo) {
                                                    //Si el saldo es menor al costo del cargo 
                                                    $MontoPagado = $SaldoPagos;
                                                    $Adeudo = $TotalCargo - $MontoPagado;
                                                    $SaldoPagos = 0;
                                                }
                                            }
                                            
                                            $totalPagado+=$MontoPagado;
                                            $totalAdeudo+=$Adeudo;
                                            $totalrecargos+=$recargos;
                                            $totalmiscelaneos+=$miscelaneos;
                                            $totalBeca+=$cargo->getDescuento_beca();

                                            //Fondo
                                            $classAdeudo = "";
                                            //Esconder boton editar fecha y adeudos
                                            $style_edit = '';
                                            if ($Adeudo <= 0) {
                                                $style_edit = 'style="visibility: hidden"';
                                                $classAdeudo = 'adeudo';
                                                $cargo->setPagado(1);
                                                $DaoPagosCiclo->update($cargo);
                                            }else{
                                                $cargo->setPagado(0);
                                                $DaoPagosCiclo->update($cargo);  
                                            }


                                            $NombreUsuDescuento="";
                                            if($cargo->getUsuCapturaDescuento()>0){
                                            $aplicaDescuento=$DaoUsuarios->show($cargo->getUsuCapturaDescuento());
                                            $NombreUsuDescuento=$aplicaDescuento->getNombre_usu()." ".$aplicaDescuento->getApellidoP_usu();
                                            }
                                            
                                            $NombreUsuPaqueteDescuento="";
                                            if($descuentoPaqueteCiclo->getId_usu_captura()>0){
                                            $aplicaPaqueteDescuento=$DaoUsuarios->show($descuentoPaqueteCiclo->getId_usu_captura());
                                            $NombreUsuPaqueteDescuento=$aplicaPaqueteDescuento->getNombre_usu()." ".$aplicaPaqueteDescuento->getApellidoP_usu();
                                            }
                                            
                                            ?>
                                            <tr  class="<?php echo $classAdeudo; ?>" id-pago="<?php echo $cargo->getId() ?>">
                                                <td class="td-center"><?php echo $i + 1 ?></td>
                                                <td><?php echo $cargo->getConcepto() ?></td>
                                                <td class="td-center"><?php echo $cargo->getFecha_pago() ?> <i class="fa fa-pencil-square-o" onclick="mostrar_fecha_pago(<?php echo $cargo->getId() ?>,<?php echo $i + 1 ?>)" title="Editar fecha limite de pago"></i></td>
                                                <td class="td-center"><?php echo "$" . number_format($cargo->getMensualidad(), 2) ?></td>
                                                <td class="td-center"><?php echo "$" . number_format($cargo->getDescuento(), 2) ?> 
                                                    <?php
                                                    if (isset($perm['37'])) {
                                                        ?>
                                                    <i class="fa fa-pencil-square-o" onclick="mostrar_descuento(<?php echo $cargo->getId() ?>,<?php echo $i + 1 ?>)" title="Editar descuento"></i>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo $NombreUsuDescuento;?></td>
                                                <td>$<?php echo  number_format($decuentoPorPaquete, 2);?></td>
                                                <td><?php echo $NombreUsuPaqueteDescuento;?></td>
                                                <td class="td-center"><?php echo "$" . number_format($cargo->getDescuento_beca(), 2) ?></td>
                                                <td class="td-center"><?php echo "$" . number_format($MontoPagado, 2) ?></td>
                                                <td class="td-center"><?php echo "$" . number_format($Adeudo, 2) ?></td>
                                                <td class="right">
                                                    <!--<i class="fa fa-history" onclick="mostrar_log_seguimiento(<?php echo $cargo->getId(); ?>)" title="Historial de seguimiento"></i>-->
                                                    <i class="fa fa-plus-square" onclick="show_box_recargo(<?php echo $cargo->getId(); ?>)" title="Añadir recargo"></i>
                                                    <?php
                                                    if (isset($perm['69'])) {
                                                        ?>
                                                        <i class="fa fa-trash" onclick="delete_cargo(<?php echo $cargo->getId(); ?>)" title="Eliminar cargo"></i>
                                                        <?php
                                                    }
                                                    //echo $SaldoPagos." ".$MontoPagado;
                                                    ?>
                                                </td>
                                            </tr>
                                            
                                            
                                            <?php
                                            $query = "SELECT * FROM Recargos_pago WHERE Id_pago=" . $cargo->getId() . " AND Fecha_recargo<='" . date('Y-m-d') . "' ORDER BY Fecha_recargo ASC";
                                            foreach ($DaoRecargos->advanced_query($query) as $recargo) {
                                                ?>
                                                <tr class="recargo">
                                                    <td></td>
                                                    <td>Recargo</td>
                                                    <td class="td-center"><?php echo $recargo->getFecha_recargo() ?> <i class="fa fa-pencil-square-o" onclick="mostrar_fecha_recargo(<?php echo $recargo->getId() ?>)"></i> </td>
                                                    <td class="td-center">$<?php echo number_format($recargo->getMonto_recargo(), 2) ?></td>
                                                    <td colspan="7"></td>
                                                    <td class="right">
                                                        <?php
                                                        //if ($cargo->getCantidad_Pagada() < $Adeudo) {
                                                            if (isset($perm['38'])) {
                                                                ?>
                                                                <i class="fa fa-trash" onclick="delete_recargo(<?php echo $recargo->getId() ?>)" title="Eliminar recargo"></i>
                                                                <?php
                                                            }
                                                        //}
                                                        ?>

                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            $i++;
                                        }
                                        $totalCargosOferta+=$mensualidad;
                                        $totalBecasOferta+=$totalBeca;
                                        $totalDescuentosOferta+=$Descuento;
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3"><b>Total</b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($mensualidad), 2) ?></b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($Descuento), 2) ?></b></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($totalBeca), 2) ?></b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($totalPagado), 2) ?></b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($totalAdeudo), 2) ?></b></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b>Total Miscelaneos</b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($miscelaneos), 2) ?></b></td>
                                            <td colspan="8"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b>Total Regargos</b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($totalrecargos), 2) ?></b></td>
                                            <td class="td-center" colspan="8"></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <?php
                            }
                            ?>
                        </td>
                        <td id="seccion-pagos">
                            <button class="boton-normal" id="mostrar-box-pago" onclick="mostrar_pago()"><i class="fa fa-plus-square"></i> Añadir pago</button>
                            <table class="table" id-ciclo="<?php echo $cicloAlumno->getId_ciclo() ?>">
                                <thead>
                                    <tr>
                                        <td colspan="9" class="clave-ciclo">Historial de pagos</td>
                                    </tr>
                                    <tr>

                                        <td class="td-center">Folio</td>
                                        <td class="td-center">Fecha de captura</td>
                                        <td class="td-center">Fecha de pago</td>
                                        <td>Concepto</td>
                                        <td class="td-center">Monto</td>
                                        <td class="td-center">Método de pago</td>
                                        <td class="td-center">Usuario</td>
                                        <td>Comentarios</td>
                                        <td class="td-center">Acciones</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $totalPagado = 0;
                                    $i = 1;
                                    foreach ($DaoPagos->getPagosOfertaAlumno($oferta_alumno->getId()) as $pago) {
                                        $usuCapturo = $DaoUsuarios->show($pago->getId_usu());
                                        $metodo = $DaoMetodosPago->show($pago->getMetodo_pago());
                                        $totalPagado+=$pago->getMonto_pp();
                                        $ciclo=$DaoCiclos->getCicloPorFecha($pago->getFecha_captura());
                                        ?>
                                        <tr  id-pago="<?php echo $pago->getId() ?>">
                                            <td class="td-center"><?php echo $pago->getFolio() ?></td>
                                            <td class="td-center"><?php echo $pago->getFecha_captura() ?></td>
                                            <td class="td-center"><?php echo $pago->getFecha_pp() ?></td>
                                            <td><?php echo $pago->getConcepto()?></td>
                                            <td class="td-center"><?php echo "$" . number_format($pago->getMonto_pp(), 2) ?></td>
                                            <td class="td-center"><?php echo $metodo->getNombre() ?></td>
                                            <td class="td-center"><?php echo $usuCapturo->getNombre_usu() . " " . $usuCapturo->getApellidoP_usu() ?></td>
                                            <td><?php echo $pago->getComentarios() ?></td>
                                            <td class="menu td-center">
                                                <i class="fa fa-pencil-square-o" onclick="mostrar_pago(<?php echo $pago->getId()?>)" title="Editar pago"></i>
                                                <?php
                                                if (isset($perm['69'])) {
                                                    ?>
                                                    <i class="fa fa-trash" onclick="delete_pago(<?php echo $pago->getId(); ?>)" title="Eliminar pago"></i>
                                                    <?php
                                                }
                                                ?>
                                                <i class="fa fa-file-pdf-o" onclick="mostrar_recibo(<?php echo $pago->getId(); ?>,<?php echo $ciclo->getId(); ?>)" title="Mostrar recibo"></i>
                                                <i class="fa fa-paper-plane" onclick="send_recibo(<?php echo $pago->getId() ?>,<?php echo $ciclo->getId(); ?>, this)"title="Enviar recibo"></i>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td colspan="8" class="clave-ciclo">Totales</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="3"><b>Total cargos</b></td>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($totalCargosOferta), 2) ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Total pagado</b></td>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($totalPagado), 2) ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Total descuento</b></td>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($totalDescuentosOferta), 2) ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Total beca</b></td>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($totalBecasOferta), 2) ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Adeudo</b></td>
                                        <?php
                                        $z = ($totalCargosOferta - $totalPagado - $totalDescuentosOferta - $totalBecasOferta- $totalPaquetesDescuento)
                                        ?>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($z), 2) ?></b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td id="column_two"></td>
    </tr>
    <?php
}


/*
if($_POST['action']=="mostrar_pago"){
    $text="Nuevo";
    $DaoPagos= new DaoPagos();
    $DaoMetodosPago= new DaoMetodosPago();
    $monto="";
    $fecha="";
    $comentarios="";
    $concepto="";
    $metodox="";
    if($_POST['Id_pago']>0){
       $text="Editar";
       $pago=$DaoPagos->show($_POST['Id_pago']);
       $monto=$pago->getMonto_pp();
       $fecha=$pago->getFecha_pp();
       $comentarios=$pago->getComentarios();
       $concepto=$pago->getConcepto();
       $metodox= $pago->getMetodo_pago();
    }
  ?>
  
   <div class="box-emergente-form box-pagos">
        <h1><?php echo $text?> pago</h1>
        <p>Concepto<br><input type="text"  id="concepto"  value="<?php echo $concepto?>"/></p>  
        <p>Monto<br><input type="number"  id="monto_cubrir"  value="<?php echo $monto?>"/></p>     
        <p>Fecha de dep&oacute;sito<br><input type="date"  id="fecha_deposito"  value="<?php echo $fecha?>"/></p>  
        <p>M&eacute;todo de pago<br>
            <select id="metodo_pago">
              <option value="0"></option>
            <?php
            foreach($DaoMetodosPago->showAll() as $metodo){
                ?>
              <option value="<?php echo $metodo->getId(); ?>" <?php if($metodox==$metodo->getId()){ ?> selected="selected" <?php } ?>><?php echo $metodo->getNombre() ?></option>
                <?php  
            }
            ?>
            </select>
        </p> 
        <p>Comentarios<br><input type="text" id="Comentarios" value="<?php echo $comentarios?>"/></p>  
        <?php
        $Id_pago=0;
        if($_POST['Id_pago']>0){
           $Id_pago=$_POST['Id_pago'];
        }
        ?>
        <button onclick="save_monto(<?php echo $Id_pago?>,this)">Confirmar pago</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
<?php
}
 */


if($_POST['action']=="mostrar_pago"){
    $text="Nuevo";
    $DaoPagos= new DaoPagos();
    $DaoMetodosPago= new DaoMetodosPago();
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoAlumnos = new DaoAlumnos();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoCiclos = new DaoCiclos();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoRecargos = new DaoRecargos();
    $DaoPaquetesDescuentosAlumno= new DaoPaquetesDescuentosAlumno();
            
    $monto="";
    $fecha="";
    $comentarios="";
    $concepto="";
    $metodox="";
    $Id_pago_ciclo="";
    $Id_ciclo_a_usar="";
    $Id_d=0;
    if(isset($_POST['Id_pago']) && $_POST['Id_pago']>0){
       $text="Editar";
       $pago=$DaoPagos->show($_POST['Id_pago']);
       $monto=$pago->getMonto_pp();
       $fecha=$pago->getFecha_pp();
       $comentarios=$pago->getComentarios();
       $concepto=$pago->getConcepto();
       $metodox= $pago->getMetodo_pago();
       $Id_pago_ciclo=$pago->getId_pago_ciclo();
       $Id_ciclo_a_usar=$pago->getId_ciclo_a_usar();
       $Id_d=$_POST['Id_pago'];
    }
  ?>
  
   <div class="box-emergente-form box-pagos">
        <h1><?php echo $text?> pago</h1>
        <p>Concepto<br>
            <select id="concepto" onchange="updateMontoCargo()">
                <option value="-1"></option>
                <option value="-2" <?php if(strlen($Id_pago_ciclo)==0 && strlen($Id_ciclo_a_usar)==0 && $Id_d>0){ ?> selected="selected" <?php } ?>>Pago a cuenta</option>
                <option value="-3" <?php if($Id_ciclo_a_usar>0){ ?> selected="selected" <?php } ?>>Pago adelantado</option>
            <?php
            $oferta_alumno = $DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
            $SaldoPagos = $DaoPagos->getTotalPagosACuenta($oferta_alumno->getId());
            $i = 0;
            
            foreach ($DaoCiclosAlumno->getCiclosOferta($oferta_alumno->getId()) as $cicloAlumno) {
                $ciclo = $DaoCiclos->show($cicloAlumno->getId_ciclo());
                //Si el alumno tieneun paquete de descuento en el ciclo en tonces descontar a los cargos
                $descuentoPaqueteCiclo=$DaoPaquetesDescuentosAlumno->getPaqueteDescuentoCicloAlumno($cicloAlumno->getId_ciclo(),$oferta_alumno->getId());
                                
                $mensualidad = 0;
                $Descuento = 0;
                $Adeudo = 0;
                $miscelaneos = 0;
                $totalAdeudo = 0;
                $totalrecargos = 0;
                $totalmiscelaneos = 0;
                $totalBeca = 0;
                $totalPagado = 0;
                //Pagos que se usaran solo en el ciclo que se eligio
                if($DaoPagos->getTotalPagosAdelantados($cicloAlumno->getId_ciclo(),$oferta_alumno->getId_alum(),$_POST['Id_ofe_alum'])>0){
                   $pagoAdelantado=$DaoPagos->getTotalPagosAdelantados($cicloAlumno->getId_ciclo(),$oferta_alumno->getId_alum(),$_POST['Id_ofe_alum']);  
                   $SaldoPagos+=$pagoAdelantado;
                }
                foreach ($DaoPagosCiclo->getCargosCiclo($cicloAlumno->getId()) as $cargo) {
                        //Recargos
                        $recargos = 0;
                        foreach ($DaoRecargos->getRecargosPago($cargo->getId()) as $recargo) {
                            $recargos+=$recargo->getMonto_recargo();
                        }
                        //Miscelaneos
                        if ($cargo->getTipo_pago() == "miscelaneo") {
                            $miscelaneos = $cargo->getMensualidad();
                        }

                        $mensualidad+= $cargo->getMensualidad() + $recargos;
                        $Descuento+=$cargo->getDescuento();

                        //$TotalCargo = round($cargo->getMensualidad() - $cargo->getDescuento() - $cargo->getDescuento_beca() + $recargos);
                        //Esta linea es para saber si el pago se dirigio a un cargo en especifico,o se fue directamente a cuenta
                        //Es decir no se escogio ningun cargo y fue entonces uno adelantado
                        $text="";
                        $cantidadPagada=$DaoPagos->getTotalPagosCargo($cargo->getId());
                        if($cantidadPagada>0){
                           $text="Pagado por un cargo";
                           $TotalCargo=round($cargo->getMensualidad() - $cargo->getDescuento() - $cargo->getDescuento_beca() + $recargos);

                           //Si tiene algun descuento por paquete
                           $decuentoPorPaquete=0;
                           if($descuentoPaqueteCiclo->getId()>0 && $descuentoPaqueteCiclo->getPorcentaje()>0){
                                $decuentoPorPaquete=($TotalCargo*$descuentoPaqueteCiclo->getPorcentaje())/100;
                                $totalPaquetesDescuento+=$decuentoPorPaquete;
                           }
                           $TotalCargo=$TotalCargo-$decuentoPorPaquete;
                           $Deuda=$TotalCargo-$cantidadPagada;

                           if($TotalCargo>0 && $SaldoPagos>0){
                              $MontoPagado = $TotalCargo;
                              $SaldoPagos=$SaldoPagos-$Deuda;
                              $Adeudo=0;
                           }else{
                              $MontoPagado = $cantidadPagada;
                              $Adeudo = $Deuda;  
                           }



                        }else{
                           //Esta linea espor si fue pago adelantado
                           $text="Pago adelantado";
                           $TotalCargo = round($cargo->getMensualidad() - $cargo->getDescuento() - $cargo->getDescuento_beca() + $recargos);
                           //Si tiene algun descuento por paquete
                           $decuentoPorPaquete=0;
                           if($descuentoPaqueteCiclo->getId()>0 && $descuentoPaqueteCiclo->getPorcentaje()>0){
                                $decuentoPorPaquete=($TotalCargo*$descuentoPaqueteCiclo->getPorcentaje())/100;
                                $totalPaquetesDescuento+=$decuentoPorPaquete;
                           }
                           $TotalCargo=$TotalCargo-$decuentoPorPaquete;
                            if ($SaldoPagos >= $TotalCargo) {
                                $SaldoPagos = $SaldoPagos - $TotalCargo;
                                $MontoPagado=$TotalCargo;
                                $Adeudo = 0;
                            } elseif ($SaldoPagos < $TotalCargo) {
                                //Si el saldo es menor al costo del cargo 
                                $MontoPagado = $SaldoPagos;
                                $Adeudo = $TotalCargo - $MontoPagado;
                                $SaldoPagos = 0;
                            }
                        }
                    $i++;
                    if ($Adeudo>0 || $Id_pago_ciclo==$cargo->getId()) {
                    ?>
                        <option value="<?php echo $cargo->getId()?>" adeudo="<?php echo $Adeudo;?>" <?php if($Id_pago_ciclo==$cargo->getId()){ ?> selected="selected" <?php } ?>>#<?php echo $i." - ".$cargo->getConcepto()." (Adeudo $".  number_format($Adeudo,2).")" ?> </option>
                    <?php
                    }
                }
            }
            
              ?>
            </select>
        </p>  
        <p id="p-ciclo-usar" <?php if($Id_ciclo_a_usar>0){ ?> style="display: block;" <?php } ?>>Ciclo a cubrir<br>
            <select id="Id_ciclo_a_usar">
                <option value="0"></option>
                <?php
                  foreach ($DaoCiclos->getCiclosFuturos() as $ciclo){
                 ?>
                <option value="<?php echo $ciclo->getId()?>" <?php if($Id_ciclo_a_usar==$ciclo->getId()){ ?> selected="selected" <?php } ?>><?php echo $ciclo->getClave()?></option>
                <?php
                  }
                ?>
            </select>
        </p>
        <p>Monto<br><input type="number"  id="monto_cubrir"  value="<?php echo $monto?>" readonly="readonly"/></p>     
        <p>Fecha de dep&oacute;sito<br><input type="date"  id="fecha_deposito"  value="<?php echo $fecha?>"/></p>  
        <p>M&eacute;todo de pago<br>
            <select id="metodo_pago">
              <option value="0"></option>
            <?php
            foreach($DaoMetodosPago->showAll() as $metodo){
                ?>
              <option value="<?php echo $metodo->getId(); ?>" <?php if($metodox==$metodo->getId()){ ?> selected="selected" <?php } ?>><?php echo $metodo->getNombre() ?></option>
                <?php  
            }
            ?>
            </select>
        </p> 
        <p>Comentarios<br><input type="text" id="Comentarios" value="<?php echo $comentarios?>"/></p>  
        <?php
        $Id_pago=0;
        if(isset($_POST['Id_pago']) && $_POST['Id_pago']>0){
           $Id_pago=$_POST['Id_pago'];
        }
        ?>
        <button onclick="save_monto(<?php echo $Id_pago?>,this)">Confirmar pago</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
<?php
}


if($_POST['action']=="show_box_paquete_descuento"){
    $DaoPaquetesDescuentos= new DaoPaquetesDescuentos();
    $DaoCiclos= new DaoCiclos();
    $Id=0;
    if($_POST['Id']>0){
       $Id =$_POST['Id'];
    }
  ?>
    <div class="box-emergente-form box-paquetes">
        <h1>Descuento</h1>   
        <p>Tipo de descuento<br>
            <select id="paquete" onchange="updateCampoDescuento()">
                <option value="0"></option>
           <?php
             foreach ($DaoPaquetesDescuentos->showAll() as $paquete){
           ?>
                <option value="<?php echo $paquete->getId()?>" <?php if($_POST['Id_paq']==$paquete->getId()){ ?> selected="selected" <?php } ?> porcentaje="<?php echo $paquete->getPorcentaje()?>"> <?php echo $paquete->getNombre()?></option>
                <?php
             }
             ?>
            </select>
        </p> 
        <p>Porcentaje<br><input type="number" id="porcentajePaquete" readonly="readonly"/></p> 
        <p>Ciclo<br>
          <select id="ciclo">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>" <?php if($_POST['Id_ciclo']==$v->getId()){ ?> selected="selected" <?php } ?>><?php echo $v->getClave(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <button onclick="save_paquete_descuento(<?php echo $Id?>,this)">Guardar</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
    <?php
}


if($_POST['action']=="save_paquete_descuento"){
   $DaoPaquetesDescuentosAlumno= new DaoPaquetesDescuentosAlumno();
   $DaoPaquetesDescuentos= new DaoPaquetesDescuentos();
   $DaoOfertasAlumno= new DaoOfertasAlumno();
   $DaoAlumnos= new DaoAlumnos();
   $DaoUsuarios= new DaoUsuarios();
   
   $paquete=$DaoPaquetesDescuentos->show($_POST['Id_paq']);
   $ofertaAlumno=$DaoOfertasAlumno->show($_POST['id_ofe_alum']);
   $alum=$DaoAlumnos->show($ofertaAlumno->getId_alum());
   
   if($_POST['Id']>0){
       $PaquetesDescuentosAlumno=$DaoPaquetesDescuentoAlumno->show($_POST['Id']);
       $TextoHistorial="Edita descuento de ".$PaquetesDescuentosAlumno->getPorcentaje()."% a ".$_POST['Porcentaje']."% por ".$paquete->getNombre()." para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", el día ".date('Y-m-d H:i:s');
       $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Descuentos");
       
       $PaquetesDescuentosAlumno->setId_ciclo($_POST['Id_ciclo']);
       $PaquetesDescuentosAlumno->setId_paq($_POST['Id_paq']);
       $PaquetesDescuentosAlumno->setPorcentaje($_POST['Porcentaje']);
       $DaoPaquetesDescuentosAlumno->update($PaquetesDescuentosAlumno);
   }else{
       $PaquetesDescuentosAlumno= new PaquetesDescuentosAlumno();
       $PaquetesDescuentosAlumno->setFechaCaptura(date('Y-m-d H:i:s'));
       $PaquetesDescuentosAlumno->setId_ciclo($_POST['Id_ciclo']);
       $PaquetesDescuentosAlumno->setId_ofe_alum($_POST['id_ofe_alum']);
       $PaquetesDescuentosAlumno->setId_paq($_POST['Id_paq']);
       $PaquetesDescuentosAlumno->setId_usu_captura($_COOKIE['admin/Id_usu']);
       $PaquetesDescuentosAlumno->setPorcentaje($_POST['Porcentaje']);
       $DaoPaquetesDescuentosAlumno->add($PaquetesDescuentosAlumno);
       
       
       $TextoHistorial="Añade descuento de ".$_POST['Porcentaje']."% por ".$paquete->getNombre()." para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", el día ".date('Y-m-d H:i:s');
       $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Descuentos");
  
   }
     updatePagos($_POST['id_ofe_alum']);
}



