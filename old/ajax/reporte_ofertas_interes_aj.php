<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="filtro"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoMediosEnterar= new DaoMediosEnterar();
    $DaoCiclos= new DaoCiclos();
    $ciclo=$DaoCiclos->getActual();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $query="";

    if($_POST['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_POST['Id_ofe'];
    }
    if($_POST['Id_esp']>0){
        $query=$query." AND Id_esp=".$_POST['Id_esp'];
    }
    if($_POST['Id_ori']>0){
        $query=$query." AND Id_ori=".$_POST['Id_ori'];
    }
    if($_POST['Id_ciclo']>0){
        $query=$query." AND Id_ciclo=".$_POST['Id_ciclo'];
    }
    
    $query = "SELECT * FROM inscripciones_ulm 
    JOIN Ofertas_interes ON inscripciones_ulm.Id_ins=Ofertas_interes.Id_alum
    WHERE tipo=1  AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()." ".$query." ORDER BY DateCreated DESC";
    $consulta=$base->advanced_query($query);
    $row_consulta = $consulta->fetch_assoc();
    $totalRows_consulta= $consulta->num_rows;
    if($totalRows_consulta>0){
      do{

               $nombre_ori="";
               $oferta = $DaoOfertas->show($row_consulta['Id_ofe']);
               $esp = $DaoEspecialidades->show($row_consulta['Id_esp']);
               if ($row_consulta['Id_ori'] > 0) {
                  $ori = $DaoOrientaciones->show($row_consulta['Id_ori']);
                  $nombre_ori = $ori->getNombre();
                }
                $opcion="Plan por materias"; 
                if($row_consulta['Opcion_pago']==2){
                  $opcion="Plan completo";  
                }

                $MedioEnt="";
                if($row_consulta['Id_medio_ent']>0){
                  $medio=$DaoMediosEnterar->show($row_consulta['Id_medio_ent']);
                  $MedioEnt=$medio->getMedio();
                }
               $ciclo=$DaoCiclos->show($row_consulta['Id_ciclo']);

             ?>
                     <tr id_alum="<?php echo $row_consulta['Id_ins'];?>">
                       <td><?php echo $row_consulta['Matricula'] ?></td>
                       <td style="width: 115px;"><?php echo $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins'] ?></td>
                       <td><?php echo $oferta->getNombre_oferta(); ?></td>
                       <td><?php echo $esp->getNombre_esp(); ?></td>
                       <td><?php echo $nombre_ori; ?></td>
                       <td><?php echo $ciclo->getClave(); ?></td>
                       <td><?php echo $row_consulta['DateCreated']; ?></td>
                       <td><input type="checkbox"> </td>
                     </tr>
                     <?php

              }while($row_consulta = $consulta->fetch_assoc());
           }
}



if($_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'MATRICULA');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'OFERTA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'CARRERA');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'ORIENTACIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'CICLO DE INTERÉS');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'FECHA DE CAPTURA');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','H') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoMediosEnterar= new DaoMediosEnterar();
    $DaoCiclos= new DaoCiclos();
    $ciclo=$DaoCiclos->getActual();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $query="";

    if($_GET['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_GET['Id_ofe'];
    }
    if($_GET['Id_esp']>0){
        $query=$query." AND Id_esp=".$_GET['Id_esp'];
    }
    if($_GET['Id_ori']>0){
        $query=$query." AND Id_ori=".$_GET['Id_ori'];
    }
    if($_GET['Id_ciclo']>0){
        $query=$query." AND Id_ciclo=".$_GET['Id_ciclo'];
    }
    $count=1;
    $query = "SELECT * FROM inscripciones_ulm 
    JOIN Ofertas_interes ON inscripciones_ulm.Id_ins=Ofertas_interes.Id_alum
    WHERE tipo=1  AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()." ".$query." ORDER BY DateCreated DESC";
    $consulta=$base->advanced_query($query);
    $row_consulta = $consulta->fetch_assoc();
    $totalRows_consulta= $consulta->num_rows;
    if($totalRows_consulta>0){
      do{

               $nombre_ori="";
               $oferta = $DaoOfertas->show($row_consulta['Id_ofe']);
               $esp = $DaoEspecialidades->show($row_consulta['Id_esp']);
               if ($row_consulta['Id_ori'] > 0) {
                  $ori = $DaoOrientaciones->show($row_consulta['Id_ori']);
                  $nombre_ori = $ori->getNombre();
                }
                $opcion="Plan por materias"; 
                if($row_consulta['Opcion_pago']==2){
                  $opcion="Plan completo";  
                }

                $MedioEnt="";
                if($row_consulta['Id_medio_ent']>0){
                  $medio=$DaoMediosEnterar->show($row_consulta['Id_medio_ent']);
                  $MedioEnt=$medio->getMedio();
                }
                $ciclo=$DaoCiclos->show($row_consulta['Id_ciclo']);
               
                $count++;
                $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
                $objPHPExcel->getActiveSheet()->setCellValue("B$count", $row_consulta['Matricula']);
                $objPHPExcel->getActiveSheet()->setCellValue("C$count", $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins']);
                $objPHPExcel->getActiveSheet()->setCellValue("D$count", $oferta->getNombre_oferta());
                $objPHPExcel->getActiveSheet()->setCellValue("E$count", $esp->getNombre_esp());
                $objPHPExcel->getActiveSheet()->setCellValue("F$count", $nombre_ori);
                $objPHPExcel->getActiveSheet()->setCellValue("G$count", $ciclo->getClave());
                $objPHPExcel->getActiveSheet()->setCellValue("H$count", $row_consulta['DateCreated']);

              }while($row_consulta = $consulta->fetch_assoc());
     }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=OfertasDeInteres-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}
