<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 


if($_POST['action']=="buscarAlum"){
    $DaoAlumnos= new DaoAlumnos();
    foreach($DaoAlumnos->buscarAlumno($_POST['buscar']) as $k=>$v){
      ?>
      <li><a href="ofertas_alumno.php?id=<?php echo $v->getId();?>"><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></a></li>
      <?php      
    }
}


if($_POST['action']=="box_beca"){
  $DaoBecas= new DaoBecas();
  $DaoCiclosAlumno= new DaoCiclosAlumno();
  $DaoCiclos= new DaoCiclos();
  $DaoOfertasAlumno= new DaoOfertasAlumno();
  $ofertaAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
?>
<div class="box-emergente-form">
	<h1>Aplicar Beca</h1>
        <p>Tipo<br>
            <select id="tipo_beca" onchange="updatePorcentajeBeca()">
                  <option value="0"></option>
                  <!--
                  <option value="1">Rendimiento Academico</option>
                  <option value="2">Estudio Socioecon&oacute;mico</option>
                  -->
                  <?php
                  foreach($DaoBecas->showAll() as $beca){
                      ?>
                       <option value="<?php echo $beca->getId()?>"><?php echo $beca->getNombre()?></option>
                      <?php
                  }
                  ?>
            </select>
        </p>
        <p>Porcentaje<br><input type="text" id="porcentaje" readonly="readonly"/></p>
        <p>Ciclo<br>
          <select id="ciclo_alum">
               <option value="0"></option>
                <?php
                foreach($DaoCiclosAlumno->getCiclosOferta($_POST['Id_ofe_alum']) as $cicloAlumno){
                        $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());
                  ?>
               <option value="<?php echo $cicloAlumno->getId()?>"><?php echo $ciclo->getClave()?></option>
                 <?php
                }
                ?>
           </select>
        </p>
        
        <p><button onclick="aplicar_beca(<?php echo $ofertaAlumno->getId_alum();?>,<?php echo $ofertaAlumno->getId();?>)">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
</div>
<?php
}

if($_POST['action']=="updatePorcentajeBeca"){
    $DaoBecas= new DaoBecas();
    $beca=$DaoBecas->show($_POST['Id_beca']);
    echo number_format($beca->getMonto(),0);
}

if($_POST['action']=="getCalificacionMin"){
    $DaoBecas= new DaoBecas();
    $beca=$DaoBecas->show($_POST['Id_beca']);
    echo number_format($beca->getCalificacionMin(),0);
}


if($_POST['action']=="aplicar_beca"){   
    $base= new base();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoBecas= new DaoBecas();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoEspecialidades= new DaoEspecialidades();
    
    $beca=$DaoBecas->show($_POST['Tipo_beca']);
    $ofertasAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $Last_ciclo=$DaoOfertasAlumno->getLastCicloOferta($_POST['Id_ofe_alum']);
    
    $query = "SELECT * FROM ciclos_alum_ulm 
    JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
    WHERE Id_ofe_alum=".$_POST['Id_ofe_alum']." AND Id_ciclo<".$Last_ciclo['Id_ciclo']."  AND materias_ciclo_ulm.Activo=1";
    foreach($base->advanced_query($query) as $row_materias){

             $mat_esp=$DaoMateriasEspecialidad->show($row_materias['Id_mat_esp']);
             $mat=$DaoMaterias->show($mat_esp->getId_mat());

             if($row_materias['CalTotalParciales']>=$mat->getPromedio_min()){
                $calTotal+=$row_materias['CalTotalParciales'];
             }elseif($row_materias['CalExtraordinario']>=$mat->getPromedio_min()){
                 $calTotal+=$row_materias['CalExtraordinario'];
             }elseif($row_materias['CalEspecial']>=$mat->getPromedio_min()){
                 $calTotal+=$row_materias['CalEspecial'];
             }
             $countMat++;
     }
     
     $calTotal=$calTotal/$countMat;
     if($calTotal>$beca->getCalificacionMin()){
            //En realidad actualizar la oferta del alumno esta de mas
            $ofertasAlumno->setBeca($beca->getMonto());
            $ofertasAlumno->setTipo_beca($_POST['Tipo_beca']);
            $DaoOfertasAlumno->update($ofertasAlumno);
             
            //Aplicamos la beca al ciclo
            $cicloAlumno=$DaoCiclosAlumno->show($_POST['Id_ciclo_alum']);
            $cicloAlumno->setTipo_beca($_POST['Tipo_beca']);
            $cicloAlumno->setPorcentaje_beca($beca->getMonto());
            $cicloAlumno->setIdUsuCapBeca($_COOKIE['admin/Id_usu']);
            $cicloAlumno->setDiaCapBeca(date('Y-m-d H:i:s'));
            $DaoCiclosAlumno->update($cicloAlumno);
            
            //Aplicamos la beca a los cargos del ciclo
            //Se agrega campo descuento_beca para que se grabe el descuento
            // por si algun dia cambian de porcetaje el tipo de beca este no afecte al porcentaje
            // de beca que tenia anteriormente
            $query = "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=" . $_POST['Id_ciclo_alum']." AND Id_alum=".$_POST['Id_alum']." AND Concepto='Mensualidad' AND Cantidad_pagada IS NULL";
            foreach($DaoPagosCiclo->advanced_query($query) as $cargo){
                $montoDescuento=($cargo->getMensualidad()*$beca->getMonto())/100;
                $Pago=$DaoPagosCiclo->show($cargo->getId());
                $Pago->setDescuento_beca($montoDescuento);
                $DaoPagosCiclo->update($Pago);
            }

            //Crear historial
            $DaoUsuarios= new DaoUsuarios();
            $DaoAlumnos= new DaoAlumnos();
            $DaoCiclos= new DaoCiclos();

            $alum=$DaoAlumnos->show($_POST['Id_alum']);
            $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());

            $TextoHistorial="Añade beca del ".$_POST['Porcentaje']." al alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ciclo ".$ciclo->getClave();
            $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Becas");

            update_becas($_POST['Id_alum']);
     }else{
        echo number_format($calTotal,2);
     }

}


function update_becas($Id_alum){
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoOfertas = new DaoOfertas();
    $DaoAlumnos = new DaoAlumnos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoCiclos = new DaoCiclos();
    
    $_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $perm=array();
    foreach($DaoUsuarios->getPermisosUsuario($_usu->getId()) as $k=>$v){
        $perm[$v['Id_per']]=1;
    }
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1>Ofertas del alumno</h1>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <li>Buscar<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o matricula"/>
                            <ul id="buscador_int"></ul>
                        </li>

                    </ul>
                </div>
                <?php
                if ($Id_alum > 0) {
                    $alum = $DaoAlumnos->show($Id_alum);
                    ?>
                    <div class="seccion" id="list_ofertas">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>Matricula</td>
                                    <td class="normal"><?php echo $alum->getMatricula() ?></td>
                                    <td>Nombre</td>
                                    <td colspan="6" class="normal"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
                                </tr>
                                <tr>
                                    <td>Nivel</td>
                                    <td>Carrera</td>
                                    <td>Orientaci&oacute;n</td>
                                    <td>Beca</td>
                                    <td>Precio</td>
                                    <td>Inscripción</td>
                                    <td>Pagos</td>
                                    <td>Mensualidad</td>
                                    <td>Acciones</td>
                                </tr>

                            </thead>
                            <tbody>
                                <?php
                                if (count($DaoOfertasAlumno->getOfertasAlumno($alum->getId())) > 0) {
                                    foreach ($DaoOfertasAlumno->getOfertasAlumno($alum->getId()) as $k => $v) {

                                        $nombre_ori = "";
                                        $oferta = $DaoOfertas->show($v->getId_ofe());
                                        $esp = $DaoEspecialidades->show($v->getId_esp());
                                        if ($v->getId_ori() > 0) {
                                            $ori = $DaoOrientaciones->show($v->getId_ori());
                                            $nombre_ori = $ori->getNombre();
                                        }

                                        $PrimerCicloAlumno = $DaoCiclosAlumno->getPrimerCicloOferta($v->getId());
                                        $UltimoCicloAlumno = $DaoCiclosAlumno->getLastCicloOferta($v->getId());

                                        $NumPagosPrimerCiclo = count($DaoPagosCiclo->getCargosCiclo($PrimerCicloAlumno->getId()));
                                        $NumPagosUltimoCiclo = count($DaoPagosCiclo->getCargosCiclo($UltimoCicloAlumno->getId()));

                                        if ($NumPagosPrimerCiclo > 0) {
                                            $Id_ciclo_alum = $PrimerCicloAlumno->getId();
                                        } elseif ($NumPagosUltimoCiclo > 0) {
                                            $Id_ciclo_alum = $UltimoCicloAlumno->getId();
                                        } else {
                                            $Id_ciclo_alum = 0;
                                        }

                                        if ($Id_ciclo_alum > 0) {
                                            $Precio_curso = ($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos();
                                            ?>
                                            <tr>
                                                <td><?php echo $oferta->getNombre_oferta(); ?></td>
                                                <td style="width: 200px;"><?php echo $esp->getNombre_esp() . " " . $nombre_ori; ?></td>
                                                <td><?php echo $nombre_ori; ?></td>
                                                <td><?php echo number_format($UltimoCicloAlumno->getPorcentaje_beca(), 2) ?> %</td>
                                                <td><?php echo "$" . number_format($esp->getPrecio_curso(), 2) ?></td>
                                                <td><?php echo "$" . number_format($esp->getInscripcion_curso(), 2) ?></td>
                                                <td><?php echo $esp->getNum_pagos() ?></td>
                                                <td><?php echo "$" . number_format($Precio_curso, 2) ?></td>
                                                <td style="width:110px;">
                                                    <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                                                    <div class="box-buttom">
                                                        <?php
                                                        if (isset($perm['35'])) {
                                                            if ($oferta->getTipoOferta() == 1) {
                                                                ?>
                                                                <a href="pago.php?id=<?php echo $v->getId() ?>" target="_blank"><button>Cobranza</button></a><br>
                                                                <?php
                                                            } elseif ($oferta->getTipoOferta() == 2) {
                                                                ?>
                                                                <a href="cargos_curso.php?id=<?php echo $v->getId() ?>" target="_blank"><button>Cobranza</button></a><br>
                                                                <?php
                                                            }
                                                        }
                                                        //Si tiene ya generados los cargos o es opcion de pagos por materia debera de por lo menos a ver pagado la inscripcion
                                                        if (($NumPagosPrimerCiclo > 1 || $NumPagosUltimoCiclo > 1) || ($v->getOpcionPago() == 1 && $DaoPagosCiclo->verificarPagoInscripcion($v->getId()) == 1)) {
                                                            if (isset($perm['58'])) {
                                                                ?>
                                                                <a href="asignar_materias.php?id=<?php echo $v->getId() ?>" target="_blank"><button>Asignar materias</button></a><br>
                                                                <?php
                                                            }
                                                            if (isset($perm['63'])) {
                                                                ?>
                                                                <a href="acreditar_materias.php?id=<?php echo $v->getId() ?>" target="_blank"><button>Acreditar materias</button></a>
                                                                <?php
                                                            }
                                                            if (isset($perm['65'])) {
                                                                ?>
                                                                <!--<a href="pagos_adelantados.php?id=<?php echo $v->getId() ?>" target="_blank"><button>Adelantar pago</button></a>-->
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once '../estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="interesados.php" class="link">Interesados </a></li>
                    <li><a href="alumnos.php" class="link">Alumnos </a></li>
                </ul>
            </div>
        </td>
    </tr>
<?php
}
