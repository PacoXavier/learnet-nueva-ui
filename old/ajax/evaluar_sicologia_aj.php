<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="save_eva"){
    $DaoAnalisisSicologico= new DaoAnalisisSicologico();
    $DaoAnalisisSicologico->deleteByAlumno($_POST['Id_alum']);
    foreach($_POST['Campos'] as $k=>$v){
        $AnalisisSicologico= new AnalisisSicologico();
        $AnalisisSicologico->setId_alum($_POST['Id_alum']);
        $AnalisisSicologico->setId_camp($v['Id_camp']);
        $AnalisisSicologico->setId_usu($_COOKIE['admin/Id_usu']);
        $AnalisisSicologico->setValor_camp($v['Valor']);
        $AnalisisSicologico->setDateCreated(date('Y-m-d H:i:s'));
        $DaoAnalisisSicologico->add($AnalisisSicologico);
    }
    
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $alum=$DaoAlumnos->show($_POST['Id_alum']);     
    $TextoHistorial="Evalua al alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM()." en la sección de psicología";
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Análisis psicológico");
}
