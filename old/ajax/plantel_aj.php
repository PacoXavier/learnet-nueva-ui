<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if (isset($_POST['action']) && $_POST['action']=="update_pag"){
  update_page($_POST['Id_plantel']);
}



function update_page($Id_plantel) {
    
    $DaoOfertas= new DaoOfertas();
    $DaoOfertasPlantel= new DaoOfertasPlantel();
    $DaoDatosBancarios=new DaoDatosBancarios();
    $DaoDiasPlantel= new DaoDiasPlantel();

    $DaoPlanteles= new DaoPlanteles();
    $DaoDirecciones= new DaoDirecciones();
    $DaoUsuarios= new DaoUsuarios();
    $_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
?>
<table id="tabla">
    <?php
    $Nombre_plantel="";
    $Clave="";
    $Tel="";
    $getCalle_dir="";
    $getNumExt_dir="";
    $getNumInt_dir="";
    $getColonia_dir="";
    $getCiudad_dir="";
    $getEstado_dir="";
    $getCp_dir="";
    $Id_dir=0;
    $abreviatura="";
    $email="";
    $dominio="";
    $formato_pago="";
    $formato_pago_evento="";
    $logo="";
    $etiquetaLibro="";
    $etiquetaActivo="";
    $formato_inscripcion="";
    
    $Banco="";
    $Beneficiario="";
    $Convenio="";
    $Transferencias="";
    $RFC="";
    $Id_dato_banc="";
    
    $L=0;
    $M=0;
    $I=0;
    $J=0;
    $V=0;
    $S=0;
    $D=0;
    
    if ($Id_plantel > 0) {
        $plantel = $DaoPlanteles->show($Id_plantel);
        $Nombre_plantel=$plantel->getNombre_plantel();
        $Clave=$plantel->getClave();
        $Tel=$plantel->getTel();
        $abreviatura=$plantel->getAbreviatura();
        $email=$plantel->getEmail();
        $dominio=$plantel->getDominio();
        
        $formato_pago = $plantel->getFormato_pago();
        $formato_pago_evento = $plantel->getFormato_pago_evento();
        $etiquetaLibro=$plantel->getEtiquetaLibro();
        $etiquetaActivo=$plantel->getEtiquetaActivo();
        $formato_inscripcion=$plantel->getFormato_inscripcion();
        
        if ($plantel->getId_dir_plantel() > 0) {
            $dir = $DaoDirecciones->show($plantel->getId_dir_plantel());
            $getCalle_dir=$dir->getCalle_dir();
            $getNumExt_dir=$dir->getNumExt_dir();
            $getNumInt_dir=$dir->getNumInt_dir();
            $getColonia_dir=$dir->getColonia_dir();
            $getCiudad_dir=$dir->getCiudad_dir();
            $getEstado_dir=$dir->getEstado_dir();
            $getCp_dir=$dir->getCp_dir();
            $Id_dir=$plantel->getId_dir_plantel();
            $logo=$plantel->getId_img_logo();
         }
         
        $datosB=$DaoDatosBancarios->getDatosBancariosPlantel($Id_plantel);
        foreach($datosB as $dato){
            $Banco=$dato->getBanco();
            $Beneficiario=$dato->getBeneficiario();
            $Convenio=$dato->getConvenio();
            $Transferencias=$dato->getTrasferencias();
            $RFC=$dato->getRFC();   
            $Id_dato_banc=$dato->getId();
        }
        
        foreach($DaoDiasPlantel->getDiasPlantel($Id_plantel) as $dia){
            if($dia->getId_dia()==1){
               $L=1;
            }
            if($dia->getId_dia()==2){
               $M=1;
            }
            if($dia->getId_dia()==3){
               $I=1;
            }
            if($dia->getId_dia()==4){
               $J=1;
            }
            if($dia->getId_dia()==5){
               $V=1;
            }
            if($dia->getId_dia()==6){
               $S=1;
            }
            if($dia->getId_dia()==7){
               $D=1;
            }
        }
    }
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-building"></i> Plantel <?php echo $Nombre_plantel;?></h1>
                </div>
                <div class="seccion">
                    <h2>Datos del plantel</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Clave<br><input type="text" value="<?php echo $Clave ?>" id="clave"/></li>
                        <li>Nombre<br><input type="text" value="<?php echo $Nombre_plantel ?>" id="nombre"/></li>
                        <li>Abreviatura<br><input type="text" value="<?php echo $abreviatura ?>" id="abreviatura"/></li>
                        <li>Tel&eacute;fono<br><input type="tel" id="tel" value="<?php echo $Tel ?>"/></li>
                        <li>Email<br><input type="text" value="<?php echo $email ?>" id="email"/></li>
                        <li>Dominio<br><input type="text" value="<?php echo $dominio ?>" id="dominio"/></li>
                    </ul>
                </div>
                 <div class="seccion">
                      <h2>Direcci&oacute;n</h2>
                      <span class="linea"></span>
                      <ul class="form">
                        <li>Calle<br><input type="text" id="calle" value="<?php echo $getCalle_dir ?>"/></li>
                        <li>N&uacute;mero exterior<br><input type="text" id="numExt" value="<?php echo $getNumExt_dir ?>"/></li>
                        <li>N&uacute;mero interior<br><input type="text" id="numInt" value="<?php echo $getNumInt_dir ?>"/></li>
                        <li>Colonia<br><input type="text" id="colonia" value="<?php echo $getColonia_dir ?>"/></li>
                        <li>C&oacute;digo postal<br><input type="text" id="cp" value="<?php echo $getCp_dir ?>"/></li>
                        <li>Ciudad<br><input type="text" id="ciudad" value="<?php echo $getCiudad_dir ?>"/></li>
                        <li>Estado<br><input type="text" id="estado" value="<?php echo $getEstado_dir?>"/></li>
                      </ul>
                </div>
                <div class="seccion datosBancarios">
                    <h2>Datos bancarios</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Banco<br><input type="text" id="Banco" value="<?php echo $Banco ?>"/></li>
                        <li>Beneficiario<br><input type="text" id="Beneficiario" value="<?php echo $Beneficiario ?>"/></li>
                        <li>Convenio<br><input type="text" id="Convenio" value="<?php echo $Convenio ?>"/></li>
                        <li>Transferencias<br><input type="text" id="Transferencias" value="<?php echo $Transferencias ?>"/></li>
                        <li>RFC<br><input type="text" id="RFC" value="<?php echo $RFC ?>"/></li>
                    </ul>
                </div>
                <div class="seccion datosBancarios">
                    <h2>Días laborales</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Lunes<br><input type="checkbox" id="lunes" class="dias" value="1" <?php  if($L==1){?> checked="checked" <?php }?>/></li>
                        <li>Martes<br><input type="checkbox" id="martes" class="dias" value="2" <?php  if($M==1){?> checked="checked" <?php }?>//></li>
                        <li>Miércoles<br><input type="checkbox" id="miercoles" class="dias" value="3" <?php  if($I==1){?> checked="checked" <?php }?>//></li>
                        <li>Jueves<br><input type="checkbox" id="jueves" class="dias" value="4" <?php  if($J==1){?> checked="checked" <?php }?>//></li>
                        <li>Viernes<br><input type="checkbox" id="viernes" class="dias" value="5" <?php  if($V==1){?> checked="checked" <?php }?>//> </li>
                        <li>Sábado<br><input type="checkbox" id="sabado" class="dias" value="6" <?php  if($S==1){?> checked="checked" <?php }?>//></li>
                        <li>Domingo<br><input type="checkbox" id="domingo" class="dias" value="7" <?php  if($D==1){?> checked="checked" <?php }?>//></li>
                    </ul>
                </div>
                <div class="seccion box-list">
                    <h2>Formato de pago</h2>
                    <iframe <?php if (strlen($formato_pago) > 0) { ?> src="files/<?php echo $formato_pago ?>.pdf <?php } ?>" style="width: 250px;height: 209px;" id="formato_pago">
                      <p>Your browser does not support iframes.</p>
                    </iframe>
                </div>
                <div class="seccion box-list">
                    <h2>Formato de eventos</h2>
                    <iframe <?php if (strlen($formato_pago_evento) > 0) { ?> src="files/<?php echo $formato_pago_evento ?>.pdf <?php } ?>" style="width: 250px;height: 209px;" id="formato_pago_evento">
                      <p>Your browser does not support iframes.</p>
                    </iframe>
                </div>
                <div class="seccion box-list">
                    <h2>Formato de inscripción</h2>
                    <iframe <?php if (strlen($formato_inscripcion) > 0) { ?> src="files/<?php echo $formato_inscripcion ?>.pdf <?php } ?>" style="width: 250px;height: 209px;" id="formato_inscripcion">
                        <p>Your browser does not support iframes.</p>
                    </iframe>
                </div>
                <div class="seccion box-list">
                    <h2>Logo</h2>
                    <div id="box-logo" <?php if (strlen($logo) > 0) { ?> style="background-image:url(files/<?php echo $logo ?>.jpg) <?php } ?>"></div>
                </div>
                <div class="seccion box-list">
                    <h2>Etiqueta libro</h2>
                    <div id="box-eti-libro" <?php if (strlen($etiquetaLibro) > 0) { ?> style="background-image:url(files/<?php echo $etiquetaLibro ?>.jpg) <?php } ?>"></div>
                </div>
                <div class="seccion box-list">
                    <h2>Etiqueta activo</h2>
                    <div id="box-eti-activo" <?php if (strlen($etiquetaActivo) > 0) { ?> style="background-image:url(files/<?php echo $etiquetaActivo ?>.jpg) <?php } ?>"></div>
                </div>
                <button id="button_ins" onclick="save_plantel()">Guardar</button>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once '../estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="plantel.php" class="link">Nuevo</a></li>
                    <?php
                    if ($Id_plantel > 0) {
                    ?>
                    <li><span onclick="mostrarFinder('logo')"><i class="fa fa-file-image-o"></i> A&ntilde;adir logo</span></li>
                    <li><span onclick="mostrarFinder('pdf')"><i class="fa fa-file-pdf-o"></i> A&ntilde;adir formato de pago</span></li>
                    <li><span onclick="mostrarFinder('formato_inscripcion')"><i class="fa fa-file-pdf-o"></i> A&ntilde;adir formato de inscripción</span></li>
                    <li><span onclick="mostrarFinder('pdf_eventos')"><i class="fa fa-file-pdf-o"></i> A&ntilde;adir formato de eventos</span></li>
                    <li><span onclick="mostrarFinder('etiqueta_libro')"><i class="fa fa-file-image-o"></i> A&ntilde;adir etiqueta libro</span></li>
                    <li><span onclick="mostrarFinder('etiqueta_activo')"><i class="fa fa-file-image-o"></i> A&ntilde;adir etiqueta activo</span></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_plantel" value="<?php echo $Id_plantel ?>"/>
<input type="hidden" id="Id_dir" value="<?php echo $Id_dir ?>"/>
<input type="file" id="files" name="files" multiple="">
<?php
}

if (isset($_POST['action']) && $_POST['action'] == "save_plantel") {

    $DaoPlanteles= new DaoPlanteles();
    $DaoDirecciones=new DaoDirecciones();
    $DaoUsuarios= new DaoUsuarios();
    $DaoDiasPlantel= new DaoDiasPlantel();
    
    $resp = array();
    if ($_POST['Id_plantel']>0){
        $plantel=$DaoPlanteles->show($_POST['Id_plantel']);
        $plantel->setClave($_POST['Clave']);
        $plantel->setNombre_plantel($_POST['Nombre']);
        $plantel->setTel($_POST['Telefono']);
        $plantel->setAbreviatura($_POST['abreviatura']);
        $plantel->setEmail($_POST['email']);
        $plantel->setDominio($_POST['dominio']);
        $DaoPlanteles->update($plantel);
    
        //Crear historial
        $TextoHistorial="Actualiza los datos para el plantel ".$_POST['Nombre'];
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Planteles");
        
        $Id_plantel = $_POST['Id_plantel'];
        $resp['tipo'] = "update";
        $resp['id'] = $Id_plantel;
    } else {
        
        $Planteles= new Planteles();
        $Planteles->setClave($_POST['Clave']);
        $Planteles->setNombre_plantel($_POST['Nombre']);
        $Planteles->setTel($_POST['Telefono']);
        $plantel->setAbreviatura($_POST['abreviatura']);
        $plantel->setEmail($_POST['email']);
        $plantel->setDominio($_POST['dominio']);
        $Planteles->setActivo_plantel(1);
        $Planteles->setDateCreated(date('Y-m-d H:i:s'));
        $Id_plantel=$DaoPlanteles->add($Planteles);

        $TextoHistorial="Añade como nuevo el plantel ".$_POST['Nombre'];
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Planteles");

        $resp['tipo'] = "new";
        $resp['id'] = $Id_plantel;
    }
    
    if ($_POST['Id_dir']>0){
        $Direcciones=$DaoDirecciones->show($_POST['Id_dir']);
        $Direcciones->setCalle_dir($_POST['calle']);
        $Direcciones->setCiudad_dir($_POST['ciudad']);
        $Direcciones->setColonia_dir($_POST['colonia']);
        $Direcciones->setCp_dir($_POST['cp']);
        $Direcciones->setEstado_dir($_POST['estado']);
        $Direcciones->setNumExt_dir($_POST['numExt']);
        $Direcciones->setNumInt_dir($_POST['numInt']);
        $DaoDirecciones->update($Direcciones);
        $id_dir = $_POST['Id_dir'];

    } elseif(strlen($_POST['calle']) > 0 && strlen($_POST['numExt']) > 0 && strlen($_POST['ciudad']) > 0) {
    
        $Direcciones=new Direcciones();
        $Direcciones->setCalle_dir($_POST['calle']);
        $Direcciones->setCiudad_dir($_POST['ciudad']);
        $Direcciones->setColonia_dir($_POST['colonia']);
        $Direcciones->setCp_dir($_POST['cp']);
        $Direcciones->setEstado_dir($_POST['estado']);
        $Direcciones->setNumExt_dir($_POST['numExt']);
        $Direcciones->setNumInt_dir($_POST['numInt']);
        $Direcciones->setIdRel_dir($id_usu);
        $Direcciones->setTipoRel_dir('plantel');
        $id_dir=$DaoDirecciones->add($Direcciones);

    }
   if($id_dir>0){
       $Planteles=$DaoPlanteles->show($Id_plantel);
       $Planteles->setId_dir_plantel($id_dir);
       $DaoPlanteles->update($Planteles);
   }
   
   $DaoDatosBancarios= new DaoDatosBancarios();
   if($_POST['Id_dato_banc']>0){
       $DatosBancarios=$DaoDatosBancarios->show($_POST['Id_dato_banc']);
       $DatosBancarios->setBanco($_POST['Banco']);
       $DatosBancarios->setBeneficiario($_POST['Beneficiario']);
       $DatosBancarios->setConvenio($_POST['Convenio']);
       $DatosBancarios->setRFC($_POST['RFC']);
       $DatosBancarios->setTrasferencias($_POST['Transferencias']);
       $DaoDatosBancarios->update($DatosBancarios);
   }else{
       if(strlen($_POST['Banco'])>0 && strlen($_POST['Beneficiario'])>0 && strlen($_POST['Convenio'])>0){
            $DatosBancarios= new DatosBancarios();
            $DatosBancarios->setBanco($_POST['Banco']);
            $DatosBancarios->setBeneficiario($_POST['Beneficiario']);
            $DatosBancarios->setConvenio($_POST['Convenio']);
            $DatosBancarios->setId_plantel($Id_plantel);
            $DatosBancarios->setRFC($_POST['RFC']);
            $DatosBancarios->setTrasferencias($_POST['Transferencias']);
            $DaoDatosBancarios->add($DatosBancarios);
       }
       
   }
   
   $DaoDiasPlantel->deleteDiasPlantel($Id_plantel);
   foreach($_POST['dias'] as $dia){
       $DiasPlantel= new DiasPlantel();
       $DiasPlantel->setId_dia($dia);
       $DiasPlantel->setId_plantel($Id_plantel);
       $DaoDiasPlantel->add($DiasPlantel);
   }
   echo json_encode($resp);
}





if(isset($_POST['action']) && $_POST['action']=="save_ofertas_plantel"){
    $DaoOfertasPlantel= new DaoOfertasPlantel();
    foreach($DaoOfertasPlantel->getOfertasPlantel($_POST['Id_plantel']) as $k=>$v){
        $DaoOfertasPlantel->delete($v->getId());
    }
    
    foreach($_POST['Ofertas'] as $v){
        $OfertasPlantel= new OfertasPlantel();
        $OfertasPlantel->setId_ofe($v);
        $OfertasPlantel->setId_plantel($_POST['Id_plantel']);
        $DaoOfertasPlantel->add($OfertasPlantel);
    }
    
    getOfertasPlantel($_POST['Id_plantel']);
}



function getOfertasPlantel($Id_plantel){

    $DaoPlanteles= new DaoPlanteles();
    $DaoDirecciones= new DaoDirecciones();
    $DaoOfertas= new DaoOfertas();
    $DaoOfertasPlantel= new DaoOfertasPlantel();
    
    $count = 1;
    foreach ($DaoOfertasPlantel->getOfertasPlantel($Id_plantel) as $k => $v) {
        $ofe=$DaoOfertas->show($v->getId_ofe());
        ?>
        <tr>
            <td><?php echo $count ?></td>
            <td><?php echo $ofe->getNombre_oferta() ?></td>
        </tr>
        <?php
        $count++;
    }
}


if(isset($_POST['action']) && $_POST['action']=="mostrar_box_ofertas"){
    $DaoOfertas= new DaoOfertas();
    $base= new base();
?>
	<div id="box_emergente" class="ofertas-plantel">
            <h1><i class="fa fa-graduation-cap"></i> Ofertas del plantel</h1>
            <p>Selecciona las ofertas impartidas en el plantel</p>
            <ul id="list-ofertas">
                <?php
                $query="SELECT * FROM ofertas_ulm 
                LEFT JOIN (SELECT * FROM OfertasPlantel WHERE Id_plantel=".$_POST['Id_plantel'].") AS OfertasDelPlantel ON ofertas_ulm.Id_oferta=OfertasDelPlantel.Id_ofe
                WHERE Activo_oferta=1 ORDER BY Id_oferta ASC";
                foreach($base->advanced_query($query) as $k=>$v){
                    ?>
                <li><input type="checkbox" value="<?php echo $v['Id_oferta']?>" <?php if($v['Id_oferta']==$v['Id_ofe']){ ?> checked="checked" <?php } ?>/> <?php echo $v['Nombre_oferta']?></li>
                    <?php
                }
                ?>
            </ul>
	    <p><button onclick="save_ofertas_plantel()">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
	</div>
<?php
}


if(isset($_GET['action']) && $_GET['action']=="upload_image"){
	if(count($_FILES)>0) { 
	   $type=substr($_FILES["file"]["name"],strpos($_FILES["file"]["name"],".")+1);
	if (($type == "gif" || $type == "jpeg" || $type == "png" || $type == "jpg" )){
	
	  if ($_FILES["file"]["error"] > 0){
	  
	      echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
	      
	    }else{
                $base= new base();
                $key=$base->generarKey();
                $DaoPlanteles= new DaoPlanteles();
                $Planteles= $DaoPlanteles->show($_GET['Id_plantel']);
                
                if(file_exists("../files/".$Planteles->getId_img_logo().".jpg")){
                       unlink("../files/".$Planteles->getId_img_logo().".jpg");
                }
                                  
                
                move_uploaded_file($_FILES["file"]["tmp_name"],"../files/".$key.".jpg");
                //header("Location: contabilidad.php");
                //se usa cuando se suben archivos por carga de archivos normales
                // print_r($_FILES);
                
                $Planteles->setId_img_logo($key);
                $DaoPlanteles->update($Planteles);
                

                echo $key;
	    }
	    
	  }else{
	        echo "Invalid file: ".$type;
	  }
	
	
	}elseif(isset($_GET['action'])){
		if (($_GET['TypeFile'] == "image/gif" || $_GET['TypeFile'] == "image/jpeg" || $_GET['TypeFile'] == "image/png" )){
		  if ($_FILES["file"]["error"] > 0){
		  
		      echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
		      
		    }else{
                            $base= new base();
                            $key=$base->generarKey();
                            $DaoPlanteles= new DaoPlanteles();
                            $Planteles= $DaoPlanteles->show($_GET['Id_plantel']);

                            if(file_exists("../files/".$Planteles->getId_img_logo().".jpg")){
                                   unlink("../files/".$Planteles->getId_img_logo().".jpg");
                            }

		           
                            if(isset($_GET['base64'])) {
                                // If the browser does not support sendAsBinary ()
                                    $content = base64_decode(file_get_contents('php://input'));
                            } else {
                                    $content = file_get_contents('php://input');
                            }
                            file_put_contents('../files/'.$key.'.jpg', $content);

                            $Planteles->setId_img_logo($key);
                            $DaoPlanteles->update($Planteles);

                            echo $key;
		  } 
		  
		}else {
		        //print_r($_FILES);
		        //print_r($_GET);
		        echo "Invalid file: ".$_GET['TypeFile'];
		}
	}
}



if(isset($_GET['action']) && $_GET['action']=="upload_formato_pago"){
    
	if(count($_FILES)>0) { 
	   $type=substr($_FILES["file"]["name"],strpos($_FILES["file"]["name"],".")+1);
	if (($type == "pdf")){
	
	  if ($_FILES["file"]["error"] > 0){
	  
	      echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
	      
	    }else{
                $base= new base();
                $key=$base->generarKey();
                $DaoPlanteles= new DaoPlanteles();
                $Planteles= $DaoPlanteles->show($_GET['Id_plantel']);
                
                if(file_exists("../files/".$Planteles->getFormato_pago().".pdf")){
                       unlink("../files/".$Planteles->getFormato_pago().".pdf");
                }
                                  
                
                move_uploaded_file($_FILES["file"]["tmp_name"],"../files/".$key.".pdf");
                //header("Location: contabilidad.php");
                //se usa cuando se suben archivos por carga de archivos normales
                // print_r($_FILES);
                
                $Planteles->setFormato_pago($key);
                $DaoPlanteles->update($Planteles);

                echo $key;
	    }
	    
	  }else{
	        echo "Invalid file: ".$type;
	  }
	
	}elseif(isset($_GET['action'])){
		if (($_GET['TypeFile'] == "application/pdf")){
		  if ($_FILES["file"]["error"] > 0){
		  
		      echo "Return Code: " . $_FILES["file"]["error"] . "<br/>";
		      
		    }else{
                            $base= new base();
                            $key=$base->generarKey();
                            $DaoPlanteles= new DaoPlanteles();
                            $Planteles= $DaoPlanteles->show($_GET['Id_plantel']);

                            if(file_exists("../files/".$Planteles->getFormato_pago().".pdf")){
                                   unlink("../files/".$Planteles->getFormato_pago().".pdf");
                            }

		           
                            if(isset($_GET['base64'])) {
                                // If the browser does not support sendAsBinary ()
                                    $content = base64_decode(file_get_contents('php://input'));
                            } else {
                                    $content = file_get_contents('php://input');
                            }
                            file_put_contents('../files/'.$key.'.pdf', $content);

                            $Planteles->setFormato_pago($key);
                            $DaoPlanteles->update($Planteles);
                            echo $key;
		  } 
		  
		}else {
		        //print_r($_FILES);
		        //print_r($_GET);
		        echo "Invalid file: ".$_GET['TypeFile'];
		}
	}
}



if(isset($_GET['action']) && $_GET['action']=="upload_formato_pago_evento"){
    
	if(count($_FILES)>0) { 
	   $type=substr($_FILES["file"]["name"],strpos($_FILES["file"]["name"],".")+1);
	if (($type == "pdf")){
	
	  if ($_FILES["file"]["error"] > 0){
	  
	      echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
	      
	    }else{
                $base= new base();
                $key=$base->generarKey();
                $DaoPlanteles= new DaoPlanteles();
                $Planteles= $DaoPlanteles->show($_GET['Id_plantel']);
                
                if(file_exists("./files/".$Planteles->getFormato_pago_evento().".pdf")){
                       unlink("../files/".$Planteles->getFormato_pago_evento().".pdf");
                }
                                  
                
                move_uploaded_file($_FILES["file"]["tmp_name"],"../files/".$key.".pdf");
                //header("Location: contabilidad.php");
                //se usa cuando se suben archivos por carga de archivos normales
                // print_r($_FILES);
                
                $Planteles->setFormato_pago_evento($key);
                $DaoPlanteles->update($Planteles);

                echo $key;
	    }
	    
	  }else{
	        echo "Invalid file: ".$type;
	  }
	
	}elseif(isset($_GET['action'])){
		if (($_GET['TypeFile'] == "application/pdf")){
		  if ($_FILES["file"]["error"] > 0){
		  
		      echo "Return Code: " . $_FILES["file"]["error"] . "<br/>";
		      
		    }else{
                            $base= new base();
                            $key=$base->generarKey();
                            $DaoPlanteles= new DaoPlanteles();
                            $Planteles= $DaoPlanteles->show($_GET['Id_plantel']);

                            if(file_exists("../files/".$Planteles->getFormato_pago_evento().".pdf")){
                                   unlink("../files/".$Planteles->getFormato_pago_evento().".pdf");
                            }

		           
                            if(isset($_GET['base64'])) {
                                // If the browser does not support sendAsBinary ()
                                    $content = base64_decode(file_get_contents('php://input'));
                            } else {
                                    $content = file_get_contents('php://input');
                            }
                            file_put_contents('../files/'.$key.'.pdf', $content);

                            $Planteles->setFormato_pago_evento($key);
                            $DaoPlanteles->update($Planteles);
                            echo $key;
		  } 
		  
		}else {
		        //print_r($_FILES);
		        //print_r($_GET);
		        echo "Invalid file: ".$_GET['TypeFile'];
		}
	}
}



if(isset($_GET['action']) && $_GET['action']=="upload_etiqueta_libro"){
	if(count($_FILES)>0) { 
	   $type=substr($_FILES["file"]["name"],strpos($_FILES["file"]["name"],".")+1);
	if (($type == "gif" || $type == "jpeg" || $type == "png" || $type == "jpg" )){
	
	  if ($_FILES["file"]["error"] > 0){
	  
	      echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
	      
	    }else{
                $base= new base();
                $key=$base->generarKey();
                $DaoPlanteles= new DaoPlanteles();
                $Planteles= $DaoPlanteles->show($_GET['Id_plantel']);
                
                if(file_exists("./files/".$Planteles->getEtiquetaLibro().".jpg")){
                       unlink("../files/".$Planteles->getEtiquetaLibro().".jpg");
                }

                move_uploaded_file($_FILES["file"]["tmp_name"],"../files/".$key.".jpg");
                $Planteles->setEtiquetaLibro($key);
                $DaoPlanteles->update($Planteles);
                

                echo $key;
	    }
	    
	  }else{
	        echo "Invalid file: ".$type;
	  }
	
	
	}elseif(isset($_GET['action'])){
		if (($_GET['TypeFile'] == "image/gif" || $_GET['TypeFile'] == "image/jpeg" || $_GET['TypeFile'] == "image/png" )){
		  if ($_FILES["file"]["error"] > 0){
		  
		      echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
		      
		    }else{
                            $base= new base();
                            $key=$base->generarKey();
                            $DaoPlanteles= new DaoPlanteles();
                            $Planteles= $DaoPlanteles->show($_GET['Id_plantel']);

                            if(file_exists("./files/".$Planteles->getEtiquetaLibro().".jpg")){
                                   unlink("../files/".$Planteles->getEtiquetaLibro().".jpg");
                            }
		           
                            if(isset($_GET['base64'])) {
                                // If the browser does not support sendAsBinary ()
                                    $content = base64_decode(file_get_contents('php://input'));
                            } else {
                                    $content = file_get_contents('php://input');
                            }
                            file_put_contents('../files/'.$key.'.jpg', $content);

                            $Planteles->setEtiquetaLibro($key);
                            $DaoPlanteles->update($Planteles);

                            echo $key;
		  } 
		  
		}else {
		        //print_r($_FILES);
		        //print_r($_GET);
		        echo "Invalid file: ".$_GET['TypeFile'];
		}
	}
}

if(isset($_GET['action']) && $_GET['action']=="upload_etiqueta_activo"){
	if(count($_FILES)>0) { 
	   $type=substr($_FILES["file"]["name"],strpos($_FILES["file"]["name"],".")+1);
	if (($type == "gif" || $type == "jpeg" || $type == "png" || $type == "jpg" )){
	
	  if ($_FILES["file"]["error"] > 0){
	  
	      echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
	      
	    }else{
                $base= new base();
                $key=$base->generarKey();
                $DaoPlanteles= new DaoPlanteles();
                $Planteles= $DaoPlanteles->show($_GET['Id_plantel']);
                
                if(file_exists("./files/".$Planteles->getEtiquetaActivo().".jpg")){
                       unlink("../files/".$Planteles->getEtiquetaActivo().".jpg");
                }

                move_uploaded_file($_FILES["file"]["tmp_name"],"../files/".$key.".jpg");
                $Planteles->setEtiquetaActivo($key);
                $DaoPlanteles->update($Planteles);
                

                echo $key;
	    }
	    
	  }else{
	        echo "Invalid file: ".$type;
	  }
	
	
	}elseif(isset($_GET['action'])){
		if (($_GET['TypeFile'] == "image/gif" || $_GET['TypeFile'] == "image/jpeg" || $_GET['TypeFile'] == "image/png" )){
		  if ($_FILES["file"]["error"] > 0){
		  
		      echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
		      
		    }else{
                            $base= new base();
                            $key=$base->generarKey();
                            $DaoPlanteles= new DaoPlanteles();
                            $Planteles= $DaoPlanteles->show($_GET['Id_plantel']);

                            if(file_exists("./files/".$Planteles->getEtiquetaActivo().".jpg")){
                                   unlink("../files/".$Planteles->getEtiquetaActivo().".jpg");
                            }
		           
                            if(isset($_GET['base64'])) {
                                // If the browser does not support sendAsBinary ()
                                    $content = base64_decode(file_get_contents('php://input'));
                            } else {
                                    $content = file_get_contents('php://input');
                            }
                            file_put_contents('../files/'.$key.'.jpg', $content);

                            $Planteles->setEtiquetaActivo($key);
                            $DaoPlanteles->update($Planteles);

                            echo $key;
		  } 
		  
		}else {
		        //print_r($_FILES);
		        //print_r($_GET);
		        echo "Invalid file: ".$_GET['TypeFile'];
		}
	}
}


if(isset($_GET['action']) && $_GET['action']=="upload_formato_inscripcion"){
    
	if(count($_FILES)>0) { 
	   $type=substr($_FILES["file"]["name"],strpos($_FILES["file"]["name"],".")+1);
	if (($type == "pdf")){
	
	  if ($_FILES["file"]["error"] > 0){
	  
	      echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
	      
	    }else{
                $base= new base();
                $key=$base->generarKey();
                $DaoPlanteles= new DaoPlanteles();
                $Planteles= $DaoPlanteles->show($_GET['Id_plantel']);
                
                if(file_exists("../files/".$Planteles->getFormato_inscripcion().".pdf")){
                       unlink("../files/".$Planteles->getFormato_inscripcion().".pdf");
                }
                                  
                
                move_uploaded_file($_FILES["file"]["tmp_name"],"../files/".$key.".pdf");
                //header("Location: contabilidad.php");
                //se usa cuando se suben archivos por carga de archivos normales
                // print_r($_FILES);
                
                $Planteles->setFormato_inscripcion($key);
                $DaoPlanteles->update($Planteles);

                echo $key;
	    }
	    
	  }else{
	        echo "Invalid file: ".$type;
	  }
	
	}elseif(isset($_GET['action'])){
		if (($_GET['TypeFile'] == "application/pdf")){
		  if ($_FILES["file"]["error"] > 0){
		  
		      echo "Return Code: " . $_FILES["file"]["error"] . "<br/>";
		      
		    }else{
                            $base= new base();
                            $key=$base->generarKey();
                            $DaoPlanteles= new DaoPlanteles();
                            $Planteles= $DaoPlanteles->show($_GET['Id_plantel']);

                            if(file_exists("../files/".$Planteles->getFormato_inscripcion().".pdf")){
                                   unlink("../files/".$Planteles->getFormato_inscripcion().".pdf");
                            }

		           
                            if(isset($_GET['base64'])) {
                                // If the browser does not support sendAsBinary ()
                                    $content = base64_decode(file_get_contents('php://input'));
                            } else {
                                    $content = file_get_contents('php://input');
                            }
                            file_put_contents('../files/'.$key.'.pdf', $content);

                            $Planteles->setFormato_inscripcion($key);
                            $DaoPlanteles->update($Planteles);
                            echo $key;
		  } 
		  
		}else {
		        //print_r($_FILES);
		        //print_r($_GET);
		        echo "Invalid file: ".$_GET['TypeFile'];
		}
	}
}