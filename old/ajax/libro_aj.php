<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if (isset($_GET['action']) && $_GET['action'] == "upload_image") {
   $base= new base();
   $DaoLibros= new DaoLibros();
   $Libro=$DaoLibros->show($_GET['Id_lib']);
   $key = $base->generarKey();
   
    if (count($_FILES) > 0) {
        $type = substr($_FILES["file"]["name"], strpos($_FILES["file"]["name"], ".") + 1);
        if (($type == "gif" || $type == "jpeg" || $type == "png" || $type == "jpg")) {

            if ($_FILES["file"]["error"] > 0) {

                echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
            } else {
                

                if (file_exists("../files/" . $Libro->getId_img() . ".jpg")) {
                    unlink("../files/" . $Libro->getId_img() . ".jpg");
                }
                
                $Libro->setId_img($key);
                $DaoLibros->update($Libro);

                move_uploaded_file($_FILES["file"]["tmp_name"], "../files/" . $key . ".jpg");
                //header("Location: contabilidad.php");
                //se usa cuando se suben archivos por carga de archivos normales
                // print_r($_FILES);
                $sourcefile = '../files/' . $key . '.jpg';
                $picsize = getimagesize($sourcefile);

                $source_x = $picsize[0] * 1;
                $source_y = $picsize[1] * 1;

                //modificar el tamaño de la imagen
                $result1 = $base->resizeToFile($sourcefile, 300, 300 * $source_y / $source_x, $sourcefile, 80);

                echo $key;
            }
        } else {
            echo "Invalid file: " . $type;
        }
    } elseif (isset($_GET['action'])) {
        if (($_GET['TypeFile'] == "image/gif" || $_GET['TypeFile'] == "image/jpeg" || $_GET['TypeFile'] == "image/png")) {
            if ($_FILES["file"]["error"] > 0) {

                echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
            } else {

                if (file_exists("../files/" . $Libro->getId_img() . ".jpg")) {
                    unlink("../files/" . $Libro->getId_img() . ".jpg");
                }
                
                $Libro->setId_img($key);
                $DaoLibros->update($Libro);

                if (isset($_GET['base64'])) {
                    // If the browser does not support sendAsBinary ()
                    $content = base64_decode(file_get_contents('php://input'));
                } else {
                    $content = file_get_contents('php://input');
                }
                file_put_contents('../files/' . $key . '.jpg', $content);

                $sourcefile = '../files/' . $key . '.jpg';
                $picsize = getimagesize($sourcefile);

                $source_x = $picsize[0] * 1;
                $source_y = $picsize[1] * 1;

                //modificar el tamaño de la imagen
                $result1 = $base->resizeToFile($sourcefile, 300, 300 * $source_y / $source_x, $sourcefile, 80);

                echo $key;
            }
        } else {
            //print_r($_FILES);
            //print_r($_GET);
            echo "Invalid file: " . $_GET['TypeFile'];
        }
    }
}




if (isset($_POST['action']) && $_POST['action']=="update_pag"){
   update_page($_POST['Id_libro']);
}

function update_page($Id_lib) {
    $DaoLibros= new DaoLibros();
    $DaoAulas= new DaoAulas();
    $DaoCategoriasLibros= new DaoCategoriasLibros();
    $DaoUsuarios= new DaoUsuarios();
    $_usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    ?>
    <table id="tabla">
    <?php
    $Id_img="";
    $Codigo="";
    $Titulo="";
    $Autor="";
    $Editorial="";
    $Anio="";
    $Edicion="";
    $ISBN="";
    $Seccion="";
    $Fecha_adq="";
    $Valor="";
    $Garantia_meses="";   
    $Id_aula="";
    $Id_cat="";
    $Sinopsis="";
    $Descripcion="";
    $Id_subcat="";
    
        if (isset($Id_lib) && $Id_lib > 0) {
            $libro = $DaoLibros->show($Id_lib);
            $Id_lib=$libro->getId();
            $Id_img=$libro->getId_img();
            $Codigo=$libro->getCodigo();
            $Titulo=$libro->getTitulo();
            $Autor=$libro->getAutor();
            $Editorial=$libro->getEditorial();
            $Anio=$libro->getAnio();
            $Edicion=$libro->getEdicion();
            $ISBN=$libro->getISBN();
            $Seccion=$libro->getSeccion();
            $Fecha_adq=$libro->getFecha_adq();
            $Valor=$libro->getValor();
            $Garantia_meses=$libro->getGarantia_meses();
            $Id_aula=$libro->getId_aula();
            $Id_cat=$libro->getId_cat();
            $Sinopsis=$libro->getSinopsis();
            $Descripcion=$libro->getDescripcion();
            $Id_subcat=$libro->getId_subcat();
        }
        ?>
        <tr>
            <td id="column_one">
                <div class="fondo">
                    <div id="box_top">
                        <div class="foto_alumno" 
                        <?php if (strlen($Id_img) > 0) { ?> 
                                 style="background-image:url(files/<?php echo $Id_img ?>.jpg) <?php } ?>">
                        </div>
                    </div>
                    <div class="seccion">
                        <h2>Datos del libro</h2>
                        <span class="linea"></span>
                        <ul class="form">
                            <?php
                            if ($Id_lib> 0) {
                            ?>
                            <li>C&oacute;digo<br><input type="text" value="<?php echo $Codigo ?>" id="codigo" readonly="readonly"/></li>
                            <?php
                                }
                             ?>
                            <li>T&iacute;tulo<br><input type="text" value="<?php echo $Titulo ?>" id="titulo"/></li>
                            <li>Autor<br><input type="text" value="<?php echo $Autor ?>" id="autor"/></li>
                            <li>Editorial<br><input type="text" value="<?php echo $Editorial ?>" id="editorial"/></li>
                            <li>Año<br><input type="text" value="<?php echo $Anio ?>" id="anio"/></li>
                            <li>Edici&oacute;n<br><input type="text" value="<?php echo $Edicion ?>" id="edicion"/></li>
                            <li>ISBN<br><input type="text" value="<?php echo $ISBN ?>" id="isbn"/></li>
                            <li>Secci&oacute;n<br><input type="text" value="<?php echo $Seccion ?>" id="seccion"/></li>
                            <li>Fecha de adquisici&oacute;n<br><input type="date" value="<?php echo $Fecha_adq ?>" id="fecha_adquisicion"/></li>
                            <li>Valor Factura<br><input type="text" value="<?php echo $Valor ?>" id="valor_factura"/></li>
                            <li>Meses de garantia<br><input type="text" id="meses_garantia" value="<?php echo $Garantia_meses ?>"/></li>
                            <li>Ubicaci&oacute;n<br>
                                <select id="aula">
                                    <option value="0"></option>
                                    <?php
                                    foreach($DaoAulas->showAll() as $aula){
                                    ?>
                                     <option value="<?php echo $aula->getId();?>" <?php if ($aula->getId() == $Id_aula) { ?> selected="selected"<?php } ?>><?php echo $aula->getClave_aula();?></option>
                                     <?php
                                     }
                                     ?>

                                </select>
                            </li>
                            <li>Categor&iacute;a<br>
                                <select id="categoria" onchange="update_subcat()">
                                    <option value="0"></option>
                                    <?php
                                    foreach($DaoCategoriasLibros->showAll() as $cat){
                                    ?>
                                     <option value="<?php echo $cat->getId();?>" <?php if ($cat->getId() == $Id_cat) { ?> selected="selected"<?php } ?>><?php echo $cat->getNombre()?></option>
                                     <?php
                                     }
                                     ?>
                                </select>
                            <li><div id="box-subcat">
                                    <p>Subcategor&iacute;a<br>
                                    <select id="subcat">
                                        <option value="0"></option>
                                        <?php
                                        if($Id_cat>0){
                                        foreach($DaoCategoriasLibros->getSubcategorias($Id_cat) as $cat){
                                        ?>
                                         <option value="<?php echo $cat->getId();?>" <?php if ($cat->getId() == $Id_subcat) { ?> selected="selected"<?php } ?>><?php echo $cat->getNombre()?></option>
                                         <?php
                                         }
                                        }
                                         ?>
                                    </select>
                                    </p>
                                </div></li><br>
                            <li>Sinopsis<br><textarea id="Sinopsis"><?php echo $Sinopsis?></textarea></li>
                            <li>Descripci&oacute;n<br><textarea id="descripcion"><?php echo $Descripcion?></textarea></li>
                        </ul>
                    </div>
                    <button id="button_ins" onclick="save_libro()">Guardar</button>
                </div>
            </td>
            <td id="column_two">
                <div id="box_menus">
                    <?php
                    require_once '../estandares/menu_derecho.php';
                    ?>
                    <ul>
                        <li><a href="libro.php" class="link">Nuevo</a></li>
                        <?php
                        if ($Id_lib> 0) {
                            ?>
                            <li><span onclick="mostrarFinder()">Añadir fotografia</span></li>
                            <li><a href="historial_prestamo_libro.php?id=<?php echo $Id_lib;?>" target="_blank">Historial de prestamos</a></li>
                            <li><a href="prestamo_libros.php?id=<?php echo $Id_lib;?>">Prestamo de libros</a></li>
                            <?php
                        }
                        ?>
                    </ul>
                    <input type="file" id="files" name="files" multiple="">
                </div>
            </td>
        </tr>
    </table>
    <input type="hidden" id="Id_lib" value="<?php echo $Id_lib ?>"/>
<?php
}


if (isset($_POST['action']) && $_POST['action'] == "save_libro") {
    $base= new base();
    $DaoUsuarios= new DaoUsuarios();
    $DaoLibros= new DaoLibros();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $resp = array();
    if ($_POST['Id_lib']>0){
        $Libros=$DaoLibros->show($_POST['Id_lib']);
        $Libros->setTitulo($_POST['titulo']);
        $Libros->setAutor($_POST['autor']);
        $Libros->setEditorial($_POST['editorial']);
        $Libros->setAnio($_POST['anio']);
        $Libros->setEdicion($_POST['edicion']);
        $Libros->setSeccion($_POST['seccion']);
        $Libros->setId_cat($_POST['Id_cat']);
        $Libros->setId_aula($_POST['Id_aula']);
        $Libros->setDescripcion($_POST['descripcion']);
        $Libros->setCodigo($DaoLibros->update_codigo_activo($_POST['Id_cat'],$_POST['Id_subcat'],$_POST['Id_lib']));
        $Libros->setValor($_POST['valor_factura']);
        $Libros->setFecha_adq($_POST['fecha_adquisicion']);
        $Libros->setGarantia_meses($_POST['meses_garantia']);
        $Libros->setISBN($_POST['isbn']);
        $Libros->setId_subcat($_POST['Id_subcat']);
        $Libros->setSinopsis($_POST['Sinopsis']);
        $DaoLibros->update($Libros);
    
        $TextoHistorial = "Actualizó los datos para el libro ".$_POST['titulo']; 
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Libros"); 

        $Id_lib = $_POST['Id_lib'];
        $resp['tipo'] = "update";
        $resp['id'] = $Id_lib;

    } else {
        $Libros= new Libros();
        $Libros->setTitulo($_POST['titulo']);
        $Libros->setAutor($_POST['autor']);
        $Libros->setEditorial($_POST['editorial']);
        $Libros->setAnio($_POST['anio']);
        $Libros->setEdicion($_POST['edicion']);
        $Libros->setSeccion($_POST['seccion']);
        $Libros->setId_cat($_POST['Id_cat']);
        $Libros->setId_aula($_POST['Id_aula']);
        $Libros->setDescripcion($_POST['descripcion']);
        $Libros->setCodigo($DaoLibros->generar_codigo_activo($_POST['Id_cat'],$_POST['Id_subcat']));
        $Libros->setId_plantel($usu->getId_plantel());
        $Libros->setValor($_POST['valor_factura']);
        $Libros->setFecha_adq($_POST['fecha_adquisicion']);
        $Libros->setGarantia_meses($_POST['meses_garantia']);
        $Libros->setDisponible(1);
        $Libros->setFecha_alta(date('Y-m-d'));
        $Libros->setISBN($_POST['isbn']);
        $Libros->setId_subcat($_POST['Id_subcat']);
        $Libros->setSinopsis($_POST['Sinopsis']);
        $Id_lib=$DaoLibros->add($Libros);
    
    $TextoHistorial = "Añadio el nuevo libro ".$_POST['titulo']; 
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Libros"); 
    
    $resp['tipo'] = "new";
    $resp['id'] = $Id_lib;
    }
    echo json_encode($resp);

}


if(isset($_POST['action']) && $_POST['action']=="update_subcat"){
    ?>
    <p>Subcategor&iacute;a<br>
        <select id="subcat">
            <option value="0"></option>
        <?php
            $DaoCategoriasLibros= new DaoCategoriasLibros();  
            foreach($DaoCategoriasLibros->getSubcategorias($_POST['Id_cat']) as $cat){
            ?>
             <option value="<?php echo $cat->getId();?>"><?php echo $cat->getNombre()?></option>
             <?php
             }
             ?>
        </select>
    </p>
    <?php
}

