<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 


if($_POST['action']=="buscarDocen"){
    $DaoDocentes= new DaoDocentes();
    $DaoUsuarios= new DaoUsuarios();
    if(strlen($_POST['buscar'])>=4){
        $count=1;
        foreach($DaoDocentes->buscarDocente($_POST['buscar']) as $docen){
                $status="Activo"; 
                $color="color:green";
                if(strlen($docen->getBaja_docen())>0){
                   $status="Inactivo";
                   $color="color:red";
                }
	  ?>
              <tr id-data="<?php echo $docen->getId()?>" style="<?php echo $color;?>">
                  <td onclick="mostrar(<?php echo $docen->getId()?>)"><?php echo $docen->getClave_docen()?></td>
                     <td><?php echo $count;?></td>
                     <td><?php echo $docen->getNombre_docen()." ".$docen->getApellidoP_docen()." ".$docen->getApellidoM_docen()?></td>
                     <td><?php echo $docen->getTelefono_docen()?></td>
                     <td><?php echo $docen->getEmail_docen()?></td>
                    <td><?php echo $status?></td>
                    <td class="td-center"><input type="checkbox"> </td>
                    <td class="td-center">
                        <button onclick="mostrar(<?php echo $docen->getId() ?>)">Editar</button>
                    <?php
                    if(strlen($docen->getBaja_docen())>0){
                    ?>
                      <button onclick="reactivarDocente(<?php echo $docen->getId()  ?>)">Dar de alta</button>
                    <?php
                    }else{
                        ?>
                        <button onclick="verificarGrupos(<?php echo $docen->getId()  ?>)">Dar de baja</button>
                        <?php
                    }
                    ?>
                    </td>
	     </tr>
          <?php      
        }
    }else{
        $count=1;
        $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        $query="SELECT * FROM Docentes WHERE Id_plantel=" . $usu->getId_plantel() . " ORDER BY Id_docen DESC";
        foreach($DaoDocentes->advanced_query($query) as $k=>$v){ 
           $status="Activo"; 
           $color="color:green";
           if(strlen($v->getBaja_docen())>0){
              $status="Inactivo";
              $color="color:red";
           }
        ?>
            <tr id-data="<?php echo $v->getId()?>" style="<?php echo $color;?>">
                 <td onclick="mostrar(<?php echo $v->getId()?>)"><?php echo $count;?></td>
                    <td><?php echo $v->getClave_docen();?></td>
                     <td><?php echo $v->getNombre_docen()." ".$v->getApellidoP_docen()." ".$v->getApellidoM_docen()?></td>
                     <td><?php echo $v->getTelefono_docen()?></td>
                     <td><?php echo $v->getEmail_docen()?></td>
                     <td><?php echo $status?></td>
                     <td class="td-center"><input type="checkbox"> </td>
                     <td class="td-center">
                     <button onclick="mostrar(<?php echo $v->getId() ?>)">Editar</button>
                     <?php
                     if(strlen($v->getBaja_docen())>0){
                     ?>
                       <button onclick="reactivarDocente(<?php echo $v->getId()?>)">Dar de alta</button>
                     <?php
                     }else{
                         ?>
                         <button onclick="verificarGrupos(<?php echo $v->getId()?>)">Dar de baja</button>
                         <?php
                     }
                     ?>
                     </td>
             </tr>
       <?php
       $count++; 
       }    
    }
}



if($_POST['action']=="delete_docen"){
    $DaoDocentes= new DaoDocentes();
    $doc=$DaoDocentes->show($_POST['Id_docen']);
    $doc->setBaja_docen(date('Y-m-d H:i:s'));
    $DaoDocentes->update($doc);
    getDocentes($_POST['Id_docen']);
}

if($_POST['action']=="verificarGrupos"){
    $base= new base();
    $DaoCiclos= new DaoCiclos();
    $ciclo=$DaoCiclos->getActual();
    $query="SELECT * FROM Horario_docente WHERE Id_docente=".$_POST['Id_docen']." AND Id_ciclo=".$ciclo->getId();
    $consulta=$base->advanced_query($query);
    $row_consulta = $consulta->fetch_assoc();
    $totalRows_consulta= $consulta->num_rows;
    echo $totalRows_consulta;
}

if($_POST['action']=="send_email"){
   $DaoUsuarios= new DaoUsuarios();
   $DaoPlanteles= new DaoPlanteles();
   $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
   $plantel=$DaoPlanteles->show($usu->getId_plantel());
   
   foreach($_POST['docentes'] as $v){
     $DaoDocentes= new DaoDocentes();
     $docente=$DaoDocentes->show($v);
     if(strlen($docente->getEmail_docen())>0){
       $base = new base();
       $arrayData= array();
       $arrayData['Asunto']=$_POST['asunto'];
       $arrayData['Mensaje']=$_POST['mensaje'];
       $arrayData['IdRel']=$_COOKIE['admin/Id_usu'];
       $arrayData['TipoRel']="usu";
       $arrayData['email-usuario']= $plantel->getEmail();
       $arrayData['nombre-usuario']=$DaoPlanteles->covertirCadena($plantel->getNombre_plantel(),2);
       $arrayData['replyTo']=$usu->getEmail_usu();
       $arrayData['Destinatarios']=array();
       
       $Data= array();
       $Data['email']=$docente->getEmail_docen();
       //$Data['email']= "christian310332@gmail.com";
       $Data['name']= $docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
       array_push($arrayData['Destinatarios'], $Data);
       $base->send_email($arrayData);
     }
   }
}


if($_POST['action']=="reactivarDocente"){
    $DaoDocentes= new DaoDocentes();
    $docente=$DaoDocentes->show($_POST['Id_docen']);
    $docente->setBaja_docen(NULL);
    $DaoDocentes->update($docente);
    getDocentes($_POST['Id_docen']);
}


function getDocentes($Id_docen){
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $docente=$DaoDocentes->show($Id_docen);
    $TextoHistorial="Elimina al docente ".$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Docentes");

    $usu= $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $count=1;
    $query="SELECT * FROM Docentes WHERE Id_plantel=" . $usu->getId_plantel() . " ORDER BY Id_docen DESC";
    foreach($DaoDocentes->advanced_query($query) as $k=>$v){ 
            $status="Activo"; 
            $color="color:green";
            if(strlen($v->getBaja_docen())>0){
               $status="Inactivo";
               $color="color:red";
            }
    ?>
         <tr id-data="<?php echo $v->getId()?>" style="<?php echo $color;?>">
             <td onclick="mostrar(<?php echo $v->getId()?>)"><?php echo $count;?></td>
                <td><?php echo $v->getClave_docen();?></td>
                 <td><?php echo $v->getNombre_docen()." ".$v->getApellidoP_docen()." ".$v->getApellidoM_docen()?></td>
                 <td><?php echo $v->getTelefono_docen()?></td>
                 <td><?php echo $v->getEmail_docen()?></td>
                <td><?php echo $status?></td>
                <td class="td-center"><input type="checkbox"> </td>
                <td class="td-center">
                <button onclick="mostrar(<?php echo $v->getId() ?>)">Editar</button>
                    <?php
                    if(strlen($v->getBaja_docen())>0){
                    ?>
                      <button onclick="reactivarDocente(<?php echo $v->getId()?>)">Dar de alta</button>
                    <?php
                    }else{
                        ?>
                        <button onclick="verificarGrupos(<?php echo $v->getId()?>)">Dar de baja</button>
                        <?php
                    }
                    ?>
                </td>
         </tr>
   <?php
   $count++; 
   }   
    
}