<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if ($_POST['action']=="box_prestamo"){
?>
<div class="box-emergente-form box-captura">
    <h1>Capturar prestamo</h1>
    <span class="linea"></span>
       <p>Solicitante<br><input type="text" id="usuario_recibe" onkeyup="buscarUsuarioPrestamo()"/></p>
        <ul id="buscador_prestamo"> </ul>
       
        <p>Fecha de devoluci&oacute;n<br><input type="date" id="fecha_devolucion"/></p>
        <p style="padding-bottom: 20px;">Estado de  entrega<br><textarea id="Estado_entrega"></textarea></p>
       <p><button onclick="save_prestamo(this)">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
</div>
<?php
}
if ($_POST['action']=="box_recepcion"){
    $DaoHistorialPrestamoLibro=new DaoHistorialPrestamoLibro();
    $DaoLibros= new DaoLibros();
    $libro=$DaoLibros->show($_POST['Id_lib']);

    if($libro->getDisponible()==0){
      $prestamo=$DaoHistorialPrestamoLibro->getLastPrestamo($_POST['Id_lib']);

      if($prestamo->getTipo_usu()=="usu" && $prestamo->getUsu_recibe()>0){
        $DaoUsuarios= new DaoUsuarios();
        $usuario=$DaoUsuarios->show($prestamo->getUsu_recibe());

        $nombre=$usuario->getNombre_usu()."  ".$usuario->getApellidoP_usu()."  ".$usuario->getApellidoM_usu();
        $tipo_usu="Usuario";
        $codigo=$usuario->getClave_usu();
      }elseif($prestamo->getTipo_usu()=="docen" && $prestamo->getUsu_recibe()>0){
          $DaoDocentes= new DaoDocentes();
          $docente = $DaoDocentes->show($prestamo->getUsu_recibe());
          $nombre=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
          $tipo_usu="Docente";
          $codigo=$docente->getClave_docen();
      }elseif($prestamo->getTipo_usu()=="alum" && $prestamo->getUsu_recibe()>0){
          $DaoAlumnos= new DaoAlumnos();
          $alum = $DaoAlumnos->show($prestamo->getUsu_recibe());
          $nombre=$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
          $tipo_usu="Alumno";
          $codigo=$alum->getMatricula();
      }
    }
?>
<div class="box-emergente-form box-captura">
    <h1>Capturar recepción</h1>
    <span class="linea"></span>
    <p>C&oacute;digo<br><input type="text" disabled="disabled" value="<?php echo $codigo?>"/></p>
    <p>Solicitante<br><input type="text" disabled="disabled" value="<?php echo strtoupper($nombre)?>"/></p>
    <p style="padding-bottom: 20px;">Estado de  entrega<br><textarea id="Estado_entrega"></textarea></p>
    <p><button onclick="save_recepcion()">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
</div>
<?php
}


if ($_POST['action'] == "save_prestamo") {
    $DaoLibros= new DaoLibros();
    $libro=$DaoLibros->show($_POST['Id_lib']);
    $libro->setDisponible(0);
    $DaoLibros->update($libro);
    
    $DaoHistorialPrestamoLibro= new DaoHistorialPrestamoLibro();
    $HistorialPrestamoLibro= new HistorialPrestamoLibro();
    $HistorialPrestamoLibro->setUsu_entrega($_COOKIE['admin/Id_usu']);
    $HistorialPrestamoLibro->setUsu_recibe($_POST['usuario_recibe']);
    $HistorialPrestamoLibro->setFecha_entrega(date('Y-m-d'));
    $HistorialPrestamoLibro->setFecha_devolucion($_POST['fecha_devolucion']);
    $HistorialPrestamoLibro->setEstado_entrega($_POST['Estado_entrega']);
    $HistorialPrestamoLibro->setId_libro($_POST['Id_lib']);
    $HistorialPrestamoLibro->setTipo_usu($_POST['tipo']);
    $DaoHistorialPrestamoLibro->add($HistorialPrestamoLibro);    
    
    $DaoUsuarios= new DaoUsuarios();
    $TextoHistorial = "Presto el libro ".$libro->getCodigo()." - ".$libro->getTitulo();  
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Préstamo de libros"); 
    
    update_page($_POST['Id_lib']);
}

if ($_POST['action'] == "save_recepcion") {
    $DaoLibros= new DaoLibros();
    $libro=$DaoLibros->show($_POST['Id_lib']);
    $libro->setDisponible(1);
    $DaoLibros->update($libro);
    
    $DaoHistorialPrestamoLibro= new DaoHistorialPrestamoLibro();
    $historial=$DaoHistorialPrestamoLibro->show($_POST['Id_prestamo']);
    $historial->setEstado_recibe($_POST['Estado_entrega']);
    $historial->setFecha_entregado(date('Y-m-d'));
    $DaoHistorialPrestamoLibro->update($historial);
        
    $DaoUsuarios= new DaoUsuarios();
    $TextoHistorial = "Recibió el libro ".$libro->getCodigo()." - ".$libro->getTitulo(); 
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Préstamo de libros"); 
    
    update_page($_POST['Id_lib']);
}


if($_POST['action']=="buscar_int"){
   $DaoLibros= new DaoLibros();
    foreach($DaoLibros->buscarLibro($_POST['buscar']) as $k=>$v){
      ?>
       <a href="prestamo_libros.php?id=<?php echo $v->getId()?>">
            <li><?php echo $v->getCodigo()." - ".$v->getTitulo()?></li>
       </a>
      <?php      
    }
}


if($_POST['action']=="buscarUsuarioPrestamo"){
  ?>
  <li><span>Usuarios</span></li>
  <?php
    $DaoUsuarios= new DaoUsuarios();
    foreach($DaoUsuarios->buscarUsuario($_POST['buscar']) as $k=>$v){
      ?>
      <li id_usu="<?php echo $v->getId()?>" tipo_usu="usu"><?php echo $v->getClave_usu()." - ".$v->getNombre_usu()." ".$v->getApellidoP_usu()." ".$v->getApellidoM_usu()?></li>
      <?php      
    }
  ?>
    <li><span>Docentes</span></li>
    <?php
    $DaoDocentes= new DaoDocentes();
    foreach($DaoDocentes->buscarDocente($_POST['buscar']) as $k=>$v){
      ?>
       <li id_usu="<?php echo $v->getId()?>" tipo_usu="docen"><?php echo $v->getClave_docen()." - ".$v->getNombre_docen()." ".$v->getApellidoP_docen()." ".$v->getApellidoM_docen()?></li>
      <?php      
    }
   ?>
   <li><span>Alumnos</span></li>
   <?php
    $DaoAlumnos= new DaoAlumnos();
    foreach($DaoAlumnos->buscarAlumno($_POST['buscar']) as $k=>$v){
      ?>
       <li id_usu="<?php echo $v->getId()?>" tipo_usu="alum"><?php echo $v->getMatricula()." - ".$v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM()?></li>
      <?php      
    }
}


function update_page($Id_lib){
    $DaoUsuarios= new DaoUsuarios();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    ?>    
    <table id="tabla">
        <tr>
            <td id="column_one">
                <div class="fondo">
                    <div id="box_top">
                        <h1><i class="fa fa-keyboard-o"></i> Préstamo libros</h1>
                    </div>
                    <div class="seccion">
                        <ul class="form">
                            <li>Buscar Libro<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="T&iacute;tulo o ISBN"/>
                              <ul id="buscador_int"></ul>
                            </li>
                        </ul>
                    </div>
            <?php
        $getFecha_entrega="";
        $getFecha_devolucion="";
        $Ubicacion="";  
        $Id_prestamo=0;
        if ($Id_lib > 0) {
           $DaoLibros= new DaoLibros();
           $libro=$DaoLibros->show($Id_lib);


            if($libro->getDisponible()==1){
              $disponibilidad="Disponible";
              $style='style="color:green;"';
            }else{
              $disponibilidad="No Disponible";
              $style='style="color:red;"';

              $DaoHistorialPrestamoLibro=new DaoHistorialPrestamoLibro();
              $prestamo=$DaoHistorialPrestamoLibro->getLastPrestamo($Id_lib);

              $Id_prestamo=$prestamo->getId();
              $getFecha_entrega=$prestamo->getFecha_entrega();
              $getFecha_devolucion=$prestamo->getFecha_devolucion();


              if($prestamo->getTipo_usu()=="usu" && $prestamo->getUsu_recibe()>0){
                $DaoUsuarios= new DaoUsuarios();
                $usuario=$DaoUsuarios->show($prestamo->getUsu_recibe());

                $nombre=$usuario->getNombre_usu()."  ".$usuario->getApellidoP_usu()."  ".$usuario->getApellidoM_usu();
                $tipo_usu="Usuario";
                $codigo=$usuario->getClave_usu();
              }elseif($prestamo->getTipo_usu()=="docen" && $prestamo->getUsu_recibe()>0){
                  $DaoDocentes= new DaoDocentes();
                  $docente = $DaoDocentes->show($prestamo->getUsu_recibe());
                  $nombre=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
                  $tipo_usu="Docente";
                  $codigo=$docente->getClave_docen();
              }elseif($prestamo->getTipo_usu()=="alum" && $prestamo->getUsu_recibe()>0){
                  $DaoAlumnos= new DaoAlumnos();
                  $alum = $DaoAlumnos->show($prestamo->getUsu_recibe());
                  $nombre=$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
                  $tipo_usu="Alumno";
                  $codigo=$alum->getMatricula();
              }
            }
            $DaoAulas= new DaoAulas();            
            foreach($DaoAulas->showAll() as $k=>$v){
                 if ($v->getId() == $libro->getId_aula()) { 
                     $Ubicacion=$v->getClave_aula();
                 }
             }
        }
        ?>
                  <div class="seccion">
                        <h2>Datos del libro</h2>
                        <span class="linea"></span>
                        <ul class="form">
                            <li>T&iacute;tulo<br><input type="text" value="<?php echo $libro->getTitulo() ?>" id="modelo" disabled="disabled" /></li>
                            <li>Autor<br><input type="text" value="<?php echo $libro->getAutor() ?>" id="nombre" disabled="disabled"/></li>
                            <li>Editorial<br><input type="text" value="<?php echo $libro->getEditorial() ?>" id="nombre" disabled="disabled"/></li>
                            <li>ISBN<br><input type="text" value="<?php echo $libro->getISBN() ?>" id="nombre" disabled="disabled"/></li>
                            <li>Categor&iacute;a<br><input type="text" value="" id="nombre" disabled="disabled"/></li>
                            <li>Ubicaci&oacute;n<br><input type="text" value="<?php echo $Ubicacion ?>" id="nombre" disabled="disabled"/></li>
                            <li>Fecha de prestamo<br><input type="date" value="<?php echo $getFecha_entrega ?>" id="Fecha_devolucion" disabled="disabled"/></li>
                            <li>Fecha de devoluci&oacute;n<br><input type="date" id="Fecha_devolucion" value="<?php echo $getFecha_devolucion ?>" disabled="disabled"/></li><br>
                            <li>Disponibilidad<br><input type="text" id="Estado" value="<?php echo $disponibilidad ?>" disabled="disabled" <?php echo $style;?>/></li>
                        </ul>
                    </div>
                        <?php
           if ($Id_lib  > 0 && $libro->getDisponible()==0) {
           ?>
                   <div class="seccion">
                            <h2>Datos del usuario actual</h2>
                            <span class="linea"></span>
                            <ul class="form">
                                        <li>Tipo<br><input type="text" id="Nombre_recibe" value="<?php echo $tipo_usu; ?>" disabled="disabled"/></li>
                                        <li>C&oacute;digo <br><input type="text" id="Nombre_recibe" value="<?php echo $codigo; ?>" disabled="disabled"/></li>
                                        <li>Nombre<br><input type="text" id="Nombre_recibe" value="<?php echo $nombre; ?>" disabled="disabled"/></li>
                            </ul>
                        </div>
                                <?php
            }
            ?>
                </div>
            </td>
            <td id="column_two">
                <div id="box_menus">
                    <table id="usu_login">
                        <tr>
                            <td><div class="nom_usu"><div id="img_usu" 
                                                          <?php if (strlen($usu->getImg_usu()) > 0) { ?> style="background-image:url(files/<?php echo $usu->getImg_usu()  ?>.jpg) <?php } ?>"></div>
                                                          <?php echo $usu->getNombre_usu() . " " . $usu->getApellidoP_usu() . " " . $usu->getApellidoM_usu() ?>
                                    <img src="images/linea_uno.png" alt="linea_uno" width="185" height="5" class="lineas"/></div>
                            </td>
                            <td>
                                <div class="opcion">
                                    <a href="perfil.php?id=<?php echo $usu->getId() ?>">
                                        <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Mi perfil</a>
                                    <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/>
                                </div>
                            </td>
                            <td><div class="opcion">
                                    <a href="logout.php">
                                        <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Salir</a>
                                    <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/></div>
                            </td>
                        </tr>
                    </table>
                    <h2><img src="images/flecha_blue.png" alt="flecha_blue" width="6" height="11" />Opciones</h2>
                    <ul>
                         <?php 
            if($Id_lib > 0) {
                    if($libro->getDisponible()==1){
              ?>
                                   <li><span onclick="box_prestamo()">Capturar prestamo </span></li>
                             <?php
                    }else{
              ?>
                                   <li><span onclick="box_recepcion()">Capturar recepci&oacute;n </span></li>
                                   <?php
                    }
            ?>
                        <li><a href="historial_prestamo_libro.php?id=<?php echo $Id_lib;?>" target="_blank">Historial de prestamos</a></li>
                        <?php
             }
             ?>

                    </ul>
                </div>
            </td>
        </tr>
    </table>
    <input type="hidden" id="Id_lib" value="<?php echo $Id_lib; ?>"/>
    <input type="hidden" id="Id_prestamo" value="<?php echo $Id_prestamo; ?>"/>
    <?php
}




