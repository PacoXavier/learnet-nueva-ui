<?php
require_once('activate_error.php');
require_once('../clases/DaoEspecialidades.php');
require_once('../clases/DaoUsuarios.php');
require_once('../clases/DaoGrados.php');
require_once('../clases/modelos/Grados.php');
require_once('../clases/modelos/Especialidades.php');

if(isset($_POST['action']) && $_POST['action']=="save_curso"){
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoUsuarios= new DaoUsuarios();
    $DaoGrados= new DaoGrados();
    
    $resp= array();
    if(isset($_POST['Id_curso']) && $_POST['Id_curso']>0){
      $esp=$DaoEspecialidades->show($_POST['Id_curso']);

      for($i=1;$i<=$_POST['Num_ciclos'];$i++){
            //Que pasara con los ciclos que ya existan por ejemplo si de ser 6 se pasen a 4?
            $respuesta=$DaoGrados->showByEspecialidadGrado($_POST['Id_curso'],$i);
            if($respuesta->getId()==0){
                  $Grados= new Grados();
                  $Grados->setId_esp($_POST['Id_curso']);
                  $Grados->setGrado($i);
                  $Grados->setActivo_grado(1);
                  $DaoGrados->add($Grados);
            } 
      }  
 

      if($esp->getNombre_esp()!=$_POST['nombre_curso']){
          $TextoHistorial = "Actualizo el nombre de la especialidad ".$esp->getNombre_esp()." a " . $_POST['nombre_curso']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
      if($esp->getRegistroValidez_Oficial()!=$_POST['resgistro']){
          $TextoHistorial = "Actualizo el registro de la especialidad ".$esp->getNombre_esp()." de ".$esp->getRegistroValidez_Oficial()." a " . $_POST['resgistro']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
      if($esp->getInstitucion_certifica()!=$_POST['insti']){
          $TextoHistorial = "Actualizo la institución de la especialidad ".$esp->getNombre_esp()." de ".$esp->getInstitucion_certifica()." a " . $_POST['insti']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
      if($esp->getModalidad()!=$_POST['modalidad']){
          $TextoHistorial = "Actualizo la modalidad de la especialidad ".$esp->getNombre_esp()." de ".$esp->getModalidad()." a " . $_POST['modalidad']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
      
      if($esp->getTipo_ciclos()!=$_POST['tipoCiclo']){
          $TextoHistorial = "Actualizo el tipo de ciclo de la especialidad ".$esp->getNombre_esp()." de ".$esp->getTipo_ciclos()." a " . $_POST['tipoCiclo']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
      if($esp->getDuracionSemanas()!=$_POST['duracion']){
          $TextoHistorial = "Actualizo la duración de la especialidad ".$esp->getNombre_esp()." de ".$esp->getDuracionSemanas()." a " . $_POST['duracion']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
      if($esp->getDuracionSemanas()!=$_POST['duracion']){
          $TextoHistorial = "Actualizo la duración de la especialidad ".$esp->getNombre_esp()." de ".$esp->getDuracionSemanas()." a " . $_POST['duracion']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
      if($esp->getClavePlan_estudios()!=$_POST['clave_Plan']){
          $TextoHistorial = "Actualizo la clave para la especialidad ".$esp->getNombre_esp()." de ".$esp->getClavePlan_estudios()." a " . $_POST['clave_Plan']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
      if($esp->getEscala_calificacion()!=$_POST['escala']){
          $TextoHistorial = "Actualizo la escala de calificación para la especialidad ".$esp->getNombre_esp()." de ".$esp->getEscala_calificacion()." a " . $_POST['escala']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
      if($esp->getMinima_aprobatotia()!=$_POST['minima_apro']){
          $TextoHistorial = "Actualizo la calificación mínima aprobatoria para la especialidad ".$esp->getNombre_esp()." de ".$esp->getMinima_aprobatotia()." a " . $_POST['minima_apro']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
      if($esp->getLimite_faltas()!=$_POST['limite_faltas']){
          $TextoHistorial = "Actualizo el limite de faltas para la especialidad ".$esp->getNombre_esp()." de ".$esp->getLimite_faltas()." a " . $_POST['limite_faltas']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
      if($esp->getPrecio_curso()!=$_POST['Precio_curso']){
          $TextoHistorial = "Actualizo el precio del curso para la especialidad ".$esp->getNombre_esp()." de ".$esp->getPrecio_curso()." a " . $_POST['Precio_curso']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
           
      if($esp->getInscripcion_curso()!=$_POST['Inscripcion_curso']){
          $TextoHistorial = "Actualizo el precio del la inscripción para la especialidad ".$esp->getNombre_esp()." de ".$esp->getInscripcion_curso()." a " . $_POST['Inscripcion_curso']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
      if($esp->getNum_pagos()!=$_POST['num_pagos']){
          $TextoHistorial = "Actualizo el número de pagos para la especialidad ".$esp->getNombre_esp()." de ".$esp->getNum_pagos()." a " . $_POST['num_pagos']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
      if($esp->getNum_ciclos()!=$_POST['Num_ciclos']){
          $TextoHistorial = "Actualizo el número de ciclos para la especialidad ".$esp->getNombre_esp()." de ".$esp->getNum_ciclos()." a " . $_POST['Num_ciclos']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
      if($esp->getTipo_insc()!=$_POST['tipo_insc']){
          $TextoHistorial = "Actualizo el tipo de inscripción para la especialidad ".$esp->getNombre_esp()." de ".$esp->getTipo_insc()." a " . $_POST['tipo_insc']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 
      }
      
            $Especialidades= $DaoEspecialidades->show($_POST['Id_curso']);
            $Especialidades->setId_ofe($_POST['Id_oferta']);
            $Especialidades->setNombre_esp($_POST['nombre_curso']);
            $Especialidades->setRegistroValidez_Oficial($_POST['resgistro']);
            $Especialidades->setInstitucion_certifica($_POST['insti']);
            $Especialidades->setModalidad($_POST['modalidad']);
            $Especialidades->setTipo_ciclos($_POST['tipoCiclo']);
            $Especialidades->setDuracionSemanas($_POST['duracion']);
            $Especialidades->setClavePlan_estudios($_POST['clave_Plan']);
            $Especialidades->setEscala_calificacion($_POST['escala']);
            $Especialidades->setMinima_aprobatotia($_POST['minima_apro']);
            $Especialidades->setLimite_faltas($_POST['limite_faltas']);
            $Especialidades->setPrecio_curso($_POST['Precio_curso']);
            $Especialidades->setInscripcion_curso($_POST['Inscripcion_curso']);
            $Especialidades->setTipo_insc($_POST['tipo_insc']);
            $Especialidades->setNum_pagos($_POST['num_pagos']);
            $Especialidades->setNum_ciclos($_POST['Num_ciclos']);
            $DaoEspecialidades->update($Especialidades);      
            $id_esp=$_POST['Id_curso'];

	}else{

            $Especialidades= new Especialidades();
            $Especialidades->setId_ofe($_POST['Id_oferta']);
            $Especialidades->setNombre_esp($_POST['nombre_curso']);
            $Especialidades->setRegistroValidez_Oficial($_POST['resgistro']);
            $Especialidades->setInstitucion_certifica($_POST['insti']);
            $Especialidades->setModalidad($_POST['modalidad']);
            $Especialidades->setTipo_ciclos($_POST['tipoCiclo']);
            $Especialidades->setDuracionSemanas($_POST['duracion']);
            $Especialidades->setClavePlan_estudios($_POST['clave_Plan']);
            $Especialidades->setEscala_calificacion($_POST['escala']);
            $Especialidades->setMinima_aprobatotia($_POST['minima_apro']);
            $Especialidades->setLimite_faltas($_POST['limite_faltas']);
            $Especialidades->setPrecio_curso($_POST['Precio_curso']);
            $Especialidades->setInscripcion_curso($_POST['Inscripcion_curso']);
            $Especialidades->setTipo_insc($_POST['tipo_insc']);
            $Especialidades->setNum_pagos($_POST['num_pagos']);
            $Especialidades->setNum_ciclos($_POST['Num_ciclos']);
            $Especialidades->setActivo_esp(1);
            $id_esp=$DaoEspecialidades->add($Especialidades);
                        
          $TextoHistorial = "Añadio la nueva especialidad ".$_POST['nombre_curso']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Especialidades"); 

           for($i=1;$i<=$_POST['Num_ciclos'];$i++){
                $Grados= new Grados();
                $Grados->setId_esp($id_esp);
                $Grados->setGrado($i);
                $Grados->setActivo_grado(1);
                $DaoGrados->add($Grados);
          }  
	}
        $resp['id']=$id_esp;
        echo json_encode($resp);
}

