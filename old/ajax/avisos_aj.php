<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="mostrar_filtro_alumnos"){
    ?>
        <h1>Filtro Alumnos</h1>
	<p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              $DaoOfertas= new DaoOfertas();
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso();update_grados_ofe()">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
        <p>Grado:<br>
            <select id="grado">
              <option value="0"></option>
            </select>
        </p>
        <p>Opci&oacute;n de pago:<br>
            <select id="opcion">
              <option value="0"></option>
              <option value="1">POR MATERIAS</option>
              <option value="2">PLAN COMPLETO</option>
            </select>
        </p>
        <p><button onclick="filtro()">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
    <?php
}



if($_POST['action']=="filtro"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoMediosEnterar= new DaoMediosEnterar();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query="";

    if($_POST['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_POST['Id_ofe'];
    }
    if($_POST['Id_esp']>0){
        $query=$query." AND Id_esp=".$_POST['Id_esp'];
    }
    if($_POST['Id_ori']>0){
        $query=$query." AND Id_ori=".$_POST['Id_ori'];
    }
    if($_POST['Opcion_pago']>0){
        $query=$query." AND Opcion_pago=".$_POST['Opcion_pago'];
    }
    ?>
    <table class="table">
    <thead>
        <tr>
            <td>#</td>
            <td>Matricula</td>
            <td>Nombre</td>
            <td>Oferta</td>
            <td>Carrera</td>
            <td>Orientaci&oacute;n:</td>
            <td><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
        </tr>
    </thead>
    <tbody>
        <?php
    
    $count=1;
    $query="SELECT * FROM inscripciones_ulm 
        JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
        WHERE tipo=1  
              AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()." 
              AND Activo_oferta=1 
              AND Baja_ofe IS NULL 
              AND FechaCapturaEgreso IS NULL
              AND IdCicloEgreso IS NULL
              AND IdUsuEgreso IS NULL ".$query." 
        ORDER BY Id_ins DESC";
    $consulta=$base->advanced_query($query);
    $row_consulta = $consulta->fetch_assoc();
    $totalRows_consulta= $consulta->num_rows;
     if($totalRows_consulta>0){
       do{
           $nombre_ori="";
           $oferta = $DaoOfertas->show($row_consulta['Id_ofe']);
           $esp = $DaoEspecialidades->show($row_consulta['Id_esp']);
           if ($row_consulta['Id_ori'] > 0) {
              $ori = $DaoOrientaciones->show($row_consulta['Id_ori']);
              $nombre_ori = $ori->getNombre();
            }
            $opcion="Plan por materias"; 
            if($row_consulta['Opcion_pago']==2){
              $opcion="Plan completo";  
            }
            
            $Grado=$DaoOfertasAlumno->getLastCicloOfertaSinCiclo($row_consulta['Id_ofe_alum']);
            $Grado=$DaoGrados->show($Grado['Id_grado']);

            $MedioEnt="";
            if($row_consulta['Id_medio_ent']>0){
              $medio=$DaoMediosEnterar->show($row_consulta['Id_medio_ent']);
              $MedioEnt=$medio->getMedio();
            }
                                
            $Medio_aux=$row_consulta['Id_medio_ent'];
            if($_POST['Id_medio']>0){
                $Medio_aux=$_POST['Id_medio'];
            }
            
            $Grado_aux=$Grado->getId();
            if($_POST['Id_grado']>0){
                $Grado_aux=$_POST['Id_grado'];
            }
            
            if($Grado->getId()==$Grado_aux && $row_consulta['Id_medio_ent']==$Medio_aux){
              ?>
                      <tr id-data="<?php echo $row_consulta['Id_ins'];?>" tipo="alumno">
                            <td><?php echo $count;?></td>
                            <td><?php echo $row_consulta['Matricula'] ?></td>
                            <td style="width: 115px;"><?php echo $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins'] ?></td>
                            <td><?php echo $oferta->getNombre_oferta(); ?></td>
                            <td><?php echo $esp->getNombre_esp(); ?></td>
                            <td><?php echo $nombre_ori; ?></td>
                            <td><input type="checkbox"> </td>
                      </tr>
                      <?php
                      $count++;
               }
            }while( $row_consulta = $consulta->fetch_assoc());
    }
    ?>
       </tbody>
    </table>
    <?php
}




if($_POST['action']=="get_profesores"){
    ?>
    <table class="table">
        <thead>
            <tr>
                <td>#</td>
                <td>Clave</td>
                <td>Nombre</td>
                <td>Telefono</td>
                <td>Email</td>
                <td class="td-center"><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
            </tr>
        </thead>
        <tbody>
            <?php

            $count=1;
            $DaoDocentes= new DaoDocentes();
            foreach($DaoDocentes->showAll() as $k=>$v){ 
            ?>
                <tr id-data="<?php echo $v->getId()?>" tipo="docente">
                     <td onclick="mostrar(<?php echo $v->getId()?>)"><?php echo $count;?></td>
                        <td><?php echo $v->getClave_docen();?></td>
                         <td><?php echo $v->getNombre_docen()." ".$v->getApellidoP_docen()." ".$v->getApellidoM_docen()?></td>
                         <td><?php echo $v->getTelefono_docen()?></td>
                         <td><?php echo $v->getEmail_docen()?></td>
                         <td class="td-center"><input type="checkbox"> </td>
                 </tr>
           <?php
           $count++; 
           }
            ?>
        </tbody>
    </table>
    <?php
}

if($_POST['action']=="get_alumnos"){
$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoGrados= new DaoGrados();
$DaoMediosEnterar= new DaoMediosEnterar();
?>
   <table class="table">
    <thead>
        <tr>
            <td>#</td>
            <td>Matricula</td>
            <td>Nombre</td>
            <td>Oferta</td>
            <td>Carrera</td>
            <td>Orientaci&oacute;n:</td>
            <td><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
        </tr>
    </thead>
    <tbody>
        <?php
        $count=1;
        foreach ($DaoAlumnos->getAlumnos() as $k=>$v){
           $nombre_ori="";
           $oferta = $DaoOfertas->show($v['Id_ofe']);
           $esp = $DaoEspecialidades->show($v['Id_esp']);
           if ($v['Id_ori'] > 0) {
              $ori = $DaoOrientaciones->show($v['Id_ori']);
              $nombre_ori = $ori->getNombre();
            }
            $opcion="Plan por materias"; 
            if($v['Opcion_pago']==2){
              $opcion="Plan completo";  
            }

            $Grado=$DaoOfertasAlumno->getLastCicloOfertaSinCiclo($v['Id_ofe_alum']);
            $Grado=$DaoGrados->show($Grado['Id_grado']);

            $MedioEnt="";
            if($v['Id_medio_ent']>0){
              $medio=$DaoMediosEnterar->show($v['Id_medio_ent']);
              $MedioEnt=$medio->getMedio();
            }

             ?>
                     <tr id-data="<?php echo $v['Id_ins'];?>" tipo="alumno">
                       <td><?php echo $count;?></td>
                       <td><?php echo $v['Matricula'] ?></td>
                       <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                       <td><?php echo $oferta->getNombre_oferta() ?></td>
                       <td><?php echo $esp->getNombre_esp(); ?></td>
                       <td><?php echo $nombre_ori; ?></td>
                       <td><input type="checkbox"> </td>
                     </tr>
                     <?php
                     $count++;
            }
              ?>
    </tbody>
</table>
       <?php
}



if($_POST['action']=="send_email"){
     $base = new base();
     $DaoAlumnos= new DaoAlumnos();
     $DaoDocentes= new DaoDocentes();
     $DaoGrupos= new DaoGrupos();
     $DaoUsuarios= new DaoUsuarios();
     $DaoAttachments= new DaoAttachments();
     $DaoPlanteles= new DaoPlanteles();
     $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
     $plantel=$DaoPlanteles->show($usu->getId_plantel());
     if($_POST['TipoRel']=="alumno"){
         $alum=$DaoAlumnos->show($_POST['IdRel']);
         //Enviar email al alumno
         if($_POST['Alumnos']==1){
            $nombre=mb_strtoupper($alum->getNombre()." "." ".$alum->getApellidoP()." ".$alum->getApellidoM(),"UTF-8");
            $email=$alum->getEmail();
            if(strlen($email)>0){
                    $arrayTo=array();
                    $arrayTo['Asunto']=$_POST['asunto'];
                    $arrayTo['Mensaje']=$_POST['mensaje'];
                    $arrayTo['IdRel']=$_COOKIE['admin/Id_usu'];
                    $arrayTo['TipoRel']="usu";
                    $arrayTo['email-usuario']= $plantel->getEmail();
                    $arrayTo['nombre-usuario']=$DaoPlanteles->covertirCadena($plantel->getNombre_plantel(),2);
                    $arrayTo['replyTo']=$usu->getEmail_usu();
                    $arrayTo['Destinatarios']=array();

                    $Data= array();
                    $Data['email']= $email;
                    //$Data['email']="christian310332@gmail.com";
                    $Data['name']= $nombre;

                    array_push($arrayTo['Destinatarios'], $Data);
                    $base->send_email($arrayTo);
            }
         }
         //Enviar email al tutor
         if($_POST['Tutores']==1){
            $nombre=mb_strtoupper($alum->getNombreTutor()." "." ".$alum->getApellidoP()." ".$alum->getApellidoM(),"UTF-8");
            $email=$alum->getEmailTutor();
            if(strlen($email)>0){
                    $arrayTo=array();
                    $arrayTo['Asunto']=$_POST['asunto'];
                    $arrayTo['Mensaje']=$_POST['mensaje'];
                    $arrayTo['IdRel']=$_COOKIE['admin/Id_usu'];
                    $arrayTo['TipoRel']="usu";
                    $arrayTo['email-usuario']= $plantel->getEmail();
                    $arrayTo['nombre-usuario']=$DaoPlanteles->covertirCadena($plantel->getNombre_plantel(),2);
                    $arrayTo['replyTo']=$usu->getEmail_usu();
                    $arrayTo['Destinatarios']=array();

                    $Data= array();
                    $Data['email']= $email;
                    //$Data['email']="christian310332@gmail.com";
                    $Data['name']= $nombre;

                    array_push($arrayTo['Destinatarios'], $Data);
                    $base->send_email($arrayTo);
            }
         }
         
         
     }
     
     if($_POST['TipoRel']=="docente"){
         $docen=$DaoDocentes->show($_POST['IdRel']);
         $nombre=mb_strtoupper($docen->getNombre_docen()." "." ".$docen->getApellidoP_docen()." ".$docen->getApellidoM_docen(),"UTF-8");
         $email=$docen->getEmail_docen();
         if(strlen($email)>0){
                    $arrayTo=array();
                    $arrayTo['Asunto']=$_POST['asunto'];
                    $arrayTo['Mensaje']=$_POST['mensaje'];
                    $arrayTo['IdRel']=$_COOKIE['admin/Id_usu'];
                    $arrayTo['TipoRel']="usu";
                    $arrayTo['email-usuario']= $plantel->getEmail();
                    $arrayTo['nombre-usuario']=$DaoPlanteles->covertirCadena($plantel->getNombre_plantel(),2);
                    $arrayTo['replyTo']=$usu->getEmail_usu();
                    $arrayTo['Destinatarios']=array();

                    $Data= array();
                    $Data['email']= $email;
                    //$Data['email']="christian310332@gmail.com";
                    $Data['name']= $nombre;

                    array_push($arrayTo['Destinatarios'], $Data);
                    $base->send_email($arrayTo);
            }
     }
     
     if($_POST['TipoRel']=="grupo"){
         
         foreach($DaoGrupos->getAlumnosBYGrupo($_POST['IdRel']) as $k2=>$v2){
                $alum=$DaoAlumnos->show($v2['Id_ins']);
                $nombre=mb_strtoupper($alum->getNombre()." "." ".$alum->getApellidoP()." ".$alum->getApellidoM(),"UTF-8");
                $email=$alum->getEmail();
                
                 if(strlen($email)>0){
                            $arrayTo=array();
                            $arrayTo['Asunto']=$_POST['asunto'];
                            $arrayTo['Mensaje']=$_POST['mensaje'];
                            $arrayTo['IdRel']=$_COOKIE['admin/Id_usu'];
                            $arrayTo['TipoRel']="usu";
                            $arrayTo['email-usuario']= $plantel->getEmail();
                            $arrayTo['nombre-usuario']=$DaoPlanteles->covertirCadena($plantel->getNombre_plantel(),2);
                            $arrayTo['replyTo']=$usu->getEmail_usu();
                            $arrayTo['Destinatarios']=array();

                            $Data= array();
                            $Data['email']= $email;
                            //$Data['email']="christian310332@gmail.com";
                            $Data['name']= $nombre;

                            array_push($arrayTo['Destinatarios'], $Data);
                            $base->send_email($arrayTo);
                 }   
         }
     }
     
}


if($_POST['action']=="mostrar_filtro_grupos"){
    $DaoCiclos= new DaoCiclos();
    ?>
        <h1>Filtro Grupos</h1>
        <p>Ciclo<br>
          <select id="ciclo">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <div class="boxUlBuscador">
            <p>Grupo<br><input type="search"  class="buscarFiltro" onkeyup="buscarGruposCiclo()" placeholder="Clave"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p><button onclick="filtroGrupo()">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
    <?php
}

if($_POST['action']=="get_grupos"){
$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoGrupos= new DaoGrupos();
$DaoMaterias= new DaoMaterias();
$DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
$DaoDocentes= new DaoDocentes();
$DaoUsuarios= new DaoUsuarios();
$_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
$DaoCiclos= new DaoCiclos();
$ciclo=$DaoCiclos->getActual($_usu->getId_plantel());
?>
<table class="table">
    <thead>
        <tr>
            <td style="width: 80px">CLAVE GRUPAL</td>
            <td>Ciclo</td>
            <td>MATERIA</td>
            <td>Orientaci&oacute;n</td>
            <td>TURNO</td>
            <td>PROFESOR</td>
            <td style="text-align: center;"><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
        </tr>
    </thead>
    <tbody>
        <?php

            if($ciclo->getId()>0){
            foreach($DaoGrupos->showGruposByCiclo($ciclo->getId()) as $k=>$v){
                    $mat = $DaoMaterias->show($v->getId_mat());
                    $mat_esp=$DaoMateriasEspecialidad->show($v->getId_mat_esp());

                    $turno="";
                    if($v->getTurno()==1){
                       $turno="MATUTINO";
                    }if($v->getTurno()==2){
                       $turno="VESPERTINO";
                    }if($v->getTurno()==3){
                       $turno="NOCTURNO";
                    }
                    $nombre_ori="";
                    if($v->getId_ori()>0){
                      $ori = $DaoOrientaciones->show($v->getId_ori());
                      $nombre_ori=$ori->getNombre();
                    }


                     $NombreMat=$mat->getNombre();
                     if(strlen($mat_esp->getNombreDiferente())>0){
                        $NombreMat=$mat_esp->getNombreDiferente(); 
                     }                                 
                     /*
                     foreach($DaoGrupos->getAlumnosBYGrupo($v->getId()) as $k2=>$v2){
                     }
                     */

                     $resp=$DaoGrupos->getDocenteGrupo($v->getId());
                     $NombreDocente="";
                     if($resp['Id_docente']>0){
                         $docente=$DaoDocentes->show($resp['Id_docente']);
                         $NombreDocente=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
                     }
            ?>
            <tr id-data="<?php echo $v->getId();?>" tipo="grupo">
                <td><?php echo $v->getClave()?> </td>
                <td><?php echo $ciclo->getClave()?> </td>
                <td style="width: 120px;"><?php echo $NombreMat?></td>
                <td><?php echo $nombre_ori;?></td>
                <td><?php echo $turno;?></td>
                <td><?php echo $NombreDocente?></td>
                <td style="text-align: center;"><input type="checkbox"> </td>
            </tr>
            <?php
            }
            }
        ?>
    </tbody>
</table>
       <?php
}



if($_POST['action']=="filtroGrupo"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrupos= new DaoGrupos();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoDocentes= new DaoDocentes();

    $DaoCiclos= new DaoCiclos();

    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $ciclo=$DaoCiclos->show($_POST['Id_ciclo']);
?>
<table class="table">
    <thead>
        <tr>
            <td style="width: 80px">CLAVE GRUPAL</td>
            <td>Ciclo</td>
            <td>MATERIA</td>
            <td>Orientaci&oacute;n</td>
            <td>TURNO</td>
            <td>PROFESOR</td>
            <td style="text-align: center;"><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
        </tr>
    </thead>
    <tbody>
        <?php

    $query_Grupos  = "SELECT * , Grupos.Id_grupo AS Grupo FROM Grupos 
            LEFT JOIN Materias_especialidades ON Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
            LEFT JOIN Horario_docente ON Grupos.Id_grupo=Horario_docente.Id_grupo
            LEFT JOIN Docentes ON Horario_docente.Id_docente=Docentes.Id_docen
            WHERE Grupos.Id_grupo=".$_POST['Id_grupo']."
               AND Grupos.Id_plantel=".$usu->getId_plantel()." 
               AND Activo_grupo=1 
               AND Grupos.Id_ciclo=".$_POST['Id_ciclo']."
               GROUP BY Grupos.Id_grupo ORDER BY Grupos.Id_grupo DESC";
     foreach($base->advanced_query($query_Grupos) as $k=>$v){
         
            $mat = $DaoMaterias->show($v['Id_mat']);
            $mat_esp=$DaoMateriasEspecialidad->show($v['Id_mat_esp']);

            $turno="";
            if($v['Turno']==1){
               $turno="MATUTINO";
            }if($v['Turno']==2){
               $turno="VESPERTINO";
            }if($v['Turno']==3){
               $turno="NOCTURNO";
            }
            $nombre_ori="";
            if($v['Id_ori']>0){
              $ori = $DaoOrientaciones->show($v['Id_ori']);
              $nombre_ori=$ori->getNombre();
            }
             $NombreMat=$mat->getNombre();
             if(strlen($mat_esp->getNombreDiferente())>0){
                $NombreMat=$mat_esp->getNombreDiferente(); 
             }                                 

             $resp=$DaoGrupos->getDocenteGrupo($v['Grupo']);
             $NombreDocente="";
             if($resp['Id_docente']>0){
                 $docente=$DaoDocentes->show($resp['Id_docente']);
                 $NombreDocente=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
             }
    ?>
    <tr id-data="<?php echo $v['Grupo'];?>" tipo="grupo">
        <td><?php echo $v['Clave']?> </td>
        <td><?php echo $ciclo->getClave()?> </td>
        <td style="width: 120px;"><?php echo $NombreMat?></td>
        <td><?php echo $nombre_ori;?></td>
        <td><?php echo $turno;?></td>
        <td><?php echo $NombreDocente?></td>
        <td style="text-align: center;"><input type="checkbox"> </td>
    </tr>
    <?php
    }
    ?>
    </tbody>
</table>
       <?php
}
