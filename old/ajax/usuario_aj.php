<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 


if(isset($_POST['action']) && $_POST['action']=="save_usu"){
    $DaoUsuarios= new DaoUsuarios();
    $DaoContactos= new DaoContactos();
    $DaoDirecciones= new DaoDirecciones();
    $DaoPermisosExtra= new DaoPermisosExtra();
    $DaoPermisosTipoUsuario= new DaoPermisosTipoUsuario();
    
    $resp= array();
    if(isset($_POST['Id_usu']) && $_POST['Id_usu']>0){
            //Si se cambio el tipo de usuario
            $_usu=$DaoUsuarios->show($_POST['Id_usu']);
            if($_usu->getTipo_usu()!=$_POST['tipo_usu']){
               $DaoPermisosExtra->deletePermisosUsuario($_POST['Id_usu']);

               //Insertammos los permisos por default del tipo de usuario
                foreach($DaoPermisosTipoUsuario->getPermisosTipoUsuario($_POST['tipo_usu']) as $perm){
                    $PermisosExtra= new PermisosExtra();
                    $PermisosExtra->setId_per($perm->getId_perm());
                    $PermisosExtra->setId_usu($_POST['Id_usu']);
                    $DaoPermisosExtra->add($PermisosExtra);	
                }
            }
            $Usuarios= $DaoUsuarios->show($_POST['Id_usu']);
            $Usuarios->setNombre_usu($_POST['nombre_usu']);
            $Usuarios->setApellidoP_usu($_POST['apellidoP_usu']);
            $Usuarios->setApellidoM_usu($_POST['apellidoM_usu']);
            $Usuarios->setClave_usu($_POST['clave_usu']);
            $Usuarios->setEmail_usu($_POST['email_usu']);
            $Usuarios->setTel_usu($_POST['tel_usu']);
            $Usuarios->setCel_usu($_POST['cel_usu']);
            $Usuarios->setId_plantel($_POST['plantel']);
            $Usuarios->setNivel_estudios($_POST['escolaridad']);
            $Usuarios->setTipo_usu($_POST['tipo_usu']);
            $Usuarios->setTipo_sangre($_POST['tipo_sangre']);
            $Usuarios->setAlergias($_POST['alergias_contacto']);
            $Usuarios->setEnfermedades_cronicas($_POST['cronicas_contacto']);
            $Usuarios->setPreinscripciones_medicas($_POST['preinscripciones_medicas_contacto']);
            $DaoUsuarios->update($Usuarios);


            if(isset($_POST['Id_contacto']) && $_POST['Id_contacto']>0){
                   $Contactos= $DaoContactos->show($_POST['Id_contacto']);
                   $Contactos->setNombre($_POST['nombre_contacto']);
                   $Contactos->setParentesco($_POST['parentesco_contacto']);
                   $Contactos->setDireccion($_POST['direccion_contacto']);
                   $Contactos->setTel_casa($_POST['tel_contacto']);
                   $Contactos->setTel_ofi($_POST['telOfic_contacto']);
                   $Contactos->setCel($_POST['celular_contacto']);
                   $Contactos->setEmail($_POST['email_contacto']);
                   $DaoContactos->update($Contactos);
            }elseif(strlen($_POST['nombre_contacto'])>0 && strlen($_POST['parentesco_contacto'])>0 && strlen($_POST['direccion_contacto'])>0){
                   $Contactos= new Contactos();
                   $Contactos->setNombre($_POST['nombre_contacto']);
                   $Contactos->setParentesco($_POST['parentesco_contacto']);
                   $Contactos->setDireccion($_POST['direccion_contacto']);
                   $Contactos->setTel_casa($_POST['tel_contacto']);
                   $Contactos->setTel_ofi($_POST['telOfic_contacto']);
                   $Contactos->setCel($_POST['celular_contacto']);
                   $Contactos->setEmail($_POST['email_contacto']);
                   $Contactos->setIdRel_con($_POST['Id_usu']);
                   $Contactos->setTipoRel_con("usu");
                   $DaoContactos->add($Contactos);  
            }
            
            //Crear historial
            $TextoHistorial="Actualiza los datos del usuario ".$_usu->getNombre_usu()." ".$_usu->getApellidoP_usu()." ".$_usu->getApellidoM_usu();
            $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Usuarios");

            $id_usu=$_POST['Id_usu'];
            $resp['tipo']="update";
            $resp['id']=$id_usu;
    }else{
        
        $Usuarios= new Usuarios();
        $Usuarios->setNombre_usu($_POST['nombre_usu']);
        $Usuarios->setApellidoP_usu($_POST['apellidoP_usu']);
        $Usuarios->setApellidoM_usu($_POST['apellidoM_usu']);
        $Usuarios->setClave_usu($_POST['clave_usu']);
        $Usuarios->setEmail_usu($_POST['email_usu']);
        $Usuarios->setTel_usu($_POST['tel_usu']);
        $Usuarios->setCel_usu($_POST['cel_usu']);
        $Usuarios->setId_plantel($_POST['plantel']);
        $Usuarios->setNivel_estudios($_POST['escolaridad']);
        $Usuarios->setTipo_usu($_POST['tipo_usu']);
        $Usuarios->setAlta_usu(date('Y-m-d H:i:s'));
        $Usuarios->setTipo_sangre($_POST['tipo_sangre']);
        $Usuarios->setAlergias($_POST['alergias_contacto']);
        $Usuarios->setEnfermedades_cronicas($_POST['cronicas_contacto']);
        $Usuarios->setPreinscripciones_medicas($_POST['preinscripciones_medicas_contacto']);
        $id_usu=$DaoUsuarios->add($Usuarios);
         
        if(strlen($_POST['nombre_contacto'])>0 && strlen($_POST['parentesco_contacto'])>0 && strlen($_POST['direccion_contacto'])>0){
           $Contactos= new Contactos();
           $Contactos->setNombre($_POST['nombre_contacto']);
           $Contactos->setParentesco($_POST['parentesco_contacto']);
           $Contactos->setDireccion($_POST['direccion_contacto']);
           $Contactos->setTel_casa($_POST['tel_contacto']);
           $Contactos->setTel_ofi($_POST['telOfic_contacto']);
           $Contactos->setCel($_POST['celular_contacto']);
           $Contactos->setEmail($_POST['email_contacto']);
           $Contactos->setIdRel_con($id_usu);
           $Contactos->setTipoRel_con("usu");
           $DaoContactos->add($Contactos);  
        }
        
        //Insertammos los permisos por default del tipo de usuario
        foreach($DaoPermisosTipoUsuario->getPermisosTipoUsuario($_POST['tipo_usu']) as $perm){
            $PermisosExtra= new PermisosExtra();
            $PermisosExtra->setId_per($perm->getId_perm());
            $PermisosExtra->setId_usu($id_usu);
            $DaoPermisosExtra->add($PermisosExtra);	
        }

        //Crear historial
        $_usu=$DaoUsuarios->show($id_usu);
        $TextoHistorial="Añade como usuario nuevo a ".$_usu->getNombre_usu()." ".$_usu->getApellidoP_usu()." ".$_usu->getApellidoM_usu();
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Usuarios");


        $resp['tipo']="new";
        $resp['id']=$id_usu;	
    }
    echo json_encode($resp);
}


if(isset($_GET['action']) && $_GET['action']=="upload_image"){
    $base= new base();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_GET['Id_usu']);
    $key=$base->generarKey();
    
    if(count($_FILES)>0) { 
       $type=substr($_FILES["file"]["name"],strpos($_FILES["file"]["name"],".")+1);
    if (($type == "gif" || $type == "jpeg" || $type == "png" || $type == "jpg" )){

      if ($_FILES["file"]["error"] > 0){

          echo "Return Code: " . $_FILES["file"]["error"] . "<br />";

        }else{

                if(file_exists("../files/".$usu->getImg_usu().".jpg")){
                        unlink("../files/".$usu->getImg_usu().".jpg");
                }
                $usu->setImg_usu($key);
                $DaoUsuarios->update($usu);

                move_uploaded_file($_FILES["file"]["tmp_name"],"../files/".$key.".jpg");

                $sourcefile='../files/'.$key.'.jpg';
                $picsize=getimagesize($sourcefile);

                $source_x  = $picsize[0]*1;
                $source_y  = $picsize[1]*1;

                //modificar el tamaño de la imagen
                $result1 = $base->resizeToFile ($sourcefile, 300, 300*$source_y/$source_x, $sourcefile, 80);

                echo $key;
        }

      }else{
            echo "Invalid file: ".$type;
      }


    }elseif(isset($_GET['action'])){
            if (($_GET['TypeFile'] == "image/gif" || $_GET['TypeFile'] == "image/jpeg" || $_GET['TypeFile'] == "image/png" )){
              if ($_FILES["file"]["error"] > 0){

                  echo "Return Code: " . $_FILES["file"]["error"] . "<br />";

                }else{
                    
                    if(file_exists("../files/".$usu->getImg_usu().".jpg")){
                            unlink("../files/".$usu->getImg_usu().".jpg");
                    }
                    
                    $usu->setImg_usu($key);
                    $DaoUsuarios->update($usu);


                    if(isset($_GET['base64'])) {
                        // If the browser does not support sendAsBinary ()
                            $content = base64_decode(file_get_contents('php://input'));
                    } else {
                            $content = file_get_contents('php://input');
                    }
                    file_put_contents('../files/'.$key.'.jpg', $content);

                    $sourcefile='../files/'.$key.'.jpg';
                    $picsize=getimagesize($sourcefile);

                    $source_x  = $picsize[0]*1;
                    $source_y  = $picsize[1]*1;

                    //modificar el tamaño de la imagen
                    $result1 = $base->resizeToFile ($sourcefile, 300, 300*$source_y/$source_x, $sourcefile, 80);

                    echo $key;
              } 

            }else {
                    //print_r($_FILES);
                    //print_r($_GET);
                    echo "Invalid file: ".$_GET['TypeFile'];
            }
    }

}

if(isset($_POST['action']) && $_POST['action']=="update_pag"){
   update_page($_POST['Id_usu']);
}


function update_page($Id_usu){
    $Daousuarios= new DaoUsuarios();
    $_usu=$Daousuarios->show($_COOKIE['admin/Id_usu']);
    
    $DaoPlanteles= new DaoPlanteles();
    $DaoTiposUsuarios= new DaoTiposUsuarios();
    $DaoContactos= new DaoContactos();

    $Img_usu="";
    $Nombre_usu="";
    $ApellidoP_usu="";
    $IApellidoM_usu="";
    $Clave_usu="";
    $Email_usu="";
    $Tel_usu="";
    $Cel_usu="";
    $Nivel_estudios="";
    $Id_plantel="";
    $Tipo_usu="";
    $Tipo_sangre="";
    $Alergias="";
    $Enfermedades_cronicas="";
    $Preinscripciones_medicas="";

    $ContactoNombre="";
    $ContactoParentesco="";
    $ContactoDireccion="";
    $ContactoTel_casa="";
    $ContactoTel_ofi="";
    $ContactoCel="";
    $ContactoEmail="";
    $ContactoId_cont="";

    ?>
    <table id="tabla">
        <?php
        if (isset($Id_usu) && $Id_usu > 0) {
            $usu_sis = $Daousuarios->show($Id_usu);
            $Id_usu=$usu_sis->getId();
            $Img_usu=$usu_sis->getImg_usu();
            $Nombre_usu=$usu_sis->getNombre_usu();
            $ApellidoP_usu=$usu_sis->getApellidoP_usu();
            $ApellidoM_usu=$usu_sis->getApellidoM_usu();
            $Clave_usu=$usu_sis->getClave_usu();
            $Email_usu=$usu_sis->getEmail_usu();
            $Tel_usu=$usu_sis->getTel_usu();
            $Cel_usu=$usu_sis->getCel_usu();
            $Nivel_estudios=$usu_sis->getNivel_estudios();
            $Id_plantel=$usu_sis->getId_plantel();
            $Tipo_usu=$usu_sis->getTipo_usu();
            $Tipo_sangre=$usu_sis->getTipo_sangre();
            $Alergias=$usu_sis->getAlergias();
            $Enfermedades_cronicas=$usu_sis->getEnfermedades_cronicas();
            $Preinscripciones_medicas=$usu_sis->getPreinscripciones_medicas();

            $contacto=$DaoContactos->getPrimerContacto($Id_usu,'usu');
            $ContactoNombre=$contacto->getNombre();
            $ContactoParentesco=$contacto->getParentesco();
            $ContactoDireccion=$contacto->getDireccion();
            $ContactoTel_casa=$contacto->getTel_casa();
            $ContactoTel_ofi=$contacto->getTel_ofi();
            $ContactoCel=$contacto->getCel();
            $ContactoEmail=$contacto->getEmail();
            $ContactoId_cont=$contacto->getId();
        }
        ?>
        <tr>
            <td id="column_one">
                <div class="fondo">
                    <div id="box_top">
                        <div class="foto_alumno" 
                        <?php if (strlen($Img_usu) > 0) { ?> 
                                 style="background-image:url(files/<?php echo $Img_usu ?>.jpg) <?php } ?>">
                        </div>
                        <h1><?php echo ucwords(strtolower($Nombre_usu . " " . $ApellidoP_usu . " " . $ApellidoM_usu)) ?></h1>
                    </div>
                    <div class="seccion">
                        <h2>Datos Personales</h2>
                        <span class="linea"></span>
                        <ul class="form">
                            <li>Clave<br><input type="text" value="<?php echo $Clave_usu ?>" id="clave_usu"/></li>
                            <li>Nombre<br><input type="text" value="<?php echo $Nombre_usu ?>" id="nombre_usu"/></li>
                            <li>Apellido Paterno<br><input type="text" value="<?php echo $ApellidoP_usu ?>" id="apellidoP_usu"/></li>
                            <li>Apellido Materno<br><input type="text" value="<?php echo $ApellidoM_usu ?>" id="apellidoM_usu"/></li>
                            <li>Email<br><input type="email" id="email_usu" value="<?php echo $Email_usu ?>"/></li>
                            <li>Tel&eacute;fono Casa<br><input type="tel" id="tel_usu" value="<?php echo $Tel_usu ?>"/></li>
                            <li>Tel&eacute;fono Movil<br><input type="tel" id="cel_usu" value="<?php echo $Cel_usu ?>"/></li>
                            <li>Ultimo Grado de Estudios<br>
                                <select id="escolaridad">
                                    <option value="0"></option>
                                    <option value="1" <?php if ($Nivel_estudios == 1) { ?> selected="selected"<?php } ?>>Primaria</option>
                                    <option value="2" <?php if ($Nivel_estudios == 2) { ?> selected="selected"<?php } ?>>Secundaria</option>
                                    <option value="3" <?php if ($Nivel_estudios == 3) { ?> selected="selected"<?php } ?>>Preparatoria</option>
                                    <option value="4" <?php if ($Nivel_estudios == 4) { ?> selected="selected"<?php } ?>>Universidad</option>
                                    <option value="5" <?php if ($Nivel_estudios == 5) { ?> selected="selected"<?php } ?>>Posgrado</option>
                                </select>
                            </li>
                            <li>Plantel<br>
                                <select id="plantel">
                                    <option value="0"></option>
                                    <?php
                                    foreach($DaoPlanteles->showAll() as $plantel){
                                            ?>
                                            <option value="<?php echo $plantel->getId() ?>" <?php  if($plantel->getId()==$Id_plantel){ ?> selected="selected" <?php } ?> ><?php echo $plantel->getNombre_plantel() ?></option>
                                           <?php
                                    }
                                    ?>
                                </select>
                            </li>
                            <li>Tipo de usuario<br>
                                <select id="tipo_usu">
                                    <option value="0"></option>
                                    <?php
                                    foreach($DaoTiposUsuarios->getTiposUsuarios() as $tipo){
                                        ?>
                                        <option value="<?php echo $tipo->getId() ?>" <?php  if($tipo->getId()==$Tipo_usu){ ?> selected="selected" <?php } ?> ><?php echo $tipo->getNombre_tipo() ?></option>
                                        <?php    
                                    }
                                    ?>
                                </select>
                            </li>
                        </ul>
                    </div>
                    <div class="seccion">
                        <h2>Datos de Emergencia</h2>
                        <span class="linea"></span>
                        <ul class="form">
                            <li>Tipo de sangre<br><input type="text"  id="tipo_sangre" value="<?php echo $Tipo_sangre ?>"/></li>
                            <li>Nombre del contacto<br><input type="text" id="nombre_contacto" value="<?php echo $ContactoNombre ?>"/></li>
                            <li>Parentesco<br><input type="text" id="parentesco_contacto" value="<?php echo $ContactoParentesco ?>"/></li>
                            <li>Direcci&oacute;n<br><input type="text" id="direccion_contacto" value="<?php echo $ContactoDireccion ?>"/></li>
                            <li>Tel&eacute;fono Casa<br><input type="text" id="tel_contacto" value="<?php echo $ContactoTel_casa ?>"/></li>
                            <li>Tel&eacute;fono Oficina<br><input type="text" id="telOfic_contacto" value="<?php echo $ContactoTel_ofi ?>"/></li>
                            <li>Celular<br><input type="text" id="celular_contacto" value="<?php echo $ContactoCel ?>"/></li>
                            <li>Email<br><input type="text" id="email_contacto" value="<?php echo $ContactoEmail ?>"/></li>
                        </ul>
                        <input type="hidden" id="Id_contacto" value="<?php echo $ContactoId_cont ?>"/>
                    </div>
                    <div class="seccion">
                        <ul class="form">
                            <li>Alergias<br><textarea  id="alergias_contacto"><?php echo $Alergias ?></textarea></li>
                            <li>Enfermedades cr&oacute;nicas<br><textarea  id="cronicas_contacto"><?php echo $Enfermedades_cronicas ?></textarea></li>
                            <li>Pre-escripciones medicas<br><textarea  id="preinscripciones_medicas_contacto"><?php echo $Preinscripciones_medicas ?></textarea></li>
                        </ul>
                    </div>
                    <button class="btns btns-primary btns-lg" id="button_ins" onclick="save_usu()">Guardar</button>
                </div>
            </td>
            <td id="column_two">
                <div id="box_menus">
                    <?php
                    require_once '../estandares/menu_derecho.php';
                    ?>
                    <ul>
                        <li><a href="usuario.php" class="link"><i class="fa fa-user-plus"></i> Nuevo</a></li>
                        <?php
                        if ($Id_usu > 0) {

                            ?>
                            <li><span onclick="box_documents()"><i class="fa fa-file-o"></i> Documentos</span></li>
                            <li><span onclick="box_perm()"><i class="fa fa-eye"></i> Permisos</span></li>
                            <li><span onclick="box_pass()"><i class="fa fa-ellipsis-h"></i> Cambiar contrase&ntilde;a</span></li>
                            <li><span onclick="mostrarFinder()"><i class="fa fa-camera"></i> A&ntilde;adir fotografia</span></li>
                            <?php
                        }
                        ?>
                    </ul>
                    <input type="file" id="files" name="files" multiple="">
                </div>
            </td>
        </tr>
    </table>
    <input type="hidden" id="Id_usu" value="<?php echo $Id_usu ?>"/>
	<?php
}


if(isset($_POST['action']) && $_POST['action']=="save_pass"){
    $DaoUsuarios= new DaoUsuarios();
    $base= new base();
    $tipo_error=array();
    if($_POST['pass']==$_POST['conf']){
        if(strlen($_POST['pass'])>6){
                $tipo_error['tipo']="ok";
                $usu=$DaoUsuarios->show($_POST['Id_usu']);
                $usu->setPass_usu($base->hashPassword($_POST['pass']));
                $DaoUsuarios->update($usu);
                
                //Crear historial
                $TextoHistorial="Actualiza la contraseña para el usuario ".$usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
                $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Usuarios");
    
        }else{
                $tipo_error['tipo']="password_security";
        }
    }else{
            $tipo_error['tipo']="password_match";
    }
    echo json_encode($tipo_error);
}


if(isset($_POST['action']) && $_POST['action']=="box_pass"){
?>
    <div class="box-emergente-form box-cambiar-pass">
        <h1>Cambiar contrase&ntilde;a</h1>
        <p>Contrase&ntilde;a<br><input type="password" id="pass"/></p>
        <p>Confirmar contrase&ntilde;a:<br><input type="password" id="conf"/></p>
        <p><button onclick="save_pass()">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
    </div>
<?php
}

if(isset($_POST['action']) && $_POST['action']=="box_documents"){
   $DaoUsuarios= new DaoUsuarios();
   $usu=$DaoUsuarios->show($_POST['Id_usu']);
?>
    <div id="box_emergente"><h1>Documentos del usuario</h1>
      <table class="files_ofe">
         <thead>
                    <td>Documento</td>
                    <td>Original</td>
                    <td>Copia</td>
         </thead>
             <tr>
                     <td>CURP</td>
                     <td><input type="checkbox" <?php if($usu->getDoc_CURP()>0){ ?> checked="checked" <?php } ?> id="doc_CURP"></td>
                     <td><input type="checkbox" <?php if($usu->getCopy_CURP()>0){ ?> checked="checked" <?php } ?> id="copy_CURP" ></td>
             </tr>
             <tr>
                     <td>IMSS</td>
                     <td><input type="checkbox"  <?php if($usu->getDoc_IMSS()>0){ ?> checked="checked" <?php } ?> id="doc_IMSS"></td>
                     <td><input type="checkbox" <?php if($usu->getCopy_IMSS()>0){ ?> checked="checked" <?php } ?> id="copy_IMSS" ></td>
             </tr>
            <tr>
                     <td>RFC</td>
                     <td><input type="checkbox" <?php if($usu->getDoc_RFC()>0){ ?> checked="checked" <?php } ?> id="doc_RFC"></td>
                     <td><input type="checkbox" <?php if($usu->getCopy_RFC()>0){ ?> checked="checked" <?php } ?> id="copy_RFC" ></td>
             </tr>

      </table>
      <p><button onclick="save_documentos_usu()">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
    </div>
<?php
}


if(isset($_POST['action']) && $_POST['action']=="box_perm"){
    $base= new base();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_POST['Id_usu']);
    ?>
    <div id="box_emergente_perm">
         <h1>Permisos del usuario</h1>
         <span class="linea"></span>
         <ul id="list_perm">
             <?php
              $query = "SELECT Id_perm,Nombre_perm,Id_usu  
                      FROM permisos_ulm LEFT JOIN (SELECT * FROM permisos_extra_usu WHERE  Id_usu=".$usu->getId().") AS permisosUsuario ON permisos_ulm.Id_perm=permisosUsuario.Id_per";
              foreach($base->advanced_query($query) as $row_permisos){
                   ?>
                   <li>
                       <table>
                           <tbody>
                               <tr>
                                   <td><?php echo $row_permisos['Nombre_perm'];?></td>
                                   <td><input type="checkbox" <?php if($row_permisos['Id_usu']!=null){?> checked="checked" <?php } ?> value="<?php echo $row_permisos['Id_perm']?>"/></td>
                               </tr>
                           </tbody>
                       </table>
                   </li>
                  <?php
               }   
             ?>
            </ul>
            <p><button onclick="save_perm()">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
      </div>
    <?php	
}


if(isset($_POST['action']) && $_POST['action']=="save_documentos_usu"){
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_POST['Id_usu']);
    $usu->setDoc_RFC($_POST['doc_RFC']);
    $usu->setCopy_RFC($_POST['copy_RFC']);
    $usu->setDoc_IMSS($_POST['doc_IMSS']);
    $usu->setCopy_IMSS($_POST['copy_IMSS']);
    $usu->setDoc_CURP($_POST['doc_CURP']);
    $usu->setCopy_CURP($_POST['copy_CURP']);
    $DaoUsuarios->update($usu);

    //Crear historial
    $TextoHistorial="Actualiza los documentos para el usuario ".$usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Usuarios");
}


if(isset($_POST['action']) && $_POST['action']=="save_perm"){
   $DaoPermisosExtra= new DaoPermisosExtra();
   $DaoPermisosExtra->deletePermisosUsuario($_POST['Id_usu']);
      	
   foreach($_POST['perm'] as $v){ 	
       $PermisosExtra= new PermisosExtra();
       $PermisosExtra->setId_per($v);
       $PermisosExtra->setId_usu($_POST['Id_usu']);
       $DaoPermisosExtra->add($PermisosExtra);
    }	
    
    //Crear historial
    $DaoUsuarios= new DaoUsuarios();
    $_usu=$DaoUsuarios->show($_POST['Id_usu']);

    $TextoHistorial="Actualiza lista de permisos para el usuario ".$_usu->getNombre_usu()." ".$_usu->getApellidoP_usu()." ".$_usu->getApellidoM_usu();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Usuarios");
}