<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="filtro"){
    $DaoAccionesInteresado= new DaoAccionesInteresado();
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoUsuarios= new DaoUsuarios();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query="";

    if(strlen($_POST['fecha_ini'])>0 && $_POST['fecha_fin']==null){
        $query=" AND FechaAccion>='".$_POST['fecha_ini']."'";
    }elseif(strlen($_POST['fecha_fin'])>0 && $_POST['fecha_ini']==null){
        $query=" AND FechaAccion<='".$_POST['fecha_fin']."'";
    }elseif(strlen($_POST['fecha_ini'])>0 && strlen($_POST['fecha_fin'])>0){
        $query=" AND (FechaAccion>='".$_POST['fecha_ini']."' AND FechaAccion<='".$_POST['fecha_fin']."')";
    }
    if($_POST['Tipo']>0){
        $query=$query." AND TipoAccion=".$_POST['Tipo'];
    }
    
    $query = "SELECT * FROM Acciones_interesado WHERE Asistio IS NULL AND Plantel=".$usu->getId_plantel()." ".$query." ORDER BY Id_int";
     foreach($base->advanced_query($query) as $k=>$v){
            $int=$DaoAlumnos->show($v['Id_int']);
            $ofe_alum=$DaoOfertasAlumno->show($v['Id_ofe_alum']);
if($ofe_alum->getId_ofe()>0){
            $oferta=$DaoOfertas->show($ofe_alum->getId_ofe());
            $esp = $DaoEspecialidades->show($ofe_alum->getId_esp());
            $nombre_ori="";
            if ($ofe_alum->getId_ori() > 0) {
              $ori = $DaoOrientaciones->show($ofe_alum->getId_ori());
              $nombre_ori = $ori->getNombre();
            }

            $usuAtendio=$DaoUsuarios->show($v['Usuario']);
            $TipoAccion="";
            if($v['TipoAccion']=="1"){
               $TipoAccion="Examen"; 
            }elseif($v['TipoAccion']=="2"){
               $TipoAccion="Sesi&oacuten informativa";  
            }elseif($v['TipoAccion']=="2"){
                $TipoAccion="Clase de muestra"; 
            }
            
            $Oferta_aux=$ofe_alum->getId_ofe();
            if($_POST['Id_oferta']>0){
                $Oferta_aux=$_POST['Id_oferta'];
            }
            $Esp_aux=$ofe_alum->getId_esp();
            if($_POST['Id_esp']>0){
                $Esp_aux=$_POST['Id_esp'];
            }
            $Ori_aux=$ofe_alum->getId_ori();
            if($_POST['Id_ori']>0){
                $Ori_aux=$_POST['Id_ori'];
            }

             if($ofe_alum->getId_ofe()==$Oferta_aux && $ofe_alum->getId_esp()==$Esp_aux && $ofe_alum->getId_ori()==$Ori_aux){
        ?>
        <tr>
            <td><?php echo $TipoAccion?> </td>
            <td><?php echo $int->getNombre()." ".$int->getApellidoP()." ".$int->getApellidoM()?> </td>
            <td><?php echo  $oferta->getNombre_oferta(); ?></td>
            <td><?php echo $esp->getNombre_esp(); ?></td>
            <td><?php echo $nombre_ori; ?></td>
            <td><?php echo $v['FechaAccion'];?></td>
            <td><?php echo $v['FechaSession']?></td>
            <td><?php echo $usuAtendio->getNombre_usu()." ".$usuAtendio->getApellidoP_usu()." ".$usuAtendio->getApellidoM_usu()?></td>
            <td><button onclick="save_asistio(<?php echo $v['Id_accion']?>,this)">Guardar</button></td>
        </tr>
       <?php
        }
}
    }
}

if($_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'ACCIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'OFERTA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'ESPECIALIDAD');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'ORIENTACIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'FECHA ACCIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'FECHA CITA');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'USUARIO');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','I') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }

    $DaoAccionesInteresado= new DaoAccionesInteresado();
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoUsuarios= new DaoUsuarios();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query="";

    if(strlen($_GET['fecha_ini'])>0 && $_GET['fecha_fin']==null){
        $query=" AND FechaAccion>='".$_GET['fecha_ini']."'";
    }elseif(strlen($_GET['fecha_fin'])>0 && $_GET['fecha_ini']==null){
        $query=" AND FechaAccion<='".$_GET['fecha_fin']."'";
    }elseif(strlen($_GET['fecha_ini'])>0 && strlen($_GET['fecha_fin'])>0){
        $query=" AND (FechaAccion>='".$_GET['fecha_ini']."' AND FechaAccion<='".$_GET['fecha_fin']."')";
    }
    if($_GET['Tipo']>0){
        $query=$query." AND TipoAccion=".$_GET['Tipo'];
    }
    
     $count=1;
     $query = "SELECT * FROM Acciones_interesado WHERE Asistio IS NULL AND Plantel=".$usu->getId_plantel()." ".$query." ORDER BY Id_int";
     foreach($base->advanced_query($query) as $k=>$v){
        $int=$DaoAlumnos->show($v['Id_int']);
        
        $ofe_alum=$DaoOfertasAlumno->show($v['Id_ofe_alum']);
if($ofe_alum->getId_ofe()>0){
        $oferta=$DaoOfertas->show($ofe_alum->getId_ofe());
        $esp = $DaoEspecialidades->show($ofe_alum->getId_esp());
        $nombre_ori="";
        if ($ofe_alum->getId_ori() > 0) {
          $ori = $DaoOrientaciones->show($ofe_alum->getId_ori());
          $nombre_ori = $ori->getNombre();
        }

        $usuAtendio=$DaoUsuarios->show($v['Usuario']);
        $TipoAccion="";
        if($v['TipoAccion']=="1"){
           $TipoAccion="Examen"; 
        }elseif($v['TipoAccion']=="2"){
           $TipoAccion="Sesión informativa";  
        }elseif($v['TipoAccion']=="2"){
            $TipoAccion="Clase de muestra"; 
        }

        $Oferta_aux=$ofe_alum->getId_ofe();
        if($_GET['Id_oferta']>0){
            $Oferta_aux=$_GET['Id_oferta'];
        }
        $Esp_aux=$ofe_alum->getId_esp();
        if($_GET['Id_esp']>0){
            $Esp_aux=$_GET['Id_esp'];
        }
        $Ori_aux=$ofe_alum->getId_ori();
        if($_GET['Id_ori']>0){
            $Ori_aux=$_GET['Id_ori'];
        }
        if($ofe_alum->getId_ofe()==$Oferta_aux && $ofe_alum->getId_esp()==$Esp_aux && $ofe_alum->getId_ori()==$Ori_aux){
        $count++;
        $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
        $objPHPExcel->getActiveSheet()->setCellValue("B$count", $TipoAccion);
        $objPHPExcel->getActiveSheet()->setCellValue("C$count", $int->getNombre()." ".$int->getApellidoP()." ".$int->getApellidoM());
        $objPHPExcel->getActiveSheet()->setCellValue("D$count", $oferta->getNombre_oferta());
        $objPHPExcel->getActiveSheet()->setCellValue("E$count", $esp->getNombre_esp());
        $objPHPExcel->getActiveSheet()->setCellValue("F$count", $nombre_ori);
        $objPHPExcel->getActiveSheet()->setCellValue("G$count", $v['FechaAccion']);
        $objPHPExcel->getActiveSheet()->setCellValue("H$count", $v['FechaSession']);
        $objPHPExcel->getActiveSheet()->setCellValue("I$count", $usuAtendio->getNombre_usu()." ".$usuAtendio->getApellidoP_usu()." ".$usuAtendio->getApellidoM_usu());
        }
}
    }
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=Acciones-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}


if($_POST['action']=="save_asistio"){
    $DaoAccionesInteresado= new DaoAccionesInteresado();
    $accion=$DaoAccionesInteresado->show($_POST['Id_accion']);
    $accion->setAsistio(1);
    $DaoAccionesInteresado->update($accion);
}



