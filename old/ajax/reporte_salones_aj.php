<?php
require_once('activate_error.php');
require_once('../require_daos.php');


if ($_POST['action'] == "mostrar_horario") {
    update_page($_POST['Id_aula'], $_POST['Id_ciclo']);
}

function update_page($Id_aula, $Id_ciclo) {
    $base = new base();
    $DaoHoras = new DaoHoras();
    $DaoUsuarios = new DaoUsuarios();
    $DaoDiasPlantel = new DaoDiasPlantel();

    $paramDias = array();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    foreach ($DaoHoras->showAll() as $hora) {

        foreach ($DaoDiasPlantel->getJoinDiasPlantel($usu->getId_plantel()) as $dia) {
            $paramDias[$dia['Id_dia']]['disp_docente'] = "";
            $paramDias[$dia['Id_dia']]['nombre_materia'] = "";
            $paramDias[$dia['Id_dia']]['aula_materia'] = "";
            $paramDias[$dia['Id_dia']]['Id_grupo'] = "";
            $paramDias[$dia['Id_dia']]['Id_mat'] = "";
            $paramDias[$dia['Id_dia']]['Id_aula'] = "";
            $paramDias[$dia['Id_dia']]['clave'] = "";
        }

        $query = "SELECT HD.*,Gp.Clave,Gp.Id_mat,Materias.Nombre,Docentes.Nombre_docen,Docentes.ApellidoP_docen FROM Horario_docente as HD 
                   JOIN Grupos as Gp ON HD.Id_grupo=Gp.Id_grupo 
                   JOIN materias_uml as Materias ON Gp.Id_mat= Materias.Id_mat
                   JOIN Docentes ON HD.Id_docente= Docentes.Id_docen
                WHERE Hora= " . $hora->getId() . " AND HD.Id_ciclo=" . $Id_ciclo . " AND Aula=" . $Id_aula;
        foreach ($base->advanced_query($query) as $row_Horario_docente) {

            if ($row_Horario_docente['Lunes'] == 1) {
                $paramDias[1]['disp_docente'] = 'class="horas_docente_asignadas"';
                $paramDias[1]['nombre_materia'] = $row_Horario_docente['Nombre'];
                $paramDias[1]['aula_materia'] = $row_Horario_docente['Nombre_docen'];
                $paramDias[1]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                $paramDias[1]['Id_mat'] = $row_Horario_docente['Id_mat'];
                $paramDias[1]['Id_aula'] = $row_Horario_docente['Aula'];
                $paramDias[1]['clave'] = $row_Horario_docente['Clave'];
            }
            if ($row_Horario_docente['Martes'] == 1) {
                $paramDias[2]['disp_docente'] = 'class="horas_docente_asignadas"';
                $paramDias[2]['nombre_materia'] = $row_Horario_docente['Nombre'];
                $paramDias[2]['aula_materia'] = $row_Horario_docente['Nombre_docen'];
                $paramDias[2]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                $paramDias[2]['Id_mat'] = $row_Horario_docente['Id_mat'];
                $paramDias[2]['Id_aula'] = $row_Horario_docente['Aula'];
                $paramDias[2]['clave'] = $row_Horario_docente['Clave'];
            }
            if ($row_Horario_docente['Miercoles'] == 1) {
                $paramDias[3]['disp_docente'] = 'class="horas_docente_asignadas"';
                $paramDias[3]['nombre_materia'] = $row_Horario_docente['Nombre'];
                $paramDias[3]['aula_materia'] = $row_Horario_docente['Nombre_docen'];
                $paramDias[3]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                $paramDias[3]['Id_mat'] = $row_Horario_docente['Id_mat'];
                $paramDias[3]['Id_aula'] = $row_Horario_docente['Aula'];
                $paramDias[3]['clave'] = $row_Horario_docente['Clave'];
            }
            if ($row_Horario_docente['Jueves'] == 1) {
                $paramDias[4]['disp_docente'] = 'class="horas_docente_asignadas"';
                $paramDias[4]['nombre_materia'] = $row_Horario_docente['Nombre'];
                $paramDias[4]['aula_materia'] = $row_Horario_docente['Nombre_docen'];
                $paramDias[4]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                $paramDias[4]['Id_mat'] = $row_Horario_docente['Id_mat'];
                $paramDias[4]['Id_aula'] = $row_Horario_docente['Aula'];
                $paramDias[4]['clave'] = $row_Horario_docente['Clave'];
            }
            if ($row_Horario_docente['Viernes'] == 1) {
                $paramDias[5]['disp_docente'] = 'class="horas_docente_asignadas"';
                $paramDias[5]['nombre_materia'] = $row_Horario_docente['Nombre'];
                $paramDias[5]['aula_materia'] = $row_Horario_docente['Nombre_docen'];
                $paramDias[5]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                $paramDias[5]['Id_mat'] = $row_Horario_docente['Id_mat'];
                $paramDias[5]['Id_aula'] = $row_Horario_docente['Aula'];
                $paramDias[5]['clave'] = $row_Horario_docente['Clave'];
            }
            if ($row_Horario_docente['Sabado'] == 1) {
                $paramDias[6]['disp_docente'] = 'class="horas_docente_asignadas"';
                $paramDias[6]['nombre_materia'] = $row_Horario_docente['Nombre'];
                $paramDias[6]['aula_materia'] = $row_Horario_docente['Nombre_docen'];
                $paramDias[6]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                $paramDias[6]['Id_mat'] = $row_Horario_docente['Id_mat'];
                $paramDias[6]['Id_aula'] = $row_Horario_docente['Aula'];
                $paramDias[6]['clave'] = $row_Horario_docente['Clave'];
            }

            if ($row_Horario_docente['Domingo'] == 1) {
                $paramDias[7]['disp_docente'] = 'class="horas_docente_asignadas"';
                $paramDias[7]['nombre_materia'] = $row_Horario_docente['Nombre'];
                $paramDias[7]['aula_materia'] = $row_Horario_docente['Nombre_docen'];
                $paramDias[7]['Id_grupo'] = $row_Horario_docente['Id_grupo'];
                $paramDias[7]['Id_mat'] = $row_Horario_docente['Id_mat'];
                $paramDias[7]['Id_aula'] = $row_Horario_docente['Aula'];
                $paramDias[7]['clave'] = $row_Horario_docente['Clave'];
            }
        }
        ?>
        <tr id_hora="<?php echo $hora->getId() ?>">
            <td><?php echo $hora->getTexto_hora() ?></td>
        <?php
        foreach ($DaoDiasPlantel->getJoinDiasPlantel($usu->getId_plantel()) as $dia) {
            $id = $dia['Id_dia'];
            $onclick="";
            if(isset($paramDias[$id]['Id_grupo']) && $paramDias[$id]['Id_grupo']>0){
               $onclick='onclick="mostrarCalificacionesGrupo('.$paramDias[$id]["Id_grupo"].')"'; 
            }
            ?>
                <td <?php echo $paramDias[$id]['disp_docente'] ?> <?php echo $onclick;?>><span><?php echo $paramDias[$id]['nombre_materia']; ?></span><br><?php echo $paramDias[$id]['aula_materia'] ?><br><span><?php echo $paramDias[$id]['clave']; ?></span></td>
                <?php
            }
            ?>
        </tr>
            <?php
        }
    }
    ?>
