<?php
require_once('activate_error.php');
require_once('../require_daos.php');


if (isset($_GET['action']) && $_GET['action'] == "upload_image") {
    $base = new base();
    $Activos = $DaoActivos->show($_GET['Id_activo']);

    if (count($_FILES) > 0) {
        $type = substr($_FILES["file"]["name"], strpos($_FILES["file"]["name"], ".") + 1);
        if (($type == "gif" || $type == "jpeg" || $type == "png" || $type == "jpg")) {

            if ($_FILES["file"]["error"] > 0) {

                echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
            } else {

                if (file_exists("../files/" . $Activos->getId_img() . ".jpg")) {
                    unlink("../files/" . $Activos->getId_img() . ".jpg");
                }

                $key = $base->generarKey();
                $Activos->setId_img($key);
                $DaoActivos->update($Activos);

                move_uploaded_file($_FILES["file"]["tmp_name"], "files/" . $key . ".jpg");
                //header("Location: contabilidad.php");
                //se usa cuando se suben archivos por carga de archivos normales
                // print_r($_FILES);
                $sourcefile = 'files/' . $key . '.jpg';
                $picsize = getimagesize($sourcefile);

                $source_x = $picsize[0] * 1;
                $source_y = $picsize[1] * 1;

                //modificar el tamaño de la imagen
                $result1 = resizeToFile($sourcefile, 300, 300 * $source_y / $source_x, $sourcefile, 80);

                echo $key;
            }
        } else {
            echo "Invalid file: " . $type;
        }
    } elseif (isset($_GET['action'])) {
        if (($_GET['TypeFile'] == "image/gif" || $_GET['TypeFile'] == "image/jpeg" || $_GET['TypeFile'] == "image/png")) {
            if ($_FILES["file"]["error"] > 0) {

                echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
            } else {

                if (file_exists("../files/" . $Activos->getId_img() . ".jpg")) {
                    unlink("../files/" . $Activos->getId_img() . ".jpg");
                }

                $key = $base->generarKey();
                $Activos->setId_img($key);
                $DaoActivos->update($Activos);


                if (isset($_GET['base64'])) {
                    // If the browser does not support sendAsBinary ()
                    $content = base64_decode(file_get_contents('php://input'));
                } else {
                    $content = file_get_contents('php://input');
                }
                file_put_contents('files/' . $key . '.jpg', $content);

                $sourcefile = 'files/' . $key . '.jpg';
                $picsize = getimagesize($sourcefile);

                $source_x = $picsize[0] * 1;
                $source_y = $picsize[1] * 1;

                //modificar el tamaño de la imagen
                $result1 = resizeToFile($sourcefile, 300, 300 * $source_y / $source_x, $sourcefile, 80);

                echo $key;
            }
        } else {

            echo "Invalid file: " . $_GET['TypeFile'];
        }
    }
}




if (isset($_POST['action']) && $_POST['action'] == "update_pag") {
    update_page($_POST['Id_activo']);
}

function update_page($Id_activo) {
    ?>
    <table id="tabla">
        <?php
        $DaoActivos = new DaoActivos();
        $DaoAulas = new DaoAulas();
        $DaoUsuarios= new DaoUsuarios();
        $DaoTiposActivo= new DaoTiposActivo();
        $_usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        if (isset($Id_activo) && $Id_activo > 0) {
            $activo = $DaoActivos->show($Id_activo);
            $Nombre = $activo->getNombre();
            $Id_img = $activo->getId_img();
            $Codigo = $activo->getCodigo();
            $Marca = $activo->getMarca();
            $Modelo = $activo->getModelo();
            $Num_serie = $activo->getNum_serie();
            $Id_aula = $activo->getId_aula();
            $Valor = $activo->getValor();
            $Fecha_adq = $activo->getFecha_adq();
            $Garantia_meses = $activo->getGarantia_meses();
            $Mantenimiento_frecuencia = $activo->getMantenimiento_frecuencia();
            $Tipo_act = $activo->getTipo_act();
            $Descripcion = $activo->getDescripcion();
            $Id_activo = $activo->getId();
        }
        ?>
        <tr>
            <td id="column_one">
                <div class="fondo">
                    <div id="box_top">
                        <div class="foto_alumno" 
                        <?php if (strlen($Id_img) > 0) { ?> 
                                 style="background-image:url(files/<?php echo $Id_img ?>.jpg) <?php } ?>">
                        </div>
                    </div>
                    <div class="seccion">
                        <h2>Datos del activo</h2>
                        <span class="linea"></span>
                        <ul class="form">
                            <?php
                            if ($Id_activo > 0) {
                                ?>
                                <li>C&oacute;digo<br><input type="text" value="<?php echo $Codigo ?>" id="codigo" readonly="readonly"/></li>
                                <?php
                            }
                            ?>
                            <li>Nombre<br><input type="text" value="<?php echo $Nombre ?>" id="nombre"/></li>
                            <li>Marca<br><input type="text" value="<?php echo $Marca ?>" id="marca"/></li>
                            <li>Modelo<br><input type="text" value="<?php echo $Modelo ?>" id="modelo"/></li>
                            <li>Num. Serie<br><input type="text" value="<?php echo $Num_serie ?>" id="serie"/></li>
                            <li>Ubicaci&oacute;n<br>
                                <select id="aula">
                                    <option value="0"></option>
                                    <?php
                                    foreach ($DaoAulas->showAll() as $aula) {
                                        ?>
                                        <option value="<?php echo $aula->getId(); ?>" <?php if ($aula->getId() == $Id_aula) { ?> selected="selected"<?php } ?>><?php echo $aula->getClave_aula(); ?></option>
                                        <?php
                                    }
                                    ?>

                                </select>
                            </li>
                            <li>Valor Factura<br><input type="text" value="<?php echo $Valor ?>" id="valor_factura"/></li>
                            <li>Fecha de adquisici&oacute;n<br><input type="date" value="<?php echo $Fecha_adq ?>" id="fecha_adquisicion"/></li>
                            <li>Meses de garantia<br><input type="text" id="meses_garantia" value="<?php echo $Garantia_meses ?>"/></li>
                            <li>Frecuencia de mantenimiento<br>
                                <select id="frecuencia">
                                    <option value="0"></option>
                                    <option value="1" <?php if ($Mantenimiento_frecuencia == 1) { ?> selected="selected"<?php } ?>>Semanal</option>
                                    <option value="2" <?php if ($Mantenimiento_frecuencia == 2) { ?> selected="selected"<?php } ?>>Quincenal</option>
                                    <option value="3" <?php if ($Mantenimiento_frecuencia == 3) { ?> selected="selected"<?php } ?>>Semestral</option>
                                    <option value="4" <?php if ($Mantenimiento_frecuencia == 4) { ?> selected="selected"<?php } ?>>Anual</option>
                                </select></li>
                            <li>Tipo<br>
                                <select id="tipo">
                                    <option value="0"></option>
                                    <?php
                                    foreach($DaoTiposActivo->getTipos() as $tipo){
                                        ?>
                                          <option value="<?php echo $tipo->getId()?>" <?php if ($Tipo_act == $tipo->getId()) { ?> selected="selected"<?php } ?>><?php echo $tipo->getNombre()?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </li>
                            <li>Descripci&oacute;n<br><textarea id="descripcion"><?php echo $Descripcion ?></textarea></li>
                        </ul>
                    </div>
                    <button id="button_ins" onclick="save_activo()">Guardar</button>
                </div>
            </td>
            <td id="column_two">
                <div id="box_menus">
                    <?php
                    require_once '../estandares/menu_derecho.php';
                    ?>
                    <ul>
                        <li><a href="activo.php" class="link">Nuevo</a></li>
                        <?php
                        if ($Id_activo > 0) {
                            ?>
                            <li><span onclick="mostrarFinder()">Añadir fotografia</span></li>
                            <li><a href="historial_prestamo_activo.php?id=<?php echo $Id_activo; ?>" target="_blank">Historial de prestamos</a></li>
                            <li><a href="historial_activo.php?id=<?php echo $Id_activo; ?>">Historial de mantenimiento</a></li>
                            <li><a href="prestamo_activos.php?id=<?php echo $Id_activo; ?>">Prestamo de activos</a></li>
                            <?php
                        }
                        ?>
                    </ul>
                    <input type="file" id="files" name="files" multiple="">
                </div>
            </td>
        </tr>
    </table>
    <input type="hidden" id="Id_activo" value="<?php echo $Id_activo ?>"/>
    <?php
}

if (isset($_POST['action']) && $_POST['action'] == "save_activo") {

    $DaoUsuarios = new DaoUsuarios();
    $DaoActivos = new DaoActivos();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $resp = array();
    if ($_POST['Id_activo'] > 0) {
        $Activos = $DaoActivos->show($_POST['Id_activo']);
        $Activos->setNombre($_POST['nombre']);
        $Activos->setModelo($_POST['modelo']);
        $Activos->setDescripcion($_POST['descripcion']);
        $Activos->setValor($_POST['valor_factura']);
        $Activos->setTipo_act($_POST['tipo']);
        $Activos->setFecha_adq($_POST['fecha_adquisicion']);
        $Activos->setGarantia_meses($_POST['meses_garantia']);
        $Activos->setMantenimiento_frecuencia($_POST['frecuencia']);
        $Activos->setId_aula($_POST['Id_aula']);
        $Activos->setMarca($_POST['marca']);
        $Activos->setNum_serie($_POST['serie']);
        $DaoActivos->update($Activos);

        $TextoHistorial = "Actualizó los datos para el activo " . $_POST['nombre'];
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Activos");

        $id_activo = $_POST['Id_activo'];
        $resp['tipo'] = "update";
        $resp['id'] = $id_activo;
    } else {

        $Activos = new Activos();
        $Activos->setNombre($_POST['nombre']);
        $Activos->setModelo($_POST['modelo']);
        $Activos->setDescripcion($_POST['descripcion']);
        $Activos->setValor($_POST['valor_factura']);
        $Activos->setTipo_act($_POST['tipo']);
        $Activos->setFecha_adq($_POST['fecha_adquisicion']);
        $Activos->setGarantia_meses($_POST['meses_garantia']);
        $Activos->setMantenimiento_frecuencia($_POST['frecuencia']);
        $Activos->setId_plantel($usu->getId_plantel());
        $Activos->setId_aula($_POST['Id_aula']);
        $Activos->setMarca($_POST['marca']);
        $Activos->setNum_serie($_POST['serie']);
        $Activos->setDisponible(1);
        $Activos->setCodigo($DaoActivos->generar_codigo_activo($_POST['tipo']));
        $id_activo = $DaoActivos->add($Activos);

        $TextoHistorial = "Añadio el nuevo activo " . $_POST['nombre'];
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Activos");

        $resp['tipo'] = "new";
        $resp['id'] = $id_activo;
    }
    echo json_encode($resp);
}


