<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="filtro"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query="";
    if($_POST['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_POST['Id_ofe'];
    }
    if($_POST['Id_esp']>0){
        $query=$query." AND Id_esp=".$_POST['Id_esp'];
    }
    if($_POST['Id_ori']>0){
        $query=$query." AND Id_ori=".$_POST['Id_ori'];
    }
    if($_POST['Opcion_pago']>0){
        $query=$query." AND Opcion_pago=".$_POST['Opcion_pago'];
    }
    $count=1;
    $query="SELECT * FROM inscripciones_ulm 
        JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
        WHERE tipo=1  
              AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()." 
              AND Activo_oferta=1 
              AND Baja_ofe IS NULL 
              AND FechaCapturaEgreso IS NULL
              AND IdCicloEgreso IS NULL
              AND IdUsuEgreso IS NULL ".$query." 
        ORDER BY Id_ins DESC";
    $consulta=$base->advanced_query($query);
    $row_consulta = $consulta->fetch_assoc();
    $totalRows_consulta= $consulta->num_rows;
     if($totalRows_consulta>0){
       do{
           $nombre_ori="";
           $oferta = $DaoOfertas->show($row_consulta['Id_ofe']);
           $esp = $DaoEspecialidades->show($row_consulta['Id_esp']);
           if ($row_consulta['Id_ori'] > 0) {
              $ori = $DaoOrientaciones->show($row_consulta['Id_ori']);
              $nombre_ori = $ori->getNombre();
            }
            $opcion="Plan por materias"; 
            if($row_consulta['Opcion_pago']==2){
              $opcion="Plan completo";  
            }
            //Buscar documentos faltantes
            $Faltantes=$DaoAlumnos->getArchivosFaltantes($row_consulta['Id_ofe_alum']);
            if($Faltantes>0){
              ?>
                     <tr id_alum="<?php echo $row_consulta['Id_ins'];?>">
                       <td><?php echo $count;?></td>
                       <td><?php echo $row_consulta['Matricula'] ?></td>
                       <td style="width: 115px;"><?php echo $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins'] ?></td>
                       <td><?php echo $oferta->getNombre_oferta() ?></td>
                       <td><?php echo $esp->getNombre_esp(); ?></td>
                       <td><?php echo $nombre_ori; ?></td>
                       <td><?php echo $opcion; ?></td>
                       <td style="text-align: center;"><?php echo $Faltantes;?></td>
                       <td style="text-align: center;"><button onclick="mostrar_documentos(<?php echo $row_consulta['Id_ofe_alum'];?>)">Mostrar documentos</button></td>
                     </tr>
               <?php
                  $count++;
                }
               }while( $row_consulta = $consulta->fetch_assoc());
          }
}



if($_POST['action'] == "mostrar_documentos"){
  ?>
    <div id="box_emergente" class="documentos_faltantes">
        <table class="table">
            <thead>
                <tr>
                    <td colspan="6" style="font-size: 11px;text-align: center;"><b>Documentos</b></td>
                </tr>
                <tr>
                  <td>#</td>
                  <td>Documento</td>
                  <td style="text-align: center;">Original</td>
                  <td style="text-align: center;">Copia</td>
                  <td style="text-align: center;">Fecha de entrega</td>
                  <td>Recibido</td>
          </thead>
          <?php
                $count=1;
                $DaoArchivosAlumno= new DaoArchivosAlumno();
                $DaoUsuarios= new DaoUsuarios();
                 foreach($DaoArchivosAlumno->getArchivosAlumnoByOfertaAlumno($_POST['Id_ofe_alum']) as $k2=>$v2){
                        $nombre_usu="";
                        if($v2['Id_usu']!=null){
                           $usu=$DaoUsuarios->show($v2['Id_usu']);
                           $nombre_usu= $usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
                        }
                        ?>
                            <tr id-file="<?php echo $v2['Id_file']?>">
                                 <td><?php echo $count;?></td>
                                 <td><?php echo $v2['Nombre_file']?></td>
                                 <td style="text-align: center;"><span><?php if ($v2['Id_file_original'] > 0) { ?> si <?php } ?> </span></td>
                                 <td style="text-align: center;"><span><?php if ($v2['Id_file_copy']==1) { ?> si <?php } ?> </span></td>
                                 <td style="text-align: center;"><?php echo $v2['fecha_recibido']?></td>
                                 <td><?php echo $nombre_usu;?></td>
                            </tr>
                        <?php
                        $count++;
               }
            ?>
        </table>
        <p><button onclick="ocultar_error_layer()">Cerrar</button></p>
</div>
  <?php
}



if($_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'MATRICULA');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'OFERTA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'CARRERA');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'ORIENTACIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'OPCIÓN DE PAGO');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'DOC.FALTANTES');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','H') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query="";
    if($_GET['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_GET['Id_ofe'];
    }
    if($_GET['Id_esp']>0){
        $query=$query." AND Id_esp=".$_GET['Id_esp'];
    }
    if($_GET['Id_ori']>0){
        $query=$query." AND Id_ori=".$_GET['Id_ori'];
    }
    if($_GET['Opcion_pago']>0){
        $query=$query." AND Opcion_pago=".$_GET['Opcion_pago'];
    }
    $count=1;
    $query="SELECT * FROM inscripciones_ulm 
        JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
        WHERE tipo=1  
              AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()." 
              AND Activo_oferta=1 
              AND Baja_ofe IS NULL 
              AND FechaCapturaEgreso IS NULL
              AND IdCicloEgreso IS NULL
              AND IdUsuEgreso IS NULL ".$query." 
        ORDER BY Id_ins DESC";
    $consulta=$base->advanced_query($query);
    $row_consulta = $consulta->fetch_assoc();
    $totalRows_consulta= $consulta->num_rows;
     if($totalRows_consulta>0){
       do{
           $nombre_ori="";
           $oferta = $DaoOfertas->show($row_consulta['Id_ofe']);
           $esp = $DaoEspecialidades->show($row_consulta['Id_esp']);
           if ($row_consulta['Id_ori'] > 0) {
              $ori = $DaoOrientaciones->show($row_consulta['Id_ori']);
              $nombre_ori = $ori->getNombre();
            }
            $opcion="Plan por materias"; 
            if($row_consulta['Opcion_pago']==2){
              $opcion="Plan completo";  
            }
            //Buscar documentos faltantes
            $Faltantes=$DaoAlumnos->getArchivosFaltantes($row_consulta['Id_ofe_alum']);
            if($Faltantes>0){
                
                $count++;
                $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
                $objPHPExcel->getActiveSheet()->setCellValue("B$count", $row_consulta['Matricula']);
                $objPHPExcel->getActiveSheet()->setCellValue("C$count", $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins']);
                $objPHPExcel->getActiveSheet()->setCellValue("D$count", $oferta->getNombre_oferta());
                $objPHPExcel->getActiveSheet()->setCellValue("E$count", $esp->getNombre_esp());
                $objPHPExcel->getActiveSheet()->setCellValue("F$count", $nombre_ori);
                $objPHPExcel->getActiveSheet()->setCellValue("G$count", $opcion);
                $objPHPExcel->getActiveSheet()->setCellValue("H$count", $Faltantes);

            }
       }while( $row_consulta = $consulta->fetch_assoc());
    }


    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=DocumentosFaltantes-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}
