<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="filtro"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclos= new DaoCiclos();
    $DaoTurnos= new DaoTurnos();
    
    $ciclo=$DaoCiclos->getActual();

    $query="";

    if($_POST['Id_alumn']>0){
        $query=$query." AND Id_ins=".$_POST['Id_alumn'];
    }
    if($_POST['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_POST['Id_ofe'];
    }
    if($_POST['Id_esp']>0){
        $query=$query." AND Id_esp=".$_POST['Id_esp'];
    }
    if($_POST['Id_ori']>0){
        $query=$query." AND Id_ori=".$_POST['Id_ori'];
    }
    if($_POST['Opcion_pago']>0){
        $query=$query." AND Opcion_pago=".$_POST['Opcion_pago'];
    }
    
    if($_POST['Turno']>0){
        $query=$query." AND Turno=".$_POST['Turno'];
    }

    foreach ($DaoAlumnos->getAlumnos(null,$query,null,null) as $k=>$v){

       $nombre_ori="";
       $oferta = $DaoOfertas->show($v['Id_ofe']);
       $esp = $DaoEspecialidades->show($v['Id_esp']);
       if ($v['Id_ori'] > 0) {
          $ori = $DaoOrientaciones->show($v['Id_ori']);
          $nombre_ori = $ori->getNombre();
        }
        $opcion="Plan por materias"; 
        if($v['Opcion_pago']==2){
          $opcion="Plan completo";  
        }
    
        $Id_ciclo=$ciclo->getId();
        if($_POST['Id_ciclo']>0){
           $Id_ciclo=$_POST['Id_ciclo'];
        }
        $tur = $DaoTurnos->show($v['Turno']);
        $t=$tur->getNombre();

        $queryx = "SELECT materias_uml.Nombre,Materias_especialidades.NombreDiferente 
               FROM ciclos_alum_ulm 
        JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
        JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
        JOIN materias_uml ON Materias_especialidades.Id_mat=materias_uml.Id_mat
        WHERE Id_ciclo=".$Id_ciclo."  AND Id_ofe_alum=".$v['Id_ofe_alum']." AND Id_grupo IS NULL";
         foreach($base->advanced_query($queryx) as $k2=>$v2){
                 $NombreMat=$v2['Nombre'];
                 if(strlen($v2['NombreDiferente'])>0){
                    $NombreMat=$v2['NombreDiferente']; 
                 }
      ?>
              <tr id_alum="<?php echo $v['Id_ins'];?>">
                <td><?php echo $v['Matricula'] ?></td>
                <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                <td><?php echo $oferta->getNombre_oferta(); ?></td>
                <td><?php echo $esp->getNombre_esp(); ?></td>
                <td><?php echo $nombre_ori; ?></td>
                <td><?php echo $NombreMat; ?></td>
                <td><?php echo $t; ?></td>
                <td><?php echo $opcion; ?></td>
              </tr>
              <?php
        }
    }
                             
}



if($_GET['action']=="download_excel"){
    $base= new base();
    $DaoTurnos= new DaoTurnos();
    
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'MATRICULA');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'OFERTA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'CARRERA');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'ORIENTACIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'MATERIA');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'TURNO');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'OPCIÓN DE PAGO');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','I') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }

    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclos= new DaoCiclos();
    $ciclo=$DaoCiclos->getActual();

    $query="";

    if($_GET['Id_alumn']>0){
        $query=$query." AND Id_ins=".$_GET['Id_alumn'];
    }
    if($_GET['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_GET['Id_ofe'];
    }
    if($_GET['Id_esp']>0){
        $query=$query." AND Id_esp=".$_GET['Id_esp'];
    }
    if($_GET['Id_ori']>0){
        $query=$query." AND Id_ori=".$_GET['Id_ori'];
    }
    if($_GET['Opcion_pago']>0){
        $query=$query." AND Opcion_pago=".$_GET['Opcion_pago'];
    }
    if($_GET['Turno']>0){
        $query=$query." AND Turno=".$_GET['Turno'];
    }
    $count=1;
    foreach ($DaoAlumnos->getAlumnos(null,$query,null,null) as $k=>$v){

       $nombre_ori="";
       $oferta = $DaoOfertas->show($v['Id_ofe']);
       $esp = $DaoEspecialidades->show($v['Id_esp']);
       if ($v['Id_ori'] > 0) {
          $ori = $DaoOrientaciones->show($v['Id_ori']);
          $nombre_ori = $ori->getNombre();
        }
        $opcion="Plan por materias"; 
        if($v['Opcion_pago']==2){
          $opcion="Plan completo";  
        }
    
        $Id_ciclo=$ciclo->getId();
        if($_GET['Id_ciclo']>0){
           $Id_ciclo=$_GET['Id_ciclo'];
        }

        $tur = $DaoTurnos->show($v['Turno']);
        $t=$tur->getNombre();
        
        $queryx = "SELECT materias_uml.Nombre,Materias_especialidades.NombreDiferente 
               FROM ciclos_alum_ulm 
        JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
        JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
        JOIN materias_uml ON Materias_especialidades.Id_mat=materias_uml.Id_mat
        WHERE Id_ciclo=".$Id_ciclo."  AND Id_ofe_alum=".$v['Id_ofe_alum']." AND Id_grupo IS NULL";
         foreach($base->advanced_query($queryx) as $k2=>$v2){
                 $NombreMat=$v2['Nombre'];
                 if(strlen($v2['NombreDiferente'])>0){
                    $NombreMat=$v2['NombreDiferente']; 
                 }
                 
                $count++;
                $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
                $objPHPExcel->getActiveSheet()->setCellValue("B$count", $v['Matricula']);
                $objPHPExcel->getActiveSheet()->setCellValue("C$count", $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins']);
                $objPHPExcel->getActiveSheet()->setCellValue("D$count", $oferta->getNombre_oferta());
                $objPHPExcel->getActiveSheet()->setCellValue("E$count", $esp->getNombre_esp());
                $objPHPExcel->getActiveSheet()->setCellValue("F$count", $nombre_ori);
                $objPHPExcel->getActiveSheet()->setCellValue("G$count", $NombreMat);
                $objPHPExcel->getActiveSheet()->setCellValue("H$count", $t);
                $objPHPExcel->getActiveSheet()->setCellValue("I$count", $opcion);
        }
    }
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=Sin-grupo-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}

