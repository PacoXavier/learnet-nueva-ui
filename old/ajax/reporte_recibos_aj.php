<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if(isset($_POST['action']) && $_POST['action']=="filtro"){
    $DaoAulas= new DaoAulas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclos= new DaoCiclos();
    $DaoPagos= new DaoPagos();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoEventos= new DaoEventos();
    $DaoUsuarios= new DaoUsuarios();
    $base= new base();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $query="";
    if(isset($_POST['Folio']) && $_POST['Folio']>0){
        $query=$query." AND Pagos_pagos.Folio=".$_POST['Folio'];
    }
    if(isset($_POST['Id_alum']) && $_POST['Id_alum']>0){
        $query=$query." AND IdRel=".$_POST['Id_alum']." AND TipoRel='alum'";
    }
    
    if(isset($_POST['Concepto']) && strlen($_POST['Concepto'])>0){
        $query=$query." AND ConceptoPago='".$_POST['Concepto']."'";
    }
    
    if(isset($_POST['Fecha_ini']) && strlen($_POST['Fecha_ini'])>0 && isset($_POST['Fecha_fin']) && $_POST['Fecha_fin']==null){
        $query=$query." AND Fecha_pp>='".$_POST['Fecha_ini']."'";
    }elseif(strlen($_POST['Fecha_fin'])>0 && $_POST['Fecha_ini']==null){
        $query=$query." AND Fecha_pp<='".$_POST['Fecha_fin']."'";
    }elseif(strlen($_POST['Fecha_ini'])>0 && strlen($_POST['Fecha_fin'])>0){
        $query=$query." AND (Fecha_pp>='".$_POST['Fecha_ini']."' AND Fecha_pp<='".$_POST['Fecha_fin']."')";
    }
    
    if(isset($_POST['Id_ofe']) &&  $_POST['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_POST['Id_ofe'];
    }
    if(isset($_POST['Id_esp']) &&  $_POST['Id_esp']>0){
        $query=$query." AND Id_esp=".$_POST['Id_esp'];
    }
    if(isset($_POST['Id_ori']) &&  $_POST['Id_ori']>0){
        $query=$query." AND Id_ori=".$_POST['Id_ori'];
    }
    
    if(isset($_POST['activo']) &&  $_POST['activo']==1 && isset($_POST['cancelado']) &&  $_POST['cancelado']==1 ){
        $query=$query." AND (Activo=1 || Activo=0)";
    }else{
         if(isset($_POST['activo']) && $_POST['activo']==1){
                $query=$query." AND Activo=1";
         }elseif(isset($_POST['cancelado']) && $_POST['cancelado']==1){
                $query=$query." AND Activo=0";
         }else{
              $query=$query." AND (Activo=1 || Activo=0)"; 
         }      
    }
   

    $query="
        SELECT Pagos_pagos.* FROM Pagos_pagos
        JOIN ofertas_alumno ON Pagos_pagos.Id_ofe_alum=ofertas_alumno.Id_ofe_alum
        WHERE Id_plantel=".$usu->Id_plantel." 
            
            ".$query."
            ORDER BY Folio DESC";
    $count=1;
    $total=0;
    foreach($base->advanced_query($query) as $k=>$v){
        $pago=$DaoPagos->show($v['Id_pp']);
        if($pago->getId_evento()>0){
            $resp=$DaoEventos->show($pago->getId_evento());
            $aula=$DaoAulas->show($resp->getId_salon());
            $Nombre=$v['NombrePaga'];
            $Concepto="Renta de aula ".$aula->getClave_aula();
        }else{
            $Concepto=$pago->getConcepto();
            if($pago->getTipoRel()=="alum"){
              $alum=$DaoAlumnos->show($pago->getIdRel());
              $Nombre=$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();   
            }elseif($pago->getTipoRel()=="docen"){
              $docen=$DaoDocentes->show($pago->getIdRel());
              $Nombre=$docen->getNombre_docen()." ".$docen->getApellidoP_docen()." ".$docen->getApellidoM_docen();     
            }elseif($pago->getTipoRel()=="usu"){
              $u=$DaoUsuarios->show($pago->getIdRel());
              $Nombre=$u->getNombre_usu()." ".$u->getApellidoP_usu()." ".$u->getApellidoM_usu();    
            }
        }
        $NombreUsu="";
        if($v['Id_usu']>0){
            $respUsu=$DaoUsuarios->show($v['Id_usu']);
            $NombreUsu=$respUsu->getNombre_usu()." ".$respUsu->getApellidoP_usu()." ".$respUsu->getApellidoM_usu();
        }
        $classPrimer="";
        $monto=$v['Monto_pp'];
        if($v['Monto_pp']<=0 || $v['Activo']==0){
                $classPrimer='class="pink"'; 
                $monto=0;
        }
        
        $ciclo=$DaoCiclos->getCicloPorFecha($pago->getFecha_pp());
        ?>
       <tr <?php echo $classPrimer;?>>
            <td><?php echo $count?> </td>
            <td><?php echo $pago->getFolio()?> </td>
            <td><?php echo $ciclo->getClave()?></td>
            <td><?php echo $pago->getFecha_pp()?></td>
            <td><?php echo $Nombre;?></td>
            <td><?php echo $Concepto;?></td>
            <td>$<?php echo number_format($monto,2)?></td>
            <td><?php echo $NombreUsu;?></td>
        </tr>
        <?php
        $count++;    
        $total+=$monto;
    }
    ?>
    <tr>
       <td colspan="6"></td>
       <td style="color:red"><b>Total</b></td>
       <td style="color:red"><b>$<?php echo number_format($total,2)?> </b></td>
    </tr>
    <?php
}



if(isset($_GET['action']) && ['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'FOLIO');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'CICLO');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'FECHA DE PAGO');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'CONCEPTO');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'MONTO');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'USUARIO');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','H') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    
    $DaoAulas= new DaoAulas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclos= new DaoCiclos();
    $DaoPagos= new DaoPagos();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoEventos= new DaoEventos();
    $DaoUsuarios= new DaoUsuarios();
    
    $ciclo=$DaoCiclos->getActual();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $query="";
    if($_GET['Folio']>0){
        $query="Pagos_pagos.Folio".$_GET['Folio'];
    }
    
    if($_GET['Id_alum']>0){
        $query=$query." AND IdRel=".$_GET['Id_alum']." AND TipoRel='alum'";
    }
    
    if(strlen($_GET['Concepto'])>0){
        $query=$query." AND ConceptoPago='".$_GET['Concepto']."'";
    }

    if(strlen($_GET['Fecha_ini'])>0 && $_GET['Fecha_fin']==null){
        $query=$query." AND Fecha_pp>='".$_GET['Fecha_ini']."'";
    }elseif(strlen($_GET['Fecha_fin'])>0 && $_GET['Fecha_ini']==null){
        $query=$query." AND Fecha_pp<='".$_GET['Fecha_fin']."'";
    }elseif(strlen($_GET['Fecha_ini'])>0 && strlen($_GET['Fecha_fin'])>0){
        $query=$query." AND (Fecha_pp>='".$_GET['Fecha_ini']."' AND Fecha_pp<='".$_GET['Fecha_fin']."')";
    }
    
    if($_GET['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_GET['Id_ofe'];
    }
    if($_GET['Id_esp']>0){
        $query=$query." AND Id_esp=".$_GET['Id_esp'];
    }
    if($_GET['Id_ori']>0){
        $query=$query." AND Id_ori=".$_GET['Id_ori'];
    }
    
   if(isset($_GET['activo']) &&  $_GET['activo']==1 && isset($_GET['cancelado']) &&  $_GET['cancelado']==1 ){
        $query=$query." AND (Activo=1 || Activo=0)";
    }else{
         if(isset($_GET['activo']) && $_GET['activo']==1){
                $query=$query." AND Activo=1";
         }elseif(isset($_GET['cancelado']) && $_GET['cancelado']==1){
                $query=$query." AND Activo=0";
         }else{
              $query=$query." AND (Activo=1 || Activo=0)"; 
         }      
    }
         
    $query="
        SELECT Pagos_pagos.* FROM Pagos_pagos
        JOIN ofertas_alumno ON Pagos_pagos.Id_ofe_alum=ofertas_alumno.Id_ofe_alum
        WHERE Pagos_pagos.Id_plantel=".$usu->Id_plantel." 
            ".$query."
            ORDER BY Fecha_pp DESC";

    $count=1;
    foreach($base->advanced_query($query) as $k=>$v){
        $pago=$DaoPagos->show($v['Id_pp']);
        if($pago->getId_evento()>0){
            $resp=$DaoEventos->show($pago->getId_evento());
            $aula=$DaoAulas->show($resp->getId_salon());
            $Nombre=$v['NombrePaga'];
            $Concepto="Renta de aula ".$aula->getClave_aula();
        }else{
            $Concepto=$pago->getConcepto();
            if($pago->getTipoRel()=="alum"){
              $alum=$DaoAlumnos->show($pago->getIdRel());
              $Nombre=$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();   
            }elseif($pago->getTipoRel()=="docen"){
              $docen=$DaoDocentes->show($pago->getIdRel());
              $Nombre=$docen->getNombre_docen()." ".$docen->getApellidoP_docen()." ".$docen->getApellidoM_docen();     
            }elseif($pago->getTipoRel()=="usu"){
              $u=$DaoUsuarios->show($pago->getIdRel());
              $Nombre=$u->getNombre_usu()." ".$u->getApellidoP_usu()." ".$u->getApellidoM_usu();    
            }
        }
        $NombreUsu="";
        if($v['Id_usu']>0){
            $respUsu=$DaoUsuarios->show($v['Id_usu']);
            $NombreUsu=$respUsu->getNombre_usu()." ".$respUsu->getApellidoP_usu()." ".$respUsu->getApellidoM_usu();
        }
        $classPrimer="";
        $monto=$v['Monto_pp'];
        if($v['Monto_pp']<=0 || $v['Activo']==0){
                $classPrimer='class="pink"'; 
                $monto=0;
        }
        
        $ciclo=$DaoCiclos->getCicloPorFecha($pago->getFecha_pp());
        $count++;
        $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
        $objPHPExcel->getActiveSheet()->setCellValue("B$count", $v['Id_pp']);
        $objPHPExcel->getActiveSheet()->setCellValue("C$count", $ciclo->getClave());
        $objPHPExcel->getActiveSheet()->setCellValue("D$count", $v['Fecha_pp']);
        $objPHPExcel->getActiveSheet()->setCellValue("E$count", $Nombre);
        $objPHPExcel->getActiveSheet()->setCellValue("F$count", $Concepto);
        $objPHPExcel->getActiveSheet()->setCellValue("G$count", "$".number_format($monto,2));
        $objPHPExcel->getActiveSheet()->setCellValue("H$count", $NombreUsu);  
    }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=Recibos-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}




