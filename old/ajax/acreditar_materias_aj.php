<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if ($_POST['action'] == "save_acreditacion") {
    $DaoUsuarios = new DaoUsuarios();
    $DaoAlumnos = new DaoAlumnos();
    $DaoMateriasAcreditadas = new DaoMateriasAcreditadas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoMaterias = new DaoMaterias();
    $DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoOfertas= new DaoOfertas();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $base = new base();

    $query = "SELECT * FROM Materias_acreditadas WHERE Id_ofe_alum=" . $_POST['Id_oferta_alumno'] . " AND Id_mat_esp=" . $_POST['Id_mat_esp'];
    $consulta = $base->advanced_query($query);
    $row_consulta = $consulta->fetch_assoc();
    $totalRows_consulta = $consulta->num_rows;
    if ($totalRows_consulta == 0) {

        $MateriasAcreditadas = new MateriasAcreditadas();
        $MateriasAcreditadas->setId_ciclo($_POST['Id_ciclo']);
        $MateriasAcreditadas->setFecha_ac(date('Y-m-d'));
        $MateriasAcreditadas->setId_mat_esp($_POST['Id_mat_esp']);
        $MateriasAcreditadas->setId_ofe_alum($_POST['Id_oferta_alumno']);
        $MateriasAcreditadas->setId_usu($_COOKIE['admin/Id_usu']);
        $id_mat_ac = $DaoMateriasAcreditadas->add($MateriasAcreditadas);
        
        $ofertaAlumno=$DaoOfertasAlumno->show($_POST['Id_oferta_alumno']);
        $oferta=$DaoOfertas->show($ofertaAlumno->getId_ofe());
        
        $mat_esp = $DaoMateriasEspecialidad->show($_POST['Id_mat_esp']);
        $mat = $DaoMaterias->show($mat_esp->getId_mat());

        $PagosCiclo = new PagosCiclo();
        $PagosCiclo->setConcepto("Mat. Acreditada (" . $mat->getNombre() . ")");
        $PagosCiclo->setFecha_pago(date('Y-m-d'));
        $PagosCiclo->setMensualidad($oferta->getMontoCargoMateriaAcreditada());
        $PagosCiclo->setId_ciclo_alum($_POST['Id_ciclo_alum']);
        $PagosCiclo->setId_alum($_POST['Id_alum']);
        $PagosCiclo->setIdRel($_POST['alum']);
        $PagosCiclo->setTipoRel('alum');
        $PagosCiclo->setTipo_pago("pago");
        $id_pago = $DaoPagosCiclo->add($PagosCiclo);

        $MateriaAcreditada = $DaoMateriasAcreditadas->show($id_mat_ac);
        $MateriaAcreditada->setId_pago($id_pago);
        $DaoMateriasAcreditadas->update($MateriaAcreditada);

        $alum = $DaoAlumnos->show($_POST['Id_alum']);

        //Historial
        $TextoHistorial = "Añade materia acreditada " . $mat->getNombre() . " para el alumno " . $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM();
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Acreditar materias");

        update_list_materias($_POST['Id_oferta_alumno']);
    }
}





if ($_POST['action'] == "delete_mat_ac") {
    $DaoUsuarios = new DaoUsuarios();
    $DaoAlumnos = new DaoAlumnos();
    $DaoMateriasAcreditadas = new DaoMateriasAcreditadas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoMaterias = new DaoMaterias();
    $DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $base = new base();

    $mat_ac = $DaoMateriasAcreditadas->show($_POST['Id_mat_ac']);
    $mat_esp = $DaoMateriasEspecialidad->show($mat_ac->getId_mat_esp());
    $mat = $DaoMaterias->show($mat_esp->getId_mat());
    $ofeAlum = $DaoOfertasAlumno->show($mat_ac->getId_ofe_alum());
    $alum = $DaoAlumnos->show($ofeAlum->getId_alum());

    //Historial
    $TextoHistorial = "Elimina materia acreditada " . $mat->getNombre() . " para el alumno " . $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Acreditar materias");

    $DaoPagosCiclo->delete($mat_ac->getId_pago());
    $DaoMateriasAcreditadas->delete($_POST['Id_mat_ac']);
    update_list_materias($_POST['Id_oferta_alumno']);
}


function update_list_materias($Id_oferta_alumno) {
    $DaoUsuarios = new DaoUsuarios();
    $DaoAlumnos = new DaoAlumnos();
    $DaoCiclos = new DaoCiclos();
    $DaoOfertas = new DaoOfertas();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoMateriasAcreditadas = new DaoMateriasAcreditadas();
    $DaoMaterias = new DaoMaterias();
    $DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
    $DaoGrados = new DaoGrados();
    $DaoOrientaciones = new DaoOrientaciones();

    $ofe_alum = $DaoOfertasAlumno->show($Id_oferta_alumno);
    $alum = $DaoAlumnos->show($ofe_alum->getId_alum());
    $oferta = $DaoOfertas->show($ofe_alum->getId_ofe());
    $esp = $DaoEspecialidades->show($ofe_alum->getId_esp());
    $nombre_ori = "";
    $primer_ciclo = $DaoOfertasAlumno->getFirstCicloOferta($Id_oferta_alumno);
    $ultimo_ciclo = $DaoOfertasAlumno->getLastCicloOferta($Id_oferta_alumno);

    if ($ofe_alum->getId_ori() > 0) {
        $ori = $DaoOrientaciones->show($ofe_alum->getId_ori());
        $nombre_ori = $ori->getNombre();
    }

    $Opcion_pago = "";
    if ($ofe_alum->getOpcionPago() == 1) {
        $Opcion_pago = "Plan por materias";
    } else {
        $Opcion_pago = "Plan completo";
    }
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1>Acreditar materias</h1>
                </div>
                <?php
                if ($Id_oferta_alumno > 0) {
                    ?>
                    <div class="ciclo_alumno">
                        <table class="table">
                            <thead>
                                <tr><td colspan="8" style="text-align: center;font-size: 11px">Informaci&oacute;n del alumno</td></tr>
                                <tr>
                                    <td>Matricula</td>
                                    <td class="normal"><?php echo $alum->getMatricula() ?></td>
                                    <td>Nombre</td>
                                    <td colspan="3" class="normal"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
                                    <td>Ciclo actual</td>
                                    <td class="normal"><?php echo $ultimo_ciclo['Clave']; ?></td>
                                </tr>
                                <tr>
                                    <td>Carrera</td>
                                    <td colspan="3" class="normal"><?php echo $esp->getNombre_esp(); ?></td>
                                    <td>Orientaci&oacute;n</td>
                                    <td class="normal"><?php echo $nombre_ori; ?></td>
                                    <td>Nivel</td>
                                    <td colspan="3" class="normal"><?php echo $oferta->getNombre_oferta(); ?></td>
                                </tr>
                                <tr>
                                    <td>Clave</td>
                                    <td style="width:300px;" colspan="2">Materia</td>
                                    <td>Tipo</td>
                                    <td>Nivel</td>
                                    <td>Creditos</td>
                                    <td>Usuario</td>
                                    <td>Acciones</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($DaoMateriasAcreditadas->getMateriasAcreditadasByOfeAlumno($Id_oferta_alumno) as $k => $v) {
                                    $ciclo = $DaoCiclos->show($v->getId_ciclo());
                                    $mat_esp = $DaoMateriasEspecialidad->show($v->getId_mat_esp());
                                    $mat = $DaoMaterias->show($mat_esp->getId_mat());
                                    $grado = $DaoGrados->show($mat_esp->getGrado_mat());
                                    $usu_acre = $DaoUsuarios->show($v->getId_usu());
                                    ?>
                                    <tr>
                                        <td><?php echo $mat->getClave_mat() ?></td>
                                        <td colspan="2"><?php echo $mat->getNombre() ?></td>
                                        <td>Acreditada</td>
                                        <td><?php echo $grado->getGrado() ?></td>
                                        <td><?php echo $mat->getCreditos() ?></td>
                                        <td><?php echo $usu_acre->getNombre_usu() . " " . $usu_acre->getApellidoP_usu() . " " . $usu_acre->getApellidoM_usu() ?></td>
                                        <td>
                                            <button onclick="delete_mat_ac(<?php echo $v->getId() ?>)">Eliminar</button>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once '../estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><span onclick="mostrar_box_materias()">Acreditar materia</span></li>
                </ul>
            </div>
        </td>
    </tr>
    <?php
}



if ($_POST['action'] == "mostrar_box_materias") {
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoGrados= new DaoGrados();
    $DaoCiclos= new DaoCiclos();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    
    $esp = $DaoEspecialidades->show($_POST['Id_esp']);
    ?>
    <div id="box_emergente" class="materias">
        <h1>Acreditar Materia</h1>
        <p>Ciclo:<br>
            <select id="ciclo">
                <option value="0"></option>
                <?php
                foreach ($DaoCiclosAlumno->getCiclosOferta($_POST['Id_oferta_alumno']) as $cicloAlumno) {
                         $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());
                    ?>
                <option value="<?php echo $cicloAlumno->getId() ?>" id-ciclo="<?php echo $cicloAlumno->getId_ciclo() ?>"><?php echo $ciclo->getClave() ?></option>
                    <?php
                }
                ?>
            </select></p>
        <p>Grado:<br>
            <select id="grado" onchange="update_materias_grado()">
                <option value="0"></option>
                <?php
                foreach ($DaoGrados->getGradosEspecialidad($esp->getId()) as $grado) {
                    ?>
                    <option value="<?php echo $grado->getId() ?>"><?php echo $grado->getGrado() ?></option>
                    <?php
                }
                ?>
            </select>
        </p>
        <p>Materia:<br>
            <select id="materia"></select>
        </p>
        <div id="box_orientaciones"></div>
        <p><button onclick="save_acreditacion()">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
    </div>
    <?php
}


if ($_POST['action'] == "update_materias_grado") {
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    ?>
    <option value="0"></option>
    <?php
    foreach ($DaoMateriasEspecialidad->getMateriasGrado($_POST['Id_grado_ofe']) as $matEsp) {
        $mat = $DaoMaterias->show($matEsp->getId_mat());
        ?>
    <option value="<?php echo $matEsp->getId(); ?>"><?php echo $mat->getClave_mat() . " - " . $mat->getNombre() ?></option>
        <?php
    }
    ?>
    <?php
}
