<?php
require_once('activate_error.php');
require_once('../clases/DaoUsuarios.php');
require_once('../clases/DaoCiclos.php');
require_once('../clases/DaoMaterias.php');
require_once('../clases/DaoOfertas.php');
require_once('../clases/DaoOrientaciones.php');
require_once('../clases/modelos/Orientaciones.php');
require_once('../clases/DaoEspecialidades.php');
require_once('../clases/DaoMateriasEspecialidad.php');
require_once('../clases/DaoGrados.php');
require_once('../clases/DaoPrerrequisitos.php');
require_once('../clases/DaoEvaluaciones.php');

require_once('../clases/modelos/Ofertas.php');
require_once('../clases/modelos/base.php');
require_once('../clases/modelos/Especialidades.php');
require_once('../clases/modelos/Materias.php');
require_once('../clases/modelos/MateriasEspecialidad.php');
require_once('../clases/modelos/Evaluaciones.php');
require_once('../clases/modelos/Prerrequisitos.php');

if($_POST['action']=="save_materia"){
  $DaoUsuarios= new DaoUsuarios();   
  $DaoMaterias= new DaoMaterias();
  $resp=array();
  if(isset($_POST['Id_mat']) && $_POST['Id_mat']>0){
        $mat=$DaoMaterias->show($_POST['Id_mat']);
        if($mat->getNombre()!=$_POST['Nombre']){
          $TextoHistorial = "Actualizo el nombre de la materia ".$mat->getNombre()." a " . $_POST['Nombre'];  
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Materias");
        }
        if($mat->getClave_mat()!=$_POST['Clave_mat']){
          $TextoHistorial = "Actualizo la clave de la materia ".$mat->getClave_mat()." a " . $_POST['Clave_mat'];  
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Materias");
        }
        if($mat->getPrecio()!=$_POST['Precio']){
          $TextoHistorial = "Actualizo el precio de la materia ".$mat->getPrecio()." a " . $_POST['Precio'];  
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Materias");
        }
        if($mat->getCreditos()!=$_POST['Creditos']){
          $TextoHistorial = "Actualizo los créditos de la materia ".$mat->getCreditos()." a " . $_POST['Creditos']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Materias");
        }
        if($mat->getPromedio_min()!=$_POST['Promedio_min']){
          $TextoHistorial = "Actualizo el promedio minimo de la materia ".$mat->getNombre()." a " . $_POST['Promedio_min']; 
          $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Materias");
        }

        $mat->setNombre($_POST['Nombre']);
        $mat->setClave_mat($_POST['Clave_mat']);
        $mat->setPrecio($_POST['Precio']);
        $mat->setCreditos($_POST['Creditos']);
        $mat->setTipo_aula($_POST['Tipo_aula']);
        $mat->setDias_porSemana($_POST['Dias_porSemana']);
        $mat->setHoras_independientes($_POST['Horas_independientes']);
        $mat->setHoras_porSemana($_POST['Horas_porSemana']);
        $mat->setHoras_conDocente($_POST['Horas_conDocente']);
        $mat->setPromedio_min($_POST['Promedio_min']);
        $mat->setDuracion_examen($_POST['Duracion_examen']);
        $mat->setMax_alumExamen($_POST['Max_alumExamen']);
        $DaoMaterias->update($mat);

        $id_mat=$_POST['Id_mat'];

        $resp['tipo']="update";
        $resp['id']=$id_mat;
  }else{
        $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        
        $Materias= new Materias();
        $Materias->setNombre($_POST['Nombre']);
        $Materias->setClave_mat($_POST['Clave_mat']);
        $Materias->setPrecio($_POST['Precio']);
        $Materias->setCreditos($_POST['Creditos']);
        $Materias->setTipo_aula($_POST['Tipo_aula']);
        $Materias->setDias_porSemana($_POST['Dias_porSemana']);
        $Materias->setHoras_independientes($_POST['Horas_independientes']);
        $Materias->setHoras_porSemana($_POST['Horas_porSemana']);
        $Materias->setHoras_conDocente($_POST['Horas_conDocente']);
        $Materias->setPromedio_min($_POST['Promedio_min']);
        $Materias->setDuracion_examen($_POST['Duracion_examen']);
        $Materias->setMax_alumExamen($_POST['Max_alumExamen']);
        $Materias->setId_plantel($usu->getId_plantel());
        $Materias->setActivo_mat(1);
        $id_mat=$DaoMaterias->add($Materias);

        $TextoHistorial = "Añadio la nueva materia ".$_POST['Nombre']; 
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Materias");
        $resp['tipo']="new";
        $resp['id']=$id_mat;
  }

  echo json_encode($resp);
}

if($_POST['action']=="update_pag"){
   update_page($_POST['Id_mat'],$_POST['Id_esp']);
}


if($_POST['action']=="delete_ofe_compartida"){
    $DaoUsuarios= new DaoUsuarios();  
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoMaterias= new DaoMaterias();
    $DaoEspecialidades= new DaoEspecialidades();
    
    $mat_esp= $DaoMateriasEspecialidad->show($_POST['Id_mat_esp']);
    $mat=$DaoMaterias->show($mat_esp->getId_mat());
    $esp=$DaoEspecialidades->show($mat_esp->getId_esp());

    $TextoHistorial = "Elimino la  materia ".$mat->getNombre()." para la especialidad ".$esp->getNombre_esp(); 
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Materias");
    $DaoMateriasEspecialidad->delete($_POST['Id_mat_esp']);
    update_page($_POST['Id_mat'],$_POST['Id_esp']);
}


if($_POST['action']=="add_materia"){
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoMaterias= new DaoMaterias();
    
    $mat_esp= $DaoMateriasEspecialidad->show($_POST['Id_mat_esp']);
    $mat=$DaoMaterias->show($mat_esp->getId_mat());
  ?>
  <tr id_mat_esp="<?php echo $_POST['Id_mat_esp']?>"> 
      <td style="width:200px;"><?php echo $mat->getNombre()?></td>
      <td><?php echo $mat->getClave_mat()?></td>
      <td><button onclick="delete_mat_add(this)">Eliminar</button></td>
  </tr>
  <?php
}


function update_page($id_mat,$id_esp){
$DaoMaterias = new DaoMaterias();
$DaoOfertas = new DaoOfertas();
$DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
$DaoEspecialidades = new DaoEspecialidades();
$DaoGrados = new DaoGrados();
$DaoPrerrequisitos = new DaoPrerrequisitos();
$DaoEvaluaciones = new DaoEvaluaciones();
$DaoUsuarios= new DaoUsuarios();
$_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
?>
<table id="tabla">
    <?php
    $text = "Nueva";
    $Nombre = "";
    $Clave_mat = "";
    $Precio = "";
    $Creditos = "";
    $Dias_porSemana = "";
    $Horas_porSemana = "";
    $Horas_conDocente = "";
    $Horas_independientes = "";
    $Tipo_aula = "";
    $Promedio_min = "";
    $Duracion_examen = "";
    $Max_alumExamen = "";

    if ($id_mat > 0) {
        $text = "Editar";
        $mat = $DaoMaterias->show($id_mat);
        $Nombre = $mat->getNombre();
        $Clave_mat = $mat->getClave_mat();
        $Precio = $mat->getPrecio();
        $Creditos = $mat->getCreditos();
        $Dias_porSemana = $mat->getDias_porSemana();
        $Horas_porSemana = $mat->getHoras_porSemana();
        $Horas_conDocente = $mat->getHoras_conDocente();
        $Horas_independientes = $mat->getHoras_independientes();
        $Tipo_aula = $mat->getTipo_aula();
        $Promedio_min = $mat->getPromedio_min();
        $Duracion_examen = $mat->getDuracion_examen();
        $Max_alumExamen = $mat->getMax_alumExamen();
        $id_mat = $id_mat;
    }else{
        $id_mat=0;
    }
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div class="box_top">
                    <h1><i class="fa fa-cog" aria-hidden="true"></i> <?php echo $text ?> materia</h1>
                </div>
                <div class="seccion">
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Nombre de la Materia<br><input type="text" value="<?php echo $Nombre ?>" id="Nombre"/></li>
                        <li>Clave de la materia<br><input type="text" value="<?php echo $Clave_mat ?>" id="Clave_mat"/></li>
                        <li>Precio de la Materia<br><input type="text" value="<?php echo $Precio ?>" id="Precio"/></li>
                        <li>Cr&eacute;ditos<br><input type="text" value="<?php echo $Creditos ?>" id="Creditos"/></li>
                        <li>Duraci&oacute;n (D&iacute;as por Semana)<br><input type="text" value="<?php echo $Dias_porSemana ?>" id="Dias_porSemana"/></li>
                        <li>Sesiones Por Semana<br><input type="text" id="Horas_porSemana" value="<?php echo $Horas_porSemana ?>"/></li>
                        <li>Sesiones con Docente<br><input type="text" id="Horas_conDocente" value="<?php echo $Horas_conDocente ?>"/></li>
                        <li>Sesiones Independientes<br><input type="text" id="Horas_independientes" value="<?php echo $Horas_independientes ?>"/></li>
                        <li>Tipo de Aula<br><select id="Tipo_aula">
                                <option value="0"></option>
                                <option value="1" <?php if ($Tipo_aula == "1") { ?> selected="selected"<?php } ?>>Laboratorio</option>
                                <option value="2" <?php if ($Tipo_aula == "2") { ?> selected="selected"<?php } ?>>Instrucci&oacute;n</option>
                                <option value="3" <?php if ($Tipo_aula == "3") { ?> selected="selected"<?php } ?>>Especial</option>
                                <option value="4" <?php if ($Tipo_aula == "4") { ?> selected="selected"<?php } ?>>Te&oacute;rica</option>
                            </select>
                        </li>
                        <li>Promedio m&iacute;nimo<br><input type="text" id="Promedio_min" value="<?php echo $Promedio_min ?>"/></li>
                        <li>Duracion del Examen (Minutos)<br><input type="text" id="Duracion_examen" value="<?php echo $Duracion_examen ?>"/></li>
                        <li> M&aacute;ximo de Alumnos en Examen<br><input type="text" id="Max_alumExamen" value="<?php echo $Max_alumExamen ?>"/></li>
                    </ul>
                </div>
                <?php
                if ($id_mat > 0) {
                    ?>
                    <div class="seccion">
                        <h2>Especialidades asociadas</h2>
                        <span class="linea"></span>
                    </div>
                    <div id="box_ofertas_compartidas">
                        <?php
                        foreach ($DaoMateriasEspecialidad->getMateriasEspecialidadByIdMat($id_mat) as $matEsp) {
                            $esp = $DaoEspecialidades->show($matEsp->getId_esp());
                            ?>
                            <div class="oferta">
                                <div class="seccion">
                                    <table class="tabla-select">
                                        <thead>
                                            <tr>
                                                <td>Oferta</td>
                                                <td>Especialidad</td>
                                                <td>Grado ( para que cuatrimestre)</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <?php
                                                    foreach ($DaoOfertas->showAll() as $oferta) {
                                                        if ($oferta->getId() == $esp->getId_ofe()) {
                                                            echo $oferta->getNombre_oferta();
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    foreach ($DaoEspecialidades->getEspecialidadesOferta($esp->getId_ofe()) as $espe) {
                                                        if ($espe->getId() == $matEsp->getId_esp()) {
                                                            echo $espe->getNombre_esp();
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    foreach ($DaoGrados->getGradosEspecialidad($esp->getId()) as $gr) {
                                                        if ($gr->getId() == $matEsp->getGrado_mat()) {
                                                            echo $gr->getGrado();
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="tabla-checkbox">
                                        <thead>
                                            <tr>
                                                <td>Evaluacion Extraordinaria<br>(Equivale a Calificación final)</td>
                                                <td>Evaluacion Especial<br>(Equivale a Calificación final)<br></td>
                                                <td>LLeva orientaci&oacute;n</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?php if ($matEsp->getEvaluacion_extraordinaria() == 1) { ?> SI <?php } else { ?> NO <?php } ?></td>
                                                <td><?php if ($matEsp->getEvaluacion_especial() == 1) { ?> SI <?php } else { ?> NO <?php } ?></td>
                                                <td><?php if ($matEsp->getTiene_orientacion() == 1) { ?> SI <?php } else { ?> NO <?php } ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="tabla-nombre">
                                        <thead>
                                            <tr>
                                                <td colspan="3">Nombre especial</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?php echo $matEsp->getNombreDiferente() ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="box">
                                        <table class="list_evaluaciones">
                                            <thead>
                                                <tr>
                                                    <td>Nombre</td>
                                                    <td>Ponderaci&oacute;n</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($DaoEvaluaciones->getEvaluacionesMat($matEsp->getId()) as $eva) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $eva->getNombre_eva() ?></td>
                                                        <td><?php echo $eva->getPonderacion() ?>%</td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="box">
                                        <div class="seccion">
                                            <table class="list_usu">
                                                <thead>
                                                    <tr>
                                                        <td>Materia</td>
                                                        <td>Clave</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($DaoPrerrequisitos->getPrerrequisitoByIdMatEsp($matEsp->getId()) as $prerrequisito) {
                                                        $mat_esp = $DaoMateriasEspecialidad->show($prerrequisito->getId_mat_esp_pre());
                                                        $mat = $DaoMaterias->show($mat_esp->getId_mat());
                                                        ?>
                                                        <tr>
                                                            <td style="width:200px;"><?php echo $mat->getNombre() ?></td>
                                                            <td><?php echo $mat->getClave_mat() ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>  
                                    </div>
                                </div>
                                <button class="auxiliares" onclick="delete_ofe_compartida(<?php echo $matEsp->getId() ?>)">Eliminar</button>
                                <button class="auxiliares" onclick="mostrar_ofe_compartida(<?php echo $matEsp->getId() ?>)">Editar</button>
                                <a href="plan_estudio.php?id=<?php echo $matEsp->getId_esp() ?>&id_mat=<?php echo $id_mat; ?>" target="_blank"><button class="auxiliares">Plan de estudios</button></a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
                <button id="button_ins" onclick="save_materia()">Guardar</button>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once '../estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="materia.php?id_esp=<?php echo $id_esp;?>" class="link">Nueva Materia</a></li>
                    <?php
                    if ($id_mat > 0) {
                        ?>
                        <li><span onclick="delete_materia(<?php echo $id_mat; ?>)">Eliminar Materia</span></li>
                        <li><span onclick="mostrar_ofe_compartida()">Añadir especialidad asociada</span></li>
                        <?php
                    }
                    ?>
                        <li><a href="curso.php?id=<?php echo $id_esp;?>" class="link">Regresar</a></li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_mat" value="<?php echo $id_mat ?>"/>
<input type="hidden" id="Id_esp" value="<?php echo $id_esp ?>"/>
     <?php
}



if($_POST['action']=="add_ponderacion"){
?>
  <tr>
      <td><input type="text" class="Nombre_eva" value="<?php echo $_POST['Nombre_eva']?>"/></td>
     <td><input type="text" class="ponderacion" value="<?php echo $_POST['ponderacion']?>"/></td>
     <td><button onclick="delete_pon_add(this)">Eliminar</button></td>
  </tr>
<?php
}


if($_POST['action']=="delete_pre"){
    $DaoPrerrequisitos= new DaoPrerrequisitos();
    $DaoPrerrequisitos->delete($_POST['Id_mat_pre']);
    update_page($_POST['Id_mat'],$_POST['Id_esp']);
}

if($_POST['action']=="delete_materia"){
    /*
     *Verificar el nuemro de asociaciones con otras especialidades
     * SI no tiene ninguna entonces poder eliminar la materia
     * 
     */
    $DaoMaterias= new DaoMaterias();
    $DaoMaterias->delete($_POST['Id_mat']);
    
}


if($_POST['action']=="delete_ponderacion"){
    $DaoEvaluaciones= new DaoEvaluaciones();
    $DaoEvaluaciones->delete($_POST['Id_eva']);
    update_page($_POST['Id_mat'],$_POST['Id_esp']);	
}


if ($_POST['action'] == "update_curso") {
  ?>
  <option value="0"></option>
  <?php
  $DaoEspecialidades= new DaoEspecialidades();
  foreach ($DaoEspecialidades->getEspecialidadesOferta($_POST['Id_oferta']) as $k => $v) {
  ?>
  <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre_esp(); ?> </option>
  <?php
  }
}

if($_POST['action'] == "update_grado") {
   $DaoGrados= new DaoGrados();
  ?>
  <option value="0"></option>
  <?php
  foreach($DaoGrados->getGradosEspecialidad($_POST['Id_esp']) as $k=>$v){
  ?>
      <option value="<?php echo $v->getId() ?>"> <?php echo $v->getGrado() ?> </option>
  <?php
  }
}


if($_POST['action']=="buscar_mat"){
        $DaoUsuarios= new DaoUsuarios();
        $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        $DaoMaterias= new DaoMaterias();
        $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
         $query = "SELECT * FROM Materias_especialidades 
         JOIN (SELECT * FROM materias_uml WHERE (Nombre LIKE '%".$_POST['buscar']."%' || Clave_mat LIKE '%".$_POST['buscar']."%' || Clave_mat LIKE '%".$_POST['buscar']."%') AND Activo_mat=1 AND Id_plantel=".$usu->getId_plantel().") as materias
         ON Materias_especialidades.Id_mat=materias.Id_mat
         WHERE Id_esp=".$_POST['Id_esp']." AND Activo_mat_esp=1";
        foreach($DaoMateriasEspecialidad->advanced_query($query) as $k=>$v){
            $mat=$DaoMaterias->show($v->getId_mat());
          ?>
           <li id_mat_esp="<?php echo $v->getId();?>"><?php echo $mat->getClave_mat()."-".$mat->getNombre() ?></li>
          <?php      
        }
}


if($_POST['action']=="mostrar_ofe_compartida"){
    $Id_mat_esp=0;
    $Id_ofe=0;
    $Id_esp=0;
    $DaoMaterias= new DaoMaterias();
    $DaoOfertas= new DaoOfertas();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoEvaluaciones= new DaoEvaluaciones();
    $DaoGrados= new DaoGrados();
    $DaoPrerrequisitos= new DaoPrerrequisitos();
    
    $Evaluacion_extraordinaria="";
    $Evaluacion_especial="";
    $Tiene_orientacion="";
    $NombreDiferente="";
    if(isset($_POST['Id_mat_esp']) && $_POST['Id_mat_esp']){
        $mat_esp = $DaoMateriasEspecialidad->show($_POST['Id_mat_esp']);
        $esp=$DaoEspecialidades->show($mat_esp->getId_esp());
        $Id_mat_esp=$_POST['Id_mat_esp'];
        $Id_ofe=$esp->getId_ofe();
        $Id_esp=$esp->getId();
        
        $Grado_mat=$mat_esp->getGrado_mat();
        $Evaluacion_extraordinaria=$mat_esp->getEvaluacion_extraordinaria();
        $Evaluacion_especial=$mat_esp->getEvaluacion_especial();
        $Tiene_orientacion=$mat_esp->getTiene_orientacion();
        $NombreDiferente=$mat_esp->getNombreDiferente();
    }    
    ?>
    <div class="oferta" id="box-layer">
     <div class="seccion">
      <ul class="form">
          <li>Oferta<br>
              <select class="ofertas_compartidas" onchange="update_curso(this)"> 
                  <option value="0"></option>
                    <?php
                    foreach($DaoOfertas->showAll() as $oferta) {
                             ?>
                            <option value="<?php echo $oferta->getId(); ?>" <?php if ($oferta->getId() == $Id_ofe) { ?> selected="selected" <?php } ?>><?php echo $oferta->getNombre_oferta() ?></option>
                  <?php
                    }
                    ?>
              </select>
          </li>
          <li>Especialidad<br>
              <select class="especialidades" onchange="update_grado(this)"> 
                  <option value="0"></option>
                 <?php
                     if($Id_ofe>0){
                        foreach ($DaoEspecialidades->getEspecialidadesOferta($Id_ofe) as $especialidad) {
                          ?>
                            <option value="<?php echo $especialidad->getId() ?>" <?php if($especialidad->getId()==$Id_esp){?> selected="selected" <?php } ?>><?php echo $especialidad->getNombre_esp() ?></option>
                          <?php
                        }
                     }
                    ?>
              </select>
          </li>
          <li>Grado ( para que cuatrimestre)<br>
                 <select class="Grado_mat">
                 <option value="0"></option>
                 <?php
                 if($Id_esp>0){
                     foreach ($DaoGrados->getGradosEspecialidad($Id_esp) as $gr) {
                      ?>
                       <option value="<?php echo $gr->getId()?>" <?php if($gr->getId()==$Grado_mat){?> selected="selected"<?php } ?>><?php echo $gr->getGrado()?></option>
                      <?php
                     }
                 }
                   ?>
              </select>
          </li>
        <li>Evaluacion Extraordinaria<br>(Equivale a Calificacion final)<br><input type="checkbox" class="Evaluacion_extraordinaria" <?php if($Evaluacion_extraordinaria==1){?> checked="checked" <?php } ?> ></li>
        <li>Evaluacion Especial<br>(Equivale a Calificacion final)<br><input type="checkbox" class="Evaluacion_especial" <?php if($Evaluacion_especial==1){?> checked="checked" <?php } ?> ></li>
        <li>LLeva orientaci&oacute;n<br><input type="checkbox" class="orientacion" <?php if($Tiene_orientacion==1){?> checked="checked" <?php } ?>></li>
        <li>Nombre especial<br><input type="text" id="nombre_especial" value="<?php echo $NombreDiferente?>"/></li>
      </ul>
      <div class="box">
      <ul class="form">
        
      </ul>
      <table id="box-ingresar-pronderaciones">
          <tr>
              <td>Nombre evaluaci&oacute;n<br><input type="text" class="nombre_ponderacion"/></td>
              <td>Valor evaluaci&oacute;n %<br><input type="number" class="valor_ponderacion"/></td>
          </tr>
      </table>
      <button class="add_ponderacion" onclick="add_ponderacion(this)">A&ntilde;adir ponderaci&oacute;n</button>
      <table class="list_evaluaciones">
        <thead>
          <tr>
            <td>Nombre</td>
            <td>Ponderaci&oacute;n %</td>
            <td>Acciones</td>
          </tr>
        </thead>
        <tbody>
         <?php
         if($Id_mat_esp>0){
            foreach ($DaoEvaluaciones->getEvaluacionesMat($Id_mat_esp) as $eva) {
                ?>
                  <tr id_pon="<?php echo $eva->getId()?>">
                     <td><input type="text" class="Nombre_eva" value="<?php echo $eva->getNombre_eva()?>"  /></td>
                     <td><input type="text" class="ponderacion" value="<?php echo $eva->getPonderacion()?>"/></td>
                     <td><button onclick="delete_ponderacion(<?php echo $eva->getId()?>)">Eliminar</button></td>
                  </tr>
                <?php
            }
         }
          ?>
        </tbody>
      </table>
      </div>
        <div class="box">
          <div class="seccion">
                <ul class="form">
                  <li>Buscar Materia<br><input type="text" class="buscarMateria" onkeyup="buscar_mat(this)" placeholder="Buscar por nombre o clave"/> <button class="add_mat" onclick="add_materia(this)">A&ntilde;adir Pre-requisito</button>
                      <ul class="buscador_int"></ul>
                  </li>
                </ul>
                <table class="list_usu">
                  <thead>
                    <tr>
                      <td>Materia</td>
                      <td>Clave</td>
                      <td>Acciones</td>
                    </tr>
                  </thead>
                  <tbody>
                     <?php
                     if($Id_mat_esp>0){
                        foreach ($DaoPrerrequisitos->getPrerrequisitoByIdMatEsp($Id_mat_esp) as $prerrequisito) {
                           $mat_esp = $DaoMateriasEspecialidad->show($prerrequisito->getId_mat_esp_pre());
                           $mat = $DaoMaterias->show($mat_esp->getId_mat());
                           ?>
                           <tr id_mat_esp="<?php echo $prerrequisito->getId()?>">
                               <td style="width:200px;"><input type="text" class="Nombre_pre" value="<?php echo $mat->getNombre()?>"/></td>
                               <td><input type="text" class="Clave_mat" value="<?php echo $mat->getClave_mat()?>"/></td>
                               <td><button onclick="delete_pre(<?php echo $prerrequisito->getId()?>)">Eliminar</button></td>
                            </tr>
                           <?php
                       }
                     }
                    ?>
                  </tbody>
                </table>
              </div>  
        </div>
    </div>
        <button class="button" onclick="save_materia_esp(<?php echo $Id_mat_esp;?>,this)">Guardar</button>
        <button class="button" onclick="ocultar_error_layer()">Cancelar</button>
    </div>
    <?php
}


if($_POST['action']=="save_materia_esp"){
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoEvaluaciones= new DaoEvaluaciones();
    $DaoPrerrequisitos= new DaoPrerrequisitos();
    
    if(isset($_POST['Id_mat_esp']) && $_POST['Id_mat_esp']>0){
       $MateriasEspecialidad=$DaoMateriasEspecialidad->show($_POST['Id_mat_esp']);
       $MateriasEspecialidad->setGrado_mat($_POST['Grado_mat']);
       $MateriasEspecialidad->setEvaluacion_especial($_POST['Evaluacion_especial']);
       $MateriasEspecialidad->setEvaluacion_extraordinaria($_POST['Evaluacion_extraordinaria']);
       $MateriasEspecialidad->setTiene_orientacion($_POST['orientacion']);
       $MateriasEspecialidad->setNombreDiferente($_POST['NombreDiferente']);
       $DaoMateriasEspecialidad->update($MateriasEspecialidad);
       $id_mat_esp=$_POST['Id_mat_esp'];   
    }else{
       $MateriasEspecialidad= new MateriasEspecialidad();
       $MateriasEspecialidad->setId_esp($_POST['Id_esp']);
       $MateriasEspecialidad->setId_mat($_POST['Id_mat']);
       $MateriasEspecialidad->setGrado_mat($_POST['Grado_mat']);
       $MateriasEspecialidad->setEvaluacion_especial($_POST['Evaluacion_especial']);
       $MateriasEspecialidad->setEvaluacion_extraordinaria($_POST['Evaluacion_extraordinaria']);
       $MateriasEspecialidad->setTiene_orientacion($_POST['orientacion']);
       $MateriasEspecialidad->setNombreDiferente($_POST['NombreDiferente']);
       $MateriasEspecialidad->setActivo_mat_esp(1);
       $id_mat_esp=$DaoMateriasEspecialidad->add($MateriasEspecialidad);
    }
    
    if(isset($_POST['evaluaciones'])){
        foreach($_POST['evaluaciones'] as $k=>$v){
            if(isset($v['Id_eva']) && $v['Id_eva']>0){
               $Evaluaciones=$DaoEvaluaciones->show($v['Id_eva']);
               $Evaluaciones->setPonderacion($v['Ponderacion']);
               $Evaluaciones->setNombre_eva($v['Nombre_eva']);
               $DaoEvaluaciones->update($Evaluaciones);

            }else{
               $Evaluaciones= new Evaluaciones();
               $Evaluaciones->setPonderacion($v['Ponderacion']);
               $Evaluaciones->setNombre_eva($v['Nombre_eva']);
               $Evaluaciones->setId_mat_esp($id_mat_esp);
               $Evaluaciones->setActivo_eva(1);
               $DaoEvaluaciones->add($Evaluaciones); 
            }
        }
    }
    
    if(isset($_POST['materias'])){
        $DaoPrerrequisitos->deleteByIdMatEsp($id_mat_esp);
        foreach($_POST['materias'] as $k=>$v){
              $Prerrequisitos= new Prerrequisitos();
              $Prerrequisitos->setId_mat_esp($id_mat_esp);
              $Prerrequisitos->setId_mat_esp_pre($v['Id_mat_esp_pre']);
              $DaoPrerrequisitos->add($Prerrequisitos);

        }
    }
}
