<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 


if (isset($_POST['action']) && $_POST['action'] == "filtro") {

    $DaoDocentes = new DaoDocentes();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoPermutasClase = new DaoPermutasClase();
    $DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
    $DaoGrupos = new DaoGrupos();
    $DaoOfertas = new DaoOfertas();
    $DaoAsistencias = new DaoAsistencias();
    $DaoCiclos = new DaoCiclos();
    $DaoPenalizacionDocente = new DaoPenalizacionDocente();
    $base = new base();
    $DaoDocenteExamen= new DaoDocenteExamen();
    $DaoCategoriasPago = new DaoCategoriasPago();
    $DaoNivelesPago = new DaoNivelesPago();
    $DaoOfertasPorCategoria = new DaoOfertasPorCategoria();
    $DaoEvaluacionDocente = new DaoEvaluacionDocente();
    $DaoPuntosPorCategoriaEvaluadaDocente = new DaoPuntosPorCategoriaEvaluadaDocente();
    $cicloActual = $DaoCiclos->getActual();

    $query = "";
    if ($_POST['Id_ofe'] > 0) {
        $query = $query . " AND Id_oferta=" . $_POST['Id_ofe'];
    }
    $Id_ciclo = $cicloActual->getId();
    if ($_POST['Id_ciclo'] > 0) {
        $Id_ciclo = $_POST['Id_ciclo'];
        $cicloActual = $DaoCiclos->show($_POST['Id_ciclo']);
    }

    $Fecha_ini = $cicloActual->getFecha_ini();
    if (strlen($_POST['Fecha_ini']) > 0) {
        $Fecha_ini = $_POST['Fecha_ini'];
    }

    $Fecha_fin = date('Y-m-d');
    if (strlen($_POST['Fecha_fin']) > 0) {
        $Fecha_fin = $_POST['Fecha_fin'];
    }

    if ($Id_ciclo > 0) {
        foreach ($DaoDocentes->showAll() as $docente) {
            if ($docente->getTiempo_completo() != 1) {
                $Id_docen = $docente->getId();
                if (isset($_POST['Id_docen']) && $_POST['Id_docen'] > 0) {
                    $Id_docen = $_POST['Id_docen'];
                }
                if ($docente->getId() == $Id_docen) {
                    
                    //Totales
                    $count=1;
                    $totalHorasTrabajadas=0;
                    $totalHorasPorSemana=0;
                    $totalPenalizaciones=0;
                    $totalPaga=0;
                    $totalHorasExamen=0;
                    foreach($DaoOfertas->showAll() as $ofe){
                        $Id_ofe=$ofe->getId();
                        if ($_POST['Id_ofe'] > 0) {
                            $Id_ofe = $_POST['Id_ofe'];
                        }
                        if ($ofe->getId() == $Id_ofe) {
                        $Tipo="";
                        $nombreNivel="";
                        $PagoXhora="0";
                        $puntos="0";
                        $totalPenalizacionGrupo=0;
                        $horas_trabajadas=0;
                        $horasXSemana="0";
                        $TotalHorasPermuta = 0;
                        $TotalHorasCedidas = 0;
                        $descuentoPorPenalizacion="0";
                        $TotalHoras="0";
                        $total="0";

                        //Informacion de los grupos que imparte el docente
                        $InfoHoras=$DaoDocentes->getNumGruposDocente($docente->getId(),$Id_ciclo,$ofe->getId(),$Fecha_ini,$Fecha_fin);

                        //Informacion de los grupos que imparte el docente
                        foreach($InfoHoras['Grupos'] as $x){
                            $horas_trabajadas+=$x['HorasPor_semana_trabajados'];
                            $horasXSemana+=$x['HorasPor_semana'];
                              foreach($DaoPenalizacionDocente->getPenalizacionesDocente($docente->getId(), $Id_ciclo,$x['Id_grupo'],$Fecha_ini,$Fecha_fin) as $penalizacion){
                                    $totalPenalizacionGrupo++;
                                    $totalPenalizaciones++;
                              }
                        }

                        //Número de grupos del docente
                        $numGrupos=$InfoHoras['NumGrupos'];

                        //Número de horas de examen
                        $horasExamen=$DaoDocenteExamen->getTotalHorasTrabajadasExamen($Id_ciclo,$Id_docen,$ofe->getId(),null,$Fecha_ini,$Fecha_fin);

                        //Número horas de grupos permutados para la oferta
                        foreach ($DaoPermutasClase->getPermutasDocenteByCiclo($docente->getId(), $cicloActual->getId(),$Fecha_ini,$Fecha_fin) as $permuta) {
                            //Si el grupo pertenece a la oferta
                            $grupo = $DaoGrupos->show($permuta->getId_grupo());
                            $mat_esp = $DaoMateriasEspecialidad->show($grupo->getId_mat_esp());
                            $esp = $DaoEspecialidades->show($mat_esp->getId_esp());
                            $oferta = $DaoOfertas->show($esp->getId_ofe());
                            if ($oferta->getId() == $ofe->getId()) {
                                //Verificamos si capturo asistencias en el grupo
                                $existe = $DaoAsistencias->existAsistencia($permuta->getId_grupo(), $permuta->getDia());
                                if ($existe == 1) {
                                    //Cada permuta es una hora
                                    //Si el docente asignado para la permuta es el que esta en el ciclo
                                    if($permuta->getId_docente_asignado()==$docente->getId()){
                                        $TotalHorasPermuta++;
                                    }else{
                                        $TotalHorasCedidas++;
                                    }
                                }
                            }
                        }     

                        //Obtenemos la ultima evaluacion del docente
                        $evaluacion=$DaoEvaluacionDocente->getEvaluacionPorciclo($Id_docen, $Id_ciclo);
                        if($evaluacion->getId()==null){
                           $evaluacion = $DaoEvaluacionDocente->getUltimaEvaluacionDocente($Id_docen);
                        }
                        if ($evaluacion->getId() > 0) {
                            $ofertaPorCategoria = $DaoOfertasPorCategoria->getCategoriaPorOferta($ofe->getId());
                            $CategoriaTabulador = $DaoCategoriasPago->show($ofertaPorCategoria->getId_tipo());
                            if($CategoriaTabulador->getId()>0){
                                $Tipo = $CategoriaTabulador->getNombre_tipo();
                                $puntos = $evaluacion->getPuntosTotales();
                                $resul = $DaoPuntosPorCategoriaEvaluadaDocente->getNivelPorCategoria($evaluacion->getId(), $CategoriaTabulador->getId());
                                if ($resul->getId() > 0) {
                                    $nivel = $DaoNivelesPago->show($resul->getId_nivel());
                                    $nombreNivel = $nivel->getNombre_nivel();
                                    $PagoXhora = $nivel->getPagoNivel();
                                }
                            }
                        }

                        //Descuento por penalizacion 
                        if(isset($params['PorcentajeHoraPenalizada']) && $params['PorcentajeHoraPenalizada']>0 && $totalPenalizacionGrupo>0){
                           $descuentoPorPenalizacion=($params['PorcentajeHoraPenalizada']*$PagoXhora/100)*$totalPenalizacionGrupo;
                        }

                        //Total de horas 
                        $TotalHoras=$horas_trabajadas+($TotalHorasPermuta-$TotalHorasCedidas)+$horasExamen;

                        //Pago total
                        $total=($TotalHoras*$PagoXhora)-$descuentoPorPenalizacion; 
                        $nombre=$base->covertirCadena($docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen(),2);
                        ?>
                        <tr id_doc="<?php echo $docente->getId();?>" id-ciclo="<?php echo $cicloActual->getId()?>" id-ofe="<?php echo $ofe->getId()?>" fecha-ini="<?php echo $Fecha_ini?>" fecha-fin="<?php echo $Fecha_fin?>">,
                            <td><?php echo $count ?></td>
                            <td><?php echo $cicloActual->getClave() ?></td>
                            <td><?php echo $nombre ?></td>
                            <td><?php echo $ofe->getNombre_oferta(); ?></td>
                            <td style="text-align: center;"><?php echo $Tipo?></td>
                            <td style="text-align: center;"><?php echo $nombreNivel?></td>
                            <td style="text-align: center;"><?php echo $puntos;?></td>
                            <td style="text-align: center;">$<?php echo number_format($PagoXhora,2);?></td>
                            <td style="text-align: center;"><?php echo $Fecha_ini;?></td>
                            <td style="text-align: center;"><?php echo $Fecha_fin;?></td>
                            <td style="text-align: center;"><?php echo $numGrupos?></td>
                            <td style="text-align: center;"><?php echo $horasXSemana;?></td>
                            <td style="text-align: center;" onclick="mostrarDetalleExamenes(this)" class="detalle-examen"><?php echo $horasExamen;?></td>
                            <td style="text-align: center;"><?php echo $TotalHoras;?></td>
                            <td style="text-align: center;<?php if($totalPenalizacionGrupo>0){ ?> color:red; <?php } ?>" onclick="mostrarDetallePenalizaciones(this)" class="incidencias"><?php echo $totalPenalizacionGrupo;?></td>
                            <td style="text-align: center;">$<?php echo number_format($total,2);?></td>
                            <td style="text-align: center;">
                                <button onclick="mostrar_grupos(<?php echo $docente->getId()?>,<?php echo $cicloActual->getId()?>,<?php echo $ofe->getId()?>,'<?php echo $Fecha_ini?>','<?php echo $Fecha_fin?>')">Detalles</button>
                            </td>
                        </tr>
                        <?php
                        $count++;
                        $totalHorasPorSemana+=$horasXSemana;
                        $totalHorasTrabajadas+=$TotalHoras;
                        $totalHorasExamen+=$horasExamen;
                        $totalPaga+=$total;
                        }
                    }
                    ?>
                        <tr class="totales">
                            <td colspan="11">Total</td>
                            <td style="text-align: center;"><?php echo $totalHorasPorSemana;?></td>
                            <td style="text-align: center;"><?php echo $totalHorasExamen;?></td>
                            <td style="text-align: center;"><?php echo $totalHorasTrabajadas;?></td>
                            <td style="text-align: center;<?php if($totalPenalizaciones>0){ ?> color:red; <?php } ?>"><?php echo $totalPenalizaciones;?></td>
                            <td style="text-align: center;">$<?php echo number_format($totalPaga,2);?></td>
                            <td style="text-align: center;"></td>
                        </tr>
                        <?php
                }
            }
        }
    }
}
if(isset($_POST['action']) && $_POST['action'] == "mostrar_grupos"){
    $DaoDocentes=new DaoDocentes();
    $DaoMaterias= new DaoMaterias();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoPermutasClase = new DaoPermutasClase();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoGrupos= new DaoGrupos();
    $DaoOfertas= new DaoOfertas();
    $DaoAsistencias= new DaoAsistencias();
    $DaoCiclos= new DaoCiclos();
    $DaoPenalizacionDocente= new DaoPenalizacionDocente();
    $DaoDocenteExamen= new DaoDocenteExamen();
    $base= new base();

    $docente=$DaoDocentes->show($_POST['Id_docen']);
    $InfoHoras=$DaoDocentes->getNumGruposDocente($_POST['Id_docen'],$_POST['Id_ciclo'],$_POST['Id_ofe'],$_POST['Fecha_ini'],$_POST['Fecha_fin']); 
  ?>
    <div id="box_emergente" class="documentos_faltantes">
        <table class="table">
            <thead>
                <tr>
                    <td colspan="8" style="font-size: 11px;text-align: center;"><b>Grupos</b></td>
                </tr>
                <tr>
                  <td class="td-center">#</td>
                  <td class="td-center">Tipo</td>
                  <td class="td-center">Grupo</td>
                  <td>Materia</td>
                  <td class="td-center">Fecha inicio</td>
                  <td class="td-center">Fecha fin</td>
                  <td class="td-center">Sesiones trabajadas</td>
                  <td class="td-center">Sesiones penalizadas</td>
          </thead>
          
          <?php
                $count=1;
                $horas_trabajadas=0;
                $totalPenalizaciones=0;
                foreach($InfoHoras['Grupos'] as $k=>$v){
                    $grupo=$DaoGrupos->show($v['Id_grupo']);
                    $mat=$DaoMaterias->show($grupo->getId_mat());
                    
                    $totalPenalizacionGrupo=0;
                    
                    foreach($DaoPenalizacionDocente->getPenalizacionesDocente($_POST['Id_docen'],$_POST['Id_ciclo'],$v['Id_grupo'],$_POST['Fecha_ini'],$_POST['Fecha_fin']) as $penalizacion){
                            $totalPenalizacionGrupo++;
                            $totalPenalizaciones++;
                    }
                 ?>
                     <tr>
                         <td class="td-center"><?php echo $count;?></td>
                         <td class="td-center">Asignado</td>
                         <td class="td-center"><?php echo $grupo->getClave()?></td>
                         <td><?php echo $mat->getNombre()?></td>
                         <td class="td-center"><?php echo $_POST['Fecha_ini']?></td>
                         <td class="td-center"><?php echo $_POST['Fecha_fin']?></td>
                         <td class="td-center"><?php echo $v['HorasPor_semana_trabajados']?></td>
                         <td class="td-center" style="<?php if($totalPenalizacionGrupo>0){ ?> color:red; <?php } ?>"><?php echo $totalPenalizacionGrupo?></td>
                    </tr>
            <?php
                 $horas_trabajadas+=$v['HorasPor_semana_trabajados'];
                 $count++;
               }
 
               //Mostrar solo en el nivel al que pertenesca el grupo (lic,tec,prepa)
                //Insertar horas de grupos permutados
                $TotalHorasPermuta = 0;
                $TotalHorasCedidas = 0;
                foreach ($DaoPermutasClase->getPermutasDocenteByCiclo($_POST['Id_docen'], $_POST['Id_ciclo'],$_POST['Fecha_ini'],$_POST['Fecha_fin']) as $permuta) {
                    //Si el grupo pertenece a la oferta
                    $grupo = $DaoGrupos->show($permuta->getId_grupo());
                    $mat_esp = $DaoMateriasEspecialidad->show($grupo->getId_mat_esp());
                    $mat=$DaoMaterias->show($mat_esp->getId_mat());
                    $esp = $DaoEspecialidades->show($mat_esp->getId_esp());
                    $oferta = $DaoOfertas->show($esp->getId_ofe());
                    if ($oferta->getId() == $_POST['Id_ofe']) {
                        //Verificamos si capturo asistencias en el grupo
                        $existe = $DaoAsistencias->existAsistencia($permuta->getId_grupo(), $permuta->getDia());
                        if ($existe == 1) {
                            //Cada permuta es una hora
                            //Si el docente asignado para la permuta es el que esta en el ciclo
                            if($permuta->getId_docente_asignado()==$_POST['Id_docen']){
                                ?>
                                    <tr class="permuta">
                                        <td class="td-center"><?php echo $count;?></td>
                                        <td class="td-center">Permuta</td>
                                        <td class="td-center"><?php echo $grupo->getClave()?></td>
                                        <td><?php echo $mat->getNombre()?></td>
                                        <td class="td-center"><?php echo $_POST['Fecha_ini']?></td>
                                        <td class="td-center"><?php echo $_POST['Fecha_fin']?></td>
                                        <td class="td-center"><?php echo 1?></td>
                                        <td class="td-center"></td>
                                   </tr>
                                <?php
                                $count++;
                                $TotalHorasPermuta++;
                            }else{
                                $TotalHorasCedidas++;
                            }
                        }
                    }
                }
                
                //Horas de examen
                $totalHE=0;
                $horasExamen=$DaoDocenteExamen->getDetalleHorasTrabajadasExamen($_POST['Id_ciclo'],$_POST['Id_docen'],$_POST['Id_ofe'],null,$_POST['Fecha_ini'],$_POST['Fecha_fin']);    
                foreach($horasExamen as $row){
                    $start=  $base->getTxtHora($row['Start']);
                    $end=  $base->getTxtHora($row['End']);
                    $totalHorasExamen=$DaoDocenteExamen->getTotalHorasTrabajadasExamen($_POST['Id_ciclo'],$_POST['Id_docen'],$_POST['Id_ofe'],$row['Id_grupo'],$_POST['Fecha_ini'],$_POST['Fecha_fin']);    
                    ?>
                    <tr class="permuta">
                        <td class="td-center"><?php echo $count;?></td>
                        <td class="td-center">Exámen</td>
                        <td class="td-center"><?php echo $row['Clave']?></td>
                        <td><?php echo $row['Nombre']?></td>
                        <td class="td-center"><?php echo $row['FechaAplicacion']?></td>
                        <td class="td-center"><?php echo ""?></td>
                        <td class="td-center"><?php echo $totalHorasExamen?></td>
                        <td class="td-center"></td>
                    </tr>
                  <?php
                  $count++;
                  $totalHE+=$totalHorasExamen;
               }
                $TotalHoras=$horas_trabajadas+($TotalHorasPermuta-$TotalHorasCedidas)+$totalHE;
            ?>
                <tr>
                    <td colspan="6"><b>TOTAL</b></td>
                    <td style="text-align: center;"><b><?php echo $TotalHoras;?></b></td>
                    <td style="text-align: center;"><b><?php echo $totalPenalizaciones;?></b></td>
                </tr>
        </table>
        <button onclick="ocultar_error_layer()">Cerrar</button>
</div>
  <?php
}


if(isset($_POST['action']) && $_POST['action'] == "mostrarDetallePenalizaciones"){
    $DaoDocentes= new DaoDocentes();
    $DaoCiclos= new DaoCiclos();
    $DaoGrupos= new DaoGrupos();
    $DaoPenalizacionDocente= new DaoPenalizacionDocente();
    $DaoHoras= new DaoHoras();
    
    $docente=$DaoDocentes->show($_POST['Id_docen']);
    $ciclo=$DaoCiclos->show($_POST['Id_ciclo']);
    $base= new base();
    
    $Fecha_ini=$ciclo->getFecha_ini();
    if(strlen($_POST['Fecha_ini'])>0){
        $Fecha_ini=$_POST['Fecha_ini'];
    }
    
    $Fecha_fin=date('Y-m-d');
    if(strlen($_POST['Fecha_fin'])>0){
        $Fecha_fin=$_POST['Fecha_fin'];
    }
  ?>
    <div id="box_emergente" class="documentos_faltantes">
        <table class="table">
            <thead>
                <tr>
                    <td colspan="8" style="font-size: 11px;text-align: center;"><b>Incidencias</b></td>
                </tr>
                <tr>
                  <td class="td-center">#</td>
                  <td class="td-center">DÍA PENALIZADO</td>
                  <td class="td-center">GRUPO</td>
                  <td class="td-center">SESION</td>
                  <td class="td-center">FECHA DE CAPTURA</td>
                  <td class="td-center">COMENTARIOS</td>
          </thead>
          <tbody>
          <?php
                $count=1;
                $InfoHoras=$DaoDocentes->getNumGruposDocente($_POST['Id_docen'],$_POST['Id_ciclo'],$_POST['Id_ofe'],$Fecha_ini,$Fecha_fin);
                foreach($InfoHoras['Grupos'] as $k=>$v){
                    $grupo=$DaoGrupos->show($v['Id_grupo']);
                    $incidencias=$DaoPenalizacionDocente->getPenalizacionesDocente($_POST['Id_docen'],$_POST['Id_ciclo'],$grupo->getId(),$Fecha_ini,$Fecha_fin);
                    foreach($incidencias as $incidencia){
                        $hora=$DaoHoras->show($incidencia->getHora());
                    ?>
                    <tr>
                         <td class="td-center"><?php echo $count;?></td>
                         <td class="td-center"><?php echo $incidencia->getFecha()?></td>
                         <td class="td-center"><?php echo $grupo->getClave()?></td>
                         <td class="td-center"><?php echo $hora->getTexto_hora();?></td>
                         <td class="td-center"><?php echo $incidencia->getDateCreated()?></td>
                         <td class="td-center"><?php echo $incidencia->getComentarios()?></td>
                    </tr>
                  <?php
                    }
               }
            ?>
          </tbody>
        </table>
        <button onclick="ocultar_error_layer()">Cerrar</button>
</div>
  <?php
}


if(isset($_POST['action']) && $_POST['action'] == "mostrarDetalleExamenes"){
    $DaoDocentes= new DaoDocentes();
    $DaoCiclos= new DaoCiclos();
    $DaoGrupos= new DaoGrupos();
    $DaoHoras= new DaoHoras();
    $DaoDocenteExamen= new DaoDocenteExamen();
    $base= new base();
    
    $docente=$DaoDocentes->show($_POST['Id_docen']);
    $ciclo=$DaoCiclos->show($_POST['Id_ciclo']);
    $base= new base();
    
    $Fecha_ini=$ciclo->getFecha_ini();
    if(strlen($_POST['Fecha_ini'])>0){
        $Fecha_ini=$_POST['Fecha_ini'];
    }
    
    $Fecha_fin=date('Y-m-d');
    if(strlen($_POST['Fecha_fin'])>0){
        $Fecha_fin=$_POST['Fecha_fin'];
    }
  ?>
    <div id="box_emergente" class="documentos_faltantes">
        <table class="table">
            <thead>
                <tr>
                    <td colspan="8" style="font-size: 11px;text-align: center;"><b>Horas de exámen</b></td>
                </tr>
                <tr>
                  <td class="td-center">#</td>
                  <td class="td-center">DÍA EXÁMEN</td>
                  <td class="td-center">MATERIA</td>
                  <td class="td-center">AULA</td>
                  <td class="td-center">HORA INICIO</td>
                  <td class="td-center">HORA FIN</td>
                  <td class="td-center">NÚM. HORAS</td>
          </thead>
          <tbody>
          <?php
                $count=1;
                $totalT=0;
                $horasExamen=$DaoDocenteExamen->getDetalleHorasTrabajadasExamen($_POST['Id_ciclo'],$_POST['Id_docen'],$_POST['Id_ofe'],null,$Fecha_ini,$Fecha_fin);    
                foreach($horasExamen as $row){
                    $start=  $base->getTxtHora($row['Start']);
                    $end=  $base->getTxtHora($row['End']);
                    $totalHorasExamen=$DaoDocenteExamen->getTotalHorasTrabajadasExamen($_POST['Id_ciclo'],$_POST['Id_docen'],$_POST['Id_ofe'],$row['Id_grupo'],$Fecha_ini,$Fecha_fin);    
                    ?>
                    <tr>
                         <td class="td-center"><?php echo $count;?></td>
                         <td class="td-center"><?php echo $row['FechaAplicacion']?></td>
                         <td><?php echo $row['Nombre']?></td>
                         <td class="td-center"><?php echo $row['Clave_aula'];?></td>
                         <td class="td-center"><?php echo $start?></td>
                         <td class="td-center"><?php echo $end?></td>
                         <td class="td-center"><?php echo $totalHorasExamen?></td>
                    </tr>
                  <?php
                  $count++;
                  $totalT+=$totalHorasExamen;
               }
            ?>
                    <tr>
                        <td colspan="6"></td>
                        <td class="td-center"><b><?php echo $totalT?></b></td>
                    </tr>
          </tbody>
        </table>
        <button onclick="ocultar_error_layer()">Cerrar</button>
</div>
  <?php
}



if (isset($_GET['action']) && $_GET['action'] == "download_excel") {
    $base = new base();
    $DaoDocentes = new DaoDocentes();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoPermutasClase = new DaoPermutasClase();
    $DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
    $DaoGrupos = new DaoGrupos();
    $DaoOfertas = new DaoOfertas();
    $DaoAsistencias = new DaoAsistencias();
    $DaoCiclos = new DaoCiclos();
    $DaoPenalizacionDocente = new DaoPenalizacionDocente();
    $DaoDocenteExamen= new DaoDocenteExamen();
    $DaoParametrosPlantel = new DaoParametrosPlantel();
    $DaoCategoriasPago = new DaoCategoriasPago();
    $DaoNivelesPago = new DaoNivelesPago();
    $DaoOfertasPorCategoria = new DaoOfertasPorCategoria();
    $DaoEvaluacionDocente = new DaoEvaluacionDocente();
    $DaoPuntosPorCategoriaEvaluadaDocente = new DaoPuntosPorCategoriaEvaluadaDocente();

    $params = $DaoParametrosPlantel->getParametrosPlantelArray();
    $cicloActual = $DaoCiclos->getActual();
    
    $gris = "ACB9CA";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'CICLO');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'OFERTA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'TIPO');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'NIVEL');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'PUNTOS');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'PAGO POR HORA');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'FECHA INICIO');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'FECHA FIN');
    $objPHPExcel->getActiveSheet()->setCellValue('K1', 'GRUPOS');
    $objPHPExcel->getActiveSheet()->setCellValue('L1', 'HORAS POR SEMANA');
    $objPHPExcel->getActiveSheet()->setCellValue('M1', 'HORAS DE EXÁMEN');
    $objPHPExcel->getActiveSheet()->setCellValue('N1', 'HORAS TRABAJADAS');
    $objPHPExcel->getActiveSheet()->setCellValue('O1', 'PENALIZACIONES');
    $objPHPExcel->getActiveSheet()->setCellValue('P1', 'TOTAL');

    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');

    //Establece el width automatico de la celda 
    foreach ($base->xrange('A', 'P') as $columnID) {
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }



    $query = "";
    if (isset($_GET['Id_ofe']) && $_GET['Id_ofe'] > 0) {
        $query = $query . " AND Id_oferta=" . $_GET['Id_ofe'];
    }
    $Id_ciclo_eva=null;
    $Id_ciclo = $cicloActual->getId();
    if (isset($_GET['Id_ciclo']) && $_GET['Id_ciclo'] > 0) {
        $Id_ciclo = $_GET['Id_ciclo'];
        $Id_ciclo_eva = $_GET['Id_ciclo'];
        $cicloActual = $DaoCiclos->show($_GET['Id_ciclo']);
    }


    $Fecha_ini = $cicloActual->getFecha_ini();
    if (isset($_GET['Fecha_ini']) && strlen($_GET['Fecha_ini']) > 0) {
        $Fecha_ini = $_GET['Fecha_ini'];
    }

    $Fecha_fin = date('Y-m-d');
    if (isset($_GET['Fecha_fin']) && strlen($_GET['Fecha_fin']) > 0) {
        $Fecha_fin = $_GET['Fecha_fin'];
    }

    /* Datos del reporte de horas trabajadas */
    if ($Id_ciclo > 0) {
        $countRows=2;
        foreach ($DaoDocentes->showAll() as $docente) {
            if ($docente->getTiempo_completo() != 1) {
                $Id_docen = $docente->getId();
                if ($_GET['Id_docen'] > 0) {
                    $Id_docen = $_GET['Id_docen'];
                }
                if ($docente->getId() == $Id_docen) {
                    
                    //Totales
                    $count=1;
                    $totalHorasTrabajadas=0;
                    $totalHorasPorSemana=0;
                    $totalPenalizaciones=0;
                    $totalPaga=0;
                    $totalHorasExamen=0;
                    foreach($DaoOfertas->showAll() as $ofe){
                        $Id_ofe=$ofe->getId();
                        if ($_GET['Id_ofe'] > 0) {
                            $Id_ofe = $_GET['Id_ofe'];
                        }
                        if ($ofe->getId() == $Id_ofe) {
                        $Tipo="";
                        $nombreNivel="";
                        $PagoXhora="0";
                        $puntos="0";
                        $totalPenalizacionGrupo=0;
                        $horas_trabajadas=0;
                        $horasXSemana="0";
                        $TotalHorasPermuta = 0;
                        $TotalHorasCedidas = 0;
                        $descuentoPorPenalizacion="0";
                        $TotalHoras="0";
                        $total="0";

                        //Informacion de los grupos que imparte el docente
                        $InfoHoras=$DaoDocentes->getNumGruposDocente($docente->getId(),$Id_ciclo,$ofe->getId(),$Fecha_ini,$Fecha_fin);

                        //Informacion de los grupos que imparte el docente
                        foreach($InfoHoras['Grupos'] as $x){
                            $horas_trabajadas+=$x['HorasPor_semana_trabajados'];
                            $horasXSemana+=$x['HorasPor_semana'];
                              foreach($DaoPenalizacionDocente->getPenalizacionesDocente($docente->getId(), $Id_ciclo,$x['Id_grupo'],$Fecha_ini,$Fecha_fin) as $penalizacion){
                                    $totalPenalizacionGrupo++;
                                    $totalPenalizaciones++;
                              }
                        }

                        //Número de grupos del docente
                        $numGrupos=$InfoHoras['NumGrupos'];

                        //Número de horas de examen
                        $horasExamen=$DaoDocenteExamen->getTotalHorasTrabajadasExamen($Id_ciclo,$Id_docen,$ofe->getId(),null,$Fecha_ini,$Fecha_fin);

                        //Número horas de grupos permutados para la oferta
                        foreach ($DaoPermutasClase->getPermutasDocenteByCiclo($docente->getId(), $cicloActual->getId(),$Fecha_ini,$Fecha_fin) as $permuta) {
                            //Si el grupo pertenece a la oferta
                            $grupo = $DaoGrupos->show($permuta->getId_grupo());
                            $mat_esp = $DaoMateriasEspecialidad->show($grupo->getId_mat_esp());
                            $esp = $DaoEspecialidades->show($mat_esp->getId_esp());
                            $oferta = $DaoOfertas->show($esp->getId_ofe());
                            if ($oferta->getId() == $ofe->getId()) {
                                //Verificamos si capturo asistencias en el grupo
                                $existe = $DaoAsistencias->existAsistencia($permuta->getId_grupo(), $permuta->getDia());
                                if ($existe == 1) {
                                    //Cada permuta es una hora
                                    //Si el docente asignado para la permuta es el que esta en el ciclo
                                    if($permuta->getId_docente_asignado()==$docente->getId()){
                                        $TotalHorasPermuta++;
                                    }else{
                                        $TotalHorasCedidas++;
                                    }
                                }
                            }
                        }     

                        //Obtenemos la ultima evaluacion del docente
                        $evaluacion=$DaoEvaluacionDocente->getEvaluacionPorciclo($Id_docen, $Id_ciclo);
                        if($evaluacion->getId()==null){
                           $evaluacion = $DaoEvaluacionDocente->getUltimaEvaluacionDocente($Id_docen);
                        }
                        if ($evaluacion->getId() > 0) {
                            $ofertaPorCategoria = $DaoOfertasPorCategoria->getCategoriaPorOferta($ofe->getId());
                            $CategoriaTabulador = $DaoCategoriasPago->show($ofertaPorCategoria->getId_tipo());
                            if($CategoriaTabulador->getId()>0){
                                $Tipo = $CategoriaTabulador->getNombre_tipo();
                                $puntos = $evaluacion->getPuntosTotales();
                                $resul = $DaoPuntosPorCategoriaEvaluadaDocente->getNivelPorCategoria($evaluacion->getId(), $CategoriaTabulador->getId());
                                if ($resul->getId() > 0) {
                                    $nivel = $DaoNivelesPago->show($resul->getId_nivel());
                                    $nombreNivel = $nivel->getNombre_nivel();
                                    $PagoXhora = $nivel->getPagoNivel();
                                }
                            }
                        }

                        //Descuento por penalizacion 
                        if(isset($params['PorcentajeHoraPenalizada']) && $params['PorcentajeHoraPenalizada']>0 && $totalPenalizacionGrupo>0){
                           $descuentoPorPenalizacion=($params['PorcentajeHoraPenalizada']*$PagoXhora/100)*$totalPenalizacionGrupo;
                        }

                        //Total de horas 
                        $TotalHoras=$horas_trabajadas+($TotalHorasPermuta-$TotalHorasCedidas)+$horasExamen;

                        //Pago total
                        $total=($TotalHoras*$PagoXhora)-$descuentoPorPenalizacion; 
                        $nombre=$base->covertirCadena($docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen(),2);
                        $objPHPExcel->getActiveSheet()->setCellValue("A$countRows", $count);
                        $objPHPExcel->getActiveSheet()->setCellValue("B$countRows", $cicloActual->getClave());
                        $objPHPExcel->getActiveSheet()->setCellValue("C$countRows", $nombre);
                        $objPHPExcel->getActiveSheet()->setCellValue("D$countRows", $ofe->getNombre_oferta());
                        $objPHPExcel->getActiveSheet()->setCellValue("E$countRows", $Tipo);
                        $objPHPExcel->getActiveSheet()->setCellValue("F$countRows", $nombreNivel);
                        $objPHPExcel->getActiveSheet()->setCellValue("G$countRows", $puntos);
                        $objPHPExcel->getActiveSheet()->setCellValue("H$countRows", number_format($PagoXhora, 2));
                        $objPHPExcel->getActiveSheet()->setCellValue("I$countRows", $Fecha_ini);
                        $objPHPExcel->getActiveSheet()->setCellValue("J$countRows", $Fecha_fin);
                        $objPHPExcel->getActiveSheet()->setCellValue("K$countRows", $numGrupos);
                        $objPHPExcel->getActiveSheet()->setCellValue("L$countRows", $horasXSemana);
                        $objPHPExcel->getActiveSheet()->setCellValue("M$countRows", $horasExamen);
                        $objPHPExcel->getActiveSheet()->setCellValue("N$countRows", $TotalHoras);
                        $objPHPExcel->getActiveSheet()->setCellValue("O$countRows", $totalPenalizacionGrupo);
                        $objPHPExcel->getActiveSheet()->setCellValue("P$countRows", number_format($total, 2));

                        $countRows++;
                        $totalHorasPorSemana+=$horasXSemana;
                        $totalHorasTrabajadas+=$TotalHoras;
                        $totalHorasExamen+=$horasExamen;
                        $totalPaga+=$total;
                        }
                    }
                    
                    //Row suma de los totales
                    $objPHPExcel->getActiveSheet()->setCellValue("A$countRows", 'Total');
                    $objPHPExcel->getActiveSheet()->mergeCells("A$countRows:J$countRows");
                    $objPHPExcel->getActiveSheet()->setCellValue("L$countRows", $totalHorasPorSemana);
                    $objPHPExcel->getActiveSheet()->setCellValue("M$countRows", $totalHorasExamen);
                    $objPHPExcel->getActiveSheet()->setCellValue("N$countRows", $totalHorasTrabajadas);
                    $objPHPExcel->getActiveSheet()->setCellValue("O$countRows", $totalPenalizaciones);
                    $objPHPExcel->getActiveSheet()->setCellValue("P$countRows", number_format($totalPaga, 2));
                    $countRows++;
                    
                    $objPHPExcel->getActiveSheet()->mergeCells("A$countRows:O$countRows");
                    $countRows++;
                }
            }
        }
    }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=HorasTrabajadas-" . date('Y-m-d_H:i:s') . ".xlsx");
    echo $objWriter->save('php://output');
}
