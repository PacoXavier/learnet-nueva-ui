<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="mostrar_box_materias"){
   $DaoOfertas= new DaoOfertas();
   $DaoCiclosAlumno= new DaoCiclosAlumno();
   $DaoCiclos= new DaoCiclos();
?>
<div class="box-emergente-form box-materias">
	<h1>Asignar Materias</h1>
        <p>Ciclo:<br>
           <select id="ciclo">
                <option value="0"></option>
                <?php
                  foreach($DaoCiclosAlumno->getCiclosOferta($_POST['Id_oferta_alumno']) as $cicloAlumno){
                      $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());
                  ?>
                      <option value="<?php echo $cicloAlumno->getId()?>"><?php echo $ciclo->getClave()?></option>
                  <?php
                  }
                  ?>
            </select></p>
        <p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_grados_ofe()">
              <option value="0"></option>
            </select>
        </p>
        <p>Grado:<br>
            <select id="grado" onchange="update_materias()">
              <option value="0"></option>
            </select>
        </p>
   
        </p>
        <p>Materia:<br>
            <select id="lista_materias" onchange="verificar_orientaciones(this)"></select>
        </p>
        <div id="box_orientaciones"></div>
        <p>Tipo de materia:<select id="tipo_materia">
                <option value="0"></option>
                <option value="1">Materia curricular</option>
                <option value="2">Materia Extra</option>
            </select></p>
        <p><button onclick="validarPrerrequisitos()">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
</div>
<?php	
}



if ($_POST['action'] == "verificar_orientaciones") {
  $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
  $DaoOrientaciones= new DaoOrientaciones();
  
  $mat_esp=$DaoMateriasEspecialidad->show($_POST['Id_mat_esp']);
  if($mat_esp->getTiene_orientacion()==1){
      ?>
        <p>Orientaci&oacute;n<br>                                   
          <select id="orientaciones">
              <option value="0">Selecciona una orientaci&oacute;n</option>
              <?php
                  foreach ($DaoOrientaciones->getOrientacionesEspecialidad($mat_esp->getId_esp()) as $ori) {
                  ?>
                  <option value="<?php echo $ori->getId(); ?>"> <?php echo $ori->getNombre() ?> </option>
                  <?php
                  }
                  ?>
          </select></p>
    <?php
  }
}



if($_POST['action']=="update_materias_grado"){
   $DaoMaterias = new DaoMaterias();
   $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
  ?>
    <option value="0"></option>
      <?php
      foreach($DaoMateriasEspecialidad->getMateriasGrado($_POST['Id_grado']) as $k=>$v){
             $mat=$DaoMaterias->show($v->getId_mat());
             $NombreMat=$mat->getNombre();
             if(strlen($v->getNombreDiferente())>0){
                $NombreMat=$v->getNombreDiferente(); 
             }
      ?>
    <option value="<?php echo $v->getId()?>"><?php echo $mat->getClave_mat()." - ".$NombreMat?></option>
      <?php
      }
      ?>
<?php
}



if($_POST['action']=="delete_ciclo_mat"){
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoPagosCiclo= new DaoPagosCiclo();
    
    $mat = $DaoMateriasCicloAlumno->show($_POST['Id_ciclo_mat']);
    if(strlen($mat->getIds_pagos())>0){
        $ids_pagos = explode(",", $mat->getIds_pagos());

        foreach($ids_pagos as $v){
             $DaoPagosCiclo->delete($v);
        }
    }
    
    //Crear historial
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $DaoCiclos= new DaoCiclos();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    
    $materia_ciclo=$DaoMateriasCicloAlumno->show($_POST['Id_ciclo_mat']);

    $mat_esp=$DaoMateriasEspecialidad->show($materia_ciclo->getId_mat_esp());
    $mat=$DaoMaterias->show($mat_esp->getId_mat());

    $alum=$DaoAlumnos->show($materia_ciclo->getId_alum());
    $cicloAlumno=$DaoCiclosAlumno->show($materia_ciclo->getId_ciclo_alum());
    $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());

    $TextoHistorial="Elimino la materia ".$mat->getNombre()."  para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ciclo ".$ciclo->getClave();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Asignar materias");
        
    $DaoMateriasCicloAlumno->delete($_POST['Id_ciclo_mat']);
    update_list_materias($_POST['Id_oferta_alumno']);
}

if($_POST['action']=="baja_ciclo_mat"){
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $MateriasCicloAlumno=$DaoMateriasCicloAlumno->show($_POST['Id_ciclo_mat']);
    $MateriasCicloAlumno->setActivo(0);
    $DaoMateriasCicloAlumno->update($MateriasCicloAlumno);
    
    //Crear historial
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $DaoCiclos= new DaoCiclos();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    
    $materia_ciclo=$DaoMateriasCicloAlumno->show($_POST['Id_ciclo_mat']);

    $mat_esp=$DaoMateriasEspecialidad->show($materia_ciclo->getId_mat_esp());
    $mat=$DaoMaterias->show($mat_esp->getId_mat());

    $alum=$DaoAlumnos->show($materia_ciclo->getId_alum());
    $cicloAlumno=$DaoCiclosAlumno->show($materia_ciclo->getId_ciclo_alum());
    $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());

    $TextoHistorial="Aplico baja de materia ".$mat->getNombre()."  para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ciclo ".$ciclo->getClave();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Asignar materias");
    
    update_list_materias($_POST['Id_oferta_alumno']);
}





function update_list_materias($Id_oferta_alumno){
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoAlumnos = new DaoAlumnos();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoCiclos = new DaoCiclos();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoGrados= new DaoGrados();
    
    $DaoUsuarios=new DaoUsuarios();
    $_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $oferta_alumno = $DaoOfertasAlumno->show($Id_oferta_alumno);
    $alum = $DaoAlumnos->show($oferta_alumno->getId_alum());
    $oferta = $DaoOfertas->show($oferta_alumno->getId_ofe());
    $esp = $DaoEspecialidades->show($oferta_alumno->getId_esp());
    $nombre_ori="";
    if ($oferta_alumno->getId_ori() > 0) {
        $ori = $DaoOrientaciones->show($oferta_alumno->getId_ori());
        $nombre_ori = $ori->getNombre();
    }

    $primer_ciclo = $DaoOfertasAlumno->getFirstCicloOferta($Id_oferta_alumno);
    $ultimo_ciclo = $DaoOfertasAlumno->getLastCicloOferta($Id_oferta_alumno);

    $Opcion_pago = "";
    if ($oferta_alumno->getOpcionPago() == 1) {
        $Opcion_pago = "Plan por materias";
    } else {
        $Opcion_pago = "Plan completo";
    }
?>


<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div class="seccion">
                    <table  id="info_alumn">
                        <tbody><tr><th style="padding-bottom: 15px;">
                                    <font size="3">DATOS DEL ESTUDIANTE</font>
                                </th></tr>
                            <tr><td>
                                    <table width="100%">
                                        <tbody><tr>
                                                <th ><font>C&oacute;digo:</font></th>
                                                <td><font><?php echo $alum->getMatricula() ?></font></td>
                                                <th ><font>Nombre:</font></th>
                                                <td colspan="5"><font><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></font></td>
                                            </tr>
                                            <tr>
                                                <th><font>Nivel:</font></th>
                                                <td><font><?php echo $oferta->getNombre_oferta(); ?></font></td>
                                                <th><font>Admisi&oacute;n:</font></th>
                                                <td><?php echo $primer_ciclo['Clave']; ?></font></td>
                                                <th><font>Último ciclo:</font></th>
                                                <td><font><?php echo $ultimo_ciclo['Clave']; ?></font></td>
                                            </tr>
                                            <tr>
                                                <th><font>Carrera:</font></th>
                                                <td colspan="7"><font><?php echo $esp->getNombre_esp(); ?></font></td>
                                            </tr>
                                            <tr>
                                                <th><font>Orientaci&oacute;n:</font></th>
                                                <td><font color="navy" face="arial" size="2"><?php echo $nombre_ori; ?></font></td>
                                                <th><font>Opci&oacute;n de pago</font></th>
                                                <td><font color="navy" face="arial" size="2"><?php echo $Opcion_pago; ?></font></td>
                                            </tr>
                                        </tbody></table>
                                </td></tr>
                        </tbody>
                    </table>
                </div>
                <?php
                
                foreach ($DaoCiclosAlumno->getCiclosOferta($Id_oferta_alumno) as $cicloAlumno) {
                    $query = "SELECT * FROM Pagos_ciclo WHERE Concepto='Mensualidad' AND Id_ciclo_alum=".$cicloAlumno->getId()." AND Id_alum=".$alum->getId()." AND DeadCargo IS NULL";
                    $totalRows_Pagos_ciclo=$DaoPagosCiclo->getTotalRows($query);
                    $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());
                    ?>
                    <div class="ciclo_alumno">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td colspan="7" style="font-size:12px;text-align:center;"><b>Ciclo <?php echo $ciclo->getClave()?></b>
                                        <?php
                                        if($oferta->getTipoOferta()==2){
                                            ?>
                                            <button class="button-asignar" onclick="mostrarBoxAsignarMateriaCurso(<?php echo $cicloAlumno->getid()?>)">Agregar materia</button>
                                            <?php
                                         }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Clave</td>
                                    <td style="width:300px;">Materia</td>
                                    <td>Orientaci&oacute;n</td>
                                    <td>Tipo</td>
                                    <td>Nivel</td>
                                    <td>Creditos</td>
                                    <?php
                                        if($oferta_alumno->getOpcionPago()==2){
                                            ?>
                                        <td>Acciones</td>
                                    <?php
                                        }elseif($oferta_alumno->getOpcionPago()==1){
                                         if($totalRows_Pagos_ciclo==0){
                                         ?>
                                               <td>Acciones</td>
                                        <?php
                                              }
                                        }
                                     ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($DaoMateriasCicloAlumno->getMateriasCicloAlumno($cicloAlumno->getId()) as $materiasCiclo) {
                                        $mat_esp = $DaoMateriasEspecialidad->show($materiasCiclo->getId_mat_esp());
                                        
                                        $mat = $DaoMaterias->show($mat_esp->getId_mat());
                                        $grado=$DaoGrados->show($mat_esp->getGrado_mat());
                                        
                                        $nom_ori="";
                                        if($materiasCiclo->getId_ori()>0){
                                         $ori=$DaoOrientaciones->show($materiasCiclo->getId_ori());
                                         $nom_ori=$ori->getNombre();
                                        }
                                        
                                        $tipoMat="";
                                        if($materiasCiclo->getTipo()==1){
                                           $tipoMat="Normal"; 
                                        }elseif($materiasCiclo->getTipo()==2){
                                           $tipoMat="Extra";  
                                        }
                                        $NombreMat=$mat->getNombre();
                                        if(strlen($mat_esp->getNombreDiferente())>0){
                                            $NombreMat=$mat_esp->getNombreDiferente(); 
                                        }
                                        if($materiasCiclo->getActivo()==1){
                                    ?>
                                    <tr>
                                        <td><?php echo $mat->getClave_mat()?></td>
                                        <td><?php echo $NombreMat?></td>
                                        <td><?php echo $nom_ori;?></td>
                                        <td><?php echo $tipoMat;?></td>
                                        <td><?php echo $grado->getGrado()?></td>
                                        <td><?php echo $mat->getCreditos()?></td>
                                       <?php
                                            if($oferta_alumno->getOpcionPago()==2){
                                                ?>
                                        <td>
                                            <button onclick="delete_ciclo_mat(<?php echo $materiasCiclo->getId()?>)">Eliminar</button>
                                            <button onclick="baja_ciclo_mat(<?php echo $materiasCiclo->getId()?>)">Dar de baja</button>
                                        </td>
                                        <?php
                                            }elseif($oferta_alumno->getOpcionPago()==1){
                                               if($totalRows_Pagos_ciclo==0){
                                               ?>
                                               <td>
                                                 <button onclick="delete_ciclo_mat(<?php echo $materiasCiclo->getId()?>)">Eliminar</button>
                                               </td>
                                              <?php
                                              }
                                        
                                            }
                                         ?>
                                    </tr>
                                    <?php
                                        }
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                           if($oferta_alumno->getOpcionPago()==1 && count($DaoMateriasCicloAlumno->getMateriasCicloAlumno($cicloAlumno->getId()))>0){

                            if($totalRows_Pagos_ciclo==0){
                         ?>
                                <p class="nota">Generar mensualidades cuando se hayan agregado todas las materias que cursar&aacute; el alumno durante el ciclo</p>
                                <?php
                                if($oferta->getTipoOferta()==1){
                                    ?>
                                      <button class="cobro" onclick="generar_cobro(<?php echo $cicloAlumno->getId()?>)">Generar mensualidades</button>
                                  <?php
                                }else{
                                    ?>
                                      <button class="cobro" onclick="generar_cobro_curso(<?php echo $cicloAlumno->getId()?>)">Generar mensualidades</button>
                                    <?php
                                }

                              }else{
                                  if($oferta->getTipoOferta()==1){
                                  ?>
                                       <p class="mensualidades_generadas"><a href="pago.php?id=<?php echo $Id_oferta_alumno?>">Mensualidades generadas</a></p>
                                <?php
                                  }else{
                                      ?>
                                      <p class="mensualidades_generadas"><a href="cargos_curso.php?id=<?php echo $Id_oferta_alumno ?>">Mensualidades generadas</a></p>
                                   <?php
                                  }
                              }
                            }
                         ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once '../estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="ofertas_alumno.php?id=<?php echo $alum->getId(); ?>" class="link">Regresar</a></li>
                    <?php
                    if($oferta->getTipoOferta()==1){
                       ?>
                        <li><span onclick="mostrar_box_materias()">Agregar materia</span></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </td>
    </tr>
    <?php
}

if($_POST['action']=="generar_cobro"){
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $params=$DaoParametrosPlantel->getParametrosPlantelArray();
    
    $query = "SELECT * FROM Pagos_ciclo WHERE Concepto='Mensualidad' AND Id_ciclo_alum=".$_POST['Id_ciclo_alum']." AND Id_alum=".$_POST['Id_alum']." AND DeadCargo IS NULL";
    $existe=$DaoPagosCiclo->existeQuery($query);
    if($existe->getId()==0 || $existe->getId()==null){

        $mensualidad=0;
        foreach ($DaoMateriasCicloAlumno->getMateriasCicloAlumno($_POST['Id_ciclo_alum']) as $materiaCiclo) {
            if($materiaCiclo->getTipo()==1){
                 $mat_esp = $DaoMateriasEspecialidad->show($materiaCiclo->getId_mat_esp());
                 $mat = $DaoMaterias->show($mat_esp->getId_mat());
                 $mensualidad+=$mat->getPrecio();
            }
        }
        
        $DaoCiclosAlumno= new DaoCiclosAlumno();
        $cicloAlumno=$DaoCiclosAlumno->show($_POST['Id_ciclo_alum']);
        
        $DaoCiclos= new DaoCiclos();
        $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());
        
        //Se generan los pagos apartir de la fecha deinicio del ciclo
         $fecha_pago=$ciclo->getFecha_ini();
         $anio=date("Y",strtotime($ciclo->getFecha_ini()));
         $mes=date("m",strtotime($ciclo->getFecha_ini()));
        
         $DaoEspecialidades= new DaoEspecialidades();
         $esp=$DaoEspecialidades->show($_POST['Id_esp']);

         $DaoPagosCiclo= new DaoPagosCiclo();
         $base= new base();
         
         //Insertamos las mensualidades
        for($i=1;$i<=$esp->getNum_pagos();$i++){
            
            $d=1;
            if(isset($params['DiaInicioMensualidades']) && strlen($params['DiaInicioMensualidades'])>0){
               $d=$params['DiaInicioMensualidades'];
            }
            $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes + $i, $d, $anio));
                        
            $PagosCiclo= new PagosCiclo();
            $PagosCiclo->setConcepto("Mensualidad");
            $PagosCiclo->setFecha_pago($base->get_day_pago($fecha_pago,$ciclo->getId()));
            $PagosCiclo->setMensualidad($mensualidad);
            $PagosCiclo->setId_ciclo_alum($_POST['Id_ciclo_alum']);
            $PagosCiclo->setId_alum($_POST['Id_alum']);
            $PagosCiclo->setTipo_pago("pago");
            $PagosCiclo->setIdRel($_POST['Id_alum']);
            $PagosCiclo->setTipoRel("alum");
            $DaoPagosCiclo->add($PagosCiclo);

        } 
        update_list_materias($_POST['Id_oferta_alumno']);
    }
}



if($_POST['action']=="save_materia"){
    
    $queryx="";
    if(isset($_POST['Id_ori']) && $_POST['Id_ori']>0){
       $queryx=" AND Id_ori=". $_POST['Id_ori'];
    }
    $base= new base();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoCiclos= new DaoCiclos();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoMaterias= new DaoMaterias();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $params=$DaoParametrosPlantel->getParametrosPlantelArray();
    
    $ofertaAlumno= $DaoOfertasAlumno->show($_POST['Id_oferta_alumno']);
    $esp=$DaoEspecialidades->show($ofertaAlumno->getId_esp());
    
    $query = "SELECT * FROM materias_ciclo_ulm WHERE Id_ciclo_alum=".$_POST['Id_ciclo_alum']." AND Id_mat_esp=".$_POST['Id_mat_esp'].$queryx." AND Activo=1";
    $existeQuery=$DaoMateriasCicloAlumno->existeQuery($query);
    if(($existeQuery->getId()==0 || $existeQuery->getId()==null && $_POST['Tipo_mat']==1) || $_POST['Tipo_mat']==2){
        $existe=0;
        
        $MateriasCicloAlumno= new MateriasCicloAlumno();
        $MateriasCicloAlumno->setId_ciclo_alum($_POST['Id_ciclo_alum']);
        $MateriasCicloAlumno->setId_mat_esp($_POST['Id_mat_esp']);
        $MateriasCicloAlumno->setId_ori(isset($_POST['Id_ori'])?$_POST['Id_ori']:'');
        $MateriasCicloAlumno->setTipo($_POST['Tipo_mat']);
        $MateriasCicloAlumno->setActivo(1);
        $MateriasCicloAlumno->setId_alum($_POST['Id_alum']);
        $id_mat_ciclo=$DaoMateriasCicloAlumno->add($MateriasCicloAlumno);
  
        if($_POST['Tipo_mat']==2){
            $ciclosAlumno=$DaoCiclosAlumno->show($_POST['Id_ciclo_alum']);
            $ciclo=$DaoCiclos->show($ciclosAlumno->getId_ciclo());

            //Se generan los pagos apartir de la fecha de inicio del ciclo
             $fecha_pago=$ciclo->getFecha_ini();
             $anio=date("Y",strtotime($ciclo->getFecha_ini()));
             $mes=date("m",strtotime($ciclo->getFecha_ini()));
 
             $mat_esp = $DaoMateriasEspecialidad->show($_POST['Id_mat_esp']);
             $mat = $DaoMaterias->show($mat_esp->getId_mat());
             $mensualidad=$mat->getPrecio();
 
             $pagos=array();
            //Insertamos las mensualidades
            for($i=1;$i<=$esp->getNum_pagos();$i++){
                $d=1;
                if(isset($params['DiaInicioMensualidades']) && strlen($params['DiaInicioMensualidades'])>0){
                   $d=$params['DiaInicioMensualidades'];
                }
                $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes + $i, $d, $anio));
                
                $PagosCiclo= new PagosCiclo();
                $PagosCiclo->setConcepto("Mat. Extra (".$mat->getNombre().")");
                $PagosCiclo->setFecha_pago($base->get_day_pago($fecha_pago,$ciclo->getId()));
                $PagosCiclo->setMensualidad($mensualidad);
                $PagosCiclo->setId_ciclo_alum($_POST['Id_ciclo_alum']);
                $PagosCiclo->setId_alum($_POST['Id_alum']);
                $PagosCiclo->setTipo_pago("pago");
                $PagosCiclo->setIdRel($_POST['Id_alum']);
                $PagosCiclo->setTipoRel('alum');
                $id_pago=$DaoPagosCiclo->add($PagosCiclo);
                array_push($pagos, $id_pago);
            } 
            
            $Ids=implode(",", $pagos);
            $materiaCiclo=$DaoMateriasCicloAlumno->show($id_mat_ciclo);
            $materiaCiclo->setIds_pagos($Ids);
            $DaoMateriasCicloAlumno->update($materiaCiclo);
        }
        
        //Crear historial
        $DaoUsuarios= new DaoUsuarios();
        $DaoAlumnos= new DaoAlumnos();
        $DaoCiclos= new DaoCiclos();
        $DaoCiclosAlumno= new DaoCiclosAlumno();
        $DaoMaterias= new DaoMaterias();
        $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
        
        $mat_esp=$DaoMateriasEspecialidad->show($_POST['Id_mat_esp']);
        $mat=$DaoMaterias->show($mat_esp->getId_mat());

        $alum=$DaoAlumnos->show($_POST['Id_alum']);
        $cicloAlumno=$DaoCiclosAlumno->show($_POST['Id_ciclo_alum']);
        $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());

        $TextoHistorial="Añade materia ".$mat->getNombre()."  para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ciclo ".$ciclo->getClave();
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Asignar materias");
  
         update_list_materias($_POST['Id_oferta_alumno']);
     }
}

if($_POST['action']=="validarPrerrequisitos"){
    $resp=array();  
    $DaoMaterias= new DaoMaterias();
    $DaoPrerrequisitos= new DaoPrerrequisitos();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoEspecialidades= new DaoEspecialidades();
    
    $pre=$DaoPrerrequisitos->getLatsPrerrequisito($_POST['Id_mat_esp']);
    if($pre->getId()>0){
       $mat_esp = $DaoMateriasEspecialidad->show($pre->getId_mat_esp_pre());
       $mat=$DaoMaterias->show($mat_esp->getId_mat());
       $esp=$DaoEspecialidades->show($mat_esp->getId_esp());
       
       $paso=0;
       foreach($DaoMateriasCicloAlumno->getMaterias($_POST['Id_mat_esp']) as $materiasCiclo){
            if(is_numeric($materiasCiclo->getCalTotalParciales())){
               $CalTotalParciales=$materiasCiclo->getCalTotalParciales();
            }else{
               $CalTotalParciales=mb_strtolower ($materiasCiclo->getCalTotalParciales(),"UTF-8");
            }
            if ($CalTotalParciales >= $mat->getPromedio_min() || $CalTotalParciales == "cp" || $CalTotalParciales == "ac" || $materiasCiclo->getCalExtraordinario() >= $mat->getPromedio_min() || $materiasCiclo->getCalEspecial() >= $mat->getPromedio_min()) {
                $paso=1;
            }
       }                               
       $resp['existe']=$paso; 
       $resp['nombre_mat']=$mat->getClave_mat()."-".$mat->getNombre(); 
    }else{
       $resp['existe']=1; 
    } 
    echo json_encode($resp);
}

                                     
if($_POST['action']=="mostrarBoxAsignarMateriaCurso"){
   $DaoOfertas= new DaoOfertas();
   $DaoCiclosAlumno= new DaoCiclosAlumno();
?>
<div class="box-emergente-form box-materias">
	<h1>Asignar Materia</h1>
        <p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_grados_ofe()">
              <option value="0"></option>
            </select>
        </p>
        <p>Grado:<br>
            <select id="grado" onchange="update_materias()">
              <option value="0"></option>
            </select>
        </p>
   
        </p>
        <p>Materia:<br>
            <select id="lista_materias" onchange="verificar_orientaciones(this)"></select>
        </p>
        <div id="box_orientaciones"></div>
        <p>Tipo de materia:<select id="tipo_materia">
                <option value="0"></option>
                <option value="1">Materia curricular</option>
                <option value="2">Materia Extra</option>
            </select></p>
        <p><button onclick="validarPrerrequisitosMateriaCurso(<?php echo $_POST['Id_ciclo_alum']?>)">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
</div>
<?php	
}



if($_POST['action']=="save_materia_curso"){
  
    $queryx="";
    if(isset($_POST['Id_ori']) && $_POST['Id_ori']>0){
       $queryx=" AND Id_ori=". $_POST['Id_ori'];
    }
    $base= new base();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoCiclos= new DaoCiclos();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoMaterias= new DaoMaterias();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $DaoCursosEspecialidad= new DaoCursosEspecialidad();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    
    $ofertaAlumno= $DaoOfertasAlumno->show($_POST['Id_oferta_alumno']);
    $esp=$DaoEspecialidades->show($ofertaAlumno->getId_esp());
    
    $MateriasCicloAlumno= new MateriasCicloAlumno();
    $MateriasCicloAlumno->setId_ciclo_alum($_POST['Id_ciclo_alum']);
    $MateriasCicloAlumno->setId_mat_esp($_POST['Id_mat_esp']);
    $MateriasCicloAlumno->setId_ori($_POST['Id_ori']);
    $MateriasCicloAlumno->setTipo($_POST['Tipo_mat']);
    $MateriasCicloAlumno->setActivo(1);
    $MateriasCicloAlumno->setId_alum($_POST['Id_alum']);
    $id_mat_ciclo=$DaoMateriasCicloAlumno->add($MateriasCicloAlumno);

    if($_POST['Tipo_mat']==2){
        $ciclosAlumno=$DaoCiclosAlumno->show($_POST['Id_ciclo_alum']);
        $mat_esp = $DaoMateriasEspecialidad->show($_POST['Id_mat_esp']);
        $mat = $DaoMaterias->show($mat_esp->getId_mat());
        //El costo de lamateria no se divide entre el numero de mensualidades, 
        //Ya que el costo de la materia es mensual
        $mensualidad=$mat->getPrecio();
        
        $fecha_pago=$DaoCursosEspecialidad->getFechaPago($ofertaAlumno->getId_curso_esp());
        $anio = date("Y", strtotime($fecha_pago));
        $mes = date("m", strtotime($fecha_pago));
        $day= date("d", strtotime($fecha_pago));
        
         $pagos=array();
        //Insertamos las mensualidades
        for($i=1;$i<=$esp->getNum_pagos();$i++){
            $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes +$i, $day , $anio));

            $PagosCiclo= new PagosCiclo();
            $PagosCiclo->setConcepto("Mat. Extra (".$mat->getNombre().")");
            $PagosCiclo->setFecha_pago($fecha_pago);
            $PagosCiclo->setMensualidad($mensualidad);
            $PagosCiclo->setId_ciclo_alum($_POST['Id_ciclo_alum']);
            $PagosCiclo->setId_alum($_POST['Id_alum']);
            $PagosCiclo->setTipo_pago("pago");
            $PagosCiclo->setIdRel($_POST['Id_alum']);
            $PagosCiclo->setTipoRel('alum');
            $id_pago=$DaoPagosCiclo->add($PagosCiclo);
            array_push($pagos, $id_pago);
        } 

        $Ids=implode(",", $pagos);
        $materiaCiclo=$DaoMateriasCicloAlumno->show($id_mat_ciclo);
        $materiaCiclo->setIds_pagos($Ids);
        $DaoMateriasCicloAlumno->update($materiaCiclo);
    }

    //Crear historial
    $mat_esp=$DaoMateriasEspecialidad->show($_POST['Id_mat_esp']);
    $mat=$DaoMaterias->show($mat_esp->getId_mat());

    $alum=$DaoAlumnos->show($_POST['Id_alum']);
    $cicloAlumno=$DaoCiclosAlumno->show($_POST['Id_ciclo_alum']);
    $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());

    $TextoHistorial="Añade materia ".$mat->getNombre()."  para el alumno ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ciclo ".$ciclo->getClave();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Asignar materias");

     update_list_materias($_POST['Id_oferta_alumno']);
}


if($_POST['action']=="generar_cobro_curso"){
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoCursosEspecialidad= new DaoCursosEspecialidad();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoEspecialidades= new DaoEspecialidades();
    $base= new base();
         
    $query = "SELECT * FROM Pagos_ciclo WHERE Concepto='Mensualidad' AND Id_ciclo_alum=".$_POST['Id_ciclo_alum']." AND Id_alum=".$_POST['Id_alum']." AND DeadCargo IS NULL";
    $existe=$DaoPagosCiclo->existeQuery($query);
    if($existe->getId()==0 || $existe->getId()==null){

        $mensualidad=0;
        foreach ($DaoMateriasCicloAlumno->getMateriasCicloAlumno($_POST['Id_ciclo_alum']) as $materiaCiclo) {
            if($materiaCiclo->getTipo()==1){
                 $mat_esp = $DaoMateriasEspecialidad->show($materiaCiclo->getId_mat_esp());
                 $mat = $DaoMaterias->show($mat_esp->getId_mat());
                 $mensualidad+=$mat->getPrecio();
            }
        }
        
        $cicloAlumno=$DaoCiclosAlumno->show($_POST['Id_ciclo_alum']);
        $ofertaAlumno= $DaoOfertasAlumno->show($_POST['Id_oferta_alumno']);
        $esp=$DaoEspecialidades->show($ofertaAlumno->getId_esp());
        
        //Se generan los pagos apartir de la fecha especificada para el curso
        $fecha_pago=$DaoCursosEspecialidad->getFechaPago($ofertaAlumno->getId_curso_esp());
        $anio = date("Y", strtotime($fecha_pago));
        $mes = date("m", strtotime($fecha_pago));
        $day= date("d", strtotime($fecha_pago));

        //Insertamos las mensualidades
        for($i=1;$i<=$esp->getNum_pagos();$i++){
            $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes +$i, $day , $anio));
            
            $PagosCiclo= new PagosCiclo();
            $PagosCiclo->setConcepto("Mensualidad");
            $PagosCiclo->setFecha_pago($fecha_pago);
            $PagosCiclo->setMensualidad($mensualidad);
            $PagosCiclo->setId_ciclo_alum($_POST['Id_ciclo_alum']);
            $PagosCiclo->setId_alum($_POST['Id_alum']);
            $PagosCiclo->setTipo_pago("pago");
            $PagosCiclo->setIdRel($_POST['Id_alum']);
            $PagosCiclo->setTipoRel("alum");
            $DaoPagosCiclo->add($PagosCiclo);
        } 
        update_list_materias($_POST['Id_oferta_alumno']);
    }
}