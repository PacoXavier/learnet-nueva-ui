<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="filtro"){
    
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoUsuarios= new DaoUsuarios();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query="";
    if($_POST['Id_usu']>0){
        $query="AND logs_inscripciones_ulm.Id_usu=".$_POST['Id_usu'];
    }
    if(strlen($_POST['Fecha_ini'])>0 && $_POST['Fecha_fin']==null){
        $query=$query." AND Fecha_log>='".$_POST['Fecha_ini']."'";
    }elseif(strlen($_POST['Fecha_fin'])>0 && $_POST['Fecha_ini']==null){
        $query=$query." AND Fecha_log<='".$_POST['Fecha_fin']."'";
    }elseif(strlen($_POST['Fecha_ini'])>0 && strlen($_POST['Fecha_fin'])>0){
        $query=$query." AND (Fecha_log>='".$_POST['Fecha_ini']."' AND Fecha_log<='".$_POST['Fecha_fin']."')";
    }
    
    if(strlen($_POST['Fecha_ini_con'])>0 && $_POST['Fecha_fin_con']==null){
        $query=$query." AND Dia_contactar>='".$_POST['Fecha_ini_con']."'";
    }elseif(strlen($_POST['Fecha_fin_con'])>0 && $_POST['Fecha_ini_con']==null){
        $query=$query." AND Dia_contactar<='".$_POST['Fecha_fin_con']."'";
    }elseif(strlen($_POST['Fecha_ini_con'])>0 && strlen($_POST['Fecha_fin_con'])>0){
        $query=$query." AND (Dia_contactar>='".$_POST['Fecha_ini_con']."' AND Dia_contactar<='".$_POST['Fecha_fin_con']."')";
    }
    
    $count=1;
    $query_inscripciones_ulm  = "SELECT logs_inscripciones_ulm.*,
    usuarios_ulm.Nombre_usu,usuarios_ulm.ApellidoM_usu,usuarios_ulm.ApellidoP_usu,usuarios_ulm.Id_plantel,
    inscripciones_ulm.Nombre_ins,inscripciones_ulm.ApellidoM_ins,inscripciones_ulm.ApellidoP_ins
    FROM logs_inscripciones_ulm
    JOIN usuarios_ulm ON logs_inscripciones_ulm.Id_usu=usuarios_ulm.Id_usu
    JOIN inscripciones_ulm ON logs_inscripciones_ulm.Idrel_log=inscripciones_ulm.Id_ins
    WHERE usuarios_ulm.Id_plantel=".$usu->getId_plantel()." AND Tipo_relLog='inte' ".$query." ORDER BY Fecha_log DESC";
    foreach($base->advanced_query($query_inscripciones_ulm) as $k=>$v){
        ?>
        <tr>
            <td><?php echo $count?> </td>
            <td><?php echo $v['Fecha_log']?> </td>
            <td><?php echo $v['Nombre_usu']." ".$v['ApellidoP_usu']." ".$v['ApellidoM_usu'];?></td>
            <td><?php echo $v['Nombre_ins']." ".$v['ApellidoP_ins']." ".$v['ApellidoM_ins'];?></td>
            <td><?php echo $v['Comen_log'];?></td>
            <td><?php echo $v['Dia_contactar'];?></td>

        </tr>
        <?php
        $count++;
    }
}

if($_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'FECHA');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'USUARIO');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'INTERESADO');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'COMENTARIOS');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'CONTACTAR EL DÍA');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','F') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoUsuarios= new DaoUsuarios();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query="";
    if($_GET['Id_usu']>0){
        $query="AND logs_inscripciones_ulm.Id_usu=".$_GET['Id_usu'];
    }
    if(strlen($_GET['Fecha_ini'])>0 && $_GET['Fecha_fin']==null){
        $query=$query." AND Fecha_log>='".$_GET['Fecha_ini']."'";
    }elseif(strlen($_GET['Fecha_fin'])>0 && $_GET['Fecha_ini']==null){
        $query=$query." AND Fecha_log<='".$_GET['Fecha_fin']."'";
    }elseif(strlen($_GET['Fecha_ini'])>0 && strlen($_POST['Fecha_fin'])>0){
        $query=$query." AND (Fecha_log>='".$_GET['Fecha_ini']."' AND Fecha_log<='".$_GET['Fecha_fin']."')";
    }
    $count=1;
    $query_inscripciones_ulm  = "SELECT logs_inscripciones_ulm.*,
    usuarios_ulm.Nombre_usu,usuarios_ulm.ApellidoM_usu,usuarios_ulm.ApellidoP_usu,usuarios_ulm.Id_plantel,
    inscripciones_ulm.Nombre_ins,inscripciones_ulm.ApellidoM_ins,inscripciones_ulm.ApellidoP_ins
    FROM logs_inscripciones_ulm
    JOIN usuarios_ulm ON logs_inscripciones_ulm.Id_usu=usuarios_ulm.Id_usu
    JOIN inscripciones_ulm ON logs_inscripciones_ulm.Idrel_log=inscripciones_ulm.Id_ins
    WHERE usuarios_ulm.Id_plantel=".$usu->getId_plantel()." AND Tipo_relLog='inte' ".$query." ORDER BY Fecha_log DESC";
    foreach($base->advanced_query($query_inscripciones_ulm) as $k=>$v){
        $count++;
        $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
        $objPHPExcel->getActiveSheet()->setCellValue("B$count", $v['Fecha_log']);
        $objPHPExcel->getActiveSheet()->setCellValue("C$count", $v['Nombre_usu']." ".$v['ApellidoP_usu']." ".$v['ApellidoM_usu']);
        $objPHPExcel->getActiveSheet()->setCellValue("D$count", $v['Nombre_ins']." ".$v['ApellidoP_ins']." ".$v['ApellidoM_ins']);
        $objPHPExcel->getActiveSheet()->setCellValue("E$count", $v['Comen_log']);
        $objPHPExcel->getActiveSheet()->setCellValue("F$count", $v['Dia_contactar']);
    }
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=SeguimientosInteresados-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}



