<?php
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoMediosEnterar.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/PHPExcel.php');
require_once('clases/DaoDocentes.php');

if ($_POST['action'] == "filtro") {
    $base = new base();
    $DaoOfertas = new DaoOfertas();
    $DaoAlumnos = new DaoAlumnos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoGrados = new DaoGrados();
    $DaoMediosEnterar = new DaoMediosEnterar();
    $DaoDocentes = new DaoDocentes();
    $DaoUsuarios = new DaoUsuarios();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    if ($_POST['Tipo'] == "1") {
        $query = "";
        if ($_POST['Id_ofe'] > 0) {
            $query = $query . " AND Id_ofe=" . $_POST['Id_ofe'];
        }
        if ($_POST['Id_esp'] > 0) {
            $query = $query . " AND Id_esp=" . $_POST['Id_esp'];
        }
        if ($_POST['Id_ori'] > 0) {
            $query = $query . " AND Id_ori=" . $_POST['Id_ori'];
        }
        if ($_POST['Opcion_pago'] > 0) {
            $query = $query . " AND Opcion_pago=" . $_POST['Opcion_pago'];
        }
        ?>
        <table class="table">
            <thead>
                <tr>
                    <td>#</td>
                    <td style="width: 80px">Matricula</td>
                    <td>Nombre</td>
                    <td style="width: 150px">Oferta</td>
                    <td style="width: 200px">Carrera</td>
                    <td>Orientaci&oacute;n:</td>
                    <td>Opcion de pago</td>
                    <td>Sexo</td>
                    <td>Fecha nacimiento</td>
                    <td>Edad</td>
                    <td>CURP</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $count = 1;
                $query = "SELECT * FROM inscripciones_ulm 
        JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
        WHERE tipo=1  
              AND inscripciones_ulm.Id_plantel=" . $usu->getId_plantel() . " 
              AND Activo_oferta=1 
              AND Baja_ofe IS NULL 
              AND FechaCapturaEgreso IS NULL
              AND IdCicloEgreso IS NULL
              AND IdUsuEgreso IS NULL " . $query . " 
        ORDER BY Id_ins DESC";
                $consulta = $base->advanced_query($query);
                $row_consulta = $consulta->fetch_assoc();
                $totalRows_consulta = $consulta->num_rows;
                if ($totalRows_consulta > 0) {

                    do {
                        $nombre_ori = "";
                        $oferta = $DaoOfertas->show($row_consulta['Id_ofe']);
                        $esp = $DaoEspecialidades->show($row_consulta['Id_esp']);
                        if ($row_consulta['Id_ori'] > 0) {
                            $ori = $DaoOrientaciones->show($row_consulta['Id_ori']);
                            $nombre_ori = $ori->getNombre();
                        }
                        $opcion = "Plan por materias";
                        if ($row_consulta['Opcion_pago'] == 2) {
                            $opcion = "Plan completo";
                        }

                        if ($row_consulta['FechaNac'] != null) {
                            $edad = $base->calcularEdad($row_consulta['FechaNac']);
                        } else {
                            $edad = $row_consulta['Edad_ins'];
                        }

                        $sexo = "Masculino";
                        if ($row_consulta['Sexo'] == "f") {
                            $sexo = "Femenino";
                        }
                        ?>
                        <tr id_alum="<?php echo $row_consulta['Id_ins']; ?>" id_ofe_alum="<?php echo $row_consulta['Id_ofe_alum']; ?>">
                            <td><?php echo $count; ?></td>
                            <td><?php echo $row_consulta['Matricula'] ?></td>
                            <td style="width: 115px;"><?php echo $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins'] ?></td>
                            <td><?php echo $oferta->getNombre_oferta(); ?></td>
                            <td><?php echo $esp->getNombre_esp(); ?></td>
                            <td><?php echo $nombre_ori; ?></td>
                            <td><?php echo $opcion; ?></td>
                            <td><?php echo $sexo ?></td>
                            <td><?php echo $row_consulta['FechaNac'] ?></td>
                            <td><?php echo $edad ?></td>
                            <td><?php echo $row_consulta['Curp_ins'] ?></td>
                        </tr>

                        <?php
                        $count++;
                    } while ($row_consulta = $consulta->fetch_assoc());
                    ?>
                </tbody>
            </table>
            <?php
        }
    } elseif ($_POST['Tipo'] == "2") {
        ?>
        <table class="table">
            <thead>
                <tr>
                    <td>#</td>
                    <td>Clave</td>
                    <td>Nombre</td>
                    <td>Sexo</td>
                    <td>Fecha nacimiento</td>
                    <td>Edad</td>
                    <td>CURP</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $count = 1;
                foreach ($DaoDocentes->showAll() as $k => $v) {

                    $edad = "";
                    if ($v->getFechaNacimiento() != null) {
                        $edad = $base->calcularEdad($v->getFechaNacimiento());
                    }

                    $sexo = "Masculino";
                    if ($v->getSexo() == "f") {
                        $sexo = "Femenino";
                    }
                    ?>
                    <tr>
                        <td onclick="mostrar(<?php echo $v->getId() ?>)"><?php echo $count; ?></td>
                        <td><?php echo $v->getClave_docen(); ?></td>
                        <td><?php echo $v->getNombre_docen() . " " . $v->getApellidoP_docen() . " " . $v->getApellidoM_docen() ?></td>
                        <td><?php echo $sexo ?></td>
                        <td><?php echo $v->getFechaNacimiento() ?></td>
                        <td><?php echo $edad ?></td>
                        <td><?php echo $v->getCurp() ?></td>
                    </tr>
                    <?php
                    $count++;
                }
                ?>
            </tbody>
        </table>
        <?php
    }
}




if ($_GET['action'] == "download_excel") {
    $base = new base();
    $DaoDocentes = new DaoDocentes();
    $DaoOfertas = new DaoOfertas();
    $DaoAlumnos = new DaoAlumnos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoGrados = new DaoGrados();
    $DaoMediosEnterar = new DaoMediosEnterar();
    $DaoUsuarios = new DaoUsuarios();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $gris = "ACB9CA";

    $objPHPExcel = new PHPExcel();

    if ($_GET['Tipo'] == "1") {
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'MATRICULA');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'OFERTA');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'CARRERA');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'ORIENTACIÓN');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'SEXO');
        $objPHPExcel->getActiveSheet()->setCellValue('H1', 'FECHA NACIMIENTO');
        $objPHPExcel->getActiveSheet()->setCellValue('I1', 'EDAD');
        $objPHPExcel->getActiveSheet()->setCellValue('J1', 'CURP');
                        
        //Altura de la celda
        $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
        //Establece el width automatico de la celda 
        foreach ($base->xrange('A', 'J') as $columnID) {
            //Ancho
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
            //Font
            $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getFont()->setBold(true);
            //Alineacion vertical
            $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            //Borders
            $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
            //Background
            $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
        }

        $query = "";

        if ($_GET['Id_ofe'] > 0) {
            $query = $query . " AND Id_ofe=" . $_GET['Id_ofe'];
        }
        if ($_GET['Id_esp'] > 0) {
            $query = $query . " AND Id_esp=" . $_GET['Id_esp'];
        }
        if ($_GET['Id_ori'] > 0) {
            $query = $query . " AND Id_ori=" . $_GET['Id_ori'];
        }
        if ($_GET['Opcion_pago'] > 0) {
            $query = $query . " AND Opcion_pago=" . $_GET['Opcion_pago'];
        }

        $count = 1;
        $query = "SELECT * FROM inscripciones_ulm 
        JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
        WHERE tipo=1  
              AND inscripciones_ulm.Id_plantel=" . $usu->getId_plantel() . " 
              AND Activo_oferta=1 
              AND Baja_ofe IS NULL 
              AND FechaCapturaEgreso IS NULL
              AND IdCicloEgreso IS NULL
              AND IdUsuEgreso IS NULL " . $query . " 
        ORDER BY Id_ins DESC";
        $consulta = $base->advanced_query($query);
        $row_consulta = $consulta->fetch_assoc();
        $totalRows_consulta = $consulta->num_rows;
        if ($totalRows_consulta > 0) {
            do {
                $nombre_ori = "";
                $oferta = $DaoOfertas->show($row_consulta['Id_ofe']);
                $esp = $DaoEspecialidades->show($row_consulta['Id_esp']);
                if ($row_consulta['Id_ori'] > 0) {
                    $ori = $DaoOrientaciones->show($row_consulta['Id_ori']);
                    $nombre_ori = $ori->getNombre();
                }
                $opcion = "Plan por materias";
                if ($row_consulta['Opcion_pago'] == 2) {
                    $opcion = "Plan completo";
                }

                if ($row_consulta['FechaNac'] != null) {
                    $edad = $base->calcularEdad($row_consulta['FechaNac']);
                } else {
                    $edad = $row_consulta['Edad_ins'];
                }

                $sexo = "Masculino";
                if ($row_consulta['Sexo'] == "f") {
                    $sexo = "Femenino";
                }

                $count++;
                $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
                $objPHPExcel->getActiveSheet()->setCellValue("B$count", $row_consulta['Matricula']);
                $objPHPExcel->getActiveSheet()->setCellValue("C$count", $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins']);
                $objPHPExcel->getActiveSheet()->setCellValue("D$count", $oferta->getNombre_oferta());
                $objPHPExcel->getActiveSheet()->setCellValue("E$count", $esp->getNombre_esp());
                $objPHPExcel->getActiveSheet()->setCellValue("F$count", $nombre_ori);
                $objPHPExcel->getActiveSheet()->setCellValue("G$count", $sexo);
                $objPHPExcel->getActiveSheet()->setCellValue("H$count", $row_consulta['FechaNac']);
                $objPHPExcel->getActiveSheet()->setCellValue("I$count", $edad);
                $objPHPExcel->getActiveSheet()->setCellValue("J$count", $row_consulta['Curp_ins']);
            } while ($row_consulta = $consulta->fetch_assoc());
        }
    } elseif ($_GET['Tipo'] == "2") {
        $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'CLAVE');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'SEXO');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'FECHA NACIMIENTO');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'EDAD');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'CURP');

        //Altura de la celda
        $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
        //Establece el width automatico de la celda 
        foreach ($base->xrange('A', 'G') as $columnID) {
            //Ancho
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
            //Font
            $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getFont()->setBold(true);
            //Alineacion vertical
            $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            //Borders
            $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
            //Background
            $objPHPExcel->getActiveSheet()->getStyle($columnID . "1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
        }
        $count = 1;
        foreach ($DaoDocentes->showAll() as $k => $v) {
            $edad = "";
            if ($v->getFechaNacimiento() != null) {
                $edad = $base->calcularEdad($v->getFechaNacimiento());
            }

            $sexo = "Masculino";
            if ($v->getSexo() == "f") {
                $sexo = "Femenino";
            }
           

            $count++;
            $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
            $objPHPExcel->getActiveSheet()->setCellValue("B$count", $v->getClave_docen());
            $objPHPExcel->getActiveSheet()->setCellValue("C$count", $v->getNombre_docen() . " " . $v->getApellidoP_docen() . " " . $v->getApellidoM_docen());
            $objPHPExcel->getActiveSheet()->setCellValue("D$count", $sexo);
            $objPHPExcel->getActiveSheet()->setCellValue("E$count", $v->getFechaNacimiento());
            $objPHPExcel->getActiveSheet()->setCellValue("F$count", $edad);
            $objPHPExcel->getActiveSheet()->setCellValue("G$count", $v->getCurp());
        }
    }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=DatosSeguro-" . date('Y-m-d_H:i:s') . ".xlsx");
    echo $objWriter->save('php://output');
}




