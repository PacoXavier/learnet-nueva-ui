<?php
require_once('activate_error.php');
require_once('../clases/DaoOfertas.php');
require_once('../clases/DaoUsuarios.php');
require_once('../clases/modelos/Ofertas.php');

if (isset($_POST['action']) && $_POST['action'] == "box_update_oferta") {
    $nombre = "";
    $id = 0;
    $getTipo_recargo = "";
    $getValor = "";
    $tipoOferta = 0;
    $monto="";
    if (isset($_POST['Id_oferta']) && $_POST['Id_oferta'] > 0) {
        $DaoOfertas = new DaoOfertas();
        $oferta = $DaoOfertas->show($_POST['Id_oferta']);
        $nombre = $oferta->getNombre_oferta();
        $id = $oferta->getId();
        $getTipo_recargo = $oferta->getTipo_recargo();
        $getValor = $oferta->getValor();
        $tipoOferta = $oferta->getTipoOferta();
        $monto=$oferta->getMontoCargoMateriaAcreditada();
    }
    ?>
    <div class="box-emergente-form">
        <h1>Nivel acad&eacute;mico</h1>
        <p>Nombre del nivel<br><input type="text"  id="nombre" value="<?php echo $nombre ?>"/></p>
        <p>Tipo de plan<br>
            <select id="tipo-plan">
                <option value="0"></option>
                <option value="1" <?php if ($tipoOferta == "1") { ?> selected="selected" <?php } ?>>Por ciclos</option>
                <option value="2" <?php if ($tipoOferta == "2") { ?> selected="selected" <?php } ?>>Sin ciclos</option>
            </select>   </p>
        <p>Monto del cargo por materia acreditada<br><input type="text"  id="montoCargoMateriaAcreditada" value="<?php echo $monto ?>"/></p>
        <p>Tipo de recargo<br>
            <select id="tipo-recargo">
                <option></option>
                <option value="1" <?php if ($getTipo_recargo == "cargofijo") { ?> selected="selected" <?php } ?>>Cargo fijo</option>
                <option value="2" <?php if ($getTipo_recargo == "cargoporcentual") { ?> selected="selected" <?php } ?>>Porcentual</option>
            </select>   </p>
        <p>Monto de recargo<br><input type="number"  id="valor" value="<?php echo $getValor ?>"/></p>
        <button onclick="save_oferta(<?php echo $id ?>)">Guardar</button>
        <button onclick="ocultar_error_layer()">Cerrar</button>
    </div>
    <?php
}

if (isset($_POST['action']) && $_POST['action'] == "save_oferta") {
    $DaoOfertas = new DaoOfertas();
    $DaoUsuarios = new DaoUsuarios();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    if (isset($_POST['Id_oferta']) && $_POST['Id_oferta'] > 0) {
        $oferta = $DaoOfertas->show($_POST['Id_oferta']);
        $oferta->setNombre_oferta($_POST['nombre']);
        $oferta->setTipo_recargo($_POST['tipo_recargo']);
        $oferta->setValor($_POST['valor']);
        $oferta->setTipoOferta($_POST['tipo_plan']);
        $oferta->setMontoCargoMateriaAcreditada($_POST['montoCargoMateriaAcreditada']);
        $DaoOfertas->update($oferta);
    } else {
        $Ofertas = new Ofertas();
        $Ofertas->setNombre_oferta($_POST['nombre']);
        $Ofertas->setId_plantel($usu->getId_plantel());
        $Ofertas->setTipo_recargo($_POST['tipo_recargo']);
        $Ofertas->setValor($_POST['valor']);
        $Ofertas->setDateCreated(date('Y-m-d H:i:s'));
        $Ofertas->setTipoOferta($_POST['tipo_plan']);
        $Ofertas->setMontoCargoMateriaAcreditada($_POST['montoCargoMateriaAcreditada']);
        $Ofertas->setActivo_oferta(1);
        $DaoOfertas->add($Ofertas);
    }
    getOfertas();
}

if (isset($_POST['action']) && $_POST['action'] == "delete_ofe") {
    $DaoOfertas = new DaoOfertas();
    $DaoOfertas->delete($_POST['Id_ofe']);
    getOfertas();
}

function getOfertas() {
    $DaoOfertas = new DaoOfertas();
    $count = 1;
    foreach ($DaoOfertas->showAll() as $oferta) {
        $tipo = "Sin ciclos";
        if ($oferta->getTipoOferta() == 1) {
            $tipo = "Por ciclos";
        }
        $cargoAcre="";
        if(strlen($oferta->getMontoCargoMateriaAcreditada())>0){
            $cargoAcre="$".number_format($oferta->getMontoCargoMateriaAcreditada(),2);
        }
        ?>
        <tr>
            <td><?php echo $count ?></td>
            <td><?php echo $oferta->getNombre_oferta() ?></td>
            <td class="td-center"><?php echo $oferta->getDateCreated() ?></td>
            <td class="td-center"><?php echo $tipo ?></td>
            <td class="td-center"><?php echo $oferta->getTipo_recargo() ?></td>
            <td class="td-center"><?php echo $oferta->getValor() ?></td>
            <td class="td-center"><?php echo $cargoAcre ?></td>
            <td class="td-center">Activo</td>
            <td class="td-center">
                <button onclick="mostrar(<?php echo $oferta->getId() ?>)">Especialidades</button>
                <button onclick="box_update_oferta(<?php echo $oferta->getId() ?>)">Editar</button>
                <button onclick="delete_ofe(<?php echo $oferta->getId() ?>)">Eliminar</button>
            </td>
        </tr>
        <?php
        $count++;
    }
}
