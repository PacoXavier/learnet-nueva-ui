<?php
require_once('activate_error.php');
require_once('../require_daos.php');

if ($_POST['action'] == "buscarUsu") {
    $base = new base();
    $DaoUsuarios = new DaoUsuarios();
    $DaoPlanteles = new DaoPlanteles();
    $DaoTiposUsuarios = new DaoTiposUsuarios();
    foreach ($DaoUsuarios->buscarUsuario($_POST['buscar']) as $u) {
        $plantel = $DaoPlanteles->show($u->getId_plantel());
        $tipoUsu = $DaoTiposUsuarios->show($u->getTipo_usu());
        ?>
        <tr>
            <td onclick="mostrar(<?php echo $u->getId() ?>)"><?php echo $u->getNombre_usu() . " " . $u->getApellidoP_usu() . " " . $u->getApellidoM_usu() ?></td>
            <td><?php echo $u->getEmail_usu() ?></td>
            <td><?php echo $plantel->getNombre_plantel() ?></td>
            <td><?php
                if (strlen($u->getLast_session() > 0)) {
                    echo $base->formatFecha_hora($u->getLast_session());
                }
                ?>
            </td>
            <td><?php echo $tipoUsu->getNombre_tipo() ?></td>
            <td>
                <button onclick="delete_usu(<?php echo $u->getId() ?>)">Eliminar</button>
            </td>
        </tr>
        <?php
    }
}

if ($_POST['action'] == "delete_usu") {
    $base = new base();
    $DaoUsuarios = new DaoUsuarios();
    $DaoPlanteles = new DaoPlanteles();
    $DaoTiposUsuarios = new DaoTiposUsuarios();

    $usuarios = $DaoUsuarios->show($_POST['Id_usu']);
    $usuarios->setBaja_usu(date('Y-m-d H:i:s'));
    $DaoUsuarios->update($usuarios);

    //Crear historial
    $_usu = $DaoUsuarios->show($_POST['Id_usu']);
    $TextoHistorial = "Elimina al usuario " . $_usu->getNombre_usu() . " " . $_usu->getApellidoP_usu() . " " . $_usu->getApellidoP_usu();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Usuarios");

    foreach ($DaoUsuarios->showAll() as $u) {
        $plantel = $DaoPlanteles->show($u->getId_plantel());
        $tipoUsu = $DaoTiposUsuarios->show($u->getTipo_usu());
        ?>
        <tr>
            <td onclick="mostrar(<?php echo $u->getId() ?>)"><?php echo $u->getNombre_usu() . " " . $u->getApellidoP_usu() . " " . $u->getApellidoM_usu() ?></td>
            <td><?php echo $u->getEmail_usu() ?></td>
            <td><?php echo $plantel->getNombre_plantel() ?></td>
            <td><?php
        if (strlen($u->getLast_session() > 0)) {
            echo $base->formatFecha_hora($u->getLast_session());
        }
        ?>
            </td>
            <td><?php echo $tipoUsu->getNombre_tipo() ?></td>
            <td>
                <button onclick="delete_usu(<?php echo $u->getId() ?>)">Eliminar</button>
            </td>
        </tr>
        <?php
    }
}