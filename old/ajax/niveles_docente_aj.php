<?php
require_once('activate_error.php');
require_once('../require_daos.php');

if ($_POST['action'] == "mostrarCategoria") {
    $DaoOfertas = new DaoOfertas();
    $DaoCategoriasPago = new DaoCategoriasPago();
    $DaoOfertasPorCategoria= new DaoOfertasPorCategoria();
    
    $nombre = "";
    $id_cat = 0;
    if (isset($_POST['Id_cat']) && $_POST['Id_cat'] > 0) {
        $categoria = $DaoCategoriasPago->show($_POST['Id_cat']);
        $nombre = $categoria->getNombre_tipo();
        $id_cat = $categoria->getId();
    }
    ?>
    <div class="box-emergente-form">
        <h1>Categoría</h1>
        <p>Nombre<br><input type="text" value="<?php echo $nombre ?>" id="nombre-cat"/></p>
        <ul class="lista-ofertas" id="lista-ofertas-modal">
            <?php
            foreach ($DaoOfertas->showAll() as $ofe) {
                $checked="";
                $existe=$DaoOfertasPorCategoria->getOfertaCategoria($id_cat,$ofe->getId());
                if($existe->getId()>0){
                   $checked="checked"; 
                } 
                ?>
                <li><input type="checkbox" value="<?php echo $ofe->getId(); ?>" <?php echo $checked;?>/> <?php echo $ofe->getNombre_oferta(); ?></li>
                <?php
            }
            ?>
        </ul>
        <p>
            <button onclick="saveCategoria(<?php echo $id_cat ?>)">Guardar</button>
            <button onclick="ocultar_error_layer()">Cancelar</button>
        </p>
    </div>
    <?php
}

if($_POST['action']=="saveCategoria"){
    $DaoCategoriasPago= new DaoCategoriasPago();
    $DaoUsuarios = new DaoUsuarios();
    $_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    if($_POST['Id_cat']>0){
        $CategoriasPago=$DaoCategoriasPago->show($_POST['Id_cat']);
        $CategoriasPago->setNombre_tipo($_POST['nombre']);
        $DaoCategoriasPago->update($CategoriasPago);
        $Id_cat=$_POST['Id_cat'];
    }else{
        $CategoriasPago = new CategoriasPago();
        $CategoriasPago->setNombre_tipo($_POST['nombre']);
        $CategoriasPago->setActivo(1);
        $CategoriasPago->setId_plantel($_usu->getId_plantel());
        $Id_cat=$DaoCategoriasPago->add($CategoriasPago);   
        
    }

    $DaoOfertasPorCategoria= new DaoOfertasPorCategoria();    
    $DaoOfertasPorCategoria->deleteByCategoria($Id_cat);
    foreach($_POST['ofertas'] as $o){
        $OfertasPorCategoria= new OfertasPorCategoria();
        $OfertasPorCategoria->setId_ofe($o);
        $OfertasPorCategoria->setId_tipo($Id_cat);
        $DaoOfertasPorCategoria->add($OfertasPorCategoria);
    }

}

if($_POST['action']=="eliminarCategoria"){
    $DaoCategoriasPago= new DaoCategoriasPago();
    $DaoCategoriasPago->delete($_POST['Id_cat']);
}


if($_POST['action']=="eliminarNivel"){
    $DaoNivelesPago= new DaoNivelesPago();
    $DaoNivelesPago->delete($_POST['Id_nivel']);
}


if($_POST['action']=="updatePage"){
   updatePage();
}

if ($_POST['action'] == "mostrarNivel") {
    $DaoOfertas = new DaoOfertas();
    $DaoCategoriasPago = new DaoCategoriasPago();
    $DaoNivelesPago= new DaoNivelesPago();

    $id_nivel = 0;
    $id_cat=0;
    $nombre = "";
    $puntosMin="";
    $puntosMax="";
    $pagoHora="";
    $checked="";
    if (isset($_POST['Id_nivel']) && $_POST['Id_nivel'] > 0) {
        $nivel = $DaoNivelesPago->show($_POST['Id_nivel']);
        $nombre = $nivel->getNombre_nivel();
        $puntosMin=$nivel->getPuntosMin();
        $puntosMax=$nivel->getPuntosMax();
        $pagoHora=$nivel->getPagoNivel();
        $id_nivel = $nivel->getId();
        $id_cat=$nivel->getId_tipo_nivel();
        $checked='disabled="disabled"';
    }
    ?>
    <div class="box-emergente-form">
        <h1>Nivel</h1>
        <p>Categoría<br>
            <select id="select-categorias" <?php echo $checked?>>
                <option value="0"></option>
            <?php
             foreach($DaoCategoriasPago->getCategoriasPago() as $cat){
               ?>
                <option value="<?php echo $cat->getId();?>" <?php if($id_cat==$cat->getId()){ ?> selected="selected" <?php } ?>><?php echo $cat->getNombre_tipo();?></option>
                <?php
             }
            ?>
            </select>
        </p>
        <p>Nombre<br><input type="text" value="<?php echo $nombre ?>" id="nombre-niv"/></p>
        <p>Puntaje minimo<br><input type="text" value="<?php echo $puntosMin ?>" id="puntos-min"/></p>
        <p>Puntaje maximo<br><input type="text" value="<?php echo $puntosMax ?>" id="puntos-max"/></p>
        <p>Pago por hora<br><input type="text" value="<?php echo $pagoHora ?>" id="pago-hora"/></p>
        <p>
            <button onclick="saveNivel(<?php echo $id_nivel ?>)">Guardar</button>
            <button onclick="ocultar_error_layer()">Cancelar</button>
        </p>
    </div>
    <?php
}




function updatePage() {
    $DaoOfertas = new DaoOfertas();
    $DaoCategoriasPago = new DaoCategoriasPago();
    $DaoNivelesPago = new DaoNivelesPago();
    $DaoOfertasPorCategoria = new DaoOfertasPorCategoria();
    $DaoUsuarios = new DaoUsuarios();
    $_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-table" aria-hidden="true"></i> Tabulador de pago</h1>
                </div>
                <?php
                foreach ($DaoCategoriasPago->getCategoriasPago() as $cat) {
                    ?>
                    <div class="seccion" id_tipo="<?php echo $cat->getId() ?>">
                        <h2>Categoría</h2>
                        <p style="margin-bottom: 20px;">Nombre categoría<br>
                            <input type="text" value="<?php echo $cat->getNombre_tipo() ?>" class="nombre-cat" disabled="disabled"/>
                            <i class="fa fa-pencil-square-o" aria-hidden="true" title="Editar" onclick="mostrarCategoria(<?php echo $cat->getId() ?>)"></i>
                            <i class="fa fa-trash" aria-hidden="true" title="Eliminar" onclick="eliminarCategoria(<?php echo $cat->getId() ?>)"></i>
                        </p>
                        <table id="main-niveles">
                            <tbody>
                                <tr>
                                    <td>
                                        <h2>Niveles de la categoría</h2>
                                        <?php
                                        foreach ($DaoNivelesPago->getNivelesCategoria($cat->getId()) as $nivel) {
                                            ?>
                                            <ul class="form" id_nivel="<?php echo $nivel->getId() ?>">
                                                <li>Nombre nivel<br><input type="text" value="<?php echo $nivel->getNombre_nivel(); ?>" class="nombre" disabled="disabled"/></li>
                                                <li>Puntaje M&iacute;nimo<br><input type="text" value="<?php echo $nivel->getPuntosMin(); ?>" class="puntaje_profesor" disabled="disabled"/></li>
                                                <li>Puntaje M&aacute;ximo<br><input type="text" value="<?php echo $nivel->getPuntosMax(); ?>" class="puntaje_maximo" disabled="disabled"/></li>
                                                <li>Pago Hora<br><input type="text" value="<?php echo $nivel->getPagoNivel() ?>" class="pagoHora_profesor" disabled="disabled"/></li>
                                                <li>
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true" title="Editar" onclick="mostrarNivel(<?php echo $nivel->getId() ?>)"></i>
                                                    <i class="fa fa-trash" aria-hidden="true" title="Eliminar" onclick="eliminarNivel(<?php echo $nivel->getId() ?>)"></i>
                                                </li>
                                                <br>
                                                <!--
                                                <li style="display:none">
                                                    <button class="btns btns-primary btns-sm">Guardar</button>
                                                    <button class="btns btns-default btns-sm">Cancelar</button>
                                                </li>
                                                -->
                                            </ul>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <h2>Ofertas de la categoría</h2>
                                        <ul class="lista-ofertas">
                                            <?php
                                            foreach ($DaoOfertas->showAll() as $ofe) {
                                                $checked="";
                                                $existe=$DaoOfertasPorCategoria->getOfertaCategoria($cat->getId(),$ofe->getId());
                                                if($existe->getId()>0){
                                                   $checked="checked"; 
                                                } 
                                                ?>
                                                <li><input type="checkbox" disabled="disabled" value="<?php echo $ofe->getId(); ?>" <?php echo $checked;?>/> <?php echo $ofe->getNombre_oferta(); ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once '../estandares/menu_derecho.php';
                ?>
                <ul> 
                    <li class="link" onclick="mostrarCategoria(0)">Agregar categoría</li>
                    <li class="link" onclick="mostrarNivel(0)">Agregar nivel</li>
                </ul>
            </div> 
        </td>
    </tr>
    <?php
}


if($_POST['action']=="saveNivel"){
    $DaoNivelesPago= new DaoNivelesPago();
    if($_POST['Id_nivel']>0){
       $nivel=$DaoNivelesPago->show($_POST['Id_nivel']);
       $nivel->setNombre_nivel($_POST['nombre']);
       $nivel->setPuntosMin($_POST['puntosMin']);
       $nivel->setPuntosMax($_POST['puntoMax']);
       $nivel->setPagoNivel($_POST['pagoHora']);
       $DaoNivelesPago->update($nivel);
    }else{
       $nivel= new NivelesPago();
       $nivel->setNombre_nivel($_POST['nombre']);
       $nivel->setPuntosMin($_POST['puntosMin']);
       $nivel->setPuntosMax($_POST['puntoMax']);
       $nivel->setId_tipo_nivel($_POST['id_cat']);
       $nivel->setPagoNivel($_POST['pagoHora']);
       $nivel->setActivo(1);
       $DaoNivelesPago->add($nivel);
    }
}