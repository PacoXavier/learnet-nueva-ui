<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="filtro"){
    $base = new base();
    $DaoOfertas = new DaoOfertas();
    $DaoAlumnos = new DaoAlumnos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoGrados = new DaoGrados();
    $DaoMediosEnterar = new DaoMediosEnterar();
    $DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
    $DaoMaterias = new DaoMaterias();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoCiclosAlumno= new DaoCiclosAlumno();

    $DaoCiclos= new DaoCiclos();
    $cicloActual=$DaoCiclos->getActual();

    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $query="";

    if($_POST['Id_ofe']>0){
        $query=$query." AND ofertas_alumno.Id_ofe=".$_POST['Id_ofe'];
    }
    if($_POST['Id_esp']>0){
        $query=$query." AND ofertas_alumno.Id_esp=".$_POST['Id_esp'];
    }
    if($_POST['Id_ori']>0){
        $query=$query." AND ofertas_alumno.Id_ori=".$_POST['Id_ori'];
    }
    if($_POST['Opcion_pago']>0){
        $query=$query." AND ofertas_alumno.Opcion_pago=".$_POST['Opcion_pago'];
    }

    if ($cicloActual->getId() > 0) {
        $query = "
                SELECT 
                        materias_ciclo_ulm.*,count(*) AS NumMaterias,
                        ciclos_alum_ulm.Id_ciclo ,
                        Materias.Id_mat,Materias.Nombre,Materias.Promedio_min,
                        ofertas_alumno.*,
                        inscripciones_ulm.Id_ins,inscripciones_ulm.Matricula,inscripciones_ulm.Nombre_ins,inscripciones_ulm.ApellidoP_ins,inscripciones_ulm.ApellidoM_ins,ofertas_alumno.Opcion_pago
                FROM materias_ciclo_ulm 
                JOIN Materias_especialidades on materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                JOIN ciclos_alum_ulm ON materias_ciclo_ulm.Id_ciclo_alum=ciclos_alum_ulm.Id_ciclo_alum
                JOIN ofertas_alumno ON ciclos_alum_ulm.Id_ofe_alum=ofertas_alumno.Id_ofe_alum
                JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
                JOIN (
                      SELECT * FROM materias_uml 
                           WHERE Activo_mat=1 AND Id_plantel=".$usu->getId_plantel()."
                     ) AS Materias ON Materias.Id_mat=Materias_especialidades.Id_mat

                GROUP BY materias_ciclo_ulm.Id_mat_esp,materias_ciclo_ulm.Id_alum
                HAVING NumMaterias>=3  
                       AND materias_ciclo_ulm.Activo=1
                       AND ciclos_alum_ulm.Id_ciclo!=".$cicloActual->getId()."
                       AND ofertas_alumno.Activo_oferta=1 ".$query;
        foreach ($base->advanced_query($query) as $v) {
            $nombre_ori = "";
            $oferta = $DaoOfertas->show($v['Id_ofe']);
            $esp = $DaoEspecialidades->show($v['Id_esp']);
            $mat=$DaoMaterias->show($v['Id_mat']);
            if ($v['Id_ori'] > 0) {
                $ori = $DaoOrientaciones->show($v['Id_ori']);
                $nombre_ori = $ori->getNombre();
            }
            $opcion = "Plan por materias";
            if ($v['Opcion_pago'] == 2) {
                $opcion = "Plan completo";
            }
            $grado = $DaoOfertasAlumno->getLastGradoOferta($v['Id_ofe_alum']);
            $Grado = $DaoGrados->show($grado['Id_grado_ofe']);

            $ban = 0;
            $query="SELECT * FROM materias_ciclo_ulm WHERE Id_mat_esp=".$v['Id_mat_esp']." AND Id_alum=".$v['Id_ins'];
            foreach ($base->advanced_query($query) as $materiaAlumno) {
                $cicloAlu=$DaoCiclosAlumno->show($materiaAlumno['Id_ciclo_alum']);
                if($cicloAlu->getId_ciclo()<$cicloActual->getId()){
                    if ((($materiaAlumno['CalTotalParciales'] < $mat->getPromedio_min()) && strlen($materiaAlumno['CalExtraordinario']) == 0 && strlen($materiaAlumno['CalEspecial']) == 0) ||
                          (($materiaAlumno['CalExtraordinario'] < $mat->getPromedio_min()) && strlen($materiaAlumno['CalTotalParciales']) == 0 && strlen($materiaAlumno['CalEspecial']) == 0) ||
                          (($materiaAlumno['CalEspecial'] < $mat->getPromedio_min()) && strlen($materiaAlumno['CalTotalParciales']) == 0 && strlen($materiaAlumno['CalExtraordinario']) == 0)) {
                      $ban++;
                    }  
                }
            }
            if($ban>=3){
            ?>
            <tr id_alum="<?php echo $v['Id_ins']; ?>">
                <td><?php echo $v['Matricula'] ?></td>
                <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                <td><?php echo $oferta->getNombre_oferta(); ?></td>
                <td><?php echo $esp->getNombre_esp(); ?></td>
                <td><?php echo $nombre_ori; ?></td>
                <td><?php echo $Grado->getGrado(); ?></td>
                <td><?php echo $opcion; ?></td>
                <td><?php echo $mat->getNombre(); ?></td>
                <td><?php echo $ban; ?></td>
                <td><input type="checkbox"> </td>
            </tr>
            <?php
            }
        }
    }
}


if($_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'MATRICULA');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'OFERTA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'CARRERA');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'ORIENTACIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'MATERIA');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'GRADO');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'OPCIÓN DE PAGO');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'REPROBADA');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','J') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }

    $base = new base();
    $DaoOfertas = new DaoOfertas();
    $DaoAlumnos = new DaoAlumnos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoGrados = new DaoGrados();
    $DaoMediosEnterar = new DaoMediosEnterar();
    $DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
    $DaoMaterias = new DaoMaterias();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoCiclosAlumno= new DaoCiclosAlumno();

    $DaoCiclos= new DaoCiclos();
    $cicloActual=$DaoCiclos->getActual();

    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query="";

    if($_GET['Id_alumn']>0){
        $query=$query." AND Id_ins=".$_GET['Id_alumn'];
    }
    if($_GET['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_GET['Id_ofe'];
    }
    if($_GET['Id_esp']>0){
        $query=$query." AND Id_esp=".$_GET['Id_esp'];
    }
    if($_GET['Id_ori']>0){
        $query=$query." AND Id_ori=".$_GET['Id_ori'];
    }
    if($_GET['Opcion_pago']>0){
        $query=$query." AND Opcion_pago=".$_GET['Opcion_pago'];
    }
    $count=1;
    if ($cicloActual->getId() > 0) {
        $query = "
                SELECT 
                        materias_ciclo_ulm.*,count(*) AS NumMaterias,
                        ciclos_alum_ulm.Id_ciclo ,
                        Materias.Id_mat,Materias.Nombre,Materias.Promedio_min,
                        ofertas_alumno.*,
                        inscripciones_ulm.Id_ins,inscripciones_ulm.Matricula,inscripciones_ulm.Nombre_ins,inscripciones_ulm.ApellidoP_ins,inscripciones_ulm.ApellidoM_ins,ofertas_alumno.Opcion_pago
                FROM materias_ciclo_ulm 
                JOIN Materias_especialidades on materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                JOIN ciclos_alum_ulm ON materias_ciclo_ulm.Id_ciclo_alum=ciclos_alum_ulm.Id_ciclo_alum
                JOIN ofertas_alumno ON ciclos_alum_ulm.Id_ofe_alum=ofertas_alumno.Id_ofe_alum
                JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
                JOIN (
                      SELECT * FROM materias_uml 
                           WHERE Activo_mat=1 AND Id_plantel=".$usu->getId_plantel()."
                     ) AS Materias ON Materias.Id_mat=Materias_especialidades.Id_mat

                GROUP BY materias_ciclo_ulm.Id_mat_esp,materias_ciclo_ulm.Id_alum
                HAVING NumMaterias>=3  
                       AND materias_ciclo_ulm.Activo=1
                       AND ciclos_alum_ulm.Id_ciclo!=".$cicloActual->getId()."
                       AND ofertas_alumno.Activo_oferta=1 ".$query;
        foreach ($base->advanced_query($query) as $v) {
            $nombre_ori = "";
            $oferta = $DaoOfertas->show($v['Id_ofe']);
            $esp = $DaoEspecialidades->show($v['Id_esp']);
            $mat=$DaoMaterias->show($v['Id_mat']);
            if ($v['Id_ori'] > 0) {
                $ori = $DaoOrientaciones->show($v['Id_ori']);
                $nombre_ori = $ori->getNombre();
            }
            $opcion = "Plan por materias";
            if ($v['Opcion_pago'] == 2) {
                $opcion = "Plan completo";
            }
            $grado = $DaoOfertasAlumno->getLastGradoOferta($v['Id_ofe_alum']);
            $Grado = $DaoGrados->show($grado['Id_grado_ofe']);

            $ban = 0;
            $query="SELECT * FROM materias_ciclo_ulm WHERE Id_mat_esp=".$v['Id_mat_esp']." AND Id_alum=".$v['Id_ins'];
            foreach ($base->advanced_query($query) as $materiaAlumno) {
                $cicloAlu=$DaoCiclosAlumno->show($materiaAlumno['Id_ciclo_alum']);
                if($cicloAlu->getId_ciclo()<$cicloActual->getId()){
                    if ((($materiaAlumno['CalTotalParciales'] < $mat->getPromedio_min()) && strlen($materiaAlumno['CalExtraordinario']) == 0 && strlen($materiaAlumno['CalEspecial']) == 0) ||
                          (($materiaAlumno['CalExtraordinario'] < $mat->getPromedio_min()) && strlen($materiaAlumno['CalTotalParciales']) == 0 && strlen($materiaAlumno['CalEspecial']) == 0) ||
                          (($materiaAlumno['CalEspecial'] < $mat->getPromedio_min()) && strlen($materiaAlumno['CalTotalParciales']) == 0 && strlen($materiaAlumno['CalExtraordinario']) == 0)) {
                      $ban++;
                    }  
                }
            }
            if($ban>=3){
                $count++;
                $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
                $objPHPExcel->getActiveSheet()->setCellValue("B$count", $v['Matricula']);
                $objPHPExcel->getActiveSheet()->setCellValue("C$count", $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins']);
                $objPHPExcel->getActiveSheet()->setCellValue("D$count", $oferta->getNombre_oferta());
                $objPHPExcel->getActiveSheet()->setCellValue("E$count", $esp->getNombre_esp());
                $objPHPExcel->getActiveSheet()->setCellValue("F$count", $nombre_ori);
                $objPHPExcel->getActiveSheet()->setCellValue("G$count", $mat->getNombre());
                $objPHPExcel->getActiveSheet()->setCellValue("H$count", $Grado->getGrado());
                $objPHPExcel->getActiveSheet()->setCellValue("I$count", $opcion);
                $objPHPExcel->getActiveSheet()->setCellValue("J$count", $ban);
            }
        }
    }

    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=AlumnosParaBaja-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}
