<?php
require_once('activate_error.php');
require_once('../require_daos.php');
require_once('../barcodegen/class/BCGFontFile.php');
require_once('../barcodegen/class/BCGColor.php');
require_once('../barcodegen/class/BCGDrawing.php');
require_once('../barcodegen/class/BCGcode39.barcode.php');


if($_POST['action']=="buscarActivo"){
        $base= new base();
        $DaoActivos = new DaoActivos();
        $DaoUsuarios= new DaoUsuarios();
        $DaoTiposActivo = new DaoTiposActivo();
        $DaoAulas = new DaoAulas();
        $count=1;
        $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
	$query = "SELECT * FROM Activos 
        JOIN aulas ON Activos.Id_aula=aulas.Id_aula
        WHERE (Nombre LIKE '%".$_POST['buscar']."%' OR Codigo LIKE '%".$_POST['buscar']."%' OR Nombre_aula LIKE '%".$_POST['buscar']."%' OR Clave_aula LIKE '%".$_POST['buscar']."%') AND Activos.Id_plantel=".$usu->getId_plantel()." ORDER BY Id_activo DESC LIMIT 200";	
        echo $query;
	foreach($base->advanced_query($query) as $row_BusquedaPro){
                $activo=$DaoActivos->show($row_BusquedaPro['Id_activo']);
                $tipo = $DaoTiposActivo->show($activo->getTipo_act());
                $aula = $DaoAulas->show($activo->getId_aula());

                if ($activo->getDisponible() == 1) {
                    $disponibilidad = "Disponible";
                    $style = 'style="color:green;"';
                } else {
                    $disponibilidad = "No Disponible";
                    $style = 'style="color:red;"';
                }

                $status = "Activo";
                $color = "color:green;";
                if (strlen($activo->getBaja_activo()) > 0) {
                    $status = "Baja";
                    $color = "color:red;";
                }
                ?>
                <tr>
                    <td onclick="mostrar(<?php echo $activo->getId() ?>)"><?php echo $count; ?></td>
                    <td style="text-align: center;" onclick="mostrar(<?php echo $activo->getId() ?>)"><?php echo $activo->getCodigo() ?></td>
                    <td onclick="mostrar(<?php echo $activo->getId() ?>)"><?php echo $activo->getModelo() ?></td>
                    <td onclick="mostrar(<?php echo $activo->getId() ?>)"><?php echo $activo->getNombre() ?></td>
                    <td><?php echo $tipo->getNombre() ?></td>
                    <td><?php echo $activo->getFecha_adq() ?></td>
                    <td style="text-align:center;"><?php echo $aula->getClave_aula(); ?></td>
                    <td <?php echo $style; ?>><?php echo $disponibilidad; ?></td>
                    <td style="text-align:center;<?php echo $color ?>"><?php echo $status; ?></td>
                    <td>
                        <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                        <div class="box-buttom">
                            <button onclick="delete_activo(<?php echo $activo->getId() ?>)">Eliminar</button><br>
                            <button onclick="generar_etiqueta(<?php echo $activo->getId() ?>)">Etiqueta</button><br>
                            <a href="historial_prestamo_activo.php?id=<?php echo $activo->getId(); ?>" target="_blank"><button>Hist. Pres</button></a><br>
                            <a href="historial_activo.php?id=<?php echo $activo->getId(); ?>" target="_blank"><button>Hist. Mant</button></a>
                        </div>
                    </td>
                </tr>
            <?php
	    $count++;  
	}
}


if($_POST['action']=="delete_activo"){
  $DaoActivos = new DaoActivos();
  $activo=$DaoActivos->show($_POST['Id_activo']);
  $DaoUsuarios= new DaoUsuarios();
  $TextoHistorial = "Elimino el activo ".$activo->getNombre(); 
  $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Activos"); 
  
  $activo->setBaja_activo(date('Y-m-d H:i:s'));
  $DaoActivos->update($activo);
  update_page();
}

function update_page(){
    $DaoActivos = new DaoActivos();
    $DaoTiposActivo = new DaoTiposActivo();
    $DaoAulas = new DaoAulas();
    $count=1;
    foreach ($DaoActivos->showAll() as $activo) {
        $tipo = $DaoTiposActivo->show($activo->getTipo_act());
        $aula = $DaoAulas->show($activo->getId_aula());

        if ($activo->getDisponible() == 1) {
            $disponibilidad = "Disponible";
            $style = 'style="color:green;"';
        } else {
            $disponibilidad = "No Disponible";
            $style = 'style="color:red;"';
        }

        $status = "Activo";
        $color = "color:green;";
        if (strlen($activo->getBaja_activo()) > 0) {
            $status = "Baja";
            $color = "color:red;";
        }
        ?>
        <tr>
            <td onclick="mostrar(<?php echo $activo->getId() ?>)"><?php echo $count; ?></td>
            <td style="text-align: center;" onclick="mostrar(<?php echo $activo->getId() ?>)"><?php echo $activo->getCodigo() ?></td>
            <td onclick="mostrar(<?php echo $activo->getId() ?>)"><?php echo $activo->getModelo() ?></td>
            <td onclick="mostrar(<?php echo $activo->getId() ?>)"><?php echo $activo->getNombre() ?></td>
            <td><?php echo $tipo->getNombre() ?></td>
            <td><?php echo $activo->getFecha_adq() ?></td>
            <td style="text-align:center;"><?php echo $aula->getClave_aula(); ?></td>
            <td <?php echo $style; ?>><?php echo $disponibilidad; ?></td>
            <td style="text-align:center;<?php echo $color ?>"><?php echo $status; ?></td>
            <td>
                <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                <div class="box-buttom">
                    <button onclick="delete_activo(<?php echo $activo->getId() ?>)">Eliminar</button><br>
                    <button onclick="generar_etiqueta(<?php echo $activo->getId() ?>)">Etiqueta</button><br>
                    <a href="historial_prestamo_activo.php?id=<?php echo $activo->getId(); ?>" target="_blank"><button>Hist. Pres</button></a><br>
                    <a href="historial_activo.php?id=<?php echo $activo->getId(); ?>" target="_blank"><button>Hist. Mant</button></a>
                </div>
            </td>
        </tr>
        <?php
        $count++;
    }
}


if($_POST['action']=="generar_etiqueta"){
  $DaoActivos = new DaoActivos();
  $activo=$DaoActivos->show($_POST['Id_activo']);
    
    
        //+++++++++++++++++++++++++++++++++++++++++++++++
        //Parte del posterior de la credencial
        //++++++++++++ ++++++++++++++++++++++++++++++++++++

        //Generar imagen del codigo de barras
        //Generar imagen del codigo de barras
        // Loading Font
        $font = new BCGFontFile('../barcodegen/font/Arial.ttf', 9);
        $text = $activo->getCodigo();

        // The arguments are R, G, B for color.
        $color_black = new BCGColor(0, 0, 0);
        $color_white = new BCGColor(255, 255, 255);

        $drawException = null;
        try {
            $code = new BCGcode39();
            $code->setScale(2); // Resolution
            $code->setThickness(12); // Thickness
            $code->setForegroundColor($color_black); // Color of bars
            $code->setBackgroundColor($color_white); // Color of spaces
            $code->setFont($font); // Font (or 0)
            $code->parse($text); // Text
        } catch(Exception $exception) {
            $drawException = $exception;
        }

        //Here is the list of the arguments
        //1 - Filename (empty : display on screen)
        //2 - Background color 
        $drawing = new BCGDrawing('codigoBarras.jpg', $color_white);
        if($drawException) {
            $drawing->drawException($drawException);
        } else {
            $drawing->setBarcode($code);
            $drawing->draw();
        }

        //Header that says it is an image (remove it if you save the barcode to a file)
        //header('Content-Type: image/png');
        //header('Content-Disposition: inline; filename="barcode.png"');
        // Draw (or save) the image into PNG format.
        $drawing->finish(BCGDrawing::IMG_FORMAT_JPEG);

        $destino = imagecreatefromjpeg("../estandares/Activos/Id_frente.jpg"); 
        $origen = imagecreatefromjpeg('codigoBarras.jpg');

        //Colocar color al texto
        $color_texto = imagecolorallocate($destino, 0, 0, 0);

        //ruta del archivo ttf
        $fuente = "../estandares/Arial/arial.ttf"; 

        //Definimos el tamaño de la fuente
        $tamanoFuente=12;

        //Obtenemos el tamanio del codigo de barras
        $picsize=getimagesize('codigoBarras.jpg');

        $source_x  = $picsize[0]*1;
        $source_y  = $picsize[1]*1;

        // Copiar y fusionar
        imagecopymerge($destino, $origen, 55, 17, 0, 0, $source_x, $source_y, 100);

        //Imprimir y liberar memoria
        //header('Content-Type: image/jpg');
        $Img_atras="etiquetaActivo.jpg";
        $im=imagejpeg($destino,$Img_atras,100);

        //Agregamos la imagen al zip
        $zip = new ZipArchive();
        $zip->open("etiqueta_activo.zip",ZipArchive::CREATE);
        $zip->addFile($Img_atras,$Img_atras);
        $zip->close();

        imagedestroy($destino);
        imagedestroy($origen);
        
        //Borramos las imagenes creadas
        unlink($Img_atras);
        unlink('codigoBarras.jpg');
        
        
        
    /*
    //Generar imagen del codigo de barras
    // Loading Font
    $font = new BCGFontFile('barcodegen/font/Arial.ttf', 9);
    $text = $activo['Codigo'];
    
    // The arguments are R, G, B for color.
    $color_black = new BCGColor(0, 0, 0);
    $color_white = new BCGColor(255, 255, 255);

    $drawException = null;
    try {
        $code = new BCGcode39();
        $code->setScale(2); // Resolution
        $code->setThickness(12); // Thickness
        $code->setForegroundColor($color_black); // Color of bars
        $code->setBackgroundColor($color_white); // Color of spaces
        $code->setFont($font); // Font (or 0)
        $code->parse($text); // Text
    } catch(Exception $exception) {
        $drawException = $exception;
    }

    //Here is the list of the arguments
    //1 - Filename (empty : display on screen)
    //2 - Background color 
    $drawing = new BCGDrawing('etiqueta_activo_'.$activo['Codigo'].'.jpg', $color_white);
    if($drawException) {
        $drawing->drawException($drawException);
    } else {
        $drawing->setBarcode($code);
        $drawing->draw();
    }
    
    //Header that says it is an image (remove it if you save the barcode to a file)
    //header('Content-Type: image/png');
    //header('Content-Disposition: inline; filename="barcode.png"');
    // Draw (or save) the image into PNG format.
    $drawing->finish(BCGDrawing::IMG_FORMAT_JPEG);
    $Img_etiqueta='etiqueta_activo_'.$activo['Codigo'].'.jpg';
    //Agregamos la imagen al zip
    $zip = new ZipArchive();
    $zip->open("etiqueta_activo.zip",ZipArchive::CREATE);
    $zip->addFile($Img_etiqueta,$Img_etiqueta);
    $zip->close();

    //Borramos las imagenes creadas
    unlink('etiqueta_activo_'.$activo['Codigo'].'.jpg');
     * 
     */
}