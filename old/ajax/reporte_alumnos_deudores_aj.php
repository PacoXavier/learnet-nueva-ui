<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 
require_once('../estandares/cantidad_letra.php'); 


if (isset($_POST['action']) && $_POST['action'] == "buscarAlum") {
    $DaoAlumnos = new DaoAlumnos();
    foreach ($DaoAlumnos->buscarAlumnosAltasYBajas($_POST['buscar']) as $k => $v) {
        ?>
        <li id_alum="<?php echo $v->getId(); ?>"><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li>
        <?php
    }
}

if(isset($_POST['action']) && $_POST['action']=="filtro"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoRecargos= new DaoRecargos();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoUsuarios=new DaoUsuarios();
    $DaoCiclos= new DaoCiclos();
    $DaoGrupos= new DaoGrupos();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();

    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $ciclo=$DaoCiclos->getActual();
    
    $query="";
    if($_POST['Id_alum']>0){
        $query=$query."AND Id_ins=".$_POST['Id_alum'];
    }
    
    $queryCiclo="";
    if($_POST['Id_ciclo']>0){
        //$ciclo=$DaoCiclos->show($_POST['Id_ciclo']);
        $queryCiclo=" AND ciclos_alum_ulm.Id_ciclo=".$_POST['Id_ciclo'];
        //$query_fecha_pago=" AND (Fecha_pago>='".$ciclo->getFecha_ini()."' AND Fecha_pago<='".$ciclo->getFecha_fin()."')";
    }
    
    if($_POST['Turno']>0){
        $query=$query." AND Turno=".$_POST['Turno'];
    }
    
    if($_POST['Id_grupo']>0){
        $query=$query." AND materias_ciclo_ulm.Id_grupo=".$_POST['Id_grupo'];
    }

    $query_fecha_pago="";
    if(strlen($_POST['fecha_ini'])>0 && $_POST['fecha_fin']==null){
        $query_fecha_pago=" AND Fecha_pago>='".$_POST['fecha_ini']."'";
    }elseif(strlen($_POST['fecha_fin'])>0 && $_POST['fecha_ini']==null){
        $query_fecha_pago=" AND Fecha_pago<='".$_POST['fecha_fin']."'";
    }elseif(strlen($_POST['fecha_ini'])>0 && strlen($_POST['fecha_fin'])>0){
        $query_fecha_pago=" AND (Fecha_pago>='".$_POST['fecha_ini']."' AND Fecha_pago<='".$_POST['fecha_fin']."')";
    }

    if($_POST['Id_oferta']>0){
        $query=$query." AND ofertas_alumno.Id_ofe=".$_POST['Id_oferta'];
    }
    if($_POST['Id_esp']>0){
        $query=$query." AND ofertas_alumno.Id_esp=".$_POST['Id_esp'];
    }
    if($_POST['Id_ori']>0){
        $query=$query." AND ofertas_alumno.Id_ori=".$_POST['Id_ori'];
    }
 
    if($_POST['Tipo']==1){
     $query = "SELECT ofertas_alumno.*,ciclos_alum_ulm.* ,materias_ciclo_ulm.*, ofertas_alumno.Id_ori AS Orientacion, inscripciones_ulm.* FROM inscripciones_ulm 
     JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
     JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
     JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
     WHERE inscripciones_ulm.tipo=1  AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()."  AND Activo_oferta=1 ".$query." ".$queryCiclo." AND Baja_ofe IS NULL GROUP BY ofertas_alumno.Id_ofe_alum ORDER BY Id_ins"; 
    }elseif($_POST['Tipo']==2){
     //Los alumnos que estan dados de baja como cambio de plan o de oferta no se muestran aqui
     $query = "SELECT ofertas_alumno.*,ciclos_alum_ulm.* ,materias_ciclo_ulm.*, ofertas_alumno.Id_ori AS Orientacion, inscripciones_ulm.* FROM inscripciones_ulm 
     JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum 
     JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
     JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
     JOIN Motivos_bajas ON ofertas_alumno.Id_mot_baja=Motivos_bajas.Id_mot
     WHERE inscripciones_ulm.tipo=1  AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()."  AND Activo_oferta=0 AND Baja_ofe IS NOT NULL ".$query." ".$queryCiclo."  GROUP BY ofertas_alumno.Id_ofe_alum ORDER BY Id_ins";
    }else{
     //100016
     //Reporte deudores mostrar todas las deudas del alumno aunque no se seleccione nada en el filtro.
     $query = "SELECT ofertas_alumno.*,ciclos_alum_ulm.* ,materias_ciclo_ulm.*, ofertas_alumno.Id_ori AS Orientacion, inscripciones_ulm.* FROM inscripciones_ulm 
     JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum 
     JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
     JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
     LEFT JOIN Motivos_bajas ON ofertas_alumno.Id_mot_baja=Motivos_bajas.Id_mot
     WHERE inscripciones_ulm.tipo=1  AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()."  ".$query." ".$queryCiclo."   GROUP BY ofertas_alumno.Id_ofe_alum ORDER BY Id_ins";     
    }
     $count=1;
     $total=0;
     $ttAdeudo=0;
     foreach($base->advanced_query($query) as $k=>$v){
               $alum = $DaoAlumnos->show($v['Id_ins']);
               $nombre_ori="";

               $oferta = $DaoOfertas->show($v['Id_ofe']);
               $esp = $DaoEspecialidades->show($v['Id_esp']);
               if ($v['Orientacion'] > 0) {
                  $ori = $DaoOrientaciones->show($v['Orientacion']);
                  $nombre_ori = $ori->getNombre();
                }
                $totalAdeudo = $DaoAlumnos->getAdeudoOfertaAlumno($v['Id_ofe_alum'],$query_fecha_pago);
                if($totalAdeudo>0){
                    $ttAdeudo+=$totalAdeudo;
                    $cicloAlumno=$DaoCiclosAlumno->getLastCicloOferta($v['Id_ofe_alum']);
                    $total=0;
                    $countGrupos=0;
                    $porcentajeAsistencias=0;
                    if($cicloAlumno->getId()>0){

                        foreach ($DaoMateriasCicloAlumno->getMateriasCicloAlumno($cicloAlumno->getId()) as $materiaCiclo) {
                                if($materiaCiclo->getId_grupo()>0){
                                    $resp = $DaoGrupos->getAsistenciasGrupoAlum($materiaCiclo->getId_grupo(), $v['Id_ins']);
                                    $Asis = $resp['Asistencias'];
                                    $Faltas = $resp['Faltas'];
                                    $Just = $resp['Justificaciones'];

                                    $total+= $base->porcentajeAsistencias($resp['Asistencias'], $resp['Justificaciones'], $resp['Faltas']);

                                    $countGrupos++;
                                }
                          }   
                    }
                    if($countGrupos>0){
                       $porcentajeAsistencias=$DaoCiclosAlumno->redonderCalificacion($total/$countGrupos);
                    }
              ?>
                      <tr id_alum="<?php echo $alum->getId();?>" id_ofe_alum="<?php echo $v['Id_ofe_alum'];?>">
                        <td width="30px;"><?php echo $count;?></td>
                        <td><a href="alumno.php?id=<?php echo $alum->getId();?>"><?php echo $alum->getMatricula() ?></a></td>
                        <td style="width: 115px;"><a href="alumno.php?id=<?php echo $alum->getId();?>"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></a></td>
                        <td><?php echo $oferta->getNombre_oferta(); ?></td>
                        <td><?php echo $esp->getNombre_esp(); ?></td>
                        <td><?php echo $nombre_ori; ?></td>
                        <td class="td-center" style="color:red">$<?php echo number_format($totalAdeudo,2); ?></td>
                        <td class="td-center"><?php echo $porcentajeAsistencias; ?></td>
                        <td class="td-center"><input type="checkbox"> </td>
                        <td style="width:110px;">
                                <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span> 
                                <div class="box-buttom">
                                    <?php
                                    if($oferta->getTipoOferta()==1){
                                        ?>
                                         <a href="pago.php?id=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button>Mostrar pagos</button></a>
                                        <?php
                                    }elseif($oferta->getTipoOferta()==2){
                                        ?>
                                         <a href="cargos_curso.php?id=<?php echo $v['Id_ofe_alum']?>" target="_blank"><button>Mostrar pagos</button></a>
                                        <?php   
                                    }
                                    ?>
                                    <button onclick="mostrar_box_seguimiento(<?php echo $alum->getId();?>)">Seguimiento</button>
                                    <a href="historial_seguimiento.php?id=<?php echo $alum->getId();?>&tipo=alum" target="_blank"><button>Historial</button></a>
                                </div>
                        </td>
                      </tr>
                      <?php
                      $count++;
                }  
        }
        ?>
        <tr>
               <td colspan="5"></td>
               <td style="color:red"><b>Total</b></td>
               <td style="color:red" class="td-center"><b>$<?php echo number_format($ttAdeudo,2)?> </b></td>
               <td colspan="3"></td>
        </tr>
        <?php
}



if(isset($_POST['action']) && $_POST['action']=="mostrar_box_seguimiento"){
 ?>
<div id="box_emergente" class="box_seguimiento">
    <h1>Capturar seguimiento</h1>
    <p><textarea id="comentario"></textarea></p>
    <button onclick="capturar(<?php echo $_POST['Id_alum']?>)">Guardar</button>
    <button onclick="ocultar_error_layer()">Cerrar</button>
</div>
<?php   
}


if(isset($_POST['action']) && $_POST['action']=="capturar"){
    $DaoSeguimientoAdeudoAlumno= new DaoSeguimientoAdeudoAlumno();
    $SeguimientoAdeudoAlumno= new SeguimientoAdeudoAlumno();
    $SeguimientoAdeudoAlumno->setId_usu($_COOKIE['admin/Id_usu']);
    $SeguimientoAdeudoAlumno->setDateCreated(date('Y-m-d H:i:s'));
    $SeguimientoAdeudoAlumno->setTipoRel('alum');
    $SeguimientoAdeudoAlumno->setIdRel($_POST['Id_alum']);
    $SeguimientoAdeudoAlumno->setComentario($_POST['comentario']);
    $DaoSeguimientoAdeudoAlumno->add($SeguimientoAdeudoAlumno);
}



if(isset($_POST['action']) && $_POST['action']=="send_email"){
    
   $base= new base();
   $DaoAlumnos= new DaoAlumnos();
   $DaoOfertasAlumno = new DaoOfertasAlumno();
   $DaoOfertas = new DaoOfertas();
   $DaoEspecialidades = new DaoEspecialidades();
   $DaoOrientaciones = new DaoOrientaciones();
   $DaoCiclos = new DaoCiclos();
   $DaoCiclosAlumno = new DaoCiclosAlumno();
    
   foreach($_POST['alumnos'] as $k=>$v){
    $alum=$DaoAlumnos->show($v['id']);   
    $oferta_alumno = $DaoOfertasAlumno->show($v['id_ofe_alum']);
    $oferta = $DaoOfertas->show($oferta_alumno->getId_ofe());
    $esp = $DaoEspecialidades->show($oferta_alumno->getId_esp());
    if ($oferta_alumno->getId_ori() > 0) {
        $ori = $DaoOrientaciones->show($oferta_alumno->getId_ori());
        $nombre_ori = $ori->getNombre();
    }
    
    $totalAdeudo = $DaoAlumnos->getAdeudoOfertaAlumno($oferta_alumno->getId());                              
                                    
    $asunto="Cobranza";
    $mensaje="Fecha: ".date('Y-m-d H:i:s')."<br><br>
    Estimado <b>".mb_strtoupper($alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM(),"UTF-8")."</b>:<br><br>
    
    Por este medio te informamos que tienes un saldo pendiente de <b>$".  number_format($totalAdeudo,2)."</b>  (".num2letras($totalAdeudo,2)."). 
    Por concepto de colegiaturas vencidas en tu carrera de <b>".mb_strtoupper($oferta->getNombre_oferta(),"UTF-8")." / ".mb_strtoupper($esp->getNombre_esp(),"UTF-8")." / ".mb_strtoupper($nombre_ori,"UTF-8")."</b>.<br><br>

    Es necesario que estes al corriente en tus colegiaturas para evitar cualquier contratiempo
    al momento de presentar tus exámenes o evitar una suspensión por falta de pagos.<br><br>

    Si tienes alguna duda, aclaración o cuentas con tu comprobante de pago, puedes acudir a control escolar.<br>";

    $arrayData= array();
    $arrayData['Asunto']=$asunto;
    $arrayData['Mensaje']=$mensaje;
    $arrayData['Destinatarios']=array();

    if(strlen($alum->getEmail())>0){
         $Data= array();
         $Data['email']= $alum->getEmail();
         //$Data['email']= "christian310332@gmail.com";
         $Data['name']= $alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
         array_push($arrayData['Destinatarios'], $Data);
    }
    $base->send_email($arrayData);
   }
}



if(isset($_GET['action']) && $_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'MATRICULA');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'OFERTA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'CARRERA');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'ORIENTACIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'ADEUDO');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'TIPO');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','H') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }

    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoRecargos= new DaoRecargos();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoUsuarios=new DaoUsuarios();
    
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query="";
    if($_GET['Id_alum']>0){
        $query="AND Id_ins=".$_GET['Id_alum'];
    }
    
    $queryCiclo="";
    $query_fecha_pago="";
    if($_GET['Id_ciclo']>0){
        $queryCiclo=" AND ciclos_alum_ulm.Id_ciclo=".$_GET['Id_ciclo'];
        $query_fecha_pago=" AND (Fecha_pago>='".$ciclo->getFecha_ini()."' AND Fecha_pago<='".$ciclo->getFecha_fin()."')";
    }
    
    if($_GET['Turno']>0){
        $query=$query." AND Turno=".$_GET['Turno'];
    }
    
    if($_GET['Id_grupo']>0){
        $query=$query." AND materias_ciclo_ulm.Id_grupo=".$_GET['Id_grupo'];
    }

    if($_GET['Id_oferta']>0){
        $query=$query." AND Id_ofe=".$_GET['Id_oferta'];
    }
    if($_GET['Id_esp']>0){
        $query=$query." AND Id_esp=".$_GET['Id_esp'];
    }
    if($_GET['Id_ori']>0){
        $query=$query." AND Id_ori=".$_GET['Id_ori'];
    }
    
    $query_fecha_pago="";
    if(strlen($_GET['fecha_ini'])>0 && $_GET['fecha_fin']==null){
        $query_fecha_pago=" AND Fecha_pago>='".$_GET['fecha_ini']."'";
    }elseif(strlen($_GET['fecha_fin'])>0 && $_GET['fecha_ini']==null){
        $query_fecha_pago=" AND Fecha_pago<='".$_GET['fecha_fin']."'";
    }elseif(strlen($_GET['fecha_ini'])>0 && strlen($_GET['fecha_fin'])>0){
        $query_fecha_pago=" AND (Fecha_pago>='".$_GET['fecha_ini']."' AND Fecha_pago<='".$_GET['fecha_fin']."')";
    }  
    
    if($_GET['Tipo']==1){
     $query = "SELECT * FROM inscripciones_ulm 
     JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum 
     WHERE tipo=1  AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()."  AND Activo_oferta=1 ".$query." AND Baja_ofe IS NULL  ORDER BY Id_ins"; 
     $tipo="Activo";
    }else{
     //Los alumnos que estan dados de baja como cambio de plan o de oferta no se muestran aqui
     $query = "SELECT * FROM inscripciones_ulm 
     JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum 
     JOIN Motivos_bajas ON ofertas_alumno.Id_mot_baja=Motivos_bajas.Id_mot
     WHERE tipo=1  AND inscripciones_ulm.Id_plantel=".$usu->getId_plantel()."  AND Activo_oferta=0 AND Baja_ofe IS NOT NULL ".$query."   ORDER BY Id_ins";
     $tipo="Baja";
    }
     $count=1;
     foreach($base->advanced_query($query) as $k=>$v){
               $alum = $DaoAlumnos->show($v['Id_ins']);
               $nombre_ori="";

               $oferta = $DaoOfertas->show($v['Id_ofe']);
               $esp = $DaoEspecialidades->show($v['Id_esp']);
               if ($v['Id_ori'] > 0) {
                  $ori = $DaoOrientaciones->show($v['Id_ori']);
                  $nombre_ori = $ori->getNombre();
                }
                $totalAdeudo = $DaoAlumnos->getAdeudoOfertaAlumno($v['Id_ofe_alum'],$query_fecha_pago);
                if($totalAdeudo>0){
                    $count++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
                    $objPHPExcel->getActiveSheet()->setCellValue("B$count", $alum->getMatricula());
                    $objPHPExcel->getActiveSheet()->setCellValue("C$count", $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM());
                    $objPHPExcel->getActiveSheet()->setCellValue("D$count", $oferta->getNombre_oferta());
                    $objPHPExcel->getActiveSheet()->setCellValue("E$count", $esp->getNombre_esp());
                    $objPHPExcel->getActiveSheet()->setCellValue("F$count", $nombre_ori);
                    $objPHPExcel->getActiveSheet()->setCellValue("G$count", number_format($totalAdeudo,2));
                    $objPHPExcel->getActiveSheet()->setCellValue("H$count", $tipo);
                }  
        }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=Deudores-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}