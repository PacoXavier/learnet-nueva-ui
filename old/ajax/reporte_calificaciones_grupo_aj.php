<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 


if($_POST['action']=="filtro"){
    
    $DaoUsuarios= new DaoUsuarios();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $base= new base();
    $DaoGrupos= new DaoGrupos();
    $DaoAlumnos= new DaoAlumnos();
    $DaoOfertas= new DaoOfertas();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoTurnos= new DaoTurnos();
    
    $query="";
    if(isset($_POST['Id_grupo']) && $_POST['Id_grupo']>0){
      $query=" AND Grupos.Id_grupo=".$_POST['Id_grupo'];
    }
    if(isset($_POST['Id_turno']) && $_POST['Id_turno']>0){
        $query=$query." AND Grupos.Turno=".$_POST['Id_turno'];
    }
    if(isset($_POST['Id_mat']) && $_POST['Id_mat']>0){
        $query=$query." AND Grupos.Id_mat=".$_POST['Id_mat'];
    }
    if(isset($_POST['Id_ciclo']) && $_POST['Id_ciclo']>0){
        $query=$query." AND Grupos.Id_ciclo=".$_POST['Id_ciclo'];
    }
    
    if(isset($_POST['Id_ori']) && $_POST['Id_ori']>0){
        $query=$query." AND Grupos.Id_ori=".$_POST['Id_ori'];
    }
    $GroupBy="GROUP BY Id_alum,Id_grupo";
    if(isset($_POST['Id_docen']) && $_POST['Id_docen']>0){
        $query=$query." AND Horario_docente.Id_docente=".$_POST['Id_docen'];
        $GroupBy="GROUP BY Id_alum,Id_grupo";
    }
    $count=1;
    $query  = "
            SELECT Grupos.*,
                   materias_ciclo_ulm.CalExtraordinario,materias_ciclo_ulm.CalEspecial,materias_ciclo_ulm.CalTotalParciales,materias_ciclo_ulm.Activo, materias_ciclo_ulm.Id_alum, 
                   materias_uml.Nombre,materias_uml.Clave_mat
            FROM Grupos 
            JOIN materias_ciclo_ulm ON Grupos.Id_grupo=materias_ciclo_ulm.Id_grupo
            JOIN materias_uml ON Grupos.Id_mat=materias_uml.Id_mat
            JOIN Horario_docente ON Grupos.Id_grupo=Horario_docente.Id_grupo
            WHERE 
                  Grupos.Activo_grupo=1 
                  AND materias_ciclo_ulm.Activo=1 
                  AND Grupos.Id_plantel=".$usu->getId_plantel()." ".$query." 
                  ".$GroupBy."
                  ORDER BY Grupos.Id_grupo DESC";
    
    
      foreach($base->advanced_query($query) as $row_Grupos){
        $alum=$DaoAlumnos->show($row_Grupos['Id_alum']);
        $tur = $DaoTurnos->show($row_Grupos['Turno']);
        $turno=$tur->getNombre();

        $mat_esp=$DaoMateriasEspecialidad->show($row_Grupos['Id_mat_esp']);
        $mat = $DaoMaterias->show($mat_esp->getId_mat());
        
        $NombreMat=$mat->getNombre();
        if(strlen($mat_esp->getNombreDiferente())>0){
           $NombreMat=$mat_esp->getNombreDiferente(); 
        }

        $NombreOri="";
        if($row_Grupos['Id_ori']>0){
          $ori = $DaoOrientaciones->show($row_Grupos['Id_ori']);
          $NombreOri=$ori->getNombre();
        }

        //Verificamos si tiene derecho 
        $resp=$DaoGrupos->getAsistenciasGrupoAlum($row_Grupos['Id_grupo'],$row_Grupos['Id_alum']);
        $Asis=$resp['Asistencias'];
        $Faltas=$resp['Faltas'];
        $Just=$resp['Justificaciones'];

        $classTr="";
        $PorcentajeAsistencias= $base->porcentajeAsistencias($resp['Asistencias'],$resp['Justificaciones'],$resp['Faltas']);
        if($PorcentajeAsistencias<80){
             $classTr="navajowhite";
        }
        
        //Por si pusieron nomenclatura AC, CP, etc
        $CalTotalParciales=$base->getCalificacion($row_Grupos['CalTotalParciales']);
        $CalExtraordinario=$base->getCalificacion($row_Grupos['CalExtraordinario']);
        $CalEspecial=$base->getCalificacion($row_Grupos['CalEspecial']);                
        ?>
 
        <tr class="<?php echo $query;;?>">
            <td><?php echo $count?></td>
            <td><?php echo $alum->getMatricula()?> </td>
            <td><?php echo $alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM()?> </td>
            <td style="text-align: center"><?php echo $turno?></td>
            <td><?php echo $row_Grupos['Clave']?></td>
            <td><?php echo $NombreMat?></td>
            <td><?php echo $NombreOri?></td>
            <td style="text-align: center;color: red;"><?php echo $Faltas; ?></td>
            <td style="text-align: center;color: green;"><?php echo $Asis; ?></td>
            <td style="text-align: center; color: black;"><?php echo $Just; ?></td>
            <td style="text-align: center; color: black;"><?php echo number_format($PorcentajeAsistencias, 2) ?>%</td>
            <td><?php echo $CalTotalParciales?></td>
            <td><?php echo $CalExtraordinario?></td>
            <td><?php echo $CalEspecial?></td>
        </tr>

        <?php
           $count++;
      }
}




if($_POST['action'] == "verificar_orientaciones") {
  $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
  $DaoEspecialidades= new DaoEspecialidades();
  $DaoOrientaciones= new DaoOrientaciones();
  $mat_esp=$DaoMateriasEspecialidad->show($_POST['Id_mat_esp']);
  if($mat_esp->getTiene_orientacion()==1){
      ?>
        <p>Orientaci&oacute;n<br>                                   
          <select id="orientaciones">
              <option value="0">Selecciona una orientaci&oacute;n</option>
              <?php
                  foreach ($DaoOrientaciones->getOrientacionesEspecialidad($mat_esp->getId_esp()) as $ori) {
                  ?>
              <option value="<?php echo $ori->getId() ?>"> <?php echo $ori->getNombre() ?> </option>
                  <?php
                  }
                  ?>
          </select></p>
    <?php
  }
}