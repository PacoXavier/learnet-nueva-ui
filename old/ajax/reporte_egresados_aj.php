<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="filtro"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclos= new DaoCiclos();

    $query="";

    if($_POST['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_POST['Id_ofe'];
    }
    if($_POST['Id_esp']>0){
        $query=$query." AND Id_esp=".$_POST['Id_esp'];
    }
    if($_POST['Id_ori']>0){
        $query=$query." AND Id_ori=".$_POST['Id_ori'];
    }
    
    if($_POST['Id_ciclo']>0){
        $query=$query." AND IdCicloEgreso=".$_POST['Id_ciclo'];
    }
    
    if(strlen($_POST['Matricula'])>0){
        $query=$query." AND Matricula='".$_POST['Matricula']."'";
    }
    

    foreach ($DaoAlumnos->getAlumnosEgresados($query,null,null) as $k=>$v){
               $nombre_ori="";
               $oferta = $DaoOfertas->show($v['Id_ofe']);
               $esp = $DaoEspecialidades->show($v['Id_esp']);
               if ($v['Id_ori'] > 0) {
                  $ori = $DaoOrientaciones->show($v['Id_ori']);
                  $nombre_ori = $ori->getNombre();
                }
                $opcion="Plan por materias"; 
                if($v['Opcion_pago']==2){
                  $opcion="Plan completo";  
                }
                $ciclo=$DaoCiclos->show($v['IdCicloEgreso']);
              ?>
                  <tr id_alum="<?php echo $v['Id_ins'];?>">
                    <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $v['Matricula'] ?></td>
                    <td style="width: 115px;" onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                    <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $oferta->getNombre_oferta(); ?></td>
                    <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $esp->getNombre_esp(); ?></td>
                    <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $nombre_ori; ?></td>
                    <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $opcion; ?></td>
                    <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $ciclo->getClave(); ?></td>
                    <td style="text-align: center;"><input type="checkbox"> </td>
                  </tr>
                  <?php
            }
}



if($_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'MATRICULA');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'OFERTA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'CARRERA');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'ORIENTACIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'OPCIÓN DE PAGO');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'CICLO');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','H') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclos= new DaoCiclos();

    $query="";

    if($_GET['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_GET['Id_ofe'];
    }
    if($_GET['Id_esp']>0){
        $query=$query." AND Id_esp=".$_GET['Id_esp'];
    }
    if($_GET['Id_ori']>0){
        $query=$query." AND Id_ori=".$_GET['Id_ori'];
    }
    
    if($_GET['Id_ciclo']>0){
        $query=$query." AND IdCicloEgreso=".$_GET['Id_ciclo'];
    }
    $count=1;
    foreach ($DaoAlumnos->getAlumnosEgresados($query,null,null) as $k=>$v){
               $nombre_ori="";
               $oferta = $DaoOfertas->show($v['Id_ofe']);
               $esp = $DaoEspecialidades->show($v['Id_esp']);
               if ($v['Id_ori'] > 0) {
                  $ori = $DaoOrientaciones->show($v['Id_ori']);
                  $nombre_ori = $ori->getNombre();
                }
                $opcion="Plan por materias"; 
                if($v['Opcion_pago']==2){
                  $opcion="Plan completo";  
                }
                $ciclo=$DaoCiclos->show($v['IdCicloEgreso']);
                $count++;
                $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
                $objPHPExcel->getActiveSheet()->setCellValue("B$count", $v['Matricula']);
                $objPHPExcel->getActiveSheet()->setCellValue("C$count", $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins']);
                $objPHPExcel->getActiveSheet()->setCellValue("D$count", $oferta->getNombre_oferta());
                $objPHPExcel->getActiveSheet()->setCellValue("E$count", $esp->getNombre_esp());
                $objPHPExcel->getActiveSheet()->setCellValue("F$count", $nombre_ori);
                $objPHPExcel->getActiveSheet()->setCellValue("G$count", $opcion);
                $objPHPExcel->getActiveSheet()->setCellValue("H$count", $ciclo->getClave());
            }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=Egresados-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}



