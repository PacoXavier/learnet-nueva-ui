<?php
require_once('activate_error.php');
require_once('../require_daos.php');
require_once('../estandares/cantidad_letra.php');

if ($_POST['action'] == "buscarAlum") {
    $DaoAlumnos = new DaoAlumnos();
    $DaoUsuarios = new DaoUsuarios();
    $DaoCiclos = new DaoCiclos();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $base = new base();

    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query_BusquedaPro = "SELECT *, CONCAT(
            IFNULL(REPLACE(Nombre_ins,' ',''),''),
            IFNULL(REPLACE(ApellidoP_ins,' ','') ,''),
            IFNULL(REPLACE(ApellidoM_ins,' ','') ,''),
            IFNULL(REPLACE(Matricula,' ','') ,'')) Buscar   
	FROM inscripciones_ulm 
        JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
	HAVING Buscar LIKE '%" . str_replace(' ', '', $_POST['buscar']) . "%' AND tipo=1 AND Activo_oferta=1 AND inscripciones_ulm.Id_plantel=" . $usu->getId_plantel() . " AND Baja_ofe IS NULL AND IdCicloEgreso IS NULL  ORDER BY Id_ins DESC LIMIT 20";
    foreach ($base->advanced_query($query_BusquedaPro) as $busqueda) {
        $alum = $DaoAlumnos->show($busqueda['Id_ins']);

        $nombre_ori = "";
        $oferta = $DaoOfertas->show($busqueda['Id_ofe']);
        $esp = $DaoEspecialidades->show($busqueda['Id_esp']);
        if ($busqueda['Id_ori'] > 0) {
            $ori = $DaoOrientaciones->show($busqueda['Id_ori']);
            $nombre_ori = $ori->getNombre();
        }

        $opcion = "Plan por materias";
        if ($busqueda['Opcion_pago'] == 2) {
            $opcion = "Plan completo";
        }
        ?>
        <tr id_alum="<?php echo $alum->getId(); ?>" id_ofe_alum="<?php echo $busqueda['Id_ofe_alum']; ?>">
            <td><?php echo $alum->getMatricula() ?></td>
            <td style="width: 115px;"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
            <td><?php echo $oferta->getNombre_oferta(); ?></td>
            <td><?php echo $esp->getNombre_esp(); ?></td>
            <td><?php echo $nombre_ori; ?></td>
            <td><?php echo $opcion; ?></td>
            <td><p><input type="text" class="otro-email"><button onclick="addInput(this)">Añadir email</button></p></td>
            <td><span>Sin enviar</span></td>
        </tr>
        <?php
    }
}






if ($_POST['action'] == "filtro") {
    $DaoAlumnos = new DaoAlumnos();
    $DaoUsuarios = new DaoUsuarios();
    $DaoCiclos = new DaoCiclos();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $base = new base();

    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query = "";

    if ($_POST['Id_ofe'] > 0) {
        $query = $query . " AND Id_ofe=" . $_POST['Id_ofe'];
    }
    if ($_POST['Id_esp'] > 0) {
        $query = $query . " AND Id_esp=" . $_POST['Id_esp'];
    }
    if ($_POST['Id_ori'] > 0) {
        $query = $query . " AND Id_ori=" . $_POST['Id_ori'];
    }
    if ($_POST['Opcion_pago'] > 0) {
        $query = $query . " AND Opcion_pago=" . $_POST['Opcion_pago'];
    }


    $query_inscripciones_ulm = "SELECT * FROM inscripciones_ulm 
     LEFT JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum 
     WHERE tipo=1  AND inscripciones_ulm.Id_plantel=" . $usu->getId_plantel() . "  AND Activo_oferta=1 " . $query . " AND Baja_ofe IS NULL AND IdCicloEgreso IS NULL ORDER BY Id_ins";
    foreach ($base->advanced_query($query_inscripciones_ulm) as $busqueda) {
        $alum = $DaoAlumnos->show($busqueda['Id_ins']);

        $nombre_ori = "";
        $oferta = $DaoOfertas->show($busqueda['Id_ofe']);
        $esp = $DaoEspecialidades->show($busqueda['Id_esp']);
        if ($busqueda['Id_ori'] > 0) {
            $ori = $DaoOrientaciones->show($busqueda['Id_ori']);
            $nombre_ori = $ori->getNombre();
        }

        $opcion = "Plan por materias";
        if ($busqueda['Opcion_pago'] == 2) {
            $opcion = "Plan completo";
        }
        ?>
        <tr id_alum="<?php echo $alum->getId(); ?>" id_ofe_alum="<?php echo $busqueda['Id_ofe_alum']; ?>">
            <td><?php echo $alum->getMatricula() ?></td>
            <td style="width: 115px;"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
            <td><?php echo $oferta->getNombre_oferta(); ?></td>
            <td><?php echo $esp->getNombre_esp(); ?></td>
            <td><?php echo $nombre_ori; ?></td>
            <td><?php echo $opcion; ?></td>
            <td><p><input type="text" class="otro-email"><button onclick="addInput(this)">Añadir email</button></p></td>
            <td><span>Sin enviar</span></td>
        </tr>
        <?php
    }
}



if ($_POST['action'] == "send_email") {
    $base = new base();
    $DaoOfertas = new DaoOfertas();
    $DaoAlumnos = new DaoAlumnos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoRecargos = new DaoRecargos();
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoUsuarios = new DaoUsuarios();
    $DaoCiclos = new DaoCiclos();
    $DaoGrupos = new DaoGrupos();

    $ciclo = $DaoCiclos->show($_POST['Id_ciclo']);
    $alum = $DaoAlumnos->show($_POST['Id_alum']);
    $Oferta_alum = $DaoOfertasAlumno->show($_POST['Id_ofe_alum']);

    $totalAdeudo = $DaoAlumnos->getAdeudoOfertaAlumno($_POST['Id_ofe_alum']);


    $ban = 0;
    $materia = "";
    $query = "
        SELECT ciclos_alum_ulm.Id_ciclo,
             materias_ciclo_ulm.*,
             Materias_especialidades.Id_mat,Materias_especialidades.NombreDiferente,
             materias_uml.Nombre,materias_uml.Clave_mat,
             Grupos.Clave
        FROM ciclos_alum_ulm
            JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
            JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
            JOIN materias_uml ON Materias_especialidades.Id_mat=materias_uml.Id_mat
            JOIN Grupos ON materias_ciclo_ulm.Id_grupo=Grupos.Id_grupo
        WHERE Id_ofe_alum=" . $_POST['Id_ofe_alum'] . " AND ciclos_alum_ulm.Id_ciclo=" . $_POST['Id_ciclo'] . " AND materias_ciclo_ulm.Id_grupo IS NOT NULL AND Activo=1";
    $totalRows = $base->getTotalRows($query);
    foreach ($base->advanced_query($query) as $row_ciclo_actual) {

        $nombreMat = $row_ciclo_actual['Nombre'];
        if (strlen($row_ciclo_actual['CalExtraordinario']) > 0 || strlen($row_ciclo_actual['CalEspecial']) > 0 || strlen($row_ciclo_actual['CalTotalParciales']) > 0) {
            $total = 0;
            if ($row_ciclo_actual['Id_grupo'] > 0) {
                $grupo = $DaoGrupos->show($row_ciclo_actual['Id_grupo']);
                $asis = $DaoGrupos->getAsistenciasGrupoAlum($grupo->getId(), $_POST['Id_alum']);
                $total = $DaoGrupos->porcentajeAsistencias($asis['Asistencias'], $asis['Justificaciones'], $asis['Faltas']);
            }
            if (strlen($row_ciclo_actual['NombreDiferente']) > 0) {
                $nombreMat = $row_ciclo_actual['NombreDiferente'];
            }
            $ban = 1;
            $materia.='<tr>
                            <td style="font-size: 9px; text-transform: uppercase; vertical-align: middle; background-color: #f7f7f7; padding: 8px; border: 1px solid #ddd;" bgcolor="#f7f7f7" valign="middle">' . $row_ciclo_actual['Clave_mat'] . ' </td>
                            <td style="width: 200px; font-size: 9px; text-transform: uppercase; vertical-align: middle; background-color: #f7f7f7; padding: 8px; border: 1px solid #ddd;" bgcolor="#f7f7f7" valign="middle">' . mb_strtoupper($nombreMat, "UTF-8") . '</td>
                            <td style="width: 200px; font-size: 9px; text-transform: uppercase; vertical-align: middle; background-color: #f7f7f7; padding: 8px; border: 1px solid #ddd;" bgcolor="#f7f7f7" valign="middle">' . $row_ciclo_actual['Clave'] . '</td>
                            <td style="text-align: center; color: black; font-size: 9px; text-transform: uppercase; vertical-align: middle; background-color: #f7f7f7; padding: 8px; border: 1px solid #ddd;" align="center" bgcolor="#f7f7f7" valign="middle">' . $total . '%</td>
                            <td style="text-align: center; font-size: 9px; text-transform: uppercase; vertical-align: middle; background-color: #f7f7f7; padding: 8px; border: 1px solid #ddd;" align="center" bgcolor="#f7f7f7" valign="middle">' . $DaoAlumnos->redonderCalificacion($row_ciclo_actual['CalTotalParciales']) . '</td>
                            <td style="text-align: center; font-size: 9px; text-transform: uppercase; vertical-align: middle; background-color: #f7f7f7; padding: 8px; border: 1px solid #ddd;" align="center" bgcolor="#f7f7f7" valign="middle">' . $DaoAlumnos->redonderCalificacion($row_ciclo_actual['CalExtraordinario']) . '</td>
                            <td style="text-align: center; font-size: 9px; text-transform: uppercase; vertical-align: middle; background-color: #f7f7f7; padding: 8px; border: 1px solid #ddd;" align="center" bgcolor="#f7f7f7" valign="middle">' . $DaoAlumnos->redonderCalificacion($row_ciclo_actual['CalEspecial']) . '</td>
                        </tr>';
        }
    }
    if ($totalRows > 0) {
        $nombre_ori = "";
        $oferta = $DaoOfertas->show($Oferta_alum->getId_ofe());
        $esp = $DaoEspecialidades->show($Oferta_alum->getId_esp());
        if ($Oferta_alum->getId_ori() > 0) {
            $ori = $DaoOrientaciones->show($Oferta_alum->getId_ori());
            $nombre_ori = $ori->getNombre();
        }

        $opcion = "Plan por materias";
        if ($Oferta_alum->getOpcionPago() == 2) {
            $opcion = "Plan completo";
        }

        if ($totalAdeudo > 0) {
            $asunto = "Cobranza " . $ciclo->getClave();
            $mensaje = "Fecha: " . date('Y-m-d') . "<br><br>
            Estimado <b>" . mb_strtoupper($alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM(), "UTF-8") . "</b>:<br><br>

            Por este medio te informamos que tienes un saldo pendiente de <b>$" . number_format($totalAdeudo, 2) . "</b>  (" . num2letras($totalAdeudo, 2) . "). 
            Por concepto de colegiaturas vencidas en tu carrera de <b>" . mb_strtoupper($oferta->getNombre_oferta(), "UTF-8") . " / " . mb_strtoupper($esp->getNombre_esp(), "UTF-8") . " / " . strtoupper($nombre_ori, "UTF-8") . "</b>.<br><br>

            Es necesario que estes al corriente en tus colegiaturas para evitar cualquier contratiempo
            al momento de presentar tus exámenes o evitar una suspensión por falta de pagos.<br><br>

            Si tienes alguna duda, aclaración o cuentas con tu comprobante de pago, puedes comunicarte a control escolar.<br>";
        } elseif ($ban == 1) {
            $asunto = 'Calificaciones ' . $ciclo->getClave();
            ;
            $mensaje = '
                        <table class="table" style="margin-bottom: 25px; border-collapse: collapse; border-spacing: 0;">
                            <thead style="background: gainsboro;border-width: 1px;border-color: black;border-style: solid;">
                                <tr>
                                    <td style="vertical-align: middle; color: black; position: relative; font-weight: bold; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">Matricula</td>
                                    <td class="normal" style="vertical-align: middle; color: black; position: relative; font-weight: normal; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">' . $alum->getMatricula() . '</td>
                                    <td style="vertical-align: middle; color: black; position: relative; font-weight: bold; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">Nombre</td>
                                    <td colspan="2" class="normal" style="vertical-align: middle; color: black; position: relative; font-weight: normal; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">' . mb_strtoupper($alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM(), "UTF-8") . '</td>
                                    <td style="vertical-align: middle; color: black; position: relative; font-weight: bold; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">Opci&oacute;n de pago</td>
                                    <td class="normal" style="vertical-align: middle; color: black; position: relative; font-weight: normal; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">' . $opcion . '</td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle; color: black; position: relative; font-weight: bold; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">Carrera</td>
                                    <td colspan="2" class="normal" style="vertical-align: middle; color: black; position: relative; font-weight: normal; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">' . $esp->getNombre_esp() . '</td>
                                    <td style="vertical-align: middle; color: black; position: relative; font-weight: bold; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">Orientaci&oacute;n</td>
                                    <td class="normal" style="vertical-align: middle; color: black; position: relative; font-weight: normal; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">' . $nombre_ori . '</td>
                                    <td style="vertical-align: middle; color: black; position: relative; font-weight: bold; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">Nivel</td>
                                    <td class="normal" style="vertical-align: middle; color: black; position: relative; font-weight: normal; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">' . $oferta->getNombre_oferta() . '</td>
                                </tr>
                                <tr>
                                    <td style="width: 80px; vertical-align: middle; color: black; position: relative; font-weight: bold; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">CLAVE</td>
                                    <td style="width: 100px; vertical-align: middle; color: black; position: relative; font-weight: bold; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">MATERIA</td>
                                    <td style="vertical-align: middle; color: black; position: relative; font-weight: bold; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">GRUPO</td>
                                    <td style="vertical-align: middle; color: black; position: relative; font-weight: bold; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">Asistencias</td>
                                    <td style="vertical-align: middle; color: black; position: relative; font-weight: bold; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">Calificaci&oacute;n</td>
                                    <td style="vertical-align: middle; color: black; position: relative; font-weight: bold; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">Extraordinario</td>
                                    <td style="vertical-align: middle; color: black; position: relative; font-weight: bold; font-size: 9px; text-transform: uppercase; padding: 8px; border: 1px solid gray;" valign="middle">Especial</td>
                                </tr>
                            </thead>
                            <tbody>' . $materia . '</tbody>
                        </table>
                        <p style="margin-bottom: 10px;">Para cualquier duda o aclaración favor de consultarlo en coordinación escolar.</p>';
        }
        $arrayData = array();
        $arrayData['Asunto'] = $asunto;
        $arrayData['Mensaje'] = $mensaje;
        $arrayData['Destinatarios'] = array();

        $Data = array();
        $Data['email'] = $alum->getEmail();
        //$Data['email']= "christian310332@gmail.com";
        $Data['name'] = $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM();
        array_push($arrayData['Destinatarios'], $Data);

        //Otros emails
        foreach ($_POST['emails'] as $v) {
            $Data['email'] = $v;
            $Data['name'] = '';
            array_push($arrayData['Destinatarios'], $Data);
        }
        $resp = $base->send_email($arrayData);
    }
}


