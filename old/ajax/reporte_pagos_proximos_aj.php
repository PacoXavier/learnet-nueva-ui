<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 
require_once('../estandares/cantidad_letra.php'); 


if (isset($_POST['action']) && $_POST['action'] == "buscarAlum") {
    $DaoAlumnos = new DaoAlumnos();
    foreach ($DaoAlumnos->buscarAlumnosAltasYBajas($_POST['buscar']) as $k => $v) {
        ?>
        <li id_alum="<?php echo $v->getId(); ?>"><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li>
        <?php
    }
}

if(isset($_POST['action']) && $_POST['action']=="filtro"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoRecargos= new DaoRecargos();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoUsuarios=new DaoUsuarios();
    $DaoCiclos= new DaoCiclos();
    $DaoGrupos= new DaoGrupos();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();

    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $ciclo=$DaoCiclos->getActual();
    
    $query = "SELECT Pagos_ciclo.Id_alum,Matricula,Nombre_ins,ApellidoP_ins,ApellidoM_ins,Concepto,Mensualidad,Fecha_pago,Descuento,ciclos_alum_ulm.Id_ofe_alum FROM Pagos_ciclo 
INNER JOIN inscripciones_ulm on Pagos_ciclo.Id_alum =  inscripciones_ulm.Id_ins 
  and inscripciones_ulm.Activo_alum = '1' and Pagos_ciclo.Pagado = 0 and Pagos_ciclo.DeadCargo IS NULL
INNER JOIN ciclos_alum_ulm on Pagos_ciclo.Id_ciclo_alum = ciclos_alum_ulm.Id_ciclo_alum
INNER JOIN ofertas_alumno on ciclos_alum_ulm.Id_ofe_alum = ofertas_alumno.Id_ofe_alum and ofertas_alumno.Activo_oferta='1'";  

    if($_POST['Id_alum']>0){
        $query=$query."AND Pagos_ciclo.Id_alum=".$_POST['Id_alum'];
    }
    

    if(strlen($_POST['fecha_ini'])>0 && $_POST['fecha_fin']==null){
        $query=$query." AND Fecha_pago>='".$_POST['fecha_ini']."'";
    }elseif(strlen($_POST['fecha_fin'])>0 && $_POST['fecha_ini']==null){
        $query=$query." AND Fecha_pago<='".$_POST['fecha_fin']."'";
    }elseif(strlen($_POST['fecha_ini'])>0 && strlen($_POST['fecha_fin'])>0){
        $query=$query." AND (Fecha_pago>='".$_POST['fecha_ini']."' AND Fecha_pago<='".$_POST['fecha_fin']."')";
    }
                            $count = 1;
                            $total_descuentos= 0;
                            $total_pagos= 0;
                            $query = $query." ORDER BY Fecha_pago";
                            $pagos_proximos_query=$base->advanced_query($query);

                             while ($row_consulta =  mysqli_fetch_array($pagos_proximos_query)) {
                                      ?>
                                              <tr id_alum="<?php echo $row_consulta['Id_alum']?>" id_ofe_alum="<?php echo $row_consulta['Id_ofe_alum']?>">
                                                <td width="30px;"><?php echo $count;?></td>
                                                <td><a href="alumno.php?id=<?php echo $row_consulta['Id_alum']?>"><?php echo $row_consulta['Matricula'] ?></a></td>
                                                <td style="width: 115px;"><a href="alumno.php?id=<?php echo $row_consulta['Id_alum']?>"><?php echo $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins'] ?></a></td>
                                                <td><?php echo $row_consulta['Concepto'] ?></td>
                                                <td><?php echo $row_consulta['Mensualidad'] ?></td>
                                                <td><?php echo $row_consulta['Descuento'] ?></td>
                                                <td><?php echo $row_consulta['Fecha_pago'] ?></td>

                                    
                                              </tr>
                                              <?php
                                              $count++;
                                              $total_pagos += $row_consulta['Mensualidad'];
                                              $total_descuentos += $row_consulta['Descuento'];

                                        }  
                                
                                ?>
                                <tr>
                                       <td colspan="4"></td>
                                       <td style="color:red" class="td-center"><b>$<?php echo number_format($total_pagos,2)?> </b></td>
                                       <td style="color:red" class="td-center"><b>$<?php echo number_format($total_descuentos,2)?> </b></td>
                                       <td style="color:red"><b>Total: $<?php echo number_format($total_pagos - $total_descuentos,2)?></b></td>
                                </tr>
        <?php
}





if(isset($_GET['action']) && $_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'MATRICULA');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'CONCEPTO');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'MENSUALIDAD');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'DESCUENTO');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'FECHA DE PAGO');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','H') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }

    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoRecargos= new DaoRecargos();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoUsuarios=new DaoUsuarios();
    
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
$query = "SELECT Pagos_ciclo.Id_alum,Matricula,Nombre_ins,ApellidoP_ins,ApellidoM_ins,Concepto,Mensualidad,Fecha_pago,Descuento,ciclos_alum_ulm.Id_ofe_alum FROM Pagos_ciclo 
INNER JOIN inscripciones_ulm on Pagos_ciclo.Id_alum =  inscripciones_ulm.Id_ins 
  and inscripciones_ulm.Activo_alum = '1' and Pagos_ciclo.Pagado = 0 and Pagos_ciclo.DeadCargo IS NULL
INNER JOIN ciclos_alum_ulm on Pagos_ciclo.Id_ciclo_alum = ciclos_alum_ulm.Id_ciclo_alum
INNER JOIN ofertas_alumno on ciclos_alum_ulm.Id_ofe_alum = ofertas_alumno.Id_ofe_alum and ofertas_alumno.Activo_oferta='1'";  

    if($_POST['Id_alum']>0){
        $query=$query."AND Pagos_ciclo.Id_alum=".$_POST['Id_alum'];
    }
    

    if(strlen($_POST['fecha_ini'])>0 && $_POST['fecha_fin']==null){
        $query=$query." AND Fecha_pago>='".$_POST['fecha_ini']."'";
    }elseif(strlen($_POST['fecha_fin'])>0 && $_POST['fecha_ini']==null){
        $query=$query." AND Fecha_pago<='".$_POST['fecha_fin']."'";
    }elseif(strlen($_POST['fecha_ini'])>0 && strlen($_POST['fecha_fin'])>0){
        $query=$query." AND (Fecha_pago>='".$_POST['fecha_ini']."' AND Fecha_pago<='".$_POST['fecha_fin']."')";
    }

    
        $count = 0;
        $total_descuentos= 0;
        $total_pagos= 0;
        $query = $query." ORDER BY Fecha_pago";
        echo "<script> console.log('Query: ".$query."'); </script>";
        $pagos_proximos_query=$base->advanced_query($query);

        while ($row_consulta =  mysqli_fetch_array($pagos_proximos_query)) {
          $count++;
          $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
          $objPHPExcel->getActiveSheet()->setCellValue("B$count", $row_consulta['Matricula']);
          $objPHPExcel->getActiveSheet()->setCellValue("C$count", $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins']);
          $objPHPExcel->getActiveSheet()->setCellValue("D$count", $row_consulta['Concepto']);
          $objPHPExcel->getActiveSheet()->setCellValue("E$count", $row_consulta['Mensualidad']);
          $objPHPExcel->getActiveSheet()->setCellValue("F$count", $row_consulta['Descuento']);
          $objPHPExcel->getActiveSheet()->setCellValue("G$count", $row_consulta['Fecha_pago']);


        }

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=PagosProximos-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}