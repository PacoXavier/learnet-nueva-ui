<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="buscar_int"){
    $DaoDocentes= new DaoDocentes();
    foreach($DaoDocentes->buscarDocente($_POST['buscar']) as $docen){
    ?>
       <a href="disponibilidad_docente.php?id=<?php echo $docen->getId()?>">
            <li><?php echo $docen->getClave_docen()." - ".$docen->getNombre_docen()." ".$docen->getApellidoP_docen()." ".$docen->getApellidoM_docen()?></li>
       </a>
   <?php
    }

}

if($_POST['action']=="save_disponibilidad"){
   $DaoDisponibilidadDocente= new DaoDisponibilidadDocente();
   $DaoDisponibilidadDocente->deleteDisponibilidadDocenteCiclo($_POST['Id_docente'], $_POST['Id_ciclo']);
   
    foreach($_POST['Disponibilidad'] as $k=>$v){
        $DisponibilidadDocente= new DisponibilidadDocente();
        $DisponibilidadDocente->setId_ciclo($_POST['Id_ciclo']);
        $DisponibilidadDocente->setId_hora($v['hora']);
        $DisponibilidadDocente->setId_docente($_POST['Id_docente']);
        $DisponibilidadDocente->setLunes($v['lunes']);
        $DisponibilidadDocente->setMartes($v['martes']);
        $DisponibilidadDocente->setMiercoles($v['miercoles']);
        $DisponibilidadDocente->setJueves($v['jueves']);
        $DisponibilidadDocente->setViernes($v['viernes']);
        $DisponibilidadDocente->setSabado($v['sabado']);
        $DisponibilidadDocente->setDomingo($v['domingo']);
        $DaoDisponibilidadDocente->add($DisponibilidadDocente);
    }
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $DaoCiclos= new DaoCiclos();
    
    $ciclo=$DaoCiclos->show($_POST['Id_ciclo']);
    $doc=$DaoDocentes->show($_POST['Id_docente'] );
    //Historial
    $TextoHistorial = "Captura disponibilidad ".$ciclo->getClave()." para " . $doc->getNombre_docen(). " " . $doc->getApellidoP_docen() . " " . $doc->getApellidoM_docen();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Disponibilidad docente");

}

if($_POST['action']=="mostrar_disponibilidad_ciclo"){
    $base= new base();
    $DaoDocentes= new DaoDocentes();
    $DaoUsuarios= new DaoUsuarios();
    $DaoDiasPlantel = new DaoDiasPlantel();
    $DaoCiclos= new DaoCiclos();
    $docente = $DaoDocentes->show($_POST['Id_docente']);
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
  ?>
    <h2>Sesiones disponibles para el profesor</h2>
    <h2 style="font-size: 15px;"><?php echo $docente->getClave_docen()."  - ".$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen()?></h2>
    <h2 style="font-size: 15px;">Ciclo<br><select id="ciclo" onchange="mostrar_disponibilidad_ciclo(this)">
        <option value="0">Selecciona el ciclo</option>
        <?php
          foreach($DaoCiclos->getCiclosFuturos() as $ciclo){
         ?>
              <option value="<?php echo $ciclo->getId() ?>" <?php if($_POST['Id_ciclo']==$ciclo->getId() ){?> selected="selected" <?php }?> ><?php echo $ciclo->getClave() ?></option>
          <?php
          }
          ?>
        </select>
    </h2>
    <p style="font-size:14px;"><input type="checkbox" id="seleccionar-todo" onchange="seleccionarCheckbox()"/> Seleccionar todo</p>
    <span class="linea"></span>
    <table id="list_usu">
        <thead>
            <tr>
                <td>Sesiones</td>
                <?php
                foreach ($DaoDiasPlantel->getJoinDiasPlantel($usu->getId_plantel()) as $dia) {
                    ?>
                    <td><?php echo $dia['Nombre'] ?></td>
                    <?php
                }
                ?>
            </tr>
        </thead>
        <tbody>

            <?php
                $query  = "SELECT * FROM Horas 
                              LEFT JOIN (
                                         SELECT Lunes,Martes,Miercoles,Jueves,Viernes,Sabado,Domingo,Id_docente,Id_hora as Id_hora_dispo,Id_ciclo 
                                                 FROM Disponibilidad_docente 
                                         WHERE Id_docente=".$_POST['Id_docente']." AND Id_ciclo=".$_POST['Id_ciclo']."
                                        ) 
                           AS Disponibilidad ON Horas.Id_hora= Disponibilidad.Id_hora_dispo ORDER BY Id_hora ASC ";
                foreach($base->advanced_query($query) as $row_Horas){
                ?>
                       <tr id_hora="<?php echo $row_Horas['Id_hora']?>">
                            <td><?php echo $row_Horas['Texto_hora']?></td>
                            <?php
                            foreach ($DaoDiasPlantel->getJoinDiasPlantel($usu->getId_plantel()) as $dia) {
                                $checked="";
                                if($dia['Id_dia']==1 && $row_Horas['Lunes']==1){
                                  $checked='checked="checked"'; 
                                }elseif($dia['Id_dia']==2 && $row_Horas['Martes']==1){
                                   $checked='checked="checked"';  
                                }elseif($dia['Id_dia']==3 && $row_Horas['Miercoles']==1){
                                   $checked='checked="checked"';  
                                }elseif($dia['Id_dia']==4 && $row_Horas['Jueves']==1){
                                   $checked='checked="checked"';  
                                }elseif($dia['Id_dia']==5 && $row_Horas['Viernes']==1){
                                   $checked='checked="checked"';  
                                }elseif($dia['Id_dia']==6 && $row_Horas['Sabado']==1){
                                   $checked='checked="checked"';  
                                }elseif($dia['Id_dia']==7 && $row_Horas['Domingo']==1){
                                   $checked='checked="checked"';  
                                }
                                ?>
                                <td><input type="checkbox" class="<?php echo $dia['Abreviacion']?> checkbox-table" <?php echo $checked?> /></td>    
                                <?php
                            }
                            ?>
                                </tr>
                            <?php
                }
                ?>
        </tbody>
    </table>
    <?php
}