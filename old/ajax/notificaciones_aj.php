<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="mostrar_box_notificaciones"){
    $DaoTiposUsuarios= new DaoTiposUsuarios();
    $DaoNotificaciones= new DaoNotificaciones();
    $titulo="";
    $DateInit="";
    $tipoNot="";
    $Texto="";
    $Tipos_usu="";
    if($_POST['Id_not']>0){
        $Not=$DaoNotificaciones->show($_POST['Id_not']);
        $titulo=$Not->getTitulo();
        $DateInit=$Not->getDateInit();
        $tipoNot=$Not->getTipo();
        $Texto=$Not->getTexto();
        $Tipos_usu=$Not->getTipos_usu();
        $IdsTipos_usu=array();
        
        if(strlen($Tipos_usu)>0){
           $resp=explode(",", $Not->getTipos_usu());
           foreach($resp as $v){
               $IdsTipos_usu[$v]=1;
           }
        }
    }
    
    ?>
    <div class="box-emergente-form">
      <h1>Notificaci&oacute;n</h1>
      <p>T&iacute;tulo<br><input type="text"  id="titulo" value="<?php echo $titulo?>"/></p>
      <p>Fecha de inicio<br><input type="date" id="fecha_ini" value="<?php echo $DateInit?>"/></p>
      <p>Tipo<br>
          <select id="Tipo" onchange="mostrar_tipos_usu()">
        <option value="0"></option>
        <option value="1" <?php if($tipoNot=="usu"){?> selected="selected" <?php } ?>>Usuarios</option>
        <option value="2" <?php if($tipoNot=="alumn"){?> selected="selected" <?php } ?>>Alumnos</option>
        <option value="3" <?php if($tipoNot=="docen"){?> selected="selected" <?php } ?>>Docentes</option>
        </select>
      </p>
      <div id="box-tipo_usu">
          <?php 
          
            if($tipoNot=="usu"){
               ?>
               <ul id="list-tipo-usu">
               <?php
               foreach($DaoTiposUsuarios->getTiposUsuarios() as $tipo){
                   ?> 
                   <li><input type="checkbox" value="<?php echo $tipo->getId()?>" <?php if(isset($IdsTipos_usu[$tipo->getId()])){?> checked="checked" <?php } ?>><?php echo $tipo->getNombre_tipo()?></li>
               <?php
               }
               ?>
               </ul> 
          <?php
            }
          ?>
      </div>
      <p>Texto<br><textarea id="texto"><?php echo $Texto?></textarea></p>
      <button onclick="add_notificacion(<?php echo $_POST['Id_not'];?>)">Guardar</button>
      <button onclick="ocultar_error_layer()">Cancelar</button>
</div>   
<?php
}

if($_POST['action']=="mostrar_tipos_usu"){
    $DaoTiposUsuarios= new DaoTiposUsuarios();
    $DaoOfertas= new DaoOfertas();
    if($_POST['Tipo']==1){
       ?>
       <ul id="list-tipo-usu">
       <?php
       foreach($DaoTiposUsuarios->getTiposUsuarios() as $tipo){
           ?> 
            <li><input type="checkbox" value="<?php echo $tipo->getId()?>"><?php echo $tipo->getNombre_tipo();?></li>
       <?php
       }
       ?>
       </ul>
       <?php
    }else if($_POST['Tipo']==2){
        ?>
	<p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso();update_grados_ofe()">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
      <?php
    }elseif($_POST['Tipo']==3){
        ?>
	<p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso">
              <option value="0"></option>
            </select>
        </p>
      <?php
    }
}


if($_POST['action']=="add_notificacion"){
    
    $base= new base();
    $DaoDocentes= new DaoDocentes();
    $DaoAlumnos= new DaoAlumnos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoNotificaciones= new DaoNotificaciones();
    $DaoCiclos= new DaoCiclos();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    if($_POST['Tipo']==1){
       $Tipo="usu";
    }elseif($_POST['Tipo']==2){
       $Tipo="alumn";
    }elseif($_POST['Tipo']==3){
      $Tipo="docen";  
    }
    
    $Tipos_usuarios="";
    $IdsTipos_usu=array();
    if(isset($_POST['Tipos_usuarios'])){
       foreach($_POST['Tipos_usuarios'] as $k=>$v){
           array_push($IdsTipos_usu,$v['Id_tipo']);
       }
       $Tipos_usuarios=implode(",",$IdsTipos_usu);
    }
    
    if($_POST['Id_not']>0){
        $Notificaciones=$DaoNotificaciones->show($_POST['Id_not']);
        $Notificaciones->setTitulo($_POST['titulo']);
        $Notificaciones->setTexto($_POST['texto']);
        $Notificaciones->setDateInit($_POST['fecha_ini']);
        $Notificaciones->setTipo($Tipo);
        $Notificaciones->setTipos_usu($Tipos_usuarios);
        $DaoNotificaciones->update($Notificaciones);  
        $Id_not=$_POST['Id_not'];
    }else{
        $Notificaciones= new Notificaciones();
        $Notificaciones->setDateCreated(date('Y-m-d'));
        $Notificaciones->setDateInit($_POST['fecha_ini']);
        $Notificaciones->setId_plantel($usu->getId_plantel());
        $Notificaciones->setId_usu($_COOKIE['admin/Id_usu']);
        $Notificaciones->setTitulo($_POST['titulo']);
        $Notificaciones->setTexto($_POST['texto']);
        $Notificaciones->setTipo($Tipo);
        $Notificaciones->setTipos_usu($Tipos_usuarios);
        $Notificaciones->setActivo(1);
        $Id_not=$DaoNotificaciones->add($Notificaciones);
    }

    $DaoNotificacionesTipoUsuario= new DaoNotificacionesTipoUsuario();
    $DaoNotificacionesTipoUsuario->deleteNotificaciones($Id_not);
     
    if($_POST['Tipo']==1){
        
       foreach($_POST['Tipos_usuarios'] as $k=>$v){
           foreach($DaoUsuarios->getUsuariosTipo($v['Id_tipo']) as $x){
               $NotificacionesTipoUsuario= new NotificacionesTipoUsuario();
               $NotificacionesTipoUsuario->setIdRel($x->getId());
               $NotificacionesTipoUsuario->setTipoRel("usu");
               $NotificacionesTipoUsuario->setId_not($Id_not);
               $DaoNotificacionesTipoUsuario->add($NotificacionesTipoUsuario);
           }
       }
       
    }elseif($_POST['Tipo']==2){
            $querySearch="";
            if($_POST['Id_ofe']>0){
                $querySearch=$querySearch." AND Id_ofe=".$_POST['Id_ofe'];
            }
            if($_POST['Id_esp']>0){
                $querySearch=$querySearch." AND Id_esp=".$_POST['Id_esp'];
            }
            if($_POST['Id_ori']>0){
                $querySearch=$querySearch." AND Id_ori=".$_POST['Id_ori'];
            }
        foreach($DaoAlumnos->getAlumnos(null,$querySearch,null,null) as $k=>$v){
               $NotificacionesTipoUsuario= new NotificacionesTipoUsuario();
               $NotificacionesTipoUsuario->setIdRel($v['Id_ins']);
               $NotificacionesTipoUsuario->setTipoRel("alum");
               $NotificacionesTipoUsuario->setId_not($Id_not);
               $DaoNotificacionesTipoUsuario->add($NotificacionesTipoUsuario);
       }

    }elseif($_POST['Tipo']==3){
        
           $querySearch="";
            if($_POST['Id_ofe']>0 || $_POST['Id_esp']>0){
                $querySearch="";
                if($_POST['Id_ofe']>0){
                    $querySearch=$querySearch." AND especialidades_ulm.Id_ofe=".$_POST['Id_ofe'];
                }
                if($_POST['Id_esp']>0){
                    $querySearch=$querySearch." AND especialidades_ulm.Id_esp=".$_POST['Id_esp'];
                }
                $DaoCiclos= new DaoCiclos();
                $ciclo=$DaoCiclos->getActual();
                if($ciclo->getId()>0){
                    $query = "
                    SELECT 
                        Docentes.Id_docen,Docentes.Nombre_docen,Docentes.ApellidoP_docen,Docentes.ApellidoP_docen,
                        Horario_docente.Id_grupo,Horario_docente.Id_ciclo,
                        Grupos.Clave,Grupos.Id_mat_esp,
                        Materias_especialidades.Id_esp,
                        especialidades_ulm.Id_ofe,
                        ofertas_ulm.Nombre_oferta,ofertas_ulm.Id_oferta
                    FROM Docentes 
                    JOIN Horario_docente ON Docentes.Id_docen=Horario_docente.Id_docente
                    JOIN Grupos ON Horario_docente.Id_grupo=Grupos.Id_grupo
                    JOIN Materias_especialidades ON  Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
                    JOIN especialidades_ulm ON Materias_especialidades.Id_esp=especialidades_ulm.Id_esp
                    JOIN ofertas_ulm ON especialidades_ulm.Id_ofe=ofertas_ulm.Id_oferta
                    WHERE  Horario_docente.Id_ciclo=".$ciclo->getId()." ".$querySearch." GROUP BY Id_oferta, Id_docente ORDER BY Id_oferta ASC";
                     $docentes=$base->advanced_query($query);
                     $row_docentes = $docentes->fetch_assoc();
                     $totalRows_docentes= $docentes->num_rows;
                     if($totalRows_docentes>0){
                         do{
                               $NotificacionesTipoUsuario= new NotificacionesTipoUsuario();
                               $NotificacionesTipoUsuario->setIdRel($row_docentes['Id_docen']);
                               $NotificacionesTipoUsuario->setTipoRel("docen");
                               $NotificacionesTipoUsuario->setId_not($Id_not);
                               $DaoNotificacionesTipoUsuario->add($NotificacionesTipoUsuario);
                         }while($row_docentes = $docentes->fetch_assoc());
                     }          
                }
            }else{
                    foreach($DaoDocentes->showAll() as $docen){
                       $NotificacionesTipoUsuario= new NotificacionesTipoUsuario();
                       $NotificacionesTipoUsuario->setIdRel($docen->getId());
                       $NotificacionesTipoUsuario->setTipoRel("docen");
                       $NotificacionesTipoUsuario->setId_not($Id_not);
                       $DaoNotificacionesTipoUsuario->add($NotificacionesTipoUsuario);
                   }  
            }
    }
    update_page();
}



if($_POST['action']=="delete_not"){
    $DaoNotificacionesTipoUsuario= new DaoNotificacionesTipoUsuario();
    $DaoNotificaciones= new DaoNotificaciones();
    
    $DaoNotificaciones->delete($_POST['Id_not']);	
    $DaoNotificacionesTipoUsuario->deleteNotificaciones($_POST['Id_not']);
    update_page();
}

function update_page(){
    $DaoNotificaciones= new DaoNotificaciones();
    $DaoNotificacionesTipoUsuario= new DaoNotificacionesTipoUsuario();
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $DaoAlumnos= new DaoAlumnos();
    $count=1;
    foreach ($DaoNotificaciones->getNotificaciones() as $k => $v) {

        if($v->getTipo()=="usu"){
         $tipo="Usuarios";  
        }elseif($v->getTipo()=="docen"){
            $tipo="Docentes"; 
        }elseif($v->getTipo()=="alumn"){
           $tipo="Alumnos";  
        }
        $usu_not=$DaoUsuarios->show($v->getId_usu());
?>
        <tr>
            <td><?php echo $count;?></td>
            <td><?php echo $v->getTitulo() ?></td>
            <td style="width: 150px;"><?php echo $v->getTexto() ?></td>
            <td><?php echo $tipo ?></td>
            <td><?php echo $v->getDateCreated() ?></td>
            <td><?php echo $v->getDateInit();?></td>
            <td><?php echo $usu_not->getNombre_usu()." ".$usu_not->getApellidoP_usu()." ".$usu_not->getApellidoM_usu();?></td>
            <td style="width: 110px;">
                <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                <div class="box-buttom">
                    <button onclick="delete_not(<?php echo $v->getId() ?>)">Eliminar</button><br>
                    <button onclick="mostrar_box_notificaciones(<?php echo $v->getId() ?>)">Editar</button><br>
                </div>
            </td>
        </tr>
        <?php
        $count++;
    }

}


