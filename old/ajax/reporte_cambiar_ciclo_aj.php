<?php
error_log(E_ALL);
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
require_once('../require_daos.php'); 


if ($_POST['action'] == "filtro") {
    $Id_ciclo = 13;
    $base = new base();
    $DaoOfertas = new DaoOfertas();
    $DaoAlumnos = new DaoAlumnos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoCiclos = new DaoCiclos();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoUsuarios = new DaoUsuarios();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $queryAlumnos = "";

    if ($_POST['Id_ofe'] > 0) {
        $queryAlumnos = $queryAlumnos . " AND Id_ofe=" . $_POST['Id_ofe'];
    }
    if ($_POST['Id_esp'] > 0) {
        $queryAlumnos = $queryAlumnos . " AND Id_esp=" . $_POST['Id_esp'];
    }
    if ($_POST['Id_ori'] > 0) {
        $queryAlumnos = $queryAlumnos . " AND Id_ori=" . $_POST['Id_ori'];
    }
    


    $count = 1;
    //Se mostran alumnos duplicados si es que tienen mas dos ofertas
    $query = "SELECT * FROM inscripciones_ulm 
     JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
     JOIN ofertas_ulm ON ofertas_alumno.Id_ofe=ofertas_ulm.Id_oferta
     WHERE tipo=1  AND inscripciones_ulm.Id_plantel=" . $usu->getId_plantel() . "
            AND ofertas_alumno.Activo_oferta=1
           AND Baja_ofe IS NULL 
           AND FechaCapturaEgreso IS NULL
           AND IdCicloEgreso IS NULL 
           AND IdUsuEgreso IS NULL
           AND ofertas_ulm.Tipo_oferta=1
           " . $queryAlumnos . "
           ORDER BY Id_ins";
    $inscripciones_ulm = $base->advanced_query($query);
    $row_inscripciones_ulm = $inscripciones_ulm->fetch_assoc();
    $totalRows_inscripciones_ulm = $inscripciones_ulm->num_rows;
    if ($totalRows_inscripciones_ulm > 0) {
        do {
            $alum = $DaoAlumnos->show($row_inscripciones_ulm['Id_ins']);
            $nombre_ori = "";
            $oferta = $DaoOfertas->show($row_inscripciones_ulm['Id_ofe']);
            $esp = $DaoEspecialidades->show($row_inscripciones_ulm['Id_esp']);
            if ($row_inscripciones_ulm['Id_ori'] > 0) {
                $ori = $DaoOrientaciones->show($row_inscripciones_ulm['Id_ori']);
                $nombre_ori = $ori->getNombre();
            }

            $opcion = "Plan por materias";
            if ($row_inscripciones_ulm['Opcion_pago'] == 2) {
                $opcion = "Plan completo";
            }

            $cicloAlumno = $DaoCiclosAlumno->getLastCicloOferta($row_inscripciones_ulm['Id_ofe_alum']);
            $ciclo = $DaoCiclos->show($cicloAlumno->getId_ciclo());
            
            $Id_ciclo=$ciclo->getId();
            if ($_POST['Id_ciclo'] > 0) {
                $Id_ciclo=$_POST['Id_ciclo'];
            }
            if($ciclo->getId()==$Id_ciclo){
            ?>
            <tr id_alum="<?php echo $alum->getId(); ?>" id_ofe_alum="<?php echo $row_inscripciones_ulm['Id_ofe_alum']; ?>">
                <td><?php echo $count; ?></td>
                <td><?php echo $alum->getMatricula() ?></td>
                <td style="width: 115px;"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
                <td><?php echo $oferta->getNombre_oferta(); ?></td>
                <td><?php echo $esp->getNombre_esp(); ?></td>
                <td class="td-center"><?php echo $nombre_ori; ?></td>
                <td class="td-center"><?php echo $opcion; ?></td>
                <td class="td-center"><span class="ciclo-actual"><?php echo $ciclo->getClave() ?></span></td>
                <td style="text-align:center;"><input type="checkbox"> </td>
                <td class="td-center"><span>Sin cambio</span></td>
            </tr>
            <?php
            }
            $count++;
        } while ($row_inscripciones_ulm = $inscripciones_ulm->fetch_assoc());
    }
}

if ($_POST['action'] == "cambiar_ciclo") {
    $DaoAlumnos = new DaoAlumnos();
    $DaoUsuarios = new DaoUsuarios();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoCiclos = new DaoCiclos();
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoRecargos = new DaoRecargos();
    $DaoPagos = new DaoPagos();
    $DaoMateriasCicloAlumno = new DaoMateriasCicloAlumno();
    $DaoPagosAdelantados = new DaoPagosAdelantados();
    $DaoOfertas = new DaoOfertas();
    $base = new base();
    $DaoGrados = new DaoGrados();
    $DaoMaterias = new DaoMaterias();
    $DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $params=$DaoParametrosPlantel->getParametrosPlantelArray();
    
    $Id_ciclo_actual = $_POST['Id_ciclo_actual'];
    $Id_ciclo_nuevo = $_POST['Id_ciclo_nuevo'];


    $cicloNuevo = $DaoCiclos->show($Id_ciclo_nuevo);
    $alum = $DaoAlumnos->show($_POST['Id_alum']);

    //Si ya curso todas sus materias
    //Lo egresamos
    $ofertaAlumno = $DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $statusOferta = $DaoAlumnos->getStatusOferta($_POST['Id_ofe_alum']);
    if($statusOferta['status']==1) {
        $ofertaAlumno->setFechaCapturaEgreso(date('Y-m-d H:i:s'));
        $ofertaAlumno->setIdCicloEgreso($Id_ciclo_actual);
        $ofertaAlumno->setIdUsuEgreso($_COOKIE['admin/Id_usu']);
        $DaoOfertasAlumno->update($ofertaAlumno);
    } else {

        
            //Ciclo actual
            $cicloOferta = $DaoCiclosAlumno->getCicloOferta($_POST['Id_ofe_alum'], $Id_ciclo_actual);
            if ($cicloOferta->getId() > 0) {
                //Los alumnos que son nuevos no entran aqui porque no tienen el ciclo actual, mas bien tienen al que se avanzara
                //Seleccionamos el grado actual
                $grado = $DaoGrados->show($cicloOferta->getId_grado());
                
                $Id_grado_ofe = $grado->getId();

                //Verificamos si no existe el ciclo nuevo
                $cicloNuevoOferta = $DaoCiclosAlumno->getCicloOferta($_POST['Id_ofe_alum'], $Id_ciclo_nuevo);
                if ($cicloNuevoOferta->getId() == 0) {

                    //Obtenemos los datos del ciclo
                    $fecha_pago = $cicloNuevo->getFecha_ini();
                    $anio = date("Y", strtotime($cicloNuevo->getFecha_ini()));
                    $mes = date("m", strtotime($cicloNuevo->getFecha_ini()));

                    $esp = $DaoEspecialidades->show($ofertaAlumno->getId_esp());
                    $SiguienteGrado = $grado->getGrado() + 1;

                    //Verificamos si existe el nuevo grado
                    $query = "SELECT * FROM grados_ulm WHERE Id_esp=" . $ofertaAlumno->getId_esp() . " AND Grado=" . $SiguienteGrado;
                    foreach ($DaoGrados->advanced_query($query) as $gradoOferta) {
                        $Id_grado_ofe = $gradoOferta->getId();
                    }

                    //Insertamos el nuevo ciclo del alumno
                    $CiclosAlumno = new CiclosAlumno();
                    $CiclosAlumno->setId_ciclo($Id_ciclo_nuevo);
                    $CiclosAlumno->setId_grado($Id_grado_ofe);
                    $CiclosAlumno->setId_ofe_alum($_POST['Id_ofe_alum']);
                    $Id_ciclo_alum = $DaoCiclosAlumno->add($CiclosAlumno);

                    $ciclos = 0;
                    if ($esp->getTipo_insc() == 1) {
                        if ($esp->getTipo_ciclos() == 1) {
                            $ciclos = 2;
                        } elseif ($esp->getTipo_ciclos() == 2) {
                            $ciclos = 6;
                        } elseif ($esp->getTipo_ciclos() == 3) {
                            $ciclos = 12;
                        } elseif ($esp->getTipo_ciclos() == 4) {
                            $ciclos = 3;
                        }
                    } elseif ($esp->getTipo_insc() == 2) {

                        if ($esp->getTipo_ciclos() == 1) {
                            $ciclos = 1;
                        } elseif ($esp->getTipo_ciclos() == 2) {
                            $ciclos = 2;
                        } elseif ($esp->getTipo_ciclos() == 3) {
                            $ciclos = 4;
                        } elseif ($esp->getTipo_ciclos() == 4) {
                            $ciclos = 1;
                        }
                    }
                    $grado_actual = $SiguienteGrado;
                    $modulo = $grado_actual % $ciclos;

                    //Tipo_insc 1= anual, 2 cuatrimenstral, 3 unica
                    //Tipo_ciclos (Semestre=1, Bimestre=2, Mensual=3, Cuatrimestre=4)
                    //Eliminar el if despues de que se carguen todos los alumnos
                    //Tipo de inscripcion 1
                    //Tipo ciclo es 1(semestre) o 2(Bimestre) o 3(Mensual) o 4(Cuatrimestre)
                    //modulo cera simepre 1
                    //Tipo de inscripcion 2
                    //Tipo ciclo es 1(semestre) o 4(Cuatrismetrs)
                    //modulo cera simepre 0
                    //Tipo de inscripcion 2
                    //Tipo ciclo es 2(Bimestre) o 3(Mensual)
                    //modulo cera simepre 1

                    if (($esp->getTipo_insc() == 2 && (($esp->getTipo_ciclos() == 1 || $esp->getTipo_ciclos() == 4) && $modulo == 0) || (($esp->getTipo_ciclos() == 2 || $esp->getTipo_ciclos() == 3) && $modulo == 1)) || ($esp->getTipo_insc() == 1 && $modulo == 1) || ($esp->getTipo_insc() == 3 && $grado_actual == 1)) {
                        //Insertamos el cargo de la inscripcion
                        $PagosCiclo = new PagosCiclo();
                        $PagosCiclo->setConcepto("Inscripción");
                        $PagosCiclo->setFecha_pago($base->get_day_pago($fecha_pago, $Id_ciclo_nuevo));
                        $PagosCiclo->setMensualidad($esp->getInscripcion_curso());
                        $PagosCiclo->setId_ciclo_alum($Id_ciclo_alum);
                        $PagosCiclo->setId_alum($_POST['Id_alum']);
                        $PagosCiclo->setTipo_pago("pago");
                        $PagosCiclo->setTipoRel("alum");
                        $PagosCiclo->setIdRel($_POST['Id_alum']);
                        $DaoPagosCiclo->add($PagosCiclo);
                    }

                    if ($ofertaAlumno->getOpcionPago() == 2) {
                        //Insertamos las mensualidades
                        for ($i = 1; $i <= $esp->getNum_pagos(); $i++) {
                            //Se generan los cargos  apartir de la fecha de inicio del ciclo
                            $d=1;
                            if(isset($params['DiaInicioMensualidades']) && strlen($params['DiaInicioMensualidades'])>0){
                               $d=$params['DiaInicioMensualidades'];
                            }
                            $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes + $i, $d, $anio));
                        
                            $Precio_curso = ($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos();
                            $PagosCiclo = new PagosCiclo();
                            $PagosCiclo->setConcepto("Mensualidad");
                            $PagosCiclo->setFecha_pago($base->get_day_pago($fecha_pago, $Id_ciclo_nuevo));
                            $PagosCiclo->setMensualidad($Precio_curso);
                            $PagosCiclo->setId_ciclo_alum($Id_ciclo_alum);
                            $PagosCiclo->setId_alum($_POST['Id_alum']);
                            $PagosCiclo->setTipo_pago("pago");
                            $PagosCiclo->setTipoRel("alum");
                            $PagosCiclo->setIdRel($_POST['Id_alum']);
                            $DaoPagosCiclo->add($PagosCiclo);
                        }

                        $mat_rep = 0;
                        $mat_reprobadas = array();
                        //Seleccionar las materias del ciclo anterior para comprobar si las paso
                        $query = "SELECT * FROM materias_ciclo_ulm WHERE Id_ciclo_alum=" . $cicloOferta->getId();
                        foreach ($DaoMateriasCicloAlumno->advanced_query($query) as $MateriaCicloAlumnoAnterior) {
                            $mat_esp = $DaoMateriasEspecialidad->show($MateriaCicloAlumnoAnterior->getId_mat_esp());
                            $mat = $DaoMaterias->show($mat_esp->getId_mat());
                            
                            //Hacen falta mas convinaciones de letras
                            if(is_numeric($MateriaCicloAlumnoAnterior->getCalTotalParciales())){
                               $CalTotalParciales=$MateriaCicloAlumnoAnterior->getCalTotalParciales();
                            }else{
                               $CalTotalParciales=mb_strtolower ($MateriaCicloAlumnoAnterior->getCalTotalParciales(),"UTF-8");
                            }
                            
                            if ($CalTotalParciales >= $mat->getPromedio_min() || $CalTotalParciales == "cp" || $CalTotalParciales == "ac" || $MateriaCicloAlumnoAnterior->getCalExtraordinario() >= $mat->getPromedio_min() || $MateriaCicloAlumnoAnterior->getCalEspecial() >= $mat->getPromedio_min()) {
                                
                            } else {
                                //Se inserta la misma materia porque reprobo
                                $MateriasCicloAlumno = new MateriasCicloAlumno();
                                $MateriasCicloAlumno->setId_ciclo_alum($Id_ciclo_alum);
                                $MateriasCicloAlumno->setId_mat_esp($MateriaCicloAlumnoAnterior->getId_mat_esp());
                                $MateriasCicloAlumno->setId_ori($MateriaCicloAlumnoAnterior->getId_ori());
                                $MateriasCicloAlumno->setTipo(1);
                                $MateriasCicloAlumno->setActivo(1);
                                $MateriasCicloAlumno->setId_alum($_POST['Id_alum']);
                                $DaoMateriasCicloAlumno->add($MateriasCicloAlumno);

                                //Insertamos el Id de la materia reprobada
                                array_push($mat_reprobadas, $MateriaCicloAlumnoAnterior->getId_mat_esp());
                                
                                //Guardar el id de la materia consecutiva para no insertarla
                                $query_prerequisitos = "
                                        SELECT * FROM prerequisitos_ulm 
                                        JOIN Materias_especialidades ON prerequisitos_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp 
                                        JOIN materias_uml ON Materias_especialidades.Id_mat=materias_uml.Id_mat
                                        WHERE Id_mat_esp_pre=" . $MateriaCicloAlumnoAnterior->getId_mat_esp();
                                $prerequisitos = $base->advanced_query($query);
                                $row_prerequisitos = $prerequisitos->fetch_assoc();
                                $totalRows_prerequisitos = $prerequisitos->num_rows;
                                if ($totalRows_prerequisitos > 0) {
                                    array_push($mat_reprobadas, $row_prerequisitos['Id_mat_esp']);
                                }
                                $mat_rep++;
                            }
                        }

                        //Obtener las materias que cursara en el ciclo
                        $query = "SELECT * FROM Materias_especialidades
                            JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat 
                            WHERE Id_esp=" . $ofertaAlumno->getId_esp() . " 
                                  AND Grado_mat=" . $Id_grado_ofe . " 
                                  AND Activo_mat_esp=1 
                                  AND Activo_mat=1";
                        $totalRows_materias_ciclos_ulm = $base->getTotalRows($query);
                        $limiteDeMatInsertar = $totalRows_materias_ciclos_ulm - $mat_rep;
                        $CountMatInsertadas = 0;
                        foreach ($base->advanced_query($query) as $k => $v) {
                            $materiCiclo = $DaoMateriasCicloAlumno->getMateriaCicloAlumno($Id_ciclo_alum, $_POST['Id_alum'], $v['Id_mat_esp']);

                            if ($materiCiclo->getId() == 0) {
                                if (!in_array($v['Id_mat_esp'], $mat_reprobadas) && $CountMatInsertadas < $limiteDeMatInsertar) {
                                    //Hace falta la validacion de que si existe esa materia en semestres atras y la paso no la inserte
                                    
                                    $MateriasCicloAlumno = new MateriasCicloAlumno();
                                    $MateriasCicloAlumno->setId_ciclo_alum($Id_ciclo_alum);
                                    $MateriasCicloAlumno->setId_mat_esp($v['Id_mat_esp']);
                                    $MateriasCicloAlumno->setTipo(1);
                                    $MateriasCicloAlumno->setActivo(1);
                                    $MateriasCicloAlumno->setId_alum($_POST['Id_alum']);
                                    $DaoMateriasCicloAlumno->add($MateriasCicloAlumno);
                                    $CountMatInsertadas++;
                                }
                            }
                        }
                    } else {
                        //Por materias
                        //Seleccionar las materias del ciclo anterior para comprobar si las paso
                        $query = "SELECT * FROM materias_ciclo_ulm WHERE Id_ciclo_alum=" . $cicloOferta->getId();
                        foreach ($DaoMateriasCicloAlumno->advanced_query($query) as $materiasCiclo) {
                            $mat_esp = $DaoMateriasEspecialidad->show($materiasCiclo->getId_mat_esp());
                            $mat = $DaoMaterias->show($mat_esp->getId_mat());
                            //Verificar si es texto o numero
                            if(is_numeric($materiasCiclo->getCalTotalParciales())){
                               $CalTotalParciales=$materiasCiclo->getCalTotalParciales();
                            }else{
                               $CalTotalParciales=mb_strtolower ($materiasCiclo->getCalTotalParciales(),"UTF-8");
                            }
                            if ($CalTotalParciales >= $mat->getPromedio_min() || $CalTotalParciales == "cp" || $CalTotalParciales == "ac" || $materiasCiclo->getCalExtraordinario() >= $mat->getPromedio_min() || $materiasCiclo->getCalEspecial() >= $mat->getPromedio_min()) {
                                
                            } else {
                                //Se inserta la misma porque reprobo
                                $MateriasCicloAlumno = new MateriasCicloAlumno();
                                $MateriasCicloAlumno->setId_ciclo_alum($Id_ciclo_alum);
                                $MateriasCicloAlumno->setId_mat_esp($materiasCiclo->getId_mat_esp());
                                $MateriasCicloAlumno->setId_ori($materiasCiclo->getId_ori());
                                $MateriasCicloAlumno->setTipo(1);
                                $MateriasCicloAlumno->setActivo(1);
                                $MateriasCicloAlumno->setId_alum($_POST['Id_alum']);
                                $DaoMateriasCicloAlumno->add($MateriasCicloAlumno);
                            }
                        }
                    }
                }
            }
    }
    echo $cicloNuevo->getClave();
}
