<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if (isset( $_GET['action']) && $_GET['action'] == "upload_image") {
$base= new base();
$DaoAlumnos= new DaoAlumnos();
$alum = $DaoAlumnos->show($_GET['Id_alum']);
    
  if (count($_FILES) > 0) {
    $type = substr($_FILES["file"]["name"], strpos($_FILES["file"]["name"], ".") + 1);
    if (($type == "gif" || $type == "jpeg" || $type == "png" || $type == "jpg")) {

      if (isset($_FILES["file"]["error"]) && $_FILES["file"]["error"] > 0) {

        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
      } else {

            if (file_exists("../files/" . $alum->getImg_alum() . ".jpg")) {
                unlink("../files/" . $alum->getImg_alum() . ".jpg");
            }
            $key = $base->generarKey();
            $alum->setImg_alum($key);
            $DaoAlumnos->update($alum);

            move_uploaded_file($_FILES["file"]["tmp_name"], "../files/" . $key . ".jpg");
            //header("Location: contabilidad.php");
            //se usa cuando se suben archivos por carga de archivos normales
            // print_r($_FILES);
            $sourcefile = '../files/' . $key . '.jpg';
            $picsize = getimagesize($sourcefile);

            $source_x = $picsize[0] * 1;
            $source_y = $picsize[1] * 1;

            //modificar el tamaño de la imagen
            $result1 = $base->resizeToFile($sourcefile, 300, 300 * $source_y / $source_x, $sourcefile, 80);

            echo $key;
      }
    } else {
      echo "Invalid file: " . $type;
    }
  } elseif (isset($_GET['action'])) {
    if (($_GET['TypeFile'] == "image/gif" || $_GET['TypeFile'] == "image/jpeg" || $_GET['TypeFile'] == "image/png")) {
      if (isset($_FILES["file"]["error"]) && $_FILES["file"]["error"] > 0) {

        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
      } else {
          
            if (file_exists("../files/" . $alum->getImg_alum() . ".jpg")) {
                unlink("../files/" . $alum->getImg_alum() . ".jpg");
            }
            $key = $base->generarKey();
            $alum->setImg_alum($key);
            $DaoAlumnos->update($alum);

            if (isset($_GET['base64'])) {
              // If the browser does not support sendAsBinary ()
              $content = base64_decode(file_get_contents('php://input'));
            } else {
              $content = file_get_contents('php://input');
            }
            file_put_contents('../files/' . $key . '.jpg', $content);

            $sourcefile = '../files/' . $key . '.jpg';
            $picsize = getimagesize($sourcefile);

            $source_x = $picsize[0] * 1;
            $source_y = $picsize[1] * 1;

            //modificar el tamaño de la imagen
            $result1 = $base->resizeToFile($sourcefile, 300, 300 * $source_y / $source_x, $sourcefile, 80);

            echo $key;
      }
    } else {
      //print_r($_FILES);
      //print_r($_GET);
      echo "Invalid file: " . $_GET['TypeFile'];
    }
  }
}

if (isset($_POST['action']) && $_POST['action']=="box_documents"){
    $DaoArchivosAlumno= new DaoArchivosAlumno();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoUsuarios= new DaoUsuarios();

    $alum = $DaoAlumnos->show($_POST['Id_alum']);
    $base= new base();
?>
<div id="box_emergente" class="box_documentos">
    <h1><i class="fa fa-file-o"></i> Documentos del alumno</h1>
    <div id="box-documentos-oferta">
      <?php
      foreach($DaoOfertasAlumno->getOfertasAlumno($_POST['Id_alum']) as $k=>$v){
              $ofe = $DaoOfertas->show($v->getId_ofe());
              $nombreOferta=strtolower($ofe->getNombre_oferta());
              //echo $v->getId();
        ?>
          <div class="list_oferta_alumno">
              <p id="NombreAlumn"><?php echo strtoupper($alum->getMatricula()." ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM())?></p>
              <table class = "table">
              <thead>
                  <tr>
                      <td>Oferta</td>
                      <td colspan="4"><?php echo ucwords($nombreOferta)?></td>
                  </tr>
                  <tr>
                        <td>Documento</td>
                        <td>Original</td>
                        <td>Copia</td>
                        <td>Fecha de entrega</td>
                        <td>Recibido</td>
                  </tr>
              </thead>
              <?php
              foreach($DaoArchivosAlumno->getArchivosAlumnoByOfertaAlumno($v->getId()) as $k2=>$v2){
                        $nombre_usu="";
                        if($v2['Id_usu']!=null){
                           $usu=$DaoUsuarios->show($v2['Id_usu']);
                           $nombre_usu= $usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
                        }
                        ?>
                            <tr id-file="<?php echo $v2['Id_file']?>">
                                 <td><?php echo $v2['Nombre_file']?></td>
                                 <td><input type="checkbox"  <?php if ($v2['Id_file_original']==1) { ?> checked="checked" <?php } ?> class="file_original"></td>
                                 <td><input type="checkbox"  <?php if ($v2['Id_file_copy']==1) { ?> checked="checked" <?php } ?>  class="copi_file"></td>
                                 <td><?php echo $v2['fecha_recibido']?></td>
                                 <td><?php echo $nombre_usu;?></td>
                            </tr>
                        <?php
               }
              ?>
            </table>
            <input type="hidden" class="oferta" value="<?php echo $v->getId_ofe() ?>"/>
            <p><button onclick="save_documentos_alumno(<?php echo $v->getId_ofe()?>, <?php echo $v->getId();?>, this)">Guardar</button>
            <button onclick = "imprimir()">Imprimir</button>
            <button onclick = "ocultar_error_layer();cssImprimirPage()">Cancelar</button></p>
          </div>
      <?php
       }
      ?>
           </div>
      </div>
<?php
}



if (isset($_POST['action']) && $_POST['action'] == "box_curso_interes") {
    $DaoOfertas= new DaoOfertas();
    $DaoCiclos= new DaoCiclos();
    $DaoTurnos= new DaoTurnos();
?>
      <div class="box-emergente-form cambiar-oferta">
      <h1>Añadir oferta de inter&eacute;s</h1>
      <p>Oferta de inter&eacute;s<br>
         <select id="oferta" onchange="update_curso_box_curso()">
          <option value="0"></option>
          <?php
          foreach($DaoOfertas->showAll() as $k=>$v){
          ?>
              <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
          <?php
          }
          ?>
        </select>     
      </p>
      <p>Especialidad<br>
        <select id="curso" onchange="update_orientacion_box_curso()">
          <option value="0"></option>
        </select>
      </p>
      <div id="box_orientacion"></div>
      <p>Ciclo<br>
       <select id="ciclo">
          <option value="0"></option>
          <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave() ?></option>
              <?php
              }
            ?>
        </select>
      </p>
      <p>Turno<br>
        <select id="turno">
          <option value="0"></option>
            <?php
            foreach($DaoTurnos->getTurnos() as $turno){
                ?>
                <option value="<?php echo $turno->getId()?>"><?php echo $turno->getNombre()?></option>
            <?php
            }
            ?>
        </select>
      </p>
      <p><button onclick="add_ofe_alum_interes()">Agregar</button><button onclick = "ocultar_error_layer()">Cancelar</button></p>
</div>
<?php
}


if (isset($_POST['action']) && $_POST['action']=="save_documentos_alumno"){ 
  $DaoDocumentos= new DaoDocumentos();
  $DaoArchivosAlumno= new DaoArchivosAlumno();
  $DaoArchivosAlumno->deleteFilesByOferta($_POST['Id_ofe'],$_POST['Id_alumn']);

  foreach ($_POST['files'] as $k => $v) {
      $ArchivosAlumno= new ArchivosAlumno();
      $ArchivosAlumno->setId_alum($_POST['Id_alumn']);
      $ArchivosAlumno->setId_file($v['Id_file']);
      $ArchivosAlumno->setId_file_copy($v['copy_file']);
      $ArchivosAlumno->setId_file_original($v['original_file']);
      $ArchivosAlumno->setId_ofe($_POST['Id_ofe']);
      $ArchivosAlumno->setId_ofe_alum($_POST['Id_ofe_alum']);
      $ArchivosAlumno->setId_usu($_COOKIE['admin/Id_usu']);
      $ArchivosAlumno->setFecha_recibido(date('Y-m-d'));
      $DaoArchivosAlumno->add($ArchivosAlumno);
      
      $doc=$DaoDocumentos->show($v['Id_file']);
      //Captura de historial
      $DaoUsuarios= new DaoUsuarios();
      $DaoAlumnos= new DaoAlumnos();
      $alum=$DaoAlumnos->show($_POST['Id_alumn']);
      $TextoHistorial="Recibe el documento ".$doc->getNombre()." de ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
      $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Documentos del alumno");
  }
}


if (isset($_POST['action']) && $_POST['action'] == "save_alum") {
      $DaoAlumnos = new DaoAlumnos();
      $DaoDirecciones= new DaoDirecciones();
      $DaoContactos= new DaoContactos();
      $DaoTutores= new DaoTutores();
      
      if($_POST['Id_alumn']>0){
            
            if($_POST['Id_dir']>0){
              $Direcciones= $DaoDirecciones->show($_POST['Id_dir']);
              $Direcciones->setCalle_dir($_POST['calle_alum']);
              $Direcciones->setNumExt_dir($_POST['numExt_alum']);
              $Direcciones->setNumInt_dir($_POST['numInt_alum']);
              $Direcciones->setColonia_dir($_POST['colonia_alum']);
              $Direcciones->setCiudad_dir($_POST['ciudad_alum']);
              $Direcciones->setEstado_dir($_POST['estado_alum']);
              $Direcciones->setCp_dir($_POST['cp_alum']);
              $DaoDirecciones->update($Direcciones);
              $Id_dir=$_POST['Id_dir'];
            }else{
              $Direcciones= new Direcciones();
              $Direcciones->setCalle_dir($_POST['calle_alum']);
              $Direcciones->setNumExt_dir($_POST['numExt_alum']);
              $Direcciones->setNumInt_dir($_POST['numInt_alum']);
              $Direcciones->setColonia_dir($_POST['colonia_alum']);
              $Direcciones->setCiudad_dir($_POST['ciudad_alum']);
              $Direcciones->setEstado_dir($_POST['estado_alum']);
              $Direcciones->setCp_dir($_POST['cp_alum']);
              $Direcciones->setTipoRel_dir("alum");
              $Direcciones->setIdRel_dir($_POST['Id_alumn']);
              $Id_dir=$DaoDirecciones->add($Direcciones);
            }
            
            $alum=$DaoAlumnos->show($_POST['Id_alumn']);
            $alum->setNombre($_POST['nombre_alum']);
            $alum->setApellidoP($_POST['apellidoP_alum']);
            $alum->setApellidoM($_POST['apellidoM_alum']);
            $alum->setEdad($_POST['edad_alum']);
            $alum->setEmail($_POST['email_alum']);
            $alum->setTel($_POST['tel_alum']);
            $alum->setCel($_POST['cel_alum']);
            $alum->setId_medio_ent($_POST['medio_alumn']);
            $alum->setSeguimiento_ins($_POST['priodidad']);
            $alum->setSexo($_POST['sexo']);
            $alum->setFechaNac($_POST['fechaN_alum']);
            $alum->setId_dir($Id_dir);
            $alum->setCurp_ins($_POST['curp_alum']);
            $alum->setUltGrado_est($_POST['escolaridad']);
            $alum->setEscuelaPro($_POST['escula_pro']);
            $alum->setTipo_sangre($_POST['tipo_sangre']);
            $alum->setAlergias($_POST['alergias_contacto']);
            $alum->setEnfermedades_cronicas($_POST['cronicas_contacto']);
            $alum->setPreinscripciones_medicas($_POST['preinscripciones_medicas_contacto']);
            $alum->setComentarios($_POST['comentarios']);
            $alum->setMatriculaSep($_POST['matriculaSep']);
            $alum->setExpedienteSep($_POST['expedienteSep']);
            $DaoAlumnos->update($alum);
            
            if($_POST['Id_contacto']>0){
                   $Contactos= $DaoContactos->show($_POST['Id_contacto']);
                   $Contactos->setNombre($_POST['nombre_contacto']);
                   $Contactos->setParentesco($_POST['parentesco_contacto']);
                   $Contactos->setDireccion($_POST['direccion_contacto']);
                   $Contactos->setTel_casa($_POST['tel_contacto']);
                   $Contactos->setTel_ofi($_POST['telOfic_contacto']);
                   $Contactos->setCel($_POST['celular_contacto']);
                   $Contactos->setEmail($_POST['email_contacto']);
                   $DaoContactos->update($Contactos);
            }elseif(strlen($_POST['nombre_contacto'])>0 && strlen($_POST['parentesco_contacto'])>0 && strlen($_POST['direccion_contacto'])>0){
                   $Contactos= new Contactos();
                   $Contactos->setNombre($_POST['nombre_contacto']);
                   $Contactos->setParentesco($_POST['parentesco_contacto']);
                   $Contactos->setDireccion($_POST['direccion_contacto']);
                   $Contactos->setTel_casa($_POST['tel_contacto']);
                   $Contactos->setTel_ofi($_POST['telOfic_contacto']);
                   $Contactos->setCel($_POST['celular_contacto']);
                   $Contactos->setEmail($_POST['email_contacto']);
                   $Contactos->setIdRel_con($_POST['Id_alumn']);
                   $Contactos->setTipoRel_con("alumno");
                   $DaoContactos->add($Contactos);  
            }
            
            //Tutores
            if (isset($_POST['tutores']) && count($_POST['tutores']) > 0) {
                foreach($_POST['tutores'] as $tutor){
                     if($tutor['id']>0){
                         $Tutores= $DaoTutores->show($tutor['id']);
                         $Tutores->setParentesco($tutor['parantesco']);
                         $Tutores->setNombre($tutor['nombre']);
                         $Tutores->setApellidoP($tutor['apellidoP']);
                         $Tutores->setApellidoM($tutor['apellidoM']);
                         $Tutores->setTel($tutor['tel']);
                         $Tutores->setCel($tutor['cel']);
                         $Tutores->setEmail($tutor['emal']);
                         $DaoTutores->update($Tutores);  
                         $Id_tutor=$tutor['id'];
                         addDireccionTutor($tutor,$Id_tutor);

                     }else{
                         $Tutores= new Tutores();
                         $Tutores->setParentesco($tutor['parantesco']);
                         $Tutores->setNombre($tutor['nombre']);
                         $Tutores->setApellidoP($tutor['apellidoP']);
                         $Tutores->setApellidoM($tutor['apellidoM']);
                         $Tutores->setTel($tutor['tel']);
                         $Tutores->setCel($tutor['cel']);
                         $Tutores->setEmail($tutor['emal']);
                         $Tutores->setId_ins($_POST['Id_alumn']);
                         $Id_tutor=$DaoTutores->add($Tutores);

                         addDireccionTutor($tutor,$Id_tutor);
                     }
                }
            }
            $id_alumn=$_POST['Id_alumn'];
  }
        echo $id_alumn;
}



if (isset($_POST['action']) && $_POST['action'] == "mostrar_cambiar_oferta") {
    $DaoOfertas= new DaoOfertas();
    $DaoCiclos= new DaoCiclos();
    $DaoTurnos= new DaoTurnos();
?>
      <div class="box-emergente-form cambiar-oferta">
      <h1>Cambiar oferta</h1>
      <p>Oferta<br>
         <select id="oferta" onchange="getTipoCicloCambioOferta(<?php echo $_POST['Id_ofe_alum']?>)">
          <option value="0"></option>
          <?php
          foreach($DaoOfertas->showAll() as $k=>$v){
          ?>
              <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
          <?php
          }
          ?>
        </select>     
      </p>
      <p>Especialidad<br>
        <select id="curso" onchange="update_orientacion_box_curso()">
          <option value="0"></option>
        </select>
      </p>
      <div id="box_orientacion"></div>
      <p>Grado<br>
         <select id="grado">
              <option value="0"></option>
         </select>
      </p>
      <p>Ciclo<br>
       <select id="ciclo">
          <option value="0"></option>
          <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave() ?></option>
              <?php
              }
            ?>
        </select>
      </p>
      <p>Opci&oacute;n de pago<br>
            <select id="opcionPago">
              <option value="0"></option>
              <option value="1">Materias</option>
              <option value="2">Plan Completo</option>
            </select>
      </p>
      <p>Turno<br>
        <select id="turno">
          <option value="0"></option>
            <?php
            foreach($DaoTurnos->getTurnos() as $turno){
                ?>
                <option value="<?php echo $turno->getId()?>"><?php echo $turno->getNombre()?></option>
            <?php
            }
            ?>
        </select>
      </p>
      <p><button onclick="cambiar_oferta(<?php echo $_POST['Id_ofe_alum']?>)">Cambiar</button><button onclick = "ocultar_error_layer()">Cancelar</button></p>
</div>
<?php
}


if (isset($_POST['action']) && $_POST['action'] == "update_curso") {
  ?>
  <option value="0"></option>
  <?php
  $DaoEspecialidades= new DaoEspecialidades();
  foreach ($DaoEspecialidades->getEspecialidadesOferta($_POST['Id_oferta']) as $k => $v) {
  ?>
  <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre_esp(); ?> </option>
  <?php
  }
}


if (isset($_POST['action']) && $_POST['action'] == "update_orientacion") {
  $DaoOrientaciones= new DaoOrientaciones();
  if(count($DaoOrientaciones->getOrientacionesEspecialidad($_POST['Id_esp']))>0){
  ?>
  Orientaci&oacute;n<br>
  <select id="orientacion">
    <option value="0"></option>
  <?php
  foreach ($DaoOrientaciones->getOrientacionesEspecialidad($_POST['Id_esp']) as $k => $v) {
  ?>
    <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre() ?> </option>
  <?php
  }
  ?>
  </select>
  <?php
   }
}   

if (isset($_POST['action']) && $_POST['action'] == "update_grado") {
   $DaoGrados= new DaoGrados();
  ?>
  <option value="0"></option>
  <?php
  foreach($DaoGrados->getGradosEspecialidad($_POST['Id_esp']) as $k=>$v){
  ?>
      <option value="<?php echo $v->getId() ?>"> <?php echo $v->getGrado() ?> </option>
  <?php
  }
}

if (isset($_POST['action']) && $_POST['action'] == "update_orientacion_box_curso") {
  $DaoOrientaciones= new DaoOrientaciones();
  if(count($DaoOrientaciones->getOrientacionesEspecialidad($_POST['Id_esp']))>0){
  ?>
  <p>Orientaci&oacute;n<br>
  <select id="orientacion">
    <option value="0"></option>
  <?php
  foreach ($DaoOrientaciones->getOrientacionesEspecialidad($_POST['Id_esp']) as $k => $v) {
  ?>
    <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre() ?> </option>
  <?php
  }
  ?>
  </select>
  </p>
  <?php
   }
}   

if(isset($_POST['action']) && $_POST['action']=="save_motivo"){
    $DaoNotificacionesUsuario= new DaoNotificacionesUsuario();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoUsuarios= new DaoUsuarios();
    $DaoAlumnos= new DaoAlumnos();
    $DaoOfertas= new DaoOfertas();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoCiclos= new DaoCiclos();
    $base= new base();

    $ofeAlum=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $alum=$DaoAlumnos->show($ofeAlum->getId_alum());
    $ofe=$DaoOfertas->show($ofeAlum->getId_ofe());
    $esp=$DaoEspecialidades->show($ofeAlum->getId_esp());
    
    //Obtener el ultimo ciclo de la oferta
    $ultimoCiclo=$DaoOfertasAlumno->getLastCicloOferta($_POST['Id_ofe_alum']);
    $Id_ciclo=$ultimoCiclo['Id_ciclo'];
    $Id_ciclo_alum=$ultimoCiclo['Id_ciclo_alum'];
    
    if($Id_ciclo_alum){
        $query="
        SELECT Horario_docente.*, Grupos.Clave FROM materias_ciclo_ulm 
            JOIN Grupos ON materias_ciclo_ulm.Id_grupo=Grupos.Id_grupo
            JOIN Horario_docente ON materias_ciclo_ulm.Id_grupo=Horario_docente.Id_grupo
            JOIN Docentes ON Horario_docente.Id_docente=Docentes.Id_docen
        WHERE Id_ciclo_alum=".$Id_ciclo_alum." GROUP BY Id_grupo";
        $consulta=$base->advanced_query($query);
        foreach($base->advanced_query($query) as $row_consulta){
                //Crear Notificacion 
                $NotificacionesUsuario= new NotificacionesUsuario();
                $textoNot=$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM()." ha sido dado de baja del grupo #".$row_consulta['Clave'];

                $NotificacionesUsuario->setTitulo("Alumno dado de baja");
                $NotificacionesUsuario->setTexto($textoNot);
                $NotificacionesUsuario->setDateCreated(date('Y-m-d H:i:s'));
                $NotificacionesUsuario->setIdRel($row_consulta['Id_docente']);
                $NotificacionesUsuario->setTipoRel('docente');
                $DaoNotificacionesUsuario->add($NotificacionesUsuario);
         }
    }

    $OfertasAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $OfertasAlumno->setActivo(0);
    $OfertasAlumno->setBaja_ofe(date('Y-m-d H:i:s'));
    $OfertasAlumno->setId_mot_baja($_POST['Id_mot']);
    $OfertasAlumno->setComentario_baja($_POST['comentario']);
    $OfertasAlumno->setId_usu_captura_baja($_COOKIE['admin/Id_usu']);
    $DaoOfertasAlumno->update($OfertasAlumno);
    
    //Captura de historial
    $TextoHistorial="Elimina oferta de ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ".$ofe->getNombre_oferta().", ".$esp->getNombre_esp().", ".$_POST['comentario'];
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Alumnos");

    update_page($_POST['Id_alum']);

}


if (isset($_POST['action']) && $_POST['action'] == "mostrar_cambiar_plan") {
    $DaoOfertasAlumno=new DaoOfertasAlumno();
    $OfertasAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
?>
      <div class="box-emergente-form  cambiar_plan">
            <h1><i class="fa fa-exchange"></i> Plan de pago</h1>
            <p>Opción de pago<br>
                <select id="opcionPago">
                    <option value="0"></option>
                    <option value="2" <?php if($OfertasAlumno->getOpcionPago()==2){?> selected="selected" <?php } ?> >Plan completo</option>
                    <option value="1" <?php if($OfertasAlumno->getOpcionPago()==1){?> selected="selected" <?php } ?> >Por materias</option>
                </select>
            </p>
            <p><button onclick="cambiar_plan_pago(<?php echo $_POST['Id_ofe_alum']?>)">Cambiar</button><button onclick = "ocultar_error_layer()">Cancelar</button></p>
      </div>
<?php
}


if (isset($_POST['action']) && $_POST['action'] == "mostrar_box_baja") {
    $DaoMotivosBaja = new DaoMotivosBaja();
?>
<div class="box-emergente-form motivos_baja">
    <h1>Motivo de baja</h1>
    <div>
        <p>                                  
          <select id="motivos_bajas">
               <option value="0">Selecciona un motivo</option>
                <?php
                    foreach ($DaoMotivosBaja->showAll() as $motivo) {
                        ?>
                        <option value="<?php echo $motivo->getId() ?>"><?php echo $motivo->getNombre() ?></option>
                        <?php
                    }
                ?>
              </select>
        </p>
       
        <p>Comentarios:<br> <textarea id="comentario"></textarea></p>
        <button onclick="save_motivo(<?php echo $_POST['Id_ofe_alum'] ?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cancelar</button>
    </div>
</div>
<?php
}



function update_page($Id_alum) {
        $base= new base();
        $DaoAlumnos = new DaoAlumnos();
        $DaoOfertas = new DaoOfertas();
        $DaoEspecialidades = new DaoEspecialidades();
        $DaoOrientaciones = new DaoOrientaciones();
        $DaoCiclos = new DaoCiclos();
        $DaoOfertasAlumno = new DaoOfertasAlumno();
        $DaoCiclosAlumno= new DaoCiclosAlumno();
        $DaoPagosCiclo= new DaoPagosCiclo();
        $DaoDirecciones= new DaoDirecciones();
        $DaoMediosEnterar= new DaoMediosEnterar();
        $DaoContactos= new DaoContactos();
        $DaoUsuarios= new DaoUsuarios();
        $DaoTurnos= new DaoTurnos();
        $DaoTutores= new DaoTutores();
        $_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

        $perm=array();   
        foreach($DaoUsuarios->getPermisosUsuario($_usu->getId()) as $k=>$v){
            $perm[$v['Id_per']]=1;
        }
        
        $Id_dir=0;
        $Img_alum="";
        $Nombre_ins= "";
        $ApellidoP_ins="";
        $ApellidoM_ins="";
        $Matricula= "";
        $Refencia_pago="";
        $FechaNac="";
        $Edad_ins="";
        $Sexo= "";
        $Email_ins= "";
        $Tel_ins="";
        $Cel_ins="";
        $Curp_ins= "";
        $Comentarios= "";
        $NombreTutor= "";
        $EmailTutor= "";
        $TelTutor= "";
        $Tipo_sangre= "";
        $Alergias="";
        $Enfermedades_cronicas= "";
        $Preinscripciones_medicas="";
        $Id_medio_ent="";
        $Seguimiento_ins= "";
        $UltGrado_est= "";
        $EscuelaPro="";
        $matriculaSep="";
        $expedienteSep="";
        
        $ContactoNombre="";
        $ContactoParentesco="";
        $ContactoDireccion="";
        $ContactoTel_casa="";
        $ContactoTel_ofi="";
        $ContactoCel="";
        $ContactoEmail="";
        $ContactoId_cont="";

        $Calle_dir="";
        $NumExt_dir="";
        $NumInt_dir="";
        $Colonia_dir="";
        $Cp_dir="";
        $Ciudad_dir="";
        $Estado_dir="";

        ?>
        <table id="tabla">
            <?php
            $readonly = "";
            if (isset($Id_alum) && $Id_alum > 0) {
                $Id_alum=$Id_alum;
                $alum = $DaoAlumnos->show($Id_alum);
                $Img_alum=$alum->getImg_alum();
                $Nombre_ins= $alum->getNombre();
                $ApellidoP_ins=$alum->getApellidoP();
                $ApellidoM_ins=$alum->getApellidoM();
                $Matricula= $alum->getMatricula();
                $Refencia_pago=$alum->getRefencia_pago();
                $FechaNac=$alum->getFechaNac();
                $Edad_ins=$alum->getEdad();
                $Sexo= $alum->getSexo();
                $Email_ins= $alum->getEmail();
                $Tel_ins=$alum->getTel();
                $Cel_ins=$alum->getCel();
                $Curp_ins= $alum->getCurp_ins();
                $Comentarios= $alum->getComentarios();
                $NombreTutor= $alum->getNombreTutor();
                $EmailTutor= $alum->getEmailTutor();
                $TelTutor= $alum->getTelTutor();
                $Tipo_sangre= $alum->getTipo_sangre();
                $Alergias=$alum->getAlergias();
                $Enfermedades_cronicas= $alum->getEnfermedades_cronicas(); 
                $Preinscripciones_medicas=$alum->getPreinscripciones_medicas();
                $Id_medio_ent=$alum->getId_medio_ent();
                $Seguimiento_ins= $alum->getSeguimiento_ins();
                $UltGrado_est= $alum->getUltGrado_est();
                $EscuelaPro=$alum->getEscuelaPro();
                $matriculaSep=$alum->getMatriculaSep();
                $expedienteSep=$alum->getExpedienteSep();
        
                $contacto=$DaoContactos->getPrimerContacto($Id_alum,'alumno');

                $ContactoNombre=$contacto->getNombre();
                $ContactoParentesco=$contacto->getParentesco();
                $ContactoDireccion=$contacto->getDireccion();
                $ContactoTel_casa=$contacto->getTel_casa();
                $ContactoTel_ofi=$contacto->getTel_ofi();
                $ContactoCel=$contacto->getCel();
                $ContactoEmail=$contacto->getEmail();
                $ContactoId_cont=$contacto->getId();

                if ($alum->getId_dir() > 0) {
                    $Id_dir=$alum->getId_dir();
                    $dir = $DaoDirecciones->show($alum->getId_dir());
                    $Calle_dir=$dir->getCalle_dir();
                    $NumExt_dir=$dir->getNumExt_dir();
                    $NumInt_dir=$dir->getNumInt_dir();
                    $Colonia_dir=$dir->getColonia_dir();
                    $Cp_dir=$dir->getCp_dir();
                    $Ciudad_dir=$dir->getCiudad_dir();
                    $Estado_dir=$dir->getEstado_dir();
                }
                $readonly = 'readonly="readonly"';
            }
            ?>
            <tr>
                <td id="column_one">
                    <div class="fondo">
                        <div id="box_top">
                            <div class="foto_alumno" 
                            <?php if (strlen($Img_alum) > 0) { ?> 
                                     style="background-image:url(files/<?php echo $Img_alum ?>.jpg) <?php } ?>">
                            </div>
                            <h1><?php echo ucwords(strtolower($Nombre_ins . " " . $ApellidoP_ins . " " . $ApellidoM_ins)) ?></h1>
                        </div>
                        <div class="seccion">
                            <h2>Datos Personales</h2>
                            <span class="linea"></span>
                            <ul class="form">
                                <li>Matricula<br><input type="text" value="<?php echo $Matricula ?>" id="matricula" <?php echo $readonly; ?>/></li>
                                <li>Referencia de pago<br><input type="text" value="<?php echo $Refencia_pago ?>" id="referencia_pago" <?php echo $readonly; ?>/></li>
                                <li><span class="requerido">*</span>Nombre<br><input type="text" value="<?php echo $Nombre_ins ?>" id="nombre_alum"/></li>
                                <li><span class="requerido">*</span>Apellido Paterno<br><input type="text" value="<?php echo $ApellidoP_ins ?>" id="apellidoP_alum"/></li>
                                <li><span class="requerido">*</span>Apellido Materno<br><input type="text" value="<?php echo $ApellidoM_ins ?>" id="apellidoM_alum"/></li>
                                <?php
                                if ($FechaNac != null) {
                                    $edad = $base->calcularEdad($FechaNac);
                                } else {
                                    $edad = $Edad_ins;
                                }
                                ?>
                                <li>Edad<br><input type="text" id="edad_alum" value="<?php echo $edad ?>"/></li>
                                <li>Sexo<br><select id="sexo">
                                        <option></option>
                                        <option value="m" <?php if ($Sexo == "m") { ?> selected="selected"<?php } ?>>Masculino</option>
                                        <option value="f" <?php if ($Sexo == "f") { ?> selected="selected"<?php } ?>>Femenino</option>
                                    </select>
                                </li>
                                <li>Fecha de Nacimiento<br><input type="date" id="fechaN_alum" value="<?php echo $FechaNac ?>"/></li>
                                <li>Email<br><input type="email" id="email_alum" value="<?php echo $Email_ins ?>"/></li>
                                <li>Tel&eacute;fono Casa<br><input type="tel" id="tel_alum" value="<?php echo $Tel_ins ?>"/></li>
                                <li>Tel&eacute;fono Movil<br><input type="tel" id="cel_alum" value="<?php echo $Cel_ins ?>"/></li>
                                <li>CURP<br><input type="text" id="curp_alum" value="<?php echo $Curp_ins ?>"/></li>
                                <li>Matricula SEP<br><input type="text" id="matriculaSep" value="<?php echo $matriculaSep ?>"/></li>
                                <li>Expediente SEP<br><input type="text" id="expedienteSep" value="<?php echo $expedienteSep ?>"/></li>
                                <li>Comentarios<br><textarea id="comentarios"><?php echo $Comentarios ?></textarea></li>
                            </ul>
                        </div>
                        <div class="seccion">
                            <h2>Direcci&oacute;n</h2>
                            <span class="linea"></span>
                            <ul class="form">
                                <li><span class="requerido">*</span>Calle<br><input type="text" id="calle_alum" value="<?php echo $Calle_dir ?>"/></li>
                                <li><span class="requerido">*</span>N&uacute;mero exterior<br><input type="text" id="numExt_alum" value="<?php echo $NumExt_dir ?>"/></li>
                                <li>N&uacute;mero interior<br><input type="text" id="numInt_alum" value="<?php echo $NumInt_dir ?>"/></li>
                                <li>Colonia<br><input type="text" id="colonia_alum" value="<?php echo $Colonia_dir ?>"/></li>
                                <li>C&oacute;digo postal<br><input type="text" id="cp_alum" value="<?php echo $Cp_dir ?>"/></li>
                                <li><span class="requerido">*</span>Ciudad<br><input type="text" id="_alum" value="<?php echo $Ciudad_dir ?>"/></li>
                                <li>Estado<br><input type="text" id="estado_alum" value="<?php echo $Estado_dir ?>"/></li>
                            </ul>
                        </div>
                        <div class="seccion">
                            <h2>Datos del padre o tutor</h2>
                            <span class="linea"></span>
                            <div id="list_tutores">
                                <?php
                                $countTu=0;
                                if ($Id_alum > 0) {
                                    foreach($DaoTutores->getTutoresAlumno($Id_alum) as $tutor){
                                            $id_dir=0;
                                            $dir=$DaoDirecciones->getDireccionPorTipoRel($tutor->getId(),'tutor');
                                            if($dir->getId()>0){
                                              $id_dir= $dir->getId(); 
                                            }
                                    ?>
                                        <ul class="form" id-tutor="<?php echo $tutor->getId();?>" id-dir="<?php echo $dir->getId()?>">
                                            <li>Parentesco<br><input type="text" class="parentesco-tutor" value="<?php echo $tutor->getParentesco();?>"/></li>
                                            <li>Nombre<br><input type="text" class="nombre-tutor" value="<?php echo $tutor->getNombre();?>"/></li>
                                            <li>Apellido paterno<br><input type="text" class="apellidoP-tutor" value="<?php echo $tutor->getApellidoP();?>"/></li>
                                            <li>Apellido materno<br><input type="text" class="apellidoM-tutor" value="<?php echo $tutor->getApellidoM();?>"/></li>
                                            <li>Correo electrónico<br><input type="text" class="email-tutor" value="<?php echo $tutor->getEmail();?>"/></li>
                                            <li>Teléfono<br><input type="text" class="tel-tutor" value="<?php echo $tutor->getTel();?>"/></li>
                                            <li>Celular<br><input type="text" class="cel-tutor" value="<?php echo $tutor->getCel();?>"/></li>
                                            <li>Calle<br><input type="text" class="calle-tutor" value="<?php echo $dir->getCalle_dir();?>"/></li>
                                            <li>Núm ext.<br><input type="text" class="numext-tutor" value="<?php echo $dir->getNumExt_dir();?>"/></li>
                                            <li>Núm int.<br><input type="text" class="numint-tutor" value="<?php echo $dir->getNumInt_dir();?>"/></li>
                                            <li>Colonia<br><input type="text" class="colonia-tutor" value="<?php echo $dir->getColonia_dir();?>"/></li>
                                            <li><span class="requerido">*</span>Ciudad<br><input type="text" class="ciudad-tutor" value="<?php echo $dir->getCiudad_dir();?>"/></li>
                                            <li>Estado<br><input type="text" class="estado-tutor" value="<?php echo $dir->getEstado_dir();?>"/></li>
                                            <li>Código postal<br><input type="text" class="cp-tutor" value="<?php echo $dir->getCp_dir();?>"/></li>
                                            <li><button type="button" class="btns btns-primary btns-sm delete-tutor" onclick="delete_tutor(<?php echo $tutor->getId() ?>,<?php echo $id_dir?>)">Eliminar tutor</button></li>
                                        </ul>
                                        <hr>
                                    <?php
                                    $countTu++;
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="seccion">
                            <h2>Datos de Emergencia</h2>
                            <span class="linea"></span>
                            <ul class="form">
                                <li>Tipo de sangre<br><input type="text"  id="tipo_sangre" value="<?php echo $Tipo_sangre ?>"/></li>
                                <li>Nombre del contacto<br><input type="text" id="nombre_contacto" value="<?php echo $ContactoNombre ?>"/></li>
                                <li>Parentesco<br><input type="text" id="parentesco_contacto" value="<?php echo $ContactoParentesco ?>"/></li>
                                <li>Direcci&oacute;n<br><input type="text" id="direccion_contacto" value="<?php echo $ContactoDireccion ?>"/></li>
                                <li>Tel&eacute;fono Casa<br><input type="text" id="tel_contacto" value="<?php echo $ContactoTel_casa ?>"/></li>
                                <li>Tel&eacute;fono Oficina<br><input type="text" id="telOfic_contacto" value="<?php echo $ContactoTel_ofi ?>"/></li>
                                <li>Celular<br><input type="text" id="celular_contacto" value="<?php echo $ContactoCel ?>"/></li>
                                <li>Email<br><input type="text" id="email_contacto" value="<?php echo $ContactoEmail ?>"/></li>
                            </ul>
                            <input type="hidden" id="Id_contacto" value="<?php echo $ContactoId_cont ?>"/>
                        </div>
                        <div class="seccion">
                            <ul class="form">
                                <li>Alergias<br><textarea  id="alergias_contacto"><?php echo $Alergias ?></textarea></li>
                                <li>Enfermedades cr&oacute;nicas<br><textarea  id="cronicas_contacto"><?php echo $Enfermedades_cronicas ?></textarea></li>
                                <li>Pre-escripciones medicas<br><textarea  id="preinscripciones_medicas_contacto"><?php echo $Preinscripciones_medicas ?></textarea></li>
                            </ul>
                        </div>
                        <div class="seccion">
                            <h2>Datos de de inter&eacute;s</h2>
                            <span class="linea"></span>
                            <ul class="form">
                                <li>Medio por el que se enter&oacute;<br>
                                    <select id="medio_alumn">
                                        <option value="0"></option>
                                        <?php
                                        foreach ($DaoMediosEnterar->showAll() as $medio) {
                                            ?>
                                            <option value="<?php echo $medio->getId() ?>" <?php if ($Id_medio_ent == $medio->getId()) { ?> selected="selected"<?php } ?>><?php echo $medio->getMedio() ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </li>
                                <li><span class="requerido">*</span>Prioridad<br>
                                    <select id="priodidad">
                                        <option value="0"></option>
                                        <option value="1" <?php if ($Seguimiento_ins == 1) { ?> selected="selected"<?php } ?>>Alta</option>
                                        <option value="2" <?php if ($Seguimiento_ins == 2) { ?> selected="selected"<?php } ?>>Media</option>
                                        <option value="3" <?php if ($Seguimiento_ins == 3) { ?> selected="selected"<?php } ?>>Baja</option>
                                    </select>
                                </li>
                                <li>Ultimo Grado de Estudios<br>
                                    <select id="escolaridad">
                                        <option value="0"></option>
                                        <option value="1" <?php if ($UltGrado_est == 1) { ?> selected="selected"<?php } ?>>Primaria</option>
                                        <option value="2" <?php if ($UltGrado_est == 2) { ?> selected="selected"<?php } ?>>Secundaria</option>
                                        <option value="3" <?php if ($UltGrado_est == 3) { ?> selected="selected"<?php } ?>>Preparatoria</option>
                                        <option value="4" <?php if ($UltGrado_est == 4) { ?> selected="selected"<?php } ?>>Universidad</option>
                                        <option value="5" <?php if ($UltGrado_est == 5) { ?> selected="selected"<?php } ?>>Posgrado</option>
                                    </select>
                                </li>
                                <li>Escuela de procedencia<br><input type="text" id="escula_pro" value="<?php echo $EscuelaPro ?>"/></li>
                            </ul>
                        </div>
                        <div class="seccion">
                            <h2>Ofertas del alumno</h2>
                            <span class="linea"></span>
                            <?php
                            if ($Id_alum > 0) {
                                //Ofertas Activas
                                if (count($DaoOfertasAlumno->getOfertasAlumno($Id_alum)) > 0) {
                                    foreach ($DaoOfertasAlumno->getOfertasAlumno($Id_alum) as $ofertaAlumno) {
                                        $primerCicloOfertaAlumno=$DaoCiclosAlumno->getPrimerCicloOferta($ofertaAlumno->getId());
                                        $ultimoCicloOfertaAlumno=$DaoCiclosAlumno->getLastCicloOferta($ofertaAlumno->getId());
                                        $grado = $DaoOfertasAlumno->getLastGradoOferta($ofertaAlumno->getId());
                                        $oferta = $DaoOfertas->show($ofertaAlumno->getId_ofe());
                                        $esp = $DaoEspecialidades->show($ofertaAlumno->getId_esp());
                                        $nombre_ori="";
                                        if ($ofertaAlumno->getId_ori() > 0) {
                                            $ori = $DaoOrientaciones->show($ofertaAlumno->getId_ori());
                                            $nombre_ori = $ori->getNombre();
                                        }

                                        $primer_ciclo="";
                                        if($primerCicloOfertaAlumno->getId_ciclo()>0){
                                           $ciclo=$DaoCiclos->show($primerCicloOfertaAlumno->getId_ciclo());
                                           $primer_ciclo=$ciclo->getClave();
                                        }

                                        $ultimo_ciclo="";
                                        if($ultimoCicloOfertaAlumno->getId_ciclo()>0){
                                           $ciclo=$DaoCiclos->show($ultimoCicloOfertaAlumno->getId_ciclo());
                                           $ultimo_ciclo=$ciclo->getClave();
                                        }

                                        $beca = $ultimoCicloOfertaAlumno->getPorcentaje_beca();
                                        if ($ultimoCicloOfertaAlumno->getPorcentaje_beca() == NULL) {
                                            $beca = 0;
                                        }

                                        $Opcion_pago = "";
                                        if ($ofertaAlumno->getOpcionPago() == 1) {
                                            $Opcion_pago = "Plan por materias";
                                        } else {
                                            $Opcion_pago = "Plan completo";
                                        }

                                        $tur = $DaoTurnos->show($ofertaAlumno->getTurno());
                                        $turno=$tur->getNombre();
                                        ?>
                                        <div class="seccion">
                                            <table  class="info_alumn">
                                                <tbody>
                                                <td>
                                                    <table width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <th><font>Carrera:</font></th>
                                                                <td colspan="4"><font><?php echo $esp->getNombre_esp(); ?></font></td>
                                                                <td><b style="color:red">Activo</b></td>
                                                            </tr>
                                                            <tr>
                                                                <th><font>Orientaci&oacute;n:</font></th>
                                                                <td><font color="navy" face="arial" size="2"><?php echo $nombre_ori; ?></font></td>
                                                                <th><font>Opci&oacute;n de pago</font></th>
                                                                <td><font color="red" face="arial" size="2"><?php echo $Opcion_pago; ?></font></td>
                                                                <th><font>Num. PAGOS</font></th>
                                                                <td><font color="navy" face="arial" size="2"><?php echo $esp->getNum_pagos(); ?></font></td>
                                                            </tr>
                                                            <tr>
                                                                <th ><font>PRECIO</font></th>
                                                                <td><font>$<?php echo number_format($esp->getPrecio_curso(), 2) ?></font></td>
                                                                <th ><font>INSCRIPCIÓN</font></th>
                                                                <td><font>$<?php echo number_format($esp->getInscripcion_curso(), 2); ?></font></td>
                                                                <th ><font>MENSUALIDAD:</font></th>
                                                                <td colspan="5"><font>$<?php echo number_format(($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos(), 2); ?></font></td>
                                                            </tr>
                                                            <tr>
                                                                <th><font>Nivel:</font></th>
                                                                <td><font><?php echo $oferta->getNombre_oferta(); ?></font></td>
                                                                <th><font>Admisi&oacute;n:</font></th>
                                                                <td><?php echo $primer_ciclo; ?></font></td>
                                                                <th><font>Último ciclo:</font></th>
                                                                <td><font><?php echo $ultimo_ciclo; ?></font></td>
                                                            </tr>
                                                            <tr>
                                                                <th><font>Grado actual:</font></th>
                                                                <td><font><?php echo $grado['Grado'] ?></font></td>
                                                                <th><font>Turno:</font></th>
                                                                <td><font><?php echo $turno ?></font></td>
                                                                <th><font>Beca:</font></th>
                                                                <td><font><?php echo $beca; ?>%</font></td>
                                                            </tr>
                                                            <tr>
                                                                <th><font>Acciones:</font></th>
                                                                <td style="width:140px;"><font>
                                                                    <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                                                                    <div class="box-buttom">
                                                                        <?php
                                                                        //Obtenemos el primer ciclo de la oferta
                                                                        $ciclosAlumno=$DaoCiclosAlumno->getCiclosOferta($ofertaAlumno->getId());
                                                                        if(count($ciclosAlumno)>0){
                                                                            //Verificar si ya pago la inscripcion
                                                                            $ExistePagoInscripcion=$DaoPagosCiclo->verificarPagoInscripcion($ofertaAlumno->getId());

                                                                            //Obtenemos los pagos del alumno del primer ciclo
                                                                            $totalRows_Pagos_ciclo=0;
                                                                            foreach($DaoPagosCiclo->getCargosCiclo($ciclosAlumno[0]->getId()) as $Cargos){
                                                                                $totalRows_Pagos_ciclo++;
                                                                            }
                                                                            //SI no pago la inscripcion
                                                                            if ($ExistePagoInscripcion == 0) {
                                                                                ?>
                                                                                <p><button onclick="generar_pdf_insc(<?php echo $Id_alum; ?>,<?php echo $ofertaAlumno->getId() ?>)">Formato de pago</button></p>
                                                                                <?php
                                                                            }
                                                                            //Es decir si ya pago la inscripcion y no se registrado todavia los demas pagos
                                                                            //Es decir si es una nueva oferta
                                                                            if ($ExistePagoInscripcion == 1 && $totalRows_Pagos_ciclo == 1 && $ofertaAlumno->getOpcionPago() == 2) {
                                                                                ?>
                                                                                <p><button onclick="inscribir(<?php echo $ofertaAlumno->getId() ?>)">Inscribir</button></p>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        if($oferta->getTipoOferta()==1){
                                                                          ?>
                                                                            <p><a href="pago.php?id=<?php echo $ofertaAlumno->getId() ?>"><button>Mostrar lista de pagos</button></a></p>
                                                                            <?php     
                                                                        }elseif($oferta->getTipoOferta()==2){
                                                                            ?>                                                           
                                                                            <p><a href="cargos_curso.php?id=<?php echo $ofertaAlumno->getId() ?>"><button>Mostrar lista de pagos</button></a></p>
                                                                            <?php      
                                                                        }

                                                                        if (isset($perm['72'])) {
                                                                        ?>
                                                                        <p><button onclick="mostrar_cambiar_plan(<?php echo $ofertaAlumno->getId(); ?>)">Cambiar plan de pago</button></p>
                                                                        <?php
                                                                        }
                                                                        if (isset($perm['70'])) {
                                                                        ?>
                                                                        <p><button onclick="mostrar_cambiar_oferta(<?php echo $ofertaAlumno->getId(); ?>)">Cambio de oferta</button></p>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                        <p><a href="contrato_servicios.php?id=<?php echo $Id_alum ?>&id_ofe=<?php echo $ofertaAlumno->getId(); ?>" class="link" target="_blank"><button>Descargar contrato</button></a></p>
                                                                        <?php
                                                                        if (isset($perm['71'])) {
                                                                            ?>

                                                                        <p><button onclick="mostrar_box_baja(<?php echo $ofertaAlumno->getId() ?>)">Dar de baja oferta</button></p>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                    </font>
                                                                </td> 
                                                                <td>
                                                                    <font color="red"></font>
                                                                </td>
                                                            </tr>

                                                        </tbody></table>
                                                </td></tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <?php
                                    }
                                }

                                //Ofertas egresadas
                                if (count($DaoOfertasAlumno->getOfertasEgresadasAlumno($Id_alum)) > 0) {

                                   foreach ($DaoOfertasAlumno->getOfertasEgresadasAlumno($Id_alum) as $ofertaAlumno) {
                                        $primerCicloOfertaAlumno=$DaoCiclosAlumno->getPrimerCicloOferta($ofertaAlumno->getId());
                                        $ultimoCicloOfertaAlumno=$DaoCiclosAlumno->getLastCicloOferta($ofertaAlumno->getId());
                                        $grado = $DaoOfertasAlumno->getLastGradoOferta($ofertaAlumno->getId());
                                        $oferta = $DaoOfertas->show($ofertaAlumno->getId_ofe());
                                        $esp = $DaoEspecialidades->show($ofertaAlumno->getId_esp());
                                        $nombre_ori="";
                                        if ($ofertaAlumno->getId_ori() > 0) {
                                            $ori = $DaoOrientaciones->show($ofertaAlumno->getId_ori());
                                            $nombre_ori = $ori->getNombre();
                                        }
                                        $primer_ciclo="";
                                        if($primerCicloOfertaAlumno->getId_ciclo()>0){
                                           $ciclo=$DaoCiclos->show($primerCicloOfertaAlumno->getId_ciclo());
                                           $primer_ciclo=$ciclo->getClave();
                                        }

                                        $ultimo_ciclo="";
                                        if($ultimoCicloOfertaAlumno->getId_ciclo()>0){
                                           $ciclo=$DaoCiclos->show($ultimoCicloOfertaAlumno->getId_ciclo());
                                           $ultimo_ciclo=$ciclo->getClave();
                                        }

                                        $beca = $ultimoCicloOfertaAlumno->getPorcentaje_beca();
                                        if ($ultimoCicloOfertaAlumno->getPorcentaje_beca() == NULL) {
                                            $beca = 0;
                                        }
                                        $Opcion_pago = "";
                                        if ($ofertaAlumno->getOpcionPago() == 1) {
                                            $Opcion_pago = "Plan por materias";
                                        } else {
                                            $Opcion_pago = "Plan completo";
                                        }
                                        $tur = $DaoTurnos->show($ofertaAlumno->getTurno());
                                        $turno=$tur->getNombre();
                                        ?>
                                        <div class="seccion">
                                            <table  class="info_alumn oferta-egresada">
                                                <tbody>
                                                <td>
                                                    <table width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <th><font>Carrera:</font></th>
                                                                <td colspan="4"><font><?php echo $esp->getNombre_esp(); ?></font></td>
                                                                <td><b style="color:red">Egresado</b></td>
                                                            </tr>
                                                            <tr>
                                                                <th><font>Orientaci&oacute;n:</font></th>
                                                                <td><font color="navy" face="arial" size="2"><?php echo $nombre_ori; ?></font></td>
                                                                <th><font>Opci&oacute;n de pago</font></th>
                                                                <td><font color="red" face="arial" size="2"><?php echo $Opcion_pago; ?></font></td>
                                                                <th><font>Num. PAGOS</font></th>
                                                                <td><font color="navy" face="arial" size="2"><?php echo $esp->getNum_pagos(); ?></font></td>
                                                            </tr>
                                                            <tr>
                                                                <th ><font>PRECIO</font></th>
                                                                <td><font>$<?php echo number_format($esp->getPrecio_curso(), 2) ?></font></td>
                                                                <th ><font>INSCRIPCIÓN</font></th>
                                                                <td><font>$<?php echo number_format($esp->getInscripcion_curso(), 2); ?></font></td>
                                                                <th ><font>MENSUALIDAD:</font></th>
                                                                <td colspan="5"><font>$<?php echo number_format(($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos(), 2); ?></font></td>
                                                            </tr>
                                                            <tr>
                                                                <th><font>Nivel:</font></th>
                                                                <td><font><?php echo $oferta->getNombre_oferta(); ?></font></td>
                                                                <th><font>Admisi&oacute;n:</font></th>
                                                                <td><?php echo $primer_ciclo; ?></font></td>
                                                                <th><font>Último ciclo:</font></th>
                                                                <td><font><?php echo $ultimo_ciclo; ?></font></td>
                                                            </tr>
                                                            <tr>
                                                                <th><font>Grado actual:</font></th>
                                                                <td><font><?php echo $grado['Grado'] ?></font></td>
                                                                <th><font>Turno:</font></th>
                                                                <td><font><?php echo $turno ?></font></td>
                                                                <th><font>Beca:</font></th>
                                                                <td><font><?php echo $beca; ?>%</font></td>
                                                            </tr>
                                                            <tr>
                                                                <th><font>Acciones:</font></th>
                                                                <td style="width:140px;"><font>
                                                                    <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                                                                    <div class="box-buttom">
                                                                        <?php
                                                                          if($oferta->getTipoOferta()==1){
                                                                          ?>
                                                                            <p><a href="pago.php?id=<?php echo $ofertaAlumno->getId() ?>"><button>Mostrar lista de pagos</button></a></p>
                                                                            <?php     
                                                                        }elseif($oferta->getTipoOferta()==2){
                                                                            ?>                                                           
                                                                            <p><a href="cargos_curso.php?id=<?php echo $ofertaAlumno->getId() ?>"><button>Mostrar lista de pagos</button></a></p>
                                                                            <?php      
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                    </font>
                                                                </td> 
                                                                <td>
                                                                    <font color="red"></font>
                                                                </td>
                                                            </tr>

                                                        </tbody></table>
                                                </td></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php
                                    } 
                                }
                            }

                            ?>
                        </div>
                        <div id="datosUsu">
                            <p><b>Fecha:</b>   <?php echo date('Y-m-d H:i:s')?></p>
                            <p><b>Usuario:</b> <?php echo $_usu->getNombre_usu() . " " . $_usu->getApellidoP_usu() . " " . $_usu->getApellidoM_usu() ?></p>
                        </div>
                        <button class="btns btns-primary btns-lg" id="button_ins" onclick="save_alum()">Guardar</button>
                    </div>
                </td>
                <td id="column_two">
                    <div id="box_menus">
                        <?php
                        require_once '../estandares/menu_derecho.php';
                        ?>
                        <ul>
                            <li><a href="interesado.php" class="link">Nuevo</a></li>
                            <?php
                            if ($Id_alum > 0) {
                                if(count($DaoOfertasAlumno->getOfertasAlumno($Id_alum))>0){
                                    if (isset($perm['35'])) {
                                        ?>
                                        <li><a href="cobranza.php?id=<?php echo $Id_alum?>" class="link">Cobrar</a></li>
                                        <?php
                                    }
                                    if (isset($perm['32'])) {
                                        ?>
                                        <li><span onclick="box_documents()">Documentos</span></li>
                                        <?php
                                    }
                                }
                            }
                            if ($Id_alum > 0) {
                                ?>
                                <li><span onclick="mostrarFinder()">A&ntilde;adir fotografia</span></li>
                                <li><span onclick="box_curso()">A&ntilde;adir Oferta</span></li>
                                <li><span onclick="box_tutor()">A&ntilde;adir tutor</span></li>
                                <li><span onclick="box_curso_interes()">A&ntilde;adir Oferta de inter&eacute;s</span></li>
                                <?php
                                if (isset($perm['36'])) {
                                    ?>
                                    <li><a href="aplicar_becas.php?id=<?php echo $Id_alum ?>" class="link">Becas</a></li>
                                    <li><span onclick="imprimir()">Imprimir solicitud</span></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                        <input type="file" id="files" name="files" multiple="">
                    </div>
                </td>
            </tr>
        </table>
        <input type="hidden" id="Id_alumn" value="<?php echo $Id_alum ?>"/>
        <input type="hidden" id="Id_dir" value="<?php echo $Id_dir ?>"/>   
<?php
}



if(isset($_POST['action']) && $_POST['action']=="inscribir"){
    $base = new base();
    $DaoUsuarios = new DaoUsuarios();
    $DaoAlumnos = new DaoAlumnos();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoOfertas = new DaoOfertas();
    $DaoCiclos = new DaoCiclos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoPlanteles = new DaoPlanteles();
    $DaoDirecciones = new DaoDirecciones();
    $DaoMateriasCicloAlumno = new DaoMateriasCicloAlumno();
    $DaoMaterias = new DaoMaterias();
    $DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
    $DaoCursosEspecialidad=new DaoCursosEspecialidad();
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $params=$DaoParametrosPlantel->getParametrosPlantelArray();
    
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $alum = $DaoAlumnos->show($_POST['Id_int']);
    $ofertaAlumno = $DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $oferta = $DaoOfertas->show($ofertaAlumno->getId_ofe());
    if ($ofertaAlumno->getOpcionPago() == 2) {

        $cicloAlumno = $DaoCiclosAlumno->getPrimerCicloOferta($ofertaAlumno->getId());
        $esp = $DaoEspecialidades->show($ofertaAlumno->getId_esp());

        if($oferta->getTipoOferta()==1){
            //Se generan los pagos apartir de la fecha de inicio del ciclo
            //Generar cargos para las mensualidades
            $ciclo = $DaoCiclos->show($cicloAlumno->getId_ciclo());
            $fecha_pago = $ciclo->getFecha_ini();
            $anio = date("Y", strtotime($fecha_pago));
            $mes = date("m", strtotime($fecha_pago));

            //Generar mensualidades
            for ($i = 1; $i <= $esp->getNum_pagos(); $i++) {
                
                $d=1;
                if(isset($params['DiaInicioMensualidades']) && strlen($params['DiaInicioMensualidades'])>0){
                   $d=$params['DiaInicioMensualidades'];
                }
                $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes + $i, $d, $anio));

                $Precio_curso = ($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos();
                $PagosCiclo = new PagosCiclo();
                $PagosCiclo->setConcepto("Mensualidad");
                $PagosCiclo->setFecha_pago($base->get_day_pago($fecha_pago, $ciclo->getId()));
                $PagosCiclo->setMensualidad($Precio_curso);
                $PagosCiclo->setId_ciclo_alum($cicloAlumno->getId());
                $PagosCiclo->setId_alum($_POST['Id_int']);
                $PagosCiclo->setTipo_pago("pago");
                $PagosCiclo->setTipoRel("alum");
                $PagosCiclo->setIdRel($_POST['Id_int']);
                $DaoPagosCiclo->add($PagosCiclo);
            }
        }elseif($oferta->getTipoOferta()==2){
            $fecha_pago=$DaoCursosEspecialidad->getFechaPago($ofertaAlumno->getId_curso_esp());
            $anio = date("Y", strtotime($fecha_pago));
            $mes = date("m", strtotime($fecha_pago));
            $day= date("d", strtotime($fecha_pago));

            //Generar mensualidades
            for ($i = 1; $i <= $esp->getNum_pagos(); $i++) {
                $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes +$i, $day , $anio));
                
                $Precio_curso = ($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos();
                $PagosCiclo = new PagosCiclo();
                $PagosCiclo->setConcepto("Mensualidad");
                $PagosCiclo->setFecha_pago($fecha_pago);
                $PagosCiclo->setMensualidad($Precio_curso);
                $PagosCiclo->setId_ciclo_alum($cicloAlumno->getId());
                $PagosCiclo->setId_alum($_POST['Id_int']);
                $PagosCiclo->setTipo_pago("pago");
                $PagosCiclo->setTipoRel("alum");
                $PagosCiclo->setIdRel($_POST['Id_int']);
                $DaoPagosCiclo->add($PagosCiclo);
            }
        }

        //Cargar materias que cursara en el ciclo
        $query = "SELECT * FROM Materias_especialidades 
                           JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat
                  WHERE Id_esp=" . $ofertaAlumno->getId_esp() . " AND Grado_mat=" . $cicloAlumno->getId_grado() . " AND Activo_mat_esp=1 AND Activo_mat=1";
        foreach ($DaoMateriasEspecialidad->advanced_query($query) as $matEsp) {
            $query = "SELECT * FROM materias_ciclo_ulm WHERE Id_ciclo_alum=" . $cicloAlumno->getId() . " AND Id_mat_esp=" . $matEsp->getId() . " AND Id_alum=" . $_POST['Id_int'];
            $materia = $DaoMaterias->existeQuery($query);
            if ($materia->getId() == 0) {
                $MateriasCicloAlumno = new MateriasCicloAlumno();
                $MateriasCicloAlumno->setId_ciclo_alum($cicloAlumno->getId());
                $MateriasCicloAlumno->setId_mat_esp($matEsp->getId());
                $MateriasCicloAlumno->setTipo(1);
                $MateriasCicloAlumno->setActivo(1);
                $MateriasCicloAlumno->setId_alum($_POST['Id_int']);
                $DaoMateriasCicloAlumno->add($MateriasCicloAlumno);
            }
        }
    }
    //Captura de historial
    $TextoHistorial = "Inscribe al alumno " . $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() . ", en " . $oferta->getNombre_oferta();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial, "Alumnos");

    update_page($_POST['Id_int']);
}

if (isset($_POST['action']) && $_POST['action'] == "add_ofe_alum") {
    $DaoUsuarios = new DaoUsuarios();
    $DaoDirecciones = new DaoDirecciones();
    $DaoAlumnos = new DaoAlumnos();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoCiclos = new DaoCiclos();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoGrados = new DaoGrados();
    $DaoOfertas= new DaoOfertas();
    $DaoCursosEspecialidad= new DaoCursosEspecialidad();
    
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $alum=$DaoAlumnos->show($_POST['Id_alumn']);
    $oferta=$DaoOfertas->show($_POST['Id_oferta']);
    $esp=$DaoEspecialidades->show($_POST['Id_esp']);
    
    $resp = array();

    //Validamos si ya existe la oferta y si no entonces insertamos la nueva
    $query = "SELECT * FROM ofertas_alumno WHERE Id_ofe=" . $_POST['Id_oferta'] . " AND Id_alum= " . $_POST['Id_alumn'] . " AND Id_esp=" . $_POST['Id_esp']." AND Activo_oferta=1 AND FechaCapturaEgreso IS NULL AND IdUsuEgreso IS NULL";
    if ((isset($_POST['Id_ori']) && $_POST['Id_ori'] > 0) && isset($_POST['Id_curso_esp']) && $_POST['Id_curso_esp'] > 0) {
        $query = "SELECT * FROM ofertas_alumno WHERE Id_ofe=" .$_POST['Id_oferta'] . " AND Id_alum= " . $_POST['Id_alumn'] . " AND Id_esp=" . $_POST['Id_esp'] . " AND Id_ori=" . $_POST['Id_ori']." AND Id_curso_esp=" . $_POST['Id_curso_esp']." AND Activo_oferta=1 AND FechaCapturaEgreso IS NULL AND IdUsuEgreso IS NULL";
    }elseif (isset($_POST['Id_ori']) && $_POST['Id_ori'] > 0) {
        $query = "SELECT * FROM ofertas_alumno WHERE Id_ofe=" . $_POST['Id_oferta'] . " AND Id_alum= " . $_POST['Id_alumn'] . " AND Id_esp=" . $_POST['Id_esp'] . " AND Id_ori=" . $_POST['Id_ori']." AND Activo_oferta=1 AND FechaCapturaEgreso IS NULL AND IdUsuEgreso IS NULL";
    }elseif(isset($_POST['Id_curso_esp']) && $_POST['Id_curso_esp'] > 0){
        $query = "SELECT * FROM ofertas_alumno WHERE Id_ofe=" . $_POST['Id_oferta'] . " AND Id_alum= " . $_POST['Id_alumn'] . " AND Id_esp=" . $_POST['Id_esp'] . " AND Id_curso_esp=" . $_POST['Id_curso_esp']." AND Activo_oferta=1 AND FechaCapturaEgreso IS NULL AND IdUsuEgreso IS NULL";
    }
    $existeOferta = $DaoOfertasAlumno->existeQuery($query);
    if ($existeOferta->getId() == 0) {

        //Insertamos la oferta del alumno
        $OfertasAlumno = new OfertasAlumno();
        $OfertasAlumno->setId_ofe($_POST['Id_oferta']);
        $OfertasAlumno->setId_alum($_POST['Id_alumn']);
        $OfertasAlumno->setId_esp($_POST['Id_esp']);
        $OfertasAlumno->setOpcionPago($_POST['opcion']);
        $OfertasAlumno->setActivo(1);
        $OfertasAlumno->setTurno($_POST['Turno']);
        $OfertasAlumno->setAlta_ofe(date('Y-m-d'));
        if(isset($_POST['Id_ori']) && $_POST['Id_ori']>0){
          $OfertasAlumno->setId_ori($_POST['Id_ori']);
        }
        if(isset($_POST['Id_curso_esp']) && $_POST['Id_curso_esp']>0){
          $OfertasAlumno->setId_curso_esp($_POST['Id_curso_esp']);
        }
        $id_oferta_alumno = $DaoOfertasAlumno->add($OfertasAlumno);

        //1= por ciclos, 2= sin ciclos
        if($oferta->getTipoOferta()==1){
           $ciclo = $DaoCiclos->show($_POST['Id_ciclo']);
           $fecha_pago = $ciclo->getFecha_ini();
        }else{
           $fecha_pago=$DaoCursosEspecialidad->getFechaPagoInscripcion($_POST['Id_curso_esp']);
        }

        //Insertamos el primer ciclo del alumno
        $CiclosAlumno = new CiclosAlumno();
        $CiclosAlumno->setId_grado($_POST['Id_grado']);
        $CiclosAlumno->setId_ofe_alum($id_oferta_alumno);
        if(isset($_POST['Id_ciclo']) && $_POST['Id_ciclo']>0){
          $CiclosAlumno->setId_ciclo($_POST['Id_ciclo']);
        }else{
          $CiclosAlumno->setId_ciclo(0);
        }
        $id_ciclo_alumno = $DaoCiclosAlumno->add($CiclosAlumno);

        //Insertamos primer cargo por inscripcion
        $PagosCiclo = new PagosCiclo();
        $PagosCiclo->setConcepto("Inscripción");
        $PagosCiclo->setFecha_pago($fecha_pago);
        $PagosCiclo->setMensualidad($esp->getInscripcion_curso());
        $PagosCiclo->setId_ciclo_alum($id_ciclo_alumno);
        $PagosCiclo->setId_alum($_POST['Id_alumn']);
        $PagosCiclo->setTipo_pago('pago');
        $PagosCiclo->setTipoRel('alum');
        $PagosCiclo->setIdRel($_POST['Id_alumn']);
        $DaoPagosCiclo->add($PagosCiclo);
    }           

    //Captura de historial
    $TextoHistorial="Añade nueva oferta a ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ".$oferta->getNombre_oferta().", ".$esp->getNombre_esp();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Alumnos");

    update_page($_POST['Id_alumn']);
}




/*


if($_POST['action']=="inscribir"){
    //esta mal,ver laseccion de interesados_aj
    //Metodo de pago plan completo
    $class_interesados = new class_interesados($_POST['Id_int']);
    $alum=$class_interesados->get_interesado();
    $oferta_alum=$class_interesados->get_oferta_alumno($_POST['Id_ofe_alum']);
    
    if($oferta_alum['Opcion_pago']==2){
    $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=" . $oferta_alum['Id_ofe_alum']." AND Id_ciclo=".$oferta_alum['Ciclos_oferta'][0]['Id_ciclo'];
    $ciclos_alum_ulm= mysql_query($query_ciclos_alum_ulm, $cnn) or die(mysql_error());
    $row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm);

    $query_ciclos_ulm = "SELECT * FROM ciclos_ulm WHERE Id_ciclo=".$oferta_alum['Ciclos_oferta'][0]['Id_ciclo'];
    $ciclos_ulm = mysql_query($query_ciclos_ulm, $cnn) or die(mysql_error());
    $row_ciclos_ulm= mysql_fetch_array($ciclos_ulm);

    //Se generan los pagos apartir de la fecha deinicio del ciclo
     $fecha_pago=$row_ciclos_ulm['Fecha_ini'];
     $anio=date("Y",strtotime($row_ciclos_ulm['Fecha_ini']));
     $mes=date("m",strtotime($row_ciclos_ulm['Fecha_ini']));

     $class_ofertas= new class_ofertas();
     $esp=$class_ofertas->get_especialidad($oferta_alum['Id_esp']);
      
    //Insertamos las mensualidades
    for($i=1;$i<=$esp['Num_pagos'];$i++){
        if($i==1){
            $fecha_pago=$row_ciclos_ulm['Fecha_ini'];    
        }else{
          $fecha_pago=date("Y-m-d",mktime(0, 0, 0, $mes+($i-1),5, $anio));
        }
        $Precio_curso=($esp['Precio_curso']/$esp['Num_ciclos'])/$esp['Num_pagos'];

    $sql_nueva_entrada = sprintf("INSERT INTO Pagos_ciclo (Concepto, Fecha_pago, Mensualidad, Id_ciclo_alum,Id_alum,tipo_pago) VALUES (%s,%s, %s, %s, %s, %s);", 
          GetSQLValueString("Mensualidad" , "text"), 
          GetSQLValueString($class_ofertas->get_day_pago($fecha_pago,$oferta_alum['Ciclos_oferta'][0]['Id_ciclo']) , "date"), 
          GetSQLValueString($Precio_curso , "double"), 
          GetSQLValueString($row_ciclos_alum_ulm['Id_ciclo_alum'] , "int"),
          GetSQLValueString($_POST['Id_int'] , "int"),
          GetSQLValueString("pago" , "text"));
          $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    } 
    
    //Cargar materias
    //Obtener las materias que cursara en el ciclo
    $query_materias_ciclos_ulm = "SELECT * FROM Materias_especialidades
    JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat 
    WHERE Id_esp=".$oferta_alum['Id_esp']." AND Grado_mat=".$row_ciclos_alum_ulm['Id_grado']." AND Activo_mat_esp=1 AND Activo_mat=1";
    $materias_ciclos_ulm = mysql_query($query_materias_ciclos_ulm, $cnn) or die(mysql_error());
    $row_materias_ciclos_ulm = mysql_fetch_assoc($materias_ciclos_ulm);
    $totalRows_materias_ciclos_ulm = mysql_num_rows($materias_ciclos_ulm);
    if($totalRows_materias_ciclos_ulm>0){
        do{
           $query_materias= "SELECT * FROM materias_ciclo_ulm
            WHERE Id_ciclo_alum=".$row_ciclos_alum_ulm['Id_ciclo_alum']." AND Id_mat_esp=".$row_materias_ciclos_ulm['Id_mat_esp']." AND Id_alum=".$_POST['Id_int'];
            $materias = mysql_query($query_materias, $cnn) or die(mysql_error());
            $row_materias = mysql_fetch_assoc($materias);
            $totalRows_materias = mysql_num_rows($materias);
            if($totalRows_materias==0){ 
                   $sql_nueva_entrada = sprintf("INSERT INTO materias_ciclo_ulm (Id_ciclo_alum, Id_mat_esp,tipo,Activo,Id_alum) VALUES (%s,%s,%s,%s,%s);", 
                      GetSQLValueString($row_ciclos_alum_ulm['Id_ciclo_alum'], "int"), 
                      GetSQLValueString($row_materias_ciclos_ulm['Id_mat_esp'] , "int"),
                      GetSQLValueString(1 , "int"),
                      GetSQLValueString(1 , "int"),
                      GetSQLValueString($_POST['Id_int'] , "int"));
                      $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
            }

        }while($row_materias_ciclos_ulm = mysql_fetch_assoc($materias_ciclos_ulm));
    }  

    }
    update_page($_POST['Id_int']);
}

 if ($_POST['action'] == "add_ofe_alum") {
      //Validamos si ya esxiste la oferta y si no entonces insertamos la nueva
     $query_oferta_alumno = "SELECT * FROM ofertas_alumno WHERE Id_ofe=".$_POST['Id_oferta']." AND Id_alum= ".$_POST['Id_alumn']." AND Id_esp=".$_POST['Id_esp']." AND Activo_oferta=1";
     if($_POST['Id_ori']>0){
        $query_oferta_alumno = "SELECT * FROM ofertas_alumno WHERE Id_ofe=".$_POST['Id_oferta']." AND Id_alum= ".$_POST['Id_alumn']." AND Id_esp=".$_POST['Id_esp']." AND Id_ori=".$_POST['Id_ori']." AND Activo_oferta=1";
     }
     $oferta_alumno = mysql_query($query_oferta_alumno, $cnn) or die(mysql_error());
     $row_oferta_alumno= mysql_fetch_array($oferta_alumno);
     $totalRows_oferta_alumno = mysql_num_rows($oferta_alumno);
     if($totalRows_oferta_alumno==0){

      //Insertamos la oferta del alumno
      $sql_nueva_entrada = sprintf("INSERT INTO ofertas_alumno (Id_ofe, Id_alum, Id_esp, Id_ori,Opcion_pago,Activo_oferta,Turno,Alta_ofe) VALUES (%s,%s, %s, %s,%s,%s, %s, %s);",
              GetSQLValueString($_POST['Id_oferta'] , "int"),
              GetSQLValueString($_POST['Id_alumn'] , "int"), 
              GetSQLValueString($_POST['Id_esp'] , "int"), 
              GetSQLValueString($_POST['Id_ori'] , "int"), 
              GetSQLValueString($_POST['opcion'] , "int"),
              GetSQLValueString(1 , "int"),
              GetSQLValueString($_POST['Turno'] , "int"),
              GetSQLValueString(date('Y-m-d'), "date"));
      $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
      $id_oferta_alumno = mysql_insert_id($cnn);

      //Insertamos el primer ciclo del alumno
      $sql_nueva_entrada = sprintf("INSERT INTO ciclos_alum_ulm (Id_ciclo, Id_grado, Id_ofe_alum) VALUES (%s,%s, %s);", 
          GetSQLValueString($_POST['Id_ciclo'] , "int"), 
          GetSQLValueString($_POST['Id_grado'] , "int"), 
          GetSQLValueString($id_oferta_alumno, "int"));
      $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error()); 
      $id_ciclo_alumno = mysql_insert_id($cnn);

      //En esta seccion solo insertamos las Inscripciones
      //En alumnos cuando damos inscribir se insertaran los pagos
      $query_ciclos_ulm = "SELECT * FROM ciclos_ulm WHERE Id_ciclo=".$_POST['Id_ciclo'];
      $ciclos_ulm = mysql_query($query_ciclos_ulm, $cnn) or die(mysql_error());
      $row_ciclos_ulm= mysql_fetch_array($ciclos_ulm);

      $fecha_pago=$row_ciclos_ulm['Fecha_ini'];

      $class_ofertas= new class_ofertas();
      $esp=$class_ofertas->get_especialidad($_POST['Id_esp']);

      $ciclos=0;
      if($esp['Tipo_insc']==1){
             if($esp['Tipo_ciclos']==1){
                    $ciclos=2; 
             }elseif($esp['Tipo_ciclos']==2){
                    $ciclos=6; 
             }elseif($esp['Tipo_ciclos']==3){
                    $ciclos=12; 
             }elseif($esp['Tipo_ciclos']==4){
                    $ciclos=3; 
             }
      }elseif($esp['Tipo_insc']==2){

             if($esp['Tipo_ciclos']==1){
                    $ciclos=1; 
             }elseif($esp['Tipo_ciclos']==2){
                    $ciclos=2; 
             }elseif($esp['Tipo_ciclos']==3){
                    $ciclos=4; 
             }elseif($esp['Tipo_ciclos']==4){
                    $ciclos=1; 
             }
      }

      $query_grados_oferta = "SELECT * FROM grados_ulm WHERE Id_grado_ofe=".$_POST['Id_grado'];
      $grados_oferta = mysql_query($query_grados_oferta, $cnn) or die(mysql_error());
      $row_grados_oferta= mysql_fetch_array($grados_oferta);

      $grado_actual=$row_grados_oferta['Grado'];
      $modulo=$grado_actual % $ciclos;

      $sql_nueva_entrada = sprintf("INSERT INTO Pagos_ciclo (Concepto, Fecha_pago, Mensualidad, Id_ciclo_alum,Id_alum,tipo_pago) VALUES (%s,%s, %s, %s, %s, %s);", 
              GetSQLValueString("Inscripción" , "text"), 
              GetSQLValueString($fecha_pago , "date"), 
              GetSQLValueString($esp['Inscripcion_curso'] , "double"), 
              GetSQLValueString($id_ciclo_alumno , "int"),
              GetSQLValueString($_POST['Id_alumn'] , "int"),
              GetSQLValueString('pago' , "text"));
        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
        
        
        //Captura de historial
        $DaoUsuarios= new DaoUsuarios();
        $DaoAlumnos= new DaoAlumnos();
        $DaoOfertas= new DaoOfertas();
        $DaoEspecialidades= new DaoEspecialidades();
        
        $alum=$DaoAlumnos->show($_POST['Id_alumn']);
        $ofe=$DaoOfertas->show($_POST['Id_oferta']);
        $esp=$DaoEspecialidades->show($_POST['Id_esp']);
        $TextoHistorial="Añade nueva oferta a ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ".$ofe->getNombre_oferta().", ".$esp->getNombre_esp();
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Documentos del alumno");
      
        update_page($_POST['Id_alumn']);
      }  
}
 * 
if(isset($_POST['action']) && $_POST['action']=="cambiar_plan_pago"){
    //if(isset($_COOKIE['test_admin'])){
    $DaoAlumnos= new DaoAlumnos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoCiclos= new DaoCiclos();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoRecargos= new DaoRecargos();
    $DaoPagos= new DaoPagos();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoGrados= new DaoGrados();
    $base= new base();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);   
    
    //Obtenemos el ciclo actual de la oferta del alumno
    $query = "SELECT ciclos_alum_ulm.* FROM ofertas_alumno 
    JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
    WHERE ofertas_alumno.Id_ofe_alum=".$_POST['Id_ofe_alum']." ORDER BY Id_ciclo_alum DESC LIMIT 1";
    $ciclo_actual=$base->advanced_query($query);
    $row_ciclo_actual= $ciclo_actual->fetch_assoc();

    //Actualizamos el nuevo plan de pagos
    $OfertasAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $OfertasAlumno->setOpcionPago($_POST['opcionPago']);
    $DaoOfertasAlumno->update($OfertasAlumno); 

    $query = "SELECT * FROM ciclos_alum_ulm WHERE Id_ciclo=".$row_ciclo_actual['Id_ciclo']." AND Id_ofe_alum=".$_POST['Id_ofe_alum'];
    $ciclos_alum_ulm=$base->advanced_query($query);
    $row_ciclos_alum_ulm= $ciclos_alum_ulm->fetch_assoc();
    $totalRows_ciclos_alum_ulm= $ciclos_alum_ulm->num_rows;  
    if($totalRows_ciclos_alum_ulm>0){

        //Eliminamos todas las materias del ciclo en que se encuentra el alumno
        //$DaoMateriasCicloAlumno->deleteMateriasByIdCicloAlumno($row_ciclos_alum_ulm['Id_ciclo_alum']);

        //Eliminamos todos los cargos del ciclo en que se encuentra el alumno
        foreach($DaoPagosCiclo->getCargosCiclo($row_ciclos_alum_ulm['Id_ciclo_alum']) as $k3=>$v3){
            //Por cuestiones de seguridad no se eliman los Pagos_pagos del cargo ya se son folios seriados
            //Y puede que se enviarion por email o se entregaron al alumno
            
            $sql_nueva_entrada = sprintf("DELETE FROM Seguimiento_pago WHERE Id_pago=%s", 
            GetSQLValueString($v3->getId(), "int"));
            $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

            $DaoRecargos->deleteRecargosPago($v3->getId());
            $DaoPagosCiclo->delete($v3->getId());
        }

        //Obtenemos los datos del ciclo
        $ciclo=$DaoCiclos->show($row_ciclo_actual['Id_ciclo']);
        $fecha_pago=$ciclo->getFecha_ini();
        $anio=date("Y",strtotime($ciclo->getFecha_ini()));
        $mes=date("m",strtotime($ciclo->getFecha_ini()));

         //Seleccionamos la especialidad
        $esp=$DaoEspecialidades->show($OfertasAlumno->getId_esp());

        //Seleccionamos el grado actual
        $grado=$DaoGrados->show($row_ciclos_alum_ulm['Id_grado']);

        //Verificamos si existe el nuevo grado
        $Id_grado_ofe=$grado->getId();

        //Verificamos si aplica la inscripcion            
        $ciclos=0;
         if($esp->getTipo_insc()==1){
                 if($esp->getTipo_ciclos()==1){
                        $ciclos=2; 
                 }elseif($esp->getTipo_ciclos()==2){
                        $ciclos=6; 
                 }elseif($esp->getTipo_ciclos()==3){
                        $ciclos=12; 
                 }elseif($esp->getTipo_ciclos()==4){
                        $ciclos=3; 
                 }
          }elseif($esp->getTipo_insc()==2){

                 if($esp->getTipo_ciclos()==1){
                        $ciclos=1; 
                 }elseif($esp->getTipo_ciclos()==2){
                        $ciclos=2; 
                 }elseif($esp->getTipo_ciclos()==3){
                        $ciclos=4; 
                 }elseif($esp->getTipo_ciclos()==4){
                        $ciclos=1; 
                 }
          }
          $grado_actual=$grado->getGrado();
          $modulo=$grado_actual % $ciclos;

           //Tipo_insc 1= anual, 2 cuatrimenstral, 3 unica
           //Tipo_ciclos (Semestre=1, Bimestre=2, Mensual=3, Cuatrimestre=4)
           //Eliminar el if despues de que se carguen todos los alumnos

           //Tipo de inscripcion 1
           //Tipo ciclo es 1(semestre) o 2(Bimestre) o 3(Mensual) o 4(Cuatrimestre)
           //modulo cera simepre 1

            //Tipo de inscripcion 2
            //Tipo ciclo es 1(semestre) o 4(Cuatrismetrs)
            //modulo cera simepre 0

            //Tipo de inscripcion 2
            //Tipo ciclo es 2(Bimestre) o 3(Mensual)
            //modulo cera simepre 1

           if(($esp->getTipo_insc()==2 && (($esp->getTipo_ciclos()==1 || $esp->getTipo_ciclos()==4) && $modulo==0)  || (($esp->getTipo_ciclos()==2 || $esp->getTipo_ciclos()==3) && $modulo==1))
           || ($esp->getTipo_insc()==1 && $modulo==1) || ($esp->getTipo_insc()==3 && $grado_actual==1)){

              //Insertamos el cargo por inscripcion
              $PagosCiclo= new PagosCiclo();
              $PagosCiclo->setConcepto("Inscripción");
              $PagosCiclo->setFecha_pago($base->get_day_pago($fecha_pago,$row_ciclo_actual['Id_ciclo']));
              $PagosCiclo->setMensualidad($esp->getInscripcion_curso());
              $PagosCiclo->setId_ciclo_alum($row_ciclos_alum_ulm['Id_ciclo_alum']);
              $PagosCiclo->setId_alum($_POST['Id_alum']);
              $PagosCiclo->setTipo_pago("pago");
              $Id_pago=$DaoPagosCiclo->add($PagosCiclo);
            }  


        if($_POST['opcionPago']==2){
               //Insertamos los cargos para las mensualidades
                for($i=1;$i<=$esp->getNum_pagos();$i++){
                    //Se generan los cargos  apartir de la fecha de inicio del ciclo
                    if($i==1){
                        $fecha_pago=$ciclo->getFecha_ini();    
                    }else{
                      $fecha_pago=date("Y-m-d",mktime(0, 0, 0, $mes+($i-1),5, $anio));
                    }
                    $Precio_curso=($esp->getPrecio_curso()/$esp->getNum_ciclos())/$esp->getNum_pagos();

                    $PagosCiclo= new PagosCiclo();
                    $PagosCiclo->setConcepto("Mensualidad");
                    $PagosCiclo->setFecha_pago($base->get_day_pago($fecha_pago,$row_ciclo_actual['Id_ciclo']));
                    $PagosCiclo->setMensualidad($Precio_curso);
                    $PagosCiclo->setId_ciclo_alum($row_ciclos_alum_ulm['Id_ciclo_alum']);
                    $PagosCiclo->setId_alum($_POST['Id_alum']);
                    $PagosCiclo->setTipo_pago("pago");
                    $Id_pago=$DaoPagosCiclo->add($PagosCiclo);
                    //echo "Insertamos mensualidad<br>";
                } 

                //Seleccionamos el ciclo del alumno anterior
                $mat_reprobadas= array();
                $cicloAlumno=$DaoCiclosAlumno->getCicloAnteriorOferta($row_ciclo_actual['Id_ciclo'], $_POST['Id_ofe_alum']);
                if($cicloAlumno->getId()>0){
                    //Seleccionar las materias del ciclo anterior para comprobar si las paso
                    $mat_rep=0;
                    foreach($DaoMateriasCicloAlumno->getMateriasCicloAlumno($cicloAlumno->getId()) as $k2=>$v2){
                        $mat_esp=$DaoMateriasEspecialidad->show($v2->getId_mat_esp());
                        $mat=$DaoMaterias->show($mat_esp->getId_mat());
                        if($v2->getCalTotalParciales()>=$mat->getPromedio_min()|| $v2->getCalTotalParciales()=="CP" || $v2->getCalTotalParciales()=="AC" || $v2->getCalExtraordinario()>=$mat->getPromedio_min() || $v2->getCalEspecial()>=$mat->getPromedio_min()){
                        }else{
                              //Validar si no existe la materia
                              $existeMat=$DaoMateriasCicloAlumno->getMateriaCicloAlumno($row_ciclos_alum_ulm['Id_ciclo_alum'], $_POST['Id_alum'], $v2->getId_mat_esp());
                              if($existeMat->getId()==0){
                                  //Se inserta la misma porque reprobo
                                  $MateriasCicloAlumno=new MateriasCicloAlumno();
                                  $MateriasCicloAlumno->setId_ciclo_alum($row_ciclos_alum_ulm['Id_ciclo_alum']);
                                  $MateriasCicloAlumno->setId_alum($_POST['Id_alum']);
                                  $MateriasCicloAlumno->setId_mat_esp($v2->getId_mat_esp());
                                  $MateriasCicloAlumno->setTipo($v2->getTipo());
                                  $MateriasCicloAlumno->setActivo($v2->getActivo());
                                  $DaoMateriasCicloAlumno->add($MateriasCicloAlumno);

                                  //Guardar el id de la materia consecutiva para no insertarla
                                  $query = "SELECT * FROM prerequisitos_ulm 
                                  JOIN Materias_especialidades ON prerequisitos_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp 
                                  JOin materias_uml ON Materias_especialidades.Id_mat=materias_uml.Id_mat
                                  WHERE Id_mat_esp_pre=".$v2->getId_mat_esp();
                                  $prerequisitos=$base->advanced_query($query);
                                  $row_prerequisitos= $prerequisitos->fetch_assoc();
                                  $totalRows_prerequisitos= $prerequisitos->num_rows;  
                                  if($totalRows_prerequisitos>0){
                                     array_push($mat_reprobadas, $row_prerequisitos['Id_mat_esp']);
                                  }
                                  $mat_rep++;
                              }
                        }
                    }
                }

                //Cargar materias del nuevo ciclo
                //Obtener las materias que cursara en el ciclo
                $query = "SELECT * FROM Materias_especialidades 
                 JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat
                 WHERE Id_esp=".$OfertasAlumno->getId_esp()." AND Grado_mat=".$Id_grado_ofe." AND Activo_mat_esp=1 AND Activo_mat=1";
                $materias_ciclos_ulm=$base->advanced_query($query);
                $row_materias_ciclos_ulm= $materias_ciclos_ulm->fetch_assoc();
                $totalRows_materias_ciclos_ulm= $materias_ciclos_ulm->num_rows;  
                if($totalRows_materias_ciclos_ulm>0){
                    $limiteDeMatInsertar=$totalRows_materias_ciclos_ulm-$mat_rep;
                    $CountMatInsertadas=0;
                    do{
                       //Si no el id de la materia no se envuentra en el array entonces insertar
                       if(!in_array($row_materias_ciclos_ulm['Id_mat_esp'], $mat_reprobadas) && $CountMatInsertadas<$limiteDeMatInsertar){
                              //Validar si no existe la materia
                              $existeMat=$DaoMateriasCicloAlumno->getMateriaCicloAlumno($row_ciclos_alum_ulm['Id_ciclo_alum'], $_POST['Id_alum'], $row_materias_ciclos_ulm['Id_mat_esp']);
                              if($existeMat->getId()==0){
                                  $MateriasCicloAlumno=new MateriasCicloAlumno();
                                  $MateriasCicloAlumno->setId_ciclo_alum($row_ciclos_alum_ulm['Id_ciclo_alum']);
                                  $MateriasCicloAlumno->setId_alum($_POST['Id_alum']);
                                  $MateriasCicloAlumno->setId_mat_esp($row_materias_ciclos_ulm['Id_mat_esp']);
                                  $MateriasCicloAlumno->setTipo($row_materias_ciclos_ulm['Tipo']);
                                  $MateriasCicloAlumno->setActivo($row_materias_ciclos_ulm['Activo']);
                                  $DaoMateriasCicloAlumno->add($MateriasCicloAlumno);
                                  $CountMatInsertadas++;
                              }
                       }
                    }while( $row_materias_ciclos_ulm= $materias_ciclos_ulm->fetch_assoc());
                } 

        }
    }
       
    //}
    update_page($_POST['Id_alum']); 
}

if(isset($_POST['action']) && $_POST['action']=="cambiar_oferta"){
    //if(isset($_COOKIE['test_admin'])){
    $DaoAlumnos= new DaoAlumnos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoCiclos= new DaoCiclos();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoRecargos= new DaoRecargos();
    $DaoPagos= new DaoPagos();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoOfertas= new DaoOfertas();
    $base= new base();
    
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    //Obtenemos el ciclo actual de la oferta del alumno
    $query = "SELECT ciclos_alum_ulm.* FROM ofertas_alumno 
    JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
    WHERE ofertas_alumno.Id_ofe_alum=".$_POST['Id_ofe_alum']." ORDER BY Id_ciclo_alum DESC LIMIT 1";
    $ciclo_actual=$base->advanced_query($query);
    $row_ciclo_actual= $ciclo_actual->fetch_assoc();

    $esp=$DaoEspecialidades->show($_POST['Id_esp']);

    //Validamos si ya esxiste la oferta y si no entonces insertamos la nueva
    $query = "SELECT * FROM ofertas_alumno WHERE Id_ofe=".$_POST['Id_oferta']." AND Id_alum= ".$_POST['Id_alumn']." AND Id_esp=".$_POST['Id_esp']." AND Activo_oferta=1";
    $totalRows_oferta_alumno=0;
    foreach($base->advanced_query($query) as $k=>$v){
        $totalRows_oferta_alumno++;
    }
    if($totalRows_oferta_alumno==0){
     //echo "La oferta no existia  <br>";

     //Actualizamos la oferta actual como baja
     $OfertasAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
     $OfertasAlumno->setActivo(0);
     $OfertasAlumno->setBaja_ofe(date('Y-m-d H:i:s'));
     $OfertasAlumno->setId_mot_baja("7");
     $OfertasAlumno->setComentario_baja("Cambio de oferta");
     $OfertasAlumno->setId_usu_captura_baja($_COOKIE['admin/Id_usu']);
     $DaoOfertasAlumno->update($OfertasAlumno); 

      //Insertamos la oferta nueva del alumno
      $OfertasAlumno= new OfertasAlumno();
      $OfertasAlumno->setId_ofe($_POST['Id_oferta']);
      $OfertasAlumno->setId_alum($_POST['Id_alumn']);
      $OfertasAlumno->setId_esp($_POST['Id_esp']);
      $OfertasAlumno->setId_ori($_POST['Id_ori']);
      $OfertasAlumno->setOpcionPago($_POST['opcion']);
      $OfertasAlumno->setActivo(1);
      $OfertasAlumno->setTurno($_POST['Turno']);
      $OfertasAlumno->setAlta_ofe(date('Y-m-d'));
      $id_oferta_alumno=$DaoOfertasAlumno->add($OfertasAlumno);
      
      //Obtenemos los pagos realizados de la oferta anterior y los pasamos a la nueva oferta
      foreach($DaoPagos->getPagosOfertaAlumno($_POST['Id_ofe_alum']) as $pago){
           $p=$DaoPagos->show($pago->getId());
           //$p->setId_pago_ciclo("");
           $p->setId_ofe_alum($id_oferta_alumno);
           $DaoPagos->update($p);
      }

      //Insertamos el primer ciclo del alumno
      $CiclosAlumno= new CiclosAlumno();
      $CiclosAlumno->setId_ciclo($_POST['Id_ciclo']);
      $CiclosAlumno->setId_grado($_POST['Id_grado']);
      $CiclosAlumno->setId_ofe_alum($id_oferta_alumno);
      $id_ciclo_alumno=$DaoCiclosAlumno->add($CiclosAlumno);

      //Obtenemos los datos del ciclo
      $ciclo=$DaoCiclos->show($_POST['Id_ciclo']);
      $fecha_pago=$ciclo->getFecha_ini();
      $anio=date("Y",strtotime($ciclo->getFecha_ini()));
      $mes=date("m",strtotime($ciclo->getFecha_ini()));

      //Insertamos el cargo por  inscripcion
      $PagosCiclo= new PagosCiclo();
      $PagosCiclo->setConcepto("Inscripción");
      $PagosCiclo->setFecha_pago($base->get_day_pago($fecha_pago,$_POST['Id_ciclo']));
      $PagosCiclo->setMensualidad($esp->getInscripcion_curso());
      $PagosCiclo->setId_ciclo_alum($id_ciclo_alumno);
      $PagosCiclo->setId_alum($_POST['Id_alumn']);
      $PagosCiclo->setTipo_pago("pago");
      $Id_pago=$DaoPagosCiclo->add($PagosCiclo);

      if($_POST['opcion']==2){
            //Insertamos los cargos por las mensualidades
            for($i=1;$i<=$esp->getNum_pagos();$i++){
                //Se generan los cargos  apartir de la fecha de inicio del ciclo
                if($i==1){
                    $fecha_pago=$ciclo->getFecha_ini();    
                }else{
                  $fecha_pago=date("Y-m-d",mktime(0, 0, 0, $mes+($i-1),5, $anio));
                }
                $Precio_curso=($esp->getPrecio_curso()/$esp->getNum_ciclos())/$esp->getNum_pagos();

                $PagosCiclo= new PagosCiclo();
                $PagosCiclo->setConcepto("Mensualidad");
                $PagosCiclo->setFecha_pago($base->get_day_pago($fecha_pago,$_POST['Id_ciclo']));
                $PagosCiclo->setMensualidad($Precio_curso);
                $PagosCiclo->setId_ciclo_alum($id_ciclo_alumno);
                $PagosCiclo->setId_alum($_POST['Id_alumn']);
                $PagosCiclo->setTipo_pago("pago");
                $Id_pago=$DaoPagosCiclo->add($PagosCiclo);
            } 

            //Cargar materias
            //Obtener las materias que cursara en el ciclo
            $query = "SELECT * FROM Materias_especialidades
            JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat 
            WHERE Id_esp=".$_POST['Id_esp']." AND Grado_mat=".$_POST['Id_grado']." AND Activo_mat_esp=1 AND Activo_mat=1";
            foreach($base->advanced_query($query) as $x=>$y){
                    $MateriasCicloAlumno= new MateriasCicloAlumno();
                    $MateriasCicloAlumno->setId_ciclo_alum($id_ciclo_alumno);
                    $MateriasCicloAlumno->setId_mat_esp($y['Id_mat_esp']);
                    $MateriasCicloAlumno->setTipo(1);
                    $MateriasCicloAlumno->setActivo(1);
                    $MateriasCicloAlumno->setId_alum($_POST['Id_alumn']);
                    $DaoMateriasCicloAlumno->add($MateriasCicloAlumno);
                   // echo "Cargamos materias<br>";
            }  
        }


    }else{
       // echo "Ya existia la oferta solo se cambio de orientacion<br>";
        //Si la oferta es la misma, se insertaran la mismas materias que tenia esto esporque si no se perderia el historial de sus materias 
        //y calificaciones de los semestres pasados 
        $OfertasAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
        $OfertasAlumno->setId_ofe($_POST['Id_oferta']);
        $OfertasAlumno->setId_alum($_POST['Id_alumn']);
        $OfertasAlumno->setId_esp($_POST['Id_esp']);
        $OfertasAlumno->setId_ori($_POST['Id_ori']);
        $OfertasAlumno->setOpcionPago($_POST['opcion']);
        $OfertasAlumno->setTurno($_POST['Turno']);
        $DaoOfertasAlumno->update($OfertasAlumno);

        $CiclosAlumno=$DaoCiclosAlumno->show($row_ciclo_actual['Id_ciclo_alum']);
        $CiclosAlumno->setId_grado($_POST['Id_grado']);
        $CiclosAlumno->setId_ciclo($_POST['Id_ciclo']);
        $DaoCiclosAlumno->update($CiclosAlumno);

        if($_POST['opcion']==2){
            //Cargar materias
            //Obtener las materias que cursara en el ciclo
            $query = "SELECT * FROM Materias_especialidades
            JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat 
            WHERE Id_esp=".$_POST['Id_esp']." AND Grado_mat=".$_POST['Id_grado']." AND Activo_mat_esp=1 AND Activo_mat=1";
            foreach($base->advanced_query($query) as $k=>$v){

                     $totalRows_existe_mat=0;
                     $query_existe_mat  = "SELECT * FROM materias_ciclo_ulm WHERE Id_ciclo_alum=" .$row_ciclo_actual['Id_ciclo_alum']." AND Id_mat_esp=".$v['Id_mat_esp'];
                     foreach($base->advanced_query($query_existe_mat) as $k2=>$v2){
                       $totalRows_existe_mat++;
                     }
                     if($totalRows_existe_mat==0){
                        $MateriasCicloAlumno= new MateriasCicloAlumno();
                        $MateriasCicloAlumno->setId_ciclo_alum($row_ciclo_actual['Id_ciclo_alum']);
                        $MateriasCicloAlumno->setId_mat_esp($v['Id_mat_esp']);
                        $MateriasCicloAlumno->setId_ori($v['Id_ori']);
                        $MateriasCicloAlumno->setTipo(1);
                        $MateriasCicloAlumno->setActivo(1);
                        $MateriasCicloAlumno->setId_alum($_POST['Id_alumn']);
                        $DaoMateriasCicloAlumno->add($MateriasCicloAlumno);
                     }
            }  
        }
    }
    
    $alum=$DaoAlumnos->show($_POST['Id_alumn']);
    $ofe=$DaoOfertas->show($_POST['Id_oferta']);
    $esp=$DaoEspecialidades->show($_POST['Id_esp']);
    $TextoHistorial="Cambio de oferta a ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ".$ofe->getNombre_oferta().", ".$esp->getNombre_esp();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Cambio de oferta");
    //}
    update_page($_POST['Id_alumn']);
}


 */



if (isset($_POST['action']) && $_POST['action'] == "box_curso") {
    $DaoOfertas= new DaoOfertas();
    $DaoCiclos= new DaoCiclos();
    $DaoTurnos= new DaoTurnos();
?>
      <div class="box-emergente-form cambiar-oferta">
      <h1>Añadir oferta</h1>
      <p>Oferta de inter&eacute;s<br>
         <select id="oferta" onchange="getTipociclo()">
          <option value="0"></option>
          <?php
          foreach($DaoOfertas->showAll() as $k=>$v){
          ?>
              <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
          <?php
          }
          ?>
        </select>     
      </p>
      <p>Especialidad<br>
        <select id="curso" onchange="update_orientacion_box_curso()">
          <option value="0"></option>
        </select>
      </p>
      <div id="box_orientacion"></div>
      <p>Grado<br>
         <select id="grado">
              <option value="0"></option>
         </select>
      </p>
      <p>Ciclo<br>
       <select id="ciclo">
          <option value="0"></option>
          <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave() ?></option>
              <?php
              }
            ?>
        </select>
      </p>
      <p>Opci&oacute;n de pago<br>
            <select id="opcionPago">
              <option value="0"></option>
              <option value="1">Materias</option>
              <option value="2">Plan Completo</option>
            </select>
      </p>
      <p>Turno<br>
        <select id="turno">
          <option value="0"></option>
            <?php
            foreach($DaoTurnos->getTurnos() as $turno){
                ?>
                <option value="<?php echo $turno->getId()?>"><?php echo $turno->getNombre()?></option>
            <?php
            }
            ?>
        </select>
      </p>
      <p><button onclick="add_ofe_alum()">Agregar</button><button onclick = "ocultar_error_layer()">Cancelar</button></p>
</div>
<?php
}

if ($_POST['action'] == "getTipoCiclo") {
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoCursosEspecialidad= new DaoCursosEspecialidad();
    $DaoCiclos = new DaoCiclos();
    $DaoTurnos= new DaoTurnos();
    $ofe = $DaoOfertas->show($_POST['Id_ofe']);

    //1=plan por ciclos 
    //2=plan sin ciclos
    if ($ofe->getTipoOferta() == 1) {
        ?>
        <h1>Añadir oferta</h1>
        <p>Oferta de inter&eacute;s<br>
           <select id="oferta" onchange="getTipociclo()">
            <option value="0"></option>
            <?php
            foreach($DaoOfertas->showAll() as $k=>$v){
            ?>
                <option value="<?php echo $v->getId() ?>" <?php if($_POST['Id_ofe']==$v->getId()){ ?> selected="selected" <?php } ?>> <?php echo $v->getNombre_oferta() ?> </option>
            <?php
            }
            ?>
          </select>     
        </p>
        <p>Especialidad<br>
          <select id="curso" onchange="update_orientacion_box_curso()">
            <option value="0"></option>
                <?php
                foreach ($DaoEspecialidades->getEspecialidadesOferta($_POST['Id_ofe']) as $esp) {
                    ?>
                    <option value="<?php echo $esp->getId() ?>"><?php echo $esp->getNombre_esp() ?></option>
                    <?php
                }
                ?>
          </select>
        </p>
        <div id="box_orientacion"></div>
        <p>Grado<br>
           <select id="grado">
                <option value="0"></option>
           </select>
        </p>
        <p>Ciclo<br>
         <select id="ciclo">
            <option value="0"></option>
                <?php
                foreach ($DaoCiclos->getCiclosFuturos() as $ciclo) {
                    ?>
                    <option value="<?php echo $ciclo->getId() ?>"><?php echo $ciclo->getClave() ?></option>
                    <?php
                }
                ?>
          </select>
        </p>
        <p>Opci&oacute;n de pago<br>
              <select id="opcionPago">
                <option value="0"></option>
                <option value="1">Materias</option>
                <option value="2">Plan Completo</option>
              </select>
        </p>
        <p>Turno<br>
          <select id="turno">
            <option value="0"></option>
            <?php
            foreach($DaoTurnos->getTurnos() as $turno){
                ?>
                <option value="<?php echo $turno->getId()?>"><?php echo $turno->getNombre()?></option>
            <?php
            }
            ?>
          </select>
        </p>
        <?php
    } else {
        ?>
        <h1>Añadir oferta</h1>       
        <p>Oferta de inter&eacute;s<br>
            <select id="oferta" onchange="getTipociclo()">
                <option value="0"></option>
                <?php
                foreach ($DaoOfertas->showAll() as $oferta) {
                    ?>
                    <option value="<?php echo $oferta->getId() ?>" <?php if ($_POST['Id_ofe'] == $oferta->getId()) { ?> selected="selected" <?php } ?>> <?php echo $oferta->getNombre_oferta() ?></option>
                    <?php
                }
                ?>
            </select>
        </p>
        <p>Especialidad<br>            
            <select id="curso" onchange="update_orientacion_box_curso()">
                <option value="0"></option>
                <?php
                foreach ($DaoEspecialidades->getEspecialidadesOferta($_POST['Id_ofe']) as $esp) {
                    ?>
                    <option value="<?php echo $esp->getId() ?>"><?php echo $esp->getNombre_esp() ?></option>
                    <?php
                }
                ?>
            </select>
        </p>
        <p>Turno<br>
            <select id="turno" onchange="getCursosEspecialidad()">
                <option value="0"></option>
                <?php
                foreach($DaoTurnos->getTurnos() as $turno){
                    ?>
                    <option value="<?php echo $turno->getId()?>"><?php echo $turno->getNombre()?></option>
                <?php
                }
                ?>
            </select>
        </p>
        <p>
            Fecha del curso<br>
            <select id="cursos-especialidad"></select>
        </p>
        <p><div id="box_orientacion"></div></p>
        <p>Grado<br>            
            <select id="grado">
                <option value="0"></option>
            </select>
        </p>
        <p>Opci&oacute;n de pago<br>
            <select id="opcionPago">
                <option value="0"></option>
                <option value="1">Materias</option>
                <option value="2">Plan Completo</option>
            </select>
        </p>
        <?php
    }
    ?>
        <p><button onclick="add_ofe_alum()">Agregar</button><button onclick = "ocultar_error_layer()">Cancelar</button></p>
    <?php
}

if (isset($_POST['action']) && $_POST['action'] == "getCursosEspecialidad") {
    ?>
    <option value="0"></option>
    <?php
    $DaoCursosEspecialidad= new DaoCursosEspecialidad();
    foreach ($DaoCursosEspecialidad->getCursosEspecialidad($_POST['Id_esp'], true,$_POST['Turno']) as $curso) {
        $fechaIni = "Fecha abierta";
        if (strlen($curso->getFechaInicio()) > 0 && strlen($curso->getFechaFin()) > 0) {
            $fechaIni = "Inicio: " . $curso->getFechaInicio() . " - Fin: " . $curso->getFechaFin();
        }
        ?>
        <option value="<?php echo $curso->getId() ?>"><?php echo $fechaIni ?> </option>
        <?php
    }
}


if(isset($_POST['action']) && $_POST['action']=="cambiar_plan_pago"){
    $base= new base();
    $DaoGrados= new DaoGrados();
    $DaoAlumnos= new DaoAlumnos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoCiclos= new DaoCiclos();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoRecargos= new DaoRecargos();
    $DaoPagos= new DaoPagos();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoCursosEspecialidad= new DaoCursosEspecialidad();
    $DaoOfertas= new DaoOfertas();
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $params=$DaoParametrosPlantel->getParametrosPlantelArray();
    
    //Informacion del usuario logueado
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);   
    
    //Obtenemos el ciclo actual de la oferta del alumno
    $ultimoCicloAlumno=$DaoCiclosAlumno->getLastCicloOferta($_POST['Id_ofe_alum']);

    //Actualizamos el nuevo plan de pagos
    $OfertasAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
    $OfertasAlumno->setOpcionPago($_POST['opcionPago']);
    $DaoOfertasAlumno->update($OfertasAlumno); 

    //Informacion relacionada con la oferta del alumno
    $oferta=$DaoOfertas->show($OfertasAlumno->getId_ofe());
    $esp=$DaoEspecialidades->show($OfertasAlumno->getId_esp());
    $ciclos=$base->getCiclosTipoInscripcion($esp->getTipo_insc(), $esp->getTipo_ciclos());
    $cicloActualAlumno=$DaoCiclosAlumno->show($ultimoCicloAlumno->getId());
    
    //Fecha de pago de la inscripcion
    if($oferta->getTipoOferta()==1){
        $ciclo = $DaoCiclos->show($cicloActualAlumno->getId_ciclo());
        $fechaPagoInscripcion = $ciclo->getFecha_ini();
    }else{
        $fechaPagoInscripcion=$DaoCursosEspecialidad->getFechaPagoInscripcion($OfertasAlumno->getId_curso_esp());
    } 
    
    //Verificamos si existe el ciclo del alumno
    if($cicloActualAlumno->getId()>0){
        //Eliminamos todas las materias del ciclo en que se encuentra el alumno
        $DaoMateriasCicloAlumno->deleteMateriasByIdCicloAlumno($cicloActualAlumno->getId());
        
        //Eliminamos todos los cargos del ciclo en que se encuentra el alumno
        foreach($DaoPagosCiclo->getCargosCiclo($cicloActualAlumno->getId()) as $cargo){
            //Por cuestiones de seguridad no se eliman los Pagos_pagos del cargo ya se son folios seriados ya que puede que se enviarion por email o se entregaron al alumno
            $DaoRecargos->deleteRecargosPago($cargo->getId());
            $DaoPagosCiclo->delete($cargo->getId());
        }

        //Seleccionamos el grado actual del ciclo del alumno
        $grado=$DaoGrados->show($cicloActualAlumno->getId_grado());
        $Id_grado_ofe=$grado->getId();
        $grado_actual=$grado->getGrado();
        $modulo=$grado_actual % $ciclos;

        //Verificamos si aplica la inscripcion    
        if(($esp->getTipo_insc()==2 && (($esp->getTipo_ciclos()==1 || $esp->getTipo_ciclos()==4) && $modulo==0)  || (($esp->getTipo_ciclos()==2 || $esp->getTipo_ciclos()==3) && $modulo==1)) || ($esp->getTipo_insc()==1 && $modulo==1) || ($esp->getTipo_insc()==3 && $grado_actual==1)){
            //Insertamos el cargo por inscripcion
            $PagosCiclo= new PagosCiclo();
            $PagosCiclo->setConcepto("Inscripción");
            $PagosCiclo->setFecha_pago($fechaPagoInscripcion);
            $PagosCiclo->setMensualidad($esp->getInscripcion_curso());
            $PagosCiclo->setId_ciclo_alum($cicloActualAlumno->getId());
            $PagosCiclo->setId_alum($_POST['Id_alum']);
            $PagosCiclo->setTipo_pago("pago");
            $PagosCiclo->setTipoRel('alum');
            $PagosCiclo->setIdRel($_POST['Id_alum']);
            $DaoPagosCiclo->add($PagosCiclo);
        }  
        
        //Si la oferta del alumno es plan completo
        if($_POST['opcionPago']==2){
            
                //Si la oferta es por ciclos
                if($oferta->getTipoOferta()==1){
                    
                    //Se generan los cargos apartir de la fecha de inicio del ciclo
                    $ciclo = $DaoCiclos->show($cicloActualAlumno->getId_ciclo());
                    $fecha_pago = $ciclo->getFecha_ini();
                    $anio = date("Y", strtotime($fecha_pago));
                    $mes = date("m", strtotime($fecha_pago));
                    
                    //Generar mensualidades
                    for ($i = 1; $i <= $esp->getNum_pagos(); $i++) {
                        $d=1;
                        if(isset($params['DiaInicioMensualidades']) && strlen($params['DiaInicioMensualidades'])>0){
                           $d=$params['DiaInicioMensualidades'];
                        }
                        $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes + $i, $d, $anio));
                        
                        $Precio_curso = ($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos();
                        $PagosCiclo = new PagosCiclo();
                        $PagosCiclo->setConcepto("Mensualidad");
                        $PagosCiclo->setFecha_pago($base->get_day_pago($fecha_pago, $ciclo->getId()));
                        $PagosCiclo->setMensualidad($Precio_curso);
                        $PagosCiclo->setId_ciclo_alum($cicloActualAlumno->getId());
                        $PagosCiclo->setId_alum($_POST['Id_alum']);
                        $PagosCiclo->setTipo_pago("pago");
                        $PagosCiclo->setTipoRel("alum");
                        $PagosCiclo->setIdRel($_POST['Id_alum']);
                        $DaoPagosCiclo->add($PagosCiclo);
                    }
                }elseif($oferta->getTipoOferta()==2){
                    //Si la oferta es sin ciclos
                    //Se generan los pagos apartir de la fecha segun fue configurado el curso
                    $fecha_pago=$DaoCursosEspecialidad->getFechaPago($OfertasAlumno->getId_curso_esp());
                    $anio = date("Y", strtotime($fecha_pago));
                    $mes = date("m", strtotime($fecha_pago));
                    $day= date("d", strtotime($fecha_pago));
                    
                    //Generar mensualidades
                    for ($i = 1; $i <= $esp->getNum_pagos(); $i++) {
                        $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes +$i, $day , $anio));
                        
                        $Precio_curso = ($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos();
                        $PagosCiclo = new PagosCiclo();
                        $PagosCiclo->setConcepto("Mensualidad");
                        $PagosCiclo->setFecha_pago($fecha_pago);
                        $PagosCiclo->setMensualidad($Precio_curso);
                        $PagosCiclo->setId_ciclo_alum($cicloActualAlumno->getId());
                        $PagosCiclo->setId_alum($_POST['Id_alum']);
                        $PagosCiclo->setTipo_pago("pago");
                        $PagosCiclo->setTipoRel("alum");
                        $PagosCiclo->setIdRel($_POST['Id_alum']);
                        $DaoPagosCiclo->add($PagosCiclo);
                    }
                }

                $mat_rep=0;
                $mat_reprobadas= array();
                //Seleccionamos el ciclo que curso el alumno anteriormente
                $cicloAlumno=$DaoCiclosAlumno->getCicloAlumnoAnterior($cicloActualAlumno->getId(), $_POST['Id_ofe_alum']);
                if($cicloAlumno->getId()>0){
                    //Seleccionar las materias del ciclo anterior para comprobar si las paso
                    foreach($DaoMateriasCicloAlumno->getMateriasCicloAlumno($cicloAlumno->getId()) as $materiaCiclo){
                        $mat_esp=$DaoMateriasEspecialidad->show($materiaCiclo->getId_mat_esp());
                        $mat=$DaoMaterias->show($mat_esp->getId_mat());
                        if(is_numeric($materiaCiclo->getCalTotalParciales())){
                           $CalTotalParciales=$materiaCiclo->getCalTotalParciales();
                        }else{
                           $CalTotalParciales=mb_strtolower ($materiaCiclo->getCalTotalParciales(),"UTF-8");
                        }
                        if($CalTotalParciales>=$mat->getPromedio_min() || $CalTotalParciales=="cp" || $CalTotalParciales=="ac" || $materiaCiclo->getCalExtraordinario()>=$mat->getPromedio_min() || $materiaCiclo->getCalEspecial()>=$mat->getPromedio_min()){
                        }else{
                              //Validar si no existe la materia
                              $existeMat=$DaoMateriasCicloAlumno->getMateriaCicloAlumno($cicloActualAlumno->getId(), $_POST['Id_alum'], $materiaCiclo->getId_mat_esp());
                              if($existeMat->getId()==0){
                                  //Se inserta la misma porque reprobo
                                  $MateriasCicloAlumno=new MateriasCicloAlumno();
                                  $MateriasCicloAlumno->setId_ciclo_alum($cicloActualAlumno->getId());
                                  $MateriasCicloAlumno->setId_alum($_POST['Id_alum']);
                                  $MateriasCicloAlumno->setId_mat_esp($materiaCiclo->getId_mat_esp());
                                  $MateriasCicloAlumno->setTipo($materiaCiclo->getTipo());
                                  $MateriasCicloAlumno->setActivo($materiaCiclo->getActivo());
                                  $DaoMateriasCicloAlumno->add($MateriasCicloAlumno);

                                  //Guardar el id de la materia consecutiva para no insertarla
                                  $query = "SELECT * FROM prerequisitos_ulm 
                                                 JOIN Materias_especialidades ON prerequisitos_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp 
                                                 JOIN materias_uml ON Materias_especialidades.Id_mat=materias_uml.Id_mat
                                            WHERE Id_mat_esp_pre=".$materiaCiclo->getId_mat_esp();
                                  foreach($base->advanced_query($query) as $row_prerequisitos){
                                          array_push($mat_reprobadas, $row_prerequisitos['Id_mat_esp']);
                                  }
                                  $mat_rep++;
                              }
                        }
                    }
                }
                
                //Cargar materias del nuevo ciclo
                $CountMatInsertadas=0;
                $query = "SELECT * FROM Materias_especialidades 
                               JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat
                          WHERE Id_esp=".$OfertasAlumno->getId_esp()." AND Grado_mat=".$Id_grado_ofe." AND Activo_mat_esp=1 AND Activo_mat=1";
                $totalRows_materias_ciclos_ulm=$base->getTotalRows($query);
                $limiteDeMatInsertar=$totalRows_materias_ciclos_ulm-$mat_rep;
                foreach($base->advanced_query($query) as $row_materias_ciclos_ulm){
                       //Si el id de la materia no se encuentra en el array entonces insertar
                       if(!in_array($row_materias_ciclos_ulm['Id_mat_esp'], $mat_reprobadas) && $CountMatInsertadas<$limiteDeMatInsertar){
                              //Validar si no existe la materia
                              $existeMat=$DaoMateriasCicloAlumno->getMateriaCicloAlumno($cicloActualAlumno->getId(), $_POST['Id_alum'], $row_materias_ciclos_ulm['Id_mat_esp']);
                              if($existeMat->getId()==0){
                                  $MateriasCicloAlumno=new MateriasCicloAlumno();
                                  $MateriasCicloAlumno->setId_ciclo_alum($cicloActualAlumno->getId());
                                  $MateriasCicloAlumno->setId_alum($_POST['Id_alum']);
                                  $MateriasCicloAlumno->setId_mat_esp($row_materias_ciclos_ulm['Id_mat_esp']);
                                  $MateriasCicloAlumno->setTipo($row_materias_ciclos_ulm['Tipo']);
                                  $MateriasCicloAlumno->setActivo($row_materias_ciclos_ulm['Activo']);
                                  $DaoMateriasCicloAlumno->add($MateriasCicloAlumno);
                                  $CountMatInsertadas++;
                              }
                       }
                } 
        }
    }
    update_page($_POST['Id_alum']); 
}


if(isset($_POST['action']) && $_POST['action']=="cambiar_oferta"){
    $base= new base();
    $DaoAlumnos= new DaoAlumnos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoCiclos= new DaoCiclos();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoRecargos= new DaoRecargos();
    $DaoPagos= new DaoPagos();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoCursosEspecialidad= new DaoCursosEspecialidad();
    $DaoOfertas= new DaoOfertas();
    $DaoArchivosAlumno= new DaoArchivosAlumno();
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $params=$DaoParametrosPlantel->getParametrosPlantelArray();
    
    //Obtenemos la informacion del usuario logeado
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    //Obtenemos el ciclo actual de la oferta del alumno
    $ultimoCicloAlumno=$DaoCiclosAlumno->getLastCicloOferta($_POST['Id_ofe_alum']);

    //Informacion relacionada con la oferta del alumno
    $oferta=$DaoOfertas->show($_POST['Id_oferta']);
      
    //Obtenemos el informacion de la especialidad
    $esp=$DaoEspecialidades->show($_POST['Id_esp']);

    //Validamos si ya existe la oferta y si no entonces insertamos la nueva
    $query = "SELECT * FROM ofertas_alumno WHERE Id_ofe=".$_POST['Id_oferta']." AND Id_alum= ".$_POST['Id_alumn']." AND Id_esp=".$_POST['Id_esp']." AND Activo_oferta=1";
    $totalRows=$base->getTotalRows($query);
    if($totalRows==0){
       $Id_curso_esp=""; 
      if(isset($_POST['Id_curso_esp'])){
         $Id_curso_esp= $_POST['Id_curso_esp'];
      }
      //Insertamos la oferta nueva del alumno
      $OfertasAlumno= new OfertasAlumno();
      $OfertasAlumno->setId_ofe($_POST['Id_oferta']);
      $OfertasAlumno->setId_alum($_POST['Id_alumn']);
      $OfertasAlumno->setId_esp($_POST['Id_esp']);
      $OfertasAlumno->setId_ori($_POST['Id_ori']);
      $OfertasAlumno->setOpcionPago($_POST['opcion']);
      $OfertasAlumno->setActivo(1);
      $OfertasAlumno->setTurno($_POST['Turno']);
      $OfertasAlumno->setAlta_ofe(date('Y-m-d'));
      $OfertasAlumno->setId_curso_esp($Id_curso_esp);
      $id_oferta_alumno=$DaoOfertasAlumno->add($OfertasAlumno);
      
      //Actualizar los documentos entregados del alumno a la nueva oferta
      foreach($DaoArchivosAlumno->getArchivosAlumnoByOfertaAlumno($_POST['Id_ofe_alum']) as $row){
         if($row['Id_file_alum']>0){
            $ArchivosAlumno=$DaoArchivosAlumno->show($row['Id_file_alum']);
            $ArchivosAlumno->setId_ofe_alum($id_oferta_alumno);
            $DaoArchivosAlumno->update($ArchivosAlumno);
         }
      }

     //Actualizamos la oferta actual como baja
     $OfertasAlum=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
     $OfertasAlum->setActivo(0);
     $OfertasAlum->setBaja_ofe(date('Y-m-d H:i:s'));
     $OfertasAlum->setId_mot_baja("7");
     $OfertasAlum->setComentario_baja("Cambio de oferta");
     $OfertasAlum->setId_usu_captura_baja($_COOKIE['admin/Id_usu']);
     $DaoOfertasAlumno->update($OfertasAlum); 

      //Obtenemos los pagos realizados de la oferta anterior y los pasamos a la nueva oferta
      foreach($DaoPagos->getPagosOfertaAlumno($_POST['Id_ofe_alum']) as $pago){
              $p=$DaoPagos->show($pago->getId());
              //$p->setId_pago_ciclo("");
              $p->setId_ofe_alum($id_oferta_alumno);
              $DaoPagos->update($p);
      }

      //Insertamos el primer ciclo del alumno en la nueva oferta
      $CiclosAlumno= new CiclosAlumno();
        if(isset($_POST['Id_ciclo']) && $_POST['Id_ciclo']>0){
          $CiclosAlumno->setId_ciclo($_POST['Id_ciclo']);
        }else{
          $CiclosAlumno->setId_ciclo(0);
        }
      $CiclosAlumno->setId_grado($_POST['Id_grado']);
      $CiclosAlumno->setId_ofe_alum($id_oferta_alumno);
      $id_ciclo_alumno=$DaoCiclosAlumno->add($CiclosAlumno);

      //Fecha de pago de la inscripcion
      if($oferta->getTipoOferta()==1){
        $ciclo = $DaoCiclos->show($CiclosAlumno->getId_ciclo());
        $fechaPagoInscripcion = $ciclo->getFecha_ini();
      }else{
        $fechaPagoInscripcion=$DaoCursosEspecialidad->getFechaPagoInscripcion($OfertasAlumno->getId_curso_esp());
      } 

      //Insertamos el cargo por  inscripcion
      $PagosCiclo= new PagosCiclo();
      $PagosCiclo->setConcepto("Inscripción");
      $PagosCiclo->setFecha_pago($fechaPagoInscripcion);
      $PagosCiclo->setMensualidad($esp->getInscripcion_curso());
      $PagosCiclo->setId_ciclo_alum($id_ciclo_alumno);
      $PagosCiclo->setId_alum($_POST['Id_alumn']);
      $PagosCiclo->setTipo_pago("pago");
      $PagosCiclo->setTipoRel('alum');
      $PagosCiclo->setIdRel($_POST['Id_alumn']);
      $DaoPagosCiclo->add($PagosCiclo);
      
      //Si la oferta del alumno es plan completo
      if($_POST['opcion']==2){
            
            //Si la oferta es por ciclos
            if($oferta->getTipoOferta()==1){

                //Se generan los cargos apartir del mes de inicio del ciclo
                //en el dia que se dio de alta en parametros y si no por default el dia 1
                $ciclo = $DaoCiclos->show($CiclosAlumno->getId_ciclo());
                $fecha_pago = $ciclo->getFecha_ini();
                $anio = date("Y", strtotime($fecha_pago));
                $mes = date("m", strtotime($fecha_pago));

                //Generar mensualidades
                for ($i = 1; $i <= $esp->getNum_pagos(); $i++) {
                    $d=1;
                    if(isset($params['DiaInicioMensualidades']) && strlen($params['DiaInicioMensualidades'])>0){
                       $d=$params['DiaInicioMensualidades'];
                    }
                    $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes + $i, $d, $anio));
                    
                    $Precio_curso = ($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos();
                    $PagosCiclo = new PagosCiclo();
                    $PagosCiclo->setConcepto("Mensualidad");
                    $PagosCiclo->setFecha_pago($base->get_day_pago($fecha_pago, $ciclo->getId()));
                    $PagosCiclo->setMensualidad($Precio_curso);
                    $PagosCiclo->setId_ciclo_alum($id_ciclo_alumno);
                    $PagosCiclo->setId_alum($_POST['Id_alumn']);
                    $PagosCiclo->setTipo_pago("pago");
                    $PagosCiclo->setTipoRel("alum");
                    $PagosCiclo->setIdRel($_POST['Id_alumn']);
                    $DaoPagosCiclo->add($PagosCiclo);
                }
            }elseif($oferta->getTipoOferta()==2){
                //Si la oferta es sin ciclos
                //Se generan los pagos apartir de la fecha segun fue configurado el curso
                $fecha_pago=$DaoCursosEspecialidad->getFechaPago($OfertasAlumno->getId_curso_esp());
                $anio = date("Y", strtotime($fecha_pago));
                $mes = date("m", strtotime($fecha_pago));
                $day= date("d", strtotime($fecha_pago));

                //Generar mensualidades
                for ($i = 1; $i <= $esp->getNum_pagos(); $i++) {
                    $fecha_pago = date("Y-m-d", mktime(0, 0, 0, $mes +$i, $day , $anio));
                    
                    $Precio_curso = ($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos();
                    $PagosCiclo = new PagosCiclo();
                    $PagosCiclo->setConcepto("Mensualidad");
                    $PagosCiclo->setFecha_pago($fecha_pago);
                    $PagosCiclo->setMensualidad($Precio_curso);
                    $PagosCiclo->setId_ciclo_alum($id_ciclo_alumno);
                    $PagosCiclo->setId_alum($_POST['Id_alumn']);
                    $PagosCiclo->setTipo_pago("pago");
                    $PagosCiclo->setTipoRel("alum");
                    $PagosCiclo->setIdRel($_POST['Id_alumn']);
                    $DaoPagosCiclo->add($PagosCiclo);
                }
            }
                
            //Obtener las materias que cursara en el ciclo
            $query = "SELECT * FROM Materias_especialidades
                           JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat 
                      WHERE Id_esp=".$_POST['Id_esp']." AND Grado_mat=".$_POST['Id_grado']." AND Activo_mat_esp=1 AND Activo_mat=1";
            foreach($base->advanced_query($query) as $x=>$y){
                    $MateriasCicloAlumno= new MateriasCicloAlumno();
                    $MateriasCicloAlumno->setId_ciclo_alum($id_ciclo_alumno);
                    $MateriasCicloAlumno->setId_mat_esp($y['Id_mat_esp']);
                    $MateriasCicloAlumno->setTipo(1);
                    $MateriasCicloAlumno->setActivo(1);
                    $MateriasCicloAlumno->setId_alum($_POST['Id_alumn']);
                    $DaoMateriasCicloAlumno->add($MateriasCicloAlumno);
            }  
        }


    }else{
        //"Ya existia la oferta solo se cambio de orientacion<br>";
        //Si la oferta es la misma, se insertaran la mismas materias que tenia esto es porque si no se perderia el historial de sus materias y calificaciones de los semestres pasados 
        $OfertasAlumno=$DaoOfertasAlumno->show($_POST['Id_ofe_alum']);
        $OfertasAlumno->setId_ofe($_POST['Id_oferta']);
        $OfertasAlumno->setId_alum($_POST['Id_alumn']);
        $OfertasAlumno->setId_esp($_POST['Id_esp']);
        $OfertasAlumno->setId_ori($_POST['Id_ori']);
        $OfertasAlumno->setOpcionPago($_POST['opcion']);
        $OfertasAlumno->setTurno($_POST['Turno']);
        $OfertasAlumno->setId_curso_esp($_POST['Id_curso_esp']);
        $DaoOfertasAlumno->update($OfertasAlumno);

        $CiclosAlumno=$DaoCiclosAlumno->show($ultimoCicloAlumno->getId());
        $CiclosAlumno->setId_grado($_POST['Id_grado']);
        if(isset($_POST['Id_ciclo']) && $_POST['Id_ciclo']>0){
          $CiclosAlumno->setId_ciclo($_POST['Id_ciclo']);
        }else{
          $CiclosAlumno->setId_ciclo(0);
        }
        $DaoCiclosAlumno->update($CiclosAlumno);

        if($_POST['opcion']==2){
            
            //Obtener las materias que cursara en el ciclo
            $query = "SELECT * FROM Materias_especialidades
                           JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat 
                     WHERE Id_esp=".$_POST['Id_esp']." AND Grado_mat=".$_POST['Id_grado']." AND Activo_mat_esp=1 AND Activo_mat=1";
            foreach($base->advanced_query($query) as $k=>$v){
                     $query  = "SELECT * FROM materias_ciclo_ulm WHERE Id_ciclo_alum=" .$ultimoCicloAlumno->getId()." AND Id_mat_esp=".$v['Id_mat_esp'];
                     $totalRows=$base->getTotalRows($query);
                     if($totalRows==0){
                        $MateriasCicloAlumno= new MateriasCicloAlumno();
                        $MateriasCicloAlumno->setId_ciclo_alum($ultimoCicloAlumno->getId());
                        $MateriasCicloAlumno->setId_mat_esp($v['Id_mat_esp']);
                        $MateriasCicloAlumno->setId_ori($v['Id_ori']);
                        $MateriasCicloAlumno->setTipo(1);
                        $MateriasCicloAlumno->setActivo(1);
                        $MateriasCicloAlumno->setId_alum($_POST['Id_alumn']);
                        $DaoMateriasCicloAlumno->add($MateriasCicloAlumno);
                     }
            }  

        }
    }
    
    $alum=$DaoAlumnos->show($_POST['Id_alumn']);
    $TextoHistorial="Cambio de oferta a ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ".$oferta->getNombre_oferta().", ".$esp->getNombre_esp();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Cambio de oferta");

    update_page($_POST['Id_alumn']);
}


if ($_POST['action'] == "getTipoCicloCambioOferta") {
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoCursosEspecialidad= new DaoCursosEspecialidad();
    $DaoCiclos = new DaoCiclos();
    $DaoTurnos= new DaoTurnos();
    $ofe = $DaoOfertas->show($_POST['Id_ofe']);

    //1=plan por ciclos 
    //2=plan sin ciclos
    if ($ofe->getTipoOferta() == 1) {
        ?>
        <h1>Cambiar oferta</h1>
        <p>Oferta<br>
           <select id="oferta" onchange="getTipoCicloCambioOferta(<?php echo $_POST['Id_ofe_alum']?>)">
            <option value="0"></option>
            <?php
            foreach($DaoOfertas->showAll() as $k=>$v){
            ?>
                <option value="<?php echo $v->getId() ?>" <?php if($_POST['Id_ofe']==$v->getId()){ ?> selected="selected" <?php } ?>> <?php echo $v->getNombre_oferta() ?> </option>
            <?php
            }
            ?>
          </select>     
        </p>
        <p>Especialidad<br>
          <select id="curso" onchange="update_orientacion_box_curso()">
            <option value="0"></option>
                <?php
                foreach ($DaoEspecialidades->getEspecialidadesOferta($_POST['Id_ofe']) as $esp) {
                    ?>
                    <option value="<?php echo $esp->getId() ?>"><?php echo $esp->getNombre_esp() ?></option>
                    <?php
                }
                ?>
          </select>
        </p>
        <div id="box_orientacion"></div>
        <p>Grado<br>
           <select id="grado">
                <option value="0"></option>
           </select>
        </p>
        <p>Ciclo<br>
         <select id="ciclo">
            <option value="0"></option>
                <?php
                foreach ($DaoCiclos->getCiclosFuturos() as $ciclo) {
                    ?>
                    <option value="<?php echo $ciclo->getId() ?>"><?php echo $ciclo->getClave() ?></option>
                    <?php
                }
                ?>
          </select>
        </p>
        <p>Opci&oacute;n de pago<br>
              <select id="opcionPago">
                <option value="0"></option>
                <option value="1">Materias</option>
                <option value="2">Plan Completo</option>
              </select>
        </p>
        <p>Turno<br>
          <select id="turno">
            <option value="0"></option>
            <?php
            foreach($DaoTurnos->getTurnos() as $turno){
                ?>
                <option value="<?php echo $turno->getId()?>"><?php echo $turno->getNombre()?></option>
            <?php
            }
            ?>
          </select>
        </p>
        <?php
    } else {
        ?>
        <h1>Cambiar oferta</h1>       
        <p>Oferta<br>
            <select id="oferta" onchange="getTipoCicloCambioOferta(<?php echo $_POST['Id_ofe_alum']?>)">
                <option value="0"></option>
                <?php
                foreach ($DaoOfertas->showAll() as $oferta) {
                    ?>
                    <option value="<?php echo $oferta->getId() ?>" <?php if ($_POST['Id_ofe'] == $oferta->getId()) { ?> selected="selected" <?php } ?>> <?php echo $oferta->getNombre_oferta() ?></option>
                    <?php
                }
                ?>
            </select>
        </p>
        <p>Especialidad<br>            
            <select id="curso" onchange="update_orientacion_box_curso()">
                <option value="0"></option>
                <?php
                foreach ($DaoEspecialidades->getEspecialidadesOferta($_POST['Id_ofe']) as $esp) {
                    ?>
                    <option value="<?php echo $esp->getId() ?>"><?php echo $esp->getNombre_esp() ?></option>
                    <?php
                }
                ?>
            </select>
        </p>
        <p>Turno<br>
            <select id="turno" onchange="getCursosEspecialidad()">
                <option value="0"></option>
                <?php
                foreach($DaoTurnos->getTurnos() as $turno){
                    ?>
                    <option value="<?php echo $turno->getId()?>"><?php echo $turno->getNombre()?></option>
                <?php
                }
                ?>
            </select>
        </p>
        <p>
            Fecha del curso<br>
            <select id="cursos-especialidad"></select>
        </p>
        <p><div id="box_orientacion"></div></p>
        <p>Grado<br>            
            <select id="grado">
                <option value="0"></option>
            </select>
        </p>
        <p>Opci&oacute;n de pago<br>
            <select id="opcionPago">
                <option value="0"></option>
                <option value="1">Materias</option>
                <option value="2">Plan Completo</option>
            </select>
        </p>
        <?php
    }
    ?>
    <p><button onclick="cambiar_oferta(<?php echo $_POST['Id_ofe_alum']?>)">Cambiar</button><button onclick = "ocultar_error_layer()">Cancelar</button></p>
    <?php
}


if ($_POST['action'] == "add_ofe_alum_interes") {
    //Validamos si ya esxiste la oferta y si no entonces insertamos la nueva
    $DaoOfertasInteres= new DaoOfertasInteres();
     $query = "SELECT * FROM Ofertas_interes WHERE Id_ofe=".$_POST['Id_oferta']." AND Id_alum= ".$_POST['Id_alumn']." AND Id_esp=".$_POST['Id_esp'];
     if($_POST['Id_ori']>0){
        $query = "SELECT * FROM Ofertas_interes WHERE Id_ofe=".$_POST['Id_oferta']." AND Id_alum= ".$_POST['Id_alumn']." AND Id_esp=".$_POST['Id_esp']." AND Id_ori=".$_POST['Id_ori'];
     }
     $existe=$DaoOfertasInteres->existeQuery($query);
     if($existe->getId()==0 || $existe->getId()==null){

        //Insertamos la oferta de interes del alumno
        $OfertasInteres= new OfertasInteres();
        $OfertasInteres->setId_ofe($_POST['Id_oferta']);
        $OfertasInteres->setId_alum($_POST['Id_alumn']);
        $OfertasInteres->setId_esp($_POST['Id_esp']);
        $OfertasInteres->setId_ori($_POST['Id_ori']);
        $OfertasInteres->setId_ciclo($_POST['Id_ciclo']);
        $OfertasInteres->setTurno($_POST['Turno']);
        $OfertasInteres->setDateCreated(date('Y-m-d H:i:s'));
        $OfertasInteres->setId_usu($_COOKIE['admin/Id_usu']);
        $id_oferta_alumno=$DaoOfertasInteres->add($OfertasInteres);
          
        //Captura de historial
        $DaoUsuarios= new DaoUsuarios();
        $DaoAlumnos= new DaoAlumnos();
        $DaoOfertas= new DaoOfertas();
        $DaoEspecialidades= new DaoEspecialidades();
        
        $alum=$DaoAlumnos->show($_POST['Id_alumn']);
        $ofe=$DaoOfertas->show($_POST['Id_oferta']);
        $esp=$DaoEspecialidades->show($_POST['Id_esp']);
        $TextoHistorial="Añade oferta de interes a ".$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM().", ".$ofe->getNombre_oferta().", ".$esp->getNombre_esp();
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Alumnos");
        
        update_page($_POST['Id_alumn']);
      }  
}

if($_POST['action']=="delete_tutor"){
    $DaoTutores= new DaoTutores();
    $DaoDirecciones= new DaoDirecciones();
    $DaoTutores->delete($_POST['id_tutor']);
    $DaoDirecciones->delete($_POST['id_dir']);
    update_page($_POST['id_alumn']);
}

if (isset($_POST['action']) && $_POST['action'] == "box_tutor") {
?>
<div class="box-emergente-form box-tutor">
      <table>
          <tr>
              <td>
                    <h1>Añadir tutor</h1>
                    <p>Parentesco<br><input type="text" id="parentesco-tutor"/></p>
                    <p>Nombre<br><input type="text" id="nombre-tutor"/></p>
                    <p>Apellido paterno<br><input type="text" id="apellidoP-tutor"/></p>
                    <p>Apellido materno<br><input type="text" id="apellidoM-tutor"/></p>
                    <p>Correo electrónico<br><input type="text" id="email-tutor"/></p>
                    <p>Teléfono<br><input type="text" id="tel-tutor"/></p>
                    <p>Celular<br><input type="text" id="cel-tutor"/></p>
              </td>
              <td>
                    <h1>Añadir dirección</h1>
                    <p>Calle<br><input type="text" id="calle-tutor"/></p>
                    <p>Núm ext.<br><input type="text" id="numext-tutor"/></p>
                    <p>Núm int.<br><input type="text" id="numint-tutor"/></p>
                    <p>Colonia<br><input type="text" id="colonia-tutor"/></p>
                    <p>Ciudad<br><input type="text" id="ciudad-tutor"/></p>
                    <p>Estado<br><input type="text" id="estado-tutor"/></p>
                    <p>Código postal<br><input type="text" id="cp-tutor"/></p>
                    <button onclick="add_tutor()">Agregar</button><button onclick = "ocultar_error_layer()">Cancelar</button>
              </td>
          </tr>
      </table>
</div>
<?php
}

function addDireccionTutor($tutor,$Id_tutor){
   $DaoDirecciones= new DaoDirecciones();
    if($tutor['id_dir']>0){
        $Direcciones= $DaoDirecciones->show($tutor['id_dir']);
        $Direcciones->setCalle_dir($tutor['calle']);
        $Direcciones->setNumExt_dir($tutor['numExt']);
        $Direcciones->setNumInt_dir($tutor['numInt']);
        $Direcciones->setColonia_dir($tutor['colonia']);
        $Direcciones->setCiudad_dir($tutor['ciudad']);
        $Direcciones->setEstado_dir($tutor['estado']);
        $Direcciones->setCp_dir($tutor['cp']);
        $DaoDirecciones->update($Direcciones);
    }else{
        $Direcciones= new Direcciones();
        $Direcciones->setCalle_dir($tutor['calle']);
        $Direcciones->setNumExt_dir($tutor['numExt']);
        $Direcciones->setNumInt_dir($tutor['numInt']);
        $Direcciones->setColonia_dir($tutor['colonia']);
        $Direcciones->setCiudad_dir($tutor['ciudad']);
        $Direcciones->setEstado_dir($tutor['estado']);
        $Direcciones->setCp_dir($tutor['cp']);
        $Direcciones->setIdRel_dir($Id_tutor);
        $Direcciones->setTipoRel_dir("tutor");
        if(strlen($tutor['calle'])>0 && strlen($tutor['numExt'])>0 && strlen($tutor['ciudad'])>0){
           $DaoDirecciones->add($Direcciones);  
        }
    }
}

if($_POST['action']=="add_tutor"){
    $DaoTutores= new DaoTutores();
    $Tutores= new Tutores();
    $Tutores->setParentesco($_POST['parantesco']);
    $Tutores->setNombre($_POST['nombre']);
    $Tutores->setApellidoP($_POST['apellidoP']);
    $Tutores->setApellidoM($_POST['apellidoM']);
    $Tutores->setTel($_POST['tel']);
    $Tutores->setCel($_POST['cel']);
    $Tutores->setEmail($_POST['emal']);
    $Tutores->setId_ins($_POST['Id_alum']);
    $Id_tutor=$DaoTutores->add($Tutores);

    addDireccionTutor($_POST,$Id_tutor);   
    update_page($_POST['Id_alum']);
}
