<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="reset_password"){
    $base= new base();
    $DaoUsuarios= new DaoUsuarios();
    $tipo_error=array();
    if($_POST['pass']==$_POST['confir']){
        if(strlen($_POST['pass'])>6){
            $query = "SELECT * FROM usuarios_ulm WHERE Recover_usu='".$_POST['key']."'";
            $usuario=$DaoUsuarios->existQuery($query);
            if($usuario->getId()>0){
               $usuario->setPass_usu($base->hashPassword($_POST['pass']));
               $usuario->setRecover_usu(NULL);
               $DaoUsuarios->update($usuario);
               $tipo_error['tipo']="ok";
            }
        }else{
                $tipo_error['tipo']="password_security";
        }
    }else{
            $tipo_error['tipo']="password_match";
    }
    echo json_encode($tipo_error);
}
