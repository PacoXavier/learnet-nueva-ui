<?php
require_once('activate_error.php');
require_once('../require_daos.php'); 

if($_POST['action']=="getEventosCiclo"){
    $DaoEventos= new DaoEventos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoCiclos = new DaoCiclos();
    $DaoDiasInhabiles = new DaoDiasInhabiles();
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $cicloActual = $DaoCiclos->getActual($usu->getId_plantel());
    
    $resp['Eventos']=array();
    $resp['Ciclo'] = array();
    $resp['DiasFestivos'] = array();

    //Obtener el periodo que abaarca el cicclo actual
    $c = array();
    $c['FechaIni'] = $cicloActual->getFecha_ini();
    $c['FechaFin'] = $cicloActual->getFecha_fin();
    array_push($resp['Ciclo'], $c);

    if ($cicloActual->getId() > 0) {
        //Obtener los dias festivos
        foreach ($DaoDiasInhabiles->getDiasInhabilesCiclo($cicloActual->getId()) as $dia) {
            $d = array();
            $d['FechaIni'] = $dia->getFecha_inh();
            array_push($resp['DiasFestivos'], $d);
        }
        
    }

    foreach($DaoEventos->getEventosCiclo($_POST['Id_ciclo']) as $ev){
        $evento=array();
        $evento['Id']=$ev->getId();
        $evento['Nombre']=$ev->getNombre();
        $evento['Comentarios']=$ev->getComentarios();
        $evento['Id_salon']=$ev->getId_salon();
        $evento['Start']=$ev->getStart();
        $evento['End']=$ev->getEnd();
        array_push($resp['Eventos'], $evento);
    }
    echo json_encode($resp);
}

if($_POST['action']=="mostrar_box_evento"){
    $DaoEventos= new DaoEventos();
    $DaoCiclos= new DaoCiclos();
    $DaoAulas= new DaoAulas();

   if($_POST['Id_evento']>0){
      $ev=$DaoEventos->show($_POST['Id_evento']);
      $Nombre=$ev->getNombre();
      $Start=$_POST['FechaInicio'];
      $HoraInicio=$_POST['HoraInicio'];
      $HoraFin=$_POST['HoraFin'];
      $Id_ciclo=$ev->getId_ciclo();
      $Id_salon=$ev->getId_salon();
      $Costo=$ev->getCosto();
      $Comentarios=$ev->getComentarios();
      $Tipo=$ev->getTipoEvento();
       
   }else{
      $Nombre="";
      $Start=$_POST['Start'];
      $HoraInicio="";
      $HoraFin="";
      $Id_ciclo="";
      $Id_salon="";
      $Costo="";
      $Comentarios="";   
      $Tipo="";
   }
?>
<div class="eventos box-emergente-form">
	<h1>Evento</h1>
        <p>Nombre del evento<br><input type="text" value="<?php echo $Nombre?>" id="nombre"/></p>
        <p>Fecha de inicio<br><input type="text" value="<?php echo $Start?>" id="start" readonly="readonly"/></p>
        <p>Hora de inicio<br><input type="text" id="horaInicio" class="time start" value="<?php echo $HoraInicio;?>"/></p>
        <p>Hora de fin<br><input type="text" id="horaFin" class="time end" value="<?php echo $HoraFin;?>"/></p>
        <p>Ciclo<br>
          <select id="ciclo">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->getCiclosFuturos() as $k=>$v){
              ?>
              <option value="<?php echo $v->getId() ?>" <?php if($Id_ciclo==$v->getId()){?>  selected="selected" <?php } ?>><?php echo $v->getClave() ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p>Aula<br>
           <select id="aula">
              <option value="0"></option>
              	<?php
	        foreach($DaoAulas->showAll() as $aula){
	               ?>
                          <option value="<?php echo $aula->getId()?>" <?php if($Id_salon==$aula->getId()){?>  selected="selected" <?php } ?>><?php echo $aula->getClave_aula()." - ".$aula->getNombre_aula(); ?></option>
	                <?php
	        }
                ?>
            </select>
        </p>
        <p>Costo por renta<br><input type="text" value="<?php echo $Costo?>" id="costo"/></p>
        <p>Evento para:</p>
        <p>
           <input type="checkbox" value="alumno" id="alumno"   <?php if($Tipo=="alumno" || $Tipo=="ambos"){?> checked="checked" <?php }?>> Alumnos
           <input type="checkbox" value="docente" id="docente" <?php if($Tipo=="docente" || $Tipo=="ambos"){?> checked="checked" <?php }?>>Docentes
        </p>
        <p>Comentarios<br><textarea id="comentarios"><?php echo $Comentarios?></textarea></p>
        <p>
            <button onclick="verificarDisponibilidad(<?php echo $_POST['Id_evento']?>)">Guardar</button>
            <?php
              if($_POST['Id_evento']>0){
            ?>
              <button onclick="delete_evento(<?php echo $_POST['Id_evento']?>)">Eliminar</button>
            <?php
              }
              ?>
              <button onclick="ocultar_error_layer()">Cancelar</button>
        </p>
</div>
<?php	
}

if($_POST['action']=="verificarDisponibilidad"){
    $DaoAulas= new DaoAulas();
    $DaoHorarioDocente= new DaoHorarioDocente();
    $DaoNotificacionesUsuario= new DaoNotificacionesUsuario();
    $DaoHoras= new DaoHoras();
    $DaoEventos= new DaoEventos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoGrupos= new DaoGrupos();
    $DaoDocentes= new DaoDocentes();
    
    $Ini=  substr($_POST['HoraInicio'], 0,  strpos($_POST['HoraInicio'], ":"));
    $horaIni=$DaoHoras->getTextoHora($Ini);
    
    $Fin=  substr($_POST['HoraFin'], 0,  strpos($_POST['HoraFin'], ":"));
    $horaFin=$DaoHoras->getTextoHora($Fin);
    
    $queryHora="";
    if($horaIni->getId()==$horaFin->getId()){
        $queryHora=" AND Hora=".$horaIni->getId();
    }else{
        $query = "SELECT * FROM Horas WHERE Id_hora>=".$horaIni->getId()." && Id_hora<".$horaFin->getId();
        foreach ($DaoHoras->advanced_query($query) as $h){
            $queryHora.="Hora=".$h->getId()." || ";
        }
        $queryHora=  substr($queryHora, 0, strripos($queryHora,'||'));
        $queryHora="AND (".$queryHora.")";
    }
    
    $dia=date("N",strtotime($_POST['start']));  
    $queryDia="";
    if($dia==1){
           $queryDia="AND Lunes=1";
    }elseif($dia==2){
           $queryDia="AND Martes=1";
    }elseif($dia==3){
           $queryDia="AND Miercoles=1"; 
    }elseif($dia==4){
           $queryDia="AND Jueves=1"; 
    }elseif($dia==5){
           $queryDia="AND Viernes=1"; 
    }elseif($dia==6){
           $queryDia="AND Sabado=1"; 
    }elseif($dia==7){
           $queryDia="AND Domingo=1"; 
    }
    $resp=array();
    $resp['Ban']=0;
    $texto="";
    $query = "SELECT * FROM Horario_docente WHERE Id_ciclo=".$_POST['Id_ciclo']." AND Aula=".$_POST['aula']." ".$queryHora." ".$queryDia."  ORDER BY Hora ASC";
    foreach($DaoHorarioDocente->advanced_query($query) as $horario){
           $resp['Ban']=1;
           $doc=$DaoDocentes->show($horario->getId_docente());
           $grupo=$DaoGrupos->show($horario->getId_grupo());
           $hora=$DaoHoras->show($horario->getHora());
           $texto.="-".mb_strtoupper($doc->getNombre_docen()." ".$doc->getApellidoP_docen()." ".$doc->getApellidoM_docen())."\n Grupo ".mb_strtoupper($grupo->getClave()).", Horario de ".$hora->getTexto_hora()."\n\n ";
    }

    $resp['Texto']="Salón ocupado por: \n\n".$texto." \n\n¿Desea continuar?";
    echo json_encode($resp);
}


if($_POST['action']=="save_evento"){
    $DaoAulas= new DaoAulas();
    $DaoHorarioDocente= new DaoHorarioDocente();
    $DaoNotificacionesUsuario= new DaoNotificacionesUsuario();
    $DaoHoras= new DaoHoras();
    $DaoEventos= new DaoEventos();
    $DaoUsuarios= new DaoUsuarios();
    
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $salon=$DaoAulas->show($_POST['aula']);
    
    //Se busca a los profesores a los que se les mandara la notificacion
    $Ini=  substr($_POST['HoraInicio'], 0,  strpos($_POST['HoraInicio'], ":"));
    $horaIni=$DaoHoras->getTextoHora($Ini);
    
    $Fin=  substr($_POST['HoraFin'], 0,  strpos($_POST['HoraFin'], ":"));
    $horaFin=$DaoHoras->getTextoHora($Fin);
    
    $queryHora="";
    if($horaIni->getId()==$horaFin->getId()){
        $queryHora=" AND Hora=".$horaIni->getId();
    }else{
        $query = "SELECT * FROM Horas WHERE Id_hora>=".$horaIni->getId()." && Id_hora<".$horaFin->getId();
        foreach ($DaoHoras->advanced_query($query) as $h){
            $queryHora.="Hora=".$h->getId()." || ";
        }
        $queryHora=  substr($queryHora, 0, strripos($queryHora,'||'));
        $queryHora="AND (".$queryHora.")";
    }
    $dia=date("N",strtotime($_POST['start']));  
    $queryDia="";
    if($dia==1){
           $queryDia="AND Lunes=1";
    }elseif($dia==2){
           $queryDia="AND Martes=1";
    }elseif($dia==3){
           $queryDia="AND Miercoles=1"; 
    }elseif($dia==4){
           $queryDia="AND Jueves=1"; 
    }elseif($dia==5){
           $queryDia="AND Viernes=1"; 
    }elseif($dia==6){
           $queryDia="AND Sabado=1"; 
    }elseif($dia==7){
           $queryDia="AND Domingo=1"; 
    }
 
    $query = "SELECT * FROM Horario_docente WHERE Id_ciclo=".$_POST['Id_ciclo']." AND Aula=".$_POST['aula']." ".$queryHora." ".$queryDia." GROUP BY Id_docente,Id_grupo";
    foreach($DaoHorarioDocente->advanced_query($query) as $row_Horario_docente){
        $texto="Se programó un evento para el aula ".$salon->getClave_aula()." el día ".$_POST['start']." de ".$_POST['HoraInicio']." a ".$_POST['HoraFin'].", acude con el supervisor académico.";
        $NotificacionesUsuario= new NotificacionesUsuario();
        $NotificacionesUsuario->setDateCreated(date('Y-m-d H:i:s'));
        $NotificacionesUsuario->setTitulo("Evento programado");
        $NotificacionesUsuario->setTexto($texto);
        $NotificacionesUsuario->setIdRel($row_Horario_docente->getId());
        $NotificacionesUsuario->setTipoRel("docente");
        $DaoNotificacionesUsuario->add($NotificacionesUsuario);
    }
        
    $start=$_POST['start']."T".$_POST['HoraInicio'];
    $end=$_POST['start']."T".$_POST['HoraFin'];
    
    if($_POST['Id_evento']>0){
         $evento=$DaoEventos->show($_POST['Id_evento']);
         $evento->setNombre($_POST['Nombre']);
         $evento->setComentarios($_POST['comentarios']);
         $evento->setCosto($_POST['costo']);
         $evento->setId_salon($_POST['aula']);
         $evento->setStart($start);
         $evento->setEnd($end);
         $evento->setId_ciclo($_POST['Id_ciclo']);
         $evento->setTipoEvento($_POST['tipo']);
         $DaoEventos->update($evento);   
    }else{ 
         $evento= new Eventos();
         $evento->setNombre($_POST['Nombre']);
         $evento->setComentarios($_POST['comentarios']);
         $evento->setCosto($_POST['costo']);
         $evento->setId_salon($_POST['aula']);
         $evento->setStart($start);
         $evento->setEnd($end);
         $evento->setId_ciclo($_POST['Id_ciclo']);
         $evento->setDateCreated(date('Y-m-d H:i:s'));
         $evento->setId_usu($usu->getId());
         $evento->setId_plantel($usu->getId_plantel());
         $evento->setTipoEvento($_POST['tipo']);
         $evento->setActivo(1);
         $id_evento=$DaoEventos->add($evento);   
    }
}



if($_POST['action']=="delete_evento"){
    $DaoEventos= new DaoEventos();
    $DaoEventos->delete($_POST['Id_evento']);
}



if($_POST['action']=="update_evento_drop"){
    //Fecha hacia la que se arrastro
    $DaoEventos= new DaoEventos();
    $start=$_POST['FechaInicio']."T".$_POST['HoraInicio'];
    $end=$_POST['FechaFin']."T".$_POST['HoraFin'];
    
    $evento=$DaoEventos->show($_POST['Id_evento']);
    $evento->setStart($start);
    $evento->setEnd($end);
    $DaoEventos->update($evento);   
}



if($_POST['action']=="update_evento_resize"){
    $DaoEventos= new DaoEventos();
    $evento=$DaoEventos->show($_POST['Id_evento']);
    $evento->setStart($_POST['start']);
    $evento->setEnd($_POST['end']);
    $DaoEventos->update($evento); 
}
