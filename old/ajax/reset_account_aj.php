<?php

require_once('activate_error.php');
require_once('../require_daos.php');

if ($_POST['action'] == "reset_account") {
    $respuesta= array();
    $base = new base();
    $DaoUsuarios = new DaoUsuarios();
    $DaoPlanteles = new DaoPlanteles();

    $tipo_error = array();
    $query = "SELECT * FROM usuarios_ulm WHERE Email_usu='" . $_POST['email'] . "' AND Baja_usu IS NULL";
    $usuario = $DaoUsuarios->existQuery($query);
    if ($usuario->getId() > 0) {

        $key = $base->generarKey();
        $rescover = $base->hashPassword($key);
        $usuario->setRecover_usu($rescover);
        $DaoUsuarios->update($usuario);

        $email = $usuario->getEmail_usu();
        $nombre = $usuario->getNombre_usu() . " " . $usuario->getApellidoP_usu() . " " . $usuario->getApellidoM_usu();
        $plantel = $DaoPlanteles->show($usuario->getId_plantel());

        $direccion = "";
        $DaoDirecciones = new DaoDirecciones();
        if ($plantel->getId_dir_plantel() > 0) {
            $dir = $DaoDirecciones->show($plantel->getId_dir_plantel());
            $direccion = $dir->getCalle_dir() . ' No. ' . $dir->getNumExt_dir() . ' Col. ' . $dir->getColonia_dir() . ' ' . $dir->getCiudad_dir() . ', ' . $dir->getEstado_dir();
        }

        $imagen = "";
        if (strlen($plantel->getId_img_logo()) > 0) {
            $imagen = 'src="http://www.' . $plantel->getDominio() . '/admin_ce/files/' . $plantel->getId_img_logo() . '.jpg"';
        }

        //Enviar email
        $texto_html = ' <html>
      <head></head>
      <body style="background-color: #F8F8F8;padding-top: 40px;" bgcolor="#F8F8F8">
                      <div id="container" style="width: 600px; background-color: white; font-family: arial; font-size: 13px; margin: auto;">
                           <div id="boxtext" style="width: 540px; padding: 30px;min-height: 300px;">
                           <img ' . $imagen . ' >
                           <h1 style="font-size: 25px; font-family: arial; color: #606060; font-weight: 100; margin-top: 20px; margin-bottom: 40px;">Restauraci&oacute;n de contrase&ntilde;a</h1>
                               <p style="margin-bottom: 20px;">Hemos recibido la solicitud para restaurar tu contrase&ntilde;a en el sitio ' . $plantel->getDominio() . ', para restaurarla por favor visita la siguiente liga:</p>
                               <p style="margin-bottom: 20px;"><a href="http://www.' . $plantel->getDominio() . '/admin_ce/reset_password.php?key=' . $rescover . '">http://www.' . $plantel->getDominio() . '/admin_ce/reset_password.php?key=' . $rescover . '</a></p>
					           <p style="margin-bottom: 20px;">Si no has realizado tu la solicitud, por favor ignora este correo.</p>

                           </div>
                          <div id="footer" style="background-color: #606060; color: white; font-size: 13px; width: 540px; padding: 30px;padding-top: 20px;padding-bottom: 20px;">
                       <table>
                                <tr>
                                       <td style="vertical-align: top; padding-right: 5px;" valign="top">
                                              <div id="text_footer" style="width: 226px; font-size: 13px; color: white; font-family: arial;">
                                                      <p style="margin-bottom: 10px;">Tel&eacute;fonos:<br> ' . $plantel->getTel() . '</p>
                                                      <p style="margin-bottom: 10px;">Domicilio: <br>' . $direccion . '</p>
                                               </div>
                                       </td>
                               </tr></table>
      </div>
                    </div>
                     <p class="footer" style="font-family: arial; font-size: 10px; color: black; width: 600px; margin: 5px auto auto;">' . date('Y') . ' ' . $plantel->getNombre_plantel() . '</p>
              </body>
      </html>';

        $texto_txt = '
********************************
Restauración de contraseña
********************************

**********************
Mensaje
**********************


Mensaje: Hemos recibido la solicitud para restaurar tu contrase&ntilde;a en el sitio '.$plantel->getAbreviatura().', para restaurarla por favor visita la siguiente liga:
(http://www.' . $plantel->getDominio() . '/admin_ce/reset_password.php?key=' . $rescover . ')



Teléfonos:  ' . $plantel->getTel() . '
Domicilio:  ' . $direccion . '

' . date('Y') . ' ' . $plantel->getNombre_plantel();

        $arrayData = array();
        $arrayData['Asunto'] = 'Restauración de contraseña';
        $arrayData['Html'] = $texto_html;
        $arrayData['Texto'] = $texto_txt;
        $arrayData['Id_plantel'] = $plantel->getId();

        $arrayData['Destinatarios'] = array();

        $Data['email'] = $email;
        $Data['name'] = $nombre;
        array_push($arrayData['Destinatarios'], $Data);

        $base->send_email($arrayData);
    }else{
        $respuesta['status']="error";
        echo json_encode($respuesta);
    }
}
