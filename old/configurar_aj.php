<?php
ini_set('max_execution_time', 0);
error_reporting(E_ALL);
require_once('Connections/cnn.php');
require_once('estandares/class_interesados.php');
require_once('estandares/class_ofertas.php');
require_once('estandares/class_archivos.php');
require_once("estandares/class_usuarios.php");
require_once('estandares/class_materias.php');
require_once('estandares/class_grupos.php');
require_once('estandares/class_ciclos.php');
require_once('estandares/class_miscelaneos.php');
require_once('estandares/class_descuentos.php');
require_once('estandares/class_motivos_bajas.php');
require_once('estandares/class_tipos_usuarios.php');
require_once('estandares/class_permisos.php');
require_once('estandares/class_medios_enterar.php');
require_once('estandares/class_metodos_pago.php');
require_once('estandares/class_categorias_libros.php');
require_once('estandares/class_docentes.php');

require_once('clases/DaoMediosEnterar.php');
require_once('clases/DaoAulas.php');
require_once('clases/DaoEventosCiclo.php');
require_once('clases/DaoTurnos.php');
require_once('clases/DaoTiposActivo.php');
require_once('clases/DaoHorarioDocente.php');
require_once('clases/DaoNotificacionesUsuario.php');
require_once('clases/DaoParametros.php');
require_once('clases/DaoParametrosPlantel.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoPagosCiclo.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoGrupos.php');
require_once('clases/DaoBecas.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoCiclosAlumno.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoMateriasCicloAlumno.php');
require_once('clases/DaoHorarioDocente.php');
require_once('clases/DaoMiscelaneos.php');
require_once('clases/DaoPeriodosExamenes.php');
require_once('clases/DaoPeriodosVacaciones.php');
require_once('clases/DaoDiasInhabiles.php');
require_once('clases/DaoHoras.php');
require_once('clases/DaoTiposUsuarios.php');
require_once('clases/modelos/base.php');
require_once('clases/modelos/Becas.php');
require_once('clases/modelos/Miscelaneos.php');
require_once('clases/modelos/TiposUsuarios.php');
require_once('clases/modelos/ParametrosPlantel.php');
require_once('clases/modelos/PeriodosExamenes.php');
require_once('clases/modelos/PeriodosVacaciones.php');
require_once('clases/modelos/NotificacionesUsuario.php');
require_once('clases/modelos/Turnos.php');
require_once('clases/modelos/TiposActivo.php');
require_once('clases/modelos/EventosCiclo.php');

if (!function_exists("GetSQLValueString")) {

    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
        $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

        $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
}

mysql_select_db($database_cnn, $cnn);
mysql_query("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');

if ($_POST['action'] == "mostrar_box_medio") {
    $DaoMediosEnterar= new DaoMediosEnterar();
    $nombre="";
    $fechIni="";
    $fechaFin="";
    $id_medio="";
    if ($_POST['Id_medio'] > 0) {
        $medio=$DaoMediosEnterar->show($_POST['Id_medio']);
        $nombre=$medio->getMedio();
        $fechIni=$medio->getFechaIni();
        $fechaFin=$medio->getFechaFin();
        $id_medio=$medio->getId();
    }
    ?>
    <div class="box-emergente-form box-eventos-ciclo">
        <h1>Medio de comunicaci&oacute;n</h1>
        <p>Nombre<br><input type="text"  id="nombre" value="<?php echo $nombre ?>"/></p>
        <p>Fecha inicio<br><input type="date" id="fechaIni" value="<?php echo $fechIni ?>"/></p>
        <p>Fecha fin<br><input type="date"  id="fechaFin" value="<?php echo $fechaFin ?>"/></p>
        <button onclick="save_medio(<?php echo $id_medio ?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cerrar</button>
    </div>
    <?php
}

if ($_POST['action'] == "save_medio") {
    $DaoMediosEnterar= new DaoMediosEnterar();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    if ($_POST['Id_medio']>0){
        $medio=$DaoMediosEnterar->show($_POST['Id_medio']);
        $medio->setMedio($_POST['nombre'] );
        $medio->setFechaIni($_POST['fechaIni'] );
        $medio->setFechaFin($_POST['fechaFin'] );
        $DaoMediosEnterar->update($medio);
    } else {

        $medio= new MediosEnterar();
        $medio->setMedio($_POST['nombre'] );
        $medio->setFechaIni($_POST['fechaIni'] );
        $medio->setFechaFin($_POST['fechaFin'] );
        $medio->setId_plantel($usu->getId_plantel());
        $medio->setActivo(1);
        $DaoMediosEnterar->add($medio);
    }
    update_medios();
}


if ($_POST['action'] == "delete_medio") {
    $DaoMediosEnterar= new DaoMediosEnterar();
    $medio=$DaoMediosEnterar->show($_POST['Id_medio']);
    $medio->setActivo(0);
    $DaoMediosEnterar->update($medio);
    update_medios();
}


if ($_POST['action'] == "mostrar_box_metodo_pago") {
    if ($_POST['Id_met'] > 0) {
        $class_metodos_pago = new class_metodos_pago($_POST['Id_met']);
        $metodo=$class_metodos_pago->get_metodo();
    }
    ?>
    <div id="box_emergente">
        <h1><img src="images/configu.png" alt="configu" width="40" height="40" />M&eacute;todo de pago</h1>
        <p><input type="text" placeholder="Nombre" id="nombre" value="<?php echo $metodo['Nombre'] ?>"/></p>	
        <button onclick="save_metodo_pago(<?php echo $_POST['Id_met'] ?>)" class="button">Guardar</button>
        <span onclick = "ocultar_error_layer()">x</span>
    </div>
    <?php
}

if ($_POST['action'] == "mostrar_box_medio_contacto") {
    if ($_POST['Id_medio'] > 0) {
        $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
        $usu = $class_usuarios->get_usu();
        $query_medios_ulm = "SELECT * FROM Medios_contacto WHERE Id_plantel=".$usu['Id_plantel']." AND Id_medio=" . $_POST['Id_medio'];
        $medios_ulm = mysql_query($query_medios_ulm, $cnn) or die(mysql_error());
        $row_medios_ulm = mysql_fetch_array($medios_ulm);
    }
    ?>
    <div id="box_emergente">
        <h1><img src="images/configu.png" alt="configu" width="40" height="40" />Medio de contacto</h1>
        <p><input type="text" placeholder="Nombre del medio de contacto" id="nombre"  value="<?php echo $row_medios_ulm['Nombre_medio'] ?>"/></p>	
        <button onclick="save_medio_contacto(<?php echo $_POST['Id_medio'] ?>)" class="button">Guardar</button>
        <span onclick = "ocultar_error_layer()">x</span>
    </div>
    <?php
}





if ($_POST['action'] == "save_metodo_pago") {
    if ($_POST['Id_met']>0){
    $sql_nueva_entrada = sprintf("UPDATE Metodo_pago SET Nombre=%s WHERE Id_met=%s", 
            GetSQLValueString($_POST['nombre'] , "text"), 
            GetSQLValueString($_POST['Id_met'] , "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
} else {
    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
    $usu = $class_usuarios->get_usu();
    $sql_nueva_entrada = sprintf("INSERT INTO Metodo_pago (Nombre,Id_plantel) VALUES (%s,%s);", 
            GetSQLValueString($_POST['nombre'] , "text"),
            GetSQLValueString($usu['Id_plantel'] , "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
}
update_metodos_pago();
}

if ($_POST['action'] == "save_medio_contacto") {
    if ($_POST['Id_medio']>0){
    $sql_nueva_entrada = sprintf("UPDATE Medios_contacto  SET Nombre_medio=%s WHERE Id_medio=%s", 
            GetSQLValueString($_POST['nombre'] , "text"), 
            GetSQLValueString($_POST['Id_medio'] , "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
} else {
    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
    $usu = $class_usuarios->get_usu();
    $sql_nueva_entrada = sprintf("INSERT INTO Medios_contacto (Nombre_medio,Id_plantel) VALUES (%s,%s);", 
            GetSQLValueString($_POST['nombre'] , "text"),
            GetSQLValueString($usu['Id_plantel'] , "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
}
update_medios_contacto();
}

if ($_POST['action'] == "change_section") {
    if ($_POST['tipo'] == "medios") {
        ?>
        <div id ="box_top">
            <h1><i class="fa fa-phone"></i> Medios de comunicación</h1>
            <span id="add_medio" onclick="mostrar_box_medio()">+ A&ntilde;adir medio</span>
        </div>
        <div id="mascara_tabla">
            <?php
            update_medios();
            ?>
        </div>
        <?php
    }elseif($_POST['tipo'] == "medios_contacto"){
         ?>
        <div id ="box_top">
           <h1><i class="fa fa-location-arrow"></i> Medios de contacto</h1>
            <span id="add_medio" onclick="mostrar_box_medio_contacto()">+ A&ntilde;adir medio de contacto</span>
        </div>
        <div id="mascara_tabla">
            <?php
            update_medios_contacto();
            ?>
        </div>
        <?php   
    }elseif ($_POST['tipo'] == "documentos") {

        ?>
        <div id="box_top">
            <h1><i class="fa fa-file-text-o"></i> Documentos</h1>
            <span id="add_medio" onclick="mostrar_box_documento()">+ A&ntilde;adir documento</span>
        </div>
        <div id="mascara_tabla">
            <?php
            update_documentos();
            ?>
        </div>
        <?php
    } elseif ($_POST['tipo'] == "documentos_oferta") {
        ?>
        <div id="box_top">
            <h1><i class="fa fa-file-text-o"></i> Documentos ofertas</h1>
        </div>
        <div id="mascara_tabla">
            <?php
            update_documentos_ofertas();
            ?>
        </div>
        <?php
    } elseif ($_POST['tipo'] == "ciclos") {
        ?>
        <div id="box_top">
            <h1><i class="fa fa-calendar"></i> Ciclos</h1>
            <span id="add_medio" onclick="mostrar_box_ciclos()">+ A&ntilde;adir ciclo</span>
        </div>
        <div id="mascara_tabla">
                    <?php
                    update_ciclos();
             ?>
        </div>
        <?php
    }elseif ($_POST['tipo'] == "parametros") {
        $param= array();
        $DaoCiclos= new DaoCiclos();
        $DaoUsuarios= new DaoUsuarios();
        $DaoTiposUsuarios= new DaoTiposUsuarios();
        $DaoParametros= new DaoParametros();
        $DaoParametrosPlantel= new DaoParametrosPlantel();
        $base= new base();
        $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

        //Parametros del plantel configurados
        foreach($DaoParametrosPlantel->getParametrosPlantel($usu->getId_plantel()) as $para){
           if($para->getId_param()>0){
              $parametro=$DaoParametros->mostrarParametro($para->getId_param());
              $param[$parametro->getText_param()]=$para->getValor();
           }
        }
        ?>
        <div id="box_top">
            <h1><i class="fa fa-cogs"></i> Parametros</h1>
        </div>
        <div id="mascara_tabla">
                <div class="seccion">
                    <ul id="list-param">
                           <li>
                               <h1><i class="fa fa-check-square"></i> Disponibilidad docente</h1>
                               <p>Activo<input type="checkbox" <?php if($param['Disponibilidad']==1){ ?> checked="checked" <?php } ?> id="Disponibilidad"/></p>
                               <p>Ciclo<br>
                                    <select id="CicloDisponibilidad">
                                        <option value="0">Selecciona el ciclo</option>
                                        <?php
                                        foreach($DaoCiclos->getCiclosFuturos() as $k=>$v){
                                         ?>
                                              <option value="<?php echo $v->getId() ?>" <?php if($param['CicloDisponibilidad']==$v->getId()){?> selected="selected" <?php }?> ><?php echo $v->getClave() ?></option>
                                          <?php
                                          }
                                          ?>
                                    </select>
                               </p>
                               <p><button onclick="save_disponibilidad(4,5)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-usd"></i> Monto entrega tardía de libros</h1>
                               <p><input type="text" value="<?php echo $param['CargoEntregaLibroTarde']?>" id="cargoTardioLibro"/></p>
                               <p><button onclick="saveCargotardioLibro(16)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-percent"></i> Porcentaje descuento hora penalizada</h1>
                               <p><input type="text" value="<?php echo $param['PorcentajeHoraPenalizada']?>" id="PorcentajeHoraPenalizada"/></p>
                               <p><button onclick="savePorcentajeHoraPenalizada(29)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-link"></i> Url noticias alumnos</h1>
                               <p><input type="text" value="<?php echo $param['UrlNoticiasAlumnos']?>" id="UrlNoticiasAlumnos"/></p>
                               <p><button onclick="saveUrlNoticiasAlumnos(17)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-link"></i> Url noticias docentes</h1>
                               <p><input type="text" value="<?php echo $param['UrlNoticiasDocentes']?>" id="UrlNoticiasDocentes"/></p>
                               <p><button onclick="saveUrlNoticiasDocentes(18)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-envelope-o"></i> Email de contacto para la sección de alumnos</h1>
                               <p><input type="text" value="<?php echo $param['EmailContactoSeccionAlumnos']?>" id="EmailContactoSeccionAlumnos"/></p>
                               <p><button onclick="saveEmailContactoSeccionAlumnos(20)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-envelope-o"></i> Email de contacto para captura de calificaciones de alumnos deudores</h1>
                               <p><input type="text" value="<?php echo $param['EmailCapturaCalificacionAlumnoDeudor']?>" id="EmailCapturaCalificacionAlumnoDeudor"/></p>
                               <p><button onclick="saveEmailCapturaCalificacionAlumnoDeudor(21)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-envelope-o"></i> Email de contacto Departamento Psicopedagico</h1>
                               <p><input type="text" value="<?php echo $param['EmailDepartamentoPsicopedagico']?>" id="EmailDepartamentoPsicopedagico"/></p>
                               <p><button onclick="saveEmailDepartamentoPsicopedagico(31)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-google-plus"></i> ApiKey Google</h1>
                               <p><input type="text" value="<?php echo $param['ApiKeyGoogle']?>" id="ApiKeyGoogle"/></p>
                               <p><button onclick="saveApiKeyGoogle(24)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-google-plus"></i> ClientSecret Google</h1>
                               <p><input type="text" value="<?php echo $param['ClientSecretGoogle']?>" id="ClientSecretGoogle"/></p>
                               <p><button onclick="saveClientSecretGoogle(25)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-google-plus"></i> ClientId Google</h1>
                               <p><input type="text" value="<?php echo $param['ClientIdGoogle']?>" id="ClientIdGoogle"/></p>
                               <p><button onclick="saveClientIdGoogle(26)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-key"></i> Llave de envio de emails Mandrill (producción)</h1>
                               <p><input type="text" value="<?php echo $param['KeyMandrill']?>" id="KeyMandrill"/></p>
                               <p><button onclick="saveKeyMandrill(6)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-key"></i> Llave de envio de emails Mandrill (pruebas)</h1>
                               <p><input type="text" value="<?php echo $param['TestMandrill']?>" id="TestMandrill"/></p>
                               <p><button onclick="saveTestMandrill(7)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-calendar" aria-hidden="true"></i> Día del mes en que se genera la primera mensualidad de un alumno</h1>
                               <p><input type="text" value="<?php echo $param['DiaInicioMensualidades']?>" id="DiaInicioMensualidades"/></p>
                               <p><button onclick="saveDiaInicioMensualidades(32)">Guardar</button></p>
                           </li><br>
                           <li>
                               <h1><i class="fa fa-bell-o"></i> Recibir notificaciónes por entrega tardia de libros</h1>
                               <ul id="lista-tipos-usuarios" class="lista-tipos-usuarios">
                               <?php
                                 $arrayTipos=  explode(";", $param['RecibirNotificacionPorNoEntregarLibro']);
                                 foreach($DaoTiposUsuarios->getTiposUsuarios() as $tipo){

                                        ?>
                                   <li>
                                       <table>
                                           <tr>
                                               <td><input type="checkbox" data-id="<?php echo $tipo->getId()?>" <?php if(in_array($tipo->getId(), $arrayTipos)){?> checked="checked" <?php } ?>/></td>
                                               <td><?php echo $tipo->getNombre_tipo() ?></td>
                                           </tr>
                                       </table>
                                   </li>
                               <?php
                                }
                               ?>
                               </ul>
                               <p><button onclick="RecibirNotificacionPorNoEntregarLibro(19)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-bell-o"></i> Recibir notificaciónes de contacto a interesados</h1>
                               <ul id="lista-tipos-usuarios-notificaciones-interesados" class="lista-tipos-usuarios">
                               <?php
                                 $arrayTipos=  explode(";", $param['RecibirNotificacionesSeguimientoInteresados']);
                                 foreach($DaoTiposUsuarios->getTiposUsuarios() as $tipo){

                                        ?>
                                   <li>
                                       <table>
                                           <tr>
                                               <td><input type="checkbox" data-id="<?php echo $tipo->getId()?>" <?php if(in_array($tipo->getId(), $arrayTipos)){?> checked="checked" <?php } ?>/></td>
                                               <td><?php echo $tipo->getNombre_tipo() ?></td>
                                           </tr>
                                       </table>
                                   </li>
                               <?php
                                }
                               ?>
                               </ul>
                               <p><button onclick="RecibirNotificacionesSeguimientoInteresados(30)">Guardar</button></p>
                           </li>
                           <li>
                               <h1><i class="fa fa-bell-o"></i> Recibir notificaciónes por captura de asistencias fuera de tiempo</h1>
                               <ul id="lista-tipos-usuarios-notificaciones-captura-asistencias" class="lista-tipos-usuarios">
                               <?php
                                 $arrayTipos=  explode(";", $param['RecibirNotificacionesCapturaAistenciasFueraDeTiempo']);
                                 foreach($DaoTiposUsuarios->getTiposUsuarios() as $tipo){
                                        ?>
                                   <li>
                                       <table>
                                           <tr>
                                               <td><input type="checkbox" data-id="<?php echo $tipo->getId()?>" <?php if(in_array($tipo->getId(), $arrayTipos)){?> checked="checked" <?php } ?>/></td>
                                               <td><?php echo $tipo->getNombre_tipo() ?></td>
                                           </tr>
                                       </table>
                                   </li>
                               <?php
                                }
                               ?>
                               </ul>
                               <p><button onclick="RecibirNotificacionesCapturaAistenciasFueraDeTiempo(33)">Guardar</button></p>
                           </li>
                    </ul>


                </div>

            </div>
        <?php
    } elseif($_POST['tipo']=="aulas") {
        ?>
        <div id="box_top">
            <h1><i class="fa fa-university"></i> Aulas</h1>
            <span id="add_medio" onclick="mostrar_box_aula()">+ A&ntilde;adir aula</span>
        </div>
        <div id="mascara_tabla">
            <?php
            update_aulas();
            ?>
        </div>
        <?php
    } elseif ($_POST['tipo'] == "grupos") {
        $DaoCiclos= new DaoCiclos();
        $DaoUsuarios= new DaoUsuarios();
        $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        $cicloActual=$DaoCiclos->getActual($usu->getId_plantel());
        ?>
        <div id="box_top">
            <h1><i class="fa fa-users"></i> Grupos</h1>
            <span  onclick="mostrar_box_grupo()">+ A&ntilde;adir grupo</span>
        </div>

        <div class="seccion">
                <h2>Selecciona el ciclo</h2>
                <ul class="form">
                    <li><select id="ciclos" onchange="update_list_grupos_ciclo()">
                            <option value="0"></option>
                            <?php
                                foreach($DaoCiclos->showAll() as $k=>$v){
                                 ?>
                                    <option value="<?php echo $v->getId() ?>" <?php if($cicloActual->getId()==$v->getId()){?> selected="selected" <?php }?> ><?php echo $v->getClave() ?></option>
                                  <?php
                                  }
                                  ?>
                        </select></li>

                </ul>
                <span class="linea"></span>
        </div>

        <div id="mascara_tabla">
            <?php
            update_grupos_ciclo($cicloActual->getId());
            ?>

        </div>
        <?php
    }elseif($_POST['tipo']=="miscelaneos") {
        ?>
        <div id="box_top">
            <h1><i class="fa fa-usd"></i> Miscelaneos</h1>
            <span id="add_medio" onclick="mostrar_box_mis()">+ A&ntilde;adir miscelaneo</span>
        </div>
        <div id="mascara_tabla">
            <?php
            update_miscelaneos();
            ?>
        </div>
        <?php
    }elseif($_POST['tipo']=="descuentos") {
        ?>
        <div id="box_top">
            <h1><i class="fa fa-th"></i> Descuentos</h1>
            <span id="add_medio" onclick="mostrar_box_descuento()">+ A&ntilde;adir descuento</span>
        </div>
        <div id="mascara_tabla">
            <?php
            update_descuentos();
            ?>
        </div>
        <?php
    }elseif($_POST['tipo']=="motivos_bajas") {
        ?>
        <div id="box_top">
            <h1><i class="fa fa-eraser"></i> Motivos de baja</h1>
            <span id="add_medio" onclick="mostrar_motivo_baja()">+ A&ntilde;adir motivo</span>
        </div>
        <div id="mascara_tabla">
            <?php
            update_motivos_bajas();
            ?>
        </div>
        <?php
    }elseif($_POST['tipo']=="tipos_usuarios"){
            ?>
        <div id="box_top">
            <h1><i class="fa fa-user"></i> Tipos de usuarios</h1>
            <span id="add_medio" onclick="mostrar_tipo_usu()">+ A&ntilde;adir tipo de usuario</span>
        </div>
        <div id="mascara_tabla">
            <?php
            update_tipos_usuarios();
            ?>
        </div>
        <?php
    }elseif ($_POST['tipo'] == "metodos_pago") {
        ?>
        <div id="box_top">
            <h1><i class="fa fa-money"></i> M&eacute;todos de pago</h1>
            <span id="add_medio" onclick="mostrar_box_metodo_pago()">+ A&ntilde;adir m&eacute;todo</span>
        </div>
        <div id="mascara_tabla">
                    <?php
                    update_metodos_pago();
             ?>
        </div>
        <?php
    }elseif ($_POST['tipo'] == "categorias_libro") {
        ?>
        <div id="box_top">
            <h1><i class="fa fa-book"></i> Categor&iacute;as libro</h1>
            <span id="add_medio" onclick="mostrar_box_categoria()">+ A&ntilde;adir categor&iacute;a</span>
        </div>
        <div id="mascara_tabla">
                    <?php
                    update_categorias_libros();
             ?>
        </div>
        <?php
    }elseif ($_POST['tipo'] == "becas") {
        ?>
        <div id="box_top">
            <h1><i class="fa fa-list"></i> Tipos de beca</h1>
            <span id="add_medio" onclick="mostrar_box_beca()">+ A&ntilde;adir beca</span>
        </div>
        <div id="mascara_tabla">
                    <?php
                    update_becas();
             ?>
        </div>
        <?php
    }elseif ($_POST['tipo'] == "tiposactivo") {
        ?>
        <div id="box_top">
            <h1><i class="fa fa-desktop" aria-hidden="true"></i> Tipos de activo</h1>
            <span id="add_medio" onclick="mostrar_box_tipo_activo()">+ A&ntilde;adir tipo</span>
        </div>
        <div id="mascara_tabla">
                    <?php
                    update_tipos_activo();
             ?>
        </div>
        <?php
    }elseif ($_POST['tipo'] == "turnos") {
        ?>
        <div id="box_top">
            <h1><i class="fa fa-clock-o" aria-hidden="true"></i> Turnos</h1>
            <span id="add_medio" onclick="mostrar_box_turno()">+ A&ntilde;adir turno</span>
        </div>
        <div id="mascara_tabla">
                    <?php
                    update_turnos();
             ?>
        </div>
        <?php
    }
}

function update_ciclos() {
    $DaoCiclos= new DaoCiclos();
    $DaoDiasInhabiles= new DaoDiasInhabiles();
    foreach($DaoCiclos->showAll() as $ciclo){
        ?>
        <div class="box_calendar">
            <div class="box_left">
                <h1> <?php echo $ciclo->getClave() ?><br> <?php echo $ciclo->getNombre_ciclo() ?> </h1>
                <p>Fecha inicio:  <?php echo $ciclo->getFecha_ini() ?></p>
                <p>Fecha fin: <?php echo $ciclo->getFecha_fin() ?> </p>
                <p><span class="editar_ciclo" onclick="mostrar_box_ciclos(<?php echo $ciclo->getId(); ?>)"><i class="fa fa-pencil-square-o"></i> Editar </span></p>
                <br>
                <p>D&iacute;a inhabil<br><input type="date" class="add_dia_inhabil" /> </p>
                <p><span class="editar_ciclo" onclick="add_dia_inhabil(this,<?php echo $ciclo->getId(); ?>)"><i class="fa fa-plus-square"></i> Añadir d&iacute;a inh&aacute;bil </span></p>
                <p><span class="editar_ciclo" onclick="delete_ciclo(<?php echo $ciclo->getId();?>)"><i class="fa fa-trash"></i> Eliminar ciclo </span></p>
            </div>
            <div class="box_center"> <div class="datepicker"></div></div>
            <div class="box_right">
                <h1>D&iacute;as Inh&aacute;biles </h1>
                <div class="data_calendar">
                    <ul>
                     <?php
                       foreach($DaoDiasInhabiles->getDiasInhabilesCiclo($ciclo->getId()) as $diaInh){
                             ?>
                         <li><?php echo $diaInh->getFecha_inh() ?> <i class="fa fa-trash" onclick="delete_dia_inhabil(<?php echo $diaInh->getId()?>)"></i>
                             <input type="hidden" class="dias_inhabiles" value="<?php echo $diaInh->getFecha_inh()?>"/>
                         </li>

                       <?php
                        }
                       ?>
                    </ul>
                </div>
            </div>
            <div class="box_dias_ciclos">
            <?php
              //Rango de dias del ciclo
            $fechaInicio=strtotime($ciclo->getFecha_ini());
            $fechaFin=strtotime($ciclo->getFecha_fin());
            for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
            ?>
               <input type="hidden" class="dias_ciclo" value="<?php echo date("Y-m-d", $i);?>"/>
            <?php
            }
    ?>
            </div>
            <div class="seccion-periodo">
                <div class="box-color vacaciones"></div><h2 class="titulo-h2-periodos">Períodos vacacionales</h2>
                <p><span class="editar_ciclo" onclick="mostrar_add_periodo_vacaciones(<?php echo $ciclo->getId(); ?>)"><i class="fa fa-plus-square"></i> Añadir periodo</span></p>
                    <table class="lista-periodos">
                        <thead>
                            <tr>
                                <td class="td-count">#</td>
                                <td class="td-nombre">Nombre</td>
                                <td class="td-fecha">Fecha de inicio</td>
                                <td class="td-fecha">Fecha de fin</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count=1;
                             $DaoPeriodosVacaciones= new DaoPeriodosVacaciones();
                            foreach($DaoPeriodosVacaciones->getPeriodosVacacionesCiclo($ciclo->getId()) as $per){
                                ?>
                                    <tr>
                                        <td><?php echo $count?></td>
                                        <td><?php echo $per->getNombre()?></td>
                                        <td><?php echo $per->getFechaIni()?></td>
                                        <td><?php echo $per->getFechaFin()?></td>
                                        <td>
                                            <i class="fa fa-pencil-square-o" onclick="mostrar_add_periodo_vacaciones(<?php echo $ciclo->getId()?>,<?php echo $per->getId()?>)"></i>
                                            <i class="fa fa-trash" onclick="delete_periodo_vacaciones(<?php echo $per->getId()?>)"></i>
                                        </td>
                                    </tr>
                                <?php
                                $count++;
                                $fechaInicio=strtotime($per->getFechaIni());
                                $fechaFin=strtotime($per->getFechaFin());
                                for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
                                ?>
                                  <input type="hidden" class="dias_periodo_vacaciones" value="<?php echo date("Y-m-d", $i);?>"/>
                                 <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
            </div>
            <div class="seccion-periodo">
                <div class="box-color examenes"></div><h2 class="titulo-h2-periodos">Períodos de exámen</h2>
                <p><span class="editar_ciclo" onclick="mostrar_add_periodo(<?php echo $ciclo->getId(); ?>)"><i class="fa fa-plus-square"></i> Añadir periodo</span></p>
                    <table class="lista-periodos">
                        <thead>
                            <tr>
                                <td class="td-count">#</td>
                                <td class="td-nombre">Nombre</td>
                                <td class="td-fecha">Fecha de inicio</td>
                                <td class="td-fecha">Fecha de fin</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count=1;
                             $DaoPeriodosExamenes= new DaoPeriodosExamenes();
                            foreach($DaoPeriodosExamenes->getPeriodosExamenCiclo($ciclo->getId()) as $per){
                                ?>
                                    <tr>
                                        <td><?php echo $count?></td>
                                        <td><?php echo $per->getNombre()?></td>
                                        <td><?php echo $per->getFechaIni()?></td>
                                        <td><?php echo $per->getFechaFin()?></td>
                                        <td>
                                            <i class="fa fa-pencil-square-o" onclick="mostrar_add_periodo(<?php echo $ciclo->getId()?>,<?php echo $per->getId()?>)"></i>
                                            <i class="fa fa-trash" onclick="delete_periodo(<?php echo $per->getId()?>)"></i>
                                        </td>
                                    </tr>
                                <?php
                                $count++;
                                $fechaInicio=strtotime($per->getFechaIni());
                                $fechaFin=strtotime($per->getFechaFin());
                                for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
                                ?>
                                  <input type="hidden" class="dias_periodo_examen" value="<?php echo date("Y-m-d", $i);?>"/>
                                 <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
               </div>
                <div class="seccion-periodo">
                <div class="box-color eventos-ciclo"></div><h2 class="titulo-h2-periodos">Eventos del ciclo</h2>
                <p><span class="editar_ciclo" onclick="mostrar_evento_ciclo(<?php echo $ciclo->getId(); ?>)"><i class="fa fa-plus-square"></i> Añadir evento</span></p>
                    <table class="lista-periodos">
                        <thead>
                            <tr>
                                <td class="td-count">#</td>
                                <td class="td-nombre">Nombre</td>
                                <td class="td-fecha">Fecha de inicio</td>
                                <td class="td-fecha">Fecha de fin</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count=1;
                            $DaoEventosCiclo= new DaoEventosCiclo();
                            foreach($DaoEventosCiclo->getEventosCiclo($ciclo->getId()) as $eve){
                                ?>
                                    <tr>
                                        <td><?php echo $count?></td>
                                        <td><?php echo $eve->getNombre()?></td>
                                        <td><?php echo $eve->getFechaIni()?></td>
                                        <td><?php echo $eve->getFechaFin()?></td>
                                        <td>
                                            <i class="fa fa-pencil-square-o" onclick="mostrar_evento_ciclo(<?php echo $ciclo->getId()?>,<?php echo $eve->getId()?>)"></i>
                                            <i class="fa fa-trash" onclick="delete_evento_ciclo(<?php echo $eve->getId()?>)"></i>
                                        </td>
                                    </tr>
                                <?php
                                $count++;
                                $fechaInicio=strtotime($eve->getFechaIni());
                                $fechaFin=strtotime($eve->getFechaFin());
                                for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
                                ?>
                                  <input type="hidden" class="dias_evento_ciclo" value="<?php echo date("Y-m-d", $i);?>"/>
                                 <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
               </div>
             <input type="hidden" class="fecha_ini" value="<?php echo $ciclo->getFecha_ini();?>"/>
        </div>
        <?php
	}
}

function update_medios() {
    $DaoMediosEnterar= new DaoMediosEnterar();
    ?>
    <table class="table">
        <thead>
            <tr>
                <td>#</td>
                <td>Nombre</td>
                <td>Fecha de inicio</td>
                <td>Fecha de fin</td>
                <td class="td-center">Estatus</td>
                <td class="td-center">Acciones</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $count=1;
            foreach($DaoMediosEnterar->getMedios() as $medio){
                $txt="Inactivo";
                 $color="red";
                 if((date('Y-m-d')>=$medio->getFechaIni() && date('Y-m-d')<=$medio->getFechaFin()) || ($medio->getFechaIni()==null && $medio->getFechaFin()==null )){
                    $txt="Activo"; 
                    $color="green";
                }
                ?>
                    <tr>
                        <td><?php echo $count?></td>
                        <td><?php echo $medio->getMedio() ?></td>
                        <td><?php echo $medio->getFechaIni() ?></td>
                        <td><?php echo $medio->getFechaFin() ?></td>
                        <td class="td-center" style="color:<?php echo $color;?>"><?php echo $txt?></td>
                        <td class="td-center">
                            <button onclick="delete_medio(<?php echo $medio->getId(); ?>)">Eliminar</button>
                            <button onclick="mostrar_box_medio(<?php echo $medio->getId(); ?>)">Editar</button>
                        </td>
                    </tr>
                <?php  
                $count++;
            }
            ?>
        </tbody>
    </table>
    <?php
}

function update_metodos_pago() {
	?>
	<table class="table">
	    <thead>
	        <tr>
                    <td>#</td>
	            <td>Nombre</td>
	            <td>Acciones</td>
	        </tr>
	    </thead>
	    <tbody>
	        <?php
                $count=1;
                $class_metodos_pago= new class_metodos_pago();
                foreach($class_metodos_pago->get_metodos() as $k2=>$v2){
                    ?>
                	<tr>
                            <td><?php echo $count?></td>
	                    <td><?php echo $v2['Nombre'] ?></td>
	                    <td>
	                        <button onclick="delete_metodo_pago(<?php echo $v2['Id_met']; ?>)">Eliminar</button>
	                        <button onclick="mostrar_box_metodo_pago(<?php echo $v2['Id_met']; ?>)">Editar</button>
	                    </td>
	                </tr>
                    <?php  
                    $count++;
                }
	        ?>
	    </tbody>
	</table>
	<?php
}

function update_categorias_libros() {
	?>
	<table class="table">
	    <thead>
	        <tr>
                    <td>#</td>
	            <td>Nombre</td>
                    <td>C&oacute;digo</td>
                    <td>Subcategor&iacute;as</td>
	            <td style="text-align: center;">Acciones</td>
	        </tr>
	    </thead>
	    <tbody>
	        <?php
                $count=1;
                $class_categorias_libros= new class_categorias_libros();
                foreach($class_categorias_libros->get_categorias() as $k2=>$v2){
                    ?>
                	<tr>
                            <td><?php echo $count?></td>
	                    <td><?php echo $v2['Nombre'] ?></td>
                            <td><?php echo $v2['Codigo'] ?></td>
                            <td>
                                <table>
                                    <tbody>
                                        <?php
                                        foreach($v2['Subcategorias'] as $k3=>$v3){
                                        ?>
                                        <tr>
                                            <td>- <?php echo $v3['Nombre']?></td>
                                            <td><span onclick="delete_categoria_libros(<?php echo $v3['Id_cat']; ?>)">Eliminar</span></td>
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </td>
                            <td style="text-align: center;">
	                        <button onclick="delete_categoria_libros(<?php echo $v2['Id_cat']; ?>)">Eliminar</button>
	                        <button onclick="mostrar_box_categoria(<?php echo $v2['Id_cat']; ?>)">Editar</button>
                                <button onclick="mostrar_box_subcategoria(<?php echo $v2['Id_cat']; ?>)">Subcategoría</button>
	                    </td>
	                </tr>
                    <?php  
                    $count++;
                }
	        ?>
	    </tbody>
	</table>
	<?php
}

function update_descuentos() {
	global $cnn, $database_cnn;
	?>
	<table  class="table">
	    <thead>
	        <tr>
                    <td>#</td>
	            <td>Nombre</td>
                    <td>Porcentaje</td>
                    <td>Fecha inicio</td>
                    <td>Fecha fin</td>
	            <td>Acciones</td>
	        </tr>
	    </thead>
	    <tbody>
                
	        <?php
                $count=1;
                $class_descuentos= new class_descuentos();
                 foreach($class_descuentos->get_descuentos() as $k=>$v){
	                ?>
	                <tr>
                             <td><?php echo $count?></td>
                            <td><?php echo $v['Nombre'] ?></td>
                            <td><?php echo $v['Porcentaje'] ?> %</td>
                            <td><?php echo $v['Fecha_inicio'] ?></td>
                            <td><?php echo $v['Fecha_fin'] ?></td>
	                    <td>
	                        <button onclick="delete_descuento(<?php echo $v['Id_des']; ?>)">Eliminar</button>
	                        <button onclick="mostrar_box_descuento(<?php echo $v['Id_des']; ?>)">Editar</button>
	                    </td>
	                </tr>
	                <?php
                        $count++;
	        }
	        ?>
	    </tbody>
	</table>
	<?php
}

function update_motivos_bajas() {
	global $cnn, $database_cnn;
	?>
	<table  class="table">
	    <thead>
	        <tr>
                    <td>#</td>
	            <td>Nombre</td>
	            <td>Acciones</td>
	        </tr>
	    </thead>
	    <tbody>
                
	        <?php
                $count=1;
                $class_motivos_bajas= new class_motivos_bajas();
                 foreach($class_motivos_bajas->get_motivos_bajas() as $k=>$v){
	                ?>
	                <tr>
                            <td><?php echo $count?></td>
                            <td><?php echo $v['Nombre'] ?></td>
	                    <td>
	                        <button onclick="delete_motivo_baja(<?php echo $v['Id_mot']; ?>)">Eliminar</button>
	                        <button onclick="mostrar_motivo_baja(<?php echo $v['Id_mot']; ?>)">Editar</button>
	                    </td>
	                </tr>
	                <?php
                        $count++;
	        }
	        ?>
	    </tbody>
	</table>
	<?php
}


function update_medios_contacto() {
	global $cnn, $database_cnn;
           $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
           $usu = $class_usuarios->get_usu();
	?>
	<table class="table">
	    <thead>
	        <tr>
                    <td>#</td>
	            <td>Nombre</td>
	            <td>Acciones</td>
	        </tr>
	    </thead>
	    <tbody>
	        <?php
                $count=1;
	        $query_Medios_contacto = "SELECT * FROM Medios_contacto WHERE Id_plantel=".$usu['Id_plantel']." AND Activo_medio=1 ORDER BY Id_medio DESC";
	        $Medios_contacto = mysql_query($query_Medios_contacto, $cnn) or die(mysql_error());
	        $row_Medios_contacto = mysql_fetch_array($Medios_contacto);
	        $totalRows_Medios_contacto = mysql_num_rows($Medios_contacto);
	        if ($totalRows_Medios_contacto > 0) {
	            do {
	                ?>
	                <tr>
                            <td><?php echo $count?></td>
	                    <td><?php echo $row_Medios_contacto['Nombre_medio'] ?></td>
	                    <td>
	                        <button onclick="delete_medio_contacto(<?php echo $row_Medios_contacto['Id_medio']; ?>)">Eliminar</button>
	                        <button onclick="mostrar_box_medio_contacto(<?php echo $row_Medios_contacto['Id_medio']; ?>)">Editar</button>
	                    </td>
	                </tr>
	                <?php
                        $count++;
	            } while ($row_Medios_contacto = mysql_fetch_array($Medios_contacto));
	        }
	        ?>
	    </tbody>
	</table>
	<?php
}



function update_aulas() {
	global $cnn, $database_cnn;
           $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
           $usu = $class_usuarios->get_usu();
	?>
	<table class="table">
	    <thead>
	        <tr>
                    <td>#</td>
	            <td>Clave</td>
	            <td>Nombre</td>
	            <td>Tipo de Aula</td>
	            <td>Capacidad</td>
	            <td>Acciones</td>
	        </tr>
	    </thead>
	    <tbody>
	        <?php
                $count=1;
	        $query_aulas = "SELECT * FROM aulas WHERE Activo_aula=1 AND Id_plantel=".$usu['Id_plantel']." ORDER BY Nombre_aula ASC";
	        $aulas = mysql_query($query_aulas, $cnn) or die(mysql_error());
	        $row_aulas = mysql_fetch_array($aulas);
	        $totalRows_aulas= mysql_num_rows($aulas);
	        if($totalRows_aulas > 0) {
	            do {
                          $tipo_aula="";
                          switch($row_aulas['Tipo_aula']){
                              case 1: $tipo_aula="Laboratorio";
                              break;
                              case 2: $tipo_aula="Instrucci&oacute;n";
                              break;
                              case 3: $tipo_aula="Especial";
                              break;
                              case 4: $tipo_aula="Te&oacute;rica";
                              break;
                          
                          }
	               ?>
	                <tr>
                            <td><?php echo $count?></td>
	                    <td><?php echo $row_aulas['Clave_aula'] ?></td>
	                    <td><?php echo $row_aulas['Nombre_aula'] ?></td>
	                    <td><?php echo $tipo_aula ?></td>
	                    <td><?php echo $row_aulas['Capacidad_aula'] ?></td>
	                    <td>
	                        <button onclick="delete_aula(<?php echo $row_aulas['Id_aula']; ?>)">Eliminar</button>
	                        <button onclick="mostrar_box_aula(<?php echo $row_aulas['Id_aula']; ?>)">Editar</button>
	                    </td>
	                </tr>
	                <?php
                        $count++;
	            } while ($row_aulas = mysql_fetch_array($aulas));
	        }
	        ?>
	    </tbody>
	</table>
	<?php
}

function update_miscelaneos() {
	?>
	<table class="table">
	    <thead>
	        <tr>
                    <td>#</td>
	            <td>Concepto</td>
	            <td>Costo</td>
	            <td>Acciones</td>
	        </tr>
	    </thead>
	    <tbody>
	        <?php   
                $count=1;
                $DaoMiscelaneos= new DaoMiscelaneos();
	        foreach($DaoMiscelaneos->showAll() as $mis){
	               ?>
	                <tr>
                             <td><?php echo $count; ?></td>
	                    <td><?php echo $mis->getNombre_mis() ?></td>
                            <td>$ <?php echo number_format($mis->getCosto_mis(),2) ?></td>
	                    <td>
	                        <button onclick="delete_mis(<?php echo $mis->getId(); ?>)">Eliminar</button>
	                        <button onclick="mostrar_box_mis(<?php echo $mis->getId(); ?>)">Editar</button>
	                    </td>
	                </tr>
	                <?php
                        $count++;
	            }
                    
	        ?>
	    </tbody>
	</table>
	<?php
}



function update_documentos() {
	global $cnn, $database_cnn;
           $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
           $usu = $class_usuarios->get_usu();
	?>
	<table class="table">
	    <thead>
	        <tr>
                    <td>#</td>
	            <td>Nombre</td>
	            <td>Acciones</td>
	        </tr>
	    </thead>
	    <tbody>
	        <?php
                $count=1;
	        $query_archivos_ulm = "SELECT * FROM archivos_ulm WHERE Id_plantel=".$usu['Id_plantel']." ORDER BY Id_file DESC";
	        $archivos_ulm = mysql_query($query_archivos_ulm, $cnn) or die(mysql_error());
	        $row_archivos_ulm = mysql_fetch_array($archivos_ulm);
	        $totalRows_archivos_ulm = mysql_num_rows($archivos_ulm);
	        if ($totalRows_archivos_ulm > 0) {
	            do {
	                ?>
	                <tr >
                            <td><?php echo $count; ?></td>
	                    <td><?php echo $row_archivos_ulm['Nombre_file'] ?> </td>
	                    <td>
	                        <button onclick="delete_file(<?php echo $row_archivos_ulm['Id_file']; ?>)">Eliminar</button>
	                        <button onclick="mostrar_box_documento(<?php echo $row_archivos_ulm['Id_file']; ?>)">Editar</button>
	                    </td>
	                </tr>
	                <?php
                        $count++;
	            } while ($row_archivos_ulm = mysql_fetch_array($archivos_ulm));
	        }
	        ?>
	    </tbody>
	</table>
	<?php
}

function update_documentos_ofertas() {
	$class_ofertas = new class_ofertas();
	?>
	<table class="table">
	    <thead>
	        <tr>
	            <td>Oferta</td>
	            <td>Documentos</td>
	            <td>Acciones</td>
	        </tr>
	    </thead>
	    <tbody>
	        <?php
	        foreach ($class_ofertas->get_ofertas() as $k => $v) {
	            ?>
	            <tr>
	                <td><?php echo $v['Nombre_oferta'] ?> </td>
	                <td>
	                    <ul>
	                        <?php
	                        foreach ($v['Documentos'] as $k2 => $v2) {
	                            ?>
	                            <li>- <?php echo $v2['Nombre_file'] ?></li>
	                            <?php
	                        }
	                        ?>
	                    </ul>
	                </td>
	                <td> <button onclick="mostrar_box_documentos_ofertas(<?php echo $v['Id_oferta']; ?>)">Editar</button></td>
	            </tr>
	            <?php
	        }
	        ?>
	    </tbody>
	</table>
	<?php
}




function update_grupos_ciclo($Id_ciclo) {
        $DaoMaterias= new DaoMaterias();
        $DaoGrupos= new DaoGrupos();
        $DaoTurnos= new DaoTurnos();
        $DaoOrientaciones= new DaoOrientaciones();
        $DaoHorarioDocente= new DaoHorarioDocente();
	?>
        <ul class="form">
            <li>Buscar<br><input type="text" id="buscargrupos" onkeyup="buscarGrupos()" placeholder="Nombre de la materia o clave grupal"/></li><br>
            <li><button class="btns btns-primary btns-sm" onclick="confirmarAsignacion()">Asignación masiva de alumnos</button></li>
        </ul>
           <table class="table" id="table-grupos">
	    <thead>
	        <tr>
                    <td>#</td>
	            <td>Clave grupal</td>
                    <td>Materia</td>
                    <td>Orientaci&oacute;n</td>
	            <td class="td-center">Turno</td>
	            <td class="td-center">Capacidad</td>
                    <td class="td-center">Disponibles</td>
                     <td class="td-center"><input type="checkbox" id="all-grupos" onchange="marcarGrupos()"></td>
                    <td class="td-center">Acciones</td>
	        </tr>
	    </thead>
	    <tbody>
                <?php
                if($Id_ciclo>0){
                    $count=1;
                    foreach($DaoGrupos->getGruposByCiclo($Id_ciclo) as $k=>$v){
                            $mat = $DaoMaterias->show($v->getId_mat());
                            $tur = $DaoTurnos->show($v->getTurno());
                            $turno=$tur->getNombre();

                            $nombre_ori="";
                            if($v->getId_ori()>0){
                              $ori = $DaoOrientaciones->show($v->getId_ori());
                              $nombre_ori=$ori->getNombre();
                            }
                            $disponibilidad=$DaoGrupos->getDisponibilidadGrupo($v->getId());
                            $red="pink";
                            $horario=$DaoHorarioDocente->getHorarioByGrupoCiclo($v->getId(),$Id_ciclo);
                            if($horario->getId_docente()>0){
                               $red=""; 
                            }
                    ?>
                    <tr id-data="<?php echo $v->getId()?>" count="<?php echo $count;?>" class="<?php echo $red?>">
                        <td><?php echo $count; ?></td>
                        <td><?php echo $v->getClave()?> </td>
                        <td style="width: 170px;"><?php echo $mat->getNombre()?></td>
                        <td><?php echo $nombre_ori?></td>
                        <td class="td-center"><?php echo $turno;?></td>
                        <td class="td-center"><?php echo $v->getCapacidad()?></td>
                        <td class="td-center <?php if($disponibilidad<=0){ ?> sin_disponibilidad <?php } else{ ?> con_disponibilidad <?php } ?> " > <?php echo $disponibilidad?></td>
                        <td class="td-center checkbox"><input type="checkbox" <?php if(strlen($red)>0){?> disabled="disabled" <?php } ?>></td>
                        <td class="td-center"> 
                            <i class="fa fa-pencil-square-o" onclick="mostrar_box_grupo(<?php echo $v->getId()?>)"></i>
                            <i class="fa fa-trash" onclick="delete_grupo(<?php echo $v->getId()?>)"></i>
                            <i class="fa fa-plus-square" onclick="validar_existencia_docente(<?php echo $v->getId()?>)"></i>
                            <i class="fa fa-users" onclick="mostrar_alum(<?php echo $v->getId()?>)"></i>
                        </td>
                    </tr>
                    <?php
                    $count++;
                    }
                }
                ?>
	</table>
        <input type="hidden" id="Id_ciclo" value="<?php echo $Id_ciclo;?>"/>
	<?php
}


function update_tipos_activo() {
    ?>
    <table  class="table">
        <thead>
            <tr>
                <td>#</td>
                <td>Nombre</td>
                <td class="td-center">Acciones</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $count=1;
            $DaoTiposActivo= new DaoTiposActivo();
             foreach($DaoTiposActivo->getTipos() as $tipo){
                    ?>
                    <tr>
                         <td><?php echo $count?></td>
                        <td><?php echo $tipo->getNombre() ?></td>
                        <td class="td-center">
                            <button onclick="delete_tipo_activo(<?php echo $tipo->getId(); ?>)">Eliminar</button>
                            <button onclick="mostrar_box_tipo_activo(<?php echo $tipo->getId(); ?>)">Editar</button>
                        </td>
                    </tr>
                    <?php
                    $count++;
            }
            ?>
        </tbody>
    </table>
    <?php
}

function update_turnos() {
    ?>
    <table  class="table">
        <thead>
            <tr>
                <td>#</td>
                <td>Nombre</td>
                <td>Hora de inicio</td>
                <td>Hora de fin</td>
                <td class="td-center">Acciones</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $count=1;
            $DaoTurnos= new DaoTurnos();
             foreach($DaoTurnos->getTurnos() as $turno){
                    ?>
                    <tr>
                         <td><?php echo $count?></td>
                        <td><?php echo $turno->getNombre() ?></td>
                        <td><?php echo $turno->getHoraIni() ?></td>
                        <td><?php echo $turno->getHoraFin() ?></td>
                        <td class="td-center">
                            <button onclick="delete_turno(<?php echo $turno->getId(); ?>)">Eliminar</button>
                            <button onclick="mostrar_box_turno(<?php echo $turno->getId(); ?>)">Editar</button>
                        </td>
                    </tr>
                    <?php
                    $count++;
            }
            ?>
        </tbody>
    </table>
    <?php
}


if ($_POST['action'] == "delete_metodo_pago") {
	$sql_nueva_entrada = sprintf("UPDATE  Metodo_pago  SET Activo=%s WHERE Id_met=%s", 
                GetSQLValueString(0 , "int"), 
	        GetSQLValueString($_POST['Id_met'] , "int"));
	$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
	update_metodos_pago();
}


if ($_POST['action'] == "delete_medio_contacto") {
    $sql_nueva_entrada = sprintf("UPDATE Metodo_pago SET Activo_medio=%s WHERE Id_medio=%s", 
            GetSQLValueString(0 , "int"), 
            GetSQLValueString($_POST['Id_met'] , "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
	update_medios_contacto();
}
	
if ($_POST['action'] == "delete_descuento") {
    $sql_nueva_entrada = sprintf("UPDATE Paquetes_descuentos SET Activo_des=%s WHERE Id_des=%s", 
            GetSQLValueString(0 , "int"), 
            GetSQLValueString($_POST['Id_des'] , "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
	update_descuentos();
}

if ($_POST['action'] == "delete_motivo_baja") {
    $sql_nueva_entrada = sprintf("UPDATE Motivos_bajas SET Activo_mot=%s WHERE Id_mot=%s", 
            GetSQLValueString(0 , "int"), 
            GetSQLValueString($_POST['Id_mot'] , "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
	update_motivos_bajas();
}


if ($_POST['action'] == "mostrar_box_documento") {
	if ($_POST['Id_file'] > 0) {
	    $query_archivos_ulm = "SELECT * FROM archivos_ulm WHERE Id_file=".$_POST['Id_file'];
	    $archivos_ulm = mysql_query($query_archivos_ulm, $cnn) or die(mysql_error());
	    $row_archivos_ulm = mysql_fetch_array($archivos_ulm);
	}
	?>
	<div id="box_emergente">
	    <h1><img src="images/configu.png" alt="configu" width="40" height="40" />Documentos</h1>
	    <p><input type="text" placeholder="Nombre del documento" id="nombre" value="<?php echo $row_archivos_ulm['Nombre_file'] ?>"/></p>	
	    <button onclick="save_file(<?php echo $_POST['Id_file'] ?>)" class="button">Guardar</button>
	    <span onclick = "ocultar_error_layer()">x</span>
	</div>
	<?php
}


if ($_POST['action'] == "save_file") {
           $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
           $usu = $class_usuarios->get_usu();
	if ($_POST['Id_file'] > 0) {
	    $sql_nueva_entrada = sprintf("UPDATE archivos_ulm SET Nombre_file=%s WHERE Id_file=%s", 
	            GetSQLValueString($_POST['nombre'] , "text"), 
	            GetSQLValueString($_POST['Id_file'] , "int"));
	    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
	} else {
	    $sql_nueva_entrada = sprintf("INSERT INTO archivos_ulm (Nombre_file,Id_plantel) VALUES (%s, %s);", 
	            GetSQLValueString($_POST['nombre'] , "text"),
                       GetSQLValueString($usu['Id_plantel'] , "int"));
	    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
	}
	update_documentos();
}

if ($_POST['action'] == "delete_file") {
  $sql_nueva_entrada = sprintf("DELETE FROM archivos_ulm WHERE Id_file=%s",
          GetSQLValueString($_POST['Id_file'] , "int"));
  $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
  update_documentos();
     
}


if ($_POST['action'] == "mostrar_box_documentos_ofertas") {
        $DaoUsuarios= new DaoUsuarios();
        $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
	$class_ofertas = new class_ofertas($_POST['Id_ofe']);
	$files_ofe = $class_ofertas->get_oferta();
	$class_archivos = new class_archivos();
	?>
	<div id ="box_emergente">
	    <h1><img src="images/configu.png" alt="configu" width="40" height="40" />Selecci&oacute;n de documentos</h1>
	    <p>Selecciona los documentos que deseas para cada oferta</p>
	    <ul id="list_files">
	        <?php
	        $query_archivos_ulm = "SELECT * FROM archivos_ulm LEFT JOIN 
	          (SELECT Id_file as file_ofe FROM archivos_ofertas_ulm WHERE Id_ofe=" . $_POST['Id_ofe'] . ") AS files 
	          ON archivos_ulm.Id_file=files.file_ofe
                  WHERE archivos_ulm.Id_plantel=".$usu->getId_plantel();
	        $archivos_ulm = mysql_query($query_archivos_ulm, $cnn) or die(mysql_error());
	        $row_archivos_ulm = mysql_fetch_array($archivos_ulm);
	        $totalRows_archivos_ulm = mysql_num_rows($archivos_ulm);
	        if ($totalRows_archivos_ulm > 0) {
	            do {
	                ?>
	                <li><input type="checkbox" value="<?php echo $row_archivos_ulm['Id_file'] ?>" 
	                           <?php if ($row_archivos_ulm['file_ofe'] > 0) { ?> checked="ckecked" <?php } ?>/> <?php echo $row_archivos_ulm['Nombre_file'] ?></li>
	                    <?php
	                } while ($row_archivos_ulm = mysql_fetch_array($archivos_ulm));
	            }
	            ?>
	    </ul>	
	    <button onclick="save_documentos_oferta(<?php echo $_POST['Id_ofe'] ?>)" class="button">Guardar</button>
	    <span onclick = "ocultar_error_layer()">x</span>
	</div>
	<?php
}

//Bien no pasa nada
if ($_POST['action'] == "save_documentos_oferta") {
  $sql_nueva_entrada = sprintf("DELETE FROM archivos_ofertas_ulm WHERE Id_ofe=%s", 
          GetSQLValueString($_POST['Id_ofe'] , "int"));
  $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

  foreach ($_POST['files'] as $k => $v) {
      $sql_nueva_entrada = sprintf("INSERT INTO archivos_ofertas_ulm (Id_file, Id_ofe) VALUES (%s, %s);", 
              GetSQLValueString($v['Id_file'], "int"), 
              GetSQLValueString($_POST['Id_ofe'], "int"));
      $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
  }
  update_documentos_ofertas();
}


if ($_POST['action'] == "mostrar_box_ciclos") {
if ($_POST['Id_ciclo'] > 0) {
    $query_ciclos_ulm = "SELECT * FROM ciclos_ulm WHERE Id_ciclo=" . $_POST['Id_ciclo'];
    $ciclos_ulm = mysql_query($query_ciclos_ulm, $cnn) or die(mysql_error());
    $row_ciclos_ulm = mysql_fetch_array($ciclos_ulm);
}
?>
<div class="box-emergente-form ciclosBox">
    <h1>Ciclo</h1>
    <div>
        <p>Nombre:<br> <input type="text" placeholder="Nombre del ciclo" id="nombre" value="<?php echo $row_ciclos_ulm['Nombre_ciclo'] ?>"/></p>	
        <p>Clave:<br><input type="text" placeholder="Clave" id="clave" value="<?php echo $row_ciclos_ulm['Clave'] ?>"/></p>
        <p>Fecha inicio<br><input type="date"  id="Fecha_ini" value="<?php echo $row_ciclos_ulm['Fecha_ini'] ?>"/></p>
        <p>Fecha fin<br><input type="date"  id="Fecha_fin" value="<?php echo $row_ciclos_ulm['Fecha_fin'] ?>"/></p>
        <button onclick="save_ciclo(<?php echo $_POST['Id_ciclo'] ?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cerrar</button>
    </div>
</div>
<?php
}



if ($_POST['action'] == "save_ciclo") {
    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
    $usu = $class_usuarios->get_usu();
    if ($_POST['Id_ciclo'] > 0) {
        $sql_nueva_entrada = sprintf("UPDATE ciclos_ulm SET Nombre_ciclo=%s ,Clave=%s, Fecha_ini=%s, Fecha_fin=%s WHERE Id_ciclo=%s", 
                GetSQLValueString($_POST['nombre'], "text"), 
                GetSQLValueString($_POST['clave'], "text"), 
                GetSQLValueString($_POST['Fecha_ini'], "date"), 
                GetSQLValueString($_POST['Fecha_fin'], "date"), 
                GetSQLValueString($_POST['Id_ciclo'], "int"));
        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
        $Id_ciclo=$_POST['Id_ciclo'];
    } else {
        $sql_nueva_entrada = sprintf("INSERT INTO ciclos_ulm (Nombre_ciclo, Clave, Fecha_ini, Fecha_fin,Activo_ciclo,Id_plantel) VALUES (%s, %s, %s, %s, %s, %s);", 
                GetSQLValueString($_POST['nombre'], "text"), 
                GetSQLValueString($_POST['clave'], "text"), 
                GetSQLValueString($_POST['Fecha_ini'], "date"), 
                GetSQLValueString($_POST['Fecha_fin'], "date"),
                GetSQLValueString(1, "int"),
                GetSQLValueString($usu['Id_plantel'], "int"));
        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
        $Id_ciclo=  mysql_insert_id($cnn);
    }
    //Eliminamos los dias que sean domingo
    $sql_nueva_entrada = sprintf("DELETE FROM Dias_inhabiles_ciclo  WHERE Id_ciclo=%s AND EsFinDeSemana=1;", 
            GetSQLValueString($Id_ciclo, "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    
    //Rango de dias del ciclo
    $fechaInicio=strtotime($_POST['Fecha_ini']);
    $fechaFin=strtotime($_POST['Fecha_fin']);
    for($i=$fechaInicio; $i<=$fechaFin; $i+=86400){
          //Verificamos si el dia es domingo para inhabilitarlo por default
          $dia=date("Y-m-d", $i);
          if(date('w', strtotime($dia))==0){
                $sql_nueva_entrada = sprintf("INSERT INTO Dias_inhabiles_ciclo (fecha_inh, Id_ciclo,EsFinDeSemana) VALUES (%s, %s,%s);", 
                        GetSQLValueString($dia, "date"), 
                        GetSQLValueString($Id_ciclo, "int"),
                        GetSQLValueString(1, "int"));
                        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
          }

      }
    update_ciclos();
}


if($_POST['action']=="add_dia_inhabil"){
    $query_ciclos_ulm = "SELECT * FROM ciclos_ulm  WHERE Id_ciclo=" . $_POST['Id_ciclo'];
    $ciclos_ulm = mysql_query($query_ciclos_ulm, $cnn) or die(mysql_error());
    $row_ciclos_ulm = mysql_fetch_array($ciclos_ulm);
    if($_POST['Fecha_inhabil']>=$row_ciclos_ulm['Fecha_ini'] && $_POST['Fecha_inhabil']<=$row_ciclos_ulm['Fecha_fin']){
        $sql_nueva_entrada = sprintf("INSERT INTO Dias_inhabiles_ciclo (fecha_inh, Id_ciclo) VALUES (%s, %s);", 
                GetSQLValueString($_POST['Fecha_inhabil'], "date"), 
                GetSQLValueString($_POST['Id_ciclo'], "int"));
        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
        update_ciclos();
    }
}
    
if($_POST['action']=="delete_dia_inhabil"){
    $sql_nueva_entrada = sprintf("DELETE FROM Dias_inhabiles_ciclo  WHERE Id_dia=%s;", 
            GetSQLValueString($_POST['Id_dia'], "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    update_ciclos();
}

if($_POST['action']=="delete_ciclo"){
    $sql_nueva_entrada = sprintf("UPDATE ciclos_ulm SET Activo_ciclo=%s WHERE Id_ciclo=%s", 
            GetSQLValueString(0, "int"),
            GetSQLValueString($_POST['Id_ciclo'], "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    update_ciclos();
}





if($_POST['action']=="delete_aula"){
    $sql_nueva_entrada = sprintf("UPDATE aulas SET Activo_aula=%s WHERE Id_aula=%s", 
            GetSQLValueString(0, "int"),
            GetSQLValueString($_POST['Id_aula'], "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    update_aulas();
}

if($_POST['action']=="delete_mis"){
    $DaoMiscelaneos= new DaoMiscelaneos();
    $mis=$DaoMiscelaneos->show($_POST['Id_mis']);
    $mis->setActivo_mis(0);
    $DaoMiscelaneos->update($mis);
    update_miscelaneos();
}

if ($_POST['action'] == "mostrar_box_mis") {
	if ($_POST['Id_mis'] > 0) {
            $class_miscelaneos= new class_miscelaneos();
            $mis=$class_miscelaneos->get_mis($_POST['Id_mis']);
	}
?>
<div id="box_emergente" class="miscelaneos">
    <h1><img src="images/configu.png" alt="configu" width="40" height="40" />Miscelaneo</h1>
    <div>
        <p>Concepto:<br><input type="text" placeholder="Concepto" id="concepto" value="<?php echo $mis['Nombre_mis']?>"/></p>
        <p>Costo:<br> <input type="text" placeholder="Costo" id="costo" value="<?php echo $mis['Costo_mis'] ?>"/></p>	
        <button onclick="save_miscelaneo(<?php echo $_POST['Id_mis'] ?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cancelar</button>
    </div>
</div>
<?php
}

if ($_POST['action'] == "save_miscelaneo") {
    $DaoMiscelaneos= new DaoMiscelaneos();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    if ($_POST['Id_mis'] > 0) {
        $mis=$DaoMiscelaneos->show($_POST['Id_mis']);
        $mis->setNombre_mis($_POST['concepto']);
        $mis->setCosto_mis($_POST['costo']);
        $DaoMiscelaneos->update($mis);
    } else {
        $Miscelaneos= new Miscelaneos();
        $Miscelaneos->setNombre_mis($_POST['concepto']);
        $Miscelaneos->setCosto_mis($_POST['costo']);
        $Miscelaneos->setActivo_mis(1);
        $Miscelaneos->setId_plantel($usu->getId_plantel());
        $DaoMiscelaneos->add($Miscelaneos);
    }
    update_miscelaneos();
}


if ($_POST['action'] == "save_descuento") {
    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
    $usu = $class_usuarios->get_usu();
    if ($_POST['Id_des'] > 0) {
        $sql_nueva_entrada = sprintf("UPDATE Paquetes_descuentos SET Nombre=%s ,Porcentaje=%s,Fecha_inicio=%s,Fecha_fin=%s WHERE Id_des=%s", 
                GetSQLValueString($_POST['nombre'], "text"), 
                GetSQLValueString($_POST['porcentaje'], "double"), 
                GetSQLValueString($_POST['Fecha_inicio'], "date"),
                GetSQLValueString($_POST['Fecha_fin'], "date"),
                GetSQLValueString($_POST['Id_des'], "int"));
        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    } else {
        $sql_nueva_entrada = sprintf("INSERT INTO Paquetes_descuentos (Nombre, Porcentaje, Fecha_inicio, Fecha_fin, Activo_des,Id_plantel) VALUES (%s, %s, %s,%s,%s,%s);", 
                GetSQLValueString($_POST['nombre'], "text"), 
                GetSQLValueString($_POST['porcentaje'], "double"), 
                GetSQLValueString($_POST['Fecha_inicio'], "date"),
                GetSQLValueString($_POST['Fecha_fin'], "date"),
                GetSQLValueString(1, "int"),
                 GetSQLValueString($usu['Id_plantel'], "int"));
        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    }
    update_descuentos();
}


if ($_POST['action'] == "save_motivo_baja") {
    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
    $usu = $class_usuarios->get_usu();
    if ($_POST['Id_mot'] > 0) {
        $sql_nueva_entrada = sprintf("UPDATE Motivos_bajas SET Nombre=%s WHERE Id_mot=%s", 
                GetSQLValueString($_POST['nombre'], "text"), 
                GetSQLValueString($_POST['Id_mot'], "int"));
        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    } else {
        $sql_nueva_entrada = sprintf("INSERT INTO Motivos_bajas (Nombre, Activo_mot, Id_plantel) VALUES (%s, %s, %s);", 
                GetSQLValueString($_POST['nombre'], "text"), 
                GetSQLValueString(1, "int"), 
                GetSQLValueString($usu['Id_plantel'], "int"));
        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    }
    update_motivos_bajas();
}





if ($_POST['action'] == "mostrar_box_aula") {
    if ($_POST['Id_aula'] > 0) {
        $query_aulas = "SELECT * FROM aulas WHERE Id_aula=" . $_POST['Id_aula'];
        $aulas = mysql_query($query_aulas, $cnn) or die(mysql_error());
        $row_aulas = mysql_fetch_array($aulas);
    }
?>
<div class="box-emergente-form box-aulas">
    <h1>Aula</h1>
    <div>
        <p>Clave:<br><input type="text" placeholder="Clave" id="clave" value="<?php echo $row_aulas['Clave_aula'] ?>"/></p>
        <p>Nombre:<br> <input type="text" placeholder="Nombre del aula" id="nombre" value="<?php echo $row_aulas['Nombre_aula'] ?>"/></p>	
        <p>Capacidad maxima:<br> <input type="text" placeholder="Capacidad maxima" id="capacidad" value="<?php echo $row_aulas['Capacidad_aula'] ?>"/></p>	
        <p>Tipo de aula:<br> <select id="tipo_aula">
	        <option value="0"></option>
	        <option value="1" <?php if($row_aulas['Tipo_aula']==1){?> selected="selected" <?php } ?>>Laboratorio</option>
	        <option value="2" <?php if($row_aulas['Tipo_aula']==2){?> selected="selected" <?php } ?>>Instrucci&oacute;n</option>
	        <option value="3" <?php if($row_aulas['Tipo_aula']==3){?> selected="selected" <?php } ?>>Especial</option>
	        <option value="4" <?php if($row_aulas['Tipo_aula']==4){?> selected="selected" <?php } ?>>Te&oacute;rica</option>
        </select></p>	
        <button onclick="save_aula(<?php echo $_POST['Id_aula'] ?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cerrar</button>
    </div>
</div>
<?php
}




if ($_POST['action'] == "save_aula") {
    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
    $usu = $class_usuarios->get_usu();
    if ($_POST['Id_aula'] > 0) {
        $sql_nueva_entrada = sprintf("UPDATE aulas SET Nombre_aula=%s ,Clave_aula=%s, Tipo_aula=%s, Capacidad_aula=%s WHERE Id_aula=%s", 
                GetSQLValueString($_POST['nombre'], "text"), 
                GetSQLValueString($_POST['clave'], "text"), 
                GetSQLValueString($_POST['tipo_aula'], "int"), 
                GetSQLValueString($_POST['capacidad'], "int"), 
                GetSQLValueString($_POST['Id_aula'], "int"));
        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    } else {
        $sql_nueva_entrada = sprintf("INSERT INTO aulas (Nombre_aula, Clave_aula, Tipo_aula, Capacidad_aula,Id_plantel) VALUES (%s, %s, %s, %s, %s);", 
                GetSQLValueString($_POST['nombre'], "text"), 
                GetSQLValueString($_POST['clave'], "text"), 
                GetSQLValueString($_POST['tipo_aula'], "int"), 
                GetSQLValueString($_POST['capacidad'], "int"),
                GetSQLValueString($usu['Id_plantel'], "int"));
        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    }
    update_aulas();
}



if ($_POST['action'] == "mostrar_box_grupo") {
    $DaoTurnos= new DaoTurnos();
    $DaoGrupos= new DaoGrupos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoOfertas= new DaoOfertas();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoMaterias= new DaoMaterias();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    
    $Id_grupo=0;
    $Id_ofe=0;
    $Id_esp=0;
    $Id_mat=0;
    $Id_ori=0;
    $Capacidad="";
    $Turno=0;
    $Clave="";
    
    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    if (isset($_POST['Id_grupo']) &&  $_POST['Id_grupo']> 0) {
        $grupo= $DaoGrupos->show($_POST['Id_grupo']);
        $Id_grupo=$grupo->getId();
        $mat_esp=$DaoMateriasEspecialidad->show($grupo->getId_mat_esp());
        $esp=$DaoEspecialidades->show($mat_esp->getId_esp());
        $Id_ofe=$esp->getId_ofe();
        $Id_esp=$esp->getId();
        $Id_mat=$mat_esp->getId_mat();
        $Id_ori= $grupo->getId_ori();
        $Turno=$grupo->getTurno();
        $Capacidad=$grupo->getCapacidad();
        $Clave=$grupo->getClave();
    }
?>
<div class="box-emergente-form grupos">
    <h1>Grupo</h1>
    <div>
        <p>Clave grupal:<br><input type="text" placeholder="Clave" id="clave_grupal" value="<?php echo $Clave ?>"/></p>
        <p>Oferta<br>                                   
          <select id="ofertas" onchange="update_curso(this)">
               <option value="0">Selecciona una oferta</option>
                <?php
                    foreach ($DaoOfertas->showAll() as $oferta) {
                        ?>
                        <option value="<?php echo $oferta->getId()?>" <?php if($oferta->getId()==$Id_ofe){?> selected="selected" <?php } ?>><?php echo $oferta->getNombre_oferta() ?></option>
                        <?php
                    }
                ?>
              </select>
        </p>
        <p>Especialidad<br>                                   
          <select id="especialidades" onchange="update_materias(this)">
              <option value="0">Selecciona una especialidad</option>
              <?php
              if($Id_esp>0){
                  foreach ($DaoEspecialidades->getEspecialidadesOferta($Id_ofe) as $esp) {
                  ?>
              <option value="<?php echo $esp->getId() ?>" <?php if($esp->getId()==$Id_esp){?> selected="selected" <?php } ?>> <?php echo $esp->getNombre_esp() ?> </option>
                  <?php
                  }
              }
              ?>
          </select>
        </p>
        <p>Materia:<br> <select id="lista_materias" onchange="verificar_orientaciones()">
	        <option value="0">Selecciona una materia</option>
                <?php
                if($Id_mat>0){
                      foreach ($DaoMateriasEspecialidad->getMateriasEspecialidad($Id_esp) as $matEsp) {
                          $mat=$DaoMaterias->show($matEsp->getId_mat());
                          $nombre=$mat->getNombre();
                          if(strlen($matEsp->getNombreDiferente())>0){
                             $nombre=$matEsp->getNombreDiferente();
                          }
                      ?>
                <option value="<?php echo $matEsp->getId_mat() ?>" id_mat_esp="<?php echo $matEsp->getId() ?>" <?php if($matEsp->getId_mat()==$Id_mat){?> selected="selected" <?php } ?>> <?php echo $nombre ?> </option>
                      <?php
                      }
                }
                ?>     
        </select></p>
        <div id="box_orientaciones">
            <?php
            if($Id_ofe>0){
              if(count($DaoOrientaciones->getOrientacionesEspecialidad($Id_esp))>0){
              ?>
              Orientaci&oacute;n<br>
              <select id="orientaciones">
                <option value="0"></option>
              <?php
              foreach ($DaoOrientaciones->getOrientacionesEspecialidad($Id_esp) as $ori) {
              ?>
                <option value="<?php echo $ori->getId() ?>" <?php if($Id_ori==$ori->getId() ){?> selected="selected" <?php } ?>> <?php echo $ori->getNombre() ?> </option>
              <?php
              }
              ?>
              </select>
              <?php
              }  
            }

            ?>
        </div>
        <p>Capacidad:<br> <input type="text" placeholder="Capacidad maxima" id="capacidad" value="<?php echo $Capacidad ?>"/></p>
        <p>Turno:<br>
            <select id="turno">
                <option value="0"></option>
                  <?php
                  foreach($DaoTurnos->getTurnos() as $turno){
                      ?>
                      <option value="<?php echo $turno->getId()?>" <?php if ($Turno == $turno->getId()) { ?> selected="selected" <?php } ?>><?php echo $turno->getNombre()?></option>
                  <?php
                  }
                  ?>
         </select></p>	
        <button onclick="save_grupo(<?php echo $Id_grupo ?>,this)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cancelar</button>
    </div>
</div>
<?php
}

if ($_POST['action'] == "update_curso") {
  ?>
  <option value="0"></option>
  <?php
    $DaoEspecialidades= new DaoEspecialidades();
    foreach ($DaoEspecialidades->getEspecialidadesOferta($_POST['Id_oferta']) as $esp) {
    ?>
    <option value="<?php echo $esp->getId() ?>"> <?php echo $esp->getNombre_esp() ?> </option>
    <?php
    }
}

if ($_POST['action'] == "update_materias") {
  ?>
  <option value="0"></option>
  <?php
  $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
  $DaoMaterias= new DaoMaterias();
  foreach ($DaoMateriasEspecialidad->getMateriasEspecialidad($_POST['Id_esp']) as $matEsp) {
        $mat=$DaoMaterias->show($matEsp->getId_mat());
        $nombre=$mat->getNombre();
        if(strlen($matEsp->getNombreDiferente())>0){
           $nombre=$matEsp->getNombreDiferente();
        }
    ?>
    <option value="<?php echo $matEsp->getId_mat() ?>" id_mat_esp="<?php echo $matEsp->getId() ?>" <?php if($matEsp->getId_mat()==$Id_mat){?> selected="selected" <?php } ?>> <?php echo $nombre ?> </option>
    <?php
    }
}

if($_POST['action']=="save_grupo"){
    
    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
    $usu = $class_usuarios->get_usu();
    
        if ($_POST['Id_grupo'] > 0) {
            $sql_nueva_entrada = sprintf("UPDATE Grupos SET Clave=%s ,Turno=%s,  Id_ciclo=%s, Id_mat=%s,Capacidad=%s ,Id_mat_esp=%s, Id_ori=%s WHERE Id_grupo=%s", 
                    GetSQLValueString($_POST['clave_grupal'], "text"), 
                    GetSQLValueString($_POST['turno'], "int"), 
                    GetSQLValueString($_POST['Id_ciclo'], "int"), 
                    GetSQLValueString($_POST['materia'], "int"),
                    GetSQLValueString($_POST['capacidad'], "int"),
                    GetSQLValueString($_POST['Id_mat_esp'], "int"),
                    GetSQLValueString($_POST['Id_ori'], "int"),
                    GetSQLValueString($_POST['Id_grupo'], "int"));
            $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
            update_grupos_ciclo($_POST['Id_ciclo']);
        } else {
                $query_Grupos = "SELECT * FROM Grupos WHERE Clave='" . $_POST['clave_grupal']."' AND Id_ciclo=".$_POST['Id_ciclo']." AND Activo_grupo=1";
                $Grupos = mysql_query($query_Grupos, $cnn) or die(mysql_error());
                $row_Grupos = mysql_fetch_array($Grupos);
                $totalRows_Grupos  = mysql_num_rows($Grupos);    
                if($totalRows_Grupos==0){
                        $sql_nueva_entrada = sprintf("INSERT INTO Grupos (Clave, Turno, Activo_grupo, Id_ciclo,Id_mat,Capacidad, Id_plantel,Id_mat_esp, Id_ori) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s);", 
                                GetSQLValueString($_POST['clave_grupal'], "text"), 
                                GetSQLValueString($_POST['turno'], "int"), 
                                GetSQLValueString(1, "int"), 
                                GetSQLValueString($_POST['Id_ciclo'], "int"),
                                GetSQLValueString($_POST['materia'], "int"),
                                GetSQLValueString($_POST['capacidad'], "int"),
                                GetSQLValueString($usu['Id_plantel'], "int"),
                                GetSQLValueString($_POST['Id_mat_esp'], "int"),
                                GetSQLValueString($_POST['Id_ori'], "int"));
                        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
                        update_grupos_ciclo($_POST['Id_ciclo']);
                 }
        }
        
   
}


if($_POST['action']=="delete_grupo"){
    $query_Grupos = "SELECT * FROM Horario_docente WHERE Id_grupo=" . $_POST['Id_grupo'];
    $Grupos = mysql_query($query_Grupos, $cnn) or die(mysql_error());
    $row_Grupos = mysql_fetch_array($Grupos);
    $totalRows_Grupos  = mysql_num_rows($Grupos);    
    if($totalRows_Grupos==0){
        //Si existen alumno activos asignados al grupo, no se podra eliminar
        $query_Grupos = "SELECT ofertas_alumno.*,Id_grupo FROM materias_ciclo_ulm
        JOIN ciclos_alum_ulm ON materias_ciclo_ulm.Id_ciclo_alum=ciclos_alum_ulm.Id_ciclo_alum
        JOIN ofertas_alumno ON ciclos_alum_ulm.Id_ofe_alum=ofertas_alumno.Id_ofe_alum
        WHERE Id_grupo=".$_POST['Id_grupo']." AND Activo_oferta=1";
        $Grupos = mysql_query($query_Grupos, $cnn) or die(mysql_error());
        $row_Grupos = mysql_fetch_array($Grupos);
        $totalRows_Grupos  = mysql_num_rows($Grupos); 
        if($totalRows_Grupos==0){
            $sql_nueva_entrada = sprintf("UPDATE Grupos SET Activo_grupo=%s WHERE Id_grupo=%s", 
                    GetSQLValueString(0, "int"),
                    GetSQLValueString($_POST['Id_grupo'], "int"));
            $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
            update_grupos_ciclo($_POST['Id_ciclo']);
        }else{
           echo "Actualmente el grupo se encuentra con alumnos asignados\nEliminelos para borrarlo.";
        }
    
    }else{
        $class_docentes= new class_docentes($row_Grupos['Id_docente']);
        $doc=$class_docentes->get_docente();
        echo "Grupo asignado a: \n\n-".mb_strtoupper($doc['Nombre_docen']." ".$doc['ApellidoP_docen']." ".$doc['ApellidoM_docen'])."\n\nElimine el grupo del horario del docente para borrarlo.";
    }

}

if($_POST['action']=="update_list_grupos_ciclo"){
    update_grupos_ciclo($_POST['Id_ciclo']);
}


if($_POST['action']=="validar_existencia_docente"){
    $resp=array();
    $resp['ExisteDocente']=0;
    $DaoHorarioDocente= new DaoHorarioDocente();
    $DaoGrupos= new DaoGrupos();
    $grupo= $DaoGrupos->show($_POST['Id_grupo']);
    $horario=$DaoHorarioDocente->getHorarioByGrupoCiclo($_POST['Id_grupo'],$_POST['Id_ciclo']);
    if($horario->getId_docente()>0){
       $resp['ExisteDocente']=1; 
    }
    $resp['Grupo']=strtoupper($grupo->getClave()); 
    echo json_encode($resp);
}



if($_POST['action']=="mostrar_box_busqueda"){
        $base= new base();
        $DaoGrupos= new DaoGrupos();
        $DaoPagosCiclo= new DaoPagosCiclo();
        $DaoCiclosAlumno= new DaoCiclosAlumno();
        $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
        $DaoUsuarios= new DaoUsuarios();
        $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        $grupo= $DaoGrupos->show($_POST['Id_grupo']);
        
        $Id_ori_grupo="";
        $query_orientacion="";
        if($grupo->getId_ori()>0){
           $Id_ori_grupo=$grupo->getId_ori();
           //$query_orientacion=" AND materias_ciclo_ulm.Id_ori=".$grupo->getId_ori();
        }
  
?>
<div class="box-emergente-form add-alumnos-grupo">
	<h1>Añadir alumno</h1>
        <div class="box-table">
            <?php
            $cantAlumnos=count($DaoGrupos->getAlumnosBYGrupo($_POST['Id_grupo']));
            if($cantAlumnos<$grupo->getCapacidad()){
            ?>
            <table class="table" id="table-alumnos">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Código</td>
                        <td>Nombre</td>
                        <td>Orientación</td>
                        <td><input type="checkbox" id="all_alumns" onchange="marcarAlumnos()"></td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $coun=1;
                    $query = "
                    SELECT 
                        ofertas_alumno.Id_ofe_alum,ofertas_alumno.Id_alum,ofertas_alumno.Opcion_pago,
                        Materias_especialidades.Id_mat_esp,Materias_especialidades.Id_mat,
                        materias_ciclo_ulm.Id_ciclo_mat,materias_ciclo_ulm.Id_grupo,materias_ciclo_ulm.Tipo,materias_ciclo_ulm.Id_ori AS Id_ori_mat_ext,
                        ciclos_alum_ulm.Id_ciclo,ciclos_alum_ulm.Id_ciclo_alum,
                        especialidades_ulm.Num_pagos,
                        inscripciones_ulm.Matricula,inscripciones_ulm.Nombre_ins,inscripciones_ulm.ApellidoP_ins,inscripciones_ulm.ApellidoM_ins,
                        orientaciones_ulm.Nombre_ori,orientaciones_ulm.Id_ori
                    FROM  ofertas_alumno 
                    JOIN especialidades_ulm ON ofertas_alumno.Id_esp=especialidades_ulm.Id_esp
                    JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
                    JOIN (SELECT materias_ciclo_ulm.* 
                                  From materias_ciclo_ulm 
                                  JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                           WHERE materias_ciclo_ulm.Id_grupo IS NULL 
                                 AND Materias_especialidades.Id_mat=".$grupo->getId_mat()." ".$query_orientacion."
                         ) AS materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum 
                    JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                    JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
                    LEFT JOIN orientaciones_ulm ON ofertas_alumno.Id_ori=orientaciones_ulm.Id_ori
                    WHERE ofertas_alumno.Activo_oferta=1 
                          AND (ciclos_alum_ulm.Id_ciclo=".$_POST['Id_ciclo']."  OR ciclos_alum_ulm.Id_ciclo='0')
                          AND ofertas_alumno.Turno=".$grupo->getTurno();
                     foreach($base->advanced_query($query) as $k=>$row){
                           //Si el alumno tiene la materia
                           //Si el alumno ya pago la inscripcion
                           //Si el alumno no esta inscripto en un grupo
                          $Id_ori_alum=$row['Id_ori'];
                          if($row['Tipo']==2){
                             $Id_ori_alum=$row['Id_ori_mat_ext']; 
                          }
                          if($Id_ori_grupo==null){
                            $Id_ori_alum=null;
                          }
                          //63-66
                          if((count($DaoPagosCiclo->getCargosCiclo($row['Id_ciclo_alum']))>1 || ($row['Num_pagos']==1 && count($DaoPagosCiclo->getCargosCiclo($row['Id_ciclo_alum']))>=1)) && $Id_ori_grupo==$Id_ori_alum){
                           ?>
                            <tr id_alum="<?php echo $row['Id_ins']; ?>" id_ciclo_mat="<?php echo $row['Id_ciclo_mat']; ?>">
                                <td><?php echo $coun?></td>
                                <td><?php echo $row['Matricula']?></td>
                                <td><?php echo $row['Nombre_ins']." " . $row['ApellidoP_ins'] . " " . $row['ApellidoM_ins'] ?></td>
                                <td><?php echo $row['Nombre_ori'] ?></td>
                                <td><input type="checkbox"></td>
                            </tr>
                           <?php
                   
                          }
                          $coun++;
                    }
                    ?>
                </tbody>
            </table>
            <?php
            }else{
                ?>
            <p>No se permiten más alumnos, el grupo esta completo<br><br>Capacidad máxima: <?php echo $grupo->getCapacidad() ?> alumnos</p>
                <?php
            }
            ?>
        </div>
        <p> 
            <?php
            if($cantAlumnos<$grupo->getCapacidad()){
             ?>
            <button class="button" onclick="add_alum_grupo(<?php echo $_POST['Id_grupo']?>,this)">Guardar</button>
            <?php
            }
            ?>
            <button class="button" onclick="ocultar_error_layer()">Cancelar</button>
        </p>
</div>
<?php	

}


/*
if($_POST['action']=="buscarAlum"){   
        $base= new base();
        $DaoGrupos= new DaoGrupos();
        $DaoPagosCiclo= new DaoPagosCiclo();
        $DaoUsuarios= new DaoUsuarios();
        $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        $grupo= $DaoGrupos->show($_POST['Id_grupo']);
        
        //Este buscador es avanzado ya que concatena cualquier campo que este dentro de la funcion concat
        //Si el campo viene vacio quita el espacio
        //Si el campo contiene un espacio en cualquier parte lo suprime
        //Ejemplo: christianalejandrosantosgarcia207388896
        //Tambien tenemos que suprimir los espacios en blanco del campo a buscar en php
	$query = "SELECT *
	FROM inscripciones_ulm 
	WHERE
	(Nombre_ins LIKE '%".str_replace(' ','',$_POST['buscar'])."%' 
        OR ApellidoP_ins LIKE '%".str_replace(' ','',$_POST['buscar'])."%' 
        OR ApellidoM_ins LIKE '%".str_replace(' ','',$_POST['buscar'])."%')  
        AND tipo=1 AND Activo_alum=1 AND Id_plantel=".$usu->getId_plantel()." ORDER BY Id_ins DESC";	
        $consulta=$base->advanced_query($query);
        $row_consulta = $consulta->fetch_assoc();
        $totalRows_consulta= $consulta->num_rows;
        if($totalRows_consulta>0){
           do{
                $query_materias_oferta = "SELECT 
                    ofertas_alumno.Id_ofe_alum,ofertas_alumno.Id_alum,ofertas_alumno.Opcion_pago,
                    Materias_especialidades.Id_mat_esp,Materias_especialidades.Id_mat,
                    materias_ciclo_ulm.Id_ciclo_mat,materias_ciclo_ulm.Id_grupo,
                    ciclos_alum_ulm.Id_ciclo,ciclos_alum_ulm.Id_ciclo_alum,
                    especialidades_ulm.Num_pagos
                FROM  ofertas_alumno 
                JOIN especialidades_ulm ON ofertas_alumno.Id_esp=especialidades_ulm.Id_esp
                JOIN  ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
                JOIN  materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum 
                JOIN  Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                WHERE Activo_oferta=1 AND ofertas_alumno.Id_alum=".$row_consulta['Id_ins']." AND ciclos_alum_ulm.Id_ciclo=".$_POST['Id_ciclo'];	
                $materias_oferta=$base->advanced_query($query_materias_oferta);
                $row_materias = $materias_oferta->fetch_assoc();
                $totalRows_materias= $materias_oferta->num_rows;
                if($totalRows_materias>0){
                   do{
                       //Si el alumno tiene la materia
                       //Si el alumno ya pago la inscripcion
                       //Si el alumno no esta inscripto en un grupo
                      if($row_materias['Id_mat']==$grupo->getId_mat() 
                         && (count($DaoPagosCiclo->getCargosCiclo($row_materias['Id_ciclo_alum']))>1 || ($row_materias['Num_pagos']==1 && count($DaoPagosCiclo->getCargosCiclo($row_materias['Id_ciclo_alum']))>=1))
                         && $row_materias['Id_grupo']==NULL){
                       ?>
                        <li id_alum="<?php echo $row_consulta['Id_ins']; ?>" id_ciclo_mat="<?php echo $row_materias['Id_ciclo_mat']; ?>"><?php echo $row_consulta['Matricula']."-"." ".$row_consulta['Nombre_ins']." " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins'] ?></li>
                       <?php
                      }
                   }while($row_materias = $materias_oferta->fetch_assoc());  
                }
           }while($row_consulta = $consulta->fetch_assoc());  
        }  
}
*/




if($_POST['action']=="add_alum_grupo"){
    $respuesta=array();
    $respuesta['Chocan']=array();
    $DaoAlumnos=new DaoAlumnos();
    $DaoOfertasAlumno=new DaoOfertasAlumno();
    $DaoCiclosAlumno= new DaoCiclosAlumno();
    $DaoGrupos= new DaoGrupos();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    
    foreach($_POST['alumnos'] as $k=>$v){
        $materiasCicloAlumno=$DaoMateriasCicloAlumno->show($v['Id_ciclo_mat']);
        $Id_ciclo_alumno=$materiasCicloAlumno->getId_ciclo_alum();
        $Id_alum=$materiasCicloAlumno->getId_alum();
        $cicloAlumno=$DaoCiclosAlumno->show($Id_ciclo_alumno);
        //Validamos si las horas del grupo a registrar, no se cruzan con las horas de otro grupo del alumno
        $resp=$DaoAlumnos->getValidarHoraDiaLibres($cicloAlumno->getId_ofe_alum(), $cicloAlumno->getId_ciclo(), $_POST['Id_grupo']);
        if($resp==0){
            $materiaCiclo=$DaoMateriasCicloAlumno->show($v['Id_ciclo_mat']);
            $materiaCiclo->setId_grupo($_POST['Id_grupo']);
            $DaoMateriasCicloAlumno->update($materiaCiclo);
        }else{
            $alumno=array();
            $alumno['Id_alum']=$Id_alum;
            $alumno['Id_grupo']=$_POST['Id_grupo'];
            $alumno['Id_horario']=$resp;
            array_push($respuesta['Chocan'], $alumno);
        }
    }
    echo json_encode($respuesta);
}


if($_POST['action']=="mostrar_errores_inscripcion"){
    $DaoAlumnos=new DaoAlumnos();
    $DaoGrupos= new DaoGrupos();
    $DaoHorarioDocente= new DaoHorarioDocente();
    $DaoHoras= new DaoHoras();
 ?>
  <div id="box_emergente" class="conflicto">
	<h1><i class="fa fa-exclamation-triangle"></i> Conflicto</h1>
        <p>Los siguientes alumnos no pueden ser añadidos por que existe un conflicto entre horas con otro grupo</p>
        <table class="table">
            <thead>
                 <td>#</td>
                 <td>Matricula</td>
                 <td>Nombre</td>
                 <td class="td-center">Grupo</td>
                 <td class="td-center">Día</td>
                 <td class="td-center">Hora</td>
            </thead>
            <tbody>
                <?php
                $count=1;
                foreach($_POST['Lista'] as $lista){
                    $alumno=$DaoAlumnos->show($lista['Id_alum']);
                    $horario=$DaoHorarioDocente->show($lista['Id_horario']);
                    $grupo=$DaoGrupos->show($horario->getId_grupo());
                    $hora=$DaoHoras->show($horario->getHora());
                    $Dia="";
                    if($horario->getLunes()==1){
                        $Dia="Lunes";
                     }elseif($horario->getMartes()==1){
                        $Dia="Martes";
                     }elseif($horario->getMiercoles()==1){
                        $Dia="Miercoles";
                     }elseif($horario->getJueves()==1){
                        $Dia="Jueves";
                     }elseif($horario->getViernes()==1){
                        $Dia="Viernes";
                     }elseif($horario->getSabado()==1){
                        $Dia="Sabado";
                     }
                ?>
                <tr>
                    <td><?php echo $count?></td>
                    <td><?php echo $alumno->getMatricula()?></td>
                    <td><?php echo $alumno->getNombre()." ".$alumno->getApellidoP()." ".$alumno->getApellidoM()?></td>
                    <td class="td-center"><?php echo $grupo->getClave()?></td>
                    <td class="td-center"><?php echo $Dia?></td>
                    <td class="td-center"><?php echo $hora->getTexto_hora()?></td>
                </tr>
                <?php
                   $count++;
                }
                ?>
            </tbody>
        </table>
        <button class="button" onclick="ocultar_error_layer()">Cerrar</button>
   </div>
 <?php
}




if($_POST['action']=="mostrar_alum"){
    $DaoAlumnos= new DaoAlumnos();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoGrupos= new DaoGrupos();
    $grupo=$DaoGrupos->show($_POST['Id_grupo']);
?>
  <div class="box-emergente-form add-alumnos-grupo">
      <h1>Grupo <span class="clave_grupo"><?php echo strtoupper($grupo->getClave())?></span></h1>
        <div id="mascara_alumnos_grupo">
        <table id="alumos_del_grupo" class="table">
            <thead>
                <td>#</td>
                <td>Nombre</td>
                <td>Orientación</td>
                <td class="td-center">Acciones</td>
            </thead>
            <tbody>
                <?php
                $count=1;
                foreach($DaoGrupos->getAlumnosBYGrupo($_POST['Id_grupo']) as $k=>$v){
                    $nombreOri="";
                    $alumno=$DaoAlumnos->show($v['Id_alum']);
                    if($v['Id_ori']>0){
                        $ori=$DaoOrientaciones->show($v['Id_ori']);
                        $nombreOri=$ori->getNombre();
                    }
                ?>
                <tr>
                    <td><?php echo $count;?></td>
                    <td><?php echo $alumno->getNombre()." ".$alumno->getApellidoP()." ".$alumno->getApellidoM()?></td>
                    <td><?php echo $nombreOri;?></td>
                    <td class="td-center"><i class="fa fa-trash" onclick="delete_alum_grupo(<?php echo $v['Id_ciclo_mat']?>)"></i></td>
                </tr>
                <?php
                $count++;
                }
                ?>
            </tbody>
        </table>
        </div>
        <p><button class="button" onclick="ocultar_error_layer()">Cerrar</button></p>
        <input type="hidden" id="Id_grupo" value="<?php echo $_POST['Id_grupo'];?>"/>
</div>
<?php	

}


if($_POST['action']=="delete_alum_grupo"){    
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoGrupos= new DaoGrupos();
    $DaoAlumnos= new DaoAlumnos();
    $DaoHorarioDocente= new DaoHorarioDocente();
    $DaoNotificacionesUsuario= new DaoNotificacionesUsuario();
    
    $materiaCiclo=$DaoMateriasCicloAlumno->show($_POST['Id_ciclo_mat']);
    $materiaCiclo->setId_grupo(NULL);
    $DaoMateriasCicloAlumno->update($materiaCiclo);

    $alum=$DaoAlumnos->show($materiaCiclo->getId_alum());
    $grupo=$DaoGrupos->show($_POST['Id_grupo']);

    $query="SELECT * FROM Horario_docente WHERE Id_grupo= ".$_POST['Id_grupo'];
    $resp=$DaoHorarioDocente->getHorarioQuery($query);
    if($resp->getId()>0){
        //Crear Notificacion para el docente
        $NotificacionesUsuario= new NotificacionesUsuario();
        $textoNot=$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM()." ha sido dado de baja del grupo #".$grupo->getClave();
        $NotificacionesUsuario->setTitulo("Alumno dado de baja");
        $NotificacionesUsuario->setTexto($textoNot);
        $NotificacionesUsuario->setDateCreated(date('Y-m-d H:i:s'));
        $NotificacionesUsuario->setIdRel($resp->getId_docente());
        $NotificacionesUsuario->setTipoRel('docente');
        $DaoNotificacionesUsuario->add($NotificacionesUsuario);
    
    }
    
    
    $count=1;
    foreach($DaoGrupos->getAlumnosBYGrupo($_POST['Id_grupo']) as $k=>$v){
        $alumno=$DaoAlumnos->show($v['Id_alum']);
    ?>
    <tr>
        <td><?php echo $count;?></td>
        <td><?php echo $alumno->getNombre()." ".$alumno->getApellidoP()." ".$alumno->getApellidoM()?></td>
        <td class="td-center"><i class="fa fa-trash" onclick="delete_alum_grupo(<?php echo $v['Id_ciclo_mat']?>)"></i></td>
    </tr>
    <?php
    $count++;
    }
}



if ($_POST['action'] == "mostrar_box_descuento") {
	if ($_POST['Id_des'] > 0) {
            $class_descuentos= new class_descuentos();
            $des=$class_descuentos->get_descuento($_POST['Id_des']);
	}
?>
<div id="box_emergente" class="miscelaneos">
    <h1><img src="images/configu.png" alt="configu" width="40" height="40" />Descuentos</h1>
    <div>
        <p>Nombre:<br><input type="text" placeholder="Concepto" id="nombre" value="<?php echo $des['Nombre']?>"/></p>
        <p>Porcentaje:<br> <input type="text" placeholder="Porcentaje" id="porcentaje" value="<?php echo $des['Porcentaje'] ?>"/></p>	
        
        <p>Fecha inicio:<br> <input type="date"  id="fecha_inicio" value="<?php echo $des['Fecha_inicio'] ?>"/></p>
        <p>Fecha fin:<br> <input type="date"  id="fecha_fin" value="<?php echo $des['Fecha_fin'] ?>"/></p>
        <button onclick="save_descuento(<?php echo $_POST['Id_des'] ?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cancelar</button>
    </div>
</div>
<?php
}


if ($_POST['action'] == "buscarGrupos") {
    $count=1;
    $DaoMaterias= new DaoMaterias();
    $DaoGrupos= new DaoGrupos();
    $DaoTurnos= new DaoTurnos();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoHorarioDocente= new DaoHorarioDocente();
    foreach($DaoGrupos->buscarGrupoCiclo($_POST['buscar'],$_POST['Id_ciclo']) as $k=>$v){
            $mat = $DaoMaterias->show($v->getId_mat());
            $tur = $DaoTurnos->show($v->getTurno());
            $turno=$tur->getNombre();
                                        
            $nombre_ori="";
            if($v->getId_ori()>0){
              $ori = $DaoOrientaciones->show($v->getId_ori());
              $nombre_ori=$ori->getNombre();
            }
            $disponibilidad=$DaoGrupos->getDisponibilidadGrupo($v->getId());
            $red="pink";
            $horario=$DaoHorarioDocente->getHorarioByGrupoCiclo($v->getId(),$_POST['Id_ciclo']);
            if($horario->getId_docente()>0){
               $red=""; 
            }                  
            ?>
            <tr id-data="<?php echo $v->getId();?>" count="<?php echo $count;?>" class="<?php echo $red;?>">
                <td><?php echo $count; ?></td>
                <td><?php echo $v->getClave()?> </td>
                <td style="width: 170px;"><?php echo $mat->getNombre()?></td>
                <td><?php echo $nombre_ori?></td>
                <td class="td-center"><?php echo $turno;?></td>
                <td class="td-center"><?php echo $v->getCapacidad()?></td>
                <td class="td-center <?php if($disponibilidad<=0){ ?> sin_disponibilidad <?php } else{ ?> con_disponibilidad <?php } ?> " > <?php echo $disponibilidad?></td>
                <td class="td-center checkbox"><input type="checkbox" <?php if(strlen($red)>0){?> disabled="disabled" <?php } ?>></td>
                <td class="td-center"> 
                    <i class="fa fa-pencil-square-o" onclick="mostrar_box_grupo(<?php echo $v->getId()?>)"></i>
                    <i class="fa fa-trash" onclick="delete_grupo(<?php echo $v->getId()?>)"></i>
                    <i class="fa fa-plus-square" onclick="validar_existencia_docente(<?php echo $v->getId()?>)"></i>
                    <i class="fa fa-users" onclick="mostrar_alum(<?php echo $v->getId()?>)"></i>
                </td>
            </tr>
            <?php
        $count++;
        }
}

if($_POST['action']=="updateGrupo"){
    updateTrGrupo($_POST['Id_grupo'],$_POST['Id_ciclo']);
}

function updateTrGrupo($Id_grupo,$Id_ciclo){
    $DaoMaterias= new DaoMaterias();
    $DaoGrupos= new DaoGrupos();
    $DaoTurnos= new DaoTurnos();
    $DaoOrientaciones= new DaoOrientaciones();   
    $DaoHorarioDocente= new DaoHorarioDocente();
    $grupo=$DaoGrupos->show($Id_grupo);
    $mat = $DaoMaterias->show($grupo->getId_mat());
    $tur = $DaoTurnos->show($grupo->getTurno());
    $turno=$tur->getNombre();
    
    $nombre_ori="";
    if($grupo->getId_ori()>0){
    $ori = $DaoOrientaciones->show($grupo->getId_ori());
    $nombre_ori=$ori->getNombre();
    }
    $disponibilidad=$DaoGrupos->getDisponibilidadGrupo($grupo->getId());
    $red="pink";
    $horario=$DaoHorarioDocente->getHorarioByGrupoCiclo($Id_grupo,$Id_ciclo);
    if($horario->getId_docente()>0){
       $red=""; 
    }
    ?>
    <td><?php echo $count; ?></td>
    <td><?php echo $grupo->getClave()?> </td>
    <td style="width: 170px;"><?php echo $mat->getNombre()?></td>
    <td><?php echo $nombre_ori?></td>
    <td class="td-center"><?php echo $turno;?></td>
    <td class="td-center"><?php echo $grupo->getCapacidad()?></td>
    <td class="td-center <?php if($disponibilidad<=0){ ?> sin_disponibilidad <?php } else{ ?> con_disponibilidad <?php } ?> " > <?php echo $disponibilidad?></td>
    <td class="td-center checkbox"><input type="checkbox" <?php if(strlen($red)>0){?> disabled="disabled" <?php } ?>></td>
    <td class="td-center"> 
        <i class="fa fa-pencil-square-o" onclick="mostrar_box_grupo(<?php echo $grupo->getId()?>)"></i>
        <i class="fa fa-trash" onclick="delete_grupo(<?php echo $grupo->getId()?>)"></i>
        <i class="fa fa-plus-square" onclick="validar_existencia_docente(<?php echo $grupo->getId()?>)"></i>
        <i class="fa fa-users" onclick="mostrar_alum(<?php echo $grupo->getId()?>)"></i>
    </td>
    <?php   
}


if ($_POST['action'] == "mostrar_motivo_baja") {
	if ($_POST['Id_mot'] > 0) {
            $class_motivos_bajas= new class_motivos_bajas();
            $mot=$class_motivos_bajas->get_motivo_baja($_POST['Id_mot']);
	}
?>
<div id="box_emergente" class="motivos_baja">
    <h1><img src="images/configu.png" alt="configu" width="40" height="40" />Motivos de baja</h1>
    <div>
        <p>Nombre:<br><input type="text" placeholder="Nombre" id="nombre" value="<?php echo $mot['Nombre']?>"/></p>
        <button onclick="save_motivo_baja(<?php echo $_POST['Id_mot'] ?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cancelar</button>
    </div>
</div>
<?php
}

if($_POST['action'] == "verificar_orientaciones") {
  $class_ofertas = new class_ofertas();
  $mat_esp=$class_ofertas->get_materia_esp($_POST['Id_mat_esp']);
  if($mat_esp['Tiene_orientacion']==1){
      ?>
        <p>Orientaci&oacute;n<br>                                   
          <select id="orientaciones">
              <option value="0">Selecciona una orientaci&oacute;n</option>
              <?php
                  $esp=$class_ofertas->get_especialidad($mat_esp['Id_esp']);
                  foreach ($esp['Orientaciones'] as $k => $v) {
                  ?>
                  <option value="<?php echo $v['Id_ori'] ?>"> <?php echo $v['Nombre_ori'] ?> </option>
                  <?php
                  }
                  ?>
          </select></p>
    <?php
  }
}
 

function update_tipos_usuarios() {
	?>
	<table  class="table">
	    <thead>
	        <tr>
                    <td>#</td>
	            <td>Nombre</td>
	            <td>Acciones</td>
	        </tr>
	    </thead>
	    <tbody>
                
	        <?php
                $count=1;
                $DaoTiposUsuarios= new DaoTiposUsuarios();
                 foreach($DaoTiposUsuarios->getTiposUsuarios() as $tipo){
	                ?>
	                <tr>
                            <td><?php echo $count?></td>
                            <td><?php echo $tipo->getNombre_tipo() ?></td>
	                    <td>
	                        <button onclick="delete_tipo_usu(<?php echo $tipo->getId(); ?>)">Eliminar</button>
	                        <button onclick="mostrar_tipo_usu(<?php echo $tipo->getId(); ?>)">Editar</button>
	                    </td>
	                </tr>
	                <?php
                        $count++;
	        }
	        ?>
	    </tbody>
	</table>
	<?php
}


if ($_POST['action'] == "mostrar_tipo_usu") {
    $permisos=array();
    if ($_POST['Id_tipo'] > 0) {
        $class_tipos_usuarios= new class_tipos_usuarios();
        $tipo=$class_tipos_usuarios->get_tipo($_POST['Id_tipo']);
        foreach($tipo['Permisos'] as $k=>$v){
            $permisos[$v['Id_perm']]=1;
        }
    }
?>
<div id="box_emergente" class="tipos_usuarios">
    <h1>Tipo de usuario</h1>
    <p>Nombre:<br><input type="text" placeholder="Nombre" id="nombre" value="<?php echo $tipo['Nombre_tipo']?>"/></p>
     <ul id="list_perm">
         <?php
          $class_permisos= new class_permisos();
          foreach($class_permisos->get_permisos() as $k=>$v){
               ?>
               <li>
                   <table>
                       <tbody>
                           <tr>
                               <td><?php echo $v['Nombre_perm'];?></td>
                               <td><input type="checkbox" <?php if(isset($permisos[$v['Id_perm']])){?> checked="checked" <?php } ?> value="<?php echo $v['Id_perm']?>"/></td>
                           </tr>
                       </tbody>
                   </table>
               </li>
              <?php
           }   
         ?>     
        </ul>
    <button onclick="save_tipo_usu(<?php echo $_POST['Id_tipo'] ?>)" class="button">Guardar</button>
    <button onclick="ocultar_error_layer()" class="button">Cancelar</button>
  </div>
<?php
}


if ($_POST['action'] == "save_tipo_usu") {
    $DaoTiposUsuarios= new DaoTiposUsuarios();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    if ($_POST['Id_tipo'] > 0) {
        $TiposUsuarios=$DaoTiposUsuarios->show($_POST['Id_tipo']);
        $TiposUsuarios->setNombre_tipo($_POST['nombre']);
        $DaoTiposUsuarios->update($TiposUsuarios);
        $Id_tipo=$_POST['Id_tipo'];
    } else {
        $TiposUsuarios= new TiposUsuarios();
        $TiposUsuarios->setNombre_tipo($_POST['nombre']);
        $TiposUsuarios->setActivo_tipo(1);
        $TiposUsuarios->setId_plantel($usu->getId_plantel());
        $Id_tipo=$DaoTiposUsuarios->add($TiposUsuarios);
    }
    
   $sql_nueva_entrada=sprintf("DELETE FROM permisos_tipo_usu WHERE Id_tipo_usu=%s",
      	GetSQLValueString($Id_tipo, "int"));
      	$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
      	
   foreach($_POST['perm'] as $v){ 	
  	$sql_nueva_entrada=sprintf("INSERT INTO permisos_tipo_usu (Id_perm, Id_tipo_usu) VALUES (%s, %s);",
  	   	GetSQLValueString($v, "int"),
  	   	GetSQLValueString($Id_tipo, "int"));
  	   	$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());	
    }	
    update_tipos_usuarios();
}

if ($_POST['action'] == "delete_tipo_usu") {
    $DaoTiposUsuarios= new DaoTiposUsuarios();
    $DaoTiposUsuarios->delete($_POST['Id_tipo']);
    update_tipos_usuarios();
}

if ($_POST['action'] == "mostrar_box_categoria") {
    if ($_POST['Id_cat'] > 0) {
        $class_categorias_libros = new class_categorias_libros($_POST['Id_cat']);
        $cat=$class_categorias_libros->get_categoria();
    }
    ?>
    <div id="box_emergente" class="categorias">
        <h1><img src="images/configu.png" alt="configu" width="40" height="40" />Categor&iacute;a</h1>
        <p><input type="text" placeholder="Nombre" id="nombre" value="<?php echo $cat['Nombre'] ?>"/></p>
        <p><input type="text" placeholder="C&oacute;digo" id="codigo" value="<?php echo $cat['Codigo'] ?>"/></p>
        <button onclick="save_categoria(<?php echo $_POST['Id_cat'] ?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cerrar</button>
    </div>
    <?php
}

if ($_POST['action'] == "mostrar_box_beca") {
    $DaoBecas= new DaoBecas();
    $getNombre="";
    $getDescripcion="";
    $getMonto="";
    $getCalificacionMin="";
    $texto="Nueva";
    if ($_POST['Id_beca']> 0) {
        $beca=$DaoBecas->show($_POST['Id_beca']);
        $getNombre=$beca->getNombre();
        $getDescripcion=$beca->getDescripcion();
        $getMonto=$beca->getMonto();
        $getCalificacionMin=$beca->getCalificacionMin();
        $texto="Editar";
    }
    ?>
    <div class="box-emergente-form box-becas">
        <h1><?php echo $texto;?> beca</h1>
        <p>Nombre<input type="text"  id="nombre" value="<?php echo $getNombre ?>"/></p>
        <p>Porcentaje<input type="text" id="porcentaje" value="<?php echo $getMonto ?>"/></p>
        <p>Calificación minima<input type="text"  id="calificacion" value="<?php echo $getCalificacionMin ?>"/></p>
        <p>Descripción<textarea id="descripcion"><?php echo $getDescripcion;?></textarea></p>
        <button onclick="save_beca(<?php echo $_POST['Id_beca'] ?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cerrar</button>
    </div>
    <?php
    
}



if ($_POST['action'] == "mostrar_box_subcategoria") {
    ?>
    <div id="box_emergente" class="categorias">
        <h1><img src="images/configu.png" alt="configu" width="40" height="40" />Subcategor&iacute;a</h1>
        <p><input type="text" placeholder="Nombre" id="nombre" value="<?php echo $cat['Nombre'] ?>"/></p>
        <p><input type="text" placeholder="C&oacute;digo" id="codigo" value="<?php echo $cat['Codigo'] ?>"/></p>
        <button onclick="save_subcategoria(<?php echo $_POST['Id_cat'] ?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cerrar</button>
    </div>
    <?php
}

if ($_POST['action'] == "save_categoria") {
    if ($_POST['Id_cat']>0){
    $sql_nueva_entrada = sprintf("UPDATE Categorias_libros SET Nombre=%s,Codigo=%s WHERE Id_cat=%s", 
            GetSQLValueString($_POST['nombre'] , "text"), 
            GetSQLValueString($_POST['codigo'] , "text"),
            GetSQLValueString($_POST['Id_cat'] , "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
} else {
    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
    $usu = $class_usuarios->get_usu();
    $sql_nueva_entrada = sprintf("INSERT INTO Categorias_libros (Nombre,Id_plantel,Activo,Codigo,Parent) VALUES (%s,%s,%s,%s,%s);", 
            GetSQLValueString($_POST['nombre'] , "text"),
            GetSQLValueString($usu['Id_plantel'] , "int"),
            GetSQLValueString(1 , "int"),
            GetSQLValueString($_POST['codigo'] , "text"),
            GetSQLValueString(0, "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
}
update_categorias_libros();
}

if ($_POST['action'] == "delete_categoria_libros") {
	$sql_nueva_entrada = sprintf("UPDATE  Categorias_libros  SET Activo=%s WHERE Id_cat=%s", 
                GetSQLValueString(0 , "int"), 
	        GetSQLValueString($_POST['Id_cat'] , "int"));
	$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
	update_categorias_libros();
}


if ($_POST['action'] == "save_subcategoria") {
    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
    $usu = $class_usuarios->get_usu();
    $sql_nueva_entrada = sprintf("INSERT INTO Categorias_libros (Nombre,Id_plantel,Activo,Codigo,Parent) VALUES (%s,%s,%s,%s,%s);", 
            GetSQLValueString($_POST['nombre'] , "text"),
            GetSQLValueString($usu['Id_plantel'] , "int"),
            GetSQLValueString(1 , "int"),
            GetSQLValueString($_POST['codigo'] , "text"),
            GetSQLValueString($_POST['Id_cat'], "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    update_categorias_libros();
}


/*PLan nuevo de clases*/
function update_becas(){
    ?>
 	<table class="table">
	    <thead>
	        <tr>
                    <td>#</td>
	            <td>Nombre</td>
                    <td class="td-center">Porcentaje</td>
                    <td class="td-center">Calificación min.</td>
                    <td>Descripción</td>
                    <td class="td-center">Fecha</td>
	            <td class="td-center">Acciones</td>
	        </tr>
	    </thead>
	    <tbody>
	        <?php
                $count=1;
                $DaoBecas= new DaoBecas();
                foreach($DaoBecas->showAll() as $beca){
                    ?>
                	<tr>
                            <td><?php echo $count?></td>
                            
	                    <td><?php echo $beca->getNombre() ?></td>
                            <td class="td-center"><?php echo $beca->getMonto() ?></td>
                            <td class="td-center"><?php echo $beca->getCalificacionMin() ?></td>
                            <td><?php echo $beca->getDescripcion() ?></td>
                            <td class="td-center"><?php echo $beca->getDateCreated() ?></td>
                            <td class="td-center" style="width: 100px;">
                                <i class="fa fa-trash-o" aria-hidden="true" onclick="delete_beca(<?php echo $beca->getId(); ?>)" title="Eliminar"></i>
                                <i class="fa fa-pencil-square-o" aria-hidden="true" onclick="mostrar_box_beca(<?php echo $beca->getId(); ?>)" title="Editar"></i>
                                <?php
                                if(file_exists("files/".$beca->getContrato().".pdf")){
                                    ?>
                                      <a href="files/<?php echo $beca->getContrato()?>.pdf" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true" title="Mostrar contrato beca"></i></a>
                                      <i class="fa fa-times" aria-hidden="true" title="Eliminar contato beca" onclick="deleteContratoBeca(<?php echo $beca->getId(); ?>)"></i>
                                    <?php
                                }else{
                                   ?>
                                      <i class="fa fa-plus" aria-hidden="true" onclick="mostrarFinder(<?php echo $beca->getId(); ?>)" title="Añadir contrato beca"></i>
                                   <?php
                                }
                                ?>
                                
	                    </td>
	                </tr>
                    <?php  
                    $count++;
                }
	        ?>
	    </tbody>
	</table>   
<?php
}


if($_POST['action']=="save_beca"){
    $DaoBecas= new DaoBecas();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    if($_POST['Id_beca']>0){
        $beca=$DaoBecas->show($_POST['Id_beca']);
        $beca->setCalificacionMin($_POST['calificacion']);
        $beca->setDescripcion($_POST['descripcion']);
        $beca->setMonto($_POST['porcentaje']);
        $beca->setNombre($_POST['nombre']);
        $DaoBecas->update($beca);
    }else{
       $Becas= new Becas();
       $Becas->setActivo(1);
       $Becas->setCalificacionMin($_POST['calificacion']);
       $Becas->setDateCreated(date('Y-m-d H:i:s'));
       $Becas->setDescripcion($_POST['descripcion']);
       $Becas->setId_plantel($usu->getId_plantel());
       $Becas->setId_usu($_COOKIE['admin/Id_usu']);
       $Becas->setMonto($_POST['porcentaje']);
       $Becas->setNombre($_POST['nombre']);
       $DaoBecas->add($Becas);
    }
    update_becas();
}



if ($_POST['action'] == "delete_beca") {
    $DaoBecas= new DaoBecas();
    $DaoBecas->delete($_POST['Id_beca']);
    update_becas();
}


if($_POST['action']=="save_disponibilidad"){
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $ParametrosPlantel=$DaoParametrosPlantel->existeParametro($_POST['Id_disponibilidad']);
    if($ParametrosPlantel->getId()>0){
        $ParametrosPlantel->setValor($_POST['Disponibilidad']);
        $DaoParametrosPlantel->update($ParametrosPlantel);
    }else{
        $ParametrosPlantel= new ParametrosPlantel();
        $ParametrosPlantel->setId_param($_POST['Id_disponibilidad']);
        $ParametrosPlantel->setId_plantel($usu->getId_plantel());
        $ParametrosPlantel->setValor($_POST['Disponibilidad']);
        $DaoParametrosPlantel->add($ParametrosPlantel);
    }
    
    $ParametrosPlantel=$DaoParametrosPlantel->existeParametro($_POST['Id_ciclo']);
    if($ParametrosPlantel->getId()>0){
        $ParametrosPlantel->setValor($_POST['CicloDisponibilidad']);
        $DaoParametrosPlantel->update($ParametrosPlantel);
    }else{
        $ParametrosPlantel= new ParametrosPlantel();
        $ParametrosPlantel->setId_param($_POST['Id_ciclo']);
        $ParametrosPlantel->setId_plantel($usu->getId_plantel());
        $ParametrosPlantel->setValor($_POST['CicloDisponibilidad']);
        $DaoParametrosPlantel->add($ParametrosPlantel);
    }
}





if($_POST['action']=="saveParametroTexto"){
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $ParametrosPlantel=$DaoParametrosPlantel->existeParametro($_POST['Id_param']);
    if($ParametrosPlantel->getId()>0){
        $ParametrosPlantel->setValor($_POST['valor']);
        $DaoParametrosPlantel->update($ParametrosPlantel);
    }else{
        $ParametrosPlantel= new ParametrosPlantel();
        $ParametrosPlantel->setId_param($_POST['Id_param']);
        $ParametrosPlantel->setId_plantel($usu->getId_plantel());
        $ParametrosPlantel->setValor($_POST['valor']);
        $DaoParametrosPlantel->add($ParametrosPlantel);
    } 
}



if ($_POST['action'] == "mostrar_add_periodo") {
    $getNombre="";
    $getFechaIni="";
    $getFechaFin="";
    $Id_per=0;
    if ($_POST['Id_per']> 0) {
        $DaoPeriodosExamenes= new DaoPeriodosExamenes();
        $periodo=$DaoPeriodosExamenes->show($_POST['Id_per']);
        $getNombre=$periodo->getNombre();
        $getFechaIni=$periodo->getFechaIni();
        $getFechaFin=$periodo->getFechaFin();
        $Id_per=$_POST['Id_per'];
    }

    ?>
    <div class="box-emergente-form box-becas">
        <h1>Período de exámen</h1>
        <p>Nombre<br><input type="text"  id="nombre" value="<?php echo $getNombre ?>"/></p>
        <p>Fecha inicio<br><input type="date" id="fechaIni" value="<?php echo $getFechaIni ?>"/></p>
        <p>Fecha fin<br><input type="date"  id="fechaFin" value="<?php echo $getFechaFin ?>"/></p>
        <button onclick="save_periodo_examen(<?php echo $Id_per ?>,<?php echo $_POST['Id_ciclo']?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cerrar</button>
    </div>
    <?php
    
}


if($_POST['action']=="save_periodo_examen"){
    $DaoPeriodosExamenes= new DaoPeriodosExamenes();
     if ($_POST['Id_per']> 0) {
        $periodo=$DaoPeriodosExamenes->show($_POST['Id_per']);
        $periodo->setNombre($_POST['nombre']);
        $periodo->setFechaIni($_POST['fechaIni']);
        $periodo->setFechaFin($_POST['fechaFin']);
        $DaoPeriodosExamenes->update($periodo);
    }  else{
        $periodo=new PeriodosExamenes();
        $periodo->setNombre($_POST['nombre']);
        $periodo->setFechaIni($_POST['fechaIni']);
        $periodo->setFechaFin($_POST['fechaFin']);
        $periodo->setId_ciclo($_POST['id_ciclo']);
        $periodo->setId_usu($_COOKIE['admin/Id_usu']);
        $periodo->setDateCreated(date('Y-m-d H:i:s'));
        $DaoPeriodosExamenes->add($periodo);   
    } 
    
    update_ciclos();
}


if($_POST['action']=="delete_periodo"){
    $DaoPeriodosExamenes= new DaoPeriodosExamenes();
    $DaoPeriodosExamenes->delete($_POST['Id_per']);
     update_ciclos();
}

if ($_POST['action'] == "mostrar_add_periodo_vacaciones") {
    $getNombre="";
    $getFechaIni="";
    $getFechaFin="";
    $Id_per=0;
    if ($_POST['Id_per']> 0) {
        $DaoPeriodosVacaciones= new DaoPeriodosVacaciones();
        $periodo=$DaoPeriodosVacaciones->show($_POST['Id_per']);
        $getNombre=$periodo->getNombre();
        $getFechaIni=$periodo->getFechaIni();
        $getFechaFin=$periodo->getFechaFin();
        $Id_per=$_POST['Id_per'];
    }

    ?>
    <div class="box-emergente-form box-becas">
        <h1>Período de vacaciones</h1>
        <p>Nombre<br><input type="text"  id="nombre" value="<?php echo $getNombre ?>"/></p>
        <p>Fecha inicio<br><input type="date" id="fechaIni" value="<?php echo $getFechaIni ?>"/></p>
        <p>Fecha fin<br><input type="date"  id="fechaFin" value="<?php echo $getFechaFin ?>"/></p>
        <button onclick="save_periodo_vacaciones(<?php echo $Id_per ?>,<?php echo $_POST['Id_ciclo']?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cerrar</button>
    </div>
    <?php
    
}


if($_POST['action']=="save_periodo_vacaciones"){
    $DaoPeriodosVacaciones= new DaoPeriodosVacaciones();
     if ($_POST['Id_per']> 0) {
        $periodo=$DaoPeriodosVacaciones->show($_POST['Id_per']);
        $periodo->setNombre($_POST['nombre']);
        $periodo->setFechaIni($_POST['fechaIni']);
        $periodo->setFechaFin($_POST['fechaFin']);
        $DaoPeriodosVacaciones->update($periodo);
    }  else{
        $periodo=new PeriodosVacaciones();
        $periodo->setNombre($_POST['nombre']);
        $periodo->setFechaIni($_POST['fechaIni']);
        $periodo->setFechaFin($_POST['fechaFin']);
        $periodo->setId_ciclo($_POST['id_ciclo']);
        $periodo->setId_usu($_COOKIE['admin/Id_usu']);
        $periodo->setDateCreated(date('Y-m-d H:i:s'));
        $DaoPeriodosVacaciones->add($periodo);   
    } 
    
    update_ciclos();
}


if($_POST['action']=="delete_periodo_vacaciones"){
    $DaoPeriodosVacaciones= new DaoPeriodosVacaciones();
    $DaoPeriodosVacaciones->delete($_POST['Id_per']);
     update_ciclos();
}


if (isset($_POST['action']) && $_POST['action'] == "mostrar_box_tipo_activo") {
    $getNombre="";
    $texto="Nueva";
    $Id_tipo=0;
    if (isset($_POST['Id_tipo']) && $_POST['Id_tipo']> 0) {
        $DaoTiposActivo= new DaoTiposActivo();
        $tipo=$DaoTiposActivo->show($_POST['Id_tipo']);
        $getNombre=$tipo->getNombre();
        $texto="Editar";
        $Id_tipo=$_POST['Id_tipo'];
    }
    ?>
    <div class="box-emergente-form box-becas">
        <h1><?php echo $texto;?> tipo de activo</h1>
        <p>Nombre<input type="text"  id="nombre" value="<?php echo $getNombre ?>"/></p>
        <button onclick="save_tipo_activo(<?php echo $Id_tipo ?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cerrar</button>
    </div>
    <?php
    
}


if (isset($_POST['action']) && $_POST['action'] == "mostrar_box_turno") {
    $getNombre="";
    $getHoraIni="";
    $getHoraFin="";
    $texto="Nueva";
    if (isset($_POST['Id_turno']) && $_POST['Id_turno']> 0) {
        $DaoTurnos= new DaoTurnos();
        $turno=$DaoTurnos->show($_POST['Id_turno']);
        $getNombre=$turno->getNombre();
        $getHoraIni=$turno->getHoraIni();
        $getHoraFin=$turno->getHoraFin();
        $texto="Editar";
    }
    ?>
    <div class="box-emergente-form box-becas">
        <h1><?php echo $texto;?> turno</h1>
        <p>Nombre<input type="text"  id="nombre" value="<?php echo $getNombre ?>"/></p>
        <p>Hora inicio<input type="text" class="time start" id="HoraIni" value="<?php echo $getHoraIni ?>"/></p>
        <p>Hora fin<input type="text"  class="time end" id="HoraFin" value="<?php echo $getHoraFin ?>"/></p>
        <button onclick="save_turno(<?php echo $_POST['Id_turno'] ?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cerrar</button>
    </div>
    <?php
    
}


if (isset($_POST['action']) && $_POST['action'] == "save_tipo_activo") {
    $DaoTiposActivo= new DaoTiposActivo();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    if (isset($_POST['Id_tipo']) && $_POST['Id_tipo']> 0) {
        $TiposActivo=$DaoTiposActivo->show($_POST['Id_tipo']);
        $TiposActivo->setNombre($_POST['Nombre']);
        $DaoTiposActivo->update($TiposActivo);
    }else{
        $TiposActivo= new TiposActivo();
        $TiposActivo->setNombre($_POST['Nombre']);
        $TiposActivo->setId_usu($usu->getId());
        $TiposActivo->setId_plantel($usu->getId_plantel());
        $TiposActivo->setDateCreated(date('Y-m-d H:i:s'));
        $TiposActivo->setActivo(1);
        $DaoTiposActivo->add($TiposActivo);
    }
    update_tipos_activo();
}

if (isset($_POST['action']) && $_POST['action'] == "delete_tipo_activo") {
    $DaoTiposActivo= new DaoTiposActivo();
    $TiposActivo=$DaoTiposActivo->show($_POST['Id_tipo']);
    $TiposActivo->setActivo(0);
    $DaoTiposActivo->update($TiposActivo);
    update_tipos_activo();
}



if (isset($_POST['action']) && $_POST['action'] == "save_turno") {
    $DaoTurnos= new DaoTurnos();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    if (isset($_POST['Id_turno']) && $_POST['Id_turno']> 0) {
        $Turno=$DaoTurnos->show($_POST['Id_turno']);
        $Turno->setNombre($_POST['Nombre']);
        $Turno->setHoraIni($_POST['HoraIni']);
        $Turno->setHoraFin($_POST['HoraFin']);
        $DaoTurnos->update($Turno);
    }else{
        $Turnos= new Turnos();
        $Turnos->setNombre($_POST['Nombre']);
        $Turnos->setHoraIni($_POST['HoraIni']);
        $Turnos->setHoraFin($_POST['HoraFin']);
        $Turnos->setId_usu($usu->getId());
        $Turnos->setId_plantel($usu->getId_plantel());
        $Turnos->setDateCreated(date('Y-m-d H:i:s'));
        $Turnos->setActivo(1);
        $DaoTurnos->add($Turnos);
    }
     update_turnos();
}


if (isset($_POST['action']) && $_POST['action'] == "delete_turno") {
    $DaoTurnos= new DaoTurnos();
    $Turno=$DaoTurnos->show($_POST['Id_turno']);
    $Turno->setActivo(0);
    $DaoTurnos->update($Turno);
    update_turnos();
}


if (isset( $_GET['action']) && $_GET['action'] == "upload_contrato_beca") {
    $base= new base();
    $DaoBecas= new DaoBecas();
    $beca = $DaoBecas->show($_GET['Id_beca']);
    
  if (count($_FILES) > 0) {
    $type = substr($_FILES["file"]["name"], strpos($_FILES["file"]["name"], ".") + 1);
    if ($type == "pdf") {

      if (isset($_FILES["file"]["error"]) && $_FILES["file"]["error"] > 0) {

        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
      } else {

            if (file_exists("files/" . $beca->getContrato() . ".pdf")) {
                unlink("files/" . $beca->getContrato() . ".pdf");
            }
            $key = $base->generarKey();
            $beca->setContrato($key);
            $DaoBecas->update($beca);

            move_uploaded_file($_FILES["file"]["tmp_name"], "files/" . $key . ".pdf");
      }
    } else {
      echo "Invalid file: " . $type;
    }
  } elseif (isset($_GET['action'])) {

    if ($_GET['TypeFile'] == "application/pdf") {
      if (isset($_FILES["file"]["error"]) && $_FILES["file"]["error"] > 0) {

        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
      } else {
          
            if (file_exists("files/" . $beca->getContrato() . ".pdf")) {
                unlink("files/" . $beca->getContrato() . ".pdf");
            }
            $key = $base->generarKey();
            $beca->setContrato($key);
            $DaoBecas->update($beca);

            if (isset($_GET['base64'])) {
              // If the browser does not support sendAsBinary ()
              $content = base64_decode(file_get_contents('php://input'));
            } else {
              $content = file_get_contents('php://input');
            }
            file_put_contents('files/' . $key . '.pdf', $content);
      }
    } else {

      echo "Invalid file: " . $_GET['TypeFile'];
    }
  }
  update_becas();
}

if(isset($_POST['action']) && $_POST['action']=="deleteContratoBeca"){
   $DaoBecas= new DaoBecas();
   $beca=$DaoBecas->show($_POST['Id_beca']);
   if(file_exists("files/".$beca->getContrato().".pdf")){
       $beca->setContrato('');
       $DaoBecas->update($beca);
       unlink("files/".$beca->getContrato().".pdf");
   }
   update_becas();
}


if ($_POST['action'] == "mostrar_evento_ciclo") {
    $getNombre="";
    $getFechaIni="";
    $getFechaFin="";
    $Id_eve=0;
    $Tipo="";
    if ($_POST['Id_eve']> 0) {
        $DaoEventosCiclo= new DaoEventosCiclo();
        $evento=$DaoEventosCiclo->show($_POST['Id_eve']);
        $getNombre=$evento->getNombre();
        $getFechaIni=$evento->getFechaIni();
        $getFechaFin=$evento->getFechaFin();
        $Tipo=$evento->getTipoEvento();
        $Id_eve=$_POST['Id_eve'];
    }

    ?>
    <div class="box-emergente-form box-eventos-ciclo">
        <h1>Evento del ciclo</h1>
        <p>Nombre<br><input type="text"  id="nombre" value="<?php echo $getNombre ?>"/></p>
        <p>Fecha inicio<br><input type="date" id="fechaIni" value="<?php echo $getFechaIni ?>"/></p>
        <p>Fecha fin<br><input type="date"  id="fechaFin" value="<?php echo $getFechaFin ?>"/></p>
        <p>Evento para:</p>
        <p><input type="checkbox" value="alumno" id="alumno"   <?php if($Tipo=="alumno" || $Tipo=="ambos"){?> checked="checked" <?php }?>> Alumnos</p>
        <p><input type="checkbox" value="docente" id="docente" <?php if($Tipo=="docente" || $Tipo=="ambos"){?> checked="checked" <?php }?>> Docentes</p>
        <button onclick="save_evento_ciclo(<?php echo $Id_eve ?>,<?php echo $_POST['Id_ciclo']?>)" class="button">Guardar</button>
        <button onclick="ocultar_error_layer()" class="button">Cerrar</button>
    </div>
    <?php
}

if($_POST['action']=="save_evento_ciclo"){
      $DaoEventosCiclo= new DaoEventosCiclo();
     if ($_POST['Id_eve']> 0) {
        $evento=$DaoEventosCiclo->show($_POST['Id_eve']);
        $evento->setNombre($_POST['nombre']);
        $evento->setFechaIni($_POST['fechaIni']);
        $evento->setFechaFin($_POST['fechaFin']);
        $evento->setTipoEvento($_POST['tipo']);
        $DaoEventosCiclo->update($evento);
    }  else{
        $evento=new EventosCiclo();
        $evento->setNombre($_POST['nombre']);
        $evento->setFechaIni($_POST['fechaIni']);
        $evento->setFechaFin($_POST['fechaFin']);
        $evento->setId_ciclo($_POST['id_ciclo']);
        $evento->setId_usu($_COOKIE['admin/Id_usu']);
        $evento->setDateCreated(date('Y-m-d H:i:s'));
        $evento->setTipoEvento($_POST['tipo']);
        $DaoEventosCiclo->add($evento);   
    } 
    update_ciclos();
}


if($_POST['action']=="delete_evento_ciclo"){
    $DaoEventosCiclo= new DaoEventosCiclo();
    $DaoEventosCiclo->delete($_POST['Id_eve']);
     update_ciclos();
}



if($_POST['action']=="asignarAlumnosGrupo"){
    
    if($_POST['Id_grupo']>0 && $_POST['Id_ciclo']>0){
        $DaoAlumnos=new DaoAlumnos();
        $DaoOfertasAlumno=new DaoOfertasAlumno();
        $base= new base();
        $DaoGrupos= new DaoGrupos();
        $DaoPagosCiclo= new DaoPagosCiclo();
        $DaoCiclosAlumno= new DaoCiclosAlumno();
        $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
        $DaoUsuarios= new DaoUsuarios();
        $DaoHorarioDocente= new DaoHorarioDocente();

        //Verificamos si elgrupo tiene docente asignado
        $horario=$DaoHorarioDocente->getHorarioByGrupoCiclo($_POST['Id_grupo'],$_POST['Id_ciclo']);
        if($horario->getId_docente()>0){
            $grupo= $DaoGrupos->show($_POST['Id_grupo']);
            
            $Id_ori_grupo="";
            $query_orientacion="";
            if($grupo->getId_ori()>0){
               $Id_ori_grupo=$grupo->getId_ori();
            }
            $query = "
            SELECT 
                ofertas_alumno.Id_ofe_alum,ofertas_alumno.Id_alum,ofertas_alumno.Opcion_pago,
                materias_ciclo_ulm.Id_ciclo_mat,materias_ciclo_ulm.Id_grupo,materias_ciclo_ulm.Tipo,materias_ciclo_ulm.Id_ori AS Id_ori_mat_ext,
                ciclos_alum_ulm.Id_ciclo,ciclos_alum_ulm.Id_ciclo_alum,
                especialidades_ulm.Num_pagos,
                inscripciones_ulm.Matricula,inscripciones_ulm.Nombre_ins,inscripciones_ulm.ApellidoP_ins,inscripciones_ulm.ApellidoM_ins,
                orientaciones_ulm.Nombre_ori,orientaciones_ulm.Id_ori
            FROM ofertas_alumno 
            JOIN especialidades_ulm ON ofertas_alumno.Id_esp=especialidades_ulm.Id_esp
            JOIN (SELECT *  FROM ciclos_alum_ulm 
                      WHERE Id_ciclo=".$_POST['Id_ciclo']."
                  ) AS ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
            JOIN (SELECT materias_ciclo_ulm.* 
                          From materias_ciclo_ulm 
                          JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                   WHERE materias_ciclo_ulm.Id_grupo IS NULL 
                         AND Materias_especialidades.Id_mat=".$grupo->getId_mat()."
                 ) AS materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum 
            JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
            LEFT JOIN orientaciones_ulm ON ofertas_alumno.Id_ori=orientaciones_ulm.Id_ori
            WHERE ofertas_alumno.Activo_oferta=1 
                  AND ofertas_alumno.Turno=".$grupo->getTurno();
             foreach($base->advanced_query($query) as $k=>$row){
                //Verificar la capacidad del grupo
                $cantAlumnos=$DaoGrupos->getTotalAlumnosGrupo($_POST['Id_grupo']);
                if($cantAlumnos<$grupo->getCapacidad()){
                   //Si el alumno tiene la materia
                   //Si el alumno ya pago la inscripcion
                   //Si el alumno no esta inscripto en un grupo
                    $Id_ori_alum=$row['Id_ori'];
                    if($row['Tipo']==2){
                       $Id_ori_alum=$row['Id_ori_mat_ext']; 
                    }
                   if($Id_ori_grupo==null){
                      $Id_ori_alum=null;
                  }
                  $cargosCiclo=$DaoPagosCiclo->getCargosCiclo($row['Id_ciclo_alum']);
                  if((count($cargosCiclo)>1 || ($row['Num_pagos']==1 && count($cargosCiclo)>=1)) && $Id_ori_grupo==$Id_ori_alum){
                        $materiasCicloAlumno=$DaoMateriasCicloAlumno->show($row['Id_ciclo_mat']);
                        $Id_ciclo_alumno=$materiasCicloAlumno->getId_ciclo_alum();
                        $Id_alum=$materiasCicloAlumno->getId_alum();
                        $cicloAlumno=$DaoCiclosAlumno->show($Id_ciclo_alumno);
                        //Validamos si las horas del grupo a registrar, no se cruzan con las horas de otro grupo del alumno
                        $resp=$DaoAlumnos->getValidarHoraDiaLibres($cicloAlumno->getId_ofe_alum(), $cicloAlumno->getId_ciclo(), $_POST['Id_grupo']);
                        if($resp==0){
                            $materiaCiclo=$DaoMateriasCicloAlumno->show($row['Id_ciclo_mat']);
                            $materiaCiclo->setId_grupo($_POST['Id_grupo']);
                            //$DaoMateriasCicloAlumno->update($materiaCiclo);
                         }

                    }
                }
            }
        }

        updateTrGrupo($_POST['Id_grupo'],$_POST['Id_ciclo']);
    }
    exit();
}


if($_POST['action']=="getCantidadAlumnos"){
   $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
   $DaoMaterias= new DaoMaterias();
   $DaoAulas= new DaoAulas();
   $matEsp=$DaoMateriasEspecialidad->show($_POST['Id_mat_esp']);
   $mat=$DaoMaterias->show($matEsp->getId_mat());
   $aula=$DaoAulas->getTipoAula($mat->getTipo_aula());
    echo $aula->getCapacidad_aula();
}




if($_POST['action']=="RecibirNotificaciones"){
    $tiposUsuario=implode(";", $_POST['tiposUsuario']);
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $DaoParametrosPlantel= new DaoParametrosPlantel();
    $ParametrosPlantel=$DaoParametrosPlantel->existeParametro($_POST['Id_param']);
    if($ParametrosPlantel->getId()>0){
        $ParametrosPlantel->setValor($tiposUsuario);
        $DaoParametrosPlantel->update($ParametrosPlantel);
    }else{
        $ParametrosPlantel= new ParametrosPlantel();
        $ParametrosPlantel->setId_param($_POST['Id_param']);
        $ParametrosPlantel->setId_plantel($usu->getId_plantel());
        $ParametrosPlantel->setValor($tiposUsuario);
        $DaoParametrosPlantel->add($ParametrosPlantel);
    } 
}
