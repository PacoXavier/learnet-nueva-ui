<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

$base= new base();
$DaoAlumnos = new DaoAlumnos();
$DaoOfertas = new DaoOfertas();
$DaoEspecialidades = new DaoEspecialidades();
$DaoOrientaciones = new DaoOrientaciones();
$DaoCiclos = new DaoCiclos();
$DaoOfertasAlumno = new DaoOfertasAlumno();
$DaoCiclosAlumno= new DaoCiclosAlumno();
$DaoPagosCiclo= new DaoPagosCiclo();
$DaoDirecciones= new DaoDirecciones();
$DaoMediosEnterar= new DaoMediosEnterar();
$DaoContactos= new DaoContactos();
$DaoTurnos= new DaoTurnos();
$DaoTutores= new DaoTutores();

links_head("Alumno  ");
?>
<link rel="stylesheet" media="print" href="css/alumno_print.css?v=2011.6.16.18.35" id="cssPrintpage"> 
<?php
write_head_body();
write_body();

$Id_alum=0;
$Id_dir=0;
$Img_alum="";
$Nombre_ins= "";
$ApellidoP_ins="";
$ApellidoM_ins="";
$Matricula= "";
$Refencia_pago="";
$FechaNac="";
$Edad_ins="";
$Sexo= "";
$Email_ins= "";
$Tel_ins="";
$Cel_ins="";
$Curp_ins= "";
$Comentarios= "";
$NombreTutor= "";
$EmailTutor= "";
$TelTutor= "";
$Tipo_sangre= "";
$Alergias="";
$Enfermedades_cronicas= "";
$Preinscripciones_medicas="";
$Id_medio_ent="";
$Seguimiento_ins= "";
$UltGrado_est= "";
$EscuelaPro="";
$matriculaSep="";
$expedienteSep="";

$ContactoNombre="";
$ContactoParentesco="";
$ContactoDireccion="";
$ContactoTel_casa="";
$ContactoTel_ofi="";
$ContactoCel="";
$ContactoEmail="";
$ContactoId_cont="";
                
$Calle_dir="";
$NumExt_dir="";
$NumInt_dir="";
$Colonia_dir="";
$Cp_dir="";
$Ciudad_dir="";
$Estado_dir="";
            
?>
<table id="tabla">
    <?php
    $readonly = "";
    if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
        $Id_alum=$_REQUEST['id'];
        $alum = $DaoAlumnos->show($Id_alum);
        $Img_alum=$alum->getImg_alum();
        $Nombre_ins= $alum->getNombre();
        $ApellidoP_ins=$alum->getApellidoP();
        $ApellidoM_ins=$alum->getApellidoM();
        $Matricula= $alum->getMatricula();
        $Refencia_pago=$alum->getRefencia_pago();
        $FechaNac=$alum->getFechaNac();
        $Edad_ins=$alum->getEdad();
        $Sexo= $alum->getSexo();
        $Email_ins= $alum->getEmail();
        $Tel_ins=$alum->getTel();
        $Cel_ins=$alum->getCel();
        $Curp_ins= $alum->getCurp_ins();
        $Comentarios= $alum->getComentarios();
        $NombreTutor= $alum->getNombreTutor();
        $EmailTutor= $alum->getEmailTutor();
        $TelTutor= $alum->getTelTutor();
        $Tipo_sangre= $alum->getTipo_sangre();
        $Alergias=$alum->getAlergias();
        $Enfermedades_cronicas= $alum->getEnfermedades_cronicas(); 
        $Preinscripciones_medicas=$alum->getPreinscripciones_medicas();
        $Id_medio_ent=$alum->getId_medio_ent();
        $Seguimiento_ins= $alum->getSeguimiento_ins();
        $UltGrado_est= $alum->getUltGrado_est();
        $EscuelaPro=$alum->getEscuelaPro();
        $matriculaSep=$alum->getMatriculaSep();
        $expedienteSep=$alum->getExpedienteSep();

        $contacto=$DaoContactos->getPrimerContacto($Id_alum,'alumno');
        
        $ContactoNombre=$contacto->getNombre();
        $ContactoParentesco=$contacto->getParentesco();
        $ContactoDireccion=$contacto->getDireccion();
        $ContactoTel_casa=$contacto->getTel_casa();
        $ContactoTel_ofi=$contacto->getTel_ofi();
        $ContactoCel=$contacto->getCel();
        $ContactoEmail=$contacto->getEmail();
        $ContactoId_cont=$contacto->getId();
        
        if ($alum->getId_dir() > 0) {
            $Id_dir=$alum->getId_dir();
            $dir = $DaoDirecciones->show($alum->getId_dir());
            $Calle_dir=$dir->getCalle_dir();
            $NumExt_dir=$dir->getNumExt_dir();
            $NumInt_dir=$dir->getNumInt_dir();
            $Colonia_dir=$dir->getColonia_dir();
            $Cp_dir=$dir->getCp_dir();
            $Ciudad_dir=$dir->getCiudad_dir();
            $Estado_dir=$dir->getEstado_dir();
        }
        $readonly = 'readonly="readonly"';
    }
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <div class="foto_alumno" 
                    <?php if (strlen($Img_alum) > 0) { ?> 
                             style="background-image:url(files/<?php echo $Img_alum ?>.jpg) <?php } ?>">
                    </div>
                    <h1><?php echo ucwords(strtolower($Nombre_ins . " " . $ApellidoP_ins . " " . $ApellidoM_ins)) ?></h1>
                </div>
                <div class="seccion">
                    <h2>Datos Personales</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Matricula<br><input type="text" value="<?php echo $Matricula ?>" id="matricula" <?php echo $readonly; ?>/></li>
                        <li>Referencia de pago<br><input type="text" value="<?php echo $Refencia_pago ?>" id="referencia_pago" <?php echo $readonly; ?>/></li>
                        <li><span class="requerido">*</span>Nombre<br><input type="text" value="<?php echo $Nombre_ins ?>" id="nombre_alum"/></li>
                        <li><span class="requerido">*</span>Apellido Paterno<br><input type="text" value="<?php echo $ApellidoP_ins ?>" id="apellidoP_alum"/></li>
                        <li><span class="requerido">*</span>Apellido Materno<br><input type="text" value="<?php echo $ApellidoM_ins ?>" id="apellidoM_alum"/></li>
                        <?php
                        if ($FechaNac != null) {
                            $edad = $base->calcularEdad($FechaNac);
                        } else {
                            $edad = $Edad_ins;
                        }
                        ?>
                        <li>Edad<br><input type="text" id="edad_alum" value="<?php echo $edad ?>" <?php echo $readonly; ?>/></li>
                        <li>Sexo<br><select id="sexo">
                                <option></option>
                                <option value="m" <?php if ($Sexo == "m") { ?> selected="selected"<?php } ?>>Masculino</option>
                                <option value="f" <?php if ($Sexo == "f") { ?> selected="selected"<?php } ?>>Femenino</option>
                            </select>
                        </li>
                        <li>Fecha de Nacimiento<br><input type="date" id="fechaN_alum" value="<?php echo $FechaNac ?>"/></li>
                        <li>Email<br><input type="email" id="email_alum" value="<?php echo $Email_ins ?>"/></li>
                        <li>Tel&eacute;fono Casa<br><input type="tel" id="tel_alum" value="<?php echo $Tel_ins ?>"/></li>
                        <li>Tel&eacute;fono Movil<br><input type="tel" id="cel_alum" value="<?php echo $Cel_ins ?>"/></li>
                        <li>CURP<br><input type="text" id="curp_alum" value="<?php echo $Curp_ins ?>"/></li>
                        <li>Matricula SEP<br><input type="text" id="matriculaSep" value="<?php echo $matriculaSep ?>"/></li>
                        <li>Expediente SEP<br><input type="text" id="expedienteSep" value="<?php echo $expedienteSep ?>"/></li>
                        <li>Comentarios<br><textarea id="comentarios"><?php echo $Comentarios ?></textarea></li>
                    </ul>
                </div>
                <div class="seccion">
                    <h2>Direcci&oacute;n</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li><span class="requerido">*</span>Calle<br><input type="text" id="calle_alum" value="<?php echo $Calle_dir ?>"/></li>
                        <li><span class="requerido">*</span>N&uacute;mero exterior<br><input type="text" id="numExt_alum" value="<?php echo $NumExt_dir ?>"/></li>
                        <li>N&uacute;mero interior<br><input type="text" id="numInt_alum" value="<?php echo $NumInt_dir ?>"/></li>
                        <li>Colonia<br><input type="text" id="colonia_alum" value="<?php echo $Colonia_dir ?>"/></li>
                        <li>C&oacute;digo postal<br><input type="text" id="cp_alum" value="<?php echo $Cp_dir ?>"/></li>
                        <li><span class="requerido">*</span>Ciudad<br><input type="text" id="ciudad_alum" value="<?php echo $Ciudad_dir ?>"/></li>
                        <li>Estado<br><input type="text" id="estado_alum" value="<?php echo $Estado_dir ?>"/></li>
                    </ul>
                </div>
                <div class="seccion">
                    <h2>Datos del padre o tutor</h2>
                    <span class="linea"></span>
                    <div id="list_tutores">
                        <?php
                        $countTu=0;
                        if ($Id_alum > 0) {
                            foreach($DaoTutores->getTutoresAlumno($Id_alum) as $tutor){
                                    $id_dir=0;
                                    $dir=$DaoDirecciones->getDireccionPorTipoRel($tutor->getId(),'tutor');
                                    if($dir->getId()>0){
                                      $id_dir= $dir->getId(); 
                                    }
                            ?>
                                <ul class="form" id-tutor="<?php echo $tutor->getId();?>" id-dir="<?php echo $dir->getId()?>">
                                    <li>Parentesco<br><input type="text" class="parentesco-tutor" value="<?php echo $tutor->getParentesco();?>"/></li>
                                    <li>Nombre<br><input type="text" class="nombre-tutor" value="<?php echo $tutor->getNombre();?>"/></li>
                                    <li>Apellido paterno<br><input type="text" class="apellidoP-tutor" value="<?php echo $tutor->getApellidoP();?>"/></li>
                                    <li>Apellido materno<br><input type="text" class="apellidoM-tutor" value="<?php echo $tutor->getApellidoM();?>"/></li>
                                    <li>Correo electrónico<br><input type="text" class="email-tutor" value="<?php echo $tutor->getEmail();?>"/></li>
                                    <li>Teléfono<br><input type="text" class="tel-tutor" value="<?php echo $tutor->getTel();?>"/></li>
                                    <li>Celular<br><input type="text" class="cel-tutor" value="<?php echo $tutor->getCel();?>"/></li>
                                    <li>Calle<br><input type="text" class="calle-tutor" value="<?php echo $dir->getCalle_dir();?>"/></li>
                                    <li>Núm ext.<br><input type="text" class="numext-tutor" value="<?php echo $dir->getNumExt_dir();?>"/></li>
                                    <li>Núm int.<br><input type="text" class="numint-tutor" value="<?php echo $dir->getNumInt_dir();?>"/></li>
                                    <li>Colonia<br><input type="text" class="colonia-tutor" value="<?php echo $dir->getColonia_dir();?>"/></li>
                                    <li>Ciudad<br><input type="text" class="ciudad-tutor" value="<?php echo $dir->getCiudad_dir();?>"/></li>
                                    <li>Estado<br><input type="text" class="estado-tutor" value="<?php echo $dir->getEstado_dir();?>"/></li>
                                    <li>Código postal<br><input type="text" class="cp-tutor" value="<?php echo $dir->getCp_dir();?>"/></li>
                                    <li><button type="button" class="btns btns-primary btns-sm delete-tutor" onclick="delete_tutor(<?php echo $tutor->getId() ?>,<?php echo $id_dir?>)">Eliminar tutor</button></li>
                                </ul>
                                <hr>
                            <?php
                            $countTu++;
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="seccion">
                    <h2>Datos de Emergencia</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Tipo de sangre<br><input type="text"  id="tipo_sangre" value="<?php echo $Tipo_sangre ?>"/></li>
                        <li>Nombre del contacto<br><input type="text" id="nombre_contacto" value="<?php echo $ContactoNombre ?>"/></li>
                        <li>Parentesco<br><input type="text" id="parentesco_contacto" value="<?php echo $ContactoParentesco ?>"/></li>
                        <li>Direcci&oacute;n<br><input type="text" id="direccion_contacto" value="<?php echo $ContactoDireccion ?>"/></li>
                        <li>Tel&eacute;fono Casa<br><input type="text" id="tel_contacto" value="<?php echo $ContactoTel_casa ?>"/></li>
                        <li>Tel&eacute;fono Oficina<br><input type="text" id="telOfic_contacto" value="<?php echo $ContactoTel_ofi ?>"/></li>
                        <li>Celular<br><input type="text" id="celular_contacto" value="<?php echo $ContactoCel ?>"/></li>
                        <li>Email<br><input type="text" id="email_contacto" value="<?php echo $ContactoEmail ?>"/></li>
                    </ul>
                    <input type="hidden" id="Id_contacto" value="<?php echo $ContactoId_cont ?>"/>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <li>Alergias<br><textarea  id="alergias_contacto"><?php echo $Alergias ?></textarea></li>
                        <li>Enfermedades cr&oacute;nicas<br><textarea  id="cronicas_contacto"><?php echo $Enfermedades_cronicas ?></textarea></li>
                        <li>Pre-escripciones medicas<br><textarea  id="preinscripciones_medicas_contacto"><?php echo $Preinscripciones_medicas ?></textarea></li>
                    </ul>
                </div>
                <div class="seccion">
                    <h2>Datos de de inter&eacute;s</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Medio por el que se enter&oacute;<br>
                            <select id="medio_alumn">
                                <option value="0"></option>
                                <?php
                                foreach ($DaoMediosEnterar->showAll() as $medio) {
                                    ?>
                                    <option value="<?php echo $medio->getId() ?>" <?php if ($Id_medio_ent == $medio->getId()) { ?> selected="selected"<?php } ?>><?php echo $medio->getMedio() ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </li>
                        <li><span class="requerido">*</span>Prioridad<br>
                            <select id="priodidad">
                                <option value="0"></option>
                                <option value="1" <?php if ($Seguimiento_ins == 1) { ?> selected="selected"<?php } ?>>Alta</option>
                                <option value="2" <?php if ($Seguimiento_ins == 2) { ?> selected="selected"<?php } ?>>Media</option>
                                <option value="3" <?php if ($Seguimiento_ins == 3) { ?> selected="selected"<?php } ?>>Baja</option>
                            </select>
                        </li>
                        <li>Ultimo Grado de Estudios<br>
                            <select id="escolaridad">
                                <option value="0"></option>
                                <option value="1" <?php if ($UltGrado_est == 1) { ?> selected="selected"<?php } ?>>Primaria</option>
                                <option value="2" <?php if ($UltGrado_est == 2) { ?> selected="selected"<?php } ?>>Secundaria</option>
                                <option value="3" <?php if ($UltGrado_est == 3) { ?> selected="selected"<?php } ?>>Preparatoria</option>
                                <option value="4" <?php if ($UltGrado_est == 4) { ?> selected="selected"<?php } ?>>Universidad</option>
                                <option value="5" <?php if ($UltGrado_est == 5) { ?> selected="selected"<?php } ?>>Posgrado</option>
                            </select>
                        </li>
                        <li>Escuela de procedencia<br><input type="text" id="escula_pro" value="<?php echo $EscuelaPro ?>"/></li>
                    </ul>
                </div>
                <div class="seccion">
                    <h2>Ofertas del alumno</h2>
                    <span class="linea"></span>
                    <?php
                    if ($Id_alum > 0) {
                        //Ofertas Activas
                        if (count($DaoOfertasAlumno->getOfertasAlumno($Id_alum)) > 0) {
                            foreach ($DaoOfertasAlumno->getOfertasAlumno($Id_alum) as $ofertaAlumno) {
                                $primerCicloOfertaAlumno=$DaoCiclosAlumno->getPrimerCicloOferta($ofertaAlumno->getId());
                                $ultimoCicloOfertaAlumno=$DaoCiclosAlumno->getLastCicloOferta($ofertaAlumno->getId());
                                $grado = $DaoOfertasAlumno->getLastGradoOferta($ofertaAlumno->getId());
                                $oferta = $DaoOfertas->show($ofertaAlumno->getId_ofe());
                                $esp = $DaoEspecialidades->show($ofertaAlumno->getId_esp());
                                $nombre_ori="";
                                if ($ofertaAlumno->getId_ori() > 0) {
                                    $ori = $DaoOrientaciones->show($ofertaAlumno->getId_ori());
                                    $nombre_ori = $ori->getNombre();
                                }

                                $primer_ciclo="";
                                if($primerCicloOfertaAlumno->getId_ciclo()>0){
                                   $ciclo=$DaoCiclos->show($primerCicloOfertaAlumno->getId_ciclo());
                                   $primer_ciclo=$ciclo->getClave();
                                }
                                
                                $ultimo_ciclo="";
                                if($ultimoCicloOfertaAlumno->getId_ciclo()>0){
                                   $ciclo=$DaoCiclos->show($ultimoCicloOfertaAlumno->getId_ciclo());
                                   $ultimo_ciclo=$ciclo->getClave();
                                }
                                
                                $beca = $ultimoCicloOfertaAlumno->getPorcentaje_beca();
                                if ($ultimoCicloOfertaAlumno->getPorcentaje_beca() == NULL) {
                                    $beca = 0;
                                }
                                
                                $Opcion_pago = "";
                                if ($ofertaAlumno->getOpcionPago() == 1) {
                                    $Opcion_pago = "Plan por materias";
                                } else {
                                    $Opcion_pago = "Plan completo";
                                }
                                $tur = $DaoTurnos->show($ofertaAlumno->getTurno());
                                $turno=$tur->getNombre();

                                ?>
                                <div class="seccion">
                                    <table  class="info_alumn">
                                        <tbody>
                                        <td>
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <th><font>Carrera:</font></th>
                                                        <td colspan="4"><font><?php echo $esp->getNombre_esp(); ?></font></td>
                                                        <td><b style="color:red">Activo</b></td>
                                                    </tr>
                                                    <tr>
                                                        <th><font>Orientaci&oacute;n:</font></th>
                                                        <td><font color="navy" face="arial" size="2"><?php echo $nombre_ori; ?></font></td>
                                                        <th><font>Opci&oacute;n de pago</font></th>
                                                        <td><font color="red" face="arial" size="2"><?php echo $Opcion_pago; ?></font></td>
                                                        <th><font>Num. PAGOS</font></th>
                                                        <td><font color="navy" face="arial" size="2"><?php echo $esp->getNum_pagos(); ?></font></td>
                                                    </tr>
                                                    <tr>
                                                        <th ><font>PRECIO</font></th>
                                                        <td><font>$<?php echo number_format($esp->getPrecio_curso(), 2) ?></font></td>
                                                        <th ><font>INSCRIPCIÓN</font></th>
                                                        <td><font>$<?php echo number_format($esp->getInscripcion_curso(), 2); ?></font></td>
                                                        <th ><font>MENSUALIDAD:</font></th>
                                                        <td colspan="5"><font>$<?php echo number_format(($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos(), 2); ?></font></td>
                                                    </tr>
                                                    <tr>
                                                        <th><font>Nivel:</font></th>
                                                        <td><font><?php echo $oferta->getNombre_oferta(); ?></font></td>
                                                        <th><font>Admisi&oacute;n:</font></th>
                                                        <td><?php echo $primer_ciclo; ?></font></td>
                                                        <th><font>Último ciclo:</font></th>
                                                        <td><font><?php echo $ultimo_ciclo; ?></font></td>
                                                    </tr>
                                                    <tr>
                                                        <th><font>Grado actual:</font></th>
                                                        <td><font><?php echo $grado['Grado'] ?></font></td>
                                                        <th><font>Turno:</font></th>
                                                        <td><font><?php echo $turno ?></font></td>
                                                        <th><font>Beca:</font></th>
                                                        <td><font><?php echo $beca; ?>%</font></td>
                                                    </tr>
                                                    <tr>
                                                        <th><font>Acciones:</font></th>
                                                        <td style="width:140px;"><font>
                                                            <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                                                            <div class="box-buttom">
                                                                <?php
                                                                //Obtenemos el primer ciclo de la oferta
                                                                $ciclosAlumno=$DaoCiclosAlumno->getCiclosOferta($ofertaAlumno->getId());
                                                                if(count($ciclosAlumno)>0){
                                                                    //Verificar si ya pago la inscripcion
                                                                    $ExistePagoInscripcion=$DaoPagosCiclo->verificarPagoInscripcion($ofertaAlumno->getId());

                                                                    //Obtenemos los pagos del alumno del primer ciclo
                                                                    $totalRows_Pagos_ciclo=0;
                                                                    foreach($DaoPagosCiclo->getCargosCiclo($ciclosAlumno[0]->getId()) as $Cargos){
                                                                        $totalRows_Pagos_ciclo++;
                                                                    }
                                                                    //SI no pago la inscripcion
                                                                    if ($ExistePagoInscripcion == 0) {
                                                                        ?>
                                                                        <p><button onclick="generar_pdf_insc(<?php echo $Id_alum; ?>,<?php echo $ofertaAlumno->getId() ?>)">Formato de pago</button></p>
                                                                        <?php
                                                                    }
                                                                    //Es decir si ya pago la inscripcion y no se registrado todavia los demas pagos
                                                                    //Es decir si es una nueva oferta
                                                                    if ($ExistePagoInscripcion == 1 && $totalRows_Pagos_ciclo == 1 && $ofertaAlumno->getOpcionPago() == 2) {
                                                                        ?>
                                                                        <p><button onclick="inscribir(<?php echo $ofertaAlumno->getId() ?>)">Inscribir</button></p>
                                                                        <?php
                                                                    }
                                                                }
                                                                if($oferta->getTipoOferta()==1){
                                                                  ?>
                                                                    <p><a href="pago.php?id=<?php echo $ofertaAlumno->getId() ?>"><button>Mostrar lista de pagos</button></a></p>
                                                                    <?php     
                                                                }elseif($oferta->getTipoOferta()==2){
                                                                    ?>                                                           
                                                                    <p><a href="cargos_curso.php?id=<?php echo $ofertaAlumno->getId() ?>"><button>Mostrar lista de pagos</button></a></p>
                                                                    <?php      
                                                                }

                                                                if (isset($perm['72'])) {
                                                                ?>
                                                                <p><button onclick="mostrar_cambiar_plan(<?php echo $ofertaAlumno->getId(); ?>)">Cambiar plan de pago</button></p>
                                                                <?php
                                                                }
                                                                if (isset($perm['70'])) {
                                                                ?>
                                                                <p><button onclick="mostrar_cambiar_oferta(<?php echo $ofertaAlumno->getId(); ?>)">Cambio de oferta</button></p>
                                                                <?php
                                                                }
                                                                ?>
                                                                <p><a href="contrato_servicios.php?id=<?php echo $Id_alum ?>&id_ofe=<?php echo $ofertaAlumno->getId(); ?>" class="link" target="_blank"><button>Descargar contrato</button></a></p>
                                                                <?php
                                                                if (isset($perm['71'])) {
                                                                    ?>
                                                               
                                                                <p><button onclick="mostrar_box_baja(<?php echo $ofertaAlumno->getId() ?>)">Dar de baja oferta</button></p>
                                                                <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            </font>
                                                        </td> 
                                                        <td>
                                                            <font color="red"></font>
                                                        </td>
                                                    </tr>

                                                </tbody></table>
                                        </td></tr>
                                        </tbody>
                                    </table>
                                </div>

                                <?php
                            }
                        }
                        
                        //Ofertas egresadas
                        if (count($DaoOfertasAlumno->getOfertasEgresadasAlumno($Id_alum)) > 0) {
                           
                           foreach ($DaoOfertasAlumno->getOfertasEgresadasAlumno($Id_alum) as $ofertaAlumno) {
                                $primerCicloOfertaAlumno=$DaoCiclosAlumno->getPrimerCicloOferta($ofertaAlumno->getId());
                                $ultimoCicloOfertaAlumno=$DaoCiclosAlumno->getLastCicloOferta($ofertaAlumno->getId());
                                $grado = $DaoOfertasAlumno->getLastGradoOferta($ofertaAlumno->getId());
                                $oferta = $DaoOfertas->show($ofertaAlumno->getId_ofe());
                                $esp = $DaoEspecialidades->show($ofertaAlumno->getId_esp());
                                $nombre_ori="";
                                if ($ofertaAlumno->getId_ori() > 0) {
                                    $ori = $DaoOrientaciones->show($ofertaAlumno->getId_ori());
                                    $nombre_ori = $ori->getNombre();
                                }
                                $primer_ciclo="";
                                if($primerCicloOfertaAlumno->getId_ciclo()>0){
                                   $ciclo=$DaoCiclos->show($primerCicloOfertaAlumno->getId_ciclo());
                                   $primer_ciclo=$ciclo->getClave();
                                }
                                
                                $ultimo_ciclo="";
                                if($ultimoCicloOfertaAlumno->getId_ciclo()>0){
                                   $ciclo=$DaoCiclos->show($ultimoCicloOfertaAlumno->getId_ciclo());
                                   $ultimo_ciclo=$ciclo->getClave();
                                }

                                $beca = $ultimoCicloOfertaAlumno->getPorcentaje_beca();
                                if ($ultimoCicloOfertaAlumno->getPorcentaje_beca() == NULL) {
                                    $beca = 0;
                                }
                                $Opcion_pago = "";
                                if ($ofertaAlumno->getOpcionPago() == 1) {
                                    $Opcion_pago = "Plan por materias";
                                } else {
                                    $Opcion_pago = "Plan completo";
                                }
                                $tur = $DaoTurnos->show($ofertaAlumno->getTurno());
                                $turno=$tur->getNombre();
                                ?>
                                <div class="seccion">
                                    <table  class="info_alumn oferta-egresada">
                                        <tbody>
                                        <td>
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <th><font>Carrera:</font></th>
                                                        <td colspan="4"><font><?php echo $esp->getNombre_esp(); ?></font></td>
                                                        <td><b style="color:red">Egresado</b></td>
                                                    </tr>
                                                    <tr>
                                                        <th><font>Orientaci&oacute;n:</font></th>
                                                        <td><font color="navy" face="arial" size="2"><?php echo $nombre_ori; ?></font></td>
                                                        <th><font>Opci&oacute;n de pago</font></th>
                                                        <td><font color="red" face="arial" size="2"><?php echo $Opcion_pago; ?></font></td>
                                                        <th><font>Num. PAGOS</font></th>
                                                        <td><font color="navy" face="arial" size="2"><?php echo $esp->getNum_pagos(); ?></font></td>
                                                    </tr>
                                                    <tr>
                                                        <th ><font>PRECIO</font></th>
                                                        <td><font>$<?php echo number_format($esp->getPrecio_curso(), 2) ?></font></td>
                                                        <th ><font>INSCRIPCIÓN</font></th>
                                                        <td><font>$<?php echo number_format($esp->getInscripcion_curso(), 2); ?></font></td>
                                                        <th ><font>MENSUALIDAD:</font></th>
                                                        <td colspan="5"><font>$<?php echo number_format(($esp->getPrecio_curso() / $esp->getNum_ciclos()) / $esp->getNum_pagos(), 2); ?></font></td>
                                                    </tr>
                                                    <tr>
                                                        <th><font>Nivel:</font></th>
                                                        <td><font><?php echo $oferta->getNombre_oferta(); ?></font></td>
                                                        <th><font>Admisi&oacute;n:</font></th>
                                                        <td><?php echo $primer_ciclo; ?></font></td>
                                                        <th><font>Último ciclo:</font></th>
                                                        <td><font><?php echo $ultimo_ciclo; ?></font></td>
                                                    </tr>
                                                    <tr>
                                                        <th><font>Grado actual:</font></th>
                                                        <td><font><?php echo $grado['Grado'] ?></font></td>
                                                        <th><font>Turno:</font></th>
                                                        <td><font><?php echo $turno ?></font></td>
                                                        <th><font>Beca:</font></th>
                                                        <td><font><?php echo $beca; ?>%</font></td>
                                                    </tr>
                                                    <tr>
                                                        <th><font>Acciones:</font></th>
                                                        <td style="width:140px;"><font>
                                                            <span onclick="mostrar_botones(this)" class="mostrar-opc">Mostrar</span>
                                                            <div class="box-buttom">
                                                                <?php
                                                                  if($oferta->getTipoOferta()==1){
                                                                  ?>
                                                                    <p><a href="pago.php?id=<?php echo $ofertaAlumno->getId() ?>"><button>Mostrar lista de pagos</button></a></p>
                                                                    <?php     
                                                                }elseif($oferta->getTipoOferta()==2){
                                                                    ?>                                                           
                                                                    <p><a href="cargos_curso.php?id=<?php echo $ofertaAlumno->getId() ?>"><button>Mostrar lista de pagos</button></a></p>
                                                                    <?php      
                                                                }
                                                                ?>
                                                            </div>
                                                            </font>
                                                        </td> 
                                                        <td>
                                                            <font color="red"></font>
                                                        </td>
                                                    </tr>

                                                </tbody></table>
                                        </td></tr>
                                        </tbody>
                                    </table>
                                </div>
                                <?php
                            } 
                        }
                    }
                    
                    ?>
                </div>
                <div id="datosUsu">
                    <p><b>Fecha:</b>   <?php echo date('Y-m-d H:i:s')?></p>
                    <p><b>Usuario:</b> <?php echo $_usu->getNombre_usu() . " " . $_usu->getApellidoP_usu() . " " . $_usu->getApellidoM_usu() ?></p>
                </div>
                <button class="btns btns-primary btns-lg" id="button_ins" onclick="save_alum()">Guardar</button>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="interesado.php" class="link">Nuevo</a></li>
                    <?php
                    if ($Id_alum > 0) {
                        if(count($DaoOfertasAlumno->getOfertasAlumno($Id_alum))>0){
                            if (isset($perm['35'])) {
                                ?>
                                <li><a href="cobranza.php?id=<?php echo $Id_alum?>" class="link">Cobrar</a></li>
                                <?php
                            }
                            if (isset($perm['32'])) {
                                ?>
                                <li><span onclick="box_documents()">Documentos</span></li>
                                <?php
                            }
                        }
                    }
                    if ($Id_alum > 0) {
                        ?>
                        <li><span onclick="mostrarFinder()">A&ntilde;adir fotografia</span></li>
                        <li><span onclick="box_curso()">A&ntilde;adir Oferta</span></li>
                        <li><span onclick="box_tutor()">A&ntilde;adir tutor</span></li>
                        
                        <li><span onclick="box_curso_interes()">A&ntilde;adir Oferta de inter&eacute;s</span></li>
                        <?php
                        if (isset($perm['36'])) {
                            ?>
                            <li><a href="aplicar_becas.php?id=<?php echo $Id_alum ?>" class="link">Becas</a></li>
                            <li><span onclick="imprimir()">Imprimir solicitud</span></li>
                            <?php
                        }
                    }
                    ?>
                </ul>
                <input type="file" id="files" name="files" multiple="">
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_alumn" value="<?php echo $Id_alum ?>"/>
<input type="hidden" id="Id_dir" value="<?php echo $Id_dir ?>"/>
<?php
write_footer();
