<?php
require_once('estandares/includes.php');
require_once('clases/DaoCiclos.php');
links_head("Grupos del alumno   ");
write_head_body();
write_body();
?>

<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Informaci&oacute;n del alumno</h1>
                </div>
                <span class="spanfiltros" onclick="mostrar_filtro()">Mostrar filtros</span>
                <!--<span class="spanEnviar" onclick="mostrar_box_email()">Enviar por email</span>-->
                <div id="mascara_tabla">

                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h1>Filtros</h1>
    <div class="boxUlBuscador">
        <p>Alumno<br><input type="search"  class="buscarFiltro" onkeyup="buscarAlum()" placeholder="Nombre"/></p>
        <ul class="Ulbuscador"></ul>
    </div>
    <p>Oferta<br>
          <select id="Id_ofe_alum" onchange="getCiclosOferta()">
          <option value="0"></option>
        </select>
    </p>
    <p>Ciclo<br>
      <select id="ciclo">
          <option value="0"></option>
        </select>
    </p>
    <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();
