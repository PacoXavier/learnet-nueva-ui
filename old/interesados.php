<?php
require_once('estandares/includes.php');                              
if(!isset($perm['30'])){
  header('Location: home.php');
}
require_once('estandares/class_medios_enterar.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
links_head("Interesados  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-users" aria-hidden="true"></i> Interesados</h1>
                </div>
                <ul class="form">
                    <li>Buscar interesado<br><input type="search"  id="buscar" onkeyup="buscarInt()" /></li>
                </ul>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Filtros</li>
                        <li onclick="mostrar_box_email()"><i class="fa fa-envelope"></i> Email</li>
                        <li onclick="mostrar_box_masivo()"><i class="fa fa-keyboard-o" aria-hidden="true"></i> Seguimiento masivo</li>
                        <li onclick="download_excel()"><i class="fa fa-download"></i> Descargar</li>
                    </ul>
                </div>
                <ul id="list-colors">
                    <li style="margin-bottom: 3px;"><b>Prioridad:</b></li>
                    <li><div id="alta"></div>Alta</li>
                    <li><div id="media"></div>Media</li>
                    <li><div id="baja"></div>Baja y Nula</li>
                </ul>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Id</td>
                                <td>Nombre</td>
                                <td>Oferta</td>
                                <td style="width: 140px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td style="width: 50px;">Alta</td>
                                <td>Email</td>
                                <td>Cel.</td>
                                <td>Prioridad</td>
                                <td>Seguimiento</td>
                                <td>Contactar</td>
                                <td><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                                <td>Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                             foreach($DaoAlumnos->getInteresados() as $k=>$v){
                                    if($v['Id_ofe']>0){
                                       $alum = $DaoAlumnos->show($v['Id_ins']);
                                       if($v['Id_ofe']>0){
                                       $nombre_ori="";

                                       $oferta = $DaoOfertas->show($v['Id_ofe']);
                                       $esp = $DaoEspecialidades->show($v['Id_esp']);
                                       if ($v['Id_ori'] > 0) {
                                          $ori = $DaoOrientaciones->show($v['Id_ori']);
                                          $nombre_ori = $ori->getNombre();
                                        }
                                        
                                        $color="";
                                        $Seguimiento_ins = "";
                                        if ($v['Seguimiento_ins'] == 1) {
                                          $Seguimiento_ins = "Alta";
                                          $color="green";
                                        } elseif ($v['Seguimiento_ins'] == 2) {
                                          $Seguimiento_ins = "Media";
                                          $color="yellow";
                                        } elseif ($v['Seguimiento_ins'] == 3) {
                                          $Seguimiento_ins = "Baja";
                                          $color="pink";
                                        } elseif ($v['Seguimiento_ins'] == 4) {
                                          $Seguimiento_ins = "NULA";
                                          $color="pink";
                                        }

                                         $query_log  = "SELECT * FROM logs_inscripciones_ulm WHERE Idrel_log=".$v['Id_ins']." AND Tipo_relLog='inte' ORDER BY Id_log DESC LIMIT 1";
                                         $log = mysql_query($query_log, $cnn) or die(mysql_error());
                                         $row_log = mysql_fetch_array($log);
                                       }
                                      ?>
                                             <tr id_alum="<?php echo $alum->getId();?>" class="<?php echo $color;?>">
                                                <td  onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getId(); ?></td>
                                                <td onclick="mostrar(<?php echo $alum->getId(); ?>)" style="width: 115px;"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
                                                <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $oferta->getNombre_oferta(); ?></td>
                                                <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $esp->getNombre_esp(); ?></td>
                                                <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $nombre_ori; ?></td>
                                                <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getFecha_ins(); ?></td>
                                                <td style="word-break: break-all;width: 130px;"><?php echo $alum->getEmail(); ?></td>
                                                <td onclick="mostrar(<?php echo $alum->getId(); ?>)"><?php echo $alum->getCel(); ?></td>
                                                <td onclick="mostrar(<?php echo $alum->getId(); ?>)" class="prioridad"><?php echo $Seguimiento_ins; ?></td>
                                                <td><div class="box_seguimiento"><?php echo $row_log['Comen_log']?></div></td>
                                                <td class="contactar"><?php echo $row_log['Dia_contactar']; ?></td>
                                                <td><input type="checkbox"> </td>
                                                <td>
                                                    <button onclick="mostrar_box_comentario(<?php echo $alum->getId(); ?>)">Seguimiento</button>
                                                </td>
                                              </tr>
                                              <?php
                                    }
                             }
                                  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
        <div class="boxUlBuscador">
            <p>Usuario que capturo<br><input type="search"  class="buscarFiltro" onkeyup="buscarUsu()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p><input type="radio" name="seguimiento" id="todos"/> Todos <br><input type="radio" id="conseguimiento" name="seguimiento"/> con seguimiento <input type="radio" name="seguimiento" id="SinSeguimiento"/> sin seguimiento</p>
	<p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso();">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
        <p>Medio por el que se entero:<br>
            <select id="medio_entero" >
              <option value="0"></option>
              <?php
                $class_medios_enterar= new class_medios_enterar();
                foreach($class_medios_enterar->get_medios() as $k2=>$v2){
                    ?>
                    <option value="<?php echo $v2['Id_medio'] ?>"><?php echo $v2['Medio'] ?></option>
                    <?php  
                }
                ?>
            </select>
        </p>
        <p>Prioridad:<br>
            <select id="priodidad" >
              <option value="0"></option>
              <option value="1">Alta</option>
              <option value="2">Media</option>
              <option value="3">Baja</option>
            </select>
        </p>
        <p>Fecha inicio<br><input type="date"  id="fecha_ini"/></p>
        <p>Fecha fin<br><input type="date"  id="fecha_fin"/></p>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>

<script src="js/ckeditor/ckeditor.js"></script>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();



