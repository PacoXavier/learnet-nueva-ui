<?php
require_once('Connections/cnn.php');
require_once('estandares/class_bdd.php');
require_once('estandares/class_usuarios.php');
require_once('estandares/class_docentes.php');
require_once('estandares/class_activos.php');


if (!function_exists("GetSQLValueString")) {

    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
        $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

        $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }

}

mysql_select_db($database_cnn, $cnn);
mysql_query("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');




if ($_POST['action']=="box_captura"){
?>
<div id="box_emergente"><h1>Capturar mantenimiento</h1>
    <span class="linea"></span>
        <p>Estado del activo<br><input type="text" id="estado"/></p>
        <p>Comentarios<br><textarea id="comentarios"></textarea></p>
    <p><button onclick="save_mantenimiento()">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
</div>
<?php
}




if ($_POST['action'] == "save_mantenimiento") {
    if($_COOKIE['admin/Tipo']=="usu"){
        $Tipo=1;
    }else{
        $Tipo=2;
    }
    $sql_nueva_entrada = sprintf("INSERT INTO Historial_activo (Id_activo,Estado, Comentarios, Id_usu, Tipo_usu, Fecha_hist) VALUES (%s, %s,%s, %s,%s,%s);", 
            GetSQLValueString($_POST['Id_activo'], "int"), 
            GetSQLValueString($_POST['estado'], "text"), 
            GetSQLValueString($_POST['comentarios'], "text"),
            GetSQLValueString($_COOKIE['admin/Id_usu'], "int"), 
            GetSQLValueString($Tipo, "text"), 
            GetSQLValueString(date('Y-m-d  H:i:s'), "date"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    
    update_historial($_POST['Id_activo']);
}


if($_POST['action']=="delete_hist"){
	$sql_nueva_entrada=sprintf("DELETE FROM  Historial_activo  WHERE Id_hist=%s",
	   	GetSQLValueString($_POST['Id_hist'], "int"));
	   	$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());	
           update_historial($_POST['Id_activo']);
}


function update_historial($Id_activo){
    global $cnn, $database_cnn;

        $class_activos = new class_activos($Id_activo);
        $activo=$class_activos->get_activo();
        $count=1;
        foreach ($activo['Historial'] as $k => $v) {

            if($v['Tipo_usu']==1){
                 $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                 $user=$class_usuarios->get_usu();
                 $nombre=$user['Nombre_usu']." ".$user['ApellidoP_usu']." ".$user['ApellidoM_usu'];
            }else{
                 $class_docentes = new class_docentes($_COOKIE['admin/Id_usu']);
                 $docente=$class_docentes->get_docente();
                 $nombre=$docente['Nombre_docen']." ".$docente['ApellidoP_docen']." ".$docente['ApellidoM_docen']; 
            }
            ?>
                            
                                <tr>
                                    <td><?php echo $count ?></td>
                                    <td><?php echo $nombre ?></td>
                                    <td><?php echo $v['Fecha_hist'] ?></td>
                                     <td><?php echo $v['Estado'] ?></td>
                                    <td><?php echo $v['Comentarios'] ?></td>
                                    <td><button onclick="delete_hist(<?php echo $v['Id_hist'] ?>)">Eliminar</button></td>
                                </tr>
                                <?php
            $count++;
        }
}



