<?php
require_once('estandares/includes.php');
require_once('require_daos.php');
$DaoCiclos= new DaoCiclos();
$DaoDocentes= new DaoDocentes();
$DaoPermutaClase= new DaoPermutasClase();
$DaoUsuarios= new DaoUsuarios();
$DaoGrupos= new DaoGrupos();
$DaoHoras= new DaoHoras();
$DaoAulas= new DaoAulas();

links_head("Permuta de clases  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-random"></i> Permuta de clases</h1>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <?php
                        $id=0;
                        $nombre="";
                        if ($_REQUEST['id'] > 0) {
                            $docente = $DaoDocentes->show($_REQUEST['id']);
                            $id=$_REQUEST['id'];
                            $nombre=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
                        }
                        ?>
                        <li>Buscar Docente<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o clave" value="<?php echo $nombre ?>"/>
                            <ul id="buscador_int"></ul>
                        </li><br>
                        <?php
                        if ($_REQUEST['id'] > 0) {
                        ?>
                        <li>Ciclo<br>
                            <select id="Id_ciclo" onchange="getPermutas()">
                                <option value="0">Selecciona un ciclo</option>
                              <?php
                              foreach($DaoCiclos->showAll() as $k=>$v){
                              ?>
                               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave() ?></option>
                              <?php
                              }
                              ?>

                            </select></li>
                            <?php
                         }
                        ?>
                    </ul>
                    <div id="box-info"></div>
                </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
            </div>
        </td>
    </tr>
</table>
<?php
$ciclo=$DaoCiclos->getActual();
?>
<input type="hidden" id="Id_docente" value="<?php echo $id; ?>"/>
<input type="hidden" id="FechaIniCiclo" value="<?php echo $ciclo->getFecha_ini(); ?>"/>
<input type="hidden" id="FechaFinCiclo" value="<?php echo $ciclo->getFecha_fin(); ?>"/>
<div class="box-dias-ciclo"></div>
<?php
write_footer();


