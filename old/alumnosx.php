<?php
require_once('Connections/cnn.php');
require_once('estandares/includes.php');

$pagina=$_SERVER['SCRIPT_FILENAME'];
$pagina=substr($pagina,0, strpos($pagina,".php"));
while(strpos($pagina,"/")!== false){
	$pagina=substr($pagina, strpos($pagina,"/")+1);
}

mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");
date_default_timezone_set('America/Mexico_City');

/*
 if(!$_COOKIE['Id_usu']>0 && $_SERVER['SERVER_PORT']!=="8888" && $pagina!=="index"){
header('Location: index.php');
}
*/

links_head("Alumnos  ");
write_head_body();
write_body();
?>
<div class="fondo">
	<h1>Alumnos</h1>
	<div class="box_buscador"><input type="text" placeholder="Buscar alumno"></div>
	<table id="list_usu">
	
	<thead>
		<tr>
			<td>Foto</td>
			<td>Matricula</td>
			<td>Nombre</td>
			<td>Tel/Cel</td>
			<td>Email</td>
			<td>Estatus</td>
			<td>Plantel</td>
			<td>Fecha de ingreso</td>
			<td>Fecha de baja</td>
		</tr>
	</thead>
	   <tbody>
	   <?php
	   for($i=0;$i<10;$i++){
	   ?>
	     <tr>
		     <td><div class="fotousuario" style="background-image:url(https://lh5.googleusercontent.com/-a3TtCe-0SCQ/AAAAAAAAAAI/AAAAAAAAJCU/IM0csRFNFUo/photo.jpg)"></div></td>
		     <td>310332</td>
		     <td>Erick Iván Ramírez Torres</td>
		     <td>36427687</td>
		     <td>erick.rmz@gmail.com</td>
		     <td>Activo</td>
		     <td>Universidad Libre de Música - Matriz</td>
		     <td>30 de Oct, 2013 21:27:59.</td>
		     <td>30 de Oct, 2013 21:27:59.</td>
	     </tr>
	   <?php
	   }
	   ?>
	   </tbody>
	</table>
</div>
<div id="boxfil">
	<h1>Filtros</h1>
	<p>Estatus<br><select>
		<option></option>
		<option></option>
		<option></option>
		<option></option>
		<option></option>
	   </select>
	</p>
	<p>Prioridad<br><select>
		<option></option>
		<option></option>
		<option></option>
		<option></option>
		<option></option>
	   </select>
	</p>
	<p>Nivel Estudios<br><select>
		<option></option>
		<option></option>
		<option></option>
		<option></option>
		<option></option>
	   </select>
	</p>
	<p>Turno<br><select>
		<option></option>
		<option></option>
		<option></option>
		<option></option>
		<option></option>
	   </select>
	</p>
    <p>Foto<br><select>
		<option></option>
		<option></option>
		<option></option>
		<option></option>
		<option></option>
	   </select>
	</p>
	<p>Escolaridad<br><select>
		<option></option>
		<option></option>
		<option></option>
		<option></option>
		<option></option>
	   </select>
	</p>
</div>
<?php
write_footer();

?>
