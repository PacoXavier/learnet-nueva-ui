<?php
require_once('estandares/includes.php');
require_once('clases/DaoEventos.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoAulas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoPagos.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoPagosCiclo.php');
require_once('clases/DaoCiclosAlumno.php');
require_once('clases/modelos/Alumnos.php');
require_once('clases/modelos/OfertasAlumno.php');
require_once('clases/modelos/Ciclos.php');
require_once('clases/modelos/Pagos.php');
require_once('clases/modelos/PagosCiclo.php');
require_once('clases/modelos/CiclosAlumno.php');
require_once('clases/modelos/Eventos.php');
require_once('clases/modelos/Aulas.php');
require_once('clases/modelos/Usuarios.php');
require_once('clases/DaoOfertas.php');

$DaoAulas= new DaoAulas();
$DaoAlumnos= new DaoAlumnos();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoCiclos= new DaoCiclos();
$DaoPagos= new DaoPagos();
$DaoPagosCiclo= new DaoPagosCiclo();
$DaoCiclosAlumno= new DaoCiclosAlumno();
$DaoEventos= new DaoEventos();
$DaoUsuarios= new DaoUsuarios();
$DaoDocentes= new DaoDocentes();
$DaoOfertas= new DaoOfertas();

links_head("Recibos  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-print"></i> Recibos</h1>
                </div>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Filtros</li>
                        <li onclick="download_excel()"><i class="fa fa-download"></i> Descargar</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Folio</td>
                                <td>Ciclo</td>
                                <td>Fecha de pago</td>
                                <td>Nombre</td>
                                <td>Concepto</td>
                                <td>Monto</td>
                                <td>Usuario</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                                $ciclo=$DaoCiclos->getActual();
                                if($ciclo->getId()>0){
                                    $count=1;
                                    /*
                                    $query= "
                                        SELECT Pagos_pagos.* FROM Pagos_pagos
                                               JOIN Pagos_ciclo ON Pagos_pagos.Id_pago_ciclo=Pagos_ciclo.Id_pago_ciclo  
                                               JOIN inscripciones_ulm ON Pagos_ciclo.Id_alum=inscripciones_ulm.Id_ins
                                               JOIN ciclos_alum_ulm ON Pagos_ciclo.Id_ciclo_alum=ciclos_alum_ulm.Id_ciclo_alum
                                               JOIN ofertas_alumno ON ciclos_alum_ulm.Id_ofe_alum=ofertas_alumno.Id_ofe_alum
                                        WHERE Id_ciclo=".$ciclo->getId()." AND Activo_oferta=1 AND inscripciones_ulm.Id_plantel=".$ciclo->Id_plantel."
                                        UNION
                                        SELECT Pagos_pagos.* FROM Pagos_pagos
                                                 JOIN Eventos ON Pagos_pagos.Id_evento=Eventos.Id_event
                                                 JOIN usuarios_ulm ON Eventos.Id_usu=usuarios_ulm.Id_usu
                                        WHERE Id_ciclo=".$ciclo->getId()." AND usuarios_ulm.Id_plantel=".$ciclo->Id_plantel." AND Pagos_pagos.Activo=1
                                        ORDER BY Id_pp DESC";
                                     */
                                    
                                    $query="
                                        SELECT Pagos_pagos.* FROM Pagos_pagos
                                        WHERE Fecha_captura>='".$ciclo->getFecha_ini()."' 
                                            AND Fecha_captura<='".$ciclo->getFecha_fin()."' 
                                            AND Pagos_pagos.Id_plantel=".$ciclo->Id_plantel." 
                                            
                                            ORDER BY Folio DESC";
                                    $total=0;
                                    foreach($DaoPagos->advanced_query($query) as $k=>$v){

                                        if($v->getId_evento()>0){
                                            $resp=$DaoEventos->show($v->getId_evento());
                                            $aula=$DaoAulas->show($resp->getId_salon());
                                            $Nombre=$v->getNombrePaga();
                                            $Concepto="Renta de aula ".$aula->getClave_aula();
                                        }else{
                                            $Concepto=$v->getConcepto();
                                            if($v->getTipoRel()=="alum"){
                                              $alum=$DaoAlumnos->show($v->getIdRel());
                                              $Nombre=$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();   
                                            }elseif($v->getTipoRel()=="docen"){
                                              $docen=$DaoDocentes->show($v->getIdRel());
                                              $Nombre=$docen->getNombre_docen()." ".$docen->getApellidoP_docen()." ".$docen->getApellidoM_docen();     
                                            }elseif($v->getTipoRel()=="usu"){
                                              $u=$DaoUsuarios->show($v->getIdRel());
                                              $Nombre=$u->getNombre_usu()." ".$u->getApellidoP_usu()." ".$u->getApellidoM_usu();    
                                            }
                                        }
                                                                                    
                                        $NombreUsu="";
                                        if($v->getId_usu()>0){
                                            $respUsu=$DaoUsuarios->show($v->getId_usu());
                                            $NombreUsu=$respUsu->getNombre_usu()." ".$respUsu->getApellidoP_usu()." ".$respUsu->getApellidoM_usu();
                                        }
                                        
                                        
                                        $classPrimer="";
                                        $monto=$v->getMonto_pp();
                                        if($v->getMonto_pp()<=0 || $v->getActivo()==0){
                                            $classPrimer='class="pink"'; 
                                            $monto=0;
                                        }
                                    ?>
                                        <tr <?php echo $classPrimer;?>>
                                            <td><?php echo $count?> </td>
                                            <td><?php echo $v->getFolio()?> </td>
                                            <td><?php echo $ciclo->getClave()?></td>
                                            <td><?php echo $v->getFecha_pp()?></td>
                                            <td><?php echo $Nombre;?></td>
                                            <td><?php echo $Concepto;?></td>
                                            <td>$<?php echo number_format($monto,2)?></td>
                                            <td><?php echo $NombreUsu;?></td>
                                        </tr>
                                        <?php
                                        $count++;    
                                        $total+=$monto;
                                    }
                                }
                            ?>
                                   <tr>
                                       <td colspan="6"></td>
                                       <td style="color:red"><b>Total</b></td>
                                       <td style="color:red"><b>$<?php echo number_format($total,2)?> </b></td>
                                    </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>

<div class="boxfil">
	<h1>Filtros</h1>
        <div class="boxUlBuscador">
            <p>Alumno<br><input type="search"  class="buscarFiltro" onkeyup="buscarAlum()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p>Folio<br><input type="search"  id="folio"/></p>
        <p>Activos<input type="checkbox"  id="activo" checked="checked"/> Cancelados<input type="checkbox" checked="checked" id="cancelado"/></p>
        <p>Concepto<br>
          <select id="concepto">
              <option></option>
              <?php
              $query="SELECT * FROM Pagos_pagos GROUP BY ConceptoPago";
              foreach($DaoPagos->advanced_query($query) as $k=>$v){
              ?>
               <option value="<?php echo $v->getConcepto() ?>"><?php echo $v->getConcepto(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p>Fecha inicio<br><input type="date"  id="fecha_ini"/></p>
        <p>Fecha fin<br><input type="date"  id="fecha_fin"/></p>
        <p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso()">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();

