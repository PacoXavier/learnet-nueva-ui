<?php
require_once('estandares/includes.php');
links_head("Alumnos por carreras");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1>Alumnos por Carreras</h1>
                </div>
                <div id="mascara_tabla">
                    <table id="list_usu">
                        <thead>
                            <tr>
                                <td>Carrera</td>
                                <td>Carrera</td>
                                <td>Num. Alumnos</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            for ($i = 0; $i < 100; $i++) {
                                ?>
                                <tr onclick="mostrar(<?php echo $i; ?>)">
                                    <td><img src="images/icon_musica.png" alt="icon_musica" width="16" height="15" /></td>
                                    <td>Pre-Licenciatura</td>
                                    <td>78</td>

                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div id="box_button_mostrar"><span id="mostrar" onclick="mostrar_inte()">Mostrar m&aacute;s</span></div>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <table id="usu_login">
                    <?php
                    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                    $usu = $class_usuarios->get_usu();
                    ?>
                    <tr>
                        <td><div class="nom_usu"><div id="img_usu" 
                                                      <?php if (strlen($usu['Img_usu']) > 0) { ?> style="background-image:url(files/<?php echo $usu['Img_usu'] ?>.jpg) <?php } ?>"></div>
                                                      <?php echo $usu['Nombre_usu'] . " " . $usu['ApellidoP_usu'] . " " . $usu['ApellidoM_usu'] ?>
                                <img src="images/linea_uno.png" alt="linea_uno" width="185" height="5" class="lineas"/></div>
                        </td>
                        <td>
                            <div class="opcion">
                                <a href="perfil.php?id=<?php echo $usu['Id_usu'] ?>">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Mi perfil</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/>
                            </div>
                        </td>
                        <td><div class="opcion">
                                <a href="logout.php">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Salir</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/></div>
                        </td>
                    </tr>
                </table>				     <h2><img src="images/flecha_blue.png" alt="flecha_blue" width="6" height="11" />Opciones Alumnado</h2>
                <ul>
                    <li><a href="interesado.php" class="link">Nuevo</a></li>
                    <li><span onclick="mostrar_box_busqueda()">Buscar</span></li>
                    <li><span onclick="mostrar_reportes()">Reportes</span>
                        <ul id="list_reportes">
                            <li><a href="alumnos.php">Datos generales</a></li>
                            <li><a href="reporte_alumnos_deudores.php">Deudores </a></li>
                            <li><a href="interesados.php">Interesados </a></li>
                            <li><a href="reporte_alumnos_bajas.php">Bajas </a></li>
                            <li><a href="reporte_alumnos_carreras.php">Por carreras</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<?php
write_footer();
