<?php 
if($_REQUEST['id_ofe_alum']>0){
require_once('Connections/cnn.php');
require_once('estandares/Fpdf/fpdf.php'); 
require_once('estandares/Fpdf/fpdi.php'); 
require_once("estandares/QRcode/qrlib.php");

require_once('estandares/class_interesados.php'); 
require_once('estandares/class_ofertas.php'); 
require_once('estandares/class_usuarios.php'); 
require_once('estandares/class_motivos_bajas.php'); 
require_once('estandares/funciones.php');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');

// initiate FPDI 
$pdf = new FPDI(); 

// add a page 
$pdf->AddPage(); 

// set the sourcefile 
$pdf->setSourceFile('estandares/Solicitud_baja.pdf'); 

// import page 1 
$tplIdx = $pdf->importPage(1); 

// use the imported page and place it at point 10,10 with a width of 100 mm 
$pdf->useTemplate($tplIdx); 

//Id_ofe_alum
$class_interesados = new class_interesados();
$Oferta_alum=$class_interesados->get_oferta_alumno($_REQUEST['id_ofe_alum']);

$class_ofertas= new class_ofertas();
$oferta=$class_ofertas->get_oferta($Oferta_alum['Id_ofe']);
$esp = $class_ofertas->get_especialidad($Oferta_alum['Id_esp']);
$nombre_ori="";
if ($Oferta_alum['Id_ori'] > 0) {
  $ori = $class_ofertas->get_orientacion($Oferta_alum['Id_ori']);
  $nombre_ori = " / ".$ori['Nombre_ori'];
}

$turno="";
if($Oferta_alum['Turno']==1){
   $turno="Matutino"; 
}elseif($Oferta_alum['Turno']==2){
   $turno="Vespertino";  
}elseif($Oferta_alum['Turno']==3){
   $turno="Nocturno";  
}

$alum=$class_interesados->get_interesado($Oferta_alum['Id_alum']);

$class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
$usu = $class_usuarios->get_usu();

$class_motivos_bajas = new class_motivos_bajas($Oferta_alum['Id_mot_baja']);
$mot=$class_motivos_bajas->get_motivo_baja();

// now write some text above the imported page 
$pdf->SetFont('Arial','B',9); 
$pdf->SetTextColor(0,0,0); 
$pdf->SetFont('Arial','',9); 
$pdf->SetXY(156,36.5); 
$pdf->Write(10, utf8_decode(date('Y-m-d H:i:s'))); 

$pdf->SetXY(50,47.5); 
$pdf->Write(10, utf8_decode(strtoupper($alum['Nombre_ins']." ".$alum['ApellidoP_ins']." ".$alum['ApellidoM_ins']))); 

$pdf->SetXY(50,52.5); 
$pdf->Write(10, utf8_decode($alum['Matricula'])); 

$pdf->SetXY(50,57.5); 
$pdf->Write(10, utf8_decode(strtoupper($oferta['Nombre_oferta']." / ".$esp['Nombre_esp']." ".$nombre_ori))); 

$pdf->SetXY(50,72.5); 
$pdf->Write(10, utf8_decode(strtoupper($turno))); 

$pdf->SetXY(50,77.5); 
$pdf->Write(10, utf8_decode(strtoupper($usu['Nombre_usu']." ".$usu['ApellidoP_usu']." ".$usu['ApellidoM_usu'])));  

$pdf->SetXY(87,111.5); 
$pdf->Write(10, utf8_decode(formatFecha($Oferta_alum['Baja_ofe']))); 

$pdf->SetXY(30,93); 
$pdf->Write(10, utf8_decode(strtoupper($mot['Nombre'])));

$pdf->SetXY(30,150.5); 
$pdf->Write(10, utf8_decode($Oferta_alum['Comentario_baja']));


$pdf->Output('recibo.pdf', 'I'); 
}