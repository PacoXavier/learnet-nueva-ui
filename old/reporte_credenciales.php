<?php
require_once('estandares/includes.php');
if(!isset($perm['62'])){
  header('Location: home.php');
}
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();

links_head("Generar credenciales  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-camera-retro"></i> Generar credenciales</h1>
                </div>
                <ul class="form">
                </ul>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Mostrar filtros</li>
                        <li onclick="confirmarEnvio()"><i class="fa fa-cloud-download"></i> Generar</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td  style="text-align:center;">Ult. Renovaci&oacute;n</td>
                                <td  style="text-align:center;">Vigencia</td>
                                <td  style="text-align:center;"><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                                <td  style="text-align:center;">Estatus</td>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            //Se mostran alumnos duplicados si es que tienen mas dos ofertas
                             $query = "SELECT * FROM inscripciones_ulm 
                             JOIN ofertas_alumno ON inscripciones_ulm.Id_ins=ofertas_alumno.Id_alum
                             WHERE tipo=1  AND inscripciones_ulm.Id_plantel=".$_usu->getId_plantel()."  AND Activo_oferta=1 AND Baja_ofe IS NULL AND IdCicloEgreso IS NULL ORDER BY Id_ins DESC";
                             foreach($base->advanced_query($query) as $k=>$v){
                                        
                                       if(strlen($v['Img_alum'])>0 && $v['Id_ofe']!=14){
                                           $alum = $DaoAlumnos->show($v['Id_ins']);
                                           $nombre_ori="";

                                           $oferta = $DaoOfertas->show($v['Id_ofe']);
                                           $esp = $DaoEspecialidades->show($v['Id_esp']);
                                           if ($v['Id_ori'] > 0) {
                                              $ori = $DaoOrientaciones->show($v['Id_ori']);
                                              $nombre_ori = $ori->getNombre();
                                            }
                                      
                                            $vigencia="";
                                            if(strlen($v['UltimaGeneracion_credencial'])>0){
                                              $anio=date("Y",strtotime($v['UltimaGeneracion_credencial']));
                                              $mes=date("m",strtotime($v['UltimaGeneracion_credencial']));
                                              $dia=date("d",strtotime($v['UltimaGeneracion_credencial']));
                                              $vigencia=date("Y-m-d",mktime(0, 0, 0, $mes, $dia, $anio+1));
                                            }
                                            $hoy=date("Y-m-d");
                                            $style="background: lightgreen;";
                                            $Status="Vigente";
                                            if($hoy>$vigencia){
                                                $style="background: pink;";
                                                $Status="Renovar";
                                            }
                                          ?>
                                                  <tr id_alum="<?php echo $v['Id_ins'];?>" id_ofe_alum="<?php echo $v['Id_ofe_alum'];?>" id_ofe="<?php echo $v['Id_ofe']?>">
                                                    <td><?php echo $v['Matricula'] ?></td>
                                                    <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                                                    <td><?php echo $oferta->getNombre_oferta(); ?></td>
                                                    <td><?php echo $esp->getNombre_esp(); ?></td>
                                                    <td><?php echo $nombre_ori; ?></td>
                                                    <td style="text-align:center;"><?php echo $v['UltimaGeneracion_credencial']?></td>
                                                    <td style="text-align:center;"><?php echo $vigencia; ?></td>
                                                    <td style="text-align:center;"><input type="checkbox"> </td>
                                                    <td style="text-align:center;<?php echo $style?>"><span><?php echo $Status?></span></td>
                                                  </tr>
                                                  <?php
                                         }
                                    }
                                  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
        <div class="boxUlBuscador">
            <p>Alumno<br><input type="search"  class="buscarFiltro" onkeyup="buscarAlum()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
       <p>Estatus<br>
            <select id="status">
              <option value="0"></option>
              <option value="1">Vigente</option>
              <option value="2">Renovar</option>
            </select>
	</p>
	<p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso();">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();

