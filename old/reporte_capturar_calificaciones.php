<?php
require_once('estandares/includes.php');
if(!isset($perm['60'])){
  header('Location: home.php');
}
require_once('estandares/class_interesados.php');
require_once('estandares/class_materias.php');
require_once('estandares/class_grupos.php');
require_once('estandares/class_ciclos.php');
require_once("estandares/class_justificaciones.php");
links_head("Calificaciones  ");
write_head_body();
write_body();

$query_Grupos = "SELECT * FROM Grupos
JOIN Materias_especialidades ON Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
JOIN materias_uml ON Materias_especialidades.Id_mat=materias_uml.Id_mat WHERE Id_grupo=".$_REQUEST['id'];
$Grupos = mysql_query($query_Grupos, $cnn) or die(mysql_error());
$row_Grupos = mysql_fetch_array($Grupos);

$array_eva=array();
$query_Evaluaciones = "SELECT * FROM Evaluaciones WHERE Id_mat_esp=".$row_Grupos['Id_mat_esp']." ORDER BY Id_eva ASC";
$Evaluaciones = mysql_query($query_Evaluaciones, $cnn) or die(mysql_error());
$row_Evaluaciones= mysql_fetch_array($Evaluaciones);
$totalRows_Evaluaciones = mysql_num_rows($Evaluaciones);
if($totalRows_Evaluaciones>0){
    do{
        $eva=array();
        $eva['Id_eva']=$row_Evaluaciones['Id_eva'];
        $eva['Nombre']=$row_Evaluaciones['Nombre_eva'];
        $eva['Ponderacion']=$row_Evaluaciones['Ponderacion'];
        array_push($array_eva, $eva);
     }while($row_Evaluaciones= mysql_fetch_array($Evaluaciones));
}
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Capturar calificaciones</h1>
                </div>
                <span class="spanfiltros" onclick="save_calificaciones()">Guardar calificaciones</span>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td style="width: 80px">CLAVE GRUPAL</td>
                                <td style="width: 120px">MATERIA</td>
                                <td>TURNO</td>
                                <td>Matricula</td>
                                <td>Nombre</td>
                                <?php 
                                foreach($array_eva as $k=>$v){
                                    ?>
                                     <td class="boxinput"><?php echo $v['Nombre']?></td>
                                <?php
                                }
                                
                               ?>
                                <td class="boxinput">Calificaci&oacute;n<br> Parciales</td>
                                <?php
                                if($row_Grupos['Evaluacion_extraordinaria']==1){
                                ?>
                                 <td class="boxinput">Evaluaci&oacute;n<br> Extraordinaria</td>
                                <?php
                                }
                                if($row_Grupos['Evaluacion_especial']==1){
                                ?>
                               <td class="boxinput">Evaluaci&oacute;n<br> Especial</td>
                                <?php
                                }
                                ?>
                       
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                    $class_grupos= new class_grupos($_REQUEST['id']);
                                    $grupo=$class_grupos->get_grupo();

                                    $class_materias = new class_materias($grupo['Id_mat']);
                                    $mat = $class_materias->get_materia();
                                    $mat_esp=$class_materias->get_esp_mat($grupo['Id_mat_esp']);

                                    $turno="";
                                    if($grupo['Turno']==1){
                                       $turno="MATUTINO";
                                    }if($grupo['Turno']==2){
                                       $turno="VESPERTINO";
                                    }if($grupo['Turno']==3){
                                       $turno="NOCTURNO";
                                    }

                                    foreach($grupo['Alumnos'] as $k=>$v){
                                        if($v['Id_alum']>0){
                                        $class_interesados= new class_interesados($v['Id_alum']);
                                        $alumn=$class_interesados->get_interesado();
                                        
                                        $asis=$class_grupos->get_asistencias_grupo_alum($grupo['Id_grupo'],$v['Id_alum']);
                                        $total= $class_grupos->porcentaje_asistencias($asis['Asistencias'],$asis['Justificaciones'],$asis['Faltas']);
                                        
                                        $colorCalTotalParciales="red";
                                        if($v['CalTotalParciales']>=$mat['Promedio_min']){
                                            $colorCalTotalParciales="green";
                                        }
                                        
                                         $NombreMat=$mat['Nombre'];
                                         if(strlen($mat_esp['NombreDiferente'])>0){
                                            $NombreMat=$mat_esp['NombreDiferente']; 
                                         }

                                    ?>
                                    <tr id-alum="<?php echo $v['Id_alum']?>" id-ciclo-mat="<?php echo $v['Id_ciclo_mat']?>">
                                        <td><?php echo $grupo['Clave']?> </td>
                                        <td style="width: 120px;"><?php echo $NombreMat?></td>
                                        <td><?php echo $turno;?></td>
                                        <td><?php echo $alumn['Matricula']?></td>
                                        <td><?php echo $alumn['Nombre_ins']." ".$alumn['ApellidoP_ins']." ".$alumn['ApellidoM_ins']?></td>
                                        <?php 
                                        
                                        foreach($array_eva as $k2=>$v2){
                                            $query_Calificaciones = "SELECT * FROM Calificaciones  WHERE Id_ciclo_mat=".$v['Id_ciclo_mat']." AND Id_eva=".$v2['Id_eva'];
                                            $Calificaciones = mysql_query($query_Calificaciones, $cnn) or die(mysql_error());
                                            $row_Calificaciones= mysql_fetch_array($Calificaciones);
                                            $totalRows_Calificaciones = mysql_num_rows($Calificaciones);
                                            $calificacion=null;
                                            if($totalRows_Calificaciones>0){
                                                $calificacion=$row_Calificaciones['Calificacion'];
                                            }
                                            ?>
                                             <td class="boxinput parcial"  id-eva="<?php echo $v2['Id_eva']?>" ponderacion="<?php echo $v2['Ponderacion']?>"><input type="text" value="<?php echo number_format($calificacion,2);?>" class="parcialUno"/></td>
                                        <?php
                                        }
                                        ?>
                                         <td class="boxinput" style="text-align: center;color: <?php echo $colorCalTotalParciales;?>;"><?php echo number_format($v['CalTotalParciales'],2)?></td>
                                        <?php
                                        if($v['Evaluacion_extraordinaria']==1){
                                        ?>
                                        <td class="boxinput"><input type="text" value="<?php echo number_format($v['CalExtraordinario'],2)?>" class="Extraordinario"/></td>
                                        <?php
                                        }
                                        if($v['Evaluacion_especial']==1){
                                        ?>
                                        <td class="boxinput"><input type="text" value="<?php echo number_format($v['CalEspecial'],2)?>" class="Especial"/></td>
                                        <?php
                                        }
                                        ?>
                                    </tr>
                                    <?php
                                    }
                                    }
                                    ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_grupo" value="<?php echo $_REQUEST['id'];?>"/>
<?php
write_footer();
