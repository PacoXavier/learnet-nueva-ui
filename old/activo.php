<?php
require_once('estandares/includes.php');
require_once('require_daos.php');

$Id_activo=0;
$Nombre = "";
$Id_img = "";
$Codigo = "";
$Marca = "";
$Modelo = "";
$Num_serie = "";
$Id_aula = "";
$Valor = "";
$Fecha_adq = "";
$Garantia_meses = "";
$Mantenimiento_frecuencia = "";
$Tipo_act = "";
$Descripcion = "";

$DaoActivos = new DaoActivos();
$DaoAulas = new DaoAulas();
$DaoTiposActivo= new DaoTiposActivo();

links_head("Activo  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <?php
    if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
        $activo = $DaoActivos->show($_REQUEST['id']);
        $Nombre = $activo->getNombre();
        $Id_img = $activo->getId_img();
        $Codigo = $activo->getCodigo();
        $Marca = $activo->getMarca();
        $Modelo = $activo->getModelo();
        $Num_serie = $activo->getNum_serie();
        $Id_aula = $activo->getId_aula();
        $Valor = $activo->getValor();
        $Fecha_adq = $activo->getFecha_adq();
        $Garantia_meses = $activo->getGarantia_meses();
        $Mantenimiento_frecuencia = $activo->getMantenimiento_frecuencia();
        $Tipo_act = $activo->getTipo_act();
        $Descripcion = $activo->getDescripcion();
        $Id_activo=$activo->getId();
    }
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <div class="foto_alumno" 
                    <?php if (strlen($Id_img) > 0) { ?> 
                             style="background-image:url(files/<?php echo $Id_img ?>.jpg) <?php } ?>">
                    </div>
                </div>
                <div class="seccion">
                    <h2>Datos del activo</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <?php
                        if ($Id_activo > 0) {
                            ?>
                            <li>C&oacute;digo<br><input type="text" value="<?php echo $Codigo ?>" id="codigo" readonly="readonly"/></li>
                            <?php
                        }
                        ?>
                        <li>Nombre<br><input type="text" value="<?php echo $Nombre ?>" id="nombre"/></li>
                        <li>Marca<br><input type="text" value="<?php echo $Marca ?>" id="marca"/></li>
                        <li>Modelo<br><input type="text" value="<?php echo $Modelo ?>" id="modelo"/></li>
                        <li>Num. Serie<br><input type="text" value="<?php echo $Num_serie ?>" id="serie"/></li>
                        <li>Ubicaci&oacute;n<br>
                            <select id="aula">
                                <option value="0"></option>
                                <?php
                                foreach ($DaoAulas->showAll() as $aula) {
                                    ?>
                                    <option value="<?php echo $aula->getId(); ?>" <?php if ($aula->getId() == $Id_aula) { ?> selected="selected"<?php } ?>><?php echo $aula->getClave_aula(); ?></option>
                                    <?php
                                }
                                ?>

                            </select>
                        </li>
                        <li>Valor Factura<br><input type="text" value="<?php echo $Valor ?>" id="valor_factura"/></li>
                        <li>Fecha de adquisici&oacute;n<br><input type="date" value="<?php echo $Fecha_adq ?>" id="fecha_adquisicion"/></li>
                        <li>Meses de garantia<br><input type="text" id="meses_garantia" value="<?php echo $Garantia_meses ?>"/></li>
                        <li>Frecuencia de mantenimiento<br>
                            <select id="frecuencia">
                                <option value="0"></option>
                                <option value="1" <?php if ($Mantenimiento_frecuencia == 1) { ?> selected="selected"<?php } ?>>Semanal</option>
                                <option value="2" <?php if ($Mantenimiento_frecuencia == 2) { ?> selected="selected"<?php } ?>>Quincenal</option>
                                <option value="3" <?php if ($Mantenimiento_frecuencia == 3) { ?> selected="selected"<?php } ?>>Semestral</option>
                                <option value="4" <?php if ($Mantenimiento_frecuencia == 4) { ?> selected="selected"<?php } ?>>Anual</option>
                            </select></li>
                        <li>Tipo<br>
                            <select id="tipo">
                                <option value="0"></option>
                                <?php
                                foreach($DaoTiposActivo->getTipos() as $tipo){
                                    ?>
                                      <option value="<?php echo $tipo->getId()?>" <?php if ($Tipo_act == $tipo->getId()) { ?> selected="selected"<?php } ?>><?php echo $tipo->getNombre()?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </li>
                        <li>Descripci&oacute;n<br><textarea id="descripcion"><?php echo $Descripcion ?></textarea></li>
                    </ul>
                </div>
                <button id="button_ins" onclick="save_activo()">Guardar</button>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="activo.php" class="link">Nuevo</a></li>
                    <?php
                    if ($Id_activo > 0) {
                        ?>
                        <li><span onclick="mostrarFinder()">Añadir fotografia</span></li>
                        <li><a href="historial_prestamo_activo.php?id=<?php echo $Id_activo; ?>" target="_blank">Historial de prestamos</a></li>
                        <li><a href="historial_activo.php?id=<?php echo $Id_activo; ?>">Historial de mantenimiento</a></li>
                        <li><a href="prestamo_activos.php?id=<?php echo $Id_activo; ?>">Prestamo de activos</a></li>
                        <?php
                    }
                    ?>
                </ul>
                <input type="file" id="files" name="files" multiple="">
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_activo" value="<?php echo $Id_activo ?>"/>
<?php
write_footer();