<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

$DaoDocentes= new DaoDocentes();
$DaoHoras= new DaoHoras();
$DaoCiclos= new DaoCiclos();
$DaoDiasPlantel= new DaoDiasPlantel();

links_head("Horario Docente  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-clock-o" aria-hidden="true"></i> Horario Docente</h1>
                </div>
                
                <span class="spanfiltros" onclick="mostrar_filtro()">Mostrar filtros</span>
                <div class="box-filter-reportes">
                    <ul>
                        <li><div class="box disponibles"></div>Horas disponibles</li>
                        <li><div class="box asignadas"></div>Horas asignadas</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                            <thead>
                                <tr>
                                    <td>Horas</td>
                                    <?php
                                    foreach($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia){
                                        ?>
                                          <td><?php echo $dia['Nombre']?></td>
                                        <?php
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                                  foreach($DaoHoras->showAll() as $hora){    
                                 ?>
                                    <tr id_hora="<?php echo $hora->getId()?>">
                                        <td><?php echo $hora->getTexto_hora()?></td>
                                        <?php
                                        foreach($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia){
                                            ?>
                                              <td><span></span><br></td>
                                            <?php
                                        }
                                        ?>
                                    </tr>
                                 <?php
                                 }
                                 ?>
                            </tbody>
                        </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
        <p>Ciclo<br>
          <select id="ciclo">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave() ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p>Docente<br>
           <select id="docente">
              <option value="0"></option>
              	<?php
                foreach ($DaoDocentes->showAll() as $k => $v) {
                    ?>
                        <option value="<?php echo $v->getId()?>"><?php echo $v->getNombre_docen() . " " . $v->getApellidoP_docen() . " " . $v->getApellidoM_docen() ?></option>
                    <?php
                }
                ?>
            </select>
        </p>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();
