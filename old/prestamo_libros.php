<?php
require_once('estandares/includes.php');
if(!isset($perm['52'])){
  header('Location: home.php');
}

require_once('clases/DaoAulas.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoLibros.php');
require_once('clases/DaoHistorialPrestamoLibro.php');

links_head("Préstamo libros  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-keyboard-o"></i> Préstamo libros</h1>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <li>Buscar Libro<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="T&iacute;tulo o ISBN"/>
                          <ul id="buscador_int"></ul>
                        </li>
                    </ul>
                </div>
        <?php
    $getFecha_entrega="";
    $getFecha_devolucion="";
    $Ubicacion="";  
    $Id_prestamo=0;
    $getTitulo="";
    $getAutor="";
    $getEditorial="";
    $getISBN="";
    if ($_REQUEST['id'] > 0) {
       $DaoLibros= new DaoLibros();
       $libro=$DaoLibros->show($_REQUEST['id']);
       $getTitulo=$libro->getTitulo();
       $getAutor=$libro->getAutor();
       $getEditorial=$libro->getEditorial();
       $getISBN=$libro->getISBN();

        if($libro->getDisponible()==1){
          $disponibilidad="Disponible";
          $style='style="color:green;"';
        }else{
          $disponibilidad="No Disponible";
          $style='style="color:red;"';
          
          $DaoHistorialPrestamoLibro=new DaoHistorialPrestamoLibro();
          $prestamo=$DaoHistorialPrestamoLibro->getLastPrestamo($_REQUEST['id']);
          
          $Id_prestamo=$prestamo->getId();
          $getFecha_entrega=$prestamo->getFecha_entrega();
          $getFecha_devolucion=$prestamo->getFecha_devolucion();
          

          if($prestamo->getTipo_usu()=="usu" && $prestamo->getUsu_recibe()>0){
            $DaoUsuarios= new DaoUsuarios();
            $usuario=$DaoUsuarios->show($prestamo->getUsu_recibe());

            $nombre=$usuario->getNombre_usu()."  ".$usuario->getApellidoP_usu()."  ".$usuario->getApellidoM_usu();
            $tipo_usu="Usuario";
            $codigo=$usuario->getClave_usu();
          }elseif($prestamo->getTipo_usu()=="docen" && $prestamo->getUsu_recibe()>0){
              $DaoDocentes= new DaoDocentes();
              $docente = $DaoDocentes->show($prestamo->getUsu_recibe());
              $nombre=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
              $tipo_usu="Docente";
              $codigo=$docente->getClave_docen();
          }elseif($prestamo->getTipo_usu()=="alum" && $prestamo->getUsu_recibe()>0){
              $DaoAlumnos= new DaoAlumnos();
              $alum = $DaoAlumnos->show($prestamo->getUsu_recibe());
              $nombre=$alum->getNombre()." ".$alum->getApellidoP()." ".$alum->getApellidoM();
              $tipo_usu="Alumno";
              $codigo=$alum->getMatricula();
          }
        }
        $DaoAulas= new DaoAulas();            
        foreach($DaoAulas->showAll() as $k=>$v){
             if ($v->getId() == $libro->getId_aula()) { 
                 $Ubicacion=$v->getClave_aula();
             }
         }
    }
    ?>
              <div class="seccion">
                    <h2>Datos del libro</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>T&iacute;tulo<br><input type="text" value="<?php echo $getTitulo ?>" id="modelo" disabled="disabled" /></li>
                        <li>Autor<br><input type="text" value="<?php echo $getAutor ?>" id="nombre" disabled="disabled"/></li>
                        <li>Editorial<br><input type="text" value="<?php echo $getEditorial ?>" id="nombre" disabled="disabled"/></li>
                        <li>ISBN<br><input type="text" value="<?php echo $getISBN ?>" id="nombre" disabled="disabled"/></li>
                        <li>Categor&iacute;a<br><input type="text" value="" id="nombre" disabled="disabled"/></li>
                        <li>Ubicaci&oacute;n<br><input type="text" value="<?php echo $Ubicacion ?>" id="nombre" disabled="disabled"/></li>
                        <li>Fecha de prestamo<br><input type="date" value="<?php echo $getFecha_entrega ?>" id="Fecha_devolucion" disabled="disabled"/></li>
                        <li>Fecha de devoluci&oacute;n<br><input type="date" id="Fecha_devolucion" value="<?php echo $getFecha_devolucion ?>" disabled="disabled"/></li><br>
                        <li>Disponibilidad<br><input type="text" id="Estado" value="<?php echo $disponibilidad ?>" disabled="disabled" <?php echo $style;?>/></li>
                    </ul>
                </div>
                    <?php
       if ($_REQUEST['id']  > 0 && $libro->getDisponible()==0) {
       ?>
               <div class="seccion">
                        <h2>Datos del usuario actual</h2>
                        <span class="linea"></span>
                        <ul class="form">
                                    <li>Tipo<br><input type="text" id="Nombre_recibe" value="<?php echo $tipo_usu; ?>" disabled="disabled"/></li>
                                    <li>C&oacute;digo <br><input type="text" id="Nombre_recibe" value="<?php echo $codigo; ?>" disabled="disabled"/></li>
                                    <li>Nombre<br><input type="text" id="Nombre_recibe" value="<?php echo $nombre; ?>" disabled="disabled"/></li>
                        </ul>
                    </div>
                            <?php
        }
        ?>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <table id="usu_login">
                    <tr>
                        <td><div class="nom_usu"><div id="img_usu" 
                                                      <?php if (strlen($usu['Img_usu']) > 0) { ?> style="background-image:url(files/<?php echo $usu['Img_usu'] ?>.jpg) <?php } ?>"></div>
                                                      <?php echo $usu['Nombre_usu'] . " " . $usu['ApellidoP_usu'] . " " . $usu['ApellidoM_usu'] ?>
                                <img src="images/linea_uno.png" alt="linea_uno" width="185" height="5" class="lineas"/></div>
                        </td>
                        <td>
                            <div class="opcion">
                                <a href="perfil.php?id=<?php echo $usu['Id_usu'] ?>">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Mi perfil</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/>
                            </div>
                        </td>
                        <td><div class="opcion">
                                <a href="logout.php">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Salir</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/></div>
                        </td>
                    </tr>
                </table>
                <h2><img src="images/flecha_blue.png" alt="flecha_blue" width="6" height="11" />Opciones</h2>
                <ul>
                     <?php 
        if($_REQUEST['id'] > 0) {
                if($libro->getDisponible()==1){
          ?>
                               <li><span onclick="box_prestamo()">Capturar prestamo </span></li>
                         <?php
                }else{
          ?>
                               <li><span onclick="box_recepcion()">Capturar recepci&oacute;n </span></li>
                               <?php
                }
        ?>
                    <li><a href="historial_prestamo_libro.php?id=<?php echo $_REQUEST['id'];?>" target="_blank">Historial de prestamos</a></li>
                    <?php
         }
         ?>
                  
                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_lib" value="<?php echo $_REQUEST['id']; ?>"/>
<input type="hidden" id="Id_prestamo" value="<?php echo $Id_prestamo; ?>"/>
<?php
write_footer();