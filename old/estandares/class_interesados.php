<?php

require_once("class_bdd.php");
require_once("class_ofertas.php");
require_once("class_materias.php");
require_once("class_usuarios.php");
require_once("class_pagos.php");
require_once("class_recargos.php");

class class_interesados extends class_bdd {

    public $Id_int;

    public function __construct($Id_int) {
        class_bdd::__construct();
        if ($Id_int > 0) {
            $this->Id_int = $Id_int;
        }
    }

    public function get_interesados($tipo) {
        $resp = array();
        $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
        $usu = $class_usuarios->get_usu();
        $query_inscripciones_ulm = "SELECT * FROM inscripciones_ulm WHERE tipo=" . $tipo . "  AND Id_plantel=".$usu['Id_plantel']." ORDER BY Id_ins DESC";
        $inscripciones_ulm = mysql_query($query_inscripciones_ulm, $this->cnn) or die(mysql_error());
        $row_inscripciones_ulm = mysql_fetch_array($inscripciones_ulm);
        $totalRows_inscripciones_ulm = mysql_num_rows($inscripciones_ulm);
        if ($totalRows_inscripciones_ulm > 0) {
            do {
                array_push($resp, $this->get_interesado($row_inscripciones_ulm['Id_ins']));
            } while ($row_inscripciones_ulm = mysql_fetch_array($inscripciones_ulm));
        }
        return $resp;
    }

    
    
    
    public function get_interesado($Id_int = null) {
        if ($Id_int == null) {
            $Id_int = $this->Id_int;
        }
        $resp = array();
        $query_inscripciones_ulm = "SELECT * FROM inscripciones_ulm  WHERE Id_ins=" . $Id_int;
        $inscripciones_ulm = mysql_query($query_inscripciones_ulm, $this->cnn) or die(mysql_error());
        $row_inscripciones_ulm = mysql_fetch_array($inscripciones_ulm);
        $totalRows_inscripciones_ulm = mysql_num_rows($inscripciones_ulm);
        if ($totalRows_inscripciones_ulm > 0) {

            $resp['Id_ins'] = $row_inscripciones_ulm['Id_ins'];
            $resp['Nombre_ins'] = $row_inscripciones_ulm['Nombre_ins'];
            $resp['ApellidoP_ins'] = $row_inscripciones_ulm['ApellidoP_ins'];
            $resp['ApellidoM_ins'] = $row_inscripciones_ulm['ApellidoM_ins'];
            $resp['Edad_ins'] = $row_inscripciones_ulm['Edad_ins'];
            $resp['Email_ins'] = $row_inscripciones_ulm['Email_ins'];
            $resp['Tel_ins'] = $row_inscripciones_ulm['Tel_ins'];
            $resp['Cel_ins'] = $row_inscripciones_ulm['Cel_ins'];
            $resp['Id_medio_ent'] = $row_inscripciones_ulm['Id_medio_ent'];
            $resp['Seguimiento_ins'] = $row_inscripciones_ulm['Seguimiento_ins'];
            $resp['Fecha_ins'] = $row_inscripciones_ulm['Fecha_ins'];
            $resp['FechaNac'] = $row_inscripciones_ulm['FechaNac'];
            $resp['Img_alum'] = $row_inscripciones_ulm['Img_alum'];
            $resp['Sexo'] = $row_inscripciones_ulm['Sexo'];
            $resp['Id_dir'] = $row_inscripciones_ulm['Id_dir'];
            $resp['Curp_ins'] = $row_inscripciones_ulm['Curp_ins'];
            $resp['NombreTutor'] = $row_inscripciones_ulm['NombreTutor'];
            $resp['EmailTutor'] = $row_inscripciones_ulm['EmailTutor'];
            $resp['TelTutor'] = $row_inscripciones_ulm['TelTutor'];
            $resp['UltGrado_est'] = $row_inscripciones_ulm['UltGrado_est'];
            $resp['EscuelaPro'] = $row_inscripciones_ulm['EscuelaPro'];
            $resp['Matricula'] = $row_inscripciones_ulm['Matricula'];
            $resp['Alta_alum'] = $row_inscripciones_ulm['Alta_alum'];
            $resp['Baja_alum'] = $row_inscripciones_ulm['Baja_alum'];
            $resp['Activo_alum'] = $row_inscripciones_ulm['Activo_alum'];
            $resp['Pass_alum'] = $row_inscripciones_ulm['Pass_alum'];
            $resp['Comentarios'] = $row_inscripciones_ulm['Comentarios'];
            $resp['Tipo_sangre'] = $row_inscripciones_ulm['Tipo_sangre'];
            $resp['Alergias'] = $row_inscripciones_ulm['Alergias'];
            $resp['Enfermedades_cronicas'] = $row_inscripciones_ulm['Enfermedades_cronicas'];
            $resp['Preinscripciones_medicas'] = $row_inscripciones_ulm['Preinscripciones_medicas'];
            $resp['Contacto'] = array();
            $resp['Contacto'] = $this->get_contacto($row_inscripciones_ulm['Id_ins'], 'alumno');
            //$resp['Grado_alum']=$row_alumno['Grado_alum'];
            //resp['Turno_alum']=$row_alumno['Turno_alum'];
            $resp['RecoverPass'] = $row_inscripciones_ulm['RecoverPass'];
            $resp['LastSession'] = $row_inscripciones_ulm['LastSession'];
            $resp['medio_contacto'] = $row_inscripciones_ulm['medio_contacto'];
            $resp['Refencia_pago'] = $row_inscripciones_ulm['Refencia_pago'];
            $resp['Img_alum'] = $row_inscripciones_ulm['Img_alum'];
            $resp['Ofertas'] = array();
            $resp['Ofertas'] = $this->get_ofertas_alumno($row_inscripciones_ulm['Id_ins']);
            $resp['Logs'] = array();
            $resp['Logs'] = $this->get_logs_int($row_inscripciones_ulm['Id_ins']);
            $resp['Amolestaciones'] = array();
            $resp['Amolestaciones'] = $this->get_amolestaciones_alumno($row_inscripciones_ulm['Id_ins']);
        }
        return $resp;
    }

    public function get_logs_int($Id_int = null) {
        if ($Id_int == null) {
            $Id_int = $this->Id_int;
        }
        $resp = array();
        $query_inscripciones_ulm = "SELECT * FROM logs_inscripciones_ulm WHERE Idrel_log=" . $Id_int . " AND Tipo_relLog='inte' ORDER BY Id_log DESC";
        $inscripciones_ulm = mysql_query($query_inscripciones_ulm, $this->cnn) or die(mysql_error());
        $row_inscripciones_ulm = mysql_fetch_array($inscripciones_ulm);
        $totalRows_inscripciones_ulm = mysql_num_rows($inscripciones_ulm);
        if ($totalRows_inscripciones_ulm > 0) {
            do {
                $class_usuarios = new class_usuarios($row_inscripciones_ulm['Id_usu']);
                $usu = $class_usuarios->get_usu();
                $log = array();
                $log['Id_log'] = $row_inscripciones_ulm['Id_log'];
                $log['Nombre_usu'] = $usu['Nombre_usu'] . " " . $usu['ApellidoP_usu'];
                $log['Fecha_log'] = $row_inscripciones_ulm['Fecha_log'];
                $log['Comen_log'] = $row_inscripciones_ulm['Comen_log'];
                $log['Dia_contactar'] = $row_inscripciones_ulm['Dia_contactar'];
                array_push($resp, $log);
            } while ($row_inscripciones_ulm = mysql_fetch_array($inscripciones_ulm));
        }
        return $resp;
    }

    public function get_ofertas_alumno($Id_alum = null) {
        if ($Id_alum == null) {
            $Id_alum = $this->Id_alum;
        }

        $resp = array();
        $query_ofertas_alumno = "SELECT * FROM ofertas_alumno WHERE Id_alum=" . $Id_alum." AND Activo_oferta=1 AND Baja_ofe IS NULL";
        $ofertas_alumno = mysql_query($query_ofertas_alumno, $this->cnn) or die(mysql_error());
        $row_ofertas_alumno = mysql_fetch_array($ofertas_alumno);
        $totalRows_ofertas_alumno = mysql_num_rows($ofertas_alumno);
        if ($totalRows_ofertas_alumno > 0) {
            do {
                array_push($resp, $this->get_oferta_alumno($row_ofertas_alumno['Id_ofe_alum']));
            } while ($row_ofertas_alumno = mysql_fetch_array($ofertas_alumno));
        }
        return $resp;
    }

    public function get_oferta_alumno($Id_ofe_alum) {
        $resp = array();
        $query_ofertas_alumno = "SELECT * FROM ofertas_alumno WHERE Id_ofe_alum=" . $Id_ofe_alum;
        $ofertas_alumno = mysql_query($query_ofertas_alumno, $this->cnn) or die(mysql_error());
        $row_ofertas_alumno = mysql_fetch_array($ofertas_alumno);
        $totalRows_ofertas_alumno = mysql_num_rows($ofertas_alumno);
        if ($totalRows_ofertas_alumno > 0) {
            do {
                $resp['Id_ofe_alum'] = $row_ofertas_alumno['Id_ofe_alum'];
                $resp['Id_ofe'] = $row_ofertas_alumno['Id_ofe'];
                $resp['Id_alum'] = $row_ofertas_alumno['Id_alum'];
                $resp['Id_esp'] = $row_ofertas_alumno['Id_esp'];
                $resp['Id_ori'] = $row_ofertas_alumno['Id_ori'];
                $resp['beca'] = $row_ofertas_alumno['beca'];
                $resp['Tipo_beca'] = $row_ofertas_alumno['Tipo_beca'];
                $resp['Opcion_pago'] = $row_ofertas_alumno['Opcion_pago'];
                $resp['Turno'] = $row_ofertas_alumno['Turno'];
                $resp['Alta_ofe'] = $row_ofertas_alumno['Alta_ofe'];
                $resp['Baja_ofe'] = $row_ofertas_alumno['Baja_ofe'];
                $resp['Id_mot_baja'] = $row_ofertas_alumno['Id_mot_baja'];
                $resp['Comentario_baja'] = $row_ofertas_alumno['Comentario_baja'];
                $resp['UltimaGeneracion_credencial'] = $row_ofertas_alumno['UltimaGeneracion_credencial'];
                
                $resp['FechaCapturaEgreso'] = $row_ofertas_alumno['FechaCapturaEgreso'];
                $resp['IdCicloEgreso'] = $row_ofertas_alumno['IdCicloEgreso'];
                $resp['IdUsuEgreso'] = $row_ofertas_alumno['IdUsuEgreso'];
                
                $resp['Ciclos_oferta'] = array();
                $resp['Ciclos_oferta'] = $this->get_ciclos_oferta($row_ofertas_alumno['Id_ofe_alum']);
                $resp['Materias_acreditadas'] = array();
                $resp['Materias_acreditadas'] = $this->get_materias_acreditadas($row_ofertas_alumno['Id_ofe_alum']);
                $resp['Pagos_adelantados'] = array();
                $resp['Pagos_adelantados'] = $this->get_pagos_adelantados($row_ofertas_alumno['Id_ofe_alum']);
            } while ($row_ofertas_alumno = mysql_fetch_array($ofertas_alumno));
        }
        return $resp;
    }

    public function get_ciclos_oferta($Id_ofe_alum) {
        $resp = array();
        $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=" . $Id_ofe_alum . " ORDER BY Id_ciclo ASC";
        $ciclos_alum_ulm = mysql_query($query_ciclos_alum_ulm, $this->cnn) or die(mysql_error());
        $row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm);
        $totalRows_ciclos_alum_ulm = mysql_num_rows($ciclos_alum_ulm);
        if ($totalRows_ciclos_alum_ulm > 0) {
            do {
                array_push($resp, $this->get_ciclo_oferta($row_ciclos_alum_ulm['Id_ciclo_alum']));
            } while ($row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm));
        }
        return $resp;
    }

    public function get_ciclo_oferta($Id_ciclo_ulm) {
        $resp = array();
        $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm WHERE Id_ciclo_alum=" . $Id_ciclo_ulm . " ORDER BY Id_ciclo ASC";
        $ciclos_alum_ulm = mysql_query($query_ciclos_alum_ulm, $this->cnn) or die(mysql_error());
        $row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm);
        $totalRows_ciclos_alum_ulm = mysql_num_rows($ciclos_alum_ulm);
        if ($totalRows_ciclos_alum_ulm > 0) {
 
                $resp['Id_ciclo_alum'] = $row_ciclos_alum_ulm['Id_ciclo_alum'];
                $resp['Id_ciclo'] = $row_ciclos_alum_ulm['Id_ciclo'];
                $resp['Id_grado'] = $row_ciclos_alum_ulm['Id_grado'];
                $resp['Id_ofe_alum'] = $row_ciclos_alum_ulm['Id_ofe_alum'];
                $resp['Tipo_beca'] = $row_ciclos_alum_ulm['Tipo_beca'];
                $resp['Porcentaje_beca'] = $row_ciclos_alum_ulm['Porcentaje_beca'];
                $resp['Pagos_ciclo'] = array();
                $resp['Pagos_ciclo'] = $this->get_pagos_ciclo($row_ciclos_alum_ulm['Id_ciclo_alum']);
                $resp['Materias_ciclo_oferta'] = array();
                $resp['Materias_ciclo_oferta'] = $this->get_materias_ciclo($row_ciclos_alum_ulm['Id_ciclo_alum']);
        }
        return $resp;
    }
    
    public function get_pagos_ciclo($Id_ciclo_alum) {
        $resp = array();
        $class_pagos= new class_pagos();
        $query_Pagos_ciclo = "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=" .$Id_ciclo_alum;
        $Pagos_ciclo = mysql_query($query_Pagos_ciclo, $this->cnn) or die(mysql_error());
        $row_Pagos_ciclo = mysql_fetch_array($Pagos_ciclo);
        $totalRows_Pagos_ciclo = mysql_num_rows($Pagos_ciclo);
        if ($totalRows_Pagos_ciclo > 0) {
            do {
               array_push($resp, $class_pagos->get_pago($row_Pagos_ciclo['Id_pago_ciclo']));
            } while ($row_Pagos_ciclo = mysql_fetch_array($Pagos_ciclo));
        }
        return $resp;
    }
    
    public function get_materias_ciclo($Id_ciclo_alum) {
        $resp = array();
        $query_materias_ciclo_ulm = "SELECT * FROM materias_ciclo_ulm 
        JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
        JOIN grados_ulm ON Materias_especialidades.Grado_mat=grados_ulm.Id_grado_ofe
        WHERE Id_ciclo_alum=" .$Id_ciclo_alum."  ORDER BY Grado ASC";
        $materias_ciclo_ulm = mysql_query($query_materias_ciclo_ulm, $this->cnn) or die(mysql_error());
        $row_materias_ciclo_ulm = mysql_fetch_array($materias_ciclo_ulm);
        $totalRows_materias_ciclo_ulm = mysql_num_rows($materias_ciclo_ulm);
        if ($totalRows_materias_ciclo_ulm > 0) {
            do {
               array_push($resp, $this->get_materia_ciclo($row_materias_ciclo_ulm['Id_ciclo_mat']));
               $resp[count($resp)-1]['Grado']=$row_materias_ciclo_ulm['Grado'];
            } while ($row_materias_ciclo_ulm = mysql_fetch_array($materias_ciclo_ulm));
        }
        return $resp;
    }
    
    public function get_materia_ciclo($Id_ciclo_mat) {
        $resp = array();
        $query_materias_ciclo_ulm = "SELECT * FROM materias_ciclo_ulm WHERE Id_ciclo_mat=" . $Id_ciclo_mat;
        $materias_ciclo_ulm= mysql_query($query_materias_ciclo_ulm, $this->cnn) or die(mysql_error());
        $row_materias_ciclo_ulm = mysql_fetch_array($materias_ciclo_ulm);
        $totalRows_materias_ciclo_ulm = mysql_num_rows($materias_ciclo_ulm);
        if ($totalRows_materias_ciclo_ulm > 0) {
 
                $resp['Id_ciclo_mat'] = $row_materias_ciclo_ulm['Id_ciclo_mat'];
                $resp['Id_ciclo_alum'] = $row_materias_ciclo_ulm['Id_ciclo_alum'];
                $resp['Id_mat_esp'] = $row_materias_ciclo_ulm['Id_mat_esp'];
                $resp['CalTotalParciales'] = $row_materias_ciclo_ulm['CalTotalParciales'];
                $resp['CalExtraordinario'] = $row_materias_ciclo_ulm['CalExtraordinario'];
                $resp['CalEspecial'] = $row_materias_ciclo_ulm['CalEspecial'];
                $resp['Id_ori'] = $row_materias_ciclo_ulm['Id_ori'];
                $resp['Id_grupo'] = $row_materias_ciclo_ulm['Id_grupo'];
                $resp['Tipo'] = $row_materias_ciclo_ulm['Tipo'];
                $resp['Ids_pagos'] = $row_materias_ciclo_ulm['Ids_pagos'];
                $resp['Activo'] = $row_materias_ciclo_ulm['Activo'];
                $resp['Id_alum'] = $row_materias_ciclo_ulm['Id_alum'];
                
        }
        return $resp;
    }
    
    public function get_first_ciclo_oferta($Id_ofe_alum) {
        $resp = array();
        $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm JOIN ciclos_ulm ON ciclos_alum_ulm.Id_ciclo=ciclos_ulm.Id_ciclo
        WHERE Id_ofe_alum=" . $Id_ofe_alum . " ORDER BY ciclos_alum_ulm.Id_ciclo ASC LIMIT 1";
        $ciclos_alum_ulm = mysql_query($query_ciclos_alum_ulm, $this->cnn) or die(mysql_error());
        $row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm);
        $totalRows_ciclos_alum_ulm = mysql_num_rows($ciclos_alum_ulm);
        if ($totalRows_ciclos_alum_ulm > 0) {
            $resp['Id_ciclo_alum'] = $row_ciclos_alum_ulm['Id_ciclo_alum'];
            $resp['Id_ciclo'] = $row_ciclos_alum_ulm['Id_ciclo'];
            $resp['Id_grado'] = $row_ciclos_alum_ulm['Id_grado'];
            $resp['Id_grupo'] = $row_ciclos_alum_ulm['Id_grupo'];
            $resp['Id_ofe_alum'] = $row_ciclos_alum_ulm['Id_ofe_alum'];
            $resp['Nombre_ciclo'] = $row_ciclos_alum_ulm['Nombre_ciclo'];
            $resp['Clave'] = $row_ciclos_alum_ulm['Clave'];
            $resp['Fecha_ini'] = $row_ciclos_alum_ulm['Fecha_ini'];
        }
        return $resp;
    }
    
    public function get_last_ciclo_oferta($Id_ofe_alum) {
        $resp = array();
        $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm JOIN ciclos_ulm ON ciclos_alum_ulm.Id_ciclo=ciclos_ulm.Id_ciclo
        WHERE Id_ofe_alum=" . $Id_ofe_alum . " ORDER BY ciclos_alum_ulm.Id_ciclo DESC LIMIT 1";
        $ciclos_alum_ulm = mysql_query($query_ciclos_alum_ulm, $this->cnn) or die(mysql_error());
        $row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm);
        $totalRows_ciclos_alum_ulm = mysql_num_rows($ciclos_alum_ulm);
        if ($totalRows_ciclos_alum_ulm > 0) {
            $resp['Id_ciclo_alum'] = $row_ciclos_alum_ulm['Id_ciclo_alum'];
            $resp['Id_ciclo'] = $row_ciclos_alum_ulm['Id_ciclo'];
            $resp['Id_grado'] = $row_ciclos_alum_ulm['Id_grado'];
            $resp['Id_grupo'] = $row_ciclos_alum_ulm['Id_grupo'];
            $resp['Id_ofe_alum'] = $row_ciclos_alum_ulm['Id_ofe_alum'];
            $resp['Tipo_beca'] = $row_ciclos_alum_ulm['Tipo_beca'];
            $resp['Porcentaje_beca'] = $row_ciclos_alum_ulm['Porcentaje_beca'];
            $resp['Nombre_ciclo'] = $row_ciclos_alum_ulm['Nombre_ciclo'];
            $resp['Clave'] = $row_ciclos_alum_ulm['Clave'];
        }
        return $resp;
    }
    
    public function get_faltas_alumno_ciclo($Id_ciclo,$Id_alum) {
        $resp = array();
        $query_Asistencias = "SELECT * FROM Asistencias WHERE Id_ciclo=" .$Id_ciclo." AND Id_alum=".$Id_alum." AND Asistio=0 
        AND Fecha_asis NOT IN (SELECT Dia_justificado FROM Justificaciones WHERE Id_ciclo=".$Id_ciclo." AND Id_alum=".$Id_alum.") GROUP BY Fecha_asis ORDER BY Fecha_asis ASC";
        $Asistencias = mysql_query($query_Asistencias, $this->cnn) or die(mysql_error());
        $row_Asistencias = mysql_fetch_array($Asistencias);
        $totalRows_Asistencias = mysql_num_rows($Asistencias);
        if ($totalRows_Asistencias > 0) {
            do {
               array_push($resp, $row_Asistencias['Fecha_asis']);
            } while ($row_Asistencias = mysql_fetch_array($Asistencias));
        }
        return $resp;
    }
    
    public function get_last_grado_oferta($Id_ofe_alum) {
        $resp = array();
        $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm JOIN grados_ulm ON ciclos_alum_ulm.Id_grado=grados_ulm.Id_grado_ofe
        WHERE Id_ofe_alum=" . $Id_ofe_alum . " ORDER BY Grado DESC LIMIT 1";
        $ciclos_alum_ulm = mysql_query($query_ciclos_alum_ulm, $this->cnn) or die(mysql_error());
        $row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm);
        $totalRows_ciclos_alum_ulm = mysql_num_rows($ciclos_alum_ulm);
        if ($totalRows_ciclos_alum_ulm > 0) {
            $resp['Id_grado_ofe'] = $row_ciclos_alum_ulm['Id_grado_ofe'];
            $resp['Grado'] = $row_ciclos_alum_ulm['Grado'];
        }
        return $resp;
    }
    
    public function get_first_grado_oferta($Id_ofe_alum) {
        $resp = array();
        $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm JOIN grados_ulm ON ciclos_alum_ulm.Id_grado=grados_ulm.Id_grado_ofe
        WHERE Id_ofe_alum=" . $Id_ofe_alum . " ORDER BY Grado ASC LIMIT 1";
        $ciclos_alum_ulm = mysql_query($query_ciclos_alum_ulm, $this->cnn) or die(mysql_error());
        $row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm);
        $totalRows_ciclos_alum_ulm = mysql_num_rows($ciclos_alum_ulm);
        if ($totalRows_ciclos_alum_ulm > 0) {
            $resp['Id_grado_ofe'] = $row_ciclos_alum_ulm['Id_grado_ofe'];
            $resp['Grado'] = $row_ciclos_alum_ulm['Grado'];
        }
        return $resp;
    }
    
    public function get_materias_acreditadas($Id_ofe_alum) {
        $resp = array();
        $query_Materias_acreditadas = "SELECT * FROM Materias_acreditadas WHERE Id_ofe_alum=" .$Id_ofe_alum;
        $Materias_acreditadas = mysql_query($query_Materias_acreditadas, $this->cnn) or die(mysql_error());
        $row_Materias_acreditadas = mysql_fetch_array($Materias_acreditadas);
        $totalRows_Materias_acreditadas = mysql_num_rows($Materias_acreditadas);
        if ($totalRows_Materias_acreditadas > 0) {
            do {
                array_push($resp, $this->get_materia_acreditada($row_Materias_acreditadas['Id_mat_ac']));
            } while ($row_Materias_acreditadas = mysql_fetch_array($Materias_acreditadas));
        }
        return $resp;
    }
    
    public function get_materia_acreditada($Id_mat_ac) {
        $resp = array();
        $query_Materias_acreditadas = "SELECT * FROM Materias_acreditadas WHERE Id_mat_ac=" .$Id_mat_ac;
        $Materias_acreditadas = mysql_query($query_Materias_acreditadas, $this->cnn) or die(mysql_error());
        $row_Materias_acreditadas = mysql_fetch_array($Materias_acreditadas);
        $totalRows_Materias_acreditadas = mysql_num_rows($Materias_acreditadas);
        if ($totalRows_Materias_acreditadas > 0) {
                $resp['Id_mat_ac']=$row_Materias_acreditadas['Id_mat_ac'];
                $resp['Id_mat_esp']=$row_Materias_acreditadas['Id_mat_esp'];
                $resp['Id_ofe_alum']=$row_Materias_acreditadas['Id_ofe_alum'];
                $resp['Fecha_ac']=$row_Materias_acreditadas['Fecha_ac'];
                $resp['Id_ciclo']=$row_Materias_acreditadas['Id_ciclo'];
                $resp['Id_usu']=$row_Materias_acreditadas['Id_usu'];
                $resp['Id_usu']=$row_Materias_acreditadas['Id_usu'];
                $resp['Id_pago']=$row_Materias_acreditadas['Id_pago'];
        }
        return $resp;
    }
    
    public function get_amolestaciones_alumno($Id_alum) {
        $resp = array();
        $query_Reportes_alumno = "SELECT * FROM Reportes_alumno WHERE Id_recibe=" .$Id_alum." AND Tipo_recibe='alum'";
        $Reportes_alumno = mysql_query($query_Reportes_alumno, $this->cnn) or die(mysql_error());
        $row_Reportes_alumno = mysql_fetch_array($Reportes_alumno);
        $totalRows_Reportes_alumno = mysql_num_rows($Reportes_alumno);
        if ($totalRows_Reportes_alumno > 0) {
            do {
                array_push($resp, $this->get_amolestacion_alumno($row_Reportes_alumno['Id_rep']));
            } while ($row_Reportes_alumno = mysql_fetch_array($Reportes_alumno));
        }
        return $resp;
    }
    
    
    public function get_amolestacion_alumno($Id_rep) {
        $resp = array();
        $query_Reportes_alumno = "SELECT * FROM Reportes_alumno WHERE Id_rep=" .$Id_rep;
        $Reportes_alumno = mysql_query($query_Reportes_alumno, $this->cnn) or die(mysql_error());
        $row_Reportes_alumno = mysql_fetch_array($Reportes_alumno);
        $totalRows_Reportes_alumno = mysql_num_rows($Reportes_alumno);
        if ($totalRows_Reportes_alumno > 0) {
                $resp['Id_rep']=$row_Reportes_alumno['Id_rep'];
                $resp['Fecha_rep']=$row_Reportes_alumno['Fecha_rep'];
                $resp['Id_usu']=$row_Reportes_alumno['Id_usu'];
                $resp['Motivo']=$row_Reportes_alumno['Motivo'];
                $resp['Id_recibe']=$row_Reportes_alumno['Id_recibe'];
                $resp['Tipo_recibe']=$row_Reportes_alumno['Tipo_recibe'];
                $resp['Id_reporta']=$row_Reportes_alumno['Id_reporta'];
                $resp['Tipo_reporta']=$row_Reportes_alumno['Tipo_reporta'];
                $resp['Id_ciclo']=$row_Reportes_alumno['Id_ciclo'];
        }
        return $resp;
    }
    
    
    public function get_pagos_adelantados($Id_ofe_alum) {
        $resp = array();
        $query_Pagos_adelantados = "SELECT * FROM Pagos_adelantados WHERE Id_ofe_alum=" .$Id_ofe_alum." ORDER BY Id_pad ASC";
        $Pagos_adelantados = mysql_query($query_Pagos_adelantados, $this->cnn) or die(mysql_error());
        $row_Pagos_adelantados = mysql_fetch_array($Pagos_adelantados);
        $totalRows_Pagos_adelantados = mysql_num_rows($Pagos_adelantados);
        if ($totalRows_Pagos_adelantados > 0) {
            do {
                array_push($resp, $this->get_pago_adelantado($row_Pagos_adelantados['Id_pad']));
            } while ($row_Pagos_adelantados = mysql_fetch_array($Pagos_adelantados));
        }
        return $resp;
    }
    
    public function get_pago_adelantado($Id_pad) {
        $resp = array();
        $query_Pagos_adelantados = "SELECT * FROM Pagos_adelantados WHERE Id_pad=" .$Id_pad;
        $Pagos_adelantados = mysql_query($query_Pagos_adelantados, $this->cnn) or die(mysql_error());
        $row_Pagos_adelantados = mysql_fetch_array($Pagos_adelantados);
        $totalRows_Pagos_adelantados = mysql_num_rows($Pagos_adelantados);
        if ($totalRows_Pagos_adelantados > 0) {
                $resp['Id_pad']=$row_Pagos_adelantados['Id_pad'];
                $resp['Fecha_pago']=$row_Pagos_adelantados['Fecha_pago'];
                $resp['Monto_pago']=$row_Pagos_adelantados['Monto_pago'];
                $resp['Saldo']=$row_Pagos_adelantados['Saldo'];
                $resp['Id_usu']=$row_Pagos_adelantados['Id_usu'];
                $resp['Id_ofe_alum']=$row_Pagos_adelantados['Id_ofe_alum'];
                $resp['Id_alum']=$row_Pagos_adelantados['Id_alum'];
                $resp['Ids_pagos']=$row_Pagos_adelantados['Ids_pagos'];
                $resp['Id_met_pago']=$row_Pagos_adelantados['Id_met_pago'];
                $resp['Id_ciclo_usar']=$row_Pagos_adelantados['Id_ciclo_usar'];
                $resp['Id_paq_des']=$row_Pagos_adelantados['Id_paq_des'];
                $resp['DateCreated']=$row_Pagos_adelantados['DateCreated'];
                
        }
        return $resp;
    }
    
    public function get_alumnos_amolestados() {
        $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
        $usu = $class_usuarios->get_usu();
        $resp = array();
        $query_Reportes_alumno = "SELECT *, COUNT(Id_alum) AS Cantidad FROM Reportes_alumno
        JOIN inscripciones_ulm ON  Reportes_alumno.Id_alum=inscripciones_ulm.Id_ins WHERE Id_plantel=".$usu['Id_plantel']."
        GROUP BY Reportes_alumno.Id_alum ORDER BY Fecha_rep DESC" ;
        $Reportes_alumno = mysql_query($query_Reportes_alumno, $this->cnn) or die(mysql_error());
        $row_Reportes_alumno = mysql_fetch_array($Reportes_alumno);
        $totalRows_Reportes_alumno = mysql_num_rows($Reportes_alumno);
        if ($totalRows_Reportes_alumno > 0) {
            do {
                array_push($resp, $this->get_amolestacion_alumno($row_Reportes_alumno['Id_rep']));
                $resp[count($resp)-1]['Cantidad']=$row_Reportes_alumno['Cantidad'];
            } while ($row_Reportes_alumno = mysql_fetch_array($Reportes_alumno));
        }
        return $resp;
    }
    
    
        
   public function get_ciclo_oferta_ciclo($Id_ofe_alum,$Id_ciclo) {
        $resp = array();  
        $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=" . $Id_ofe_alum . " AND Id_ciclo=".$Id_ciclo;
        $ciclos_alum_ulm = mysql_query($query_ciclos_alum_ulm, $this->cnn) or die(mysql_error());
        $row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm);
        $totalRows_ciclos_alum_ulm = mysql_num_rows($ciclos_alum_ulm);
        if ($totalRows_ciclos_alum_ulm > 0) {

                $resp['Id_ciclo_alum'] = $row_ciclos_alum_ulm['Id_ciclo_alum'];
                $resp['Id_ciclo'] = $row_ciclos_alum_ulm['Id_ciclo'];
                $resp['Id_grado'] = $row_ciclos_alum_ulm['Id_grado'];
                $resp['Id_ofe_alum'] = $row_ciclos_alum_ulm['Id_ofe_alum'];
                $resp['Pagos_ciclo'] = array();
                $resp['Pagos_ciclo'] = $this->get_pagos_ciclo($row_ciclos_alum_ulm['Id_ciclo_alum']);
                $resp['Materias_ciclo_oferta'] = array();
                $resp['Materias_ciclo_oferta'] = $this->get_materias_ciclo($row_ciclos_alum_ulm['Id_ciclo_alum']);
     
        }
        return $resp;
    }
    
    public function adeudo_oferta_alumno($Id_alumn,$Id_ofe_alum){
        $Adeudo = 0;
        $totalAdeudo = 0;
        $oferta_alumno = $this->get_oferta_alumno($Id_ofe_alum);
        foreach ($oferta_alumno['Ciclos_oferta'] as $k => $v) {
            $query_Pagos_ciclo = "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=" . $v['Id_ciclo_alum'] ."  AND Fecha_pago<='".date('Y-m-d')."' ORDER BY Id_pago_ciclo ASC";
            $Pagos_ciclo = mysql_query($query_Pagos_ciclo, $this->cnn) or die(mysql_error());
            $row_Pagos_ciclo = mysql_fetch_array($Pagos_ciclo);
            $totalRows_Pagos_ciclo = mysql_num_rows($Pagos_ciclo);
            if ($totalRows_Pagos_ciclo > 0) {
              do {
                $recargos = 0;

                $class_recargos= new class_recargos();
                foreach($class_recargos->get_recargos_pago($row_Pagos_ciclo['Id_pago_ciclo']) as $k=>$v ){
                    //$recargos+=$v['Monto_recargo'];
                }
                $Adeudo = round($row_Pagos_ciclo['Mensualidad'] - $row_Pagos_ciclo['Descuento'] - $row_Pagos_ciclo['Descuento_beca'] - $row_Pagos_ciclo['Cantidad_Pagada'] + $recargos);
                $totalAdeudo+=$Adeudo;

              } while ($row_Pagos_ciclo = mysql_fetch_array($Pagos_ciclo));
            }
        }   
        
        return $totalAdeudo;
    }
    
    public function getMateriasByIdOfeAlumnAndIdCiclo($Id_ofe_alum,$Id_ciclo) {
            $resp = array();
            $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm 
             JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
             WHERE Id_ofe_alum=" . $Id_ofe_alum . " AND Id_ciclo=".$Id_ciclo." AND Activo=1 ORDER BY Id_ciclo ASC";
            $ciclos_alum_ulm = mysql_query($query_ciclos_alum_ulm, $this->cnn) or die(mysql_error());
            $row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm);
            $totalRows_ciclos_alum_ulm = mysql_num_rows($ciclos_alum_ulm);
            if ($totalRows_ciclos_alum_ulm > 0) {
                do {
                    array_push($resp, $this->get_materia_ciclo($row_ciclos_alum_ulm['Id_ciclo_mat']));
                } while ($row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm));
            }
            return $resp;
    }
}

