<?php
error_reporting(E_ERROR);
if (!$_COOKIE['admin/Id_usu'] > 0 && $_SERVER['SERVER_PORT'] !== "8888" && $pagina !== "index") {
    header('Location: index.php');
}
require_once('Connections/cnn.php');
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");
date_default_timezone_set('America/Mexico_City');

$pagina = $_SERVER['SCRIPT_FILENAME'];
$pagina = substr($pagina, 0, strpos($pagina, ".php"));
while (strpos($pagina, "/") !== false) {
    $pagina = substr($pagina, strpos($pagina, "/") + 1);
}
require_once 'estandares/class_usuarios.php';
require_once('clases/DaoTareasActividad.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoPlanteles.php');
require_once('clases/DaoParametrosPlantel.php');


function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}


$class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
$usu = $class_usuarios->get_usu();

$DaoParametrosPlantel= new DaoParametrosPlantel();
$params=$DaoParametrosPlantel->getParametrosPlantelArray();
$curPageURL=curPageURL();

// ver si tienes wwww
if(strpos($curPageURL,"//www.")=== false){
	$urlDest="http://www.".$params['Dominio']."/";
	$curPageURL=substr($curPageURL, strpos($curPageURL,"//")+2);
	$curPageURL=substr($curPageURL, strpos($curPageURL,"/")+1);
	//header("Location: $urlDest$curPageURL");
	//exit();
}
$DaoPlanteles=new DaoPlanteles();
$DaoUsuarios= new DaoUsuarios();

$_usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
$ptl=$DaoPlanteles->show($_usu->getId_plantel());
$perm=array();

foreach($DaoUsuarios->getPermisosUsuario($_usu->getId()) as $k=>$v){
    $perm[$v['Id_per']]=1;
}

 function links_head($title) {
     global $pagina;
    ?>
    <!DOCTYPE html>
    <!--[if lt IE 7 ]> <html lang="es" class="no-js ie6"> <![endif]-->
    <!--[if IE 7 ]>    <html lang="es" class="no-js ie7"> <![endif]-->
    <!--[if IE 8 ]>    <html lang="es" class="no-js ie8"> <![endif]-->
    <!--[if IE 9 ]>    <html lang="es" class="no-js ie9"> <![endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->

    <html lang="es" class="no-js" itemscope itemtype="http://schema.org/Article"> <!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <!--[if IE]><![endif]-->
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <title><?php echo $title; ?></title>
            <meta name="description" content="">
            <meta name="keywords" content="" />
            <meta name="author" content="GeneraWeb">
            <meta name="viewport" content="width=980, initial-scale=1.0">

            <!-- !CSS -->
            <link href="estandares/jquery-mega-drop-down-menu/css/skins/white.css" rel="stylesheet" type="text/css" />
            <link href="estandares/jquery-mega-drop-down-menu/css/skins/black.css" rel="stylesheet" type="text/css" />
            <link rel="stylesheet" href="css/styles.css?v=0">
             <?php
                if(file_exists("css/".$pagina.".css")){
                    ?>
                    <link rel="stylesheet" href="css/<?php echo $pagina; ?>.css?v=2011.5.5.13.35">
                   <?php
                }
                ?>
            <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700' rel='stylesheet' type='text/css'>
            <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
            <link href='favicon.png' rel='shortcut icon' type='image/png'/>
            <script src="js/selectores.js?a=2"></script>
    <?php
}


   function write_head_body() {
       global $pagina,$perm,$usu,$_usu,$ptl;
        ?>
            </head>
            <!-- !Body -->
            <body>
                <div id="errorLayer"></div>
                <div id="errorLayerSelector"></div>
                <div id="container">
                    <header>
                        <?php
                        $logo='';
                       if(strlen($ptl->getId_img_logo())>0){
                           $logo='src="files/'.$ptl->getId_img_logo().'.jpg"';
                       }

                        ?>
                        <a href="home.php"><img <?php echo $logo?>  id="logo"></a>
                        <div class="black">
                        <ul class="mega-menu">
                            <li><a href="home.php" class="dc-mega">Inicio</a></li>
                            <?php
                            if (isset($perm['30'])) {
                                ?>
                            <li><a href="interesados.php" class="dc-mega">Interesados</a>
                                    <div class="sub-container non-mega"><ul class="sub">
                                        <li><a href="interesado.php">Nuevo</a></li>
                                    </ul></div>
                            </li>
                            <?php
                            }
                            if (isset($perm['41'])) {
                            ?>
                            <li class=""><a href="alumnos.php" class="dc-mega">Alumnos</a>
                                <div class="sub-container non-mega"><ul class="sub">
                                        <li><a href="ofertas_alumno.php">Ofertas del alumno</a></li>
                                        <li><a href="analisis_sicologico.php">An&aacute;lisis Psicol&oacute;gico</a></li>
                                    </ul></div>
                            </li>
                            <?php
                            }
                            
                            if (isset($perm['45'])) {
                            ?>
                             <li class=""><a href="usuarios.php" class="dc-mega">Usuarios</a>
                                <div class="sub-container non-mega"><ul class="sub">
                                        <li><a href="actividades.php">Actividades</a></li>
                                        <li><a href="reporte_historial_usuarios.php">Historial</a></li>
                                    </ul></div>
                            </li>
                            <?php
                            }
                            if (isset($perm['46'])) {
                            ?>
                            <li class=""><a href="docentes.php" class="dc-mega">Docentes</a>
                                <div class="sub-container non-mega"><ul class="sub">
                                                <li><a href="reporte_horas_trabajadas.php">Sesiones trabajadas</a></li>
                                                <?php
                                                if (isset($perm['49'])) {
                                                ?>
                                                    <li><a href="evaluaciones_docente.php">Evaluar docente </a></li>
                                                    <li><a href="niveles_docente.php">Tabulador de pago </a></li>
                                                    <li><a href="categorias_evaluacion.php">Categorías de evaluación </a></li>
                                                <?php
                                                }
                                                ?>
                                                <li><a href="disponibilidad_docente.php">Disponibilidad</a></li>
                                                <li><a href="ligar_materias.php">Asignar materias </a></li>
                                                <li><a href="permutas_clase.php">Permuta de clases</a></li>
                                                <li><a href="penalizacion.php">Incidencias</a></li>
                                    </ul></div>
                            </li>
                            <?php
                            }
                            if (isset($perm['50'])) {
                            ?>
                            <li class="dc-mega-li"><a href="#" class="dc-mega">Coordinaci&oacute;n Acad&eacute;mica</a>
                                <div class="sub-container mega" style="left: 0px;"><ul class="sub">
                                        <div class="row">
                                            <?php
                                            if (isset($perm['48'])) {
                                                ?>
                                                <li class="mega-unit mega-hdr"><a href="#" class="mega-hdr-a">Horarios</a>
                                                    <ul>

                                                        <li><a href="horario_docente.php">Crear horario de clase</a></li>
                                                        <li><a href="horario_examen.php">Crear horario de exámen</a></li>
                                                        <li><a href="reporte_salones.php">Horario del aula</a></li>
                                                        <li><a href="reporte_horario_docente.php">Horario del docente</a></li>
                                                        <li><a href="reporte_horario_alumno.php">Horario del alumno</a></li>
                                                        <li><a href="reporte_examenes.php">Horario de exámenes</a></li>
                                                    </ul>
                                                </li>
                                            <?php
                                            }
                                            ?>
              
                                                <li class="mega-unit mega-hdr"><a href="#" class="mega-hdr-a">Ofertas academicas</a>
                                                <ul>
                                                    <li><a href="planteles.php">Planteles</a></li>
                                                    <?php
                                                    if (isset($perm['42'])) {
                                                    ?>
                                                    <li><a href="ofertas_academicas.php">Ofertas</a></li>
                                                    <?php
                                                    }
                                                    if (isset($perm['43'])) {
                                                    ?>
                                                    <!--<li><a href="materia.php">Materias</a></li>-->
                                                    <?php
                                                    }
                                                    ?>
                                                </ul>
                                            </li>
                                            <?php
                                             if (isset($perm['62'])) {
                                             ?>
                                            <li class="mega-unit mega-hdr"><a href="#" class="mega-hdr-a">Credenciales</a>
                                                    <ul>
                                                        <li><a href="reporte_credenciales.php">Alumnos</a></li>
                                                        <li><a href="reporte_credenciales_docentes.php">Docentes</a></li>
                                                    </ul>
                                                </li>
                                                <?php
                                             }
                                             ?>
                                        </div>
                                        <div class="row">
                                            <li class="mega-unit mega-hdr"><a href="#" class="mega-hdr-a">Calificaciones</a>
                                                <ul>
                                                    <li><a href="kardex.php">Kardex</a></li>
                                                   <li><a href="reporte_grupos_alumno.php">Grupos del alumno</a></li>
                                                   <li><a href="reporte_calificaciones_grupo.php">Por materia</a></li>
                                                   <?php
                                                    if (isset($perm['68'])) {
                                                     ?>
                                                       <li><a href="capturar_calificaciones.php">Capturar calificaciones</a></li>
                                                       <li><a href="reporte_envio_calificaciones.php">Env&iacute;o de calificaciones</a></li>
                                                     <?php
                                                    }
                                                 ?>
                                                </ul>
                                            </li>
                                            <li class="mega-unit mega-hdr"><a href="#" class="mega-hdr-a">Alumnos</a>
                                                <ul>
                                                    <li><a href="justificacion_asistencias.php">Justificar faltas </a></li>
                                                    <li><a href="reporte_por_egresar.php">Alumnos a egresar</a></li>
                                                <?php
                                                
                                                if (isset($perm['64'])) {
                                                    ?>
                                                    <li><a href="amolestaciones.php">Amonestaciones </a></li>
                                                    <?php
                                                }
                                                if (isset($perm['36'])) {
                                                    ?>
                                                    <li><a href="aplicar_becas.php">Aplicar becas </a></li>
                                                    <?php
                                                }
                                                if (isset($perm['73'])) {
                                                ?>
                                                    <li><a href="reporte_cambiar_ciclo.php">Cambiar de nivel </a></li>
                                                <?php
                                                }
                                                ?>
                                                </ul>
                                            </li>
                                            <li class="mega-unit mega-hdr"><a href="#" class="mega-hdr-a">General</a>
                                                <ul>
                                                    <li><a href="avisos.php">Enviar correos </a></li>
                                                <?php
                                                if (isset($perm['67'])) {
                                                    ?>
                                                    <li><a href="notificaciones.php">Notificaciones </a></li>
                                                    <?php
                                                }
                                                ?>
                                                <li><a href="reporte_rentabilidad.php">Rentabilidad grupos</a></li>
                                                <li><a href="eventos.php">Crear eventos</a></li>
                                                <li><a href="imagenes.php">Im&aacute;genes</a></li>
                                                <li><a href="estado_cuenta.php">Cobranza usuarios</a></li>
                                                <li><a href="documentos_drive.php">Documentos en drive</a></li>
                                                </ul>
                                            </li>
                                        </div>
                                    </ul></div>
                            </li>
                            <?php
                            }
                            if (isset($perm['51']) || isset($perm['52'])) {
                            ?>
                            <li class="dc-mega-li"><a href="#" class="dc-mega">Biblioteca</a>
                                <div class="sub-container mega"><ul class="sub">
                                        <div class="row">
                                            <?php
                                            if (isset($perm['52'])) {
                                                ?>
                                            <li class="mega-unit mega-hdr"><a href="#" class="mega-hdr-a">Libros</a>
                                                <ul>
                                                    <li><a href="libros.php">Libros</a></li>
                                                    <li><a href="prestamo_libros.php">Préstamo de libros</a></li>
                                                    <li><a href="reporte_prestamo_libros.php">Historial de préstamo</a></li>
                                                </ul>
                                            </li>
                                            <?php
                                            }
                                            if (isset($perm['51'])) {
                                                ?>
                                            <li class="mega-unit mega-hdr"><a href="#" class="mega-hdr-a">Activos</a>
                                                <ul>
                                                    <li><a href="activos.php">Activos</a></li>
                                                    <li><a href="prestamo_activos.php">Préstamo de activos</a></li>
                                                    <li><a href="reporte_prestamo_activos.php">Historial de préstamo</a></li>
                                                </ul>
                                            </li>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </ul></div>
                            </li>
                            <?php
                            }
                            ?>
                            <li class="dc-mega-li"><a href="#" class="dc-mega">Reportes</a>
                                <div class="sub-container mega" style="left: 233px;"><ul class="sub">
                                        <div class="row"><li class="mega-unit mega-hdr"><a href="#" class="mega-hdr-a">Alumnos</a>
                                                <ul>
                                                    <li><a href="reporte_alumnos.php">Alumnos</a></li>
                                                    <li><a href="reporte_alumnos_bajas.php">Bajas</a></li>
                                                    <li><a href="reporte_egresados.php">Egresados</a></li>
                                                    <li><a href="reporte_alumnos_deudores.php">Deudores</a></li>
                                                    <li><a href="reporte_alumnos_sin_grupo.php">Sin grupo</a></li>
                                                    <li><a href="reporte_alumnos_para_baja.php">Para baja</a></li>
                                                    <li><a href="reporte_amolestaciones.php">Amonestados</a></li>
                                                    <li><a href="reporte_alumnos_becados.php">Becados</a></li>
                                                    <li><a href="reporte_alumnos_reprobados.php">Reprobados</a></li>
                                                </ul>
                                            </li><li class="mega-unit mega-hdr"><a href="#" class="mega-hdr-a">Interesados</a>
                                                <ul>
                                                    <li><a href="reporte_ofertas_interes.php">Ofertas de inter&eacute;s alumnos</a></li>
                                                    <li><a href="reporte_acciones.php">Acciones interesados</a></li>
                                                    <li><a href="reporte_seguimientos_interesados.php">Seguimientos interesados</a></li>
                                                    
                                                </ul>
                                            </li><li class="mega-unit mega-hdr"><a href="#" class="mega-hdr-a">Generales</a>
                                                <ul>
                                                    <li><a href="reporte_grupos.php">Grupos</a></li>
                                                    <li><a href="reporte_recibos.php">Recibos generados</a></li>
                                                    <li><a href="reporte_pagos_proximos.php">Pagos próximos</a></li>
                                                    <li><a href="reporte_documentos_faltantes.php">Documentos faltantes</a></li>
                                                    <li><a href="reporte_eventos.php">Eventos</a></li>
                                                    <li><a href="reporte_primer_ingreso.php">Ciclo de ingreso</a></li>
                                                    <li><a href="reporte_seguro.php">Datos del seguro</a></li>
                                                </ul>
                                            </li></div>
                                    </ul></div>
                            </li>
                            <li><a href="perfil.php?id=<?php echo $usu['Id_usu'] ?>" class="dc-mega">Mi perfil</a></li>
                            <?php
                            if (isset($perm['57'])) {
                            ?>
                            <li><a href="configurar.php" class="dc-mega">Configurar</a></li>
                            <?php
                            }
                            ?>

                        </ul>
                    </div>
                        <nav>
                            <div id="box-box-tareas">
                            <?php
                            $DaoTareasActividad= new DaoTareasActividad();
                            
                            $z=count($DaoTareasActividad->getTareasActividadUserFaltantes($_COOKIE['admin/Id_usu']));
                            if($z>0){
                                ?>
                                    <i class="fa fa-bell" id="icon-lista-de-tareas"></i>
                                    <a href="actividades.php">
                                        <div id="box-count-tareas">
                                             <?php echo $z?> 
                                        </div>
                                   </a>                    
                           <?php
                            }
                            ?>
                            </div>
                        </nav>
                    </header>   
        <?php
    }

    function write_body() {
        global $database_cnn, $cnn,$usu,$_usu;
        ?>
                    <div id="contenido">
                        <div id="box-notificaciones"></div>
                    <?php
                    if(isset($_COOKIE['test_admin'])){
                    ?>
                        <span id="text_prueba">Modo de Prueba</span>
                        <?php
                    }
    }

    
     function write_footer() {
          global $pagina,$ptl;
                    ?>

                        <div id="box_alerta">
                            <p></p>
                            <!--<button onclick="ocultar_alerta()">Aceptar</button>-->
                        </div>
                    </div><!--!/#contenido -->
                </div><!--!/#container -->
                <div id="box_img_foot"><img src="images/img_foot.png" alt="img_foot" width="980" height="21">
                    <p>&copy; <?php echo date('Y')?> <?php echo $ptl->getNombre_plantel()?></p>
                    <a href="logout.php"> <span id="salir"><img src="images/flecha_logout.png" alt="flecha_logout" width="11" height="13">Salir</span></a>
                </div>
                <footer><!--1811927-->

                </footer>
                <!-- !Javascript - at the bottom for fast page loading -->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
                <script src="js/scripts.js?a=2"></script>
                <?php
                if(file_exists("js/".$pagina.".js")){
                    ?>
                   <script src="js/<?php echo $pagina; ?>.js?a=2"></script> 
                   <?php
                }
                ?>
                <!--<script src="jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>-->
                <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
                <link rel="stylesheet" href="jquery-ui-1.10.4.custom/css/start/jquery-ui-1.10.4.custom.min.css" />
                <script type='text/javascript' src='estandares/jquery-mega-drop-down-menu/js/jquery.hoverIntent.minified.js'></script>
                <script type='text/javascript' src='estandares/jquery-mega-drop-down-menu/js/jquery.dcmegamenu.1.3.3.js'></script>
                <!-- notificaciones-->
                <script type="text/javascript" src="js/pnotify.custom.min.js"></script>
                <link href="css/pnotify.custom.min.css" media="all" rel="stylesheet" type="text/css" />
            </body>
        </html>
        <?php
    }

    