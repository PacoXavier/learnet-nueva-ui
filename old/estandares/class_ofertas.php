<?php

require_once("class_bdd.php");
require_once("class_archivos.php");
require_once("class_materias.php");
require_once("class_usuarios.php");

class class_ofertas extends class_bdd {

    public $Id_ofe;
    public $description;

    public function __construct($Id_ofe) {
        class_bdd::__construct();
        if ($Id_ofe > 0) {
            $this->Id_ofe = $Id_ofe;
        }
    }

    public function get_ofertas() {
        $resp = array();
        $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
        $usu = $class_usuarios->get_usu();
        //$query_ofertas_ulm = "SELECT * FROM ofertas_ulm WHERE Activo_oferta=1  ORDER BY Nombre_oferta ASC";
        $query_ofertas_ulm="SELECT * FROM ofertas_ulm 
        WHERE Activo_oferta=1 AND Id_plantel_x=".$usu['Id_plantel']." ORDER BY Nombre_oferta ASC";
                    
        $ofertas_ulm = mysql_query($query_ofertas_ulm, $this->cnn) or die(mysql_error());
        $row_ofertas_ulm = mysql_fetch_array($ofertas_ulm);
        $totalRows_ofertas_ulm = mysql_num_rows($ofertas_ulm);
        if ($totalRows_ofertas_ulm > 0) {
            do {
                array_push($resp, $this->get_oferta($row_ofertas_ulm['Id_oferta']));
            } while ($row_ofertas_ulm = mysql_fetch_array($ofertas_ulm));
        }
        return $resp;
    }

    public function get_oferta($Id_ofe = null) {
        if ($Id_ofe == null) {
            $Id_ofe = $this->Id_ofe;
        }

        $resp = array();
        $query_ofertas_ulm = "SELECT * FROM ofertas_ulm WHERE Id_oferta=" . $Id_ofe;

        $ofertas_ulm = mysql_query($query_ofertas_ulm, $this->cnn) or die(mysql_error());
        $row_ofertas_ulm = mysql_fetch_array($ofertas_ulm);
        $totalRows_ofertas_ulm = mysql_num_rows($ofertas_ulm);
        if ($totalRows_ofertas_ulm > 0) {
            $resp['Id_oferta'] = $row_ofertas_ulm['Id_oferta'];
            $resp['Nombre_oferta'] = $row_ofertas_ulm['Nombre_oferta'];
            $resp['Clave_oferta'] = $row_ofertas_ulm['Clave_oferta'];
            $resp['Activo_oferta'] = $row_ofertas_ulm['Activo_oferta'];
            $resp['Especialidades'] = array();
            $resp['Especialidades'] = $this->get_especialidades($row_ofertas_ulm['Id_oferta']);
            $resp['Documentos'] = array();
            $resp['Documentos'] = $this->get_archivos_oferta($row_ofertas_ulm['Id_oferta']);
        }
        return $resp;
    }

    public function get_especialidades($Id_ofe = null) {
        if ($Id_ofe == null) {
            $Id_ofe = $this->Id_ofe;
        }
        $resp = array();
        $query_especialidades_ulm = "SELECT * FROM especialidades_ulm WHERE Id_ofe=" . $Id_ofe . " AND Activo_esp=1";
        $especialidades_ulm = mysql_query($query_especialidades_ulm, $this->cnn) or die(mysql_error());
        $row_especialidades_ulm = mysql_fetch_array($especialidades_ulm);
        $totalRows_especialidades_ulm = mysql_num_rows($especialidades_ulm);
        if ($totalRows_especialidades_ulm > 0) {
            do {
                array_push($resp, $this->get_especialidad($row_especialidades_ulm['Id_esp']));
            } while ($row_especialidades_ulm = mysql_fetch_array($especialidades_ulm));
        }
        return $resp;
    }

    public function get_especialidad($Id_esp) {
        $resp = array();
        $query_especialidad = "SELECT * FROM especialidades_ulm WHERE Id_esp=" . $Id_esp;
        $especialidad = mysql_query($query_especialidad, $this->cnn) or die(mysql_error());
        $row_especialidad = mysql_fetch_array($especialidad);
        $totalRows_especialidad = mysql_num_rows($especialidad);
        if ($totalRows_especialidad > 0) {

            $resp['Id_esp'] = $row_especialidad['Id_esp'];
            $resp['Id_ofe'] = $row_especialidad['Id_ofe'];
            $resp['Nombre_esp'] = $row_especialidad['Nombre_esp'];
            $resp['Activo_esp'] = $row_especialidad['Activo_esp'];
            $resp['Orientaciones'] = array();
            $resp['Orientaciones'] = $this->get_orientaciones($row_especialidad['Id_esp']);
            $resp['RegistroValidez_Oficial'] = $row_especialidad['RegistroValidez_Oficial'];
            $resp['Institucion_certifica'] = $row_especialidad['Institucion_certifica'];
            $resp['Modalidad'] = $row_especialidad['Modalidad'];
            $resp['Tipo_ciclos'] = $row_especialidad['Tipo_ciclos'];
            $resp['DuracionSemanas'] = $row_especialidad['DuracionSemanas'];
            $resp['ClavePlan_estudios'] = $row_especialidad['ClavePlan_estudios'];
            $resp['Escala_calificacion'] = $row_especialidad['Escala_calificacion'];
            $resp['Minima_aprobatotia'] = $row_especialidad['Minima_aprobatotia'];
            $resp['Limite_faltas'] = $row_especialidad['Limite_faltas'];
            $resp['Num_ciclos'] = $row_especialidad['Num_ciclos'];
            $resp['Precio_curso'] = $row_especialidad['Precio_curso'];
            $resp['Inscripcion_curso'] = $row_especialidad['Inscripcion_curso'];
            $resp['Num_pagos'] = $row_especialidad['Num_pagos'];
            $resp['Tipo_insc'] = $row_especialidad['Tipo_insc'];
            $resp['Grados'] = array();
            $resp['Grados'] = $this->get_grados($row_especialidad['Id_esp']);
            $resp['Materias']=array();    
            $resp['Materias']= $this->get_materias_esp($row_especialidad['Id_esp']);
        }
        return $resp;
    }
    
    public function get_grados($Id_esp){
         $resp=array();
            $query_grados = "SELECT * FROM grados_ulm WHERE Id_esp=" . $Id_esp . " AND Activo_grado=1 ORDER BY Grado ASC";
            $grados = mysql_query($query_grados, $this->cnn) or die(mysql_error());
            $row_grados = mysql_fetch_array($grados);
            $totalRows_grados = mysql_num_rows($grados);
            if ($totalRows_grados > 0) {
                do {
                    array_push($resp, $this->get_grado($row_grados['Id_grado_ofe']));
                } while ($row_grados = mysql_fetch_array($grados));
            }
             return $resp;
     }
     
    public function get_grado($Id_grado_ofe){
         $resp=array();
            $query_grados = "SELECT * FROM grados_ulm WHERE Id_grado_ofe=" . $Id_grado_ofe . " ORDER BY Grado ASC";
            $grados = mysql_query($query_grados, $this->cnn) or die(mysql_error());
            $row_grados = mysql_fetch_array($grados);
            $totalRows_grados = mysql_num_rows($grados);
            if ($totalRows_grados > 0) {
                $resp['Id_grado_esp'] = $row_grados['Id_grado_ofe'];
                $resp['Id_esp'] = $row_grados['Id_esp'];
                $resp['Grado'] = $row_grados['Grado'];

            }
             return $resp;
     }
    
    public function get_materias_esp($Id_esp) {
        $resp = array();
        $query_materias_uml = "SELECT * FROM Materias_especialidades 
        JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat
        JOIN grados_ulm ON Materias_especialidades.Grado_mat=grados_ulm.Id_grado_ofe 
        WHERE Materias_especialidades.Id_esp=" . $Id_esp . " AND Activo_mat_esp=1 AND Activo_mat=1
        ORDER BY Grado ASC ";
        $materias_uml = mysql_query($query_materias_uml, $this->cnn) or die(mysql_error());
        $row_materias_uml = mysql_fetch_assoc($materias_uml);
        $totalRows_materias_uml = mysql_num_rows($materias_uml);
        if ($totalRows_materias_uml > 0) {
            do {
                array_push($resp, $this->get_materia_esp($row_materias_uml['Id_mat_esp']));
            } while ($row_materias_uml = mysql_fetch_assoc($materias_uml));
        }
        return $resp;
    }
    
    
    public function get_materia_esp($Id_mat_esp) {
        $resp = array();
        $query_materias_uml = "SELECT * FROM Materias_especialidades 
        JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat
        JOIN grados_ulm ON Materias_especialidades.Grado_mat=grados_ulm.Id_grado_ofe 
        WHERE Materias_especialidades.Id_mat_esp=" . $Id_mat_esp . " AND Activo_mat_esp=1 AND Activo_mat=1
        ORDER BY Grado ASC ";
        $materias_uml = mysql_query($query_materias_uml, $this->cnn) or die(mysql_error());
        $row_materias_uml = mysql_fetch_assoc($materias_uml);
        $totalRows_materias_uml = mysql_num_rows($materias_uml);
        if ($totalRows_materias_uml > 0) {

                $resp['Id_mat_esp']=$row_materias_uml['Id_mat_esp'];
                $resp['Id_esp']=$row_materias_uml['Id_esp'];
                $resp['Id_mat']=$row_materias_uml['Id_mat'];
                $resp['Activo_mat_esp']=$row_materias_uml['Activo_mat_esp'];
                $resp['Grado_mat']=$row_materias_uml['Grado_mat'];
                $resp['Evaluacion_extraordinaria']=$row_materias_uml['Evaluacion_extraordinaria'];
                $resp['Evaluacion_especial']=$row_materias_uml['Evaluacion_especial'];
                $resp['Tiene_orientacion']=$row_materias_uml['Tiene_orientacion'];
                $resp['NombreDiferente']=$row_materias_uml['NombreDiferente'];
                $resp['Requisitos']=array();
                $resp['Requisitos']=$this->get_requisitos($row_materias_uml['Id_mat_esp']);
                $resp['Evaluaciones']=array();
                $resp['Evaluaciones']=$this->get_evaluaciones($row_materias_uml['Id_mat']);
        }
        return $resp;
    }
    
    
    
       public function get_requisitos($Id_mat){
         $resp=array();
             $query_prerequisitos = "SELECT * FROM prerequisitos_ulm WHERE Id_mat_esp=".$Id_mat."  ORDER BY Id_mat_pre ASC";
             $prerequisitos = mysql_query($query_prerequisitos, $this->cnn) or die(mysql_error());
             $row_prerequisitos= mysql_fetch_array($prerequisitos);
             $totalRows_prerequisitos = mysql_num_rows($prerequisitos);
             if($totalRows_prerequisitos>0){
                do{
                    $pre= array();
                        $pre['Id_mat_pre']=$row_prerequisitos['Id_mat_pre'];
                        $pre['Id_mat_esp']=$row_prerequisitos['Id_mat_esp'];
                        $pre['Id_mat_esp_pre']=$row_prerequisitos['Id_mat_esp_pre'];
                        array_push($resp, $pre);

                    }while($row_prerequisitos= mysql_fetch_array($prerequisitos));
             }
             return $resp;
     }



     public function get_evaluaciones($Id_mat){
            $resp=array();
             $query_Evaluaciones = "SELECT * FROM Evaluaciones WHERE Id_mat_esp=".$Id_mat;
             $Evaluaciones = mysql_query($query_Evaluaciones, $this->cnn) or die(mysql_error());
             $row_Evaluaciones= mysql_fetch_array($Evaluaciones);
             $totalRows_Evaluaciones = mysql_num_rows($Evaluaciones);
             if($totalRows_Evaluaciones>0){
                do{
                    $eva= array();
                        $eva['Id_eva']=$row_Evaluaciones['Id_eva'];
                        $eva['Ponderacion']=$row_Evaluaciones['Ponderacion'];
                        $eva['Id_mat_esp']=$row_Evaluaciones['Id_mat_esp'];
                        array_push($resp, $eva);

                    }while($row_Evaluaciones= mysql_fetch_array($Evaluaciones));
             }
             return $resp;
     }

    public function get_orientaciones($Id_esp) {
        $resp = array();
        $query_orientaciones_ulm = "SELECT * FROM orientaciones_ulm WHERE Id_esp=" . $Id_esp . " AND Activo_ori=1  ORDER BY Nombre_ori ASC ";
        $orientaciones_ulm = mysql_query($query_orientaciones_ulm, $this->cnn) or die(mysql_error());
        $row_orientaciones_ulm = mysql_fetch_array($orientaciones_ulm);
        $totalRows_orientaciones_ulm = mysql_num_rows($orientaciones_ulm);
        if ($totalRows_orientaciones_ulm > 0) {
            do {
                array_push($resp, $this->get_orientacion($row_orientaciones_ulm['Id_ori']));
            } while ($row_orientaciones_ulm = mysql_fetch_array($orientaciones_ulm));
        }
        return $resp;
    }

    public function get_orientacion($Id_ori) {
        $resp = array();
        $query_orientacion = "SELECT * FROM orientaciones_ulm WHERE Id_ori=" . $Id_ori;
        $orientacion = mysql_query($query_orientacion, $this->cnn) or die(mysql_error());
        $row_orientacion = mysql_fetch_array($orientacion);
        $totalRows_orientacion = mysql_num_rows($orientacion);
        if ($totalRows_orientacion > 0) {
            do {

                $resp['Id_ori'] = $row_orientacion['Id_ori'];
                $resp['Id_esp'] = $row_orientacion['Id_esp'];
                $resp['Nombre_ori'] = $row_orientacion['Nombre_ori'];
                $resp['Activo_ori'] = $row_orientacion['Activo_ori'];
            } while ($row_orientacion = mysql_fetch_array($orientacion));
        }
        return $resp;
    }

    public function get_cursos_sin_orientacion() {
        $resp = array();
        $query_cursos_sin_orientacion = "SELECT * FROM especialidades_ulm ORDER BY Id_esp ASC";
        $cursos_sin_orientacion = mysql_query($query_cursos_sin_orientacion, $this->cnn) or die(mysql_error());
        $row_cursos_sin_orientacion = mysql_fetch_array($cursos_sin_orientacion);
        $totalRows_cursos_sin_orientacion = mysql_num_rows($cursos_sin_orientacion);
        if ($totalRows_cursos_sin_orientacion > 0) {
            do {
                array_push($resp, $this->get_especialidad($row_cursos_sin_orientacion['Id_esp']));
            } while ($row_cursos_sin_orientacion = mysql_fetch_array($cursos_sin_orientacion));
        }
        return $resp;
    }

    public function get_cursos_con_orientacion() {
        $resp = array();
        $query_cursos_sin_orientacion = "SELECT * FROM orientaciones_ulm";
        $cursos_sin_orientacion = mysql_query($query_cursos_sin_orientacion, $this->cnn) or die(mysql_error());
        $row_cursos_sin_orientacion = mysql_fetch_array($cursos_sin_orientacion);
        $totalRows_cursos_sin_orientacion = mysql_num_rows($cursos_sin_orientacion);
        if ($totalRows_cursos_sin_orientacion > 0) {
            do {
                array_push($resp, $this->get_orientacion($row_cursos_sin_orientacion['Id_ori']));
            } while ($row_cursos_sin_orientacion = mysql_fetch_array($cursos_sin_orientacion));
        }
        return $resp;
    }

    public function get_archivos_oferta($Id_ofe = null) {
        if ($Id_ofe == null) {
            $Id_ofe = $this->Id_ofe;
        }
        $resp = array();
        $query_archivos_ofertas_ulm = "SELECT * FROM archivos_ofertas_ulm WHERE Id_ofe=" . $Id_ofe;
        $archivos_ofertas_ulm = mysql_query($query_archivos_ofertas_ulm, $this->cnn) or die(mysql_error());
        $row_archivos_ofertas_ulm = mysql_fetch_array($archivos_ofertas_ulm);
        $totalRows_archivos_ofertas_ulm = mysql_num_rows($archivos_ofertas_ulm);
        if ($totalRows_archivos_ofertas_ulm > 0) {
            do {
                $class_archivos = new class_archivos($row_archivos_ofertas_ulm['Id_file']);
                array_push($resp, $class_archivos->get_archivo());
            } while ($row_archivos_ofertas_ulm = mysql_fetch_array($archivos_ofertas_ulm));
        }
        return $resp;
    }

}
