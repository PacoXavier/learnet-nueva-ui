<?php
require_once("class_bdd.php"); 
require_once("class_usuarios.php");
class class_activos extends class_bdd{

                 public $Id_activo;
	     
	     public function  __construct($Id_activo){
	     	   class_bdd::__construct();
	     	   if($Id_activo>0){
	     	   	  $this->Id_activo=$Id_activo;
	     	   }
	     }
	     
	     public function get_activos(){
                    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                    $usu = $class_usuarios->get_usu();
	         $resp=array();
		     $query_activos = "SELECT * FROM Activos WHERE  Id_plantel=".$usu['Id_plantel']." ORDER BY Id_activo DESC";
		     $activos = mysql_query($query_activos, $this->cnn) or die(mysql_error());
		     $row_activos= mysql_fetch_array($activos);
		     $totalRows_activos = mysql_num_rows($activos);
		     if($totalRows_activos>0){
                               do{
                                  array_push($resp, $this->get_activo($row_activos['Id_activo']));
                               }while( $row_activos= mysql_fetch_array($activos));
		     }
		     return $resp;
	     }
	     
	     public function get_activo($Id_activo=null){
	         if($Id_activo==null){
		        $Id_activo=$this->Id_activo;
	         }
	         $resp=array();
		     $query_activos = "SELECT * FROM Activos WHERE Id_activo=".$Id_activo;
		     $activos = mysql_query($query_activos, $this->cnn) or die(mysql_error());
		     $row_activos= mysql_fetch_array($activos);
		     $totalRows_activos = mysql_num_rows($activos);
		     if($totalRows_activos>0){

			        $resp['Id_activo']=$row_activos['Id_activo'];
			        $resp['Nombre']=$row_activos['Nombre'];
			        $resp['Modelo']=$row_activos['Modelo'];
                                $resp['Marca']=$row_activos['Marca'];
                                $resp['Num_serie']=$row_activos['Num_serie'];
			        $resp['Descripcion']=$row_activos['Descripcion'];
			        $resp['Id_img']=$row_activos['Id_img'];
			        $resp['Valor']=$row_activos['Valor'];
			        $resp['Codigo']=$row_activos['Codigo'];
			        $resp['Tipo_act']=$row_activos['Tipo_act'];
			        $resp['Fecha_adq']=$row_activos['Fecha_adq'];
			        $resp['Disponible']=$row_activos['Disponible'];
			        $resp['Garantia_meses']=$row_activos['Garantia_meses'];
			        $resp['Mantenimiento_frecuencia']=$row_activos['Mantenimiento_frecuencia'];
			        $resp['Id_plantel']=$row_activos['Id_plantel'];
			        $resp['Baja_activo']=$row_activos['Baja_activo'];
                                $resp['Id_aula']=$row_activos['Id_aula'];
                                $resp['Comentario_baja']=$row_activos['Comentario_baja'];
			        $resp['Historial']=array();
                                $resp['Historial_prestamos']=array();
                                          
                                $query_historial = "SELECT * FROM Historial_activo WHERE Id_activo=".$row_activos['Id_activo']." ORDER  BY Id_hist DESC";
                                $historial = mysql_query($query_historial, $this->cnn) or die(mysql_error());
                                $row_historial= mysql_fetch_array($historial);
                                $totalRows_historial = mysql_num_rows($historial);
                                if($totalRows_historial>0){
                                do{
                                    $hist=array();
                                    $hist['Id_hist']=$row_historial['Id_hist'];
                                    $hist['Id_activo']=$row_historial['Id_activo'];
                                    $hist['Comentarios']=$row_historial['Comentarios'];
                                    $hist['Id_usu']=$row_historial['Id_usu'];
                                    $hist['Tipo_usu']=$row_historial['Tipo_usu'];
                                    $hist['Fecha_hist']=$row_historial['Fecha_hist'];
                                    $hist['Estado']=$row_historial['Estado'];
                                    array_push($resp['Historial'], $hist);
                                }while($row_historial= mysql_fetch_array($historial));
                                }

                                $query_historial = "SELECT * FROM Historial_activo_prestamo WHERE Id_activo=".$row_activos['Id_activo']." ORDER  BY Id_prestamo DESC";
                                $historial = mysql_query($query_historial, $this->cnn) or die(mysql_error());
                                $row_historial= mysql_fetch_array($historial);
                                $totalRows_historial = mysql_num_rows($historial);
                                if($totalRows_historial>0){
                                do{
                                    $hist=array();
                                    $hist['Id_prestamo']=$row_historial['Id_prestamo'];
                                    $hist['Id_activo']=$row_historial['Id_activo'];
                                    $hist['Comentarios']=$row_historial['Comentarios'];
                                    $hist['Usu_entrega']=$row_historial['Usu_entrega'];
                                    $hist['Usu_recibe']=$row_historial['Usu_recibe'];
                                    $hist['Estado_entrega']=$row_historial['Estado_entrega'];
                                    $hist['Estado_recibe']=$row_historial['Estado_recibe'];
                                    $hist['Fecha_entrega']=$row_historial['Fecha_entrega'];
                                    $hist['Fecha_devolucion']=$row_historial['Fecha_devolucion'];
                                    $hist['Fecha_entregado']=$row_historial['Fecha_entregado'];
                                    $hist['Tipo_usu']=$row_historial['Tipo_usu'];
                                    
                                    array_push($resp['Historial_prestamos'], $hist);
                                }while($row_historial= mysql_fetch_array($historial));
                                }
		     }
		     return $resp;
	     }	
             
              public function generar_codigo_activo($Id_tipo) {
                //Generamos la matricula del activo
                //Tomando como base el primer digito es del tipo al que pertence
                //y los restantes son la ultima matricula existente
                $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                $usu = $class_usuarios->get_usu();
                $query_Activos = "SELECT * FROM Activos WHERE  Codigo IS NOT NULL AND Id_plantel=".$usu['Id_plantel']." AND Tipo_act =".$Id_tipo." ORDER BY Codigo DESC LIMIT 1";
                $Activos = mysql_query($query_Activos, $this->cnn) or die(mysql_error());
                $row_Activos = mysql_fetch_array($Activos);
                $totalRows_Activos = mysql_num_rows($Activos);
                $matricula=0;
                if (strlen($row_Activos['Codigo'])>0) {
                      $matricula = substr($row_Activos['Codigo'], 1);
                      $matricula = (int) $Id_tipo.$matricula;
                      $matricula++;
                }else{
                      $matricula++;
                      $ceros="";
                      for($i=0;$i<(4-strlen($matricula));$i++){
                         $ceros.="0";
                      }
                      $matricula=$Id_tipo.$ceros.$matricula;
                }
                return $matricula;
              }
              
}



