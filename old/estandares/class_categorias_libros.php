<?php
require_once("class_bdd.php"); 
require_once("class_usuarios.php");
class class_categorias_libros extends class_bdd{

      
    public $Id_cat;
	     
	     public function  __construct($Id_cat){
	     	   class_bdd::__construct();
	     	   if($Id_cat>0){
	     	   	  $this->Id_cat=$Id_cat;
	     	   }
	     }
	     
	     public function get_categorias(){
                 $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                 $usu = $class_usuarios->get_usu();
	         $resp=array();
		     $query_Categorias_libros = "SELECT * FROM Categorias_libros WHERE  Id_plantel=".$usu['Id_plantel']." AND Activo=1 AND Parent=0 ORDER BY Id_cat DESC";
		     $Categorias_libros = mysql_query($query_Categorias_libros, $this->cnn) or die(mysql_error());
		     $row_Categorias_libros= mysql_fetch_array($Categorias_libros);
		     $totalRows_Categorias_libros = mysql_num_rows($Categorias_libros);
		     if($totalRows_Categorias_libros>0){
                               do{
                                  array_push($resp, $this->get_categoria($row_Categorias_libros['Id_cat']));
                               }while($row_Categorias_libros= mysql_fetch_array($Categorias_libros));
		     }
		     return $resp;
	     }
             
	     
	     public function get_categoria($Id_cat=null){
	         if($Id_cat==null){
		        $Id_cat=$this->Id_cat;
	         }
	             $resp=array();
		     $query_Categorias_libros = "SELECT * FROM Categorias_libros WHERE Id_cat=".$Id_cat;
		     $Categorias_libros = mysql_query($query_Categorias_libros, $this->cnn) or die(mysql_error());
		     $row_Categorias_libros= mysql_fetch_array($Categorias_libros);
		     $totalRows_Categorias_libros = mysql_num_rows($Categorias_libros);
		     if($totalRows_Categorias_libros>0){
                        $resp['Id_cat']=$row_Categorias_libros['Id_cat'];
                        $resp['Codigo']=$row_Categorias_libros['Codigo'];
                        $resp['Nombre']=$row_Categorias_libros['Nombre'];
                        $resp['Codigo']=$row_Categorias_libros['Codigo'];
                        $resp['Parent']=$row_Categorias_libros['Parent'];
                        $resp['Id_plantel']=$row_Categorias_libros['Id_plantel'];
                        $resp['Activo']=$row_Categorias_libros['Activo'];
                        $resp['Subcategorias']=array();
                        $resp['Subcategorias']=$this->get_Subcategorias($row_Categorias_libros['Id_cat']);
		     }
		     return $resp;
	     }	 
             
            public function get_Subcategorias($Id_cat){
	         $resp=array();
		     $query_Categorias_libros = "SELECT * FROM Categorias_libros WHERE  Activo=1 AND Parent=".$Id_cat." ORDER BY Id_cat DESC";
		     $Categorias_libros = mysql_query($query_Categorias_libros, $this->cnn) or die(mysql_error());
		     $row_Categorias_libros= mysql_fetch_array($Categorias_libros);
		     $totalRows_Categorias_libros = mysql_num_rows($Categorias_libros);
		     if($totalRows_Categorias_libros>0){
                               do{
                                  array_push($resp, $this->get_categoria($row_Categorias_libros['Id_cat']));
                               }while($row_Categorias_libros= mysql_fetch_array($Categorias_libros));
		     }
		     return $resp;
	     }
              
}



