<?php
require_once("class_bdd.php");
class class_justificaciones extends class_bdd{

	     public $Id_jus;
	     
	     public function  __construct($Id_jus){
	     	   class_bdd::__construct();
	     	   if($Id_jus>0){
	     	   	  $this->Id_jus=$Id_jus;
	     	   }
	     }
	     
	     public function get_justificaciones(){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_Justificaciones = "SELECT * FROM Justificaciones WHERE Id_plantel=".$usu['Id_plantel']." ORDER BY Dia_justificado DESC";
		     $Justificaciones= mysql_query($query_Justificaciones, $this->cnn) or die(mysql_error());
		     $row_Justificaciones= mysql_fetch_array($Justificaciones);
		     $totalRows_Justificaciones  = mysql_num_rows($Justificaciones);
		     if($totalRows_Justificaciones>0){
			     do{
			      array_push($resp, $this->get_justificacion($row_Justificaciones['Id_jus']));
			     }while($row_Justificaciones= mysql_fetch_array($Justificaciones));
		     }
		     return $resp;
	     }
	     
	     public function get_justificacion($Id_jus=null){
	     	 if($Id_jus==null){
		        $Id_jus=$this->Id_jus;
	         }
	         $resp=array();
		     $query_Justificaciones  = "SELECT * FROM Justificaciones WHERE Id_jus=".$Id_jus;
		     $Justificaciones= mysql_query($query_Justificaciones, $this->cnn) or die(mysql_error());
		     $row_Justificaciones= mysql_fetch_array($Justificaciones);
		     $totalRows_Justificaciones  = mysql_num_rows($Justificaciones);
		     if($totalRows_Justificaciones>0){

			        $resp['Id_jus']=$row_Justificaciones['Id_jus'];
			        $resp['Id_alum']=$row_Justificaciones['Id_alum'];
                                $resp['Id_ciclo']=$row_Justificaciones['Id_ciclo'];
                                $resp['Dia_justificado']=$row_Justificaciones['Dia_justificado'];
                                $resp['Id_usu']=$row_Justificaciones['Id_usu'];
                                $resp['Date']=$row_Justificaciones['Date'];
                                $resp['Comentarios']=$row_Justificaciones['Comentarios'];
                                $resp['Id_plantel']=$row_Justificaciones['Id_plantel'];
                                $resp['HoraIni']=$row_Justificaciones['HoraIni'];
                                $resp['HoraFin']=$row_Justificaciones['HoraFin'];
		     }
		     return $resp;
	     }  
             
              public function get_justificacion_by_ciclo_alum($Id_alum,$Id_ciclo){
	             $resp=array();
		     $query_Justificaciones  = "SELECT * FROM Justificaciones WHERE Id_alum=".$Id_alum." AND Id_ciclo=".$Id_ciclo." ORDER BY Dia_justificado DESC";
		     $Justificaciones= mysql_query($query_Justificaciones, $this->cnn) or die(mysql_error());
		     $row_Justificaciones= mysql_fetch_array($Justificaciones);
		     $totalRows_Justificaciones  = mysql_num_rows($Justificaciones);
		     if($totalRows_Justificaciones>0){
                        do{
                           array_push($resp, $this->get_justificacion($row_Justificaciones['Id_jus']));
                        }while($row_Justificaciones= mysql_fetch_array($Justificaciones));
		     }
		     return $resp;
	     } 
             
             public function get_justificacion_by_grupo_alum($Id_alum,$Id_ciclo,$Id_grupo){
	             $resp=array();
		     $query_Justificaciones  = "SELECT * FROM Justificaciones WHERE Id_alum=".$Id_alum." AND Id_ciclo=".$Id_ciclo." ORDER BY Dia_justificado DESC";
		     $Justificaciones= mysql_query($query_Justificaciones, $this->cnn) or die(mysql_error());
		     $row_Justificaciones= mysql_fetch_array($Justificaciones);
		     $totalRows_Justificaciones  = mysql_num_rows($Justificaciones);
		     if($totalRows_Justificaciones>0){
                        do{
                                //Si se les justificosolo unas horas
                                if($row_Justificaciones['HoraIni']!=null && $row_Justificaciones['HoraFin']!=null){
                                    $Ini=  substr($row_Justificaciones['HoraIni'], 0,  strpos($row_Justificaciones['HoraIni'], ":"));
                                    $query_HoraIni = "SELECT * FROM Horas WHERE Texto_hora LIKE '%".$Ini."%'";
                                    $HoraIni = mysql_query($query_HoraIni, $this->cnn) or die(mysql_error());
                                    $row_HoraIni = mysql_fetch_array($HoraIni);

                                    $Fin=  substr($row_Justificaciones['HoraFin'], 0,  strpos($row_Justificaciones['HoraFin'], ":"));
                                    $query_HoraFin = "SELECT * FROM Horas WHERE Texto_hora LIKE '%".$Fin."%'";
                                    $HoraFin = mysql_query($query_HoraFin, $this->cnn) or die(mysql_error());
                                    $row_HoraFin = mysql_fetch_array($HoraFin);
                                    
                                    $queryHora="";
                                    if($row_HoraIni['Id_hora']==$row_HoraFin['Id_hora']){
                                        $queryHora=" AND Hora=".$row_HoraIni['Id_hora'];
                                    }else{
                                        $query_Rango = "SELECT * FROM Horas WHERE Id_hora>=".$row_HoraIni['Id_hora']." && Id_hora<".$row_HoraFin['Id_hora'];
                                        $Rango = mysql_query($query_Rango, $this->cnn) or die(mysql_error());
                                        $row_Rango = mysql_fetch_array($Rango);  
                                        $totalRows_Rango= mysql_num_rows($Rango);
                                        if($totalRows_Rango>0){
                                            do{
                                               $queryHora.="Hora=".$row_Rango['Id_hora']." || ";
                                            }while($row_Rango = mysql_fetch_array($Rango));
                                        }
                                        $queryHora=  substr($queryHora, 0, strripos($queryHora,'||'));
                                        $queryHora="AND (".$queryHora.")";
                                    }
                                    //Verificamos el dia justificado
                                    $dia=date("N",strtotime($row_Justificaciones['Dia_justificado']));  
                                    $queryDia="";
                                    if($dia==1){
                                           $queryDia="AND Lunes=1";
                                    }elseif($dia==2){
                                           $queryDia="AND Martes=1";
                                    }elseif($dia==3){
                                           $queryDia="AND Miercoles=1"; 
                                    }elseif($dia==4){
                                           $queryDia="AND Jueves=1"; 
                                    }elseif($dia==5){
                                           $queryDia="AND Viernes=1"; 
                                    }elseif($dia==6){
                                           $queryDia="AND Sabado=1"; 
                                    }
                                    //SELECT * FROM Horario_docente WHERE Id_ciclo=13 AND Id_grupo=801  AND Viernes=1
                                    $query_Horario_docente = "SELECT * FROM Horario_docente WHERE Id_ciclo=".$Id_ciclo." AND Id_grupo=".$Id_grupo." ".$queryHora." ".$queryDia;
                                    $Horario_docente = mysql_query($query_Horario_docente, $this->cnn) or die(mysql_error());
                                    $row_Horario_docente = mysql_fetch_array($Horario_docente);
                                    $totalRows_Horario_docente= mysql_num_rows($Horario_docente);
                                    if($totalRows_Horario_docente>0){
                                       array_push($resp, $this->get_justificacion($row_Justificaciones['Id_jus']));
                                    }
                                }else{
                                    //Si se justifico eldia completo
                                   array_push($resp, $this->get_justificacion($row_Justificaciones['Id_jus']));
                                }
                        }while($row_Justificaciones= mysql_fetch_array($Justificaciones));
		     }
		     return $resp;
	     } 
	
}
