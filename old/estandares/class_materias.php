<?php
require_once("class_bdd.php");
require_once("class_usuarios.php");
class class_materias extends class_bdd{

		 public $Id_mat;
	     
	     public function  __construct($Id_mat){
	     	   class_bdd::__construct();
	     	   if($Id_mat>0){
	     	   	  $this->Id_mat=$Id_mat;
	     	   }
	     }
	     
	     public function get_materias(){
                 $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                 $usu = $class_usuarios->get_usu();
	         $resp=array();
		     $query_materias = "SELECT * FROM materias_uml WHERE Activo_mat=1 AND Id_plantel=".$usu['Id_plantel']." ORDER BY Nombre ASC";
		     $materias = mysql_query($query_materias, $this->cnn) or die(mysql_error());
		     $row_materias= mysql_fetch_array($materias);
		     $totalRows_materias = mysql_num_rows($materias);
		     if($totalRows_materias>0){
			     do{
			        array_push($resp, $this->get_materia($row_materias['Id_mat']));
			     }while($row_materias= mysql_fetch_array($materias));
		     }
		     return $resp;
	     }
	     
	     public function get_materia($Id_mat=null){
	         if($Id_mat==null){
		        $Id_mat=$this->Id_mat;
	         }
	         $resp=array();
		     $query_materias = "SELECT * FROM materias_uml WHERE Id_mat=".$Id_mat;
		     $materias = mysql_query($query_materias, $this->cnn) or die(mysql_error());
		     $row_materias= mysql_fetch_array($materias);
		     $totalRows_materias = mysql_num_rows($materias);
		     if($totalRows_materias>0){
			        $resp['Id_mat']=$row_materias['Id_mat'];
			        $resp['Clave_plan']=$row_materias['Clave_plan'];
			        $resp['Nombre']=$row_materias['Nombre'];
			        $resp['Clave_mat']=$row_materias['Clave_mat'];
			        $resp['Precio']=$row_materias['Precio'];
			        $resp['Creditos']=$row_materias['Creditos'];
			        $resp['Tipo_aula']=$row_materias['Tipo_aula'];
			        $resp['Dias_porSemana']=$row_materias['Dias_porSemana'];
			        $resp['Id_esp']=$row_materias['Id_esp'];
			        $resp['Horas_independientes']=$row_materias['Horas_independientes'];
			        $resp['Horas_porSemana']=$row_materias['Horas_porSemana'];
			        $resp['Horas_conDocente']=$row_materias['Horas_conDocente'];
                                $resp['Promedio_min']=$row_materias['Promedio_min'];
                                $resp['Valor_cadaEvaluacion']=$row_materias['Valor_cadaEvaluacion'];
                                $resp['Duracion_examen']=$row_materias['Duracion_examen'];
                                $resp['Max_alumExamen']=$row_materias['Max_alumExamen'];
                                $resp['Compartidas']=array();
                                $resp['Compartidas']=$this->get_esp_materias($row_materias['Id_mat']);

		     }
		     return $resp;
	     }
             
           public function get_esp_materias($Id_mat) {
            $resp = array();
            $class_ofertas= new class_ofertas();
            $query_materias_uml = "SELECT * FROM Materias_especialidades WHERE Id_mat=" . $Id_mat . " AND Activo_mat_esp=1";
            $materias_uml = mysql_query($query_materias_uml, $this->cnn) or die(mysql_error());
            $row_materias_uml = mysql_fetch_assoc($materias_uml);
            $totalRows_materias_uml = mysql_num_rows($materias_uml);
            if ($totalRows_materias_uml > 0) {
                do {
                    array_push($resp, $this->get_esp_mat($row_materias_uml['Id_mat_esp']));
                } while ($row_materias_uml = mysql_fetch_assoc($materias_uml));
            }
            return $resp;
        }
        
        public function get_esp_mat($Id_mat_esp) {
            $resp = array();
            $class_ofertas= new class_ofertas();
            $query_materias_uml = "SELECT * FROM Materias_especialidades WHERE Id_mat_esp=" . $Id_mat_esp;
            $materias_uml = mysql_query($query_materias_uml, $this->cnn) or die(mysql_error());
            $row_materias_uml = mysql_fetch_assoc($materias_uml);
            $totalRows_materias_uml = mysql_num_rows($materias_uml);
            if ($totalRows_materias_uml > 0) {

                    $resp['Id_mat_esp']=$row_materias_uml['Id_mat_esp'];
                    $resp['Id_esp']=$row_materias_uml['Id_esp'];
                    $resp['Id_mat']=$row_materias_uml['Id_mat'];
                    $resp['Activo_mat_esp']=$row_materias_uml['Activo_mat_esp'];
                    $resp['Grado_mat']=$row_materias_uml['Grado_mat'];
                    $resp['Evaluacion_extraordinaria']=$row_materias_uml['Evaluacion_extraordinaria'];
                    $resp['Evaluacion_especial']=$row_materias_uml['Evaluacion_especial'];
                    $resp['Tiene_orientacion']=$row_materias_uml['Tiene_orientacion'];
                    $resp['NombreDiferente']=$row_materias_uml['NombreDiferente'];
                    $resp['Requisitos']=array();
                    $resp['Requisitos']=$this->get_requisitos_mat($row_materias_uml['Id_mat_esp']);
                    $resp['Evaluaciones']=array();
                    $resp['Evaluaciones']=$this->get_evaluaciones_mat($row_materias_uml['Id_mat_esp']);
            }
            return $resp;
        }
        
        public function get_requisitos_mat($Id_mat_esp){
         $resp=array();
             $query_prerequisitos = "SELECT * FROM prerequisitos_ulm WHERE Id_mat_esp=".$Id_mat_esp."  ORDER BY Id_mat_pre ASC";
             $prerequisitos = mysql_query($query_prerequisitos, $this->cnn) or die(mysql_error());
             $row_prerequisitos= mysql_fetch_array($prerequisitos);
             $totalRows_prerequisitos = mysql_num_rows($prerequisitos);
             if($totalRows_prerequisitos>0){
                do{
                    $pre= array();
                        $pre['Id_mat_pre']=$row_prerequisitos['Id_mat_pre'];
                        $pre['Id_mat_esp']=$row_prerequisitos['Id_mat_esp'];
                        $pre['Id_mat_esp_pre']=$row_prerequisitos['Id_mat_esp_pre'];
                        array_push($resp, $pre);

                    }while($row_prerequisitos= mysql_fetch_array($prerequisitos));
             }
             return $resp;
     }



     public function get_evaluaciones_mat($Id_mat_esp){
            $resp=array();
             $query_Evaluaciones = "SELECT * FROM Evaluaciones WHERE Id_mat_esp=".$Id_mat_esp;
             $Evaluaciones = mysql_query($query_Evaluaciones, $this->cnn) or die(mysql_error());
             $row_Evaluaciones= mysql_fetch_array($Evaluaciones);
             $totalRows_Evaluaciones = mysql_num_rows($Evaluaciones);
             if($totalRows_Evaluaciones>0){
                do{
                    $eva= array();
                        $eva['Id_eva']=$row_Evaluaciones['Id_eva'];
                        $eva['Nombre_eva']=$row_Evaluaciones['Nombre_eva'];
                        $eva['Ponderacion']=$row_Evaluaciones['Ponderacion'];
                        $eva['Id_mat_esp']=$row_Evaluaciones['Id_mat_esp'];
                        array_push($resp, $eva);

                    }while($row_Evaluaciones= mysql_fetch_array($Evaluaciones));
             }
             return $resp;
     }
     
     public function get_mat_grados($Id_grado_ofe) {
            $resp = array();
            $class_ofertas= new class_ofertas();
            $query_materias_uml = "SELECT * FROM Materias_especialidades 
            JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat
            WHERE Grado_mat=" . $Id_grado_ofe." AND Activo_mat_esp=1 AND Activo_mat=1";
            $materias_uml = mysql_query($query_materias_uml, $this->cnn) or die(mysql_error());
            $row_materias_uml = mysql_fetch_assoc($materias_uml);
            $totalRows_materias_uml = mysql_num_rows($materias_uml);
            if ($totalRows_materias_uml > 0) {
                do{
                    $mat= array();
                    $mat['Id_mat_esp']=$row_materias_uml['Id_mat_esp'];
                    $mat['Id_esp']=$row_materias_uml['Id_esp'];
                    $mat['Id_mat']=$row_materias_uml['Id_mat'];
                    $mat['Activo_mat_esp']=$row_materias_uml['Activo_mat_esp'];
                    $mat['Grado_mat']=$row_materias_uml['Grado_mat'];
                    $mat['Evaluacion_extraordinaria']=$row_materias_uml['Evaluacion_extraordinaria'];
                    $mat['Evaluacion_especial']=$row_materias_uml['Evaluacion_especial'];
                    $mat['Tiene_orientacion']=$row_materias_uml['Tiene_orientacion'];
                    $mat['NombreDiferente']=$row_materias_uml['NombreDiferente'];
                    $mat['Requisitos']=array();
                    $mat['Requisitos']=$this->get_requisitos_mat($row_materias_uml['Id_mat_esp']);
                    $mat['Evaluaciones']=array();
                    $mat['Evaluaciones']=$this->get_evaluaciones_mat($row_materias_uml['Id_mat_esp']);
                    
                    array_push($resp, $mat);
                }while($row_materias_uml = mysql_fetch_assoc($materias_uml));
            }
            return $resp;
        }
             
            
 
}
?>