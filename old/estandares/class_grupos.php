<?php
require_once("class_bdd.php");
require_once("class_justificaciones.php");

class class_grupos extends class_bdd{

	     public $Id_grupo;
	     
	     public function  __construct($Id_grupo){
	     	   class_bdd::__construct();
	     	   if($Id_grupo>0){
	     	   	  $this->Id_grupo=$Id_grupo;
	     	   }
	     }
	     
	     public function get_grupos(){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_Grupos  = "SELECT * FROM Grupos WHERE Activo_grupo=1 AND Id_plantel=".$usu['Id_plantel']." ORDER BY Id_grupo DESC";
		     $Grupos= mysql_query($query_Grupos, $this->cnn) or die(mysql_error());
		     $row_Grupos= mysql_fetch_array($Grupos);
		     $totalRows_Grupos  = mysql_num_rows($Grupos);
		     if($totalRows_Grupos>0){
			     do{
			      array_push($resp, $this->get_grupo($row_Grupos['Id_grupo']));
			     }while($row_Grupos= mysql_fetch_array($Grupos));
		     }
		     return $resp;
	     }
	     
	     public function get_grupo($Id_grupo=null){
	     	 if($Id_grupo==null){
		        $Id_grupo=$this->Id_grupo;
	         }
	         $resp=array();
		     $query_Grupos  = "SELECT * , Grupos.Id_grupo AS Grupo,Grupos.Id_ciclo AS Ciclo FROM Grupos 
                            LEFT JOIN Horario_docente ON Grupos.Id_grupo=Horario_docente.Id_grupo
                            LEFT JOIN Docentes ON Horario_docente.Id_docente=Docentes.Id_docen
                            WHERE Grupos.Id_grupo=".$Id_grupo;
		     $Grupos= mysql_query($query_Grupos, $this->cnn) or die(mysql_error());
		     $row_Grupos= mysql_fetch_array($Grupos);
		     $totalRows_Grupos  = mysql_num_rows($Grupos);
		     if($totalRows_Grupos>0){

                         
			        $resp['Id_grupo']=$row_Grupos['Grupo'];
			        $resp['Clave']=$row_Grupos['Clave'];
                                $resp['Turno']=$row_Grupos['Turno'];
                                $resp['Activo_grupo']=$row_Grupos['Activo_grupo'];
                                $resp['Id_ciclo']=$row_Grupos['Ciclo'];
                                $resp['Id_mat']=$row_Grupos['Id_mat'];
                                $resp['Id_ori']=$row_Grupos['Id_ori'];
                                $resp['Id_mat_esp']=$row_Grupos['Id_mat_esp'];
                                $resp['Id_plantel']=$row_Grupos['Id_plantel'];
                                $resp['Capacidad']=$row_Grupos['Capacidad'];
                                $resp['Id_docen']=$row_Grupos['Id_docen'];
                                $resp['Nombre_docen']=$row_Grupos['Nombre_docen']." ".$row_Grupos['ApellidoP_docen']." ".$row_Grupos['ApellidoM_docen'];
                                /*
                                 $query_disponibilidad  = "SELECT * FROM ofertas_alumno 
                JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
                JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
                JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
                WHERE  materias_ciclo_ulm.Id_grupo=".$Id_grupo." AND Activo_oferta=1";
                                 */
                                 
                                                                  $query_disponibilidad  = "SELECT * FROM ofertas_alumno 
                JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
                JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
                JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
                WHERE  materias_ciclo_ulm.Id_grupo=".$Id_grupo." AND Activo_oferta=1";
                                 $disponibilidad= mysql_query($query_disponibilidad, $this->cnn) or die(mysql_error());
                                 $row_disponibilidad= mysql_fetch_array($disponibilidad);
                                 $totalRows_disponibilidad  = mysql_num_rows($disponibilidad);
                                 
                                 $resp['Id_ofe_alum']=$row_disponibilidad['Id_ofe_alum'];
                                 $resp['Disponibilidad']=$row_Grupos['Capacidad']-$totalRows_disponibilidad;
                                 $resp['Alumnos']=$this->get_alumnos_grupo($Id_grupo);
		     }
		     return $resp;
	     }
             
             public function get_grupos_by_ciclo($Id_ciclo){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_Grupos  = "SELECT * FROM Grupos WHERE Id_ciclo=".$Id_ciclo." AND Id_plantel=".$usu['Id_plantel']." AND Activo_grupo=1 ORDER BY Id_grupo DESC";
		     $Grupos= mysql_query($query_Grupos, $this->cnn) or die(mysql_error());
		     $row_Grupos= mysql_fetch_array($Grupos);
		     $totalRows_Grupos  = mysql_num_rows($Grupos);
		     if($totalRows_Grupos>0){
			     do{
			        array_push($resp, $this->get_grupo($row_Grupos['Id_grupo']));
			     }while($row_Grupos= mysql_fetch_array($Grupos));
		     }
		     return $resp;
	     }
             
             
             
            public function get_alumnos_grupo($Id_grupo){
	         $resp=array();
                 if($Id_grupo>0){
                     /*
		     $query_Alumnos_grupo  = "SELECT * FROM ofertas_alumno 
                JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
                JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
                JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
                WHERE  materias_ciclo_ulm.Id_grupo=".$Id_grupo." AND Activo_oferta=1";
                      */
                $query_Alumnos_grupo  = "SELECT * FROM ofertas_alumno 
                JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
                JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum=materias_ciclo_ulm.Id_ciclo_alum
                JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
                WHERE  materias_ciclo_ulm.Id_grupo=".$Id_grupo." AND Activo_oferta=1";
		     $Alumnos_grupo= mysql_query($query_Alumnos_grupo, $this->cnn) or die(mysql_error());
		     $row_Alumnos_grupo= mysql_fetch_array($Alumnos_grupo);
		     $totalRows_Alumnos_grupo  = mysql_num_rows($Alumnos_grupo);
		     if($totalRows_Alumnos_grupo>0){
			     do{
                                $alumno_grupo=array();
                                $alumno_grupo['Id_ofe_alum']=$row_Alumnos_grupo['Id_ofe_alum'];
                                $alumno_grupo['Id_ciclo_alum']=$row_Alumnos_grupo['Id_ciclo_alum'];
                                $alumno_grupo['Id_ciclo_mat']=$row_Alumnos_grupo['Id_ciclo_mat'];
                                $alumno_grupo['Id_grupo']=$row_Alumnos_grupo['Id_grupo'];
                                $alumno_grupo['Id_alum']=$row_Alumnos_grupo['Id_alum'];
                                $alumno_grupo['CalTotalParciales']=$row_Alumnos_grupo['CalTotalParciales'];
                                $alumno_grupo['CalExtraordinario']=$row_Alumnos_grupo['CalExtraordinario'];
                                $alumno_grupo['CalEspecial']=$row_Alumnos_grupo['CalEspecial'];
                                $alumno_grupo['Evaluacion_extraordinaria']=$row_Alumnos_grupo['Evaluacion_extraordinaria'];
                                $alumno_grupo['Evaluacion_especial']=$row_Alumnos_grupo['Evaluacion_especial'];
                                $alumno_grupo['NombreDiferente']=$row_Alumnos_grupo['NombreDiferente'];
			        array_push($resp, $alumno_grupo);
			     }while($row_Alumnos_grupo= mysql_fetch_array($Alumnos_grupo));
		     }
                 } 
                  
		     return $resp;
	     }
             
             public function get_grupos_by_materia($Id_mat,$Id_ciclo){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_Grupos  = "SELECT * FROM Grupos WHERE Id_mat=".$Id_mat." AND Id_plantel=".$usu['Id_plantel']." AND Id_ciclo=".$Id_ciclo." AND Activo_grupo=1 ORDER BY Id_grupo DESC";
		     $Grupos= mysql_query($query_Grupos, $this->cnn) or die(mysql_error());
		     $row_Grupos= mysql_fetch_array($Grupos);
		     $totalRows_Grupos  = mysql_num_rows($Grupos);
		     if($totalRows_Grupos>0){
			     do{
			        array_push($resp, $this->get_grupo($row_Grupos['Id_grupo']));
			     }while($row_Grupos= mysql_fetch_array($Grupos));
		     }
		     return $resp;
	     }
             
             public function get_asistencias_grupo_alum($Id_grupo,$Id_alum){
	            $resp=array();
                    $Asis = 0;
                    $Faltas = 0;
                    $Just = 0;
                    $query_Asistencias = "SELECT * FROM Asistencias WHERE Id_grupo=" . $Id_grupo . " AND Id_alum=" . $Id_alum;
                    $Asistencias = mysql_query($query_Asistencias, $this->cnn) or die(mysql_error());
                    $row_Asistencias = mysql_fetch_array($Asistencias);
                    $totalRows_Asistencias = mysql_num_rows($Asistencias);
                    if ($totalRows_Asistencias > 0) {
                        do {
                            if ($row_Asistencias['Asistio'] == 1) {
                                $Asis++;
                            } elseif ($row_Asistencias['Asistio'] == 0) {
                                $Faltas++;
                            }
                        } while ($row_Asistencias = mysql_fetch_array($Asistencias));
                    }
                    
                    $grupo=$this->get_grupo($Id_grupo);
                    //Si se justifica una falta espor todo el dia y para todos los grupos
                    $class_justificaciones = new class_justificaciones();
                    foreach ($class_justificaciones->get_justificacion_by_grupo_alum($Id_alum, $grupo['Id_ciclo'],$Id_grupo)as $v) {
                        $Just++;
                    }
                    $resp['Asistencias']=$Asis;
                    $resp['Faltas']=$Faltas;
                    $resp['Justificaciones']=$Just;
		    return $resp;
	     }

	
}
