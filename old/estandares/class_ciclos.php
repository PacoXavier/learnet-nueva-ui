<?php
require_once("class_bdd.php");
require_once("class_usuarios.php");
class class_ciclos extends class_bdd{

	     public $Id_ciclo;
	     
	     public function  __construct($Id_ciclo){
	     	   class_bdd::__construct();
	     	   if($Id_ciclo>0){
	     	   	  $this->Id_ciclo=$Id_ciclo;
	     	   }
	     }
	     
	     public function get_ciclos(){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_ciclos = "SELECT * FROM ciclos_ulm WHERE Activo_ciclo=1 AND Id_plantel=".$usu['Id_plantel']." ORDER BY Id_ciclo DESC";
		     $ciclos= mysql_query($query_ciclos, $this->cnn) or die(mysql_error());
		     $row_ciclos= mysql_fetch_array($ciclos);
		     $totalRows_ciclos  = mysql_num_rows($ciclos);
		     if($totalRows_ciclos>0){
			     do{
			      array_push($resp, $this->get_ciclo($row_ciclos['Id_ciclo']));
			     }while($row_ciclos= mysql_fetch_array($ciclos));
		     }
		     return $resp;
	     }
	     
	     public function get_ciclo($Id_ciclo=null){
	     	 if($Id_ciclo==null){
		        $Id_ciclo=$this->Id_ciclo;
	         }
	         $resp=array();
		     $query_ciclos_ulm  = "SELECT * FROM ciclos_ulm WHERE Id_ciclo=".$Id_ciclo;
		     $ciclos_ulm= mysql_query($query_ciclos_ulm, $this->cnn) or die(mysql_error());
		     $row_ciclos_ulm= mysql_fetch_array($ciclos_ulm);
		     $totalRows_ciclos_ulm  = mysql_num_rows($ciclos_ulm);
		     if($totalRows_ciclos_ulm>0){

			        $resp['Id_ciclo']=$row_ciclos_ulm['Id_ciclo'];
			        $resp['Nombre_ciclo']=$row_ciclos_ulm['Nombre_ciclo'];
                                $resp['Clave']=$row_ciclos_ulm['Clave'];
                                $resp['Fecha_ini']=$row_ciclos_ulm['Fecha_ini'];
                                $resp['Fecha_fin']=$row_ciclos_ulm['Fecha_fin'];
                                $resp['Activo_ciclo']=$row_ciclos_ulm['Activo_ciclo'];
                                $resp['Id_plantel']=$row_ciclos_ulm['Id_plantel'];
                                $resp['Dias_inhabilies']=array();
                                $resp['Dias_inhabilies']=$this->get_dias_inhabiles_ciclo($row_ciclos_ulm['Id_ciclo']);
		     }
		     return $resp;
	     }  
             
            public function get_actual(){
                $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                $usu = $class_usuarios->get_usu();
                
                $query_ciclo_actual = "SELECT  * FROM ciclos_ulm  WHERE Fecha_ini<='".date('Y-m-d')."' AND Fecha_fin>='".date('Y-m-d')."' AND Activo_ciclo=1 AND Id_plantel=".$usu['Id_plantel'];
                $ciclo_actual= mysql_query($query_ciclo_actual, $this->cnn) or die(mysql_error());
                $row_ciclo_actual= mysql_fetch_array($ciclo_actual);
                $totalRows_ciclo_actual = mysql_num_rows($ciclo_actual);
		if($totalRows_ciclo_actual>0){
                    $resp['Id_ciclo']=$row_ciclo_actual['Id_ciclo'];
                    $resp['Nombre_ciclo']=$row_ciclo_actual['Nombre_ciclo'];
                    $resp['Clave']=$row_ciclo_actual['Clave'];
                    $resp['Fecha_ini']=$row_ciclo_actual['Fecha_ini'];
                    $resp['Fecha_fin']=$row_ciclo_actual['Fecha_fin'];
                    $resp['Activo_ciclo']=$row_ciclo_actual['Activo_ciclo'];
                    $resp['Id_plantel']=$row_ciclo_actual['Id_plantel'];
		 }
		 return $resp;
	     }  
             
             public function get_ciclos_futuros(){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
                     $ciclo=$this->get_actual();
                     if(strlen($ciclo['Fecha_ini'])>0){
                         $query=$ciclo['Fecha_ini'];
                     }else{
                         $query=date('Y-m-d');
                     }
		     $query_ciclos = "SELECT * FROM ciclos_ulm WHERE Activo_ciclo=1 AND Id_plantel=".$usu['Id_plantel']." AND Fecha_ini>='".$query."' ORDER BY Id_ciclo DESC";
		     $ciclos= mysql_query($query_ciclos, $this->cnn) or die(mysql_error());
		     $row_ciclos= mysql_fetch_array($ciclos);
		     $totalRows_ciclos  = mysql_num_rows($ciclos);
		     if($totalRows_ciclos>0){
			     do{
			      array_push($resp, $this->get_ciclo($row_ciclos['Id_ciclo']));
			     }while($row_ciclos= mysql_fetch_array($ciclos));
		     }
		     return $resp;
	     }
             
             public function get_dias_inhabiles_ciclo($Id_ciclo){
                    $resp=array();
	            $query_Dias_inhabiles_ciclo = "SELECT * FROM Dias_inhabiles_ciclo WHERE Id_ciclo=".$Id_ciclo;
	            $ciclos_Dias_inhabiles_ciclo = mysql_query($query_Dias_inhabiles_ciclo, $this->cnn) or die(mysql_error());
	            $row_Dias_inhabiles_ciclo = mysql_fetch_array($ciclos_Dias_inhabiles_ciclo);
	            $totalRows_Dias_inhabiles_ciclo = mysql_num_rows($ciclos_Dias_inhabiles_ciclo);   
	            if($totalRows_Dias_inhabiles_ciclo>0){
	               do{
                            $Dia=array();
                            $Dia['Id_dia']=$row_Dias_inhabiles_ciclo['Id_dia'];
                            $Dia['fecha_inh']=$row_Dias_inhabiles_ciclo['fecha_inh'];
                            $Dia['Id_ciclo']=$row_Dias_inhabiles_ciclo['Id_ciclo'];
                            $Dia['EsFinDeSemana']=$row_Dias_inhabiles_ciclo['EsFinDeSemana'];
                            array_push($resp, $Dia);
                       }while($row_Dias_inhabiles_ciclo = mysql_fetch_array($ciclos_Dias_inhabiles_ciclo));
                     }
		     return $resp;
	     }  
	
}
