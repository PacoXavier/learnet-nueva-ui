<?php
require_once("class_bdd.php");
class class_grados extends class_bdd{
 
	     public $Id_grado;
	     
	     public function  __construct($Id_grado){
	     	   class_bdd::__construct();
	     	   if($Id_grado>0){
	     	   	  $this->Id_grado=$Id_grado;
	     	   }
	     }
	    
	     public function get_grados(){
	             $resp=array();
		     $query_grados_uml  = "SELECT * FROM grados_ulm  WHERE Activo_grado=1";
		     $grados_uml= mysql_query($query_grados_uml, $this->cnn) or die(mysql_error());
		     $row_grados_uml= mysql_fetch_array($grados_uml);
		     $totalRows_grados_uml  = mysql_num_rows($grados_uml);
		     if($totalRows_grados_uml>0){
			     do{
			      array_push($resp, $this->get_grado($row_grados_uml['Id_grado_ofe']));
			     }while($row_grados_uml= mysql_fetch_array($grados_uml));
		     }
		     return $resp;
	     }
	     
	     public function get_grado($Id_grado=null){
	     	 if($Id_grado==null){
		        $Id_grado=$this->Id_grado;
	         }
	         $resp=array();
                 $query_grados_uml  = "SELECT * FROM grados_ulm WHERE Id_grado_ofe=".$Id_grado;
                 $grados_uml= mysql_query($query_grados_uml, $this->cnn) or die(mysql_error());
                 $row_grados_uml= mysql_fetch_array($grados_uml);
                 $totalRows_grados_uml  = mysql_num_rows($grados_uml);
                 if($totalRows_grados_uml>0){
                    $resp['Id_grado_ofe']=$row_grados_uml['Id_grado_ofe'];
                    $resp['Id_esp']=$row_grados_uml['Id_esp'];
                    $resp['Grado']=$row_grados_uml['Grado'];
                    $resp['Activo_grado']=$row_grados_uml['Activo_grado'];   
                 }
                 return $resp;
	     }
}
