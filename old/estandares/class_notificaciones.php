<?php
require_once("class_bdd.php");
require_once("class_usuarios.php");

class class_notificaciones extends class_bdd{

	     public $Id_not;
	     
	     public function  __construct($Id_not){
	     	   class_bdd::__construct();
	     	   if($Id_not>0){
	     	   	  $this->Id_not=$Id_not;
	     	   }
	     }
	     
	     public function get_notificaciones(){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_Notificaciones  = "SELECT * FROM Notificaciones WHERE Id_plantel=".$usu['Id_plantel']." AND Activo=1 ORDER BY Id_not DESC";
		     $Notificaciones= mysql_query($query_Notificaciones, $this->cnn) or die(mysql_error());
		     $row_Notificaciones= mysql_fetch_array($Notificaciones);
		     $totalRows_Notificaciones  = mysql_num_rows($Notificaciones);
		     if($totalRows_Notificaciones>0){
			     do{
			      array_push($resp, $this->get_notificacion($row_Notificaciones['Id_not']));
			     }while($row_Notificaciones= mysql_fetch_array($Notificaciones));
		     }
		     return $resp;
	     }
	     
             
	     public function get_notificacion($Id_not=null){
	     	 if($Id_not==null){
		        $Id_not=$this->Id_not;
	         }
	             $resp=array();
		     $query_Notificaciones  = "SELECT * FROM Notificaciones WHERE Id_not=".$Id_not;
		     $Notificaciones=mysql_query($query_Notificaciones, $this->cnn) or die(mysql_error());
		     $row_Notificaciones= mysql_fetch_array($Notificaciones);
		     $totalRows_Notificaciones  = mysql_num_rows($Notificaciones);
		     if($totalRows_Notificaciones>0){

			        $resp['Id_not']=$row_Notificaciones['Id_not'];
			        $resp['Titulo']=$row_Notificaciones['Titulo'];
                                $resp['Texto']=$row_Notificaciones['Texto'];
                                $resp['DateCreated']=$row_Notificaciones['DateCreated'];
                                $resp['DateInit']=$row_Notificaciones['DateInit'];
                                $resp['Id_usu']=$row_Notificaciones['Id_usu'];
                                $resp['Id_plantel']=$row_Notificaciones['Id_plantel'];
                                $resp['Tipo']=$row_Notificaciones['Tipo'];
                                $resp['Tipos_usu']=$row_Notificaciones['Tipos_usu'];
                                $resp['Activo']=$row_Notificaciones['Activo'];
		     }
		     return $resp;
	     }
             
             public function get_not_tipo($TipoRel,$IdRel){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_Notificaciones_usuarios  = "
                            SELECT * FROM Notificaciones_usuarios
                            JOIN (SELECT * FROM Notificaciones WHERE DateInit<='".date('Y-m-d')."' AND Id_plantel=".$usu['Id_plantel'].") 
                            AS Noti ON Notificaciones_usuarios.Id_not=Noti.Id_not 
                            WHERE IdRel=".$IdRel." AND TipoRel='".$TipoRel."' AND DateRead IS NULL ORDER BY Id DESC";
		     $Notificaciones_usuarios= mysql_query($query_Notificaciones_usuarios, $this->cnn) or die(mysql_error());
		     $row_Notificaciones_usuarios= mysql_fetch_array($Notificaciones_usuarios);
		     $totalRows_Notificaciones_usuarios  = mysql_num_rows($Notificaciones_usuarios);
		     if($totalRows_Notificaciones_usuarios>0){
			     do{
			        array_push($resp, $this->get_notificacion($row_Notificaciones_usuarios['Id_not']));
                                $resp[count($resp)-1]['Id']=$row_Notificaciones_usuarios['Id'];
			     }while($row_Notificaciones_usuarios= mysql_fetch_array($Notificaciones_usuarios));
		     }
		     return $resp;
	     }
             
             


	
}
