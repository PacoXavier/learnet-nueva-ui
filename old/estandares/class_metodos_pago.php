<?php
require_once("class_bdd.php"); 
require_once("class_usuarios.php");
class class_metodos_pago extends class_bdd{

             public $Id_metodo;
	     
	     public function  __construct($Id_metodo){
	     	   class_bdd::__construct();
	     	   if($Id_metodo>0){
	     	   	  $this->Id_metodo=$Id_metodo;
	     	   }
	     }
	     
	     public function get_metodos(){
                 $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                 $usu = $class_usuarios->get_usu();
	         $resp=array();
		     $query_Metodo_pago = "SELECT * FROM Metodo_pago WHERE  Id_plantel=".$usu['Id_plantel']." AND Activo=1 ORDER BY Id_met DESC";
		     $Metodo_pago = mysql_query($query_Metodo_pago, $this->cnn) or die(mysql_error());
		     $row_Metodo_pago= mysql_fetch_array($Metodo_pago);
		     $totalRows_Metodo_pago = mysql_num_rows($Metodo_pago);
		     if($totalRows_Metodo_pago>0){
                               do{
                                  array_push($resp, $this->get_metodo($row_Metodo_pago['Id_met']));
                               }while($row_Metodo_pago= mysql_fetch_array($Metodo_pago));
		     }
		     return $resp;
	     }
	     
	     public function get_metodo($Id_metodo=null){
	         if($Id_metodo==null){
		        $Id_metodo=$this->Id_metodo;
	         }
	         $resp=array();
		     $query_Metodo_pago = "SELECT * FROM Metodo_pago WHERE Id_met=".$Id_metodo;
		     $Metodo_pago = mysql_query($query_Metodo_pago, $this->cnn) or die(mysql_error());
		     $row_Metodo_pago= mysql_fetch_array($Metodo_pago);
		     $totalRows_Metodo_pago = mysql_num_rows($Metodo_pago);
		     if($totalRows_Metodo_pago>0){
                        $resp['Id_met']=$row_Metodo_pago['Id_met'];
                        $resp['Nombre']=$row_Metodo_pago['Nombre'];
                        $resp['Id_plantel']=$row_Metodo_pago['Id_plantel'];
                        $resp['Activo']=$row_Metodo_pago['Activo'];

		     }
		     return $resp;
	     }	
              
}



