<?php
require_once("class_bdd.php");
require_once("class_usuarios.php");
class class_pagos_adelantados extends class_bdd{

	     public $Id_pad;
	     
	     public function  __construct($Id_pad){
	     	   class_bdd::__construct();
	     	   if($Id_pad>0){
	     	   	  $this->Id_pad=$Id_pad;
	     	   }
	     }
	     
	     public function get_pagos_adelantados(){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_Pagos_adelantados = "SELECT * FROM Pagos_adelantados WHERE  Id_plantel=".$usu['Id_plantel']." ORDER BY Id_pad DESC";
		     $Pagos_adelantados= mysql_query($query_Pagos_adelantados, $this->cnn) or die(mysql_error());
		     $row_Pagos_adelantados= mysql_fetch_array($Pagos_adelantados);
		     $totalRows_Pagos_adelantados  = mysql_num_rows($Pagos_adelantados);
		     if($totalRows_Pagos_adelantados>0){
			     do{
			      array_push($resp, $this->get_pago_adelantado($row_Pagos_adelantados['Id_pad']));
			     }while($row_Pagos_adelantados= mysql_fetch_array($Pagos_adelantados));
		     }
		     return $resp;
	     }
	     
	     public function get_pago_adelantado($Id_pad=null){
	     	 if($Id_pad==null){
		        $Id_pad=$this->Id_pad;
	         }
	             $resp=array();
		     $query_Pagos_adelantados  = "SELECT * FROM Pagos_adelantados WHERE Id_pad=".$Id_evento;
		     $Pagos_adelantados= mysql_query($query_Pagos_adelantados, $this->cnn) or die(mysql_error());
		     $row_Pagos_adelantados= mysql_fetch_array($Pagos_adelantados);
		     $totalRows_Pagos_adelantados  = mysql_num_rows($Pagos_adelantados);
		     if($totalRows_Pagos_adelantados>0){
                        $resp['Id_pad']=$row_Pagos_adelantados['Id_pad'];
                        $resp['Fecha_pago']=$row_Pagos_adelantados['Fecha_pago'];
                        $resp['Monto_pago']=$row_Pagos_adelantados['Monto_pago'];
                        $resp['Saldo']=$row_Pagos_adelantados['Saldo'];
                        $resp['Id_usu']=$row_Pagos_adelantados['Id_usu'];
                        $resp['Id_ofe_alum']=$row_Pagos_adelantados['Id_ofe_alum'];
                        $resp['Id_alum']=$row_Pagos_adelantados['Id_alum'];
                        $resp['Ids_pagos']=$row_Pagos_adelantados['Ids_pagos'];
                        $resp['Id_met_pago']=$row_Pagos_adelantados['Id_met_pago'];
                        $resp['Id_ciclo_usar']=$row_Pagos_adelantados['Id_ciclo_usar'];
                        $resp['Id_paq_des']=$row_Pagos_adelantados['Id_paq_des'];
                        $resp['DateCreated']=$row_Pagos_adelantados['DateCreated'];
                        $resp['Id_plantel']=$row_Pagos_adelantados['Id_plantel'];
                        $resp['Id_plantel']=$row_Pagos_adelantados['Id_pp'];
                        $resp['Desglose']=array();
                        $resp['Desglose']=$this->get_pagos_adelantados($row_Pagos_adelantados['Id_pad']);
		     }
		     return $resp;
	     }  
             
             
              public function get_desgloses_pago($Id_pad){
	             $resp=array();
		     $query_Desglose_pagoAdelantado = "SELECT * FROM Desglose_pagoAdelantado WHERE  Id_pad=".$Id_pad." ORDER BY Id ASC";
		     $Desglose_pagoAdelantado= mysql_query($query_Desglose_pagoAdelantado, $this->cnn) or die(mysql_error());
		     $row_Desglose_pagoAdelantado= mysql_fetch_array($Desglose_pagoAdelantado);
		     $totalRows_Desglose_pagoAdelantado  = mysql_num_rows($Desglose_pagoAdelantado);
		     if($totalRows_Desglose_pagoAdelantado>0){
			     do{
			      array_push($resp, $this->get_desglose($row_Desglose_pagoAdelantado['Id']));
			     }while($row_Desglose_pagoAdelantado= mysql_fetch_array($Desglose_pagoAdelantado));
		     }
		     return $resp;
	     }
             
              public function get_desglose($Id){
	             $resp=array();
		     $query_Desglose_pagoAdelantado = "SELECT * FROM Desglose_pagoAdelantado WHERE  Id=".$Id;
		     $Desglose_pagoAdelantado= mysql_query($query_Desglose_pagoAdelantado, $this->cnn) or die(mysql_error());
		     $row_Desglose_pagoAdelantado= mysql_fetch_array($Desglose_pagoAdelantado);
		     $totalRows_Desglose_pagoAdelantado  = mysql_num_rows($Desglose_pagoAdelantado);
		     if($totalRows_Desglose_pagoAdelantado>0){
                       $resp['Id']=$row_Desglose_pagoAdelantado['Id'];
                       $resp['Id_pad']=$row_Desglose_pagoAdelantado['Id_pad'];
                       $resp['Id_pago_ciclo']=$row_Desglose_pagoAdelantado['Id_pago_ciclo'];
                       $resp['Monto']=$row_Desglose_pagoAdelantado['Monto'];
                       $resp['FechaUtiliza']=$row_Desglose_pagoAdelantado['FechaUtiliza'];
                       $resp['Activo']=$row_Desglose_pagoAdelantado['Activo'];
		     }
		     return $resp;
	     }
         
}
