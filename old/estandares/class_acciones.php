<?php
require_once("class_bdd.php");
class class_acciones extends class_bdd{

		 public $Id_accion;
	     
	     public function  __construct($Id_accion){
	     	   class_bdd::__construct();
	     	   if($Id_accion>0){
	     	   	  $this->Id_accion=$Id_accion;
	     	   }
	     }
	     
	     public function get_acciones(){
                 $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                 $usu = $class_usuarios->get_usu();
	         $resp=array();
		     $query_Acciones_interesado  = "SELECT * FROM Acciones_interesado WHERE Plantel=".$usu['Id_plantel']." AND Asistio IS NULL ORDER BY Id_accion DESC";
		     $Acciones_interesado= mysql_query($query_Acciones_interesado, $this->cnn) or die(mysql_error());
		     $row_Acciones_interesado= mysql_fetch_array($Acciones_interesado);
		     $totalRows_Acciones_interesado  = mysql_num_rows($Acciones_interesado);
		     if($totalRows_Acciones_interesado>0){
			     do{
			      array_push($resp, $this->get_accion($row_Acciones_interesado['Id_accion']));
			     }while($row_Acciones_interesado= mysql_fetch_array($Acciones_interesado));
		     }
		     return $resp;
	     }
	     
	     public function get_accion($Id_accion=null){
	     	 if($Id_accion==null){
		        $Id_accion=$this->Id_accion;
	         }
	         $resp=array();
		     $query_Acciones_interesado  = "SELECT * FROM Acciones_interesado WHERE Id_accion=".$Id_accion;
		     $Acciones_interesado= mysql_query($query_Acciones_interesado, $this->cnn) or die(mysql_error());
		     $row_Acciones_interesado= mysql_fetch_array($Acciones_interesado);
		     $totalRows_Acciones_interesado  = mysql_num_rows($Acciones_interesado);
		     if($totalRows_Acciones_interesado>0){

			        $resp['Id_accion']=$row_Acciones_interesado['Id_accion'];
			        $resp['Id_int']=$row_Acciones_interesado['Id_int'];
                                $resp['TipoAccion']=$row_Acciones_interesado['TipoAccion'];
                                $resp['Id_ofe_alum']=$row_Acciones_interesado['Id_ofe_alum'];
                                $resp['Asistio']=$row_Acciones_interesado['Asistio'];
                                $resp['CostoAccion']=$row_Acciones_interesado['CostoAccion'];
                                $resp['FechaDeposito']=$row_Acciones_interesado['FechaDeposito'];
                                $resp['Pago']=$row_Acciones_interesado['Pago'];
                                $resp['FechaAccion']=$row_Acciones_interesado['FechaAccion'];
                                $resp['CalificacionExamen']=$row_Acciones_interesado['CalificacionExamen'];
                                $resp['FechaSession']=$row_Acciones_interesado['FechaSession'];
                                $resp['Usuario']=$row_Acciones_interesado['Usuario'];
                                $resp['Plantel']=$row_Acciones_interesado['Plantel'];

		     }
		     return $resp;
	     }

	
}
