<?php
require_once("class_bdd.php");
class class_descuentos extends class_bdd{

	     public $Id_des;
	     
	     public function  __construct($Id_des){
	     	   class_bdd::__construct();
	     	   if($Id_des>0){
	     	   	  $this->Id_des=$Id_des;
	     	   }
	     }
	     
	     public function get_descuentos(){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_Paquetes_descuentos  = "SELECT * FROM Paquetes_descuentos WHERE Activo_des=1 AND 
                     ((Fecha_inicio<='".date('Y-m-d')."' AND Fecha_fin>='".date('Y-m-d')."') || (Fecha_inicio IS NULL AND Fecha_fin IS NULL)) AND Id_plantel=".$usu['Id_plantel']." ORDER BY Nombre ASC";
		     $Paquetes_descuentos= mysql_query($query_Paquetes_descuentos, $this->cnn) or die(mysql_error());
		     $row_Paquetes_descuentos= mysql_fetch_array($Paquetes_descuentos);
		     $totalRows_Paquetes_descuentos  = mysql_num_rows($Paquetes_descuentos);
		     if($totalRows_Paquetes_descuentos>0){
                         do{
                          array_push($resp, $this->get_descuento($row_Paquetes_descuentos['Id_des']));
                         }while($row_Paquetes_descuentos= mysql_fetch_array($Paquetes_descuentos));
		     }
		     return $resp;
                     
	     }
	     
	     public function get_descuento($Id_des=null){
	     	 if($Id_des==null){
		      $Id_des=$this->Id_des;
	         }
	         $resp=array();
                 $query_Paquetes_descuentos  = "SELECT * FROM Paquetes_descuentos WHERE Id_des=".$Id_des;
                 $Paquetes_descuentos= mysql_query($query_Paquetes_descuentos, $this->cnn) or die(mysql_error());
                 $row_Paquetes_descuentos= mysql_fetch_array($Paquetes_descuentos);
                 $totalRows_Paquetes_descuentos  = mysql_num_rows($Paquetes_descuentos);
                 if($totalRows_Paquetes_descuentos>0){
                            $resp['Id_des']=$row_Paquetes_descuentos['Id_des'];
                            $resp['Fecha_inicio']=$row_Paquetes_descuentos['Fecha_inicio'];
                            $resp['Fecha_fin']=$row_Paquetes_descuentos['Fecha_fin'];
                            $resp['Nombre']=$row_Paquetes_descuentos['Nombre'];
                            $resp['Activo_des']=$row_Paquetes_descuentos['Activo_des'];
                            $resp['Porcentaje']=$row_Paquetes_descuentos['Porcentaje'];
                 }
                 return $resp;
	     }
             
             
	
}
