<?php
require_once("class_bdd.php");
require_once("class_permisos.php");
require_once("class_usuarios.php");
class class_docentes extends class_bdd{

		 public $Id_docen;
	     public $description;
	     
	     public function  __construct($Id_docen){
	     	   class_bdd::__construct();
	     	   if($Id_docen>0){
	     	   	  $this->Id_docen=$Id_docen;
	     	   }
	     }
	     
	     public function get_docentes(){
                    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                    $usu = $class_usuarios->get_usu();
	         $resp=array();
		     $query_usuarios_ulm = "SELECT * FROM Docentes WHERE Baja_docen IS NULL AND Id_plantel=".$usu['Id_plantel']." ORDER BY Id_docen DESC";
		     $usuarios_ulm = mysql_query($query_usuarios_ulm, $this->cnn) or die(mysql_error());
		     $row_usuarios_ulm= mysql_fetch_array($usuarios_ulm);
		     $totalRows_usuarios_ulm = mysql_num_rows($usuarios_ulm);
		     if($totalRows_usuarios_ulm>0){
			     do{
			        array_push($resp, $this->get_docente($row_usuarios_ulm['Id_docen']));
			     }while($row_usuarios_ulm= mysql_fetch_array($usuarios_ulm));
		     }
		     return $resp;
	     }
	     
	     public function get_docente($Id_docen=null){
	         if($Id_docen==null){
		        $Id_docen=$this->Id_docen;
	         }
	         $resp=array();
		     $query_usuarios_ulm = "SELECT * FROM Docentes WHERE Id_docen=".$Id_docen;
		     $usuarios_ulm = mysql_query($query_usuarios_ulm, $this->cnn) or die(mysql_error());
		     $row_usuarios_ulm= mysql_fetch_array($usuarios_ulm);
		     $totalRows_usuarios_ulm = mysql_num_rows($usuarios_ulm);
		     if($totalRows_usuarios_ulm>0){

			        $resp['Id_docen']=$row_usuarios_ulm['Id_docen'];
			        $resp['Nombre_docen']=$row_usuarios_ulm['Nombre_docen'];
			        $resp['ApellidoP_docen']=$row_usuarios_ulm['ApellidoP_docen'];
			        $resp['ApellidoM_docen']=$row_usuarios_ulm['ApellidoM_docen'];
			        $resp['Alta_docen']=$row_usuarios_ulm['Alta_docent'];
			        $resp['Baja_docen']=$row_usuarios_ulm['Baja_docen'];
			        $resp['Email_docen']=$row_usuarios_ulm['Email_docen'];
                                          $resp['Id_dir']=$row_usuarios_ulm['Id_dir'];
			        $resp['Last_session']=$row_usuarios_ulm['Last_session'];
			        $resp['Img_docen']=$row_usuarios_ulm['Img_docen'];
			        $resp['Telefono_docen']=$row_usuarios_ulm['Telefono_docen'];
			        $resp['Cel_docen']=$row_usuarios_ulm['Cel_docen'];
			        $resp['Clave_docen']=$row_usuarios_ulm['Clave_docen'];
			        $resp['Pass_docen']=$row_usuarios_ulm['Pass_docen'];
			        $resp['NivelEst_docen']=$row_usuarios_ulm['NivelEst_docen'];
                                          $resp['Tipo_sangre']=$row_usuarios_ulm['Tipo_sangre'];
                                          $resp['Alergias']=$row_usuarios_ulm['Alergias'];
                                          $resp['Enfermedades_cronicas']=$row_usuarios_ulm['Enfermedades_cronicas'];
                                          $resp['Preinscripciones_medicas']=$row_usuarios_ulm['Preinscripciones_medicas'];
			        $resp['Id_plantel']=$row_usuarios_ulm['Id_plantel'];
			        //$resp['Nombre_plantel']=$row_usuarios_ulm['Nombre_plantel'];
			        $resp['Doc_RFC']=$row_usuarios_ulm['Doc_RFC'];
			        $resp['Doc_CURP']=$row_usuarios_ulm['Doc_CURP'];
			        $resp['Doc_Curriculum']=$row_usuarios_ulm['Doc_Curriculum'];
			        $resp['Doc_CompDomicilio']=$row_usuarios_ulm['Doc_CompDomicilio'];
			        $resp['Doc_Contrato']=$row_usuarios_ulm['Doc_Contrato'];
			        $resp['Doc_CompEstudios']=$row_usuarios_ulm['Doc_CompEstudios'];
			        $resp['copy_RFC']=$row_usuarios_ulm['copy_RFC'];
			        $resp['copy_CURP']=$row_usuarios_ulm['copy_CURP'];
			        $resp['copy_Curriculum']=$row_usuarios_ulm['copy_Curriculum'];
			        $resp['copy_CompDomicilio']=$row_usuarios_ulm['copy_CompDomicilio'];
			        $resp['copy_Contrato']=$row_usuarios_ulm['copy_Contrato'];
			        $resp['copy_CompEstudios']=$row_usuarios_ulm['copy_CompEstudios'];
                                $resp['UltimaGeneracion_credencial']=$row_usuarios_ulm['UltimaGeneracion_credencial'];
                                $resp['Tiempo_completo']=$row_usuarios_ulm['Tiempo_completo'];

			        $resp['Contacto']=array();
			        $resp['Contacto']=$this->get_contacto($row_usuarios_ulm['Id_docen'],'docente');
                                $resp['Evaluaciones']=array();

                              $query_evaluacion = "SELECT * FROM Evaluacion_docente WHERE Id_doc=".$row_usuarios_ulm['Id_docen']." ORDER  BY Id_eva DESC";
                              $evaluacion = mysql_query($query_evaluacion, $this->cnn) or die(mysql_error());
                              $row_evaluacion= mysql_fetch_array($evaluacion);
                              $totalRows_evaluacion = mysql_num_rows($evaluacion);
                              if($totalRows_evaluacion>0){
                                do{
                                    array_push($resp['Evaluaciones'], $this->get_evaluacion($row_evaluacion['Id_eva']));
                                }while($row_evaluacion= mysql_fetch_array($evaluacion));
                              }
		     }
		     return $resp;
	     }
               
                public function get_evaluacion($Id_evaluacion){
	           $resp=array();
                      $query_evaluacion = "SELECT * FROM Evaluacion_docente WHERE Id_eva=".$Id_evaluacion;
                      $evaluacion = mysql_query($query_evaluacion, $this->cnn) or die(mysql_error());
                      $row_evaluacion= mysql_fetch_array($evaluacion);
                      $totalRows_evaluacion = mysql_num_rows($evaluacion);
                      if($totalRows_evaluacion>0){
                            $resp['Id_eva']=$row_evaluacion['Id_eva'];
                            $resp['Id_doc']=$row_evaluacion['Id_doc'];
                            $resp['CV']=$row_evaluacion['CV'];
                            $resp['IFE']=$row_evaluacion['IFE'];
                            $resp['RFC']=$row_evaluacion['RFC'];
                            $resp['CURP']=$row_evaluacion['CURP'];
                            $resp['Fotos']=$row_evaluacion['Fotos'];
                            $resp['Comp_domicilio']=$row_evaluacion['Comp_domicilio'];
                            $resp['Comp_estudios']=$row_evaluacion['Comp_estudios'];
                            $resp['Tecnico_val']=$row_evaluacion['Tecnico_val'];
                            $resp['Planeaciones']=$row_evaluacion['Planeaciones'];
                            $resp['Asistencias']=$row_evaluacion['Asistencias'];
                            $resp['Puntualidad']=$row_evaluacion['Puntualidad'];
                            $resp['Encuestas']=$row_evaluacion['Encuestas'];
                            $resp['Eva_int']=$row_evaluacion['Eva_int'];
                            $resp['Juntas']=$row_evaluacion['Juntas'];
                            $resp['Titulo_cedula']=$row_evaluacion['Titulo_cedula'];
                            $resp['Equivalencia']=$row_evaluacion['Equivalencia'];
                            $resp['Maestria']=$row_evaluacion['Maestria'];
                            $resp['Cap_int']=$row_evaluacion['Cap_int'];
                            $resp['Cap_ext']=$row_evaluacion['Cap_ext'];
                            $resp['Cursos_afin']=$row_evaluacion['Cursos_afin'];
                            $resp['Libros']=$row_evaluacion['Libros'];
                            $resp['Discografia']=$row_evaluacion['Discografia'];
                            $resp['Ponente']=$row_evaluacion['Ponente'];
                            $resp['Capacitador_externo']=$row_evaluacion['Capacitador_externo'];
                            $resp['Inv_academico']=$row_evaluacion['Inv_academico'];
                            $resp['Puntos']=$row_evaluacion['Puntos_totales'];
                            $resp['Profesor_a']=$row_evaluacion['Profesor_a'];
                            $resp['Profesor_b']=$row_evaluacion['Profesor_b'];
                            $resp['Profesor_c']=$row_evaluacion['Profesor_c'];
                            $resp['Profesor_d']=$row_evaluacion['Profesor_d'];
                            $resp['Fecha_evaluacion']=$row_evaluacion['Fecha_evaluacion'];
                            $resp['Id_usu_evaluador']=$row_evaluacion['Id_usu_evaluador'];
                            $resp['Id_ciclo']=$row_evaluacion['Id_ciclo'];
                            $resp['presentaciones_profecionales']=$row_evaluacion['presentaciones_profecionales'];
                            $resp['Comentarios']=$row_evaluacion['Comentarios'];
                      }
                      return $resp;
	     }
             
             
             public function get_evaluacion_ultima_eva_docente($Id_doc){
	           $resp=array();
                      $query_evaluacion = "SELECT * FROM Evaluacion_docente WHERE Id_doc=".$Id_doc." ORDER BY Id_eva DESC LIMIT 1";
                      $evaluacion = mysql_query($query_evaluacion, $this->cnn) or die(mysql_error());
                      $row_evaluacion= mysql_fetch_array($evaluacion);
                      $totalRows_evaluacion = mysql_num_rows($evaluacion);
                      if($totalRows_evaluacion>0){
                            $resp['Id_eva']=$row_evaluacion['Id_eva'];
                            $resp['Id_doc']=$row_evaluacion['Id_doc'];
                            $resp['CV']=$row_evaluacion['CV'];
                            $resp['IFE']=$row_evaluacion['IFE'];
                            $resp['RFC']=$row_evaluacion['RFC'];
                            $resp['CURP']=$row_evaluacion['CURP'];
                            $resp['Fotos']=$row_evaluacion['Fotos'];
                            $resp['Comp_domicilio']=$row_evaluacion['Comp_domicilio'];
                            $resp['Comp_estudios']=$row_evaluacion['Comp_estudios'];
                            $resp['Tecnico_val']=$row_evaluacion['Tecnico_val'];
                            $resp['Planeaciones']=$row_evaluacion['Planeaciones'];
                            $resp['Asistencias']=$row_evaluacion['Asistencias'];
                            $resp['Puntualidad']=$row_evaluacion['Puntualidad'];
                            $resp['Encuestas']=$row_evaluacion['Encuestas'];
                            $resp['Eva_int']=$row_evaluacion['Eva_int'];
                            $resp['Juntas']=$row_evaluacion['Juntas'];
                            $resp['Titulo_cedula']=$row_evaluacion['Titulo_cedula'];
                            $resp['Equivalencia']=$row_evaluacion['Equivalencia'];
                            $resp['Maestria']=$row_evaluacion['Maestria'];
                            $resp['Cap_int']=$row_evaluacion['Cap_int'];
                            $resp['Cap_ext']=$row_evaluacion['Cap_ext'];
                            $resp['Cursos_afin']=$row_evaluacion['Cursos_afin'];
                            $resp['Libros']=$row_evaluacion['Libros'];
                            $resp['Discografia']=$row_evaluacion['Discografia'];
                            $resp['Ponente']=$row_evaluacion['Ponente'];
                            $resp['Capacitador_externo']=$row_evaluacion['Capacitador_externo'];
                            $resp['Inv_academico']=$row_evaluacion['Inv_academico'];
                            $resp['Puntos']=$row_evaluacion['Puntos_totales'];
                            $resp['Profesor_a']=$row_evaluacion['Profesor_a'];
                            $resp['Profesor_b']=$row_evaluacion['Profesor_b'];
                            $resp['Profesor_c']=$row_evaluacion['Profesor_c'];
                            $resp['Profesor_d']=$row_evaluacion['Profesor_d'];
                            $resp['Fecha_evaluacion']=$row_evaluacion['Fecha_evaluacion'];
                            $resp['Id_usu_evaluador']=$row_evaluacion['Id_usu_evaluador'];
                            $resp['Id_ciclo']=$row_evaluacion['Id_ciclo'];
                            $resp['presentaciones_profecionales']=$row_evaluacion['presentaciones_profecionales'];
                            $resp['Comentarios']=$row_evaluacion['Comentarios'];
                      }
                      return $resp;
	     }
             
             public function getLastEvaluacion($Id_doc,$Id_oferta){
                  $resp=array();
                  $query_evaluacion = "SELECT * FROM Evaluacion_docente WHERE Id_doc=".$Id_doc." ORDER  BY Id_ciclo DESC";
                  $evaluacion = mysql_query($query_evaluacion, $this->cnn) or die(mysql_error());
                  $row_evaluacion= mysql_fetch_array($evaluacion);
                  $totalRows_evaluacion = mysql_num_rows($evaluacion);
                  if($totalRows_evaluacion>0){
                      if($Id_oferta==9 || $Id_oferta==14){
                        //Prepa o Academia 
                        $query_Niveles_docentes = "SELECT * FROM Niveles_docentes WHERE Id_nivel=".$row_evaluacion['Profesor_a'];
                        $Niveles_docentes = mysql_query($query_Niveles_docentes, $this->cnn) or die(mysql_error());
                        $row_Niveles_docentes = mysql_fetch_array($Niveles_docentes);
                        $Tipo="Profesor A";
                        $nivel= $row_Niveles_docentes['Nombre_nivel'];
                        $PagoXhora=$row_Niveles_docentes['Pago_nivel'];

                      }elseif($Id_oferta==10){
                        //Licenciatura
                        $query_Niveles_docentes = "SELECT * FROM Niveles_docentes WHERE Id_nivel=".$row_evaluacion['Profesor_c'];
                        $Niveles_docentes = mysql_query($query_Niveles_docentes, $this->cnn) or die(mysql_error());
                        $row_Niveles_docentes = mysql_fetch_array($Niveles_docentes);
                        $Tipo="Profesor C";
                        $nivel= $row_Niveles_docentes['Nombre_nivel'];
                        $PagoXhora=$row_Niveles_docentes['Pago_nivel'];

                      }elseif($Id_oferta==11){
                        //Carrera tecnica
                        $query_Niveles_docentes = "SELECT * FROM Niveles_docentes WHERE Id_nivel=".$row_evaluacion['Profesor_b'];
                        $Niveles_docentes = mysql_query($query_Niveles_docentes, $this->cnn) or die(mysql_error());
                        $row_Niveles_docentes = mysql_fetch_array($Niveles_docentes);
                        $Tipo="Profesor B";
                        $nivel= $row_Niveles_docentes['Nombre_nivel'];
                        $PagoXhora=$row_Niveles_docentes['Pago_nivel'];
                      }elseif($Id_oferta==13){
                        //Taller
                        $query_Niveles_docentes = "SELECT * FROM Niveles_docentes WHERE Id_nivel=".$row_evaluacion['Profesor_d'];
                        $Niveles_docentes = mysql_query($query_Niveles_docentes, $this->cnn) or die(mysql_error());
                        $row_Niveles_docentes = mysql_fetch_array($Niveles_docentes);
                        $Tipo="Profesor D";
                        $nivel= $row_Niveles_docentes['Nombre_nivel'];
                        $PagoXhora=$row_Niveles_docentes['Pago_nivel'];
                      }
                      $resp['Tipo']=$Tipo;
                      $resp['Puntos']=$row_evaluacion['Puntos_totales'];;
                      $resp['nivel']=$nivel;
                      $resp['PagoPorHora']=$PagoXhora;
                  }
                  return $resp;
	     }
             

             public function get_num_grupos($Id_docen,$Id_ciclo,$Id_ofe,$query_fechas){
                    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                    $usu = $class_usuarios->get_usu();
                    $resp=array();
                    $resp['Grupos']=array();
                    $query_grupos = "
                        SELECT 
                            Docentes.Id_docen,Docentes.Nombre_docen,
                            Horario_docente.Id_grupo,Horario_docente.Id_ciclo,
                            Grupos.Clave,Grupos.Id_mat_esp,Grupos.Id_grupo,
                            Materias_especialidades.Id_esp,
                            especialidades_ulm.Id_ofe,
                            ofertas_ulm.Nombre_oferta
                        FROM Docentes 
                        JOIN Horario_docente ON Docentes.Id_docen=Horario_docente.Id_docente
                        JOIN Grupos ON Horario_docente.Id_grupo=Grupos.Id_grupo
                        JOIN Materias_especialidades ON  Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
                        JOIN especialidades_ulm ON Materias_especialidades.Id_esp=especialidades_ulm.Id_esp
                        JOIN ofertas_ulm ON especialidades_ulm.Id_ofe=ofertas_ulm.Id_oferta
                        WHERE Id_docen=".$Id_docen." AND Horario_docente.Id_ciclo=".$Id_ciclo." AND Id_ofe= ".$Id_ofe." GROUP BY Grupos.Id_grupo";
                        $grupos = mysql_query($query_grupos, $this->cnn) or die(mysql_error());
                        $row_grupos= mysql_fetch_array($grupos);
                        $totalRows_grupos= mysql_num_rows($grupos);
                        $TotalGrupos=0;
                        if($totalRows_grupos>0){
                             $TotalGrupos=$totalRows_grupos;
                             do{
                                $Lunes=0;
                                $Martes=0;
                                $Miercoles=0;
                                $Jueves=0;
                                $Viernes=0;
                                $Sabado=0;
                                $query_horas = "SELECT * FROM Horario_docente WHERE Id_grupo=".$row_grupos['Id_grupo'];
                                $horas = mysql_query($query_horas, $this->cnn) or die(mysql_error());
                                $row_horas= mysql_fetch_array($horas);
                                $totalRows_horas= mysql_num_rows($horas);
                                if($totalRows_horas>0){
                                    do{
                                       if($row_horas['Lunes']==1){
                                           $Lunes++;
                                       }
                                       if($row_horas['Martes']==1){
                                           $Martes++;
                                       }
                                       if($row_horas['Miercoles']==1){
                                           $Miercoles++;
                                       }
                                       if($row_horas['Jueves']==1){
                                           $Jueves++;
                                       }
                                       if($row_horas['Viernes']==1){
                                           $Viernes++;
                                       }
                                       if($row_horas['Sabado']==1){
                                           $Sabado++;
                                       }
                                    }while($row_horas= mysql_fetch_array($horas));
                                }
                                
                                $LunesTrabajados=0;
                                $MartesTrabajados=0;
                                $MiercolesTrabajados=0;
                                $JuevesTrabajados=0;
                                $ViernesTrabajados=0;
                                $SabadoTrabajados=0;
                                $query_horas_trabajadas = "SELECT * From Asistencias WHERE Id_grupo=".$row_grupos['Id_grupo']." ".$query_fechas." GROUP BY Fecha_asis ORDER BY Fecha_asis ASC";
                                $horas_trabajadas = mysql_query($query_horas_trabajadas, $this->cnn) or die(mysql_error());
                                $row_horas_trabajadas= mysql_fetch_array($horas_trabajadas);
                                $totalRows_horas_trabajadas= mysql_num_rows($horas_trabajadas);
                                if($totalRows_horas_trabajadas>0){
                                    do{
                                        
                                        if(date("N",strtotime($row_horas_trabajadas['Fecha_asis']))==1){
                                               $LunesTrabajados+=$Lunes;
                                        }elseif(date("N",strtotime($row_horas_trabajadas['Fecha_asis']))==2){
                                               $MartesTrabajados+=$Martes; 
                                        }elseif(date("N",strtotime($row_horas_trabajadas['Fecha_asis']))==3){
                                               $MiercolesTrabajados+=$Miercoles;
                                        }elseif(date("N",strtotime($row_horas_trabajadas['Fecha_asis']))==4){
                                               $JuevesTrabajados+=$Jueves;
                                        }elseif(date("N",strtotime($row_horas_trabajadas['Fecha_asis']))==5){
                                               $ViernesTrabajados+=$Viernes;
                                        }elseif(date("N",strtotime($row_horas_trabajadas['Fecha_asis']))==6){
                                               $SabadoTrabajados+=$Sabado;
                                        }
                                    }while($row_horas_trabajadas= mysql_fetch_array($horas_trabajadas));
                                }
                                $Grupo=array();
                                $Grupo['Id_grupo']=$row_grupos['Id_grupo'];
                                $Grupo['Lunes']=$Lunes;
                                $Grupo['Martes']=$Martes;
                                $Grupo['Miercoles']=$Miercoles;
                                $Grupo['Jueves']=$Jueves;
                                $Grupo['Viernes']=$Viernes;
                                $Grupo['Sabado']=$Sabado;
                                $Grupo['HorasPor_semana']=$Lunes+$Martes+$Miercoles+$Jueves+$Viernes+$Sabado;
                                $Grupo['HorasPor_semana_trabajados']=$LunesTrabajados+$MartesTrabajados+$MiercolesTrabajados+$JuevesTrabajados+$ViernesTrabajados+$SabadoTrabajados;
                                array_push($resp['Grupos'], $Grupo);

                             }while($row_grupos= mysql_fetch_array($grupos));
                        }
                        $resp['NumGrupos']=$TotalGrupos;
                        return $resp;
             }
             
             
            public function get_amolestaciones_docente($Id_docen) {
            $resp = array();
            $query_Reportes_alumno = "SELECT * FROM Reportes_alumno WHERE Id_recibe=" .$Id_docen." AND Tipo_recibe='docen'";
            $Reportes_alumno = mysql_query($query_Reportes_alumno, $this->cnn) or die(mysql_error());
            $row_Reportes_alumno = mysql_fetch_array($Reportes_alumno);
            $totalRows_Reportes_alumno = mysql_num_rows($Reportes_alumno);
            if ($totalRows_Reportes_alumno > 0) {
                do {
                    array_push($resp, $this->get_amolestacion_docente($row_Reportes_alumno['Id_rep']));
                } while ($row_Reportes_alumno = mysql_fetch_array($Reportes_alumno));
            }
            return $resp;
            }


            public function get_amolestacion_docente($Id_rep) {
            $resp = array();
            $query_Reportes_alumno = "SELECT * FROM Reportes_alumno WHERE Id_rep=" .$Id_rep;
            $Reportes_alumno = mysql_query($query_Reportes_alumno, $this->cnn) or die(mysql_error());
            $row_Reportes_alumno = mysql_fetch_array($Reportes_alumno);
            $totalRows_Reportes_alumno = mysql_num_rows($Reportes_alumno);
            if ($totalRows_Reportes_alumno > 0) {
                    $resp['Id_rep']=$row_Reportes_alumno['Id_rep'];
                    $resp['Fecha_rep']=$row_Reportes_alumno['Fecha_rep'];
                    $resp['Id_usu']=$row_Reportes_alumno['Id_usu'];
                    $resp['Motivo']=$row_Reportes_alumno['Motivo'];
                    $resp['Id_recibe']=$row_Reportes_alumno['Id_recibe'];
                    $resp['Tipo_recibe']=$row_Reportes_alumno['Tipo_recibe'];
                    $resp['Id_reporta']=$row_Reportes_alumno['Id_reporta'];
                    $resp['Tipo_reporta']=$row_Reportes_alumno['Tipo_reporta'];
                    $resp['Id_ciclo']=$row_Reportes_alumno['Id_ciclo'];
            }
            return $resp;
            }
            
            
            
            public function get_horas_grupo($Id_docen,$Id_ciclo,$Id_grupo,$query_fechas){
                    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                    $usu = $class_usuarios->get_usu();
                    $resp=array();
                    $resp['Grupos']=array();
                    $query_grupos = "
                        SELECT 
                            Docentes.Id_docen,Docentes.Nombre_docen,
                            Horario_docente.Id_grupo,Horario_docente.Id_ciclo,
                            Grupos.Clave,Grupos.Id_mat_esp,
                            Materias_especialidades.Id_esp,
                            especialidades_ulm.Id_ofe,
                            ofertas_ulm.Nombre_oferta
                        FROM Docentes 
                        JOIN Horario_docente ON Docentes.Id_docen=Horario_docente.Id_docente
                        JOIN Grupos ON Horario_docente.Id_grupo=Grupos.Id_grupo
                        JOIN Materias_especialidades ON  Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
                        JOIN especialidades_ulm ON Materias_especialidades.Id_esp=especialidades_ulm.Id_esp
                        JOIN ofertas_ulm ON especialidades_ulm.Id_ofe=ofertas_ulm.Id_oferta
                        WHERE Id_docen=".$Id_docen." AND Horario_docente.Id_ciclo=".$Id_ciclo." AND Grupos.Id_grupo=".$Id_grupo." GROUP BY Grupos.Id_grupo";
                        $grupos = mysql_query($query_grupos, $this->cnn) or die(mysql_error());
                        $row_grupos= mysql_fetch_array($grupos);
                        $totalRows_grupos= mysql_num_rows($grupos);
                        $TotalGrupos=0;
                        if($totalRows_grupos>0){
                             $TotalGrupos=$totalRows_grupos;
                             do{
                                 
                                $Lunes=0;
                                $Martes=0;
                                $Miercoles=0;
                                $Jueves=0;
                                $Viernes=0;
                                $Sabado=0;
                                $query_horas = "SELECT * FROM Horario_docente WHERE Id_grupo=".$row_grupos['Id_grupo'];
                                $horas = mysql_query($query_horas, $this->cnn) or die(mysql_error());
                                $row_horas= mysql_fetch_array($horas);
                                $totalRows_horas= mysql_num_rows($horas);
                                if($totalRows_horas>0){
                                    do{
                                       if($row_horas['Lunes']==1){
                                           $Lunes++;
                                       }
                                       if($row_horas['Martes']==1){
                                           $Martes++;
                                       }
                                       if($row_horas['Miercoles']==1){
                                           $Miercoles++;
                                       }
                                       if($row_horas['Jueves']==1){
                                           $Jueves++;
                                       }
                                       if($row_horas['Viernes']==1){
                                           $Viernes++;
                                       }
                                       if($row_horas['Sabado']==1){
                                           $Sabado++;
                                       }
                                    }while($row_horas= mysql_fetch_array($horas));
                                }
                                
                                $LunesTrabajados=0;
                                $MartesTrabajados=0;
                                $MiercolesTrabajados=0;
                                $JuevesTrabajados=0;
                                $ViernesTrabajados=0;
                                $SabadoTrabajados=0;
                                
                                $query_horas_trabajadas = "SELECT * From Asistencias WHERE Id_grupo=".$row_grupos['Id_grupo']." ".$query_fechas." GROUP BY Fecha_asis ORDER BY Fecha_asis ASC";
                                $horas_trabajadas = mysql_query($query_horas_trabajadas, $this->cnn) or die(mysql_error());
                                $row_horas_trabajadas= mysql_fetch_array($horas_trabajadas);
                                $totalRows_horas_trabajadas= mysql_num_rows($horas_trabajadas);
                                if($totalRows_horas_trabajadas>0){
                                    do{
                                        
                                        if(date("N",strtotime($row_horas_trabajadas['Fecha_asis']))==1){
                                               $LunesTrabajados+=$Lunes;
                                        }elseif(date("N",strtotime($row_horas_trabajadas['Fecha_asis']))==2){
                                               $MartesTrabajados+=$Martes; 
                                        }elseif(date("N",strtotime($row_horas_trabajadas['Fecha_asis']))==3){
                                               $MiercolesTrabajados+=$Miercoles;
                                        }elseif(date("N",strtotime($row_horas_trabajadas['Fecha_asis']))==4){
                                               $JuevesTrabajados+=$Jueves;
                                        }elseif(date("N",strtotime($row_horas_trabajadas['Fecha_asis']))==5){
                                               $ViernesTrabajados+=$Viernes;
                                        }elseif(date("N",strtotime($row_horas_trabajadas['Fecha_asis']))==6){
                                               $SabadoTrabajados+=$Sabado;
                                        }
                                        
                                    }while($row_horas_trabajadas= mysql_fetch_array($horas_trabajadas));
                                }
                                
                                //
                                $Grupo=array();
                                $Grupo['Id_ofe']=$row_grupos['Id_ofe'];
                                $Grupo['Id_grupo']=$row_grupos['Id_grupo'];
                                $Grupo['Lunes']=$Lunes;
                                $Grupo['Martes']=$Martes;
                                $Grupo['Miercoles']=$Miercoles;
                                $Grupo['Jueves']=$Jueves;
                                $Grupo['Viernes']=$Viernes;
                                $Grupo['Sabado']=$Sabado;
                                $Grupo['HorasPor_semana']=$Lunes+$Martes+$Miercoles+$Jueves+$Viernes+$Sabado;
                                $Grupo['HorasPor_semana_trabajados']=$LunesTrabajados+$MartesTrabajados+$MiercolesTrabajados+$JuevesTrabajados+$ViernesTrabajados+$SabadoTrabajados;
                                array_push($resp['Grupos'], $Grupo);

                             }while($row_grupos= mysql_fetch_array($grupos));
                        }
                        
                        $resp['NumGrupos']=$TotalGrupos;
                        return $resp;
             }
               

	
}