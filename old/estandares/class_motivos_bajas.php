<?php
require_once("class_bdd.php");
class class_motivos_bajas extends class_bdd{

	     public $Id_mot;
	     
	     public function  __construct($Id_mot){
	     	   class_bdd::__construct();
	     	   if($Id_mot>0){
	     	   	  $this->Id_mot=$Id_mot;
	     	   }
	     }
	     
	     public function get_motivos_bajas(){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_motivos  = "SELECT * FROM Motivos_bajas WHERE Activo_mot=1 AND Id_plantel=".$usu['Id_plantel'];
		     $motivos= mysql_query($query_motivos, $this->cnn) or die(mysql_error());
		     $row_motivos= mysql_fetch_array($motivos);
		     $totalRows_motivos = mysql_num_rows($motivos);
		     if($totalRows_motivos>0){
			     do{
			      array_push($resp, $this->get_motivo_baja($row_motivos['Id_mot']));
			     }while($row_motivos= mysql_fetch_array($motivos));
		     }
		     return $resp;
	     }
	     
	     public function get_motivo_baja($Id_mot=null){
	     	 if($Id_mot==null){
		        $Id_mot=$this->Id_mot;
	         }

	         $resp=array();
                 $query_motivos  = "SELECT * FROM Motivos_bajas WHERE Id_mot=".$Id_mot;
                 $motivos= mysql_query($query_motivos, $this->cnn) or die(mysql_error());
                 $row_motivos= mysql_fetch_array($motivos);
                 $totalRows_motivos = mysql_num_rows($motivos);
                 if($totalRows_motivos>0){
                    $resp['Id_mot']=$row_motivos['Id_mot'];
                    $resp['Nombre']=$row_motivos['Nombre'];
                    $resp['Activo_mot']=$row_motivos['Activo_mot'];
                    $resp['Id_plantel']=$row_motivos['Id_plantel'];
                 }
                 return $resp;
	     }
}


