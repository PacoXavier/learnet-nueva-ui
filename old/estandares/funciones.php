<?php
//error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
if (!function_exists("get_mime_type")) {
function get_mime_type($filepath) {
    ob_start();
    system("file -i -b {$filepath}");
    $output = ob_get_clean();
    $output = explode("; ",$output);
    if ( is_array($output) ) {
        $output = $output[0];
    }
    return $output;
}
}
function xmlEntities($s){
	$trans = get_html_translation_table(HTML_ENTITIES);
	
	$s = strtr($s, $trans);
	
	//build first an assoc. array with the entities we want to match
	$table1 = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
	
	//now build another assoc. array with the entities we want to replace (numeric entities)
	foreach ($table1 as $k=>$v){
	  $table1[$k] = "/$v/";
		//  $c = htmlentities($k,ENT_QUOTES,"UTF-8");
	  $table2[$k] = "&#".ord($k).";";
	}
	
	//now perform a replacement using preg_replace
	//each matched value in array 1 will be replaced with the corresponding value in array 2
	$s = preg_replace($table1,$table2,$s);
	return $s;
}

function hexEntities($s){
	$trans = get_html_translation_table(HTML_ENTITIES);
	
	$s = strtr($s, $trans);
	
	//build first an assoc. array with the entities we want to match
	$table1 = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
	
	//now build another assoc. array with the entities we want to replace (numeric entities)
	foreach ($table1 as $k=>$v){
	  $table1[$k] = "/$v/";
		//  $c = htmlentities($k,ENT_QUOTES,"UTF-8");
	  $table2[$k] = "=".dechex(ord($k))."=";
	}
	
	//now perform a replacement using preg_replace
	//each matched value in array 1 will be replaced with the corresponding value in array 2
	$s = preg_replace($table1,$table2,$s);
	return $s;
}

function mail_seguro($usuario,$contra,$from_address,$to_address,$to_name,$reply, $subject,$html_body,$text_body,$attachments=false,$servidor){
	require_once "Mail.php";
	
	$from = $from_address;
	$email=$reply;
	//$to = "Xbarro<info@xbarro.com>";
	$to = "$to_name<$to_address>";
	//$subject = "Nuevo registro en xbarro.com";
	
	//$usuario="web@xbarro.com";
	//$contra="ceramicas";
	
	$mime_boundary=md5(time());
	$eol="\r\n";
	
	$body = $html_body;

	# Open the first part of the mail
	$msg = "--".$mime_boundary.$eol;
	
	$htmlalt_mime_boundary = $mime_boundary."_htmlalt"; //we must define a different MIME boundary for this section
	# Setup for text OR html -
	$msg .= "Content-Type: multipart/alternative; boundary=\"".$htmlalt_mime_boundary."\"".$eol.$eol;
	
	# Text Version
	$msg .= "--".$htmlalt_mime_boundary.$eol;
	$msg .= "Content-Type: text/plain; charset=iso-8859-1".$eol;
	$msg .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
	$msg .= strip_tags(str_replace("<br>", "\n", substr($body, (strpos($body, "<body>")+6)))).$eol.$eol;
	
	# HTML Version
	$msg .= "--".$htmlalt_mime_boundary.$eol;
	$msg .= "Content-Type: text/html; charset=iso-8859-1".$eol;
	$msg .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
	$msg .= $body.$eol.$eol;
	
	//close the html/plain text alternate portion
	$msg .= "--".$htmlalt_mime_boundary."--".$eol.$eol;
	//The attachments variable of the function is a 2-dimensional array which should contain the keys "file" and "content_type".

	if ($attachments !== false)
	{
		//  echo("tiene attachments:".count($attachments)."<br>");
		for($i=0; $i < count($attachments); $i++)
		{
			//	echo("attachment ".$i.":".$attachments[$i]["file"]."<br>");
			  if (is_file($attachments[$i]["file"]))
			  {  
					//	  	echo("es un archivo!!!");
				    # File for Attachment
				    $file_name = substr($attachments[$i]["file"], (strrpos($attachments[$i]["file"], "/")+1));
				   
				    $handle=fopen($attachments[$i]["file"], 'rb');
				    $f_contents=fread($handle, filesize($attachments[$i]["file"]));
				    $f_contents=chunk_split(base64_encode($f_contents));    //Encode The Data For Transition using base64_encode();
				    $f_type=filetype($attachments[$i]["file"]);
				    fclose($handle);
				   
				    # Attachment
				    $msg .= "--".$mime_boundary.$eol;
				    $msg .= "Content-Type: ".$attachments[$i]["content_type"]."; name=\"".$file_name."\"".$eol;  // sometimes i have to send MS Word, use 'msword' instead of 'pdf'
				    $msg .= "Content-Transfer-Encoding: base64".$eol;
				    $msg .= "Content-Description: ".$file_name.$eol;
				    $msg .= "Content-Disposition: attachment; filename=\"".$file_name."\"".$eol.$eol; // !! This line needs TWO end of lines !! IMPORTANT !!
				    $msg .= $f_contents.$eol.$eol;
		  }
		}
	}
	# Finished
	$msg .= "--".$mime_boundary."--".$eol.$eol;  // finish with two eol's for better security. see Injection.  

	$host = $servidor;
	$username = $usuario;
	$password = $contra;
 
	$headers = array ('From' => $from,
		'To' => $to,
		'Reply-To' => $email,
		'Return-Path' => $email,
		'Message-ID' => ("<".time()."-".$from.">"),
		'X-Mailer' => ("PHP v".phpversion()),
		'Subject' => $subject,
		  # Boundry for marking the split & Multitype Headers
		'MIME-Version' => "1.0",
		'Content-Type' => "multipart/mixed; boundary=\"".$mime_boundary."\"");
	$smtp = Mail::factory('sendmail');

	ini_set(sendmail_from,$from);  // the INI lines are to force the From Address to be used 		
	$mail = $smtp->send($to, $headers, $msg);
	ini_restore(sendmail_from);

	if(PEAR::isError($mail)) {
		return("&mail=" . $mail->getMessage() . "");
	} else {
		return("&mail=Message successfully sent!");
	}
}

function resampimagejpg($forcedwidth, $forcedheight, $sourcefile, $destfile, $imgcomp)
   {
   $g_imgcomp=100-$imgcomp;
   $g_srcfile=$sourcefile;
   $g_dstfile=$destfile;
   $g_fw=$forcedwidth;
   $g_fh=$forcedheight;

   if(file_exists($g_srcfile))
       {
       $g_is=getimagesize($g_srcfile);
       if(($g_is[0]-$g_fw)>=($g_is[1]-$g_fh))
           {
           $g_iw=$g_fw;
           $g_ih=($g_fw/$g_is[0])*$g_is[1];
           }
           else
           {
           $g_ih=$g_fh;
           $g_iw=($g_ih/$g_is[1])*$g_is[0];    
           }
       $img_src=imagecreatefromjpeg($g_srcfile);
       $img_dst=imagecreate($g_iw,$g_ih);
       imagecopyresampled($img_dst, $img_src, 0, 0, 0, 0, $g_iw, $g_ih, $g_is[0], $g_is[1]);
       imagejpeg($img_dst, $g_dstfile, $g_imgcomp);
       imagedestroy($img_dst);
       return true;
       }
       else
       return false;
  } 
   
   /* resizeToFile resizes a picture and writes it to the harddisk
 *  
 * $sourcefile = the filename of the picture that is going to be resized
 * $dest_x      = X-Size of the target picture in pixels
 * $dest_y      = Y-Size of the target picture in pixels
 * $targetfile = The name under which the resized picture will be stored
 * $jpegqual  = The Compression-Rate that is to be used 
 */

function resizeToFile ($sourcefile, $dest_x, $dest_y, $targetfile, $jpegqual)
{
   
	setMemoryForImage( $sourcefile );
	/* Get the  dimensions of the source picture */
   $picsize=getimagesize("$sourcefile");

   $source_x  = $picsize[0];
   $source_y  = $picsize[1];
   $source_id = imageCreateFromJPEG("$sourcefile");

	/* Create a new image object (not neccessarily true colour) */
       
   $target_id=imagecreatetruecolor($dest_x, $dest_y);

	/* Resize the original picture and copy it into the just created image
	   object. Because of the lack of space I had to wrap the parameters to 
	   several lines. I recommend putting them in one line in order keep your
	   code clean and readable */
       

   $target_pic=imagecopyresampled($target_id,$source_id,
                                   0,0,0,0,
                                   $dest_x,$dest_y,
                                   $source_x,$source_y);

	/* Create a jpeg with the quality of "$jpegqual" out of the
	   image object "$target_pic".
	   This will be saved as $targetfile */
 
   imagejpeg ($target_id,"$targetfile",$jpegqual);

   return true;

} 

function cropImage($sourcefile, $Xini, $Yini, $f_Width, $f_Height, $dest_image){
	$img = imagecreatetruecolor($f_Width,$f_Height); 
	$red = imagecolorallocate($img, 255, 255, 255); 
	imagefill( $img, 0, 0, $red );
	$org_img = imagecreatefromjpeg($sourcefile); 
	$red1 = imagecolorallocate($org_img, 255, 255, 255); 
	imagefill( $org_img, 0, 0, $red );
	//$ims = getimagesize($sourcefile);
	imagecopy($img,$org_img, 0, 0, $Xini, $Yini, $f_Width, $f_Height); 
	imagefill( $img, 0, 0, $red );
	imagefill( $org_img, 0, 0, $red );
	//fill the background with white (not sure why it has to be in this order)
	//int imagecopy ( resource dst_im, resource src_im, int dst_x, int dst_y, int src_x, int src_y, int src_w, int src_h)
	//Copy a part of src_im onto dst_im starting at the x,y coordinates src_x, src_y with a width of src_w and a height 
	//of src_h. The portion defined will be copied onto the x,y coordinates, dst_x and dst_y. 
	imagejpeg($img,$dest_image,90); 
	imagedestroy($img); 
	return true;

}

function setMemoryForImage( $filename ){
    $imageInfo = getimagesize($filename);
    $MB = 1048576;  // number of bytes in 1M
    $K64 = 65536;    // number of bytes in 64K
    $TWEAKFACTOR = 2.5;  // Or whatever works for you
    $memoryNeeded = round( ( $imageInfo[0] * $imageInfo[1]
                                           * $imageInfo['bits']
                                           * $imageInfo['channels'] / 8
                             + $K64
                           ) * $TWEAKFACTOR
                         );
    //ini_get('memory_limit') only works if compiled with "--enable-memory-limit" also
    //Default memory limit is 8MB so well stick with that. 
    //To find out what yours is, view your php.ini file.
    $memoryLimit = 8 * $MB;
    if (function_exists('memory_get_usage') && 
        memory_get_usage() + $memoryNeeded > $memoryLimit) 
    {
        $newLimit = $memoryLimitMB + ceil( ( memory_get_usage()
                                            + $memoryNeeded
                                            - $memoryLimit
                                            ) / $MB
                                        );
        ini_set( 'memory_limit', $newLimit . 'M' );
        return true;
    }else{
        return false;
    }
}


function mail_genera($from_address,$to_address,$to_name,$reply, $subject,$html_body,$text_body,$attachments=false){
	//The attachments variable of the function is a 2-dimensional array which should contain the keys "file" and "content_type".
	require_once "Mail.php";
	
	$from = $from_address;
	$email=$reply;
	//$to = "=?iso-8859-1?B?".chunk_split(base64_encode($to_name))."?=<$to_address>";
	$to=$to_address;
		
	$mime_boundary=md5(time());
	$eol="\r\n";
	
	$body = $html_body;

	# Open the first part of the mail
	$msg = "--".$mime_boundary.$eol;
	
	$htmlalt_mime_boundary = $mime_boundary."_htmlalt"; //we must define a different MIME boundary for this section

	# Setup for text OR html -
	$msg .= "Content-Type: multipart/alternative; boundary=\"".$htmlalt_mime_boundary."\"".$eol.$eol;
	
	# Text Version
	$msg .= "--".$htmlalt_mime_boundary.$eol;
	$msg .= "Content-Type: text/plain;".$eol;
	$msg .= "Content-Transfer-Encoding: base64".$eol.$eol;
	$msg .= chunk_split(base64_encode($text_body)).$eol.$eol;
	
	# HTML Version
	$msg .= "--".$htmlalt_mime_boundary.$eol;
	$msg .= "Content-Type: text/html;".$eol;
	$msg .= "Content-Transfer-Encoding: base64".$eol.$eol;
	$msg .= chunk_split(base64_encode($body)).$eol.$eol;
	
	//close the html/plain text alternate portion
	$msg .= "--".$htmlalt_mime_boundary."--".$eol.$eol;

	//attachments
	if ($attachments !== false){
		for($i=0; $i < count($attachments); $i++){
			  if (is_file($attachments[$i]["file"])){  
				    # File for Attachment
				    $file_name = substr($attachments[$i]["file"], (strrpos($attachments[$i]["file"], "/")+1));
				   
				    $handle=fopen($attachments[$i]["file"], 'rb');
				    $f_contents=fread($handle, filesize($attachments[$i]["file"]));
				    $f_contents=chunk_split(base64_encode($f_contents));    //Encode The Data For Transition using base64_encode();
				    $f_type=filetype($attachments[$i]["file"]);
				    fclose($handle);
				   
				    # Attachment
				    $msg .= "--".$mime_boundary.$eol;
				    $msg .= "Content-Type: ".$attachments[$i]["content_type"]."; name=\"".$file_name."\"".$eol;  // sometimes i have to send MS Word, use 'msword' instead of 'pdf'
				    $msg .= "Content-Transfer-Encoding: base64".$eol;
				    $msg .= "Content-Description: ".$file_name.$eol;
				    $msg .= "Content-Disposition: attachment; filename=\"".$file_name."\"".$eol.$eol; // !! This line needs TWO end of lines !! IMPORTANT !!
				    $msg .= $f_contents.$eol.$eol;
		  }
		}
	}
	# Finished
	$msg .= "--".$mime_boundary."--".$eol.$eol;  // finish with two eol's for better security. see Injection.  
 
	$headers = array ('From' => $from,
		'To' => $to,
		'Reply-To' => $email,
		'Return-Path' => $email,
		'Message-ID' => ("<".time()."-".$from.">"),
		'X-Mailer' => ("PHP v".phpversion()),
		'Subject' => $subject,
		  # Boundry for marking the split & Multitype Headers
		'MIME-Version' => "1.0",
		'Content-Type' => "multipart/mixed; boundary=\"".$mime_boundary."\"");
	$smtp = Mail::factory('sendmail');

	ini_set(sendmail_from,$from);  // the INI lines are to force the From Address to be used 		
	$mail = $smtp->send($to, $headers, $msg);
	ini_restore(sendmail_from);

	if(PEAR::isError($mail)) {
		return("mail=" . $mail->getMessage() . "");
	} else {
		return("mail=OK");
	}
}

function mail_formato($from_address,$to_address,$to_name="",$reply, $subject, $abstract, $txt_content, $logo_url="http://www.generaweb.net/img_email/logo.jpg" ,$attachments=false){

		$texto_html="		<p style='line-height: 15px;color: black;font-size: 12px;font-family: Helvetica, Verdana, Arial, sans-serif;'>
	".($abstract)."
</p>
	<div align='center' width='622px' style='widht:622px;'>
	<table width='622px' style='padding: 0px;margin: 0px' border='0px' cellpadding='0px' cellspacing='0px'>
		<tr>
			<td width='52px' height='29px' style='background-image:url(http://www.generaweb.net/img_email/top_not.jpg);background-position: 0px 0px;background-repeat: no-repeat;'>&nbsp;</td>
			<td width='145px' height='29px' style='background-image:url(http://www.generaweb.net/img_email/top_not.jpg);background-position: -52px 0px;background-repeat: no-repeat;'>&nbsp;</td>
			<td width='380px' height='29px' style='background-image:url(http://www.generaweb.net/img_email/top_not.jpg);background-position: -197px 0px;background-repeat: no-repeat;'>&nbsp;</td>
			<td width='45px' height='29px' style='background-image:url(http://www.generaweb.net/img_email/top_not.jpg);background-position: -577px 0px;background-repeat: no-repeat;'>&nbsp;</td>
		</tr>
		<tr>
			<td width='52px' height='132px' style='background-image:url(http://www.generaweb.net/img_email/tittle_not.jpg);background-position: 1px 0px;background-repeat: no-repeat;'>&nbsp;</td>
			<td width='145px' height='132px' style='background-image:url(http://www.generaweb.net/img_email/tittle_not.jpg);background-position: -51px 0px;background-repeat: no-repeat;' valign='top' align='left'><img src='".$logo_url."' width='90px' height='90px' align='left' style='margin-top:10px' /></td>
			<td width='380px' height='132px' style='background-image:url(http://www.generaweb.net/img_email/tittle_not.jpg);background-position: -196px 0px;background-repeat: no-repeat;color: #105E9C;line-height: 30px;font-size: 24px;font-family: Helvetica, Verdana, Arial, sans-serif;'>
				".($subject)."
			</td>
			<td width='45px' height='132px' style='background-image:url(http://www.generaweb.net/img_email/tittle_not.jpg);background-position: -576px 0px;background-repeat: no-repeat;'>&nbsp;</td>
		</tr>
		<tr>
			<td width='52px' style='background-image:url(http://www.generaweb.net/img_email/middle_not.jpg);background-position: 0px 0px;background-repeat: repeat-y;'>&nbsp;</td>
			<td colspan='2' style='background-image:url(http://www.generaweb.net/img_email/middle_not.jpg);background-position: -52px 0px;background-repeat: repeat-y;color: #4D4D4D;line-height: 15px;font-size: 12px;font-family: Helvetica, Verdana, Arial, sans-serif;'>
				".((str_replace("\n","<br />",str_replace("\r","<br />",$txt_content))))."				
			</td>
			<td width='45px' style='background-image:url(http://www.generaweb.net/img_email/middle_not.jpg);background-position: -577px 0px;background-repeat: repeat-y;'>&nbsp;</td>
		</tr>
		<tr>
			<td width='52px' height='40px' style='background-image:url(http://www.generaweb.net/img_email/bottom_not.jpg);background-position: 1px 0px;background-repeat: no-repeat;'>&nbsp;</td>
			<td width='145px' height='40px' style='background-image:url(http://www.generaweb.net/img_email/bottom_not.jpg);background-position: -56px 0px;background-repeat: no-repeat;'>&nbsp;</td>
			<td width='380px' height='40px' style='background-image:url(http://www.generaweb.net/img_email/bottom_not.jpg);background-position: -196px 0px;background-repeat: no-repeat;'>&nbsp;</td>
			<td width='45px' height='40px' style='background-image:url(http://www.generaweb.net/img_email/bottom_not.jpg);background-position: -576px 0px;background-repeat: no-repeat;'>&nbsp;</td>
		</tr>
	</table>
	</div>
	<p>&nbsp;</p>
	<p align='center'><a href='http://www.generaweb.net'><img src='http://www.generaweb.net/mail_not_genera.jpg' border='0'/></a></p>
";

	$texto_txt=$abstract."\n\r\n\r".$txt_content;
	
	return(mail_genera($from_address,$to_address,$to_name,$reply, $subject,$texto_html,$texto_txt,$attachments=false));

}

function mesLetra($mesActual){
	if($mesActual==1){
		return("Ene");
	}
	if($mesActual==2){
		return("Feb");
	}
	if($mesActual==3){
		return("Mar");
	}
	if($mesActual==4){
		return("Abr");
	}
	if($mesActual==5){
		return("May");
	}
	if($mesActual==6){
		return("Jun");
	}
	if($mesActual==7){
		return("Jul");
	}
	if($mesActual==8){
		return("Ago");
	}
	if($mesActual==9){
		return("Sep");
	}
	if($mesActual==10){
		return("Oct");
	}
	if($mesActual==11){
		return("Nov");
	}
	if($mesActual==12){
		return("Dic");
	}
}


function formatFecha($fechaSQL,$corta=0){
	$anioSQL=substr($fechaSQL, 0, 4);
	$mesSQL=substr($fechaSQL, 5, 2);
	$mesSQL=$mesSQL*1;
	$mesSQL=mesLetra($mesSQL);
	$diaSQL=substr($fechaSQL, 8, 2);
	$diaSQL=$diaSQL*1;
	if($corta==0){
		return ($diaSQL." de ".$mesSQL.", ".$anioSQL);
	}else{
		return ($diaSQL." ".$mesSQL.", ".substr($anioSQL,2,2));	
	}
}

/*



*/
function mail_formato09($from_address,$to_address,$to_name="",$reply, $subject, $txt_content, $logo_url="http://www.generaweb.net/img_email/logo.jpg" ,$attachments=false){

		$texto_html="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html>
	<head>
		<meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\" />
		<title>email</title>
	</head>
	<body>
	
		<table cellspacing=\"0px\" border=\"0px\" align=\"center\" width=\"582px\" cellpadding=\"0px\">
			<tr style=\"background: url('http://www.generadns.com/estandares/img_email/fondo_top.png') no-repeat 0 0; height: 90px;\">
				<td colspan=\"4\"></td>
			</tr>
			<tr style=\"height: 164px;\">
				<td style=\"background: url('http://www.generadns.com/estandares/img_email/fondo_mid_left.png');\" width=\"40px\"></td>
				<td style=\"background: url('$logo_url') no-repeat 0 0;\" width=\"164px\"><img src=\"http://www.generadns.com/estandares/img_email/logo_mask.png\" /></td>
				<td style=\"background: none;\" width=\"298px\"><p style=\"font-size: 25px; font-weight: bold; font-family: Tahoma, Helvetica, Verdana, Arial, sans-serif; color: #F26531; margin-left: 10px;\">$subject</p></td>
				<td style=\"background: url('http://www.generadns.com/estandares/img_email/fondo_mid_right.png');\" width=\"80px\"></td>
			</tr>
			<tr>
				<td style=\"background: url('http://www.generadns.com/estandares/img_email/fondo_mid_left.png');\" width=\"40px\"></td>
				<td colspan=\"2\"><p style=\"font-size: 16px; font-family: Tahoma, Helvetica, Verdana, Arial, sans-serif; color: #414042;\">".str_replace("\n", "<br/>",$txt_content)."</p></td>
				<td style=\"background: url('http://www.generadns.com/estandares/img_email/fondo_mid_right.png');\" width=\"80px\"></td>
			</tr>
			<tr style=\"background: url('http://www.generadns.com/estandares/img_email/fondo_bottom.png') no-repeat 0 0; height: 145px;\">
				<td colspan=\"4\"><a href=\"http://www.generaweb.net\"><img src=\"http://www.generadns.com/estandares/img_email/logo_gweb.png\" border=\"0px\" style=\"margin-left: 40px;\" /></a></td>
			</tr>
		</table>
	</body>
</html>
";

	$texto_txt=$subject."\n\r\n\r".$txt_content;
	
	return(mail_genera($from_address,$to_address,$to_name,$reply, $subject,$texto_html,$texto_txt,$attachments=false));

}

function generarKey($largo=13){
	$var_count=0;
	$chars = "abcdefghijkmnopqrstuvwxyz023456789";
    srand((double)microtime()*1000000);
	while($var_count<$largo){
	    $num = rand() % 33;
		$str.=substr($chars, $num, 1);
		$var_count=$var_count+1;		
	}
	return($str);
}

function parserRSS($urlRSS){
	$RSS=file_get_contents($urlRSS);
	$arrayRSS=array();
	$count=0;
	
	// encontrar el primer <item> y borrar todo lo anterior
	$RSS=substr($RSS, strpos($RSS, "<item>"));
	// encontrar el œltimo <item> y borrar todo lo siguiente
	$RSS=substr($RSS, 0, strrpos($RSS, "</item>")+7);
	$count=0;
	
	while(strlen($RSS)>25){
		$itemActual=substr($RSS, 0, strpos($RSS, "</item>")+7);
		$RSS=substr($RSS,strpos($RSS, "</item>")+7);
		$RSS=substr($RSS,strpos($RSS, "<item>"));

		$arrayRSS[$count]['tittle']=substr($itemActual, strpos($itemActual, "<title>")+7);
		$arrayRSS[$count]['tittle']=substr($arrayRSS[$count]['tittle'], 0, strpos($arrayRSS[$count]['tittle'], "</title>"));

		$arrayRSS[$count]['link']=substr($itemActual, strpos($itemActual, "<link>")+6);
		$arrayRSS[$count]['link']=substr($arrayRSS[$count]['link'], 0, strpos($arrayRSS[$count]['link'], "</link>"));

		$arrayRSS[$count]['date']=substr($itemActual, strpos($itemActual, "<pubDate>")+6);
		$arrayRSS[$count]['date']=substr($arrayRSS[$count]['date'], 0, strpos($arrayRSS[$count]['date'], "</pubDate>"));
		
		$ini_pos_cat=0;
		$arrayRSS[$count]['category']="";
		while(strpos($itemActual, "<category>",$ini_pos_cat)!== false){
			$str_category=substr($itemActual, strpos($itemActual, "<category>",$ini_pos_cat)+10);
			$str_category=substr($str_category, 0, strpos($str_category, "</category>"));
			$str_category=str_replace("<![CDATA[", "", $str_category);
			$str_category=str_replace("]]>", "", $str_category);
			
			$arrayRSS[$count]['category'].="|".$str_category;
			
			$ini_pos_cat=strpos($itemActual, "</category>",$ini_pos_cat)+11;
		}
		if(strlen($arrayRSS[$count]['category'])>0){
			$arrayRSS[$count]['category']=substr($arrayRSS[$count]['category'],1);
		}

		$arrayRSS[$count]['description']=substr($itemActual, strpos($itemActual, "<description>")+13);
		$arrayRSS[$count]['description']=substr($arrayRSS[$count]['description'], 0, strpos($arrayRSS[$count]['description'], "</description>"));
		$arrayRSS[$count]['description']=str_replace("<![CDATA[", "", $arrayRSS[$count]['description']);
		$arrayRSS[$count]['description']=str_replace("]]>", "", $arrayRSS[$count]['description']);

		$arrayRSS[$count]['content']=substr($itemActual, strpos($itemActual, "<content"));
		$arrayRSS[$count]['content']=substr($arrayRSS[$count]['content'], strpos($arrayRSS[$count]['content'], ">")+1);
		$arrayRSS[$count]['content']=substr($arrayRSS[$count]['content'], 0, strpos($arrayRSS[$count]['content'], "</content"));
		$arrayRSS[$count]['content']=str_replace("<![CDATA[", "", $arrayRSS[$count]['content']);
		$arrayRSS[$count]['content']=str_replace("]]>", "", $arrayRSS[$count]['content']);
		
		$count=$count+1;
	}
	return($arrayRSS);
}
function parserFB_wall($appId, $secret, $user){
	require_once('facebook.php');
	
	$facebook = new Facebook(array(
	  'appId'  => $appId,
	  'secret' => $secret,
	  'cookie' => true,
	));
	$FBwall = $facebook->api("/$user/feed");
	$FBwall=$FBwall['data'];

	return($FBwall);
}
function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}

function page_thumbnail($url){
	$return="";
	
	$page_string=file_get_contents($url);
	// buscar og:image
	$pos_ogImage=strpos($page_string, "og:image");
	if($pos_ogImage>0){
		// buscar el meta anterior más cercano
		$pos_meta=strrpos(substr($page_string, 0,$pos_ogImage), "meta");
		
		// buscar el content posterior más cercano
		$page_string=substr($page_string, $pos_meta);
		$page_string=substr($page_string, 0, strpos($page_string, ">"));	
		$page_string=substr($page_string, strpos($page_string, "content"));	
		$page_string=substr($page_string, strpos($page_string, "="));	
		$page_string=substr($page_string, strpos($page_string, "\"")+1);	
		$page_string=substr($page_string, 0, strpos($page_string, "\""));
		$return=$page_string;	
	}
	return $return;
}

?>
