<?php
require_once("class_bdd.php");
require_once("class_usuarios.php");
class class_eventos extends class_bdd{

	     public $Id_ciclo;
	     
	     public function  __construct($Id_evento){
	     	   class_bdd::__construct();
	     	   if($Id_evento>0){
	     	   	  $this->Id_evento=$Id_evento;
	     	   }
	     }
	     
	     public function get_eventos(){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_eventos = "SELECT * FROM Eventos WHERE Activo=1 AND Id_plantel=".$usu['Id_plantel']." ORDER BY Start DESC";
		     $eventos= mysql_query($query_eventos, $this->cnn) or die(mysql_error());
		     $row_eventos= mysql_fetch_array($eventos);
		     $totalRows_eventos  = mysql_num_rows($eventos);
		     if($totalRows_eventos>0){
			     do{
			      array_push($resp, $this->get_evento($row_eventos['Id_event']));
			     }while($row_eventos= mysql_fetch_array($eventos));
		     }
		     return $resp;
	     }
	     
	     public function get_evento($Id_evento=null){
	     	 if($Id_evento==null){
		        $Id_evento=$this->Id_evento;
	         }
	         $resp=array();
		     $query_eventos  = "SELECT * FROM Eventos WHERE Id_event=".$Id_evento;
		     $eventos= mysql_query($query_eventos, $this->cnn) or die(mysql_error());
		     $row_eventos= mysql_fetch_array($eventos);
		     $totalRows_eventos  = mysql_num_rows($eventos);
		     if($totalRows_eventos>0){

			        $resp['Id_event']=$row_eventos['Id_event'];
			        $resp['Nombre']=$row_eventos['Nombre'];
                                $resp['Comentarios']=$row_eventos['Comentarios'];
                                $resp['DateCreated']=$row_eventos['DateCreated'];
                                $resp['Id_salon']=$row_eventos['Id_salon'];
                                $resp['Start']=$row_eventos['Start'];
                                $resp['End']=$row_eventos['End'];
                                $resp['Id_usu']=$row_eventos['Id_usu'];
                                $resp['NombreUtiliza']=$row_eventos['NombreUtiliza'];
                                $resp['Id_ciclo']=$row_eventos['Id_ciclo'];
                                $resp['Id_plantel']=$row_eventos['Id_plantel'];
                                $resp['Activo']=$row_eventos['Activo'];
                                $resp['Costo']=$row_eventos['Costo'];
		     }
		     return $resp;
	     }  
             
             public function get_eventos_ciclo($Id_ciclo){
	             $resp=array();
                     $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                     $usu = $class_usuarios->get_usu();
		     $query_eventos = "SELECT * FROM Eventos WHERE Activo=1 AND Id_plantel=".$usu['Id_plantel']." AND Id_ciclo= ".$Id_ciclo." ORDER BY Start DESC";
		     $eventos= mysql_query($query_eventos, $this->cnn) or die(mysql_error());
		     $row_eventos= mysql_fetch_array($eventos);
		     $totalRows_eventos  = mysql_num_rows($eventos);
		     if($totalRows_eventos>0){
			     do{
			      array_push($resp, $this->get_evento($row_eventos['Id_event']));
			     }while($row_eventos= mysql_fetch_array($eventos));
		     }
		     return $resp;
	     }
              
}
