<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();

links_head("Documentos faltantes  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-file-text-o"></i> Documentos faltantes</h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Filtros</li>
                        <li onclick="download_excel()"><i class="fa fa-download"></i> Descargar</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td>Opcion de pago</td>
                                <td style="text-align: center">Doc.Faltantes</td>
                                <td style="text-align: center;">Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count=1;
                            foreach ($DaoAlumnos->getAlumnos() as $k=>$v){
                               $nombre_ori="";
                               $oferta = $DaoOfertas->show($v['Id_ofe']);
                               $esp = $DaoEspecialidades->show($v['Id_esp']);
                               if ($v['Id_ori'] > 0) {
                                  $ori = $DaoOrientaciones->show($v['Id_ori']);
                                  $nombre_ori = $ori->getNombre();
                                }
                                $opcion="Plan por materias"; 
                                if($v['Opcion_pago']==2){
                                  $opcion="Plan completo";  
                                }

                                //Buscar documentos faltantes
                                $Faltantes=$DaoAlumnos->getArchivosFaltantes($v['Id_ofe_alum']);
                                if($Faltantes>0){

                                 ?>
                                         <tr id_alum="<?php echo $v['Id_ins'];?>">
                                           <td><?php echo $count;?></td>
                                           <td><?php echo $v['Matricula'] ?></td>
                                           <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                                           <td><?php echo $oferta->getNombre_oferta() ?></td>
                                           <td><?php echo $esp->getNombre_esp(); ?></td>
                                           <td><?php echo $nombre_ori; ?></td>
                                           <td><?php echo $opcion; ?></td>
                                           <td style="text-align: center;"><?php echo $Faltantes;?></td>
                                           <td style="text-align: center;"><button onclick="mostrar_documentos(<?php echo $v['Id_ofe_alum'];?>)">Mostrar documentos</button></td>
                                         </tr>
                                         <?php
                                         $count++;
                                  }
                                }
                               ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
	<p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso()">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
        <p>Opci&oacute;n de pago:<br>
            <select id="opcion">
              <option value="0"></option>
              <option value="1">POR MATERIAS</option>
              <option value="2">PLAN COMPLETO</option>
            </select>
        </p>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();
