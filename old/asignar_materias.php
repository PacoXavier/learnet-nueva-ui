<?php
require_once('estandares/includes.php');
if(!isset($perm['58'])){
  header('Location: home.php');
}
require_once('estandares/includes.php');
require_once('clases/DaoPagosCiclo.php');
require_once('clases/DaoPagos.php');
require_once('clases/DaoRecargos.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoCiclosAlumno.php');
require_once('clases/DaoMetodosPago.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoMateriasCicloAlumno.php');
require_once('clases/DaoMateriasEspecialidad.php');
require_once('clases/DaoGrados.php');

links_head("Asignar materias  ");
write_head_body();
write_body();


if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoPagos = new DaoPagos();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoAlumnos = new DaoAlumnos();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoCiclos = new DaoCiclos();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoRecargos = new DaoRecargos();
    $DaoMetodosPago = new DaoMetodosPago();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoGrados= new DaoGrados();

    $oferta_alumno = $DaoOfertasAlumno->show($_REQUEST['id']);
    $alum = $DaoAlumnos->show($oferta_alumno->getId_alum());
    $oferta = $DaoOfertas->show($oferta_alumno->getId_ofe());
    $esp = $DaoEspecialidades->show($oferta_alumno->getId_esp());
    $nombre_ori="";
    if ($oferta_alumno->getId_ori() > 0) {
        $ori = $DaoOrientaciones->show($oferta_alumno->getId_ori());
        $nombre_ori = $ori->getNombre();
    }

    $primer_ciclo = $DaoOfertasAlumno->getFirstCicloOferta($_REQUEST['id']);
    $ultimo_ciclo = $DaoOfertasAlumno->getLastCicloOferta($_REQUEST['id']);

    $Opcion_pago = "";
    if ($oferta_alumno->getOpcionPago() == 1) {
        $Opcion_pago = "Plan por materias";
    } else {
        $Opcion_pago = "Plan completo";
    }
    
?>


<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div class="seccion">
                    <table  id="info_alumn">
                        <tbody><tr><th style="padding-bottom: 15px;">
                                    <font size="3">DATOS DEL ESTUDIANTE</font>
                                </th></tr>
                            <tr><td>
                                    <table width="100%">
                                        <tbody><tr>
                                                <th ><font>C&oacute;digo:</font></th>
                                                <td><font><?php echo $alum->getMatricula() ?></font></td>
                                                <th ><font>Nombre:</font></th>
                                                <td colspan="5"><font><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></font></td>
                                            </tr>
                                            <tr>
                                                <th><font>Nivel:</font></th>
                                                <td><font><?php echo $oferta->getNombre_oferta(); ?></font></td>
                                                <th><font>Admisi&oacute;n:</font></th>
                                                <td><?php echo $primer_ciclo['Clave']; ?></font></td>
                                                <th><font>Último ciclo:</font></th>
                                                <td><font><?php echo $ultimo_ciclo['Clave']; ?></font></td>
                                            </tr>
                                            <tr>
                                                <th><font>Carrera:</font></th>
                                                <td colspan="7"><font><?php echo $esp->getNombre_esp(); ?></font></td>
                                            </tr>
                                            <tr>
                                                <th><font>Orientaci&oacute;n:</font></th>
                                                <td><font color="navy" face="arial" size="2"><?php echo $nombre_ori; ?></font></td>
                                                <th><font>Opci&oacute;n de pago</font></th>
                                                <td><font color="navy" face="arial" size="2"><?php echo $Opcion_pago; ?></font></td>
                                            </tr>
                                        </tbody></table>
                                </td></tr>
                        </tbody>
                    </table>
                </div>
                <?php
                
                foreach ($DaoCiclosAlumno->getCiclosOferta($_REQUEST['id']) as $cicloAlumno) {
                    $query = "SELECT * FROM Pagos_ciclo WHERE Concepto='Mensualidad' AND Id_ciclo_alum=".$cicloAlumno->getId()." AND Id_alum=".$alum->getId()." AND DeadCargo IS NULL";
                    $totalRows_Pagos_ciclo=$DaoPagosCiclo->getTotalRows($query);
                    $ciclo=$DaoCiclos->show($cicloAlumno->getId_ciclo());
                    ?>
                    <div class="ciclo_alumno" id-ciclo-alumno="<?php echo $cicloAlumno->getId();?>">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td colspan="7" style="font-size:12px;text-align:center;"><b>Ciclo <?php echo $ciclo->getClave()?></b>
                                        <?php
                                        if($oferta->getTipoOferta()==2){
                                            ?>
                                            <button class="button-asignar" onclick="mostrarBoxAsignarMateriaCurso(<?php echo $cicloAlumno->getid()?>)">Agregar materia</button>
                                            <?php
                                         }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Clave</td>
                                    <td style="width:300px;">Materia</td>
                                    <td>Orientaci&oacute;n</td>
                                    <td>Tipo</td>
                                    <td>Nivel</td>
                                    <td>Creditos</td>
                                    <?php
                                        if($oferta_alumno->getOpcionPago()==2){
                                            ?>
                                        <td>Acciones</td>
                                    <?php
                                        }elseif($oferta_alumno->getOpcionPago()==1){
                                         if($totalRows_Pagos_ciclo==0){
                                         ?>
                                               <td>Acciones</td>
                                        <?php
                                              }
                                        }
                                     ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($DaoMateriasCicloAlumno->getMateriasCicloAlumno($cicloAlumno->getId()) as $materiasCiclo) {
                                        $mat_esp = $DaoMateriasEspecialidad->show($materiasCiclo->getId_mat_esp());
                                        
                                        $mat = $DaoMaterias->show($mat_esp->getId_mat());
                                        $grado=$DaoGrados->show($mat_esp->getGrado_mat());
                                        
                                        $nom_ori="";
                                        if($materiasCiclo->getId_ori()>0){
                                         $ori=$DaoOrientaciones->show($materiasCiclo->getId_ori());
                                         $nom_ori=$ori->getNombre();
                                        }
                                        
                                        $tipoMat="";
                                        if($materiasCiclo->getTipo()==1){
                                           $tipoMat="Normal"; 
                                        }elseif($materiasCiclo->getTipo()==2){
                                           $tipoMat="Extra";  
                                        }
                                        $NombreMat=$mat->getNombre();
                                        if(strlen($mat_esp->getNombreDiferente())>0){
                                            $NombreMat=$mat_esp->getNombreDiferente(); 
                                        }
                                        if($materiasCiclo->getActivo()==1){
                                    ?>
                                    <tr>
                                        <td><?php echo $mat->getClave_mat()?></td>
                                        <td><?php echo $NombreMat?></td>
                                        <td><?php echo $nom_ori;?></td>
                                        <td><?php echo $tipoMat;?></td>
                                        <td><?php echo $grado->getGrado()?></td>
                                        <td><?php echo $mat->getCreditos()?></td>
                                       <?php
                                            if($oferta_alumno->getOpcionPago()==2){
                                                ?>
                                        <td>
                                            <button onclick="delete_ciclo_mat(<?php echo $materiasCiclo->getId()?>)">Eliminar</button>
                                            <button onclick="baja_ciclo_mat(<?php echo $materiasCiclo->getId()?>)">Dar de baja</button>
                                        </td>
                                        <?php
                                            }
                                            if($oferta_alumno->getOpcionPago()==1){
                                               if($totalRows_Pagos_ciclo==0){
                                               ?>
                                               <td>
                                                 <button onclick="delete_ciclo_mat(<?php echo $materiasCiclo->getId()?>)">Eliminar</button>
                                               </td>
                                              <?php
                                              }
                                        
                                            }
                                         ?>
                                    </tr>
                                    <?php
                                        }
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                           if($oferta_alumno->getOpcionPago()==1 && count($DaoMateriasCicloAlumno->getMateriasCicloAlumno($cicloAlumno->getId()))>0){

                            if($totalRows_Pagos_ciclo==0){
                         ?>
                                <p class="nota">Generar mensualidades cuando se hayan agregado todas las materias que cursar&aacute; el alumno durante el ciclo</p>
                                <?php
                                if($oferta->getTipoOferta()==1){
                                    ?>
                                      <button class="cobro" onclick="generar_cobro(<?php echo $cicloAlumno->getId()?>)">Generar mensualidades</button>
                                  <?php
                                }else{
                                    ?>
                                      <button class="cobro" onclick="generar_cobro_curso(<?php echo $cicloAlumno->getId()?>)">Generar mensualidades</button>
                                    <?php
                                }

                              }else{
                                  if($oferta->getTipoOferta()==1){
                                  ?>
                                       <p class="mensualidades_generadas"><a href="pago.php?id=<?php echo $_REQUEST['id']?>">Mensualidades generadas</a></p>
                                <?php
                                  }else{
                                      ?>
                                      <p class="mensualidades_generadas"><a href="cargos_curso.php?id=<?php echo $_REQUEST['id'] ?>">Mensualidades generadas</a></p>
                                   <?php
                                  }
                              }
                            }
                         ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="ofertas_alumno.php?id=<?php echo $alum->getId(); ?>" class="link">Regresar</a></li>
                    <?php
                    if($oferta->getTipoOferta()==1){
                       ?>
                        <li><span onclick="mostrar_box_materias()">Agregar materia</span></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<input type="hidden" id="Id_esp" value="<?php echo $esp->getId()?>"/>
<input type="hidden" id="Id_oferta_alumno" value="<?php echo $_REQUEST['id']?>"/>
<input type="hidden" id="Id_alum" value="<?php echo $alum->getId()?>"/>
<?php
}
write_footer();
