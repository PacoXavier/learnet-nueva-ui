<?php
set_time_limit(0);
require_once('Connections/cnn.php');
require_once('estandares/class_interesados.php');
require_once('estandares/class_ofertas.php');
require_once('estandares/class_grados.php');
require_once('estandares/class_usuarios.php');
require_once('estandares/class_ciclos.php');
require_once('estandares/class_recargos.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoGrupos.php');
require_once('clases/DaoMateriasCicloAlumno.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoMateriasEspecialidad.php');
require_once('clases/PHPExcel.php');


if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}


mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');


if($_POST['action']=="filtro"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $query="";

    if($_POST['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_POST['Id_ofe'];
    }
    if($_POST['Id_esp']>0){
        $query=$query." AND Id_esp=".$_POST['Id_esp'];
    }
    if($_POST['Id_ori']>0){
        $query=$query." AND Id_ori=".$_POST['Id_ori'];
    }
    foreach ($DaoAlumnos->getAlumnos(null,$query) as $k => $v) {
        $nombre_ori = "";
        $oferta = $DaoOfertas->show($v['Id_ofe']);
        $esp = $DaoEspecialidades->show($v['Id_esp']);
        if ($v['Id_ori'] > 0) {
            $ori = $DaoOrientaciones->show($v['Id_ori']);
            $nombre_ori = $ori->getNombre();
        }
        $opcion = "Plan por materias";
        if ($v['Opcion_pago'] == 2) {
            $opcion = "Plan completo";
        }

        $Grado = $DaoOfertasAlumno->getLastCicloOferta($v['Id_ofe_alum']);
        $Grado = $DaoGrados->show($Grado['Id_grado']);

        //Verificar las materias de la especialidad
        $statusOferta=$DaoAlumnos->getStatusOferta($v['Id_ofe_alum']);
        $MateriasCursadas = $statusOferta['cant_mat_cursadas'];
        if ($statusOferta['status'] == 1) {
            
            ?>
            <tr id_alum="<?php echo $v['Id_ins']; ?>">
                <td><?php echo $v['Matricula'] ?></td>
                <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                <td><?php echo $oferta->getNombre_oferta(); ?></td>
                <td><?php echo $esp->getNombre_esp(); ?></td>
                <td><?php echo $nombre_ori; ?></td>
                <td><?php echo $Grado->getGrado(); ?></td>
                <td><?php echo $opcion; ?></td>
                <td style="text-align: center;"><?php echo $MateriasCursadas; ?></td>
                <!--<td><input type="checkbox"> </td>-->
                <td><button onclick="mostrar_box_egresar(<?php echo $v['Id_ofe_alum']; ?>)">Egresar</button></td>
            </tr>
            <?php
        }
    }
}




if($_POST['action'] == "mostrar_box_egresar"){
    $Adeudo = 0;
    $totalAdeudo = 0;
    $class_interesados = new class_interesados();
    $oferta_alumno = $class_interesados->get_oferta_alumno($_POST['Id_ofe_alum']);
    foreach ($oferta_alumno['Ciclos_oferta'] as $k => $v) {
        $query_Pagos_ciclo = "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=" . $v['Id_ciclo_alum'] ."  AND Fecha_pago<='".date('Y-m-d')."' AND DeadCargo IS NULL AND  DeadCargoUsu IS NULL ORDER BY Id_pago_ciclo ASC";
        $Pagos_ciclo = mysql_query($query_Pagos_ciclo, $cnn) or die(mysql_error());
        $row_Pagos_ciclo = mysql_fetch_array($Pagos_ciclo);
        $totalRows_Pagos_ciclo = mysql_num_rows($Pagos_ciclo);
        if ($totalRows_Pagos_ciclo > 0) {
            
          do {
            $recargos = 0;

            $class_recargos= new class_recargos();
            foreach($class_recargos->get_recargos_pago($row_Pagos_ciclo['Id_pago_ciclo']) as $k=>$v ){
                $recargos+=$v['Monto_recargo'];
            }
            $Adeudo = round($row_Pagos_ciclo['Mensualidad'] - $row_Pagos_ciclo['Descuento'] - $row_Pagos_ciclo['Descuento_beca'] - $row_Pagos_ciclo['Cantidad_Pagada'] + $recargos);
            $totalAdeudo+=$Adeudo;

          } while ($row_Pagos_ciclo = mysql_fetch_array($Pagos_ciclo));
        }
      }   
  ?>
    <div id="box_emergente" class="box-egresar">
	<h1>Capturar egreso</h1>
        <?php
        if($Adeudo>0){
        ?>
        <p style="color: red;">No se puede capturar el egreso debido a que el alumno tiene un adeudo de <b>$<?php echo number_format($totalAdeudo,2)?></b> pesos</p>
        <?php
        }else{
            ?>
               <p>Ciclo:<br>
                   <select id="Id_ciclo">
                        <option value="0"></option>
                        <?php
                            $class_ciclos= new class_ciclos();
                            foreach($class_ciclos->get_ciclos_futuros() as $k=>$v){
                             ?>
                                <option value="<?php echo $v['Id_ciclo'] ?>"><?php echo $v['Clave'] ?></option>
                              <?php
                              }
                              ?>
                    </select>
                </p>
        <?php
        }
        ?>
        <div id="box_orientacion"></div>
        <p><button onclick="save_egreso(<?php echo $_POST['Id_ofe_alum'];?>)">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
</div>
  <?php
}

if($_POST['action']=="save_egreso"){
  $sql_nueva_entrada=sprintf("UPDATE ofertas_alumno SET  FechaCapturaEgreso=%s,IdCicloEgreso=%s,IdUsuEgreso=%s WHERE Id_ofe_alum=%s",
            GetSQLValueString(date('Y-m-d H:i:s'), "date"),
            GetSQLValueString($_POST['Id_ciclo'], "int"),
            GetSQLValueString($_COOKIE['admin/Id_usu'], "int"),
            GetSQLValueString($_POST['Id_ofe_alum'], "int"));
  $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());	
  update_page();
}

function update_page(){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    foreach ($DaoAlumnos->getAlumnos() as $k => $v) {
        $nombre_ori = "";
        $oferta = $DaoOfertas->show($v['Id_ofe']);
        $esp = $DaoEspecialidades->show($v['Id_esp']);
        if ($v['Id_ori'] > 0) {
            $ori = $DaoOrientaciones->show($v['Id_ori']);
            $nombre_ori = $ori->getNombre();
        }
        $opcion = "Plan por materias";
        if ($v['Opcion_pago'] == 2) {
            $opcion = "Plan completo";
        }

        $Grado = $DaoOfertasAlumno->getLastCicloOfertaSinCiclo($v['Id_ofe_alum']);
        $Grado = $DaoGrados->show($Grado['Id_grado']);


        $ban = 0;
        //Verificar las materias de la especialidad
        $MateriasCursadas = 0;

        foreach ($DaoMateriasEspecialidad->getMateriasEspecialidad($v['Id_esp']) as $k2 => $v2) {
            $query = "SELECT * FROM ofertas_alumno 
            JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
            JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum= materias_ciclo_ulm.Id_ciclo_alum
            JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
            JOIN materias_uml ON Materias_especialidades.Id_mat=materias_uml.Id_mat
            WHERE ofertas_alumno.Id_ofe_alum=" . $v['Id_ofe_alum'] . " AND materias_ciclo_ulm.Id_mat_esp=" . $v2->getId();
            $consulta=$base->advanced_query($query);
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta > 0 && (($row_consulta['CalTotalParciales'] >= $row_consulta['Promedio_min']) || ($row_consulta['CalExtraordinario'] >= $row_consulta['Promedio_min']) || ($row_consulta['CalEspecial'] >= $row_consulta['Promedio_min']))) {
                $ban = 1;
                $MateriasCursadas++;
            } else {
                $ban = 0;
            }
        }

        if ($ban == 1) {
            ?>
            <tr id_alum="<?php echo $v['Id_ins']; ?>">
                <td><?php echo $v['Matricula'] ?></td>
                <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                <td><?php echo $oferta->getNombre_oferta(); ?></td>
                <td><?php echo $esp->getNombre_esp(); ?></td>
                <td><?php echo $nombre_ori; ?></td>
                <td><?php echo $Grado->getGrado(); ?></td>
                <td><?php echo $opcion; ?></td>
                <td style="text-align: center;"><?php echo $MateriasCursadas; ?></td>
                <!--<td><input type="checkbox"> </td>-->
                <td><button onclick="mostrar_box_egresar(<?php echo $v['Id_ofe_alum']; ?>)">Egresar</button></td>
            </tr>
            <?php
        }
    }
}


if($_GET['action']=="download_excel"){
    $base= new base();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'MATRICULA');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOMBRE');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'OFERTA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'CARRERA');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'ORIENTACIÓN:');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'GRADO');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'OPCIÓN DE PAGO');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'MATERIAS CURSADAS');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','I') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrados= new DaoGrados();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    
    $query="";

    if($_GET['Id_ofe']>0){
        $query=$query." AND Id_ofe=".$_GET['Id_ofe'];
    }
    if($_GET['Id_esp']>0){
        $query=$query." AND Id_esp=".$_GET['Id_esp'];
    }
    if($_GET['Id_ori']>0){
        $query=$query." AND Id_ori=".$_GET['Id_ori'];
    }
    $count=1;
    foreach ($DaoAlumnos->getAlumnos(null,$query) as $k => $v) {
        $nombre_ori = "";
        $oferta = $DaoOfertas->show($v['Id_ofe']);
        $esp = $DaoEspecialidades->show($v['Id_esp']);
        if ($v['Id_ori'] > 0) {
            $ori = $DaoOrientaciones->show($v['Id_ori']);
            $nombre_ori = $ori->getNombre();
        }
        $opcion = "Plan por materias";
        if ($v['Opcion_pago'] == 2) {
            $opcion = "Plan completo";
        }

        $Grado = $DaoOfertasAlumno->getLastCicloOfertaSinCiclo($v['Id_ofe_alum']);
        $Grado = $DaoGrados->show($Grado['Id_grado']);


        $ban = 0;
        //Verificar las materias de la especialidad
        $MateriasCursadas = 0;

        foreach ($DaoMateriasEspecialidad->getMateriasEspecialidad($v['Id_esp']) as $k2 => $v2) {
            $query = "SELECT * FROM ofertas_alumno 
            JOIN ciclos_alum_ulm ON ofertas_alumno.Id_ofe_alum=ciclos_alum_ulm.Id_ofe_alum
            JOIN materias_ciclo_ulm ON ciclos_alum_ulm.Id_ciclo_alum= materias_ciclo_ulm.Id_ciclo_alum
            JOIN Materias_especialidades ON materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
            JOIN materias_uml ON Materias_especialidades.Id_mat=materias_uml.Id_mat
            WHERE ofertas_alumno.Id_ofe_alum=" . $v['Id_ofe_alum'] . " AND materias_ciclo_ulm.Id_mat_esp=" . $v2->getId();
            $consulta=$base->advanced_query($query);
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if ($totalRows_consulta > 0 && (($row_consulta['CalTotalParciales'] >= $row_consulta['Promedio_min']) || ($row_consulta['CalExtraordinario'] >= $row_consulta['Promedio_min']) || ($row_consulta['CalEspecial'] >= $row_consulta['Promedio_min']))) {
                $ban = 1;
                $MateriasCursadas++;
            } else {
                $ban = 0;
            }
        }

        if ($ban == 1) {
            $count++;
            $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
            $objPHPExcel->getActiveSheet()->setCellValue("B$count", $v['Matricula']);
            $objPHPExcel->getActiveSheet()->setCellValue("C$count", $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins']);
            $objPHPExcel->getActiveSheet()->setCellValue("D$count", $oferta->getNombre_oferta());
            $objPHPExcel->getActiveSheet()->setCellValue("E$count", $esp->getNombre_esp());
            $objPHPExcel->getActiveSheet()->setCellValue("F$count", $nombre_ori);
            $objPHPExcel->getActiveSheet()->setCellValue("G$count", $Grado->getGrado());
            $objPHPExcel->getActiveSheet()->setCellValue("H$count", $opcion);
            $objPHPExcel->getActiveSheet()->setCellValue("I$count", $MateriasCursadas);
        }
    }
  
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=AlumnosPorEgresar-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}