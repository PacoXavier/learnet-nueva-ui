<?php
require_once('estandares/includes.php');
if (!isset($perm['67'])) {
    header('Location: home.php');
}
require_once('clases/DaoNotificaciones.php');
require_once('clases/DaoNotificacionesTipoUsuario.php');
require_once('clases/DaoTiposUsuarios.php');
require_once('clases/DaoParametrosPlantel.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoParametros.php');
require_once('clases/modelos/Notificaciones.php');
require_once('clases/modelos/NotificacionesTipoUsuario.php');
require_once('clases/modelos/base.php');
require_once("clases/DaoDocumentosDrive.php");	

$DaoNotificaciones = new DaoNotificaciones();
$DaoNotificacionesTipoUsuario = new DaoNotificacionesTipoUsuario();
$DaoUsuarios = new DaoUsuarios();
$DaoDocentes = new DaoDocentes();
$DaoAlumnos = new DaoAlumnos();
$DaoDocumentosDrive= new DaoDocumentosDrive();
$DaoParametrosPlantel= new DaoParametrosPlantel();
$params=$DaoParametrosPlantel->getParametrosPlantelArray();

links_head("Documentos  ");
?>
  <!-- BEGIN Pre-requisites -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
  <script src="https://apis.google.com/js/client:platform.js?onload=start" async defer></script>
  <!-- END Pre-requisites -->
  <!-- Continuing the <head> section -->
  <?php
  if(isset($params['ClientSecretGoogle']) && isset($params['ClientIdGoogle'])){
  ?>
    <script>
      function start() {
        gapi.load('auth2', function() {
          auth2 = gapi.auth2.init({
            client_id: '<?php echo $params['ClientIdGoogle'];?>',
            fetch_basic_profile: false,
            scope: 'profile https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/drive.readonly https://www.googleapis.com/auth/drive.appfolder https://www.googleapis.com/auth/drive.apps.readonly https://www.googleapis.com/auth/drive.file https://www.googleapis.com/auth/drive.appdata https://www.googleapis.com/auth/drive.scripts https://www.googleapis.com/auth/drive.metadata'

          });

          /*
            // Sign the user in, and then retrieve their ID.
            auth2.signIn().then(function() {
              console.log(auth2.currentUser.get().getId());
            });
            */
        });
      }

      function conectar(){
        auth2.grantOfflineAccess({'redirect_uri': 'postmessage'}).then(signInCallback);
      } 
    </script>
<?php
 }
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one" onclick="deleteEditaName()">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-file-pdf-o"></i> <i class="fa fa-file-excel-o"></i> <i class="fa fa-file-word-o"></i> Documentos en Google Drive</h1>
                    <div class="box-filter-reportes">
                    <?php
                    if(isset($params['AccessTokenGoogleDrive']) && isset($params['RefreshTokenGoogleDrive'])){
                        
                        $carpetaPrincipal=$DaoDocumentosDrive->getCarpetaPrincipal();
                        if($carpetaPrincipal->getId()>0){
                           ?>
                            <ul>
                                <li><a href="<?php echo $carpetaPrincipal->getURL()?>" target="_blank" id="link-drive"><i class="fa fa-external-link"></i> Ir a google drive</a></li>
                            </ul>
                           <?php
                        }
                    }
                    ?>
                    </div>
                    
                </div>
                <div id="mascara_tabla">
                    <?php
                    if(isset($params['ClientSecretGoogle']) && isset($params['ClientIdGoogle'])){
                    ?>
                    <!-- Add where you want your sign-in button to render -->
                    <!-- Use an image that follows the branding guidelines in a real app -->
                    <!-- Last part of BODY element in file index.html -->
                    <script>
                    function signInCallback(authResult) {
                      if (authResult['code']) {
                           var params= new Object()
                               params.action="getAccessToken"
                               params.code=authResult['code']
                           var funcionExito =function(resp){
                               window.location.reload()
                          }
                          $.post('documentos_drive_aj.php',params,funcionExito);
                      }
                    }

                    function desconectar(){
                        //auth2.disconnect()
                        if(confirm("¿Está seguro de querer desconectar la cuenta asociada a google drive?")){
                           var params= new Object()
                               params.action="desconectar"
                           var funcionExito =function(resp){
                               //console.log(resp)
                               alert("Desconectado!")
                               window.location="documentos_drive.php"
                          }
                          $.post('documentos_drive_aj.php',params,funcionExito);
                      }
                    }
                    </script>
                    <?php
                    //print_r($params);
                    if(isset($params['AccessTokenGoogleDrive']) && isset($params['RefreshTokenGoogleDrive'])){
                    ?>
                    <button onclick="desconectar()" id="desconectar-cuenta">Desconectar cuenta</button>
                    <ul id="ruta">
                        <!--<li><a href="documentos_drive.php">Mi unidad <i class="fa fa-chevron-right"></i></a></li>-->
                        <?php
                        $count=0;
                        $tipo_usu="";
                        foreach($DaoDocumentosDrive->getRutaActual($_REQUEST['id']) as $file){
                            if($count==0){
                               $link="documentos_drive.php";
                            }elseif($count>0){
                               $link="documentos_drive.php?id=".$file->getId(); 
                               if($count==1){
                                  $tipo_usu=$file->getTipoUsuario();
                               }
                            }
                            ?>
                               <li><a href="<?php echo $link;?>"><?php echo $file->getNombre()?> <i class="fa fa-chevron-right"></i></a></li>
                            <?php
                            $count++;
                        }
                        ?>
                    </ul>
                    <?php
                    }else{
                      ?>
                        <div id="box-button-drive">
                            <h2>Conectar la cuenta asociada a google drive</h2>
                            <div id="signinButton" onclick="conectar()"></div>
                        </div>
                      <?php
                    }
                    ?>
                    <ul id="lista-documentos">
                        <?php
                         foreach($DaoDocumentosDrive->getDocumentos($_REQUEST['id']) as $file){
                          ?>
                        <li onclick="event.cancelBubble=true;if (event.stopPropagation) event.stopPropagation();">
                            <i class="fa fa-times close" onclick="deleteFile(<?php echo $file->getId();?>)"></i>
                            <?php
                            if($file->getTipoDoc()=="Carpeta"){
                                ?>
                                 <i class="fa fa-folder" onclick="getFiles(<?php echo $file->getId();?>,'Carpeta')"></i>
                                 <?php
                            }elseif($file->getTipoDoc()=="Documento"){
                                 ?>
                                 <i class="fa fa-file-word-o" onclick="getFiles(<?php echo $file->getId();?>,'Documento','<?php echo $file->getURL()?>')"></i>
                                 <?php   
                            }elseif($file->getTipoDoc()=="Excel"){
                                 ?>
                                 <i class="fa fa-file-excel-o" onclick="getFiles(<?php echo $file->getId();?>,'Excel','<?php echo $file->getURL()?>')"></i>
                                 <?php   
                            }elseif($file->getTipoDoc()=="PowerPoint"){
                                 ?>
                                 <i class="fa fa-file-powerpoint-o" onclick="getFiles(<?php echo $file->getId();?>,'PowerPoint','<?php echo $file->getURL()?>')"></i>
                                 <?php   
                            }else{
                                ?>
                                 <i class="fa fa-file-o" onclick="getFiles(<?php echo $file->getId();?>,'PowerPoint','<?php echo $file->getURL()?>')"></i>
                                 <?php
                            }
                            ?>
                            
                            <div class="box-nombre" onclick="event.cancelBubble=true;if (event.stopPropagation) event.stopPropagation();">
                                 <span onclick="editaName(this,'<?php echo $file->getUID_drive()?>')"><?php echo $file->getNombre()?></span>
                            </div>
                        </li>
                    
                          <?php
                          }
                        ?>
                    </ul>
                    <?php
                    }else{
                        ?>
                        <h2 id="error">Favor de ponerse en contacto con su administrador para habilitar esta funcionalidad</h2>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <table id="usu_login">
                    <tr>
                        <td><div class="nom_usu"><div id="img_usu" 
                                                      <?php if (strlen($usu['Img_usu']) > 0) { ?> style="background-image:url(files/<?php echo $usu['Img_usu'] ?>.jpg) <?php } ?>"></div>
<?php echo $usu['Nombre_usu'] . " " . $usu['ApellidoP_usu'] . " " . $usu['ApellidoM_usu'] ?>
                                <img src="images/linea_uno.png" alt="linea_uno" width="185" height="5" class="lineas"/></div>
                        </td>
                        <td>
                            <div class="opcion">
                                <a href="perfil.php?id=<?php echo $usu['Id_usu'] ?>">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Mi perfil</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/>
                            </div>
                        </td>
                        <td><div class="opcion">
                                <a href="logout.php">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Salir</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/></div>
                        </td>
                    </tr>
                </table>
                <h2><img src="images/flecha_blue.png" alt="flecha_blue" width="6" height="11" />Opciones</h2>
                <ul>
                    <?php
                    if($_REQUEST['id']>0){
                    ?>
                    <li><span onclick="mostrarBoxNuevo()">Nuevo documento</span></li>
                    <li><span onclick="mostrarFinder()">Subir documento</span></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<input type="hidden" id="id-parent" value="<?php echo $_REQUEST['id']?>"/>
<input type="hidden" id="tipo-usu" value="<?php echo $tipo_usu?>"/>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();
