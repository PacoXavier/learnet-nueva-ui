<?php
require_once('Connections/cnn.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoGrupos.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoMateriasEspecialidad.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoDocentes.php');
require_once('clases/PHPExcel.php');
require_once('clases/DaoTurnos.php');

if (!function_exists("GetSQLValueString")) {

  function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

    $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

    switch ($theType) {
      case "text":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "long":
      case "int":
        $theValue = ($theValue != "") ? intval($theValue) : "NULL";
        break;
      case "double":
        $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
        break;
      case "date":
        $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
        break;
      case "defined":
        $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
        break;
    }
    return $theValue;
  }

}

mysql_select_db($database_cnn, $cnn);
mysql_query("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');

if($_POST['action']=="filtro"){
    $base= new base();
    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrupos= new DaoGrupos();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoDocentes= new DaoDocentes();
    $DaoTurnos= new DaoTurnos();

    $DaoCiclos= new DaoCiclos();
    $ciclo=$DaoCiclos->getActual();

    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $perm=array();
    foreach($DaoUsuarios->getPermisosUsuario($_COOKIE['admin/Id_usu']) as $k=>$v){
        $perm[$v['Id_per']]=1;
    }

    $query=" Grupos.Id_ciclo=".$ciclo->getId();
    if($_POST['Id_ciclo']>0){
        $query="  Grupos.Id_ciclo=".$_POST['Id_ciclo'];
        $ciclo=$DaoCiclos->show($_POST['Id_ciclo']);
    }
    
    if($_POST['Sin_docente']>0){
        $query=$query." AND Id_docen IS NULL";
    }else{
       if($_POST['Id_docen']>0){
        $query=$query." AND Id_docen=".$_POST['Id_docen'];
       }
    }
    if($_POST['Id_turno']>0){
        $query=$query." AND Turno=".$_POST['Id_turno'];
    }
    if($_POST['Id_mat']>0){
        $query=$query." AND Grupos.Id_mat=".$_POST['Id_mat'];
    }

    $query_Grupos  = "SELECT * , Grupos.Id_grupo AS Grupo FROM Grupos 
            LEFT JOIN Materias_especialidades ON Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
            LEFT JOIN Horario_docente ON Grupos.Id_grupo=Horario_docente.Id_grupo
            LEFT JOIN Docentes ON Horario_docente.Id_docente=Docentes.Id_docen
            WHERE ".$query." AND Grupos.Id_plantel=".$usu->getId_plantel()." AND Activo_grupo=1 GROUP BY Grupos.Id_grupo ORDER BY Grupos.Id_grupo DESC";
     foreach($base->advanced_query($query_Grupos) as $k=>$v){
         
            $mat = $DaoMaterias->show($v['Id_mat']);
            $mat_esp=$DaoMateriasEspecialidad->show($v['Id_mat_esp']);

            $tur = $DaoTurnos->show($v['Turno']);
            $turno=$tur->getNombre();
            
            $nombre_ori="";
            if($v['Id_ori']>0){
              $ori = $DaoOrientaciones->show($v['Id_ori']);
              $nombre_ori=$ori->getNombre();
            }


            $TotalAsis=0; 
            $arrayAsistencias=array();
            if($v['Grupo']>0){
                $query_Asistencias = "SELECT * FROM Asistencias WHERE Id_grupo=".$v['Grupo'];
                $Asistencias = mysql_query($query_Asistencias, $cnn) or die(mysql_error());
                $row_Asistencias = mysql_fetch_array($Asistencias);
                $totalRows_Asistencias = mysql_num_rows($Asistencias);
                if($totalRows_Asistencias>0){
                   do{
                       if($row_Asistencias['Asistio']==1 || $row_Asistencias['Asistio']==0){
                           if(!isset($arrayAsistencias[$row_Asistencias['Fecha_asis']])){
                               $arrayAsistencias[$row_Asistencias['Fecha_asis']]=1;
                           }
                       }
                   }while($row_Asistencias = mysql_fetch_array($Asistencias));
                }
            }
             $NombreMat=$mat->getNombre();
             if(strlen($mat_esp->getNombreDiferente())>0){
                $NombreMat=$mat_esp->getNombreDiferente(); 
             }                                 
             $TotalAlumnos=count($DaoGrupos->getAlumnosGrupo($v['Grupo']));
             $CalPrimerParcial=0;
             $CalSegundoParcial=0;

             $Id_eva_primerParcial=0;
             $Id_eva_SegundoParcial=0;

             $BloqueadosPrimer=0;
             $BloqueadosSegundo=0;


             $query_Evaluaciones = "SELECT * FROM Evaluaciones WHERE Id_mat_esp=".$v['Id_mat_esp']." AND Nombre_eva='PRIMER PARCIAL'";
             $Evaluaciones = mysql_query($query_Evaluaciones, $cnn) or die(mysql_error());
             $row_Evaluaciones= mysql_fetch_array($Evaluaciones);
             $totalRows_Evaluaciones = mysql_num_rows($Evaluaciones);
             if($totalRows_Evaluaciones>0){
                $Id_eva_primerParcial=$row_Evaluaciones['Id_eva'];
             }
             
             

             $query_Evaluaciones = "SELECT * FROM Evaluaciones WHERE Id_mat_esp=".$v['Id_mat_esp']." AND Nombre_eva='SEGUNDO PARCIAL'";
             $Evaluaciones = mysql_query($query_Evaluaciones, $cnn) or die(mysql_error());
             $row_Evaluaciones= mysql_fetch_array($Evaluaciones);
             $totalRows_Evaluaciones = mysql_num_rows($Evaluaciones);
             if($totalRows_Evaluaciones>0){
                $Id_eva_SegundoParcial=$row_Evaluaciones['Id_eva'];
             }

             //Primer parcial
             foreach($DaoGrupos->getAlumnosBYGrupo($v['Grupo']) as $k2=>$v2){

                    $query_Calificaciones = "SELECT * FROM Calificaciones WHERE Id_ciclo_mat=".$v2['Id_ciclo_mat']." AND Id_eva=".$Id_eva_primerParcial;
                    $Calificaciones = mysql_query($query_Calificaciones, $cnn) or die(mysql_error());
                    $row_Calificaciones= mysql_fetch_array($Calificaciones);
                    $totalRows_Calificaciones = mysql_num_rows($Calificaciones);
                    if($totalRows_Calificaciones>0 && ($row_Calificaciones['Calificacion']>0 || $row_Calificaciones['Calificacion']=="AC" || $row_Calificaciones['Calificacion']=="SD" || $row_Calificaciones['Calificacion']=="EC" || $row_Calificaciones['Calificacion']=="CP" || $row_Calificaciones['Calificacion']=="NP")){
                   // if($totalRows_Calificaciones>0 && strlen($row_Calificaciones['Calificacion'])>0){
                        $CalPrimerParcial+=1;
                    }


                    $query_Calificaciones = "SELECT * FROM Calificaciones WHERE Id_ciclo_mat=".$v2['Id_ciclo_mat']." AND Id_eva=".$Id_eva_SegundoParcial;
                    $Calificaciones = mysql_query($query_Calificaciones, $cnn) or die(mysql_error());
                    $row_Calificaciones= mysql_fetch_array($Calificaciones);
                    $totalRows_Calificaciones_se = mysql_num_rows($Calificaciones);
                    if($totalRows_Calificaciones_se>0 && ($row_Calificaciones['Calificacion']>0 || $row_Calificaciones['Calificacion']=="AC" || $row_Calificaciones['Calificacion']=="SD" || $row_Calificaciones['Calificacion']=="EC" || $row_Calificaciones['Calificacion']=="CP" || $row_Calificaciones['Calificacion']=="NP")){
                    //if($totalRows_Calificaciones>0 && strlen($row_Calificaciones['Calificacion'])>0){
                        $CalSegundoParcial+=1;
                    }

                    if($v2['Id_ofe_alum']>0){
                        $Adeudo=$DaoAlumnos->getAdeudoOfertaAlumno($v2['Id_ofe_alum']);
                        if($Adeudo>0){
                             $BloqueadosPrimer++;
                             $BloqueadosSegundo++;
                        }
                    }  
             }

             //$FaltantesPrimero=($TotalAlumnos-($CalPrimerParcial+$BloqueadosPrimer));
             $FaltantesPrimero=($TotalAlumnos-($CalPrimerParcial));
             $classPrimer='class="pink"';
             $TextPrimerParcial="Faltantes ".$FaltantesPrimero;
             if($FaltantesPrimero<=0){
                $TextPrimerParcial="Completo";
                $classPrimer='class="green"';
             }

             //$FaltantesSegundo=($TotalAlumnos-($CalSegundoParcial+$BloqueadosSegundo));
             $FaltantesSegundo=($TotalAlumnos-($CalSegundoParcial));
             $classSegundo='class="pink"';
             $TextSegundoParcial="Faltantes ".$FaltantesSegundo;
             if($FaltantesSegundo<=0){
                $TextSegundoParcial="Completo";
                $classSegundo='class="green"';
             }
             
             $resp=$DaoGrupos->getDocenteGrupo($v['Grupo']);
             $NombreDocente="";
             if($resp['Id_docente']>0){
                 $docente=$DaoDocentes->show($resp['Id_docente']);
                 $NombreDocente=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
             }
    ?>
    <tr>
        <td><?php echo $v['Clave']?> </td>
        <td><?php echo $ciclo->getClave()?> </td>
        <td style="width: 120px;"><?php echo $NombreMat?></td>
        <td><?php echo $nombre_ori;?></td>
        <td><?php echo $turno;?></td>
        <td style="text-align: center;"><?php echo $v['Capacidad']?></td>
        <td style="text-align: center;"><?php echo $NombreDocente?></td>
        <td style="color:black;font-weight: bold;text-align: center;"><?php echo count($arrayAsistencias);?></td>
        <td <?php echo $classPrimer?>><?php echo $TextPrimerParcial;?></td>
        <td <?php echo $classSegundo?>><?php echo $TextSegundoParcial;?></td>
        <td style="text-align: center;"><a href="reporte_alumnos_grupo.php?id=<?php echo $v['Id_grupo']?>" target="_blank"><button>Alumnos</button></a>
            <?php
            if(isset($perm['60'])){
            ?>
            <a href="reporte_capturar_calificaciones.php?id=<?php echo $v['Id_grupo']?>" target="_blank"><button>Calificaciones</button></a>
            <?php
            }
            if(isset($perm['61'])){
            ?>
            <!--<a href="reporte_capturar_asistencias.php?id=<?php echo $v['Id_grupo']?>" target="_blank"><button>Asistencias</button></a>-->
            <?php
            }
            ?>
        </td>
    </tr>
    <?php
    }
}



if($_GET['action']=="download_excel"){
    $base= new base();
    $DaoTurnos= new DaoTurnos();
    $gris="ACB9CA";
    
    $objPHPExcel = new PHPExcel();	
    $objPHPExcel->getActiveSheet()->setCellValue('A1', '#');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'CLAVE GRUPAL');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'CICLO');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'MATERIA');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'ORIENTACIÓN');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'TURNO');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'CAPACIDAD');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'PROFESOR');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'NUM. CLASES');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'PRIMER PARCIAL');
    $objPHPExcel->getActiveSheet()->setCellValue('k1', 'SEGUNDO PARCIAL');
    
    //Altura de la celda
    $objPHPExcel->getActiveSheet()->getRowDimension("1")->setRowHeight('20');
                
    //Establece el width automatico de la celda 
    foreach($base->xrange('A','K') as $columnID){
        //Ancho
        $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        //Font
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFont()->setBold(true);
        //Alineacion vertical
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        //Borders
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //Background
        $objPHPExcel->getActiveSheet()->getStyle($columnID."1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($gris);
    }
    

    $DaoOfertas= new DaoOfertas();
    $DaoAlumnos= new DaoAlumnos();
    $DaoEspecialidades= new DaoEspecialidades();
    $DaoOrientaciones= new DaoOrientaciones();
    $DaoOfertasAlumno= new DaoOfertasAlumno();
    $DaoGrupos= new DaoGrupos();
    $DaoMaterias= new DaoMaterias();
    $DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
    $DaoDocentes= new DaoDocentes();

    $DaoCiclos= new DaoCiclos();
    $ciclo=$DaoCiclos->getActual();

    $DaoUsuarios= new DaoUsuarios();
    $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);

    $query=" Grupos.Id_ciclo=".$ciclo->getId();
    if($_GET['Id_ciclo']>0){
        $query="  Grupos.Id_ciclo=".$_GET['Id_ciclo'];
        $ciclo=$DaoCiclos->show($_GET['Id_ciclo']);
    }
    
    if($_GET['Sin_docente']>0){
        $query=$query." AND Id_docen IS NULL";
    }else{
       if($_GET['Id_docen']>0){
        $query=$query." AND Id_docen=".$_GET['Id_docen'];
       }
    }
    if($_GET['Id_turno']>0){
        $query=$query." AND Turno=".$_GET['Id_turno'];
    }
    if($_GET['Id_mat']>0){
        $query=$query." AND Grupos.Id_mat=".$_GET['Id_mat'];
    }
    $count=1;
    $query_Grupos  = "SELECT * , Grupos.Id_grupo AS Grupo FROM Grupos 
            LEFT JOIN Materias_especialidades ON Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
            LEFT JOIN Horario_docente ON Grupos.Id_grupo=Horario_docente.Id_grupo
            LEFT JOIN Docentes ON Horario_docente.Id_docente=Docentes.Id_docen
            WHERE ".$query." AND Grupos.Id_plantel=".$usu->getId_plantel()." AND Activo_grupo=1 GROUP BY Grupos.Id_grupo ORDER BY Grupos.Id_grupo DESC";
     foreach($base->advanced_query($query_Grupos) as $k=>$v){
         
            $mat = $DaoMaterias->show($v['Id_mat']);
            $mat_esp=$DaoMateriasEspecialidad->show($v['Id_mat_esp']);

            $tur = $DaoTurnos->show($v['Turno']);
            $turno=$tur->getNombre();
            
            $nombre_ori="";
            if($v['Id_ori']>0){
              $ori = $DaoOrientaciones->show($v['Id_ori']);
              $nombre_ori=$ori->getNombre();
            }


            $TotalAsis=0; 
            $arrayAsistencias=array();
            if($v['Grupo']>0){
                $query_Asistencias = "SELECT * FROM Asistencias WHERE Id_grupo=".$v['Grupo'];
                $Asistencias = mysql_query($query_Asistencias, $cnn) or die(mysql_error());
                $row_Asistencias = mysql_fetch_array($Asistencias);
                $totalRows_Asistencias = mysql_num_rows($Asistencias);
                if($totalRows_Asistencias>0){
                   do{
                       if($row_Asistencias['Asistio']==1 || $row_Asistencias['Asistio']==0){
                           if(!isset($arrayAsistencias[$row_Asistencias['Fecha_asis']])){
                               $arrayAsistencias[$row_Asistencias['Fecha_asis']]=1;
                           }
                       }
                   }while($row_Asistencias = mysql_fetch_array($Asistencias));
                }
            }
             $NombreMat=$mat->getNombre();
             if(strlen($mat_esp->getNombreDiferente())>0){
                $NombreMat=$mat_esp->getNombreDiferente(); 
             }                                 
             $TotalAlumnos=count($DaoGrupos->getAlumnosGrupo($v['Grupo']));
             $CalPrimerParcial=0;
             $CalSegundoParcial=0;

             $Id_eva_primerParcial=0;
             $Id_eva_SegundoParcial=0;

             $BloqueadosPrimer=0;
             $BloqueadosSegundo=0;


             $query_Evaluaciones = "SELECT * FROM Evaluaciones WHERE Id_mat_esp=".$v['Id_mat_esp']." AND Nombre_eva='PRIMER PARCIAL'";
             $Evaluaciones = mysql_query($query_Evaluaciones, $cnn) or die(mysql_error());
             $row_Evaluaciones= mysql_fetch_array($Evaluaciones);
             $totalRows_Evaluaciones = mysql_num_rows($Evaluaciones);
             if($totalRows_Evaluaciones>0){
                $Id_eva_primerParcial=$row_Evaluaciones['Id_eva'];
             }

             $query_Evaluaciones = "SELECT * FROM Evaluaciones WHERE Id_mat_esp=".$v['Id_mat_esp']." AND Nombre_eva='SEGUNDO PARCIAL'";
             $Evaluaciones = mysql_query($query_Evaluaciones, $cnn) or die(mysql_error());
             $row_Evaluaciones= mysql_fetch_array($Evaluaciones);
             $totalRows_Evaluaciones = mysql_num_rows($Evaluaciones);
             if($totalRows_Evaluaciones>0){
                $Id_eva_SegundoParcial=$row_Evaluaciones['Id_eva'];
             }

             //Primer parcial
             foreach($DaoGrupos->getAlumnosBYGrupo($v['Grupo']) as $k2=>$v2){

                    $query_Calificaciones = "SELECT * FROM Calificaciones WHERE Id_ciclo_mat=".$v2['Id_ciclo_mat']." AND Id_eva=".$Id_eva_primerParcial;
                    $Calificaciones = mysql_query($query_Calificaciones, $cnn) or die(mysql_error());
                    $row_Calificaciones= mysql_fetch_array($Calificaciones);
                    $totalRows_Calificaciones = mysql_num_rows($Calificaciones);
                    if($totalRows_Calificaciones>0 && ($row_Calificaciones['Calificacion']>0 || $row_Calificaciones['Calificacion']=="AC" || $row_Calificaciones['Calificacion']=="SD" || $row_Calificaciones['Calificacion']=="EC" || $row_Calificaciones['Calificacion']=="CP" || $row_Calificaciones['Calificacion']=="NP")){
                   // if($totalRows_Calificaciones>0 && strlen($row_Calificaciones['Calificacion'])>0){
                        $CalPrimerParcial+=1;
                    }


                    $query_Calificaciones = "SELECT * FROM Calificaciones WHERE Id_ciclo_mat=".$v2['Id_ciclo_mat']." AND Id_eva=".$Id_eva_SegundoParcial;
                    $Calificaciones = mysql_query($query_Calificaciones, $cnn) or die(mysql_error());
                    $row_Calificaciones= mysql_fetch_array($Calificaciones);
                    $totalRows_Calificaciones_se = mysql_num_rows($Calificaciones);
                    if($totalRows_Calificaciones_se>0 && ($row_Calificaciones['Calificacion']>0 || $row_Calificaciones['Calificacion']=="AC" || $row_Calificaciones['Calificacion']=="SD" || $row_Calificaciones['Calificacion']=="EC" || $row_Calificaciones['Calificacion']=="CP" || $row_Calificaciones['Calificacion']=="NP")){
                    //if($totalRows_Calificaciones>0 && strlen($row_Calificaciones['Calificacion'])>0){
                        $CalSegundoParcial+=1;
                    }

                    if($v2['Id_ofe_alum']>0){
                        $Adeudo=$DaoAlumnos->getAdeudoOfertaAlumno($v2['Id_ofe_alum']);
                        if($Adeudo>0){
                             $BloqueadosPrimer++;
                             $BloqueadosSegundo++;
                        }
                    }  
             }

             //$FaltantesPrimero=($TotalAlumnos-($CalPrimerParcial+$BloqueadosPrimer));
             $FaltantesPrimero=($TotalAlumnos-($CalPrimerParcial));
             $classPrimer='class="pink"';
             $TextPrimerParcial="Faltantes ".$FaltantesPrimero;
             if($FaltantesPrimero<=0){
                $TextPrimerParcial="Completo";
                $classPrimer='class="green"';
             }

             //$FaltantesSegundo=($TotalAlumnos-($CalSegundoParcial+$BloqueadosSegundo));
             $FaltantesSegundo=($TotalAlumnos-($CalSegundoParcial));
             $classSegundo='class="pink"';
             $TextSegundoParcial="Faltantes ".$FaltantesSegundo;
             if($FaltantesSegundo<=0){
                $TextSegundoParcial="Completo";
                $classSegundo='class="green"';
             }
             
             $resp=$DaoGrupos->getDocenteGrupo($v['Grupo']);
             $NombreDocente="";
             if($resp['Id_docente']>0){
                 $docente=$DaoDocentes->show($resp['Id_docente']);
                 $NombreDocente=$docente->getNombre_docen()." ".$docente->getApellidoP_docen()." ".$docente->getApellidoM_docen();
             }
             
            $count++;
            $objPHPExcel->getActiveSheet()->setCellValue("A$count", $count);
            $objPHPExcel->getActiveSheet()->setCellValue("B$count", $v['Clave']);
            $objPHPExcel->getActiveSheet()->setCellValue("C$count", $ciclo->getClave());
            $objPHPExcel->getActiveSheet()->setCellValue("D$count", $NombreMat);
            $objPHPExcel->getActiveSheet()->setCellValue("E$count", $nombre_ori);
            $objPHPExcel->getActiveSheet()->setCellValue("F$count", $turno);
            $objPHPExcel->getActiveSheet()->setCellValue("G$count", $v['Capacidad']);
            $objPHPExcel->getActiveSheet()->setCellValue("H$count", $NombreDocente);
            $objPHPExcel->getActiveSheet()->setCellValue("I$count", count($arrayAsistencias));
            $objPHPExcel->getActiveSheet()->setCellValue("J$count", $TextPrimerParcial);
            $objPHPExcel->getActiveSheet()->setCellValue("K$count", $TextSegundoParcial);    
    }
    
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
    header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header("Content-Disposition: attachment; filename=Grupos-".date('Y-m-d_H:i:s').".xlsx");
    echo $objWriter->save('php://output');
}
