<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 
if(!isset($perm['57'])){
  header('Location: home.php');
}
$DaoMediosEnterar= new DaoMediosEnterar();
links_head("Configurar  ");
?>

<?php
write_head_body();
write_body();

?>
<table id="tabla">
  <tr>
    <td id="column_one">
      <div class="fondo">
        <div id="box_top">
          <h1><i class="fa fa-phone"></i> Medios de comunicaci&oacute;n</h1>
          <span id="add_medio" onclick="mostrar_box_medio()">+ A&ntilde;adir medio</span>
        </div>
        <div id="mascara_tabla">
          <table  class="table">
            <thead>
             <tr>
               <td>#</td>
               <td>Nombre</td>
                <td>Fecha de inicio</td>
                <td>Fecha de fin</td>
                <td class="td-center">Estatus</td>
                <td class="td-center">Acciones</td>
              </tr>
            </thead>
            <tbody>
              <?php
              $count=1;
              foreach($DaoMediosEnterar->getMedios() as $medio) {
                  $txt="Inactivo";
                  $color="red";
                  if((date('Y-m-d')>=$medio->getFechaIni() && date('Y-m-d')<=$medio->getFechaFin()) || ($medio->getFechaIni()==null && $medio->getFechaFin()==null )){
                     $txt="Activo"; 
                     $color="green";
                  }
                  ?>
                <tr>
                    <td><?php echo $count ?></td>
                    <td><?php echo $medio->getMedio() ?></td>
                    <td><?php echo $medio->getFechaIni() ?></td>
                    <td><?php echo $medio->getFechaFin() ?></td>
                    <td class="td-center" style="color:<?php echo $color;?>"><?php echo $txt?></td>
                    <td class="td-center">
                         <button onclick="delete_medio(<?php echo $medio->getId(); ?>)">Eliminar</button>
                         <button onclick="mostrar_box_medio(<?php echo $medio->getId(); ?>)">Editar</button>
                    </td>
                  </tr>
                  <?php
                  $count++;
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </td>
    <td id="column_two">
      <div id="box_menus">
        <?php
        require_once 'estandares/menu_derecho.php';
        ?>
        <ul>
          <li onclick="change_section('medios')"><span><i class="fa fa-phone"></i> Medios de comunicaci&oacute;n</span></li>
          <li  onclick="change_section('medios_contacto')"><span><i class="fa fa-location-arrow"></i> Medios de contacto</span></li>
          <li onclick="change_section('documentos')"><span><i class="fa fa-file-text-o"></i> Documentos</span></li>
          <li onclick="change_section('documentos_oferta')"><span><i class="fa fa-file-text-o"></i> Documentos Ofertas</span></li>
          <li onclick="change_section('grupos')"><span><i class="fa fa-users"></i> Grupos</span></li>
          <li  onclick="change_section('ciclos')"><span><i class="fa fa-calendar"></i> Ciclos</span></li>
          <li  onclick="change_section('aulas')"><span><i class="fa fa-university"></i> Aulas</span></li>
          <li  onclick="change_section('turnos')"><span><i class="fa fa-clock-o" aria-hidden="true"></i> Turnos</span></li>
           <li  onclick="change_section('miscelaneos')"><span><i class="fa fa-usd"></i> Miscelaneos</span></li>
          <li  onclick="change_section('descuentos')"><span><i class="fa fa-th"></i> Paquetes descuentos</span></li>
          <li  onclick="change_section('motivos_bajas')"><span><i class="fa fa-eraser"></i> Motivos de baja</span></li>
          <li  onclick="change_section('tipos_usuarios')"><span><i class="fa fa-user"></i> Tipos de usuarios</span></li>
          <li  onclick="change_section('metodos_pago')"><span><i class="fa fa-money"></i> M&eacute;todos de pago</span></li>
          <li onclick="change_section('categorias_libro')"><span><i class="fa fa-book"></i> Categor&iacute;as libros</span></li>
          <li onclick="change_section('tiposactivo')"><span><i class="fa fa-desktop" aria-hidden="true"></i> Tipos de activo</span></li>
          <li onclick="change_section('becas')"><span><i class="fa fa-list"></i> Tipos de beca</span></li>
          <li  onclick="change_section('parametros')"><span><i class="fa fa-cog"></i> Par&aacute;metros</span></li>
        </ul>
      </div>
    </td>
  </tr>
</table>
<input type="file" id="files" name="files">
<?php
write_footer();
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

