<?php
require_once('estandares/includes.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoDiasInhabiles.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoAulas.php');
require_once('clases/DaoHoras.php');
require_once('clases/DaoGrupos.php');
require_once('clases/DaoHorarioExamenGrupo.php');
require_once('clases/DaoDocentes.php');
require_once('clases/modelos/HorarioExamenGrupo.php');
require_once('clases/DaoPeriodosExamenes.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoDocenteExamen.php');
require_once('clases/DaoGrupoExamen.php');

$base= new base();
$DaoHoras= new DaoHoras();
$DaoGrupos= new DaoGrupos();
$DaoAulas= new DaoAulas();
$DaoDocentes= new DaoDocentes();
$DaoHorarioExamenGrupo= new DaoHorarioExamenGrupo();
$DaoMaterias= new DaoMaterias();
$DaoCiclos= new DaoCiclos();
$DaoPeriodosExamenes= new DaoPeriodosExamenes();
$DaoUsuarios=new DaoUsuarios();
$DaoGrupoExamen= new DaoGrupoExamen();
$DaoDocenteExamen= new DaoDocenteExamen();

$ciclo=$DaoCiclos->getActual($_usu->getId_plantel());
links_head("Horario de exámenes  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-clock-o"></i> Horario de exámenes </h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Filtros</li>
                        <!--<li onclick="mostrar_box_email()"><i class="fa fa-envelope"></i> Email</li>-->
                        <li onclick="download_excel()"><i class="fa fa-download"></i> Descargar</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Docente(s)</td>
                                <td>Grupo(s)</td>
                                <td>Materia</td>
                                <td>Aula</td>
                                <td class="td-center">Fecha de exámen</td>
                                <td class="td-center">Hora inicio</td>
                                <td class="td-center">Hora Fin</td>
                                <td class="td-center">Ciclo</td>
                                <td>Período de exámen</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count=1;
                            foreach ($DaoHorarioExamenGrupo->getExamenesCiclo($ciclo->getId()) as $examen){
                                     $per=$DaoPeriodosExamenes->show($examen->getId_periodoExamen());
                                     $grupos=array();
                                     $docentes=array();
                                     foreach($DaoGrupoExamen->getGruposExamenHorario($examen->getId()) as $ObjGrupo){
                                               $grupo=$DaoGrupos->show($ObjGrupo->getIdGrupo());
                                               $clave=$DaoGrupos->covertirCadena($grupo->getClave(), 1);
                                               array_push($grupos, $clave);
                                               $materia=$DaoMaterias->show($grupo->getId_mat());
                                     } 

                                     foreach($DaoDocenteExamen->getDocentesExamenHorario($examen->getId()) as $ObjDocente){
                                           $docente=$DaoDocentes->show($ObjDocente->getId_docen());
                                           $docenteNombre=$DaoDocentes->covertirCadena($docente->getNombre_docen()." ".$docente->getApellidoP_docen(),1);
                                           array_push($docentes, $docenteNombre);
                                     }

                                     $start=  substr($examen->getStart(),strpos($examen->getStart(), "T")+1);
                                     $end=  substr($examen->getEnd(),strpos($examen->getEnd(), "T")+1);
                                     $aula=$DaoAulas->show($examen->getId_aula());
                                     $c=$DaoCiclos->show($examen->getId_ciclo());
                                 ?>
                                         <tr>
                                           <td><?php echo $count;?></td>
                                           <td><?php echo implode(", ", $docentes)?></td>
                                           <td><?php echo implode(", ", $grupos)?></td>
                                           <td><?php echo $DaoDocentes->covertirCadena($materia->getNombre(),2)?></td>
                                           <td><?php echo $DaoDocentes->covertirCadena($aula->getNombre_aula(),2)?></td>
                                           <td class="td-center"><?php echo $examen->getFechaAplicacion() ?></td>
                                           <td class="td-center"><?php echo $start; ?></td>
                                           <td class="td-center"><?php echo $end; ?></td>
                                           <td class="td-center"><?php echo $c->getClave(); ?></td>
                                           <td><?php echo $per->getNombre(); ?></td>
                                         </tr>
                                         <?php
                                         $count++;
                                }
                                  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
        <div class="boxUlBuscador">
            <p>Docente<br><input type="text"  id="Id_docente" class="buscarFiltro" onkeyup="buscarDocent(this)" /></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <div class="boxUlBuscador">
            <p>Grupo<br><input type="text"  id="Id_grupo" class="buscarFiltro" onkeyup="buscarGruposCiclo(this)"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <div class="boxUlBuscador">
            <p>Materia<br><input type="text"  id="Id_materia" class="buscarFiltro" onkeyup="buscarMateria(this)"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p>Aula<br>
          <select id="Id_aula">
              <option value="0"></option>
              <?php
              foreach($DaoAulas->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre_aula(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p>Ciclo<br>
          <select id="ciclo">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
              <option value="<?php echo $v->getId() ?>" <?php if($ciclo->getId()==$v->getId()){ ?> selected="selected" <?php } ?>><?php echo $v->getClave(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p>Fecha inicio:<br><input type="date" id="fechaIni"/></p>
        <p>Fecha inicio:<br><input type="date" id="fechaFin"/></p>
        
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();
