<?php
require_once('estandares/includes.php');
require_once('require_daos.php');

$base = new base();
$DaoOfertas = new DaoOfertas();
$DaoAlumnos = new DaoAlumnos();
$DaoEspecialidades = new DaoEspecialidades();
$DaoOrientaciones = new DaoOrientaciones();
$DaoOfertasAlumno = new DaoOfertasAlumno();
$DaoGrados = new DaoGrados();
$DaoMediosEnterar = new DaoMediosEnterar();
$DaoCiclos = new DaoCiclos();
$DaoTurnos = new DaoTurnos();

links_head("Ingresos  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Ciclo de ingreso</h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Filtros</li>
                        <li onclick="mostrar_box_email()"><i class="fa fa-envelope"></i> Email</li>
                        <li onclick="download_excel()"><i class="fa fa-download"></i> Descargar</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Tipo</td>
                                <td>Ciclo</td>
                                <td>Matricula</td>
                                <td>Nombre</td>
                                <td>Tel.</td>
                                <td>Cel.</td>
                                <td>Email</td>
                                <td style="width: 120px">Oferta</td>
                                <td style="width: 150px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td>Turno</td>
                                <td>Usuario</td>
                                <td>Opción de pago</td>
                                <td>Medio</td>
                                <td><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h1>Filtros</h1>
    <p>Tipo<br>
        <select id="tipo">
            <option value="-1"></option>
            <option value="0">Interesados</option>
            <option value="1">Alumnos</option>
        </select>
    </p>
    <p>Ciclo<br>
        <select id="ciclo">
            <option value="0"></option>
            <?php
            foreach ($DaoCiclos->showAll() as $k => $v) {
                ?>
                <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
                <?php
            }
            ?>
        </select>
    </p>
    <div class="boxUlBuscador">
        <p>Usuario<br><input type="search"  class="buscarFiltro" onkeyup="buscarUsu()" placeholder="Nombre"/></p>
        <ul class="Ulbuscador"></ul>
    </div>
    <p>Fecha inicio<br><input type="date" id="fecha_ini"/>
    <p>Fecha fin<br><input type="date" id="fecha_fin"/>
    <p>Turno<br>
        <select id="turno">
            <option value="0"></option>
            <?php
            foreach ($DaoTurnos->getTurnos() as $turno) {
                ?>
                <option value="<?php echo $turno->getId() ?>" ><?php echo $turno->getNombre() ?></option>
                <?php
            }
            ?>
        </select>
    </p>
    <p>Oferta<br>
        <select id="oferta" onchange="update_curso_box_curso()">
            <option value="0"></option>
            <?php
            foreach ($DaoOfertas->showAll() as $k => $v) {
                ?>
                <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
                <?php
            }
            ?>
        </select>
    </p>
    <p>Especialidad:<br>
        <select id="curso" onchange="update_orientacion_box_curso();
                    update_grados_ofe()">
            <option value="0"></option>
        </select>
    </p>
    <div id="box_orientacion"></div>
    <p>Opci&oacute;n de pago:<br>
        <select id="opcion">
            <option value="0"></option>
            <option value="1">POR MATERIAS</option>
            <option value="2">PLAN COMPLETO</option>
        </select>
    </p>
    <p>Medio por el que se entero:<br>
        <select id="medio_entero" >
            <option value="0"></option>
            <?php
            foreach ($DaoMediosEnterar->showAll() as $k2 => $v2) {
                ?>
                <option value="<?php echo $v2->getId() ?>"><?php echo $v2->getMedio() ?></option>
                <?php
            }
            ?>
        </select>
    </p>
    <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<script src="js/ckeditor/ckeditor.js"></script>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();
