<?php
require_once('estandares/includes.php');
require_once('require_daos.php');
if (!isset($perm['51'])) {
    header('Location: home.php');
}
$DaoActivos= new DaoActivos();
$DaoDocentes= new DaoDocentes();
$DaoUsuarios= new DaoUsuarios();
$DaoAlumnos= new DaoAlumnos();
$DaoHistorialPrestamoActivo= new DaoHistorialPrestamoActivo();

$Id_activo=0;
$Id_hist=0;
$Fecha_entrega="";
$Fecha_devolucion="";
$disponible="";

links_head("Préstamo activos  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-keyboard-o"></i> Préstamo activos</h1>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <li>Buscar Activo<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o modelo"/>
                            <ul id="buscador_int"></ul>
                        </li>
                    </ul>
                </div>
                <?php
                if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                    $activo = $DaoActivos->show($_REQUEST['id']);
                    $Id_activo=$activo->getId();
                    
                    if ($activo->getDisponible() == 1) {
                        $disponible=1;
                        $disponibilidad = "Disponible";
                        $style = 'style="color:green;"';
                    } else {
                        $disponible=0;
                        $disponibilidad = "No Disponible";
                        $style = 'style="color:red;"';

                        $hist=$DaoHistorialPrestamoActivo->getLastHistoriaActivo($Id_activo);
                        if($hist->getId()>0){
                            $Id_hist=$hist->getId();
                            $Fecha_entrega=$hist->getFecha_entrega();
                            $Fecha_devolucion=$hist->getFecha_devolucion();
                            
                            if ($hist->getTipo_usu() == "usu" && $hist->getUsu_recibe()>0) {
                                $usuRecibe = $DaoUsuarios->show($hist->getUsu_recibe());
                                $nombre = $usuRecibe->getNombre_usu() . " " . $usuRecibe->getApellidoP_usu() . " " . $usuRecibe->getApellidoM_usu();
                                $tipo_usu = "Usuario";
                                $codigo = $usuRecibe->getClave_usu();
                            } elseif ($hist->getTipo_usu() == "docen" && $hist->getUsu_recibe()>0) {
                                $usuRecibe = $DaoDocentes->show($hist->getUsu_recibe());
                                $nombre = $usuRecibe->getNombre_docen() . " " . $usuRecibe->getApellidoP_docen() . " " . $usuRecibe->getApellidoM_docen();
                                $tipo_usu = "Docente";
                                $codigo = $usuRecibe->getClave_docen();
                            } elseif ($hist->getTipo_usu() == "alum" && $hist->getUsu_recibe()>0) {
                                $usuRecibe= $DaoAlumnos->show($historial->getUsu_recibe());
                                $nombre = $usuRecibe->getNombre() . " " . $usuRecibe->getApellidoP() . " " . $usuRecibe->getApellidoM();
                                $tipo_usu = "Alumno";
                                $codigo = $usuRecibe->getMatricula();
                            }
                        }
                    }
                }
                ?>
                <div class="seccion">
                    <h2>Datos del activo</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Modelo<br><input type="text" value="<?php echo $activo->getModelo() ?>" id="modelo" disabled="disabled" /></li>
                        <li>Nombre<br><input type="text" value="<?php echo $activo->getNombre() ?>" id="nombre" disabled="disabled"/></li><br>
                        <li>Fecha de prestamo<br><input type="date" value="<?php echo $Fecha_entrega ?>" id="Fecha_devolucion" disabled="disabled"/></li>
                        <li>Fecha de devoluci&oacute;n<br><input type="date" id="Fecha_devolucion" value="<?php echo $Fecha_devolucion ?>" disabled="disabled"/></li><br>
                        <li>Disponibilidad<br><input type="text" id="Estado" value="<?php echo $disponibilidad ?>" disabled="disabled" <?php echo $style; ?>/></li>
                    </ul>
                </div>
                <?php
                if (isset($Id_activo) &&  $Id_activo> 0 && $disponible == 0) {
                    ?>
                    <div class="seccion">
                        <h2>Datos del usuario actual</h2>
                        <span class="linea"></span>
                        <ul class="form">
                            <li>Tipo<br><input type="text" id="Nombre_recibe" value="<?php echo $tipo_usu; ?>" disabled="disabled"/></li>
                            <li>C&oacute;digo <br><input type="text" id="Nombre_recibe" value="<?php echo $codigo; ?>" disabled="disabled"/></li>
                            <li>Nombre<br><input type="text" id="Nombre_recibe" value="<?php echo $nombre; ?>" disabled="disabled"/></li>
                        </ul>
                    </div>
                    <?php
                }
                ?>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <?php
                    if (isset($Id_activo) && $Id_activo > 0) {
                        if ($disponible == 1) {
                            ?>
                            <li><span onclick="box_prestamo()">Capturar prestamo </span></li>
                            <?php
                        } else {
                            ?>
                            <li><span onclick="box_recepcion()">Capturar recepci&oacute;n </span></li>
                            <?php
                        }
                        ?>
                        <li><a href="historial_prestamo_activo.php?id=<?php echo $Id_activo; ?>" target="_blank">Historial de prestamos</a></li>
                        <li><a href="historial_activo.php?id=<?php echo $Id_activo; ?>" target="_blank">Historial de mantenimiento</a></li>
                        <?php
                    }
                    ?>

                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_activo" value="<?php echo $Id_activo; ?>"/>
<input type="hidden" id="Id_prestamo" value="<?php echo $Id_hist; ?>"/>
<?php
write_footer();

