<?php
require_once('estandares/includes.php');
require_once('require_daos.php');
$DaoOfertas = new DaoOfertas();
$DaoCategoriasPago = new DaoCategoriasPago();
$DaoNivelesPago = new DaoNivelesPago();
$DaoOfertasPorCategoria = new DaoOfertasPorCategoria();

links_head("Tabulador de pago  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-table" aria-hidden="true"></i> Tabulador de pago</h1>
                </div>
                <?php
                foreach ($DaoCategoriasPago->getCategoriasPago() as $cat) {
                    ?>
                    <div class="seccion" id_tipo="<?php echo $cat->getId() ?>">
                        <h2>Categoría</h2>
                        <p style="margin-bottom: 20px;">Nombre categoría<br>
                            <input type="text" value="<?php echo $cat->getNombre_tipo() ?>" class="nombre-cat" disabled="disabled"/>
                            <i class="fa fa-pencil-square-o" aria-hidden="true" title="Editar" onclick="mostrarCategoria(<?php echo $cat->getId()?>)"></i>
                            <i class="fa fa-trash" aria-hidden="true" title="Eliminar" onclick="eliminarCategoria(<?php echo $cat->getId()?>)"></i>
                        </p>
                        <table id="main-niveles">
                            <tbody>
                                <tr>
                                    <td>
                                        <h2>Niveles de la categoría</h2>
                                        <?php
                                        foreach ($DaoNivelesPago->getNivelesCategoria($cat->getId()) as $nivel) {
                                            ?>
                                            <ul class="form" id_nivel="<?php echo $nivel->getId() ?>">
                                                <li>Nombre nivel<br><input type="text" value="<?php echo $nivel->getNombre_nivel(); ?>" class="nombre" disabled="disabled"/></li>
                                                <li>Puntaje M&iacute;nimo<br><input type="text" value="<?php echo $nivel->getPuntosMin(); ?>" class="puntaje_profesor" disabled="disabled"/></li>
                                                <li>Puntaje M&aacute;ximo<br><input type="text" value="<?php echo $nivel->getPuntosMax(); ?>" class="puntaje_maximo" disabled="disabled"/></li>
                                                <li>Pago Hora<br><input type="text" value="<?php echo $nivel->getPagoNivel() ?>" class="pagoHora_profesor" disabled="disabled"/></li>
                                                <li>
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true" title="Editar" onclick="mostrarNivel(<?php echo $nivel->getId()?>)"></i>
                                                    <i class="fa fa-trash" aria-hidden="true" title="Eliminar" onclick="eliminarNivel(<?php echo $nivel->getId()?>)"></i>
                                                </li>
                                                <br>
                                                <!--
                                                <li style="display:none">
                                                    <button class="btns btns-primary btns-sm">Guardar</button>
                                                    <button class="btns btns-default btns-sm">Cancelar</button>
                                                </li>
                                                -->
                                            </ul>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <h2>Ofertas de la categoría</h2>
                                        <ul class="lista-ofertas">
                                            <?php
                                            foreach ($DaoOfertas->showAll() as $ofe) {
                                                $checked="";
                                                $existe=$DaoOfertasPorCategoria->getOfertaCategoria($cat->getId(),$ofe->getId());
                                                if($existe->getId()>0){
                                                   $checked="checked"; 
                                                } 
                                                ?>
                                                <li><input type="checkbox" disabled="disabled" value="<?php echo $ofe->getId(); ?>" <?php echo $checked;?>/> <?php echo $ofe->getNombre_oferta(); ?></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul> 
                    <li class="link" onclick="mostrarCategoria(0)">Agregar categoría</li>
                    <li class="link" onclick="mostrarNivel(0)">Agregar nivel</li>
                </ul>
            </div> 
        </td>
    </tr>
</table
<?php
write_footer();

