<?php
require_once('estandares/includes.php');
require_once('estandares/class_usuarios.php');
links_head("Inicio");
write_head_body();
write_body();

$class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
$usu=$class_usuarios->get_usu();

?>
	<div id="box_top">
	     <div id="mascara_user"
	     <?php if(strlen($usu['Img_usu'])>0){?> 
			  style="background-image:url(files/<?php echo $usu['Img_usu']?>.jpg) <?php } ?>">
			  </div><h1>Bienvenido, <span><?php echo ucwords(strtolower($usu['Nombre_usu']." ".$usu['ApellidoP_usu']." ".$usu['ApellidoM_usu']))?></span></h1>
	</div>
	<ul id="lists_seccions">
                  <?php
                  if(isset($perm['30'])){
                      ?>
		  <li>
		      <a href="interesados.php"><img src="images/icon_ins.png" alt="icon_ins" width="193" height="193" /></a>
		      <h2>Interesados</h2>
		      <p>Dar de alta o baja un interesado</p>
		  </li>	
                  <?php
                  }
                  if(isset($perm['41'])){
                  ?>
		  <li>
		      <a href="alumnos.php"><img src="images/icon_gps.png" alt="icon_gps" width="193" height="193" /></a>
		      <h2>Alumnos</h2>
		      <p>Dar de alta, modificar o<br>eliminar información sobre<br>alumnos</p>
		  </li>	
                  <?php
                  }
                  if(isset($perm['45'])){
                  ?>
                  <li>
		      <a href="usuarios.php"><img src="images/icon_us.png" /></a>
		      <h2>Usuarios</h2>
		      <p>Dar de alta, modificar o<br>eliminar información sobre<br>usuarios</p>
		  </li>	
                  <?php
                  }
                  if(isset($perm['42'])){
                  ?>
		  <li>
		      <a href="ofertas_academicas.php"><img src="images/icon_ofe.png" alt="icon_ofe" width="193" height="193" /></a>
		      <h2>Ofertas Acad&eacute;micas</h2>
		      <p>Dar de alta licenciaturas<br>y centros universitarios</p>
		  </li>	
                  <?php
                  }
                  if(isset($perm['57'])){
                  ?>
		  <li>
		      <a href="configurar.php"><img src="images/icon_configurar.png" /></a>
		      <h2>Configurar el sistema</h2>
		      <p>Dar de alta medios, documentos<br> ciclos y grupos</p>
		  </li>	
                  <?php
                  }
                  ?>
		  <!--
		  <li>
		      <img src="images/icon_tnos.png" alt="icon_tnos" width="193" height="193" />
		      <h2>Turnos</h2>
		      <p>Dar de alta y editar horarios</p>
		   </li>
		   <li>
		      <img src="images/icon_cal.png" alt="icon_cal" width="193" height="193" />
		      <h2>Calificaciones</h2>
		      <p>Dar de alta calificaciones</p>
		   </li>
		   -->	
	</ul>
<?php
write_footer();
