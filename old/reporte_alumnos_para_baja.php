<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoMediosEnterar.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoMateriasEspecialidad.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoMateriasCicloAlumno.php');
require_once('clases/DaoCiclosAlumno.php');

$base = new base();
$DaoOfertas = new DaoOfertas();
$DaoAlumnos = new DaoAlumnos();
$DaoEspecialidades = new DaoEspecialidades();
$DaoOrientaciones = new DaoOrientaciones();
$DaoOfertasAlumno = new DaoOfertasAlumno();
$DaoGrados = new DaoGrados();
$DaoMediosEnterar = new DaoMediosEnterar();
$DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
$DaoMaterias = new DaoMaterias();
$DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
$DaoCiclosAlumno= new DaoCiclosAlumno();

$DaoCiclos = new DaoCiclos();
$cicloActual = $DaoCiclos->getActual();

links_head("Alumnos  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Alumnos para dar de baja</h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Filtrar</li>
                        <li onclick="mostrar_box_email()"><i class="fa fa-envelope"></i> Email</li>
                        <li onclick="download_excel()"><i class="fa fa-download"></i> Descargar</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td>Grado</td>
                                <td>Opcion de pago</td>
                                <td>Materia</td>
                                <td>Reprobada</td>
                                <td><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($cicloActual->getId() > 0) {
                                $query = "
                                        SELECT 
                                                materias_ciclo_ulm.*,count(*) AS NumMaterias,
                                                ciclos_alum_ulm.Id_ciclo ,
                                                Materias.Id_mat,Materias.Nombre,Materias.Promedio_min,
                                                ofertas_alumno.*,
                                                inscripciones_ulm.Id_ins,inscripciones_ulm.Matricula,inscripciones_ulm.Nombre_ins,inscripciones_ulm.ApellidoP_ins,inscripciones_ulm.ApellidoM_ins,ofertas_alumno.Opcion_pago
                                        FROM materias_ciclo_ulm 
                                        JOIN Materias_especialidades on materias_ciclo_ulm.Id_mat_esp=Materias_especialidades.Id_mat_esp
                                        JOIN ciclos_alum_ulm ON materias_ciclo_ulm.Id_ciclo_alum=ciclos_alum_ulm.Id_ciclo_alum
                                        JOIN ofertas_alumno ON ciclos_alum_ulm.Id_ofe_alum=ofertas_alumno.Id_ofe_alum
                                        JOIN inscripciones_ulm ON ofertas_alumno.Id_alum=inscripciones_ulm.Id_ins
                                        JOIN (
                                              SELECT * FROM materias_uml 
                                                   WHERE Activo_mat=1 AND Id_plantel=".$_usu->getId_plantel()."
                                             ) AS Materias ON Materias.Id_mat=Materias_especialidades.Id_mat

                                        GROUP BY materias_ciclo_ulm.Id_mat_esp,materias_ciclo_ulm.Id_alum
                                        HAVING NumMaterias>=3  
                                               AND materias_ciclo_ulm.Activo=1
                                               AND ciclos_alum_ulm.Id_ciclo!=".$cicloActual->getId()."
                                               AND ofertas_alumno.Activo_oferta=1";
                                foreach ($base->advanced_query($query) as $v) {
                                    $nombre_ori = "";
                                    $oferta = $DaoOfertas->show($v['Id_ofe']);
                                    $esp = $DaoEspecialidades->show($v['Id_esp']);
                                    $mat=$DaoMaterias->show($v['Id_mat']);
                                    if ($v['Id_ori'] > 0) {
                                        $ori = $DaoOrientaciones->show($v['Id_ori']);
                                        $nombre_ori = $ori->getNombre();
                                    }
                                    $opcion = "Plan por materias";
                                    if ($v['Opcion_pago'] == 2) {
                                        $opcion = "Plan completo";
                                    }
                                    $grado = $DaoOfertasAlumno->getLastGradoOferta($v['Id_ofe_alum']);
                                    $Grado = $DaoGrados->show($grado['Id_grado_ofe']);
                                    
                                    $ban = 0;
                                    $query="SELECT * FROM materias_ciclo_ulm WHERE Id_mat_esp=".$v['Id_mat_esp']." AND Id_alum=".$v['Id_ins'];
                                    foreach ($base->advanced_query($query) as $materiaAlumno) {
                                        $cicloAlu=$DaoCiclosAlumno->show($materiaAlumno['Id_ciclo_alum']);
                                        if($cicloAlu->getId_ciclo()<$cicloActual->getId()){
                                            if ((($materiaAlumno['CalTotalParciales'] < $mat->getPromedio_min()) && strlen($materiaAlumno['CalExtraordinario']) == 0 && strlen($materiaAlumno['CalEspecial']) == 0) ||
                                                  (($materiaAlumno['CalExtraordinario'] < $mat->getPromedio_min()) && strlen($materiaAlumno['CalTotalParciales']) == 0 && strlen($materiaAlumno['CalEspecial']) == 0) ||
                                                  (($materiaAlumno['CalEspecial'] < $mat->getPromedio_min()) && strlen($materiaAlumno['CalTotalParciales']) == 0 && strlen($materiaAlumno['CalExtraordinario']) == 0)) {
                                              $ban++;
                                            }  
                                        }
                                    }
                                    if($ban>=3){
                                    ?>
                                    <tr id_alum="<?php echo $v['Id_ins']; ?>">
                                        <td><?php echo $v['Matricula'] ?></td>
                                        <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                                        <td><?php echo $oferta->getNombre_oferta(); ?></td>
                                        <td><?php echo $esp->getNombre_esp(); ?></td>
                                        <td><?php echo $nombre_ori; ?></td>
                                        <td><?php echo $Grado->getGrado(); ?></td>
                                        <td><?php echo $opcion; ?></td>
                                        <td><?php echo $mat->getNombre(); ?></td>
                                        <td><?php echo $ban; ?></td>
                                        <td><input type="checkbox"> </td>
                                    </tr>
                                    <?php
                                    }
                                }
                            }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <div class="boxfil">
        <h1>Filtros</h1>
        <p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
                <option value="0"></option>
                <?php
                foreach ($DaoOfertas->showAll() as $k => $v) {
                    ?>
                    <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
                    <?php
                }
                ?>
            </select>
        </p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso();">
                <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
        <p>Opci&oacute;n de pago:<br>
            <select id="opcion">
                <option value="0"></option>
                <option value="1">POR MATERIAS</option>
                <option value="2">PLAN COMPLETO</option>
            </select>
        </p>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
    </div>
    <script src="js/ckeditor/ckeditor.js"></script>
    <input type="file" id="files" name="files" multiple="">
    <?php
    write_footer();
