<?php
require_once('estandares/includes.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoCiclosAlumno.php');
require_once('clases/DaoRecargos.php');
require_once('clases/DaoPagosCiclo.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoCiclos.php');

$DaoCiclos= new DaoCiclos();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos=new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();

links_head("Env&iacuteo de calificaciones  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Enviar calificaciones</h1>
                </div>
                <ul class="form">
                    <li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>
                </ul>
                <span class="spanfiltros" onclick="mostrar_filtro()">Mostrar filtros</span>
                <span class="spanCambiar" onclick="confirmarEnvio()">Enviar</span>
                
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td>Opcion de pago</td>
                                <td>Otro email</td>
                                <td>Estatus</td>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach($DaoAlumnos->getAlumnos() as $k=>$v){
                                   $alum = $DaoAlumnos->show($v['Id_ins']);
                                   $nombre_ori="";

                                   $oferta = $DaoOfertas->show($v['Id_ofe']);
                                   $esp = $DaoEspecialidades->show($v['Id_esp']);
                                   if ($v['Id_ori'] > 0) {
                                      $ori = $DaoOrientaciones->show($v['Id_ori']);
                                      $nombre_ori = $ori->getNombre();
                                    }

                                    $opcion="Plan por materias"; 
                                    if($v['Opcion_pago']==2){
                                      $opcion="Plan completo";  
                                    }
                                  ?>
                                          <tr id_alum="<?php echo $alum->getId();?>" id_ofe_alum="<?php echo $v['Id_ofe_alum'];?>">
                                              <td><?php echo $alum->getMatricula() ?></td>
                                              <td style="width: 115px;"><?php echo $alum->getNombre() . " " . $alum->getApellidoP(). " " . $alum->getApellidoM() ?></td>
                                              <td><?php echo $oferta->getNombre_oferta(); ?></td>
                                              <td><?php echo $esp->getNombre_esp(); ?></td>
                                            <td><?php echo $nombre_ori; ?></td>
                                            <td><?php echo $opcion; ?></td>
                                            <td><p><input type="text" class="otro-email"><button onclick="addInput(this)">Añadir email</button></p></td>
                                            <td><span>Sin enviar</span></td>
                                          </tr>
                                          <?php
                                    }
                                  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
	<p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              foreach($DaoOfertas->showAll() as $oferta){
              ?>
                  <option value="<?php echo $oferta->getId() ?>"> <?php echo $oferta->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso();update_grados_ofe()">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
        <p>Ciclo:<br>
           <select id="ciclo">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $ciclo){
              ?>
               <option value="<?php echo $ciclo->getId() ?>"><?php echo $ciclo->getClave() ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p>Opci&oacute;n de pago:<br>
            <select id="opcion">
              <option value="0"></option>
              <option value="1">POR MATERIAS</option>
              <option value="2">PLAN COMPLETO</option>
            </select>
        </p>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();
