 <?php
require_once('estandares/includes.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoCiclos.php');
links_head("Calificaciones  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Calificaciones por materia o grupo</h1>
                </div>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Filtrar</li>
                    </ul>
                </div>
                <ul id="lista-x">
                    <li><div id="box-faltas"></div>Reprobado por faltas</li>
                </ul>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Matricula</td>
                                <td style="width: 100px">Alumno</td>
                                <td>TURNO</td>
                                <td>Grupo</td>
                                <td>Materia</td>
                                <td>Orientaci&oacute;n</td>
                                <td>Faltas</td>
                                <td>Asistencias</td>
                                <td>Justificaciones</td>
                                <td>Porcentaje</td>
                                <td>Calificaci&oacute;n</td>
                                <td>Evaluaci&oacute;n<br> Extraordinaria</td>
                                <td>Evaluaci&oacute;n<br> Especial</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
        <p>Ciclo<br>
          <select id="ciclo">
              <option value="0"></option>
              <?php
              $DaoCiclos = new DaoCiclos();
              foreach($DaoCiclos->showAll() as $ciclo){
              ?>
               <option value="<?php echo $ciclo->getId() ?>"><?php echo $ciclo->getClave() ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <div class="boxUlBuscador">
            <p>Docente<br><input type="search"  class="buscarFiltro" id="Id_docente" onkeyup="buscarDocent(this)"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <div class="boxUlBuscador">
            <p>Grupo<br><input type="search"  class="buscarFiltro" id="Id_grupo" onkeyup="buscarGruposCiclo(this)"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p>Turno<br>
           <select id="turno">
              <option value="0"></option>
              <option value="1">Matutino</option>
              <option value="2">Vespertino</option>
              <option value="3">Nocturno</option>
            </select>
        </p>
	<p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              $DaoOfertas = new DaoOfertas();
              foreach($DaoOfertas->showAll() as $oferta){
              ?>
               <option value="<?php echo $oferta->getId() ?>"> <?php echo $oferta->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_materias()">
              <option value="0"></option>
            </select>
        </p>
        <p>Materia:<br>
            <select id="lista_materias" onclick="verificar_orientaciones()">
	        <option value="0"></option>
              </select>
        </p>
        <div id="box_orientacion"></div>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();
