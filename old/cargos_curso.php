<?php
if (!$_REQUEST['id'] > 0) {
    header("Location: index.php");
}


//El 17 de septiembre modifique la bdd y la interfas
//Quitar pago a cuenta y poner pago adelantado
require_once('estandares/includes.php');
require_once('clases/DaoPagosCiclo.php');
require_once('clases/DaoPagos.php');
require_once('clases/DaoRecargos.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoCiclosAlumno.php');
require_once('clases/DaoMetodosPago.php');
require_once('clases/DaoPaquetesDescuentosAlumno.php');

links_head("Estado de cuenta  ");
write_head_body();
write_body();

$DaoPagosCiclo = new DaoPagosCiclo();
$DaoPagos = new DaoPagos();
$DaoOfertasAlumno = new DaoOfertasAlumno();
$DaoAlumnos = new DaoAlumnos();
$DaoOfertas = new DaoOfertas();
$DaoEspecialidades = new DaoEspecialidades();
$DaoOrientaciones = new DaoOrientaciones();
$DaoCiclos = new DaoCiclos();
$DaoCiclosAlumno = new DaoCiclosAlumno();
$DaoRecargos = new DaoRecargos();
$DaoMetodosPago = new DaoMetodosPago();
$DaoPaquetesDescuentosAlumno= new DaoPaquetesDescuentosAlumno();

$oferta_alumno = $DaoOfertasAlumno->show($_REQUEST['id']);
$alum = $DaoAlumnos->show($oferta_alumno->getId_alum());
$oferta = $DaoOfertas->show($oferta_alumno->getId_ofe());
$esp = $DaoEspecialidades->show($oferta_alumno->getId_esp());
if ($oferta_alumno->getId_ori() > 0) {
    $ori = $DaoOrientaciones->show($oferta_alumno->getId_ori());
    $nombre_ori = $ori->getNombre();
}

$primer_ciclo = $DaoOfertasAlumno->getFirstCicloOfertaSinCiclo($_REQUEST['id']);
$ultimo_ciclo = $DaoOfertasAlumno->getLastCicloOfertaSinCiclo($_REQUEST['id']);

$Opcion_pago = "";
if ($oferta_alumno->getOpcionPago() == 1) {
    $Opcion_pago = "Plan por materias";
} else {
    $Opcion_pago = "Plan completo";
}

$SaldoPagos = $DaoPagos->getTotalPagosACuenta($oferta_alumno->getId());
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-university"></i> Estado de cuenta</h1>
                </div>
                <div class="seccion">
                    <table  id="info_alumn">
                        <tbody><tr><th style="padding-bottom: 15px;text-align: left;">
                                    <font size="3">DATOS DEL ESTUDIANTE</font>
                                </th></tr>
                            <tr><td>
                                    <table width="100%">
                                        <tbody><tr>
                                                <th ><font>C&oacute;digo:</font></th>
                                                <td><font><?php echo $alum->getMatricula() ?></font></td>
                                                <th ><font>Nombre:</font></th>
                                                <td colspan="5"><font><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></font></td>
                                            </tr>
                                            <tr>
                                                <th><font>Nivel:</font></th>
                                                <td><font><?php echo $oferta->getNombre_oferta(); ?></font></td>
                                                <th><font>Admisi&oacute;n:</font></th>
                                                <td></font></td>
                                                <th><font>Último ciclo:</font></th>
                                                <td><font></font></td>
                                            </tr>
                                            <tr>
                                                <th><font>Carrera:</font></th>
                                                <td colspan="7"><font><?php echo $esp->getNombre_esp(); ?></font></td>
                                            </tr>
                                            <tr>
                                                <th><font>Orientaci&oacute;n:</font></th>
                                                <td><font color="navy" face="arial" size="2"><?php echo $nombre_ori; ?></font></td>
                                                <th><font>Opci&oacute;n de pago</font></th>
                                                <td><font color="navy" face="arial" size="2"><?php echo $Opcion_pago; ?></font></td>
                                            </tr>
                                        </tbody></table>
                                </td></tr>
                        </tbody>
                    </table>
                </div>
                <table id="estado-cuenta">
                    <tr>
                        <td id="seccion-cargos">
                            <?php
                            if (isset($perm['39'])) {
                                //Verificamos si ya se generaron los demas cargos
                                if (count($DaoPagosCiclo->getCargosCiclo($primer_ciclo['Id_ciclo_alum'])) > 1 || ($esp->getNum_pagos() == 1 && count($DaoPagosCiclo->getCargosCiclo($primer_ciclo['Id_ciclo_alum'])) >= 1)) {
                                    ?>
                                     <button id="add_pago_miscelaneo" class="boton-normal" onclick="show_miscelaneos()"><i class="fa fa-plus-square"></i> A&ntilde;adir miscelaneo</button>
                                     <button id="buttonBancario" class="boton-normal" onclick="send_email_bancario()"><i class="fa fa-paper-plane"></i> Enviar datos bancarios</button>
                                     <button id="buttonBancario" class="boton-normal" onclick="show_box_paquete_descuento()"><i class="fa fa-plus-square"></i> A&ntilde;adir paquete descuento</button>
                                    <?php
                                } else {
                                    $class = "red";
                                    if ($DaoPagosCiclo->verificarPagoInscripcion($_REQUEST['id']) == 1) {
                                        $class = "green";
                                    }
                                    if ($oferta_alumno->getOpcionPago() == 2) {
                                        $texto = "2.- Presionar bot&oacute;n inscribir para generar los cargos del per&iacute;odo";
                                    } else {
                                        $texto = "2.- Asignar las materias que cursara en el presente ciclo y presionar el bot&oacute;n de generar mensualidades";
                                    }
                                    ?>
                                    <p class="<?php echo $class; ?>">1.- Pagar cargo por inscripci&oacute;n</p>
                                    <p class="red"><?php echo $texto ?></p>
                                    <?php
                                }
                            }
                            $totalCargosOferta = 0;
                            $totalBecasOferta = 0;
                            $totalDescuentosOferta = 0;
                            $totalPaquetesDescuento=0;
                            $totalPagoAdelantado=0;
                            $i = 0;
                            
                            foreach ($DaoCiclosAlumno->getCiclosOferta($oferta_alumno->getId()) as $cicloAlumno) {
                                $ciclo = $DaoCiclos->show($cicloAlumno->getId_ciclo());
                                //Si el alumno tiene un paquete de descuento en el ciclo entonces descontar a los cargos
                                $descuentoPaqueteCiclo=$DaoPaquetesDescuentosAlumno->getPaqueteDescuentoCicloAlumno($cicloAlumno->getId_ciclo(),$oferta_alumno->getId());
                                
                                ?>
                                <table class="table" id-ciclo="<?php echo $cicloAlumno->getId_ciclo() ?>" id-ciclo-alum="<?php echo $cicloAlumno->getId();?>>">
                                    <thead>
                                        <tr>
                                            <td colspan="12" class="clave-ciclo">Cargos del Ciclo <?php echo $ciclo->getClave() ?></td>
                                        </tr>
                                        <tr>
                                            <td class="td-center">#</td>
                                            <td>Concepto</td>
                                            <td class="td-center" width="65px;">Fecha límite</td>
                                            <td class="td-center">Monto</td>
                                            <td class="td-center">Descuento</td>
                                            <td class="td-center">Aplica Desc.</td>
                                            <td class="td-center">Paquete Desc. <?php echo $descuentoPaqueteCiclo->getPorcentaje()."%"?></td>
                                            <td class="td-center">Aplica Paquete Desc.</td>
                                            <td class="td-center">Beca <?php echo $cicloAlumno->getPorcentaje_beca(); ?> %</td>
                                            <td class="td-center">Cantidad pagada</td>
                                            <td class="td-center">Adeudo</td>
                                            <td class="td-center"></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $mensualidad = 0;
                                        $Descuento = 0;
                                        $Adeudo = 0;
                                        $miscelaneos = 0;
                                        $totalAdeudo = 0;
                                        $totalrecargos = 0;
                                        $totalmiscelaneos = 0;
                                        $totalBeca = 0;
                                        $totalPagado = 0;
                                        //Pagos que se usaran solo en el ciclo que se eligio o si pago mas del adeudo delciclo elegido se pasa al siguiente ciclo
                                        if($DaoPagos->getTotalPagosAdelantados($cicloAlumno->getId_ciclo(),$alum->getId(),$_REQUEST['id'])>0){
                                           $pagoAdelantado=$DaoPagos->getTotalPagosAdelantados($cicloAlumno->getId_ciclo(),$alum->getId(),$_REQUEST['id']); 
                                           $SaldoPagos+=$pagoAdelantado;
                                        }
                                        foreach ($DaoPagosCiclo->getCargosCiclo($cicloAlumno->getId()) as $cargo) {
                                            //Recargos
                                            $recargos = 0;
                                            foreach ($DaoRecargos->getRecargosPago($cargo->getId()) as $recargo) {
                                                $recargos+=$recargo->getMonto_recargo();
                                            }
                                            //Miscelaneos
                                            if ($cargo->getTipo_pago() == "miscelaneo") {
                                                $miscelaneos = $cargo->getMensualidad();
                                            }
                                            
                                            $mensualidad+= $cargo->getMensualidad() + $recargos;
                                            $Descuento+=$cargo->getDescuento();
                                            
                                            //$TotalCargo = round($cargo->getMensualidad() - $cargo->getDescuento() - $cargo->getDescuento_beca() + $recargos);
                                            
                                            //Esta linea es para saber si el pago se dirigio a un cargo en especifico,o se fue directamente a cuenta
                                            //Es decir no se escogio ningun cargo y fue entonces uno a cuenta
                                            $text="";
                                            $cantidadPagada=$DaoPagos->getTotalPagosCargo($cargo->getId());
                                            if($cantidadPagada>0){
                                               $text="Pagado por un cargo";
                                               $TotalCargo=round($cargo->getMensualidad() - $cargo->getDescuento() - $cargo->getDescuento_beca() + $recargos);
                                               
                                               
                                               //Si tiene algun descuento por paquete
                                               $decuentoPorPaquete=0;
                                               if($descuentoPaqueteCiclo->getId()>0 && $descuentoPaqueteCiclo->getPorcentaje()>0){
                                                    
                                                    //Aplicar descuento por paquete solo a los cargos de tipo pago
                                                    if($cargo->getTipo_pago()=="pago"){
                                                       $decuentoPorPaquete=($TotalCargo*$descuentoPaqueteCiclo->getPorcentaje())/100; 
                                                    }else{
                                                        $decuentoPorPaquete=0;
                                                    }
                                                      
                                                    
                                                    $totalPaquetesDescuento+=$decuentoPorPaquete;
                                               }
                                               $TotalCargo=$TotalCargo-$decuentoPorPaquete;
                                               $Deuda=$TotalCargo-$cantidadPagada;
                                               //echo $decuentoPorPaquete;
                                               if($TotalCargo>0 && $SaldoPagos>0){
                                                  $MontoPagado = $TotalCargo;
                                                  $SaldoPagos=$SaldoPagos-$Deuda;
                                                  $Adeudo=0;
                                                   
                                               }else{
                                                  $MontoPagado = $cantidadPagada;
                                                  $Adeudo = $Deuda;  
                                               }
                                              

                                            }else{
                                               //Esta linea es por si fue pago a cuenta
                                               $text="Pago a cuenta";
                                               $TotalCargo = round($cargo->getMensualidad() - $cargo->getDescuento() - $cargo->getDescuento_beca() + $recargos);
                                               
                                               //Si tiene algun descuento por paquete
                                               $decuentoPorPaquete=0;
                                               if($descuentoPaqueteCiclo->getId()>0 && $descuentoPaqueteCiclo->getPorcentaje()>0 ){
                                                    
                                                    //Aplicar descuento por paquete solo a los cargos de tipo pago
                                                    if($cargo->getTipo_pago()=="pago"){
                                                       $decuentoPorPaquete=($TotalCargo*$descuentoPaqueteCiclo->getPorcentaje())/100; 
                                                    }else{
                                                        $decuentoPorPaquete=0;
                                                    }

                                                    $totalPaquetesDescuento+=$decuentoPorPaquete;
                                               }
                                               $TotalCargo=$TotalCargo-$decuentoPorPaquete;
                                               
                                                if ($SaldoPagos >= $TotalCargo) {
                                                    $SaldoPagos = $SaldoPagos - $TotalCargo;
                                                   
                                                    $MontoPagado=$TotalCargo;
                                                    $Adeudo = 0;
                                                } elseif ($SaldoPagos < $TotalCargo) {
                                                    
                                                    //Si el saldo es menor al costo del cargo 
                                                    $MontoPagado = $SaldoPagos;
                                                    $Adeudo = $TotalCargo - $MontoPagado;
                                                    $SaldoPagos = 0;
                                                   // echo $MontoPagado."<br>";
                                                }
                                                
                                            }
                                            
                                            
                                            
                                            
                                            $totalPagado+=$MontoPagado;
                                            $totalAdeudo+=$Adeudo;
                                            $totalrecargos+=$recargos;
                                            $totalmiscelaneos+=$miscelaneos;
                                            $totalBeca+=$cargo->getDescuento_beca();

                                            //Fondo
                                            $classAdeudo = "";
                                            //Esconder boton editar fecha y adeudos
                                            $style_edit = '';
                                            if ($Adeudo <= 0) {
                                                $style_edit = 'style="visibility: hidden"';
                                                $classAdeudo = 'adeudo';
                                                $cargo->setPagado(1);
                                                $DaoPagosCiclo->update($cargo);
                                            }else{
                                                $cargo->setPagado(0);
                                                $DaoPagosCiclo->update($cargo);  
                                            }


                                            $NombreUsuDescuento="";
                                            if($cargo->getUsuCapturaDescuento()>0){
                                            $aplicaDescuento=$DaoUsuarios->show($cargo->getUsuCapturaDescuento());
                                            $NombreUsuDescuento=$aplicaDescuento->getNombre_usu()." ".$aplicaDescuento->getApellidoP_usu();
                                            }
                                            
                                            $NombreUsuPaqueteDescuento="";
                                            if($descuentoPaqueteCiclo->getId_usu_captura()>0){
                                            $aplicaPaqueteDescuento=$DaoUsuarios->show($descuentoPaqueteCiclo->getId_usu_captura());
                                            $NombreUsuPaqueteDescuento=$aplicaPaqueteDescuento->getNombre_usu()." ".$aplicaPaqueteDescuento->getApellidoP_usu();
                                            }
                                            
                                            
                                            //Pagoa adelantado, si el ciclo es = al ciclo del pago adelantado o mayor
                                            //Tomar el saldo e irlo drestando de los cargos
                                            ?>
                                            <tr  class="<?php echo $classAdeudo; ?>" id-pago="<?php echo $cargo->getId() ?>">
                                                <td class="td-center"><?php echo $i + 1 ?></td>
                                                <td><?php echo $cargo->getConcepto() ?></td>
                                                <td class="td-center"><?php echo $cargo->getFecha_pago() ?> <i class="fa fa-pencil-square-o" onclick="mostrar_fecha_pago(<?php echo $cargo->getId() ?>,<?php echo $i + 1 ?>)" title="Editar fecha limite de pago"></i></td>
                                                <td class="td-center"><?php echo "$" . number_format($cargo->getMensualidad(), 2) ?></td>
                                                <td class="td-center"><?php echo "$" . number_format($cargo->getDescuento(), 2) ?> 
                                                    <?php
                                                    if (isset($perm['37'])) {
                                                        ?>
                                                    <i class="fa fa-pencil-square-o" onclick="mostrar_descuento(<?php echo $cargo->getId() ?>,<?php echo $i + 1 ?>)" title="Editar descuento"></i>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo $NombreUsuDescuento;?></td>
                                                <td>$<?php echo  number_format($decuentoPorPaquete, 2)?></td>
                                                <td><?php echo $NombreUsuPaqueteDescuento;?></td>
                                                <td class="td-center"><?php echo "$" . number_format($cargo->getDescuento_beca(), 2) ?></td>
                                                <td class="td-center"><?php echo "$" . number_format($MontoPagado, 2) ?></td>
                                                <td class="td-center"><?php echo "$" . number_format($Adeudo, 2) ?></td>
                                                <td class="right">
                                                    <!--<i class="fa fa-history" onclick="mostrar_log_seguimiento(<?php echo $cargo->getId(); ?>)" title="Historial de seguimiento"></i>-->
                                                    <i class="fa fa-plus-square" onclick="show_box_recargo(<?php echo $cargo->getId(); ?>)" title="Añadir recargo"></i>
                                                    <?php
                                                    if (isset($perm['69'])) {
                                                        ?>
                                                        <i class="fa fa-trash" onclick="delete_cargo(<?php echo $cargo->getId(); ?>)" title="Eliminar cargo"></i>
                                                        <?php
                                                    }
                                                    //echo $SaldoPagos." ".$MontoPagado;
                                                    ?>
                                                </td>
                                            </tr>
                                            
                                            
                                            <?php
                                            $query = "SELECT * FROM Recargos_pago WHERE Id_pago=" . $cargo->getId() . " AND Fecha_recargo<='" . date('Y-m-d') . "' ORDER BY Fecha_recargo ASC";
                                            foreach ($DaoRecargos->advanced_query($query) as $recargo) {
                                                ?>
                                                <tr class="recargo">
                                                    <td></td>
                                                    <td>Recargo</td>
                                                    <td class="td-center"><?php echo $recargo->getFecha_recargo() ?> <i class="fa fa-pencil-square-o" onclick="mostrar_fecha_recargo(<?php echo $recargo->getId() ?>)"></i> </td>
                                                    <td class="td-center">$<?php echo number_format($recargo->getMonto_recargo(), 2) ?></td>
                                                    <td colspan="7"></td>
                                                    <td class="right">
                                                        <?php
                                                        //if ($cargo->getCantidad_Pagada() < $Adeudo) {
                                                            if (isset($perm['38'])) {
                                                                ?>
                                                                <i class="fa fa-trash" onclick="delete_recargo(<?php echo $recargo->getId() ?>)" title="Eliminar recargo"></i>
                                                                <?php
                                                            }
                                                        //}
                                                        ?>

                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            $i++;
                                        }
                                        $totalCargosOferta+=$mensualidad;
                                        $totalBecasOferta+=$totalBeca;
                                        $totalDescuentosOferta+=$Descuento;
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3"><b>Total</b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($mensualidad), 2) ?></b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($Descuento), 2) ?></b></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($totalBeca), 2) ?></b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($totalPagado), 2) ?></b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($totalAdeudo), 2) ?></b></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b>Total Miscelaneos</b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($miscelaneos), 2) ?></b></td>
                                            <td colspan="8"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b>Total Regargos</b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($totalrecargos), 2) ?></b></td>
                                            <td class="td-center" colspan="8"></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <?php
                            }
                            ?>
                        </td>
                        <td id="seccion-pagos">
                            <button class="boton-normal" id="mostrar-box-pago" onclick="mostrar_pago()"><i class="fa fa-plus-square"></i> Añadir pago</button>
                            <table class="table" id-ciclo="<?php echo $cicloAlumno->getId_ciclo() ?>">
                                <thead>
                                    <tr>
                                        <td colspan="9" class="clave-ciclo">Historial de pagos</td>
                                    </tr>
                                    <tr>

                                        <td class="td-center">Folio</td>
                                        <td class="td-center">Fecha de captura</td>
                                        <td class="td-center">Fecha de pago</td>
                                        <td>Concepto</td>
                                        <td class="td-center">Monto</td>
                                        <td class="td-center">Método de pago</td>
                                        <td class="td-center">Usuario</td>
                                        <td>Comentarios</td>
                                        <td class="td-center">Acciones</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $totalPagado = 0;
                                    $i = 1;
                                    foreach ($DaoPagos->getPagosOfertaAlumno($oferta_alumno->getId()) as $pago) {
                                        $usuCapturo = $DaoUsuarios->show($pago->getId_usu());
                                        $metodo = $DaoMetodosPago->show($pago->getMetodo_pago());
                                        $totalPagado+=$pago->getMonto_pp();
                                        $ciclo=$DaoCiclos->getCicloPorFecha($pago->getFecha_captura());
                                        ?>
                                        <tr  id-pago="<?php echo $pago->getId() ?>">
                                            <td class="td-center"><?php echo $pago->getFolio() ?></td>
                                            <td class="td-center"><?php echo $pago->getFecha_captura() ?></td>
                                            <td class="td-center"><?php echo $pago->getFecha_pp() ?></td>
                                            <td><?php echo $pago->getConcepto()?></td>
                                            <td class="td-center"><?php echo "$" . number_format($pago->getMonto_pp(), 2) ?></td>
                                            <td class="td-center"><?php echo $metodo->getNombre() ?></td>
                                            <td class="td-center"><?php echo $usuCapturo->getNombre_usu() . " " . $usuCapturo->getApellidoP_usu() ?></td>
                                            <td><?php echo $pago->getComentarios() ?></td>
                                            <td class="menu td-center">
                                                <i class="fa fa-pencil-square-o" onclick="mostrar_pago(<?php echo $pago->getId()?>)" title="Editar pago"></i>
                                                <?php
                                                if (isset($perm['69'])) {
                                                    ?>
                                                    <i class="fa fa-trash" onclick="delete_pago(<?php echo $pago->getId(); ?>)" title="Eliminar pago"></i>
                                                    <?php
                                                }
                                                ?>
                                                <i class="fa fa-file-pdf-o" onclick="mostrar_recibo(<?php echo $pago->getId(); ?>,<?php echo $ciclo->getId(); ?>)" title="Mostrar recibo"></i>
                                                <i class="fa fa-paper-plane" onclick="send_recibo(<?php echo $pago->getId() ?>,<?php echo $ciclo->getId(); ?>, this)"title="Enviar recibo"></i>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td colspan="8" class="clave-ciclo">Totales</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="3"><b>Total cargos</b></td>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($totalCargosOferta), 2) ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Total pagado</b></td>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($totalPagado), 2) ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Total descuento</b></td>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($totalDescuentosOferta), 2) ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Total beca</b></td>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($totalBecasOferta), 2) ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Adeudo</b></td>
                                        <?php
                                        $z = ($totalCargosOferta - $totalPagado - $totalDescuentosOferta - $totalBecasOferta- $totalPaquetesDescuento)
                                        ?>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($z), 2) ?></b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
        <td id="column_two"></td>
    </tr>
</table>
<input type="hidden" id="id_ofe_alumno" value="<?php echo $oferta_alumno->getId() ?>"/>
<input type="hidden" id="id_alumno" value="<?php echo $oferta_alumno->getId_alum() ?>"/>
<?php
write_footer();




