<?php
require_once('Connections/cnn.php');
require_once("estandares/class_bdd.php");
require_once("estandares/class_docentes.php");
require_once("estandares/class_justificaciones.php");
require_once("estandares/class_interesados.php");
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');

if($_POST['action']=="save_calificaciones"){

    foreach($_POST['alumnos'] as $k=>$v){
        $sql_nueva_entrada=sprintf("DELETE FROM Calificaciones WHERE Id_ciclo_mat=%s",
            GetSQLValueString($v['Id_ciclo_mat'], "int"));
        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
        $CalTotalParciales=0;
        foreach($v['Parciales'] as $k2=>$v2){
            $sql_nueva_entrada=sprintf("INSERT INTO Calificaciones (Calificacion,Id_ciclo_mat,Id_eva) VALUES (%s,%s,%s)",
                GetSQLValueString($v2['cal'], "text"),
                GetSQLValueString($v['Id_ciclo_mat'], "int"),
                GetSQLValueString($v2['Id_eva'], "int"));
	    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
            $CalTotalParciales+=($v2['cal']*$v2['ponderacion'])/100;
        }
        $sql_nueva_entrada=sprintf("UPDATE materias_ciclo_ulm SET  CalExtraordinario=%s, CalEspecial=%s, CalTotalParciales=%s WHERE Id_ciclo_mat=%s",
                GetSQLValueString($v['Extraordinario'], "text"),
                GetSQLValueString($v['Especial'], "text"),
                GetSQLValueString($CalTotalParciales, "text"),
                GetSQLValueString($v['Id_ciclo_mat'], "int"));
	    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    }
}



