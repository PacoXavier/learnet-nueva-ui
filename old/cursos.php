<?php
require_once('estandares/includes.php');
require_once('estandares/class_ofertas.php');

links_head("Cursos  "); 
write_head_body();
write_body();


$class_ofertas=new class_ofertas($_REQUEST['id']);
?>
    <table id="tabla">
	    <tr>
		    <td id="column_one">
		            <div class="fondo">
				    	<div id="box_top">
						     <img src="images/icon_ofertas.png" alt="icon_ofertas" /><h1>Cursos</h1>
						</div>
						<div id="mascara_tabla">
							<table id="list_usu">
								<thead>
									<tr>
										<td>Clave</td>
										<td>Nivel de Estudios</td>
										<td>Nombre del Curso</td>
										<td>Duraci&oacute;n / Semanas</td>
										<td>Precio</td>
										<td>Inscripci&oacute;n</td>
										<td>Acciones</td>
									</tr>
								</thead>
								   <tbody>
								   <?php
                                                                    //print_r($class_ofertas->get_cursos_con_orientacion());
								   foreach($class_ofertas->get_cursos_sin_orientacion() as $k=>$v){
								      $ofe=$class_ofertas->get_oferta($v['Id_ofe'])
								   ?>
								     <tr>
									     <td><?php echo $v['ClavePlan_estudios']?></td>
									     <td><?php echo $ofe['Nombre_oferta']?></td>
									     <td><?php echo $v['Nombre_esp']?></td>
									     <td><?php echo $v['DuracionSemanas']?></td>
									     <td><?php echo $v['Precio_curso']?></td>
									     <td><?php echo $v['Inscripcion_curso']?></td>
									     <td>
									         <a href="oferta.php?id=<?php echo $v['Id_ofe']?>">
									            <img src="images/edit_green.png"/>
									         </a>
									         
									         <img src="images/delete_red.png" alt="delete_red" width="15" height="15" />
									     </td>
								     </tr>
								   <?php
								   }
								   ?>
								   </tbody>
						     </table>
						</div>
						<div id="box_button_mostrar"><span id="mostrar" onclick="mostrar_inte()">Mostrar m&aacute;s</span></div>
		            </div>
		    </td>
		    <td id="column_two"></td>
	    </tr>
    </table>
<?php
write_footer();