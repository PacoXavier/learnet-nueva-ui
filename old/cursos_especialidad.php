<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

links_head("Cursos");
write_head_body();
write_body();
$Id_esp = 0;
$DaoCursosEspecialidad= new DaoCursosEspecialidad();
$DaoTurnos= new DaoTurnos();
if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $Id_esp = $_REQUEST['id'];
}
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-list-ul" aria-hidden="true"></i> Cursos</h1>
                </div>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>Clave</td>
                            <td class="td-center">Fecha de alta</td>
                            <td class="td-center">Fecha inicio</td>
                            <td class="td-center">Fecha fin</td>
                            <td class="td-center">Tipo de pago</td>
                            <td class="td-center">Día de pago</td>
                            <td class="td-center">Núm. horas</td>
                            <td class="td-center">Núm. sesiones</td>
                            <td class="td-center">Turno</td>
                            <td class="td-center">Acciones</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $count = 1;
                        foreach ($DaoCursosEspecialidad->getCursosEspecialidad($Id_esp) as $curso) {
                            if ($curso->getTipoPago() == 1) {
                                $tipoPago = "Día de inicio del curso";
                            } elseif ($curso->getTipoPago() == 2) {
                                $tipoPago = "Día especifico";
                            } elseif ($curso->getTipoPago() == 3) {
                                $tipoPago = "Día de inscripción del alumno";
                            }

                            $tur = $DaoTurnos->show($curso->getTurno());
                            $turno=$tur->getNombre();
                            ?>
                            <tr>
                                <td><?php echo $count ?></td>
                                <td><?php echo $curso->getClave() ?></td>
                                <td class="td-center"><?php echo $curso->getDateCreated() ?></td>
                                <td class="td-center"><?php echo $curso->getFechaInicio() ?></td>
                                <td class="td-center"><?php echo $curso->getFechaFin() ?></td>
                                <td class="td-center"><?php echo $tipoPago ?></td>
                                <td class="td-center"><?php echo $curso->getDiaPago() ?></td>
                                <td class="td-center"><?php echo $curso->getNum_horas() ?></td>
                                <td class="td-center"><?php echo $curso->getNum_sesiones() ?></td>
                                <td class="td-center"><?php echo $turno ?></td>
                                <td class="td-center">
                                    <i class="fa fa-pencil-square-o" onclick="mostrarBoxModal(<?php echo $curso->getId() ?>)" title="Editar"></i>
                                    <i class="fa fa-trash" onclick="deleteCurso(<?php echo $curso->getId() ?>)" title="Eliminar"></i>
                                </td>
                            </tr>
                            <?php
                            $count++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><span onclick="mostrarBoxModal()">Nuevo curso</span></li>
                    <li><span><a href="curso.php?id=<?php echo $Id_esp; ?>">Regresar</a></span></li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="id_esp" value="<?php echo $Id_esp ?>"/>
<?php
write_footer();
