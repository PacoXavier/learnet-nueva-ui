<?php
require_once('estandares/includes.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoMateriasEspecialidad.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoPrerrequisitos.php');
require_once('clases/DaoEvaluaciones.php');

if (!isset($perm['43'])) {
    header('Location: home.php');
}

links_head("Materia  ");
write_head_body();
write_body();

$DaoMaterias = new DaoMaterias();
$DaoOfertas = new DaoOfertas();
$DaoMateriasEspecialidad = new DaoMateriasEspecialidad();
$DaoEspecialidades = new DaoEspecialidades();
$DaoGrados = new DaoGrados();
$DaoPrerrequisitos = new DaoPrerrequisitos();
$DaoEvaluaciones = new DaoEvaluaciones();
?>
<table id="tabla">
    <?php
    $text = "Nueva";
    $id_mat = 0;
    $id_esp=0;
    $Nombre = "";
    $Clave_mat = "";
    $Precio = "";
    $Creditos = "";
    $Dias_porSemana = "";
    $Horas_porSemana = "";
    $Horas_conDocente = "";
    $Horas_independientes = "";
    $Tipo_aula = "";
    $Promedio_min = "";
    $Duracion_examen = "";
    $Max_alumExamen = "";

    if (isset($_REQUEST['id_mat']) && $_REQUEST['id_mat'] > 0) {
        $text = "Editar";
        $mat = $DaoMaterias->show($_REQUEST['id_mat']);
        $Nombre = $mat->getNombre();
        $Clave_mat = $mat->getClave_mat();
        $Precio = $mat->getPrecio();
        $Creditos = $mat->getCreditos();
        $Dias_porSemana = $mat->getDias_porSemana();
        $Horas_porSemana = $mat->getHoras_porSemana();
        $Horas_conDocente = $mat->getHoras_conDocente();
        $Horas_independientes = $mat->getHoras_independientes();
        $Tipo_aula = $mat->getTipo_aula();
        $Promedio_min = $mat->getPromedio_min();
        $Duracion_examen = $mat->getDuracion_examen();
        $Max_alumExamen = $mat->getMax_alumExamen();
        $id_mat = $_REQUEST['id_mat'];
    }
    if (isset($_REQUEST['id_esp']) && $_REQUEST['id_esp'] > 0) {
        $id_esp=$_REQUEST['id_esp'];
    }
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div class="box_top">
                    <h1><i class="fa fa-cog" aria-hidden="true"></i> <?php echo $text ?> materia</h1>
                </div>
                <div class="seccion">
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Nombre de la Materia<br><input type="text" value="<?php echo $Nombre ?>" id="Nombre"/></li>
                        <li>Clave de la materia<br><input type="text" value="<?php echo $Clave_mat ?>" id="Clave_mat"/></li>
                        <li>Precio de la Materia<br><input type="text" value="<?php echo $Precio ?>" id="Precio"/></li>
                        <li>Cr&eacute;ditos<br><input type="text" value="<?php echo $Creditos ?>" id="Creditos"/></li>
                        <li>Duraci&oacute;n (D&iacute;as por Semana)<br><input type="text" value="<?php echo $Dias_porSemana ?>" id="Dias_porSemana"/></li>
                        <li>Sesiones Por Semana<br><input type="text" id="Horas_porSemana" value="<?php echo $Horas_porSemana ?>"/></li>
                        <li>Sesiones con Docente<br><input type="text" id="Horas_conDocente" value="<?php echo $Horas_conDocente ?>"/></li>
                        <li>Sesiones Independientes<br><input type="text" id="Horas_independientes" value="<?php echo $Horas_independientes ?>"/></li>
                        <li>Tipo de Aula<br><select id="Tipo_aula">
                                <option value="0"></option>
                                <option value="1" <?php if ($Tipo_aula == "1") { ?> selected="selected"<?php } ?>>Laboratorio</option>
                                <option value="2" <?php if ($Tipo_aula == "2") { ?> selected="selected"<?php } ?>>Instrucci&oacute;n</option>
                                <option value="3" <?php if ($Tipo_aula == "3") { ?> selected="selected"<?php } ?>>Especial</option>
                                <option value="4" <?php if ($Tipo_aula == "4") { ?> selected="selected"<?php } ?>>Te&oacute;rica</option>
                            </select>
                        </li>
                        <li>Promedio m&iacute;nimo<br><input type="text" id="Promedio_min" value="<?php echo $Promedio_min ?>"/></li>
                        <li>Duracion del Examen (Minutos)<br><input type="text" id="Duracion_examen" value="<?php echo $Duracion_examen ?>"/></li>
                        <li> M&aacute;ximo de Alumnos en Examen<br><input type="text" id="Max_alumExamen" value="<?php echo $Max_alumExamen ?>"/></li>
                    </ul>
                </div>
                <?php
                if ($id_mat > 0) {
                    ?>
                    <div class="seccion">
                        <h2>Especialidades asociadas</h2>
                        <span class="linea"></span>
                    </div>
                    <div id="box_ofertas_compartidas">
                        <?php
                        foreach ($DaoMateriasEspecialidad->getMateriasEspecialidadByIdMat($id_mat) as $matEsp) {
                            $esp = $DaoEspecialidades->show($matEsp->getId_esp());
                            ?>
                            <div class="oferta">
                                <div class="seccion">
                                    <table class="tabla-select">
                                        <thead>
                                            <tr>
                                                <td>Oferta</td>
                                                <td>Especialidad</td>
                                                <td>Grado ( para que cuatrimestre)</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <?php
                                                    foreach ($DaoOfertas->showAll() as $oferta) {
                                                        if ($oferta->getId() == $esp->getId_ofe()) {
                                                            echo $oferta->getNombre_oferta();
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    foreach ($DaoEspecialidades->getEspecialidadesOferta($esp->getId_ofe()) as $espe) {
                                                        if ($espe->getId() == $matEsp->getId_esp()) {
                                                            echo $espe->getNombre_esp();
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    foreach ($DaoGrados->getGradosEspecialidad($esp->getId()) as $gr) {
                                                        if ($gr->getId() == $matEsp->getGrado_mat()) {
                                                            echo $gr->getGrado();
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="tabla-checkbox">
                                        <thead>
                                            <tr>
                                                <td>Evaluacion Extraordinaria<br>(Equivale a Calificación final)</td>
                                                <td>Evaluacion Especial<br>(Equivale a Calificación final)<br></td>
                                                <td>LLeva orientaci&oacute;n</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?php if ($matEsp->getEvaluacion_extraordinaria() == 1) { ?> SI <?php } else { ?> NO <?php } ?></td>
                                                <td><?php if ($matEsp->getEvaluacion_especial() == 1) { ?> SI <?php } else { ?> NO <?php } ?></td>
                                                <td><?php if ($matEsp->getTiene_orientacion() == 1) { ?> SI <?php } else { ?> NO <?php } ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="tabla-nombre">
                                        <thead>
                                            <tr>
                                                <td colspan="3">Nombre especial</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?php echo $matEsp->getNombreDiferente() ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="box">
                                        <table class="list_evaluaciones">
                                            <thead>
                                                <tr>
                                                    <td>Nombre</td>
                                                    <td>Ponderaci&oacute;n</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($DaoEvaluaciones->getEvaluacionesMat($matEsp->getId()) as $eva) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $eva->getNombre_eva() ?></td>
                                                        <td><?php echo $eva->getPonderacion() ?>%</td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="box">
                                        <div class="seccion">
                                            <table class="list_usu">
                                                <thead>
                                                    <tr>
                                                        <td>Materia</td>
                                                        <td>Clave</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($DaoPrerrequisitos->getPrerrequisitoByIdMatEsp($matEsp->getId()) as $prerrequisito) {
                                                        $mat_esp = $DaoMateriasEspecialidad->show($prerrequisito->getId_mat_esp_pre());
                                                        $mat = $DaoMaterias->show($mat_esp->getId_mat());
                                                        ?>
                                                        <tr>
                                                            <td style="width:200px;"><?php echo $mat->getNombre() ?></td>
                                                            <td><?php echo $mat->getClave_mat() ?></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>  
                                    </div>
                                </div>
                                <button class="auxiliares" onclick="delete_ofe_compartida(<?php echo $matEsp->getId() ?>)">Eliminar</button>
                                <button class="auxiliares" onclick="mostrar_ofe_compartida(<?php echo $matEsp->getId() ?>)">Editar</button>
                                <a href="plan_estudio.php?id=<?php echo $matEsp->getId_esp() ?>&id_mat=<?php echo $id_mat; ?>" target="_blank"><button class="auxiliares">Plan de estudios</button></a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
                <button id="button_ins" onclick="save_materia()">Guardar</button>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="materia.php?id_esp=<?php echo $id_esp;?>" class="link">Nueva Materia</a></li>
                    <?php
                    if ($id_mat > 0) {
                        ?>
                        <li><span onclick="delete_materia(<?php echo $id_mat; ?>)">Eliminar Materia</span></li>
                        <li><span onclick="mostrar_ofe_compartida()">Añadir especialidad asociada</span></li>
                        <?php
                    }
                    ?>
                    <li><a href="curso.php?id=<?php echo $id_esp;?>" class="link">Regresar</a></li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_mat" value="<?php echo $id_mat ?>"/>
<input type="hidden" id="Id_esp" value="<?php echo $id_esp ?>"/>
<?php
write_footer();

