<?php
require_once('Connections/cnn.php');
require_once('estandares/class_bdd.php');
require_once('estandares/class_usuarios.php');
require_once('estandares/class_imagenes.php');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}


mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');

if ($_GET['action'] == "upload") {
    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
    $usu = $class_usuarios->get_usu();
    if (count($_FILES) > 0) {
        if ($_FILES["file"]["error"] > 0) {
            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {

            $class_bdd = new class_bdd();
            $key = $class_bdd->generarKey();
            $sql_nueva_entrada = sprintf("INSERT INTO Imagenes  (Llave, Nombre_ori, Id_plantel, Id_usu, DateCreated) Values (%s,%s,%s,%s,%s)", 
                    GetSQLValueString($key, "text"), 
                    GetSQLValueString($_FILES["file"]["name"], "text"), 
                    GetSQLValueString($usu['Id_plantel'], "int"), 
                    GetSQLValueString($usu['Id_usu'], "int"), 
                    GetSQLValueString(date('Y-m-d H:i:s') , "date"));
            $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

            move_uploaded_file($_FILES["file"]["tmp_name"], "imagenes/" . $key . ".jpg");
        }

    } elseif (isset($_GET['action'])) {

        if ($_FILES["file"]["error"] > 0) {

            echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        } else {

            $class_bdd = new class_bdd();
            $key = $class_bdd->generarKey();
            $sql_nueva_entrada = sprintf("INSERT INTO Imagenes  (Llave, Nombre_ori, Id_plantel, Id_usu, DateCreated) Values (%s,%s,%s,%s,%s)", 
                    GetSQLValueString($key, "text"), 
                    GetSQLValueString($_GET["FileName"], "text"), 
                    GetSQLValueString($usu['Id_plantel'], "int"), 
                    GetSQLValueString($usu['Id_usu'], "int"), 
                    GetSQLValueString(date('Y-m-d H:i:s') , "date"));
            $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());


            if (isset($_GET['base64'])) {
                // If the browser does not support sendAsBinary ()
                $content = base64_decode(file_get_contents('php://input'));
            } else {
                $content = file_get_contents('php://input');
            }
            file_put_contents('imagenes/' . $key . '.jpg', $content);
        }

    }
    update_list_img();
}

function update_list_img(){ 
    $class_imagenes= new class_imagenes();
    foreach($class_imagenes->get_imagenes()as $k=>$v){
        $ruta="imagenes/".$v['Llave'].".jpg";
    ?>
   <li onclick="delete_img(<?php echo $v['Id']?>)"style="background-image: url(<?php echo $ruta;?>)"></li>
    <?php
    }                 
}

  
    
if ($_POST['action'] == "delete_img") {
    $class_imagenes= new class_imagenes($_POST['Id']);
    $img=$class_imagenes->get_img();
    unlink('imagenes/'.$img['Llave'].".jpg");
    $sql_nueva_entrada = sprintf("DELETE FROM Imagenes  WHERE Id=%s", 
            GetSQLValueString($_POST['Id'] , "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    update_list_img();
}
