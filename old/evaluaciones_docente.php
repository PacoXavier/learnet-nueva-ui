<?php
require_once('estandares/includes.php');
require_once('require_daos.php');
if (!isset($perm['49'])) {
    header('Location: home.php');
}
$DaoCategoriasPago= new DaoCategoriasPago();
$DaoDocentes= new DaoDocentes();
$DaoCiclos= new DaoCiclos();
$DaoEvaluacionDocente= new DaoEvaluacionDocente();
$DaoPuntosPorCategoriaEvaluadaDocente= new DaoPuntosPorCategoriaEvaluadaDocente();
$DaoNivelesPago= new DaoNivelesPago();
$Id_docente=0;
links_head("Evaluaci&oacute;n Docente  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-tasks"></i> Evaluaci&oacute;n Docente</h1>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <li>Buscar Docente<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o clave"/>
                            <ul id="buscador_int"></ul>
                        </li>
                    </ul>
                </div>
                <?php
                if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                    $docente = $DaoDocentes->show($_REQUEST['id']);
                    $Id_docente=$docente->getId();
                    $nombre=$docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . " " . $docente->getApellidoM_docen();
                    ?>
                    <div class="seccion">
                        <h2>Historial de evaluaciones</h2>
                        <h2 style="font-size: 15px;margin-bottom: 15px;"><?php echo $docente->getClave_docen() . "  - " . $docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . " " . $docente->getApellidoM_docen() ?></h2>
                        <span class="linea"></span>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Ciclo</td>
                                    <td>Fecha</td>
                                    <td>Nombre</td>
                                    <td>Puntos</td>
                                    <?php
                                    foreach ($DaoCategoriasPago->getCategoriasPago() as $cat) {
                                       ?>
                                         <td><?php echo $cat->getNombre_tipo();?></td>
                                       <?php
                                    }
                                    ?>
                                    <td>Acciones</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                foreach ($DaoEvaluacionDocente->getEvaluacionesDocentes($docente->getId()) as $eva) {
                                    $ciclo = $DaoCiclos->show($eva->getId_ciclo());
                                    ?>
                                    <tr>
                                        <td><?php echo $count ?></td>
                                        <td><?php echo $ciclo->getClave() ?></td>
                                        <td><?php echo $eva->getDateCreated(); ?></td>
                                        <td><?php echo $nombre ?></td>
                                        <td><?php echo $eva->getPuntosTotales(); ?></td>
                                        <?php
                                        foreach ($DaoCategoriasPago->getCategoriasPago() as $cat) {
                                            $resul=$DaoPuntosPorCategoriaEvaluadaDocente->getNivelPorCategoria($eva->getId(),$cat->getId());
                                            $nombreNIvel="";
                                            if($resul->getId()>0){
                                               $nivel=$DaoNivelesPago->show($resul->getId_nivel());
                                               $nombreNIvel=$nivel->getNombre_nivel();
                                            }
                                           ?>
                                             <td><?php echo $nombreNIvel;?></td>
                                           <?php
                                        }
                                        ?>
                                        <td><button onclick="editar_evaluacion(<?php echo $eva->getId(); ?>)">Editar</button><button onclick="delete_evaluacion(<?php echo $eva->getId() ?>)">Eliminar</button></td>
                                    </tr>
                                    <?php
                                    $count++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <table id="usu_login">
                    <?php
                    require_once 'estandares/menu_derecho.php';
                    ?>
                    <ul>
                        <?php
                        if ($Id_docente > 0) {
                            ?>
                            <li><a href="evaluacion_docente.php?id_doc=<?php echo $Id_docente; ?>">Nueva evaluaci&oacute;n </a></li>
                            <?php
                        }
                        ?>
                    </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_docente" value="<?php echo $Id_docente; ?>"/>
<?php
write_footer();
?>
