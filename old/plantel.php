<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

$DaoPlanteles = new DaoPlanteles();
$DaoDirecciones = new DaoDirecciones();
$DaoOfertas = new DaoOfertas();
$DaoOfertasPlantel = new DaoOfertasPlantel();
$DaoDatosBancarios = new DaoDatosBancarios();
$DaoDiasPlantel= new DaoDiasPlantel();


links_head("Plantel  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <?php
    $Nombre_plantel = "";
    $Clave = "";
    $Tel = "";
    $getCalle_dir = "";
    $getNumExt_dir = "";
    $getNumInt_dir = "";
    $getColonia_dir = "";
    $getCiudad_dir = "";
    $getEstado_dir = "";
    $getCp_dir = "";
    $Id_dir = 0;
    $abreviatura = "";
    $email = "";
    $dominio = "";
    $logo = "";
    $formato_pago = "";
    $formato_pago_evento = "";
    $etiquetaLibro="";
    $etiquetaActivo="";
    $formato_inscripcion="";

    $Banco = "";
    $Beneficiario = "";
    $Convenio = "";
    $Transferencias = "";
    $RFC = "";
    $Id_dato_banc = "";
    $Id_plantel = 0;
    
    $L=0;
    $M=0;
    $I=0;
    $J=0;
    $V=0;
    $S=0;
    $D=0;
    if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {

        $plantel = $DaoPlanteles->show($_REQUEST['id']);
        $Nombre_plantel = $plantel->getNombre_plantel();
        $Clave = $plantel->getClave();
        $Tel = $plantel->getTel();
        $abreviatura = $plantel->getAbreviatura();
        $email = $plantel->getEmail();
        $dominio = $plantel->getDominio();
        $logo = $plantel->getId_img_logo();
        $formato_pago = $plantel->getFormato_pago();
        $formato_pago_evento = $plantel->getFormato_pago_evento();
        $etiquetaLibro=$plantel->getEtiquetaLibro();
        $etiquetaActivo=$plantel->getEtiquetaActivo();
        $formato_inscripcion=$plantel->getFormato_inscripcion();
    
        $Id_plantel = $_REQUEST['id'];
        if ($plantel->getId_dir_plantel() > 0) {
            $dir = $DaoDirecciones->show($plantel->getId_dir_plantel());
            $getCalle_dir = $dir->getCalle_dir();
            $getNumExt_dir = $dir->getNumExt_dir();
            $getNumInt_dir = $dir->getNumInt_dir();
            $getColonia_dir = $dir->getColonia_dir();
            $getCiudad_dir = $dir->getCiudad_dir();
            $getEstado_dir = $dir->getEstado_dir();
            $getCp_dir = $dir->getCp_dir();
            $Id_dir = $plantel->getId_dir_plantel();
        }

        $datosB = $DaoDatosBancarios->getDatosBancariosPlantel($_REQUEST['id']);
        foreach ($datosB as $dato) {
            $Banco = $dato->getBanco();
            $Beneficiario = $dato->getBeneficiario();
            $Convenio = $dato->getConvenio();
            $Transferencias = $dato->getTrasferencias();
            $RFC = $dato->getRFC();
            $Id_dato_banc = $dato->getId();
        }
        
        foreach($DaoDiasPlantel->getDiasPlantel($Id_plantel) as $dia){
            if($dia->getId_dia()==1){
               $L=1;
            }
            if($dia->getId_dia()==2){
               $M=1;
            }
            if($dia->getId_dia()==3){
               $I=1;
            }
            if($dia->getId_dia()==4){
               $J=1;
            }
            if($dia->getId_dia()==5){
               $V=1;
            }
            if($dia->getId_dia()==6){
               $S=1;
            }
            if($dia->getId_dia()==7){
               $D=1;
            }
        }
    }
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-building"></i> Plantel <?php echo $Nombre_plantel; ?></h1>
                </div>
                <div class="seccion">
                    <h2>Datos del plantel</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Clave<br><input type="text" value="<?php echo $Clave ?>" id="clave"/></li>
                        <li>Nombre<br><input type="text" value="<?php echo $Nombre_plantel ?>" id="nombre"/></li>
                        <li>Abreviatura<br><input type="text" value="<?php echo $abreviatura ?>" id="abreviatura"/></li>
                        <li>Tel&eacute;fono<br><input type="tel" id="tel" value="<?php echo $Tel ?>"/></li>
                        <li>Email<br><input type="text" value="<?php echo $email ?>" id="email"/></li>
                        <li>Dominio<br><input type="text" value="<?php echo $dominio ?>" id="dominio"/></li>
                    </ul>
                </div>
                <div class="seccion">
                    <h2>Direcci&oacute;n</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Calle<br><input type="text" id="calle" value="<?php echo $getCalle_dir ?>"/></li>
                        <li>N&uacute;mero exterior<br><input type="text" id="numExt" value="<?php echo $getNumExt_dir ?>"/></li>
                        <li>N&uacute;mero interior<br><input type="text" id="numInt" value="<?php echo $getNumInt_dir ?>"/></li>
                        <li>Colonia<br><input type="text" id="colonia" value="<?php echo $getColonia_dir ?>"/></li>
                        <li>C&oacute;digo postal<br><input type="text" id="cp" value="<?php echo $getCp_dir ?>"/></li>
                        <li>Ciudad<br><input type="text" id="ciudad" value="<?php echo $getCiudad_dir ?>"/></li>
                        <li>Estado<br><input type="text" id="estado" value="<?php echo $getEstado_dir ?>"/></li>
                    </ul>
                </div>
                <div class="seccion datosBancarios">
                    <h2>Datos bancarios</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Banco<br><input type="text" id="Banco" value="<?php echo $Banco ?>"/></li>
                        <li>Beneficiario<br><input type="text" id="Beneficiario" value="<?php echo $Beneficiario ?>"/></li>
                        <li>Convenio<br><input type="text" id="Convenio" value="<?php echo $Convenio ?>"/></li>
                        <li>Transferencias<br><input type="text" id="Transferencias" value="<?php echo $Transferencias ?>"/></li>
                        <li>RFC<br><input type="text" id="RFC" value="<?php echo $RFC ?>"/></li>
                    </ul>
                </div>
                <div class="seccion datosBancarios">
                    <h2>Días laborales</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Lunes<br><input type="checkbox" id="lunes" class="dias" value="1" <?php  if($L==1){?> checked="checked" <?php }?>/></li>
                        <li>Martes<br><input type="checkbox" id="martes" class="dias" value="2" <?php  if($M==1){?> checked="checked" <?php }?>//></li>
                        <li>Miércoles<br><input type="checkbox" id="miercoles" class="dias" value="3" <?php  if($I==1){?> checked="checked" <?php }?>//></li>
                        <li>Jueves<br><input type="checkbox" id="jueves" class="dias" value="4" <?php  if($J==1){?> checked="checked" <?php }?>//></li>
                        <li>Viernes<br><input type="checkbox" id="viernes" class="dias" value="5" <?php  if($V==1){?> checked="checked" <?php }?>//> </li>
                        <li>Sábado<br><input type="checkbox" id="sabado" class="dias" value="6" <?php  if($S==1){?> checked="checked" <?php }?>//></li>
                        <li>Domingo<br><input type="checkbox" id="domingo" class="dias" value="7" <?php  if($D==1){?> checked="checked" <?php }?>//></li>
                    </ul>
                </div>
                <div class="seccion box-list">
                    <h2>Formato de pago</h2>
                    <iframe <?php if (strlen($formato_pago) > 0) { ?> src="files/<?php echo $formato_pago ?>.pdf <?php } ?>" style="width: 250px;height: 209px;" id="formato_pago">
                        <p>Your browser does not support iframes.</p>
                    </iframe>
                </div>
                <div class="seccion box-list">
                    <h2>Formato de eventos</h2>
                    <iframe <?php if (strlen($formato_pago_evento) > 0) { ?> src="files/<?php echo $formato_pago_evento ?>.pdf <?php } ?>" style="width: 250px;height: 209px;" id="formato_pago_evento">
                        <p>Your browser does not support iframes.</p>
                    </iframe>
                </div>
                <div class="seccion box-list">
                    <h2>Formato de inscripción</h2>
                    <iframe <?php if (strlen($formato_inscripcion) > 0) { ?> src="files/<?php echo $formato_inscripcion ?>.pdf <?php } ?>" style="width: 250px;height: 209px;" id="formato_inscripcion">
                        <p>Your browser does not support iframes.</p>
                    </iframe>
                </div>
                <div class="seccion box-list">
                    <h2>Logo</h2>
                    <div id="box-logo" <?php if (strlen($logo) > 0) { ?> style="background-image:url(files/<?php echo $logo ?>.jpg) <?php } ?>"></div>
                </div>
                <div class="seccion box-list">
                    <h2>Etiqueta libro</h2>
                    <div id="box-eti-libro" <?php if (strlen($etiquetaLibro) > 0) { ?> style="background-image:url(files/<?php echo $etiquetaLibro ?>.jpg) <?php } ?>"></div>
                </div>
                <div class="seccion box-list">
                    <h2>Etiqueta activo</h2>
                    <div id="box-eti-activo" <?php if (strlen($etiquetaActivo) > 0) { ?> style="background-image:url(files/<?php echo $etiquetaActivo ?>.jpg) <?php } ?>"></div>
                </div>
                <button id="button_ins" onclick="save_plantel()">Guardar</button>
            </div>
        </td>  
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="plantel.php" class="link">Nuevo</a></li>
                    <?php
                    if ($Id_plantel > 0) {
                        ?>
                        <li><span onclick="mostrarFinder('logo')"><i class="fa fa-file-image-o"></i> A&ntilde;adir logo</span></li>
                        <li><span onclick="mostrarFinder('pdf')"><i class="fa fa-file-pdf-o"></i> A&ntilde;adir formato de pago</span></li>
                        <li><span onclick="mostrarFinder('formato_inscripcion')"><i class="fa fa-file-pdf-o"></i> A&ntilde;adir formato de inscripción</span></li>
                        <li><span onclick="mostrarFinder('pdf_eventos')"><i class="fa fa-file-pdf-o"></i> A&ntilde;adir formato de eventos</span></li>
                        <li><span onclick="mostrarFinder('etiqueta_libro')"><i class="fa fa-file-image-o"></i> A&ntilde;adir etiqueta libro</span></li>
                        <li><span onclick="mostrarFinder('etiqueta_activo')"><i class="fa fa-file-image-o"></i> A&ntilde;adir etiqueta activo</span></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_plantel" value="<?php echo $Id_plantel ?>"/>
<input type="hidden" id="Id_dir" value="<?php echo $Id_dir ?>"/>
<input type="hidden" id="Id_dato_banc" value="<?php echo $Id_dato_banc ?>"/>
<input type="file" id="files" name="files" multiple="">

<?php
write_footer();
