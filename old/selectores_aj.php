<?php
require_once('Connections/cnn.php');
require_once('estandares/class_bdd.php');
require_once('estandares/class_imagenes.php');

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');

if($_POST['action']=="mostrar_box_selector_img"){
?>
<div id="box-selector">
    <h1>Selecciona una im&aacute;gen</h1>
    <ul class="list-img">
        <?php
        $class_imagenes= new class_imagenes();
        foreach($class_imagenes->get_imagenes()as $k=>$v){
            $ruta="imagenes/".$v['Llave'].".jpg";
        ?>
           <li onclick="insert_img_in_text(<?php echo $v['Id']?>)" style="background-image: url(<?php echo $ruta;?>)"></li>
        <?php
        }   
        ?>
    </ul>

    <span onclick="ocultarSelector()" class="cancelar">Cancelar</span>
</div>
<?php	
}

if($_POST['action']=="insert_img_in_text"){
    $class_imagenes= new class_imagenes($_POST['Id']);
    $img=$class_imagenes->get_img(); 
    echo $img['Llave'];
}

