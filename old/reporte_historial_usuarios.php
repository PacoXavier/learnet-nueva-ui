<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoCiclosAlumno.php');
require_once('clases/DaoRecargos.php');
require_once('clases/DaoHistorialUsuario.php');
require_once('clases/DaoCiclos.php');

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoCiclosAlumno= new DaoCiclosAlumno();
$DaoHistorialUsuario= new DaoHistorialUsuario();
$DaoCiclos=new DaoCiclos();

links_head("Historial de usuarios  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-history"></i> Historial de usuarios </h1>
                </div>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Filtros</li>
                        <li onclick="download_excel()"><i class="fa fa-download"></i> Descargar</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            
                            <tr>
                                <td>#</td>
                                <td>Ciclo</td>
                                <td>Usuario</td>
                                <td>Categoría</td>
                                <td style="width: 150px">Fecha</td>
                                <td>Movimiento realizado</td>
                            </tr>
                        </thead>
                        <tbody>
                       
                            <?php
                             $count=1;
                             foreach ($DaoHistorialUsuario->showAll() as $k=>$v){
                                $_usu=$DaoUsuarios->show($v->getId_usu());
                                $ciclo=$DaoCiclos->show($v->getId_ciclo());
                               ?>
                                 <tr>
                                         <td><?php echo $count; ?></td>
                                          <td><?php echo $ciclo->getClave()?></td>
                                         <td><a href="usuario.php?id=<?php echo $_usu->getId(); ?>"> <?php echo $_usu->getNombre_usu() . " " . $_usu->getApellidoP_usu() . " " . $_usu->getApellidoM_usu(); ?></a></td>
                                         <td><?php echo $v->getCategoria() ?></a></td>
                                         <td><?php echo $v->getDateCreated(); ?></td>
                                         <td><?php echo $v->getTexto(); ?></td>
                                 </tr>
                              <?php
                                 $count++;
                             }
                             ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
        <div class="boxUlBuscador">
            <p>Usuario<br><input type="search"  class="buscarFiltro" onkeyup="buscarUsu()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p>Categoria<br>
          <select id="categoria">
              <option></option>
              <?php
              $query="SELECT * FROM HistorialUsuario GROUP BY Categoria";
              foreach($DaoHistorialUsuario->advanced_query($query) as $k=>$v){
              ?>
                 <option value="<?php echo $v->getCategoria() ?>"><?php echo $v->getCategoria(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p>Ciclo<br>
          <select id="ciclo">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p>Fecha inicio<br><input type="date"  id="fecha_ini"/></p>
        <p>Fecha fin<br><input type="date"  id="fecha_fin"/></p>  
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>

<?php
write_footer();



