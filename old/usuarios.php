<?php
require_once('estandares/includes.php');
require_once('require_daos.php');
if(!isset($perm['45'])){
  header('Location: home.php');
}
$base= new base();
$DaoUsuarios= new DaoUsuarios();
$DaoPlanteles= new DaoPlanteles();
$DaoTiposUsuarios= new DaoTiposUsuarios();

links_head("Usuarios  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-users" aria-hidden="true"></i> Usuarios</h1>
                </div>
               <ul class="form">
                    <li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>
                </ul>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>Nombre</td>
                                <td>Email</td>
                                <td>Plantel</td>
                                <td>&Uacute;ltima sesi&oacute;n</td>
                                <td>Tipo de usuario</td>
                                <td>Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($DaoUsuarios->showAll() as $u) {
                                $plantel=$DaoPlanteles->show($u->getId_plantel());
                                $tipoUsu=$DaoTiposUsuarios->show($u->getTipo_usu());
                                ?>
                                <tr>
                                    <td onclick="mostrar(<?php echo $u->getId() ?>)"><?php echo $u->getNombre_usu() . " " . $u->getApellidoP_usu() . " " . $u->getApellidoM_usu() ?></td>
                                    <td><?php echo $u->getEmail_usu() ?></td>
                                    <td><?php echo $plantel->getNombre_plantel() ?></td>
                                    <td><?php
                                        if (strlen($u->getLast_session() > 0)) {
                                            echo $base->formatFecha_hora($u->getLast_session());
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo $tipoUsu->getNombre_tipo() ?></td>
                                    <td>
                                        <button onclick="delete_usu(<?php echo $u->getId() ?>)">Eliminar</button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div id="box_button_mostrar"><span id="mostrar" onclick="mostrar_inte()">Mostrar m&aacute;s</span></div>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="usuario.php" class="link">Nuevo</a></li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<?php
write_footer();


