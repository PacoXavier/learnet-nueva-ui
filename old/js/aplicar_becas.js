var rutaAjax = "ajax/aplicar_becas_aj.php"

function buscar_int(){
 if($.trim($('#email_int').val())){
       var params= new Object()
           params.action="buscar_int"
           params.buscar=$('#email_int').val()
      var funcionExito =function(resp){
          $('#buscador_int').html(resp) 
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      $('#buscador_int').html('')
  }
}

function box_beca(Id_ofe_alum){
    var params= new Object();	
        params.action="box_beca"
        params.Id_ofe_alum=Id_ofe_alum
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}

function updatePorcentajeBeca(){
    var params= new Object();	
        params.action="updatePorcentajeBeca"
        params.Id_beca=$('#tipo_beca option:selected').val()
    var funcionExito= function(resp){
         $('#porcentaje').val(resp)
         
    }
    $.post(rutaAjax,params,funcionExito);
}


var porcentaje=0
function getCalificacionMin(Id_beca){
    var params= new Object();	
        params.action="getCalificacionMin"
        params.Id_beca=Id_beca
    var funcionExito= function(resp){
         porcentaje=resp
    }
    $.post(rutaAjax,params,funcionExito);
}


function validarExistenciaMensualidades(Id){
    var params= new Object();	
        params.action="validarExistenciaMensualidades"
        params.Id_alumn=Id
        params.Id_ciclo_alum=$('#ciclo_alum option:selected').val()
        params.Id_ofe_alum=$('#OfertaAlumno option:selected').val()
   if($('#tipo_beca option:selected').val()>0 && $('#ciclo_alum option:selected').val()>0){
            var funcionExito= function(resp){
                if(resp>0){
                    aplicar_beca(Id)
                }else{
                    alert("Para aplicar la beca, primero genera las mensualidades")
                }  
            }
            $.post(rutaAjax,params,funcionExito);   
    }else{
        alert("Completa los campos faltantes")
    }
    
}
function aplicar_beca(Id_alumn){
    var params= new Object();	
        params.action="aplicar_beca"
        params.Id_alum=Id_alumn
        params.Tipo_beca=$('#tipo_beca option:selected').val()
        params.Id_ciclo_alum=$('#ciclo_alum option:selected').val()
        params.Id_ofe_alum=$('#OfertaAlumno option:selected').val()
        params.porcentaje= $('#porcentaje').val()
        getCalificacionMin(params.Tipo_beca)
    if($('#tipo_beca option:selected').val()>0 && $('#ciclo_alum option:selected').val()>0){
        var funcionExito= function(resp){
                  if(resp.indexOf('<td id="column_one">')>0){
                     $('#tabla').html(resp) 
                      ocultar_error_layer() 
                  }else{
                      if(confirm("El alumno cuenta con un promedio de "+resp+", para aplicar la beca se requiere un mínimo de "+porcentaje+" en su calificacion")){
                          aplicar_beca(Id_alumn)
                      }else{
                        ocultar_error_layer()   
                      }
                  }
                  
        }
        $.post(rutaAjax,params,funcionExito);
     }else{
         alert("Completa los campos faltantes")
     }
}

function mostrar_botones(Obj){
   if($(Obj).next().is(":hidden")) {
	   $(Obj).next().slideDown("fast");
           $(Obj).html('Ocultar')
   } else {
	   $(Obj).next().slideUp("fast");
           $(Obj).html('Mostrar')
   }
}

function getCiclosBeca(){
   var params= new Object()
       params.action="getCiclosBeca"
       params.Id_ofe_alum=$('#OfertaAlumno option:selected').val()
       params.Id_alum=$('#Id_alum').val()
   if($('#OfertaAlumno option:selected').val()>0){
       var funcionExito= function(resp){
           $('#tabla').html(resp) 
       }
       $.post(rutaAjax,params,funcionExito);	
   }
}


function deleteBeca(Id_ciclo_alum){
    if(confirm("¿Está seguro de eliminar la beca del alumno para este ciclo?")){
   var params= new Object()
       params.action="deleteBeca"
       params.Id_ciclo_alum=Id_ciclo_alum
       var funcionExito= function(resp){
           $('#tabla').html(resp) 
       }
       $.post(rutaAjax,params,funcionExito);
   }

}


