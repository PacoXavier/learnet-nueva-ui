var rutaAjax = "ajax/reporte_horario_docente_aj.php"

$(window).ready(function(){
    $(".boxfil").draggable();
})

function filtro(Obj){
   $(Obj).text('Buscando...')
  var params= new Object()
       params.action="mostrar_horario"
       params.Id_docente=$('#docente option:selected').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
  if($('#docente option:selected').val()>0 && $('#ciclo option:selected').val()>0){
      var funcionExito =function(resp){
          $(Obj).text('Buscar')
          $('.table tbody').html(resp) 
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      alert("Selecciona los campos requeridos")
  }
}

function mostrarCalificacionesGrupo(id_grupo){
    window.open('reporte_capturar_calificaciones.php?id='+id_grupo, '_blank');
}

