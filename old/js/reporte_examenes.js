var rutaAjax = "ajax/reporte_examenes_aj.php"
$(window).ready(function(){
    $(".boxfil").draggable();
})


function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_docen=$('#Id_docente').attr('id-data')
       params.Id_grupo=$('#Id_grupo').attr('id-data')
       params.Id_materia=$('#Id_materia').attr('id-data')
       params.Id_aula=$('#Id_aula option:selected').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Fecha_ini=$('#fechaIni').val()
       params.Fecha_fin=$('#fechaFin').val()
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}

function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Id_docen=$('#Id_docente').attr('id-data')
       params.Id_grupo=$('#Id_grupo').attr('id-data')
       params.Id_materia=$('#Id_materia').attr('id-data')
       params.Id_aula=$('#Id_aula option:selected').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Fecha_ini=$('#fechaIni').val()
       params.Fecha_fin=$('#fechaFin').val()
   var query=$.param( params );
   window.open(rutaAjax+"?"+query, '_blank');
}


function buscarDocent(Obj){
   if($.trim($(Obj).val()) && $(Obj).val().length>=4){
   var params= new Object()
       params.action="buscarDocent"
       params.buscar="%"
       params.Id_ciclo=$('#ciclo option:selected').val()
       if($.trim($(Obj).val())){
       params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id-data',$(this).attr('id-data'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post('index_aj.php',params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id-data')
  }
}


function buscarGruposCiclo(Obj){
   if($.trim($(Obj).val()) && $(Obj).val().length>=4){
   var params= new Object()
       params.action="buscarGruposCiclo"
       params.buscar="%"
       params.Id_ciclo=$('#ciclo option:selected').val()
       if($.trim($(Obj).val())){
       params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id-data',$(this).attr('id-data'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post('index_aj.php',params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id-data')
  }
}


function buscarMateria(Obj){
   if($.trim($(Obj).val()) && $(Obj).val().length>=4){
   var params= new Object()
       params.action="buscarMateria"
       params.buscar="%"
       if($.trim($(Obj).val())){
       params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id-data',$(this).attr('id-data'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post('index_aj.php',params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id-data')
  }
}
