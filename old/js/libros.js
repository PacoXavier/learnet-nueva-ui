var rutaAjax = "ajax/libros_aj.php"

function mostrar(id){
	window.location="libro.php?id="+id
}


function delete_libro (Id_lib){
   if(confirm("¿Está seguro de eliminar el libro?")){
	    var params= new Object();	
	        params.action="delete_libro"
	        params.Id_lib=Id_lib
	    var funcionExito= function(resp){
	         $('.table tbody').html(resp)
	         ocultar_error_layer()
	    }
	    $.post(rutaAjax,params,funcionExito);
    }
}

function generar_etiqueta(Id_lib){
    var params= new Object();	
        params.action="generar_etiqueta"
        params.Id_lib=Id_lib
    var funcionExito= function(resp){
        window.open('download_etiqueta.php?archivo=etiqueta_libro.zip', '_blank');
    }
    $.post(rutaAjax,params,funcionExito);
}

function buscarLibro(){
   var params= new Object()
       params.action="buscarLibro"
       params.buscar="%"
       if($.trim($('#buscar').val())){
       params.buscar=$('#buscar').val()
       }
   var funcionExito= function(resp){
	   $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}

function mostrar_botones(Obj){
   if($(Obj).next().is(":hidden")) {
	   $(Obj).next().slideDown("fast");
           $(Obj).html('Ocultar')
   } else {
	   $(Obj).next().slideUp("fast");
           $(Obj).html('Mostrar')
   }

}