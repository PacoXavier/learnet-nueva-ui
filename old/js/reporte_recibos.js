var rutaAjax = "ajax/reporte_recibos_aj.php"
$(window).ready(function(){
    $(".boxfil").draggable();
})

function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_alum=$('.buscarFiltro').attr('id_alum')
       params.Folio=$('#folio').val()
       params.Concepto=$('#concepto option:selected').val()
       params.Fecha_ini=$('#fecha_ini').val()
       params.Fecha_fin=$('#fecha_fin').val()
       params.Id_ofe=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
       params.activo=0
       if($('#activo').is(':checked')){
          params.activo=1 
       }
       params.cancelado=0
       if($('#cancelado').is(':checked')){
          params.cancelado=1 
       }
       var funcionExito= function(resp){
           $(Obj).text('Buscar')
           $('.table tbody').html(resp)
       }
       $.post(rutaAjax,params,funcionExito);
}


function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Id_alum=$('.buscarFiltro').attr('id_alum')
       params.Folio=$('#folio').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Concepto=$('#concepto option:selected').val()
   var query=$.param( params );
        window.open(rutaAjax+"?"+query, '_blank');

}




