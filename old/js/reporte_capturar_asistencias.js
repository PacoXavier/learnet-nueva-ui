$(window).ready(function() {
    get_asistencias_mes();
    //handling click event on checkboxes 
    //assigning states
    $('input:checkbox[class=indeterminate]').live('click', function(){
            switch($(this).data("checked")) {
            // unchecked, going indeterminate
                    case 0:
                            $(this).data("checked",1);
                            $(this).prop("indeterminate",true);
                            $(this).attr("asistio","0");
                    break;
                    // indeterminate, going checked
                    case 1:
                            $(this).data("checked",2);
                            $(this).prop("indeterminate",false);
                            $(this).prop("checked",true);
                            $(this).attr("asistio","1");
                    break;

                    // checked, going unchecked
                    default:  
                            $(this).data("checked",0);
                            $(this).prop("indeterminate",false);
                            $(this).prop("checked",false);
                            $(this).removeAttr("asistio");
            }
            var $faltas=0;
            //Actualizamos las asistencias
            $(this).closest('tr').find('input[asistio=0]').each(function(){
                $faltas++;
            })
            $(this).closest('tr').find('.total_faltas').html($faltas)
            var $asistencias=0;
            $(this).closest('tr').find('input[asistio=1]').each(function(){
                $asistencias++;
            })
            $(this).closest('tr').find('.total_asistencias').html($asistencias)
    }); 
});

function update_chekbox(){
        $('input:checkbox[class=indeterminate]').each(function(){
                if($(this).attr("asistio")==1){
                    $(this).data("checked",2);
                    $(this).prop("indeterminate",false);
                    $(this).prop("checked",true); 
                }else if($(this).attr("asistio")==0){
                    $(this).data("checked",1);
                    $(this).prop("indeterminate",true);
                    $(this).attr("asistio","0"); 
                }else{
                    $(this).data("checked",0);
                    $(this).prop("indeterminate",false);
                    $(this).prop("checked",false);  
                }
        });
}

function hide_box(){
  $('#box').animate({
    opacity: 0
    }, 500, function() {
    // Animation complete.
   });  
} 

function show_box(){
  $('#box').animate({
    opacity: 1
    }, 500, function() {
    // Animation complete.
   });  
} 
 function get_asistencias_mes(Fecha_ini,Fecha_fin){
     hide_box()
    var params= new Object();	
        params.action="get_asistencias_mes"
        params.Id_ciclo=$('#Id_ciclo').val()
        params.Fecha_ini=Fecha_ini
        params.Fecha_fin=Fecha_fin
        params.Id_grupo=$('#Id_grupo').val()
    var funcionExito= function(resp){
         $('#box').html(resp);
         show_box()
         setTimeout(update_chekbox(),2000);
    }
    $.post("asistencias_aj.php",params,funcionExito);
}

 function get_total(){
    $('#box').css('opacity','0')
    var params= new Object();	
        params.action="get_total"
        params.Id_ciclo=$('#Id_ciclo').val()
        params.Id_grupo=$('#Id_grupo').val()
    var funcionExito= function(resp){
         $('#box').html(resp);
          show_box()
    }
    $.post("asistencias_aj.php",params,funcionExito);
}



function save_asistencias(){
    var params= new Object();	
        params.action="save_asistencias"
        params.Id_ciclo=$('#Id_ciclo').val()
        params.Id_grupo=$('#Id_grupo').val()
        params.mes=$('#mes').val()
        params.anio=$('#anio').val()
        params.alumnos= new Array();
        $('#tabla-asistencias tbody tr').each(function(){
              var alum=new Object()
                  alum.Id=$(this).attr('id-alum')
                  alum.Fechas= new Array()
                  $(this).find('td input:checkbox[class=indeterminate]').each(function(){
                         var Fecha= new Object()
                             Fecha.date=$(this).val()
                            if($(this).attr("asistio")==1){
                                Fecha.asistio=1
                                alum.Fechas.push(Fecha)
                            }else if($(this).attr("asistio")==0){
                                Fecha.asistio=0
                                alum.Fechas.push(Fecha)
                            }
                    });
                  params.alumnos.push(alum)
        })
        
    var funcionExito= function(resp){
         setTimeout(update_chekbox(),2000);
        alert("Datos guardados con éxito")
    }
    $.post("asistencias_aj.php",params,funcionExito);
}



