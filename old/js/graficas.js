var rutaAjax = "ajax/graficas_aj.php"
window.onload = function(){
        getValores()
}

function generarRgb(){
var hue = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';
return hue;
}

var options = {
 //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
    scaleBeginAtZero : true,

    showScale: true,
    
                
    responsive: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines : true,

    //String - Colour of the grid lines
    scaleGridLineColor : "rgba(0,0,0,.05)",

    //Number - Width of the grid lines
    scaleGridLineWidth : 1,

    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,

    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,

    //Boolean - If there is a stroke on each bar
    barShowStroke : true,

    //Number - Pixel width of the bar stroke
    barStrokeWidth : 2,

    //Number - Spacing between each of the X value sets
    barValueSpacing : 10,

    //Number - Spacing between data sets within X values
    barDatasetSpacing : 1,
    animation: true,
    animationEasing: "easeInOutSine"
    
}


function getValores(){
    var params= new Object();	
        params.action="getValores"
        params.Id_alum=$('#Id_alum').val()
    var funcionExito= function(resp){
        var arrayCat=new Array()
        $.each(resp, function(key, val) {
            var barChartDataCero = {
                labels : [],
                datasets : [
                        {
                            fillColor : generarRgb(),
                            strokeColor : "rgb(220,220,220)",
                            highlightFill: "rgb(220,220,220)",
                            highlightStroke: "rgb(220,220,220)"
                        }
                ]
            }
            var ctx = document.getElementById("myChart_"+key).getContext("2d");
            var myBarChart = new Chart(ctx).Bar(barChartDataCero,options);
            arrayCat.push(myBarChart)
         })
        $.each(resp, function(key, val) {
              $.each(val.Campos, function(k2, v2) {
                     arrayCat[key].addData([v2.Valor], v2.Nombre);
                     arrayCat[key].datasets[0].bars[k2].fillColor = generarRgb();
              })
        });
    }
    $.post(rutaAjax,params,funcionExito,"json");
}

/*
//myBarChart.addData([66], "Logico matematica");
myBarChart.datasets[0].bars[0].fillColor = "#2ECC71";
myBarChart.datasets[0].bars[0].highlightFill = "#58D68D";
myBarChart.datasets[0].bars[1].fillColor = "#3498DB";
myBarChart.datasets[0].bars[1].highlightFill = "#5DADE2";
*/


	


    
