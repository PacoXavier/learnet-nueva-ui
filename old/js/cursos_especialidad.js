var rutaAjax = "ajax/cursos_especialidad_aj.php"

function mostrarBoxModal(Id_curso) {
    var params = new Object();
        params.action = "mostrarBoxModal"
        params.id_curso = Id_curso
    $.post(rutaAjax, params, function(resp){
        mostrar_error_layer(resp);
    });
}



function saveCurso(Id_curso) {
    var params = new Object();
        params.action = "saveCurso"
        params.id_curso = Id_curso
        params.id_esp = $('#id_esp').val()
        params.clave = $('#clave').val()
        params.fechaIni = $('#fechaIni').val()
        params.fechaFin = $('#fechaFin').val()
        params.tipoPago = $('#tipo-pago option:selected').val()
        params.diaEspecifico = $('#diaEspecifico').val()
        params.numHoras = $('#numHoras').val()
        params.numSesiones = $('#numSesiones').val()
        params.turno = $('#turno option:selected').val()
    var error = 0
    var errores = new Array()

    if ($('#clave').val().length == 0) {
        error = "Clave requerido"
        errores.push(error);
    }
    if ($('#turno option:selected').val() == 0) {
        error = "Turno requerido"
        errores.push(error);
    }
    
    if ($('#tipo-pago option:selected').val() == 0) {
        error = "Tipo  de ciclo requerido"
        errores.push(error);
    }

    if($('#diaEspecifico').length>0){
        if ($('#diaEspecifico').val().length == 0) {
            error = "Día especifico requerido"
            errores.push(error);
        }
    }
    
    /*
    if ($('#numHoras').val().length == 0) {
        error = "Número de horas requerido"
        errores.push(error);
    }

    if ($('#numSesiones').val().length == 0) {
        error = "Número de sesiones requerido"
        errores.push(error);
    }
    
    if ($('#fechaIni').val().length == 0) {
        error = "Fecha de inicio requerido"
        errores.push(error);
    }
    if ($('#fechaFin').val() == 0) {
        error = "Fecha de fin requerido"
        errores.push(error);
    }
    */
 
    if (errores.length == 0) {
        $.post(rutaAjax, params, function () {
             updatePage()
             ocultar_error_layer()
        });
    } else {
        imprimirErrores(errores)
    }
}



function mostrarBoxDia() {
    var params = new Object();
        params.action = "mostrarBoxDia"
        params.tipoPago = $('#tipo-pago option:selected').val()
    $.post(rutaAjax, params, function(resp){
        $('#box-dia-especifico').html(resp)
    });
}

function updatePage() {
    var params = new Object();
        params.action = "updatePage"
        params.id_esp = $('#id_esp').val()
    $.post(rutaAjax, params, function(resp){
        $('.table tbody').html(resp)
    });
}


function deleteCurso(Id_curso) {
    if (confirm("¿Está seguro de eliminar el curso?")) {
        var params = new Object();
            params.action = "deleteCurso"
            params.id_curso = Id_curso
        $.post(rutaAjax, params, function(resp){
             updatePage()
        });
    }
}
