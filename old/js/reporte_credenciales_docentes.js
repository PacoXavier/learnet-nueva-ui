var rutaAjax = "ajax/reporte_credenciales_docentes_aj.php"

$(window).ready(function(){
    $(".boxfil").draggable();
})


function buscarDocen(){
   if($.trim($('.buscarFiltro').val()) && $('.buscarFiltro').val().length>=4){
   var params= new Object()
       params.action="buscarDocen"
       params.buscar="%"
       if($.trim($('.buscarFiltro').val())){
       params.buscar=$('.buscarFiltro').val()
       }
   var funcionExito= function(resp){
      $('.Ulbuscador').html(resp)
      $('.Ulbuscador li').on("click", function(event){
          $('.buscarFiltro').val($(this).text())
          $('.buscarFiltro').attr('id-data',$(this).attr('id_docen'))
          $('.Ulbuscador').html('')
       });
   }
   $.post(rutaAjax,params,funcionExito);	
   }else{
      $('.Ulbuscador').html('')
      $('.buscarFiltro').removeAttr('id-data')
  }	
}


function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_docen=$('.buscarFiltro').attr('id-data')
       params.Status=$('#status option:selected').val()
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}



function confirmarEnvio(){
    if(confirm("¿Generar credenciales?")){
        generar_credenciales()
    }
}

var row=0;

function generar_credenciales(){
    if(row<$(".table tbody tr").length){
            if(row==0){
               mostrar_layer_generando() 
            }
            doMensajeRow() 
    }else{
         row=-1
         ocultar_error_layer()
         window.open('download_credenciales_docentes.php?archivo=credenciales_docentes.zip', '_blank');
         //alert("Credenciales generadas!");  

    }
}

function mostrar_layer_generando(){
    var params= new Object();	
        params.action="mostrar_layer_generando"
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}


function doMensajeRow(){
	row=row+1;
	if($(".table tbody tr:nth-child("+((row))+")").attr("id_docen")>0 && $(".table tbody tr:nth-child("+((row))+")").find('input[type="checkbox"]').is(':checked')){
		var params= new Object()
			params.action="generar_credencial"
			params.Id_docente=$(".table tbody tr:nth-child("+((row))+")").attr("id_docen")
			$.post(rutaAjax,params,function(respuesta){
				if(respuesta.UltimaRenovacion!=undefined){
                                        $('#alumnosCAmbiados').html(row)
                                        $(".table tbody tr:nth-child("+((row))+") td:nth-child(3)").html(respuesta.UltimaRenovacion)
                                        $(".table tbody tr:nth-child("+((row))+") td:nth-child(4)").html(respuesta.Vigencia)
                                       
                                        $(".table tbody tr:nth-child("+((row))+") td:last-child").css('background','lightgreen')
                                        $(".table tbody tr:nth-child("+((row))+") td:last-child").css('color','black')
                                         $(".table tbody tr:nth-child("+((row))+") td:nth-child(6)").attr('style',respuesta.Style)
					$(".table tbody tr:nth-child("+((row))+") td:nth-child(6) span").html(respuesta.Status)
				}
				generar_credenciales()
			},"json")
	}else{
	    generar_credenciales()
	}
}



function marcar_alumnos(){
    if($('#all_alumns').is(":checked")){
      $('.table tbody tr  input[type="checkbox"]').prop('checked',true);
    }else{
      $('.table tbody tr  input[type="checkbox"]').prop('checked',false);
    }
}