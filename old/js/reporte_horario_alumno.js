var rutaAjax = "ajax/reporte_horario_alumno_aj.php"

$(window).ready(function(){
    $(".boxfil").draggable();
})

function filtro(Obj){
   $(Obj).text('Buscando...')
  var params= new Object()
       params.action="mostrar_horario"
       params.Id_aula=$('#aula option:selected').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Id_ofe_alum=$('#Id_ofe_alum option:selected').val()
  if($('#Id_ofe_alum option:selected').val()>0 && $('#ciclo option:selected').val()>0){
      var funcionExito =function(resp){
          $(Obj).text('Buscar')
          $('.table tbody').html(resp) 
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      alert("Selecciona los campos requeridos")
  }
}

function buscarAlum(){
   if($.trim($('.buscarFiltro').val()) && $('.buscarFiltro').val().length>=4){
       
   var params= new Object()
       params.action="buscarAlum"
       params.buscar=$('.buscarFiltro').val()
       //console.log(params.buscar)
   var funcionExito= function(resp){
       //console.log(resp)
      $('.Ulbuscador').html(resp)
      $('.Ulbuscador li').on("click", function(event){
          $('.buscarFiltro').val($(this).text())
          $('.buscarFiltro').attr('id-data',$(this).attr('id_alum'))
          $('.Ulbuscador').html('')
       });
   }
   $.post(rutaAjax,params,funcionExito);	
   }else{
      $('.Ulbuscador').html('')
      $('.buscarFiltro').removeAttr('id-data')
  }
}



function getOfertas(Id_alum){
      var params= new Object()
       params.action="getOfertas"
       params.Id_alum=Id_alum
   var funcionExito= function(resp){
       $('#Id_ofe_alum').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	 
}


function mostrarCalificacionesGrupo(id_grupo){
    window.open('reporte_capturar_calificaciones.php?id='+id_grupo, '_blank');
}
