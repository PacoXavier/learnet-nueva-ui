var rutaAjax = "ajax/asignar_materias_aj.php"

function mostrar_box_materias(){
    var params= new Object();	
        params.action="mostrar_box_materias"
        params.Id_esp=$('#Id_esp').val()
        params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
        
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}


function alerta(text){
    var div='<div id="box_emergente"><h1>'+text+'</h1><p><button class="delete">Aceptar</button><button onclick="ocultar_error_layer()">Cancelar</button></p></div>'
    mostrar_error_layer(div);	
}
function delete_ciclo_mat(Id_ciclo_mat){
   if(confirm("¿Está seguro de eliminar la materia?, tenga en cuenta que se borraran las calificaciones registradas para esta materia")){
	    var params= new Object();	
	        params.action="delete_ciclo_mat"
	        params.Id_ciclo_mat=Id_ciclo_mat
                params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
	    var funcionExito= function(resp){
              $('#tabla').html(resp)
              ocultar_error_layer()
	    }
	    $.post(rutaAjax,params,funcionExito);
    }
}

function baja_ciclo_mat(Id_ciclo_mat){
   if(confirm("¿Está seguro de dar de baja la materia?, tenga en cuenta que no se borraran los cobros registrados para esta materia")){
	    var params= new Object();	
	        params.action="baja_ciclo_mat"
	        params.Id_ciclo_mat=Id_ciclo_mat
                params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
	    var funcionExito= function(resp){
            $('#tabla').html(resp)
            ocultar_error_layer()
	    }
	    $.post(rutaAjax,params,funcionExito);
    }
}


function generar_cobro(Id_ciclo_alum){
    if(confirm("¿Está seguro de generar los cargos por mensualidad para este ciclo?")){
        var params= new Object();	
            params.action="generar_cobro"
            params.Id_ciclo_alum=Id_ciclo_alum
            params.Id_esp=$('#Id_esp').val();
            params.Id_alum=$('#Id_alum').val();
            params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
        var funcionExito= function(resp){
            if(resp.indexOf('column_one')>=0){
            $('#tabla').html(resp)
            ocultar_error_layer()
            }else{
                alert("Los cargos ya se encuentran generados para este ciclo")
            }
        }
        $.post(rutaAjax,params,funcionExito);
    }
}

function verificar_orientaciones(Obj){
    var params= new Object();	
        params.action="verificar_orientaciones"
        params.Id_mat_esp=$('#lista_materias option:selected').val()
    var funcionExito= function(resp){
        $('#box_orientaciones').html(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}



function update_materias(){
    var params= new Object();	
        params.action="update_materias_grado"
        params.Id_grado=$('#grado option:selected').val()
    var funcionExito= function(resp){
         $('#lista_materias').html(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}


function save_materia(){
    var params= new Object();	
        params.action="save_materia"
        params.Id_alum=$('#Id_alum').val()
        params.Id_esp=$('#Id_esp').val()
        params.Id_ciclo_alum=$('#ciclo option:selected').val()
        params.Id_mat_esp=$('#lista_materias option:selected').val()
        params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
        params.Id_ori=$('#orientaciones option:selected').val()
        params.Tipo_mat=$('#tipo_materia option:selected').val()
    if($('#ciclo option:selected').val()>0 && $('#grado option:selected').val()>0 && $('#lista_materias option:selected').val()>0 && $('#tipo_materia option:selected').val()>0){
    var funcionExito= function(resp){
        if(resp.indexOf('column_one')>=0){
        $('#tabla').html(resp)
        ocultar_error_layer()
        }else{
            alert("La materia ya se encuentra registrada para este ciclo")
        }
    }
    $.post(rutaAjax,params,funcionExito);
     }else{
         alert("Completa los campos faltantes")
     }
}


function validarPrerrequisitos(){
    var params= new Object();	
        params.action="validarPrerrequisitos"
        params.Id_alum=$('#Id_alum').val()
        params.Id_esp=$('#Id_esp').val()
        params.Id_ciclo_alum=$('#ciclo option:selected').val()
        params.Id_mat_esp=$('#lista_materias option:selected').val()
        params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
        params.Id_ori=$('#orientaciones option:selected').val()
        params.Tipo_mat=$('#tipo_materia option:selected').val()
    if($('#ciclo option:selected').val()>0 && $('#grado option:selected').val()>0 && $('#lista_materias option:selected').val()>0 && $('#tipo_materia option:selected').val()>0){
        var funcionExito= function(resp){
            if(resp.existe==1){
               save_materia();
            }else{
               alert("No se puede añadir la materia debido a que necesitas pasar el prerrequisito "+resp.nombre_mat);
            }
        }
        $.post(rutaAjax,params,funcionExito,"json");
     }else{
         alert("Completa los campos faltantes")
     }
}


function mostrarBoxAsignarMateriaCurso(Id_ciclo_alum){
    var params= new Object();	
        params.action="mostrarBoxAsignarMateriaCurso"
        params.Id_esp=$('#Id_esp').val()
        params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
        params.Id_ciclo_alum=Id_ciclo_alum
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}


function validarPrerrequisitosMateriaCurso(Id_ciclo_alum){
    var params= new Object();	
        params.action="validarPrerrequisitos"
        params.Id_alum=$('#Id_alum').val()
        params.Id_esp=$('#Id_esp').val()
        params.Id_ciclo_alum=Id_ciclo_alum
        params.Id_mat_esp=$('#lista_materias option:selected').val()
        params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
        params.Id_ori=$('#orientaciones option:selected').val()
        params.Tipo_mat=$('#tipo_materia option:selected').val()
    if(Id_ciclo_alum>0 && $('#grado option:selected').val()>0 && $('#lista_materias option:selected').val()>0 && $('#tipo_materia option:selected').val()>0){
        var funcionExito= function(resp){
            if(resp.existe==1){
               save_materia_curso(Id_ciclo_alum);
            }else{
               alert("No se puede añadir la materia debido a que necesitas pasar el prerrequisito "+resp.nombre_mat);
            }
        }
        $.post(rutaAjax,params,funcionExito,"json");
     }else{
         alert("Completa los campos faltantes")
     }
}


function save_materia_curso(Id_ciclo_alum){
    var params= new Object();	
        params.action="save_materia_curso"
        params.Id_alum=$('#Id_alum').val()
        params.Id_esp=$('#Id_esp').val()
        params.Id_ciclo_alum=Id_ciclo_alum
        params.Id_mat_esp=$('#lista_materias option:selected').val()
        params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
        params.Id_ori=$('#orientaciones option:selected').val()
        params.Tipo_mat=$('#tipo_materia option:selected').val()
    if(Id_ciclo_alum>0 && $('#grado option:selected').val()>0 && $('#lista_materias option:selected').val()>0 && $('#tipo_materia option:selected').val()>0){
    var funcionExito= function(resp){
        if(resp.indexOf('column_one')>=0){
        $('#tabla').html(resp)
        ocultar_error_layer()
        }else{
            alert("La materia ya se encuentra registrada para este ciclo")
        }
    }
    $.post(rutaAjax,params,funcionExito);
     }else{
         alert("Completa los campos faltantes")
     }
}

function generar_cobro_curso(Id_ciclo_alum){
    if(confirm("¿Está seguro de generar los cargos por mensualidad?")){
        var params= new Object();	
            params.action="generar_cobro_curso"
            params.Id_ciclo_alum=Id_ciclo_alum
            params.Id_esp=$('#Id_esp').val();
            params.Id_alum=$('#Id_alum').val();
            params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
        var funcionExito= function(resp){
            if(resp.indexOf('column_one')>=0){
            $('#tabla').html(resp)
            ocultar_error_layer()
            }else{
                alert("Los cargos ya se encuentran generados")
            }
        }
        $.post(rutaAjax,params,funcionExito);
    }
}