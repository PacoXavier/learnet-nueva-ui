var rutaAjax = "ajax/reporte_seguro_aj.php"
$(window).ready(function(){
    $(".boxfil").draggable();
})


function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Tipo=$('#tipo option:selected').val()
       params.Id_ofe=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
       params.Opcion_pago=$('#opcion option:selected').val()
   if($('#tipo option:selected').val()>0){ 
       var funcionExito= function(resp){
           $(Obj).text('Buscar')
           $('#mascara_tabla').html(resp)
       }
       $.post(rutaAjax,params,funcionExito);	
   }else{
       alert("Selecciona el tipo")
   }
}



function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Tipo=$('#tipo option:selected').val()
       params.Id_ofe=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
       params.Opcion_pago=$('#opcion option:selected').val()
   var query=$.param( params );
   window.open(rutaAjax+"?"+query, '_blank');
}


function updateFiltros(){
    if($('#tipo option:selected').val()=="1"){
        $('.box-data-alumnos').css('display','block')
    }else{
        $('.box-data-alumnos').css('display','none')
    }
}