var rutaAjax = "ajax/reporte_acciones_aj.php"
$(window).ready(function(){
    $(".boxfil").draggable();
})


function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_alum=$('.buscarFiltro').attr('id_alum')
       params.fecha_ini=$('#fecha_ini').val()
       params.fecha_fin=$('#fecha_fin').val()
       params.Tipo=$('#Tipo option:selected').val()
       params.Id_oferta=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}


function save_asistio(Id_accion,Obj){
   if(confirm("¿Confirmar asistencia?")){
   var params= new Object()
       params.action="save_asistio"
       params.Id_accion=Id_accion
   var funcionExito= function(resp){
       $(Obj).closest('tr').remove()
       alert("Asistencia confirmada")
   }
   $.post(rutaAjax,params,funcionExito);	
   }
}


function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Id_alum=$('.buscarFiltro').attr('id_alum')
       params.fecha_ini=$('#fecha_ini').val()
       params.fecha_fin=$('#fecha_fin').val()
       params.Tipo=$('#Tipo option:selected').val()
       params.Id_oferta=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
   var query=$.param( params );
   window.open(rutaAjax+"?"+query, '_blank');
}



