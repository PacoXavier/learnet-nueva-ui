var rutaAjax = "ajax/interesado_aj.php"
function save_inte(){
    var params= new Object();	
        params.action="save_inte"
        params.nombre_int=$('#nombre_int').val()
        params.Id_alumn=$('#Id_alumn').val()
        params.apellidoP_int=$('#apellidoP_int').val()
        params.apellidoM_int=$('#apellidoM_int').val()
        params.email_int=$('#email_int').val()
        params.cel_int=$('#cel_int').val()
        params.medio=$('#medio option:selected').val()
        params.prioridad=$('#prioridad option:selected').val()
        params.Id_dir=$('#Id_dir').val()
        params.medio_contacto=$('#medio_contacto option:selected').val()
        params.ciudad_int=$('#ciudad_int').val()
        params.contactar=$('#contactar').val()
        params.comentarios=$('#comentarios').val()
        params.Id_dir=$('#Id_dir').val()
        params.ofertas=new Array();
        
        //Ofertas del interesado
        $('#list_ofertas ul').each(function(){
                var ofe= new Object();
                    ofe.Id_plantel=$(this).find('.plantel option:selected').val()
                    ofe.Id_oferta=$(this).find('.oferta option:selected').val()
                    ofe.Id_esp=$(this).find('.curso option:selected').val()
                    ofe.Id_ori=$(this).find('.orientacion option:selected').val()
                    ofe.Id_grado=$(this).find('.grado option:selected').val()
                    ofe.Id_ciclo=$(this).find('.ciclo option:selected').val()
                    ofe.opcion=$(this).find('.opcionPago option:selected').val()
                    ofe.Turno=$(this).find('.turno option:selected').val()
                    ofe.Id_ofe_alum=$(this).attr('Id_ofe_alum')
                    
                    //por ciclos
                    ofe.Id_ciclo_alum=$(this).attr('Id_ciclo_alum')
                    ofe.Id_pago_ciclo=$(this).attr('Id_pago_ciclo')
                    
                    //sin ciclos
                    ofe.Id_per_ciclo=$(this).attr('Id_per_ciclo')
                    ofe.Id_cargo_per=$(this).attr('Id_cargo_per')
                    ofe.Id_curso_esp=$(this).find('.cursos-especialidad option:selected').val()
                    
                    if($(this).find('.oferta option:selected').val()>0 
                            && $(this).find('.curso option:selected').val()>0 
                            && $(this).find('.grado option:selected').val()>0 
                            && ($(this).find('.ciclo option:selected').val()>0 || $(this).find('.cursos-especialidad option:selected').val()>0)
                            && $(this).find('.opcionPago option:selected').val()>0
                            && $(this).find('.turno option:selected').val()>0
                      ){
                            params.ofertas.push(ofe)  
                    }  
        });
        params.tutores=new Array();
        $('#list_tutores ul').each(function(){
            var tutor= new Object();
                tutor.id=$(this).attr('id-tutor');
                tutor.id_dir=$(this).attr('id-dir');
                tutor.parantesco=$(this).find('.parentesco-tutor').val();
                tutor.apellidoP=$(this).find('.apellidoP-tutor').val();
                tutor.apellidoM=$(this).find('.apellidoM-tutor').val();
                tutor.nombre=$(this).find('.nombre-tutor').val();
                tutor.tel=$(this).find('.tel-tutor').val();
                tutor.cel=$(this).find('.cel-tutor').val();
                tutor.emal=$(this).find('.email-tutor').val();
                tutor.calle=$(this).find('.calle-tutor').val();
                tutor.colonia=$(this).find('.colonia-tutor').val();
                tutor.ciudad=$(this).find('.ciudad-tutor').val();
                tutor.estado=$(this).find('.estado-tutor').val();
                tutor.numExt=$(this).find('.numext-tutor').val();
                tutor.numInt=$(this).find('.numint-tutor').val();
                tutor.cp=$(this).find('.cp-tutor').val();
                params.tutores.push(tutor)  
        });

    if($('#nombre_int').val().length>0 && 
       $('#apellidoP_int').val().length>0 &&
       $('#email_int').val().length>0 &&
       $('#medio option:selected').val()>0 &&
       $('#prioridad option:selected').val()>0 && 
       params.ofertas.length>0){
        $.post(rutaAjax,params,function(resp){
             mostrar_acciones(resp.Id)
             window.location="interesado.php?id="+resp.Id  
        },"json");
    }else{
          alert('Completa los datos faltantes');
    }
}





function mostrar_acciones(Id){
    var params= new Object();	
        params.action="mostrar_acciones"
        params.Id=Id
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}


function box_inscripcion(){
    var params= new Object();	
        params.action="box_inscripcion"
        params.Id_alumn=$('#Id_alumn').val()
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}



function ejecutar_accion(Id){
    var params= new Object();	
        params.action="ejecutar_accion"
        params.Id_int=Id
        params.acciones= new Array()
        $('#tabla_acciones tbody tr').each(function(){
                 if($(this).find('.examen').is(":checked")){
                    var accion=new Object()
                        accion.Id_ofe_alumn=$(this).attr('id-ofe-alum')
                        accion.tipo=1
                        params.acciones.push(accion)
                 }
                 if($(this).find('.cita_sesion').val().length>0){
                    var accion=new Object()
                        accion.Id_ofe_alumn=$(this).attr('id-ofe-alum')
                        accion.tipo=2
                        accion.fecha_sesion=$(this).find('.cita_sesion').val()
                        params.acciones.push(accion)
                 }
                 if($(this).find('.clase_muestra').val().length>0){
                    var accion=new Object()
                        accion.Id_ofe_alumn=$(this).attr('id-ofe-alum')
                        accion.tipo=3
                        accion.fecha_sesion=$(this).find('.clase_muestra').val()
                        params.acciones.push(accion)
                 }
        })

    var funcionExito= function(resp){
        
        ocultar_error_layer(resp);
        setTimeout(function(){
           window.location="interesado.php?id="+Id
        }, 500)
         
    }
    $.post(rutaAjax,params,funcionExito);
}


function mostrar_box_comentario(Id_int,tipo){
    var params= new Object();	
        params.action="mostrar_box_comentario"
        params.Id_int=Id_int
        params.tipo=tipo
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}


function buscar_int(){
 if($.trim($('#email_int').val())){
       var params= new Object()
           params.action="buscar_int"
           params.buscar=$('#email_int').val()
      if($('#email_int').val().length>0){
            var funcionExito =function(resp){
                $('#buscador_int').html(resp) 
            }
            $.post(rutaAjax,params,funcionExito);
        }
  }else{
      $('#buscador_int').html('')
  }
}


function buscar_ciudad(){
 if($.trim($('#ciudad_int').val())){
       var params= new Object()
           params.action="buscar_ciudad"
           params.buscar=$('#ciudad_int').val()
      var funcionExito =function(resp){
          $('#buscador_ciudad').html(resp) 
          $('#buscador_ciudad li').click(function() {
             $('#ciudad_int').val($(this).text())
             $('#buscador_ciudad').html('') 
          });
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      $('#buscador_ciudad').html('')
  }
}

function box_curso(){
    var params= new Object();	
        params.action="box_curso"
        params.Id_alum=$('#Id_alumn').val()
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}

function update_oferta(Obj){
    var params= new Object();	
        params.action="update_oferta"
        params.Id_plantel=$(Obj).closest('.form').find('.plantel option:selected').val()
    var funcionExito= function(resp){
         $(Obj).closest('.form').find('.oferta').html(resp)
         $(Obj).closest('.form').find('.curso').html('')
         $(Obj).closest('.form').find('.box_orientacion').html('')
         $(Obj).closest('.form').find('.grado').html('')
    }
    $.post(rutaAjax,params,funcionExito);
}

function getEspecialidades(Obj){
    var params= new Object();	
        params.action="getEspecialidades"
        params.Id_oferta=$(Obj).closest('.form').find('.oferta option:selected').val()
    var funcionExito= function(resp){
         $(Obj).closest('.form').find('.curso').html(resp)
         $(Obj).closest('.form').find('.box_orientacion').html('')
         $(Obj).closest('.form').find('.grado').html('')
    }
    $.post(rutaAjax,params,funcionExito);
}

function getOrientacionesEspecialidad(Obj){
    var params= new Object();	
        params.action="getOrientacionesEspecialidad"
        params.Id_esp=$(Obj).closest('.form').find('.curso option:selected').val()
    var funcionExito= function(resp){
         $(Obj).closest('.form').find('.box_orientacion').html(resp)
         $(Obj).closest('.form').find('.turno option[value=0]').prop('selected',true)
          getGrado(Obj);
          getCursosEspecialidad(Obj);
    }
    $.post(rutaAjax,params,funcionExito);
}


function getGrado(Obj){
    var params= new Object();	
        params.action="getGrado"
        params.Id_esp=$(Obj).closest('.form').find('.curso option:selected').val()
    var funcionExito= function(resp){
         $(Obj).closest('.form').find('.grado').html(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}

function delete_ofe_alum(Id_ofe_alum){
    if(confirm("¿Está seguro de eliminar la oferta del alumno?")){
        var params= new Object();	
            params.action="delete_ofe_alum"
            params.Id_ofe_alum=Id_ofe_alum
            params.Id_alum=$('#Id_alumn').val()
        $.post(rutaAjax,params,function(resp){
            $('#contenido').html(resp);
            ocultar_error_layer()   
        });
    }
}




function add_ofe_alum(){
    var params= new Object();	
        params.action="add_ofe_alum"
        params.Id_alumn=$('#Id_alumn').val()
        params.Id_oferta=$('#oferta option:selected').val()
        params.Id_esp=$('#curso option:selected').val()
        params.Id_ori=$('#orientacion option:selected').val()
        params.Id_grado=$('#grado option:selected').val()
        params.Id_ciclo=$('#ciclo option:selected').val()
        params.opcion=$('#opcionPago option:selected').val()
        params.beca=$('#beca').val()
        params.descuento=$('#descuento').val()
    if($('#oferta option:selected').val()>0 && $('#curso option:selected').val()>0 && $('#grado option:selected').val()>0 && $('#ciclo option:selected').val()>0 && $('#opcionPago option:selected').val()>0){
	    var funcionExito= function(resp){
                  if(resp.length>0){
                     $('#contenido').html(resp)
                     ocultar_error_layer() 
                  }else{
                      alert("El curso ya se encuentra registrado para el alumno")
                  }
	        
	    }
	   $.post(rutaAjax,params,funcionExito);	
    }else{
	    alert("Completa los campos")
    }
}




function generar_pdf_insc(Id_alum,Id_ofe_alum){
    window.open("formato_pago.php?id="+Id_alum+"&id_ofe="+Id_ofe_alum);
    //function para crear pdf de inscripcion
    ocultar_error_layer();
}


function no_imprimir_comprobante(Id_int){
    ocultar_error_layer();
    setTimeout(function(){
       window.location="alumno.php?id="+Id_int
    }, 500)
}

function imprimir_comprobante(Id_int){
    window.open("comprobante.php?id="+Id_int);
    setTimeout(function(){
        window.location="alumno.php?id="+Id_int
    },1000) 
}

function add_oferta(){
    var params= new Object();	
        params.action="add_oferta"
    $.post(rutaAjax,params,function(resp){
        $('#list_ofertas').append(resp);
        alert("Añadido!");
    });
}


function inscribir(){
    var params= new Object();	
        params.action="inscribir"
        params.Id_int=$('#Id_alumn').val()
        params.ofertas= new Array();
        var ban=0;
        $('#box-inscripcion table tbody  tr').each(function(){
            var ofe= new Object()
                ofe.Id_ofe_alum=$(this).attr('Id_ofe_alum')
                ofe.Id_ciclo=$(this).attr('Id_ciclo')
                ofe.Id_esp=$(this).attr('Id_esp')
                ofe.pago_inscripcion=$(this).attr('pago_inscripcion')
                ofe.Id_ciclo_alumno=$(this).attr('id-ciclo-alum')
                
                params.ofertas.push(ofe)
                //Se deja que se anexen todas las ofertas
                //aunque no esten pagas para que se borren
                //de la lista de ofertas del usuario
                if(ofe.pago_inscripcion==1 && $(this).find('.inscribir').is(':checked')){
                    ban=1;
                }
        })
    if(ban==1){
    var funcionExito= function(resp){
        if(confirm("¿Imprimir comprobante de pago de inscripci\u00F3n?")){
            imprimir_comprobante($('#Id_alumn').val())
        }else{
            no_imprimir_comprobante($('#Id_alumn').val())
        }
    }
    $.post(rutaAjax,params,funcionExito);
    }else{
        alert("Selecciona una oferta")
    }
}

function pagar(Id_ofe_alum){
    window.location="pago.php?id="+Id_ofe_alum
}

function pagarCurso(Id_ofe_alum){
    window.location="cargos_curso.php?id="+Id_ofe_alum
}



function capturar_seguimiento(Id_int){
    var params= new Object();	
        params.action="capturar_seguimiento"
        params.Id_int=Id_int
        params.coment=$('#coment').val()
        params.contactar=$('#contactar').val()
        if($('#tipo-seg').length>0){
           params.tipo=$('#tipo-seg option:selected').val()
        }
    if($('#coment').val().length>0){
        $.post(rutaAjax,params,function(resp){
            ocultar_error_layer();
            buscarNotificaciones();
        });
    }else{
	 alert("Completa los datos");
    }
}

function getTipociclo (Obj){
    var params= new Object();	
        params.action="getTipoCiclo"
        params.Id_ofe=$(Obj).closest('.form').find('.oferta option:selected').val()
    $.post(rutaAjax,params,function(resp){
        $(Obj).closest('ul').html(resp);
    });
}

function getCursosEspecialidad(Obj){
    var params= new Object();	
        params.action="getCursosEspecialidad"
        params.Id_esp=$(Obj).closest('.form').find('.curso option:selected').val();
        params.Turno=$(Obj).closest('.form').find('.turno option:selected').val();
    $.post(rutaAjax,params,function(resp){
        $(Obj).closest('.form').find('.cursos-especialidad').html(resp)
    });
}


function mostrarBoxTutor(){
    var params= new Object();	
        params.action="mostrarBoxTutor";
    $.post(rutaAjax,params,function(resp){
        $('#list_tutores').append(resp);
        alert("Añadido!");
    });
}



function delete_tutor(id_tutor,id_dir){
    if(confirm("¿Está seguro de eliminar al tutor?")){
        var params= new Object();	
            params.action="delete_tutor";
            params.id_tutor=id_tutor;
            params.id_dir=id_dir;
            params.id_alumn=$('#Id_alumn').val();
        $.post(rutaAjax,params,function(resp){
            $('#contenido').html(resp);
            ocultar_error_layer()   
        });
    }
}