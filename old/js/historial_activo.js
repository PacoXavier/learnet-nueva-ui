function box_captura(){
    var params= new Object();	
        params.action="box_captura"
        params.Id_activo=$('#Id_activo').val()
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post("historial_activo_aj.php",params,funcionExito);
}


function delete_hist(Id_hist){
   if(confirm("¿Está seguro de eliminar el historial?")){
	    var params= new Object();	
	        params.action="delete_hist"
	        params.Id_hist=Id_hist
                params.Id_activo=$('#Id_activo').val()
	    var funcionExito= function(resp){
	         $('.table tbody').html(resp)
	         ocultar_error_layer()
	    }
	    $.post("historial_activo_aj.php",params,funcionExito);
    }
}

function save_mantenimiento(){
    var params= new Object();	
        params.action="save_mantenimiento"
        params.Id_activo=$('#Id_activo').val()
        params.estado=$('#estado').val()
        params.comentarios=$('#comentarios').val()
    if($('#estado').val().length>0 && $('#comentarios').val().length>0){
        var funcionExito= function(resp){
           $('.table tbody').html(resp)
	  ocultar_error_layer()
        }
        $.post("historial_activo_aj.php",params,funcionExito);
    }else{
        alert("Completa los datos faltantes")
    }
}