var rutaAjax = "ajax/reporte_envio_calificaciones_aj.php"
var row=0;

function send_email(){
    if(row<$(".table tbody tr").length){
            doMensajeRow() 
    }else{
         row=-1
          //doMensajeRow() 
          alert("Calificaciones enviadas!");  

    }
}


function doMensajeRow(){
	row=row+1;
	if($(".table tbody tr:nth-child("+((row))+")").attr("id_alum")>0){
		var params= new Object()
			params.action="send_email"
			params.Id_alum=$(".table tbody tr:nth-child("+((row))+")").attr("id_alum")
			params.Id_ofe_alum=$(".table tbody tr:nth-child("+((row))+")").attr("id_ofe_alum")
                        params.Id_ciclo=$('#ciclo option:selected').val()
                        params.emails= new Array()
                        $(".table tbody tr:nth-child("+((row))+")").find('.otro-email').each(function(){
                            if($(this).val().length>0){
                               params.emails.push($(this).val()) 
                            }
                        })
			$.post(rutaAjax,params,function(respuesta){
                               //alert(respuesta)
				if(respuesta.length>0){
                                        $('#alumnosCAmbiados').html('Alumnos cambiados: '+row)
                                        $(".table tbody tr:nth-child("+((row))+") td:last-child").css('background','lightgreen')
                                        $(".table tbody tr:nth-child("+((row))+") td:last-child").css('color','black')
                                        $(".table tbody tr:nth-child("+((row))+") td").find('.ciclo-actual').html(respuesta)
					$(".table tbody tr:nth-child("+((row))+") td:last-child span").html('Enviado')
				}
				send_email()
			})
	}else{
	    send_email()
	}
}

function confirmarEnvio(){
    
    if(confirm("¿Enviar calificaciones?")){
        
        if($('#ciclo option:selected').val()>0){
               send_email() 
        }else{
            alert("Seleccione un ciclo")
        }
    }
}


$(window).ready(function(){
    $(".boxfil").draggable();
    if($('#files').length) {
       document.getElementById('files').addEventListener('change', handleFileSelect, false);
    }
})

function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_ofe=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
       params.Opcion_pago=$('#opcion option:selected').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
       
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}



function buscarAlum(){
   var params= new Object()
       params.action="buscarAlum"
       params.buscar="%"
       if($.trim($('#buscar').val())){
       params.buscar=$('#buscar').val()
       }
   var funcionExito= function(resp){
	   $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}


function addInput(Obj){
    $(Obj).closest('td').append('<p><input type="text" class="otro-email"></p>')
}