var rutaAjax = "ajax/reporte_grupos_alumno_aj.php"
$(window).ready(function () {
    $(".boxfil").draggable();
})


function buscarAlum() {
    if ($.trim($('.buscarFiltro').val()) && $('.buscarFiltro').val().length>=4) {
        var params = new Object()
        params.action = "buscarAlum"
        params.buscar = "%"
        if ($.trim($('.buscarFiltro').val())) {
            params.buscar = $('.buscarFiltro').val()
        }
        var funcionExito = function (resp) {
            $('.Ulbuscador').html(resp)
            $('.Ulbuscador li').on("click", function (event) {
                $('.buscarFiltro').val($(this).text())
                $('.buscarFiltro').attr('id_alum', $(this).attr('id_alum'))
                $('.Ulbuscador').html('')
            });
        }
        $.post(rutaAjax, params, funcionExito);
    } else {
        $('.Ulbuscador').html('')
        $('.buscarFiltro').removeAttr('id_alum')
    }
}


function getOfertas(Id_alum) {
    var params = new Object()
    params.action = "getOfertas"
    params.Id_alum = Id_alum
    var funcionExito = function (resp) {
        $('#Id_ofe_alum').html(resp)
    }
    $.post(rutaAjax, params, funcionExito);
}

function getCiclosOferta() {
    var params = new Object()
    params.action = "getCiclosOferta"
    params.Id_ofe_alum = $('#Id_ofe_alum option:selected').val()
    var funcionExito = function (resp) {
        $('#ciclo').html(resp)
    }
    $.post(rutaAjax, params, funcionExito);
}


function filtro(Obj) {

    if ($('.buscarFiltro').attr('id_alum') > 0 && $('#Id_ofe_alum option:selected').val() > 0) {
        $(Obj).text('Buscando...')
        var params = new Object()
        params.action = "filtro"
        params.Id_alumn = $('.buscarFiltro').attr('id_alum')
        params.Id_ciclo = $('#ciclo option:selected').val()
        params.Id_ofe_alum = $('#Id_ofe_alum option:selected').val()
        var funcionExito = function (resp) {
            $(Obj).text('Buscar')
            $('#mascara_tabla').html(resp)
        }
        $.post(rutaAjax, params, funcionExito);
    } else {
        alert("Complete los campos")
    }
}



function mostrar_box_email() {
    var params = new Object();
    params.action = "mostrar_box_email"
    var funcionExito = function (resp) {
        mostrar_error_layer(resp);
    }
    $.post(rutaAjax, params, funcionExito);
}


function send_email(Obj) {
    /*
     var params= new Object()
     params.action="send_email"
     params.asunto=$('#asunto').val()
     params.mensaje=$('#mensaje').val()
     params.alumnos= new Array()
     $('.table tbody tr ').each(function(){
     if($(this).find('input[type="checkbox"]').is(":checked")){
     params.alumnos.push($(this).attr('id_alum'))
     }
     })
     if(params.alumnos.length>0){
     if($('#asunto').val().length>0 && $('#mensaje').val().length>0){
     $(Obj).html('Enviando...')
     $(Obj).removeAttr('onClick')
     var funcionExito= function(resp){
     var mns='Mensaje enviado con  éxito';
     alerta_dos(mns);
     $(Obj).html('Enviar')
     $(Obj).attr('onClick','send_email(this)')
     }
     $.post('alumnos_aj.php',params,funcionExito);	
     }else{
     alert("Completa los campos faltantes")
     }
     }else{
     alert("Selecciona alumnos para enviar el mensaje")
     }
     */
}


