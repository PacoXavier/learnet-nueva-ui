var rutaAjax = "ajax/reporte_historial_usuarios_aj.php"
$(window).ready(function(){
    $(".boxfil").draggable();
    if($('#files').length) {
       document.getElementById('files').addEventListener('change', handleFileSelect, false);
    }
})


function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_usu=$('.buscarFiltro').attr('id-data')
       params.Categoria=$('#categoria option:selected').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Fecha_ini=$('#fecha_ini').val()
       params.Fecha_fin=$('#fecha_fin').val()
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}

function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Id_usu=$('.buscarFiltro').attr('id-data')
       params.Categoria=$('#categoria option:selected').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
       
   var query=$.param( params );
   window.open(rutaAjax+"?"+query, '_blank');
}
