var rutaAjax = "ajax/avisos_aj.php"
var editor
$(document).ready(function() {
    if ($('#files').length) {
    document.getElementById('files').addEventListener('change', handleFileSelect, false);
}
    $(".boxfil_aviso").draggable();
    // The instanceReady event is fired, when an instance of CKEditor has finished
	// its initialization.
    CKEDITOR.on( 'instanceReady', function( ev ) {

    });

    CKEDITOR.replace('editor1');


    editor = CKEDITOR.instances.editor1;
    
    get_alumnos()
    
    
})


function get_profesores(){
    ocultar_filtro()
    if($('#profesores').is(':checked')){
        $('.box-tutores').css('visibility','hidden')
        $(".box-tutores input[type=checkbox]").prop('checked', false);
        var params= new Object();	
            params.action="get_profesores"
        var funcionExito= function(resp){
           $('#box-tabla').html(resp)
        }
        $.post(rutaAjax,params,funcionExito);
    }
}

function get_alumnos(){
    if($('#alumnos').is(':checked')){
        $('.box-tutores').css('visibility','visible')
        mostrar_filtro_alumnos()
        var params= new Object();	
            params.action="get_alumnos"
            var funcionExito= function(resp){
               $('#box-tabla').html(resp)
               
            }
            $.post(rutaAjax,params,funcionExito);
    }
}

function mostrar_filtro_alumnos(){
    if($('#alumnos').is(':checked')){
        var params= new Object();	
            params.action="mostrar_filtro_alumnos"
            var funcionExito= function(resp){
               mostrar_filtro(resp)
            }
            $.post(rutaAjax,params,funcionExito);
    }else{
        ocultar_filtro()
    }
}
function mostrar_filtro(html){ 
    $('.boxfil_aviso').html(html)
    $('.boxfil_aviso').css('visibility','visible')
	$('.boxfil_aviso').animate({
		opacity: 1
	},100,function(){
    })
}

function ocultar_filtro(){ 
	$('.boxfil_aviso').animate({
		opacity: 0
	},100,function(){
	    $('.boxfil_aviso').css('visibility','hidden')
            $('.boxfil_aviso').html('')
	})
}
function mostrarFinder() {
    $('#files').click()
}




function handleFileSelect(evt) {
    //Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // Great success! All the File APIs are supported.

        // FileList object
        var files = evt.target.files;

        var count = 0;
        var bin, name, type, size

        //Funcion loadStartImg
        var loadStartImg = function() {
            //alert("inicia") 
        }

        //Funcion loadEndImg
        var loadEndImg = function(e) {
            var xhr
            if (window.XMLHttpRequest) {
                xhr = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
            //var bin = reader.result;

            // progress bar loadend
            var eventSource = xhr.upload || xhr;
            eventSource.addEventListener("progress", function(e) {
                var pc = parseInt((e.loaded / e.total * 100));
                var mns = 'Cargando ...' + pc + '%'
                alerta(mns);
                if (pc == 100) {
                    ocultar_error_layer()
                }
            }, false);

            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && xhr.status == 200) {
                   $('#list_archivos').html(xhr.responseText)
                   $('#files').val('')
                }
            }
            xhr.open('POST', 'index_aj.php?action=uploadAttachment', true);
            var boundary = 'xxxxxxxxx';
            var body = '--' + boundary + "\r\n";
            body += "Content-Disposition: form-data; name='upload'; filename='" + name + "'\r\n";
            body += "Content-Type: application/octet-stream\r\n\r\n";
            body += bin + "\r\n";
            body += '--' + boundary + '--';
            xhr.setRequestHeader('content-type', 'multipart/form-data; boundary=' + boundary);
            // Firefox 3.6 provides a feature sendAsBinary ()
            if (xhr.sendAsBinary != null) {
                xhr.sendAsBinary(body);
                // Chrome 7 sends data but you must use the base64_decode on the PHP side
            } else {
                xhr.open('POST', 'index_aj.php?action=uploadAttachment&base64=ok&FileName=' + name + '&TypeFile=' + type, true);
                xhr.setRequestHeader('UP-FILENAME', name);
                xhr.setRequestHeader('UP-SIZE', size);
                xhr.setRequestHeader('UP-TYPE', type);
                xhr.send(window.btoa(bin));
            }
            if (status) {
                //document.getElementById(status).innerHTML = '';
            }
        }

        //Funcion loadErrorImg
        var loadErrorImg = function(evt) {
            switch (evt.target.error.code) {
                case evt.target.error.NOT_FOUND_ERR:
                    alert('File Not Found!');
                    break;
                case evt.target.error.NOT_READABLE_ERR:
                    alert('File is not readable');
                    break;
                case evt.target.error.ABORT_ERR:
                    break; // noop
                default:
                    alert('An error occurred reading this file.');
            }
            ;
        }

        //Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {
            /*
                                    // Only process image files.
                                    if (!f.type.match('image.*')) {
                                        alert(1)
                                        continue;
                                    }
                            */

            var reader = new FileReader();
            var preview = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail binari.
                    bin = e.target.result
                    name = theFile.name
                    type = theFile.type
                    size = theFile.size
                };
            })(f);

            preview.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail.
                    /*
                     var li = document.createElement('li');
                     $(li).attr('id','img_'+count)
                     li.innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/><div id="mascara_img"><span></span></div>'].join('');
                     $('#visor_subir_img').append(li)
                     $('#visor_subir_img li img').css({'width':'85px','height':'85px'});
                     $('#ruta_img').val($('#files').val())
                     count++;
                     */
                };
            })(f);

            //Read in the image file as a binary string.
            reader.readAsBinaryString(f);
            //Read in the image file as a data URL.
            preview.readAsDataURL(f);

            // Firefox 3.6, WebKit
            if (reader.addEventListener) {
                reader.addEventListener('loadend', loadEndImg, false);
                reader.addEventListener('loadstart', loadStartImg, false);
                if (status != null) {
                    reader.addEventListener('error', loadErrorImg, false);
                }
                // Chrome 7
            } else {
                reader.onloadend = loadEndImg;
                reader.onloadend = loadStartImg;
                if (status != null) {
                    reader.onerror = loadErrorImg;
                }
            }
        }

    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
}



function filtro(){
   var params= new Object()
       params.action="filtro"
       params.Id_ofe=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
       params.Opcion_pago=$('#opcion option:selected').val()
       params.Id_grado=$('#grado option:selected').val()
   var funcionExito= function(resp){
       $('#box-tabla').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}



var Objx
var row=0;
function send_email(Obj){
    Objx=Obj
    if(row<$(".table tbody tr").length){
            if($('#asunto').val().length>0 && editor.getData().length>0 ){
               var marcados=0
               $('.table tbody tr input[type="checkbox"]').each(function(){
                   if($(this).is(":checked")){
                      marcados++; 
                   }
               })
               
               if(marcados>0){
                    $(Obj).text('Enviando')
                    $(Obj).removeAttr('onClick')
                    doMensajeRow() 
               }else{
                   alert("Selecciona los destinatarios")
               }
            }else{
               alert("Completa los campos faltantes")  
            }
    }else{
         row=-1
         $('#asunto').val('')
         editor.setData('')
         $(Obj).html('Enviar')
         $(Obj).attr('onclick','send_email(this)')
         $('#list_archivos').html('')
         deleteAttachments()
         alert("Envio de emails terminado!");  
    }
}

function doMensajeRow(){
	row=row+1;
	if($(".table tbody tr:nth-child("+((row))+")").attr("id-data")>0 && $(".table tbody tr:nth-child("+((row))+")").find('input[type="checkbox"]').is(':checked')){
		var params= new Object()
			params.action="send_email"
			params.IdRel=$(".table tbody tr:nth-child("+((row))+")").attr("id-data")
                        params.TipoRel=$(".table tbody tr:nth-child("+((row))+")").attr("tipo")
                        params.Tutores=0
                        if($('#tutores').is(":checked")){
                           params.Tutores=1
                        }
                        params.Alumnos=0
                        if($('#alum').is(":checked")){
                           params.Alumnos=1
                        }
                        params.Docentes=0
                        if($('#profesores').is(":checked")){
                           params.Docentes=1
                        }
                        params.Grupos=0
                        if($('#grupos').is(":checked")){
                           params.Grupos=1
                        }
                        params.asunto=$('#asunto').val()
                        params.mensaje=editor.getData() 
			$.post(rutaAjax,params,function(respuesta){
                                 //console.log(respuesta)
				//if(respuesta.length>0){
                                        //$('#alumnosCAmbiados').html('Correos enviados: '+row)
                                        $(".table tbody tr:nth-child("+((row))+") td:last-child").css('background','lightgreen')
                                        $(".table tbody tr:nth-child("+((row))+") td:last-child").css('color','black')
					$(".table tbody tr:nth-child("+((row))+") td:last-child span").html('Enviado')
				//}
				send_email(Objx)
			})
	}else{
	    send_email(Objx)
	}
}

	




function get_grupos(){
    if($('#grupos').is(':checked')){
        $('.box-tutores').css('visibility','hidden')
        $(".box-tutores input[type=checkbox]").prop('checked', false);
        mostrar_filtro_grupos()
        var params= new Object();	
            params.action="get_grupos"
            var funcionExito= function(resp){
               $('#box-tabla').html(resp)
               
            }
            $.post(rutaAjax,params,funcionExito);
    }
}

function mostrar_filtro_grupos(){
    if($('#grupos').is(':checked')){
        var params= new Object();	
            params.action="mostrar_filtro_grupos"
            var funcionExito= function(resp){
               mostrar_filtro(resp)
            }
            $.post(rutaAjax,params,funcionExito);
    }else{
        ocultar_filtro()
    }
}




function filtroGrupo(){
   var params= new Object()
       params.action="filtroGrupo"
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Id_grupo=$('.buscarFiltro').attr('id-data')
   if($('#ciclo option:selected').val()>0 && $('.buscarFiltro').attr('id-data')>0){
       var funcionExito= function(resp){
           $('#box-tabla').html(resp)
       }
       $.post(rutaAjax,params,funcionExito);	
   }else{
       alert("Completa los campos faltantes")
   }
}

