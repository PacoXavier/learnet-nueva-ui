var rutaAjax = "ajax/capturar_calificaciones_aj.php"

$(window).ready(function(){
    $(".boxfil").draggable();
})
function buscarAlum(){
   if($.trim($('.buscarFiltro').val())){
   var params= new Object()
       params.action="buscarAlum"
       params.buscar="%"
       if($.trim($('.buscarFiltro').val())){
       params.buscar=$('.buscarFiltro').val()
       }
   var funcionExito= function(resp){
      $('.Ulbuscador').html(resp)
      $('.Ulbuscador li').on("click", function(event){
          $('.buscarFiltro').val($(this).text())
          $('.buscarFiltro').attr('id-data',$(this).attr('id_alum'))
          $('.Ulbuscador').html('')
       });
   }
   $.post(rutaAjax,params,funcionExito);	
   }else{
      $('.Ulbuscador').html('')
      $('.buscarFiltro').removeAttr('id_alum')
  }
}



function getOfertas(Id_alum){
      var params= new Object()
       params.action="getOfertas"
       params.Id_alum=Id_alum
   var funcionExito= function(resp){
       $('#Id_ofe_alum').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	 
}


function filtro(){
   if($('.buscarFiltro').attr('id-data')>0 && $('#Id_ofe_alum option:selected').val()>0 ){
   var params= new Object()
       params.action="filtro"
       params.Id_alumn=$('.buscarFiltro').attr('id-data')
       params.Id_ofe_alum=$('#Id_ofe_alum option:selected').val()
   var funcionExito= function(resp){
       $('#mascara_tabla').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
   }else{
       alert("Completa los campos")
   }
}

function editar(Obj){
    $(Obj).closest('.table').find('input').addClass('editar')
    $(Obj).closest('.table').find('input').removeAttr('disabled')
    $(Obj).text('Guardar')
    $(Obj).attr('onclick','saveCalificaciones(this)')
}

function saveCalificaciones(Obj){
   var params= new Object()
       params.action="saveCalificaciones"
       params.calificaciones= new Array()
       var table=$(Obj).closest('.table')
       $(table).find('tbody tr').each(function(){
               var cal= new Object()
                   cal.Id_ciclo_mat=$(this).attr('id-ciclo-mat')
                   cal.calificacion=$(this).find('input.CalTotalParciales').val()
                   cal.extra=$(this).find('input.CalExtraordinario').val()
                   //if(cal.calificacion.length>0 || cal.extra.length>0){
                      params.calificaciones.push(cal)
                   //}
       })
   var funcionExito= function(resp){
        $(Obj).closest('.table').find('input').removeClass('editar')
        $(Obj).closest('.table').find('input').attr('disabled','disabled')
        $(Obj).text('Editar')
        $(Obj).attr('onclick','editar(this)')
   }
   $.post(rutaAjax,params,funcionExito);
}

