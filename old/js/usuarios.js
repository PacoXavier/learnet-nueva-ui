var rutaAjax = "ajax/usuarios_aj.php"

function mostrar(id){
	window.location="usuario.php?id="+id
}



function mostrar_box_busqueda(){
    var params= new Object();	
        params.action="mostrar_box_busqueda"
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}
function alerta(text){
    var div='<div id="box_emergente"><h1>'+text+'</h1><p><button class="delete">Aceptar</button><button onclick="ocultar_error_layer()">Cancelar</button></p></div>'
    mostrar_error_layer(div);	
}


function delete_usu(Id_usu){
   if(confirm("¿Está seguro de eliminar el usuario?")){
	    var params= new Object();	
	        params.action="delete_usu"
	        params.Id_usu=Id_usu
	    var funcionExito= function(resp){
	         $('.table tbody').html(resp)
	         ocultar_error_layer()
	    }
	    $.post(rutaAjax,params,funcionExito);
    }
}



function buscarAlum(){
   var params= new Object()
       params.action="buscarUsu"
       params.buscar="%"
       if($.trim($('#buscar').val())){
       params.buscar=$('#buscar').val()
       }
   var funcionExito= function(resp){
	   $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}
