var rutaAjax = "ajax/activos_aj.php"

function mostrar(id){
	window.location="activo.php?id="+id
}

function delete_activo(Id_activo){
   if(confirm("¿Está seguro de eliminar el activo?")){
	    var params= new Object();	
	        params.action="delete_activo"
	        params.Id_activo=Id_activo
	    var funcionExito= function(resp){
	         $('.table tbody').html(resp)
	         ocultar_error_layer()
	    }
	    $.post(rutaAjax,params,funcionExito);
    }
}

function generar_etiqueta(Id_activo){
    var params= new Object();	
        params.action="generar_etiqueta"
        params.Id_activo=Id_activo
    var funcionExito= function(resp){
        window.open('download_etiqueta.php?archivo=etiqueta_activo.zip', '_blank');
    }
    $.post(rutaAjax,params,funcionExito);
}




function mostrar_historial(Id){
 window.location="historial_activo.php?id="+Id+"&tipo=1" 
}



function buscarActivo(){
   var params= new Object()
       params.action="buscarActivo"
       params.buscar="%"
       if($.trim($('#buscar').val())){
       params.buscar=$('#buscar').val()
       }
   var funcionExito= function(resp){
	   $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	
}

function mostrar_botones(Obj){
   if($(Obj).next().is(":hidden")) {
	   $(Obj).next().slideDown("fast");
           $(Obj).html('Ocultar')
   } else {
	   $(Obj).next().slideUp("fast");
           $(Obj).html('Mostrar')
   }

}