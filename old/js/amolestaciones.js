var rutaAjax = "ajax/amolestaciones_aj.php"
function buscar_int(){
 if($.trim($('#email_int').val())){
       var params= new Object()
           params.action="buscar_int"
           params.buscar=$('#email_int').val()
      var funcionExito =function(resp){
          $('#buscador_int').html(resp) 
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      $('#buscador_int').html('')
  }
}


function mostrar_box_amolestacion(){
    var params= new Object();	
        params.action="mostrar_box_amolestacion"
        params.Id_alum=$('#Id_alum').val()
        params.Id_ciclo=$('#ciclo option:selected').val()
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}



function delete_rep(Id_rep){
   if(confirm("¿Está seguro de eliminar la amolestación?")){
        var params= new Object();	
            params.action="delete_rep"
            params.Id_rep=Id_rep
            params.Id_amolestado=$('#Id_amolestado').val()
            params.Tipo_amolestado=$('#Tipo_amolestado').val()
        var funcionExito= function(resp){
            $('#tabla').html(resp)
        }
        $.post(rutaAjax,params,funcionExito);
    }
}

function add_amolestacion(Id_rep){
  var params= new Object()
       params.action="add_amolestacion"
       params.Id_amolestado=$('#Id_amolestado').val()
       params.Tipo_amolestado=$('#Tipo_amolestado').val()
       params.Id_amolesta=$('.buscarFiltro').attr('id-data')
       params.Tipo_amolesta=$('.buscarFiltro').attr('tipo')
       params.Id_ciclo=$('#Id_ciclo option:selected').val()
       params.Id=$('#Id_docente option:selected').val()
       params.Fecha_rep=$('#fecha_reporte').val()
       params.Motivo=$('#motivo').val()
       params.Id_rep=Id_rep
  if($('#fecha_reporte').val().length>0 && $('#Id_ciclo option:selected').val()>0 &&$('.buscarFiltro').attr('id-data')>0 
          && $('#motivo').val().length>0 && $('#Id_amolestado').val()>0){
      var funcionExito =function(resp){
         $('#tabla').html(resp)
         ocultar_error_layer();
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      alert("Completa los campos faltantes")
  }
}

function mostrar_botones(Obj){
   if($(Obj).next().is(":hidden")) {
	   $(Obj).next().slideDown("fast");
           $(Obj).html('Ocultar')
   } else {
	   $(Obj).next().slideUp("fast");
           $(Obj).html('Mostrar')
   }

}


function buscarReporta(){
   if($.trim($('.buscarFiltro').val())){
   var params= new Object()
       params.action="buscarReporta"
       params.buscar="%"
       if($.trim($('.buscarFiltro').val())){
       params.buscar=$('.buscarFiltro').val()
       }
   var funcionExito= function(resp){
      $('.UlbuscadorReporta').html(resp)
      $('.UlbuscadorReporta li').on("click", function(event){
          $('.buscarFiltro').val($(this).text())
          $('.buscarFiltro').attr('id-data',$(this).attr('id-data'))
          $('.buscarFiltro').attr('tipo',$(this).attr('tipo_usu'))
          $('.UlbuscadorReporta').html('')
       });
   }
   $.post(rutaAjax,params,funcionExito);	
   }else{
      $('.UlbuscadorReporta').html('')
      $('.buscarFiltro').removeAttr('id-data')
      $('.buscarFiltro').removeAttr('tipo_usu')
  }	
}

