var rutaAjax = "ajax/reporte_pagos_proximos_aj.php"
$(window).ready(function(){
    $(".boxfil").draggable();
})



function filtro(Obj){
   var params= new Object()
       params.action="filtro"
       params.Id_alum=$('.buscarFiltro').attr('id_alum')
       params.fecha_ini=$('#fecha_ini').val()
       params.fecha_fin=$('#fecha_fin').val()
       $(Obj).text('Buscando...')
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	

}



function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Tipo=$('#tipo option:selected').val()
       params.Id_alum=$('.buscarFiltro').attr('id_alum')
       params.fecha_ini=$('#fecha_ini').val()
       params.fecha_fin=$('#fecha_fin').val()
       params.Id_oferta=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
   var query=$.param( params );
   window.open(rutaAjax+"?"+query, '_blank');
}


function buscarAlum(Obj){
  if($.trim($(Obj).val()) && $('.buscarFiltro').val().length>=4){
   var params= new Object()
       params.action="buscarAlum"
       params.buscar="%"
       if($.trim($(Obj).val())){
           params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id_alum',$(this).attr('id_alum'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post(rutaAjax,params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id_alum')
      $(Obj).attr( 'id_alum','0')
  }
}


