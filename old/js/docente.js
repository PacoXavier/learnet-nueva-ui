function mostrarFinder(){
    $('#files').click()
}  
if($('#files').length){
        document.getElementById('files').addEventListener('change', handleFileSelect, false);
}
  
function handleFileSelect(evt) {
    //Check for the various File API support.
	if (window.File && window.FileReader && window.FileList && window.Blob) {
	    // Great success! All the File APIs are supported.

	    // FileList object
		var files = evt.target.files; 
		
		var count=0;
		var bin,name,type,size
		
		//Funcion loadStartImg
		var loadStartImg=function() { 
			//alert("inicia") 
		}
		
		//Funcion loadEndImg
		var loadEndImg=function(e) {
			var xhr
			if(window.XMLHttpRequest){
				 xhr = new XMLHttpRequest();
			}else if(window.ActiveXObject){
				 xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
			//var bin = reader.result;
			
			// progress bar loadend
			var eventSource = xhr.upload || xhr;
			    eventSource.addEventListener("progress", function(e) {  
			var pc = parseInt((e.loaded / e.total * 100));  
	            //$('#mascara_img span').html(pc+'%') 
	             var mns='Cargando ...'+pc+'%'
                 alerta(mns);
                 if(pc==100){
	                ocultar_error_layer() 
                 }
			}, false); 
				            
			xhr.onreadystatechange=function(){
				if(xhr.readyState==4 && xhr.status==200){
				  $('.foto_alumno').attr('style','background-image:url(files/'+xhr.responseText+'.jpg)')
				}
			}
			xhr.open('POST', 'docente_aj.php?action=upload_image&Id_usu='+$('#Id_usu').val(), true);
			var boundary = 'xxxxxxxxx';
			var body = '--' + boundary + "\r\n";  
			body += "Content-Disposition: form-data; name='upload'; filename='" + name + "'\r\n";  
			body += "Content-Type: application/octet-stream\r\n\r\n";  
			body += bin + "\r\n";  
			body += '--' + boundary + '--';      
			xhr.setRequestHeader('content-type', 'multipart/form-data; boundary=' + boundary);
			// Firefox 3.6 provides a feature sendAsBinary ()
			if(xhr.sendAsBinary != null) { 
				xhr.sendAsBinary(body); 
			// Chrome 7 sends data but you must use the base64_decode on the PHP side
			} else {
				xhr.open('POST', 'docente_aj.php?action=upload_image&Id_usu='+$('#Id_doc').val()+'&base64=ok&FileName='+name+'&TypeFile='+type, true);
				xhr.setRequestHeader('UP-FILENAME', name);
				xhr.setRequestHeader('UP-SIZE', size);
				xhr.setRequestHeader('UP-TYPE', type);
				xhr.send(window.btoa(bin));
			}
			if(status) {
			   //document.getElementById(status).innerHTML = '';
			}	
		}
		
		//Funcion loadErrorImg
		var loadErrorImg=function(evt) {
			switch(evt.target.error.code) {
			  case evt.target.error.NOT_FOUND_ERR:
			    alert('File Not Found!');
			    break;
			  case evt.target.error.NOT_READABLE_ERR:
			    alert('File is not readable');
			    break;
			  case evt.target.error.ABORT_ERR:
			    break; // noop
			  default:
			    alert('An error occurred reading this file.');
			};
		}
		
	    //Loop through the FileList and render image files as thumbnails.
		for (var i = 0, f; f = files[i]; i++) {
			  // Only process image files.
			  if (!f.type.match('image.*')) {
			    continue;
			  }		 
			  
			  var reader = new FileReader();
			  var preview = new FileReader();
			     
			  // Closure to capture the file information.
			  reader.onload = (function(theFile) {
			    return function(e) {
			      // Render thumbnail binari.
			      bin =e.target.result
			      name=theFile.name
			      type=theFile.type
			      size=theFile.size
			    };
			  })(f);
			  
			  preview.onload = (function(theFile) {
			    return function(e) {
			      // Render thumbnail.
			      /*
			      var li = document.createElement('li');
			          $(li).attr('id','img_'+count)
			          li.innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/><div id="mascara_img"><span></span></div>'].join('');
			      $('#visor_subir_img').append(li)
			      $('#visor_subir_img li img').css({'width':'85px','height':'85px'});
			      $('#ruta_img').val($('#files').val())
			      count++;
			      */
			    };
			  })(f);
			  
			  //Read in the image file as a binary string.
			  reader.readAsBinaryString(f);
			  //Read in the image file as a data URL.
			  preview.readAsDataURL(f);
			 	      
			  // Firefox 3.6, WebKit
			  if(reader.addEventListener) { 
					reader.addEventListener('loadend', loadEndImg, false);
					reader.addEventListener('loadstart', loadStartImg, false);
					if(status != null) {
						reader.addEventListener('error', loadErrorImg, false);
					}
			  // Chrome 7
			  } else { 
					reader.onloadend = loadEndImg;
					reader.onloadend = loadStartImg;
					if (status != null) {
						reader.onerror = loadErrorImg;
					}
			  }
		}
	  
	} else {
	   alert('The File APIs are not fully supported in this browser.');
	}
}

/*
function box_perm(){
    var params= new Object();	
        params.action="box_perm"
        params.Id_usu=$('#Id_usu').val()
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post("docente_aj.php",params,funcionExito);
}
function save_perm(){
	var params= new Object();	
        params.action="save_perm"
        params.Id_usu=$('#Id_usu').val()
        params.perm=new Array();
        $('input[type="checkbox"]').each(function(){
	        if(!$(this).attr('onclick') && $(this).is(':checked')){
		      params.perm.push($(this).val())  ;
	        }
        })
    var funcionExito= function(resp){
	      ocultar_error_layer()
    }
    $.post("docente_aj.php",params,funcionExito);	
}
*/



/**/


function box_documents(){
    var params= new Object();	
        params.action="box_documents"
        params.Id_doc=$('#Id_doc').val()
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post("docente_aj.php",params,funcionExito);
}


function box_pass(){
    var params= new Object();	
        params.action="box_pass"
        params.Id_doc=$('#Id_doc').val()
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post("docente_aj.php",params,funcionExito);
}


function save_pass(){
    var params= new Object();	
        params.action="save_pass"
        params.Id_doc=$('#Id_doc').val()
        params.pass=$('#pass').val()
        params.conf=$('#conf').val()
    var funcionExito= function(resp){
	    if(resp.tipo=="password_match"){
		   alert("Las contraseñas no coinciden")
	    }else if(resp.tipo=="password_security"){
		   alert("Elige una contraseña con por lo menos 6 carácteres")
	    }if(resp.tipo=="ok"){
	       alert("Contraseña reestablecida con éxito")
	       ocultar_error_layer()
	    }
    }
    $.post("docente_aj.php",params,funcionExito,"json");
}

function save_documentos_doc(){
    var params= new Object();	
        params.action="save_documentos_doc"
        params.Id_doc=$('#Id_doc').val()
        
        params.doc_RFC= 0
        if($('#doc_RFC').is(":checked")){
	        params.doc_RFC= 1
        }
        params.copy_RFC= 0
        if($('#copy_RFC').is(":checked")){
	        params.copy_RFC= 1
        }
        
        params.doc_CURP= 0
        if($('#doc_CURP').is(":checked")){
	        params.doc_CURP= 1
        }
        params.copy_CURP= 0
        if($('#copy_CURP').is(":checked")){
	        params.copy_CURP= 1
        }
        
        params.doc_Curriculum= 0
        if($('#doc_Curriculum').is(":checked")){
	        params.doc_Curriculum= 1
        }
        params.copy_Curriculum= 0
        if($('#copy_Curriculum').is(":checked")){
	        params.copy_Curriculum= 1
        }

        params.doc_CompDomicilio= 0
        if($('#doc_CompDomicilio').is(":checked")){
	        params.doc_CompDomicilio= 1
        }
        params.copy_CompDomicilio= 0
        if($('#copy_CompDomicilio').is(":checked")){
	        params.copy_CompDomicilio= 1
        }
        
        params.doc_Contrato= 0
        if($('#doc_Contrato').is(":checked")){
	        params.doc_Contrato= 1
        }
        params.copy_Contrato= 0
        if($('#copy_Contrato').is(":checked")){
	        params.copy_Contrato= 1
        }

        params.doc_CompEstudios= 0
        if($('#doc_CompEstudios').is(":checked")){
	        params.doc_CompEstudios= 1
        }
        params.copy_CompEstudios= 0
        if($('#copy_CompEstudios').is(":checked")){
	        params.copy_CompEstudios= 1
        }

    var funcionExito= function(resp){
	    ocultar_error_layer()
    }
    $.post("docente_aj.php",params,funcionExito);	
}


function save_usu(){
    var params= new Object();	
        params.action="save_usu"
        params.Id_doc=$('#Id_doc').val()
        params.Id_dir=$('#Id_dir').val()
        params.nombre_usu=$('#nombre_usu').val()
        params.apellidoP_usu=$('#apellidoP_usu').val()
        params.apellidoM_usu=$('#apellidoM_usu').val()
        params.email_usu=$('#email_usu').val()
        params.tel_usu=$('#tel_usu').val()
        params.cel_usu=$('#cel_usu').val()

        params.escolaridad=$('#escolaridad option:selected').val()
        params.tipo_usu=$('#tipo_usu option:selected').val()
        params.cel_usu=$('#cel_usu').val()
        params.tiempo_completo=0;
        if($('#tiempo_completo').is(':checked')){
           params.tiempo_completo=1; 
        }
        
        params.calle_docen=$('#calle_docen').val()
        params.numInt_docen=$('#numInt_docen').val()
        params.numExt_docen=$('#numExt_docen').val()
        params.colonia_docen=$('#colonia_docen').val()
        params.cp_docen=$('#cp_docen').val()
        params.ciudad_docen=$('#ciudad_docen').val()
        params.estado_docen=$('#estado_docen').val()
        
        params.Id_contacto=$('#Id_contacto').val()
        params.nombre_contacto=$('#nombre_contacto').val()
        params.parentesco_contacto=$('#parentesco_contacto').val()
        params.direccion_contacto=$('#direccion_contacto').val()
        params.tel_contacto=$('#tel_contacto').val()
        params.telOfic_contacto=$('#telOfic_contacto').val()
        params.celular_contacto=$('#celular_contacto').val()
        params.email_contacto=$('#email_contacto').val()
        params.tipo_sangre=$('#tipo_sangre').val()
        params.alergias_contacto=$('#alergias_contacto').val()
        params.cronicas_contacto=$('#cronicas_contacto').val()
        params.preinscripciones_medicas_contacto=$('#preinscripciones_medicas_contacto').val()
        
    if($('#calle_docen').val().length>0 && $('#numExt_docen').val().length>0 && $('#ciudad_docen').val().length>0){   
	    if($('#nombre_usu').val().length>0 && $('#email_usu').val().length>0 && 
	    $('#escolaridad option:selected').val()>0){
	    var funcionExito= function(resp){
	            
	            if (resp.tipo == "new") {
	                window.location = "docente.php?id=" + resp.id
	            } else {
	                update_pag(resp.id)
	                var mns = 'Datos guardados con &eacute;xito'
	                alerta(mns);
	            }
	
	    }
	    $.post("docente_aj.php",params,funcionExito,"json");
	    }else{
		   alerta("Completa los campos faltantes"); 
	    }
    }else{
	    alerta("Completa los campos de la dirección"); 
    }
}


function update_pag(Id_usu){
    var params= new Object();	
        params.action="update_pag"
        params.Id_usu=Id_usu
    var funcionExito= function(resp){
        $('#contenido').html(resp)
    }
    $.post("docente_aj.php",params,funcionExito);
}







