var rutaAjax = "ajax/disponibilidad_docente_aj.php"

function buscar_int(){
 if($.trim($('#email_int').val())){
       var params= new Object()
           params.action="buscar_int"
           params.buscar=$('#email_int').val()
      var funcionExito =function(resp){
          $('#buscador_int').html(resp) 
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      $('#buscador_int').html('')
  }
}

function save_disponibilidad(){
  var params= new Object()
     params.action="save_disponibilidad"
     params.Id_docente=$('#Id_docente').val()
     params.Id_ciclo=$('#ciclo option:selected').val()
     params.Disponibilidad=new Array();
     $('#list_usu tbody tr').each(function(){
        var dias= new Object();
           dias.hora=$(this).attr('id_hora')
           dias.lunes=0
           if($(this).find('.L').is(':checked')){
             dias.lunes=1  
           }
           dias.martes=0
           if($(this).find('.M').is(':checked')){
             dias.martes=1  
           }
           dias.miercoles=0
           if($(this).find('.I').is(':checked')){
             dias.miercoles=1  
           }
           dias.jueves=0
           if($(this).find('.J').is(':checked')){
             dias.jueves=1  
           }
           dias.viernes=0
           if($(this).find('.V').is(':checked')){
             dias.viernes=1  
           }
           dias.sabado=0
           if($(this).find('.S').is(':checked')){
             dias.sabado=1  
           }
           dias.domingo=0
           if($(this).find('.D').is(':checked')){
             dias.domingo=1  
           }
           
           if(dias.lunes==1 || dias.martes==1 || dias.miercoles==1 || dias.jueves==1 || dias.viernes==1 || dias.sabado==1 || dias.domingo==1){
             params.Disponibilidad.push(dias)
           }
     })
  if(params.Disponibilidad.length>0 && $('#ciclo option:selected').val()>0){
  var funcionExito =function(resp){
      alerta("Datos guardados con éxito")
  }
  $.post(rutaAjax,params,funcionExito);
  }else{
      alerta("Selecciona la disponibilidad para el docente")
  }

}


function mostrar_disponibilidad_ciclo(Obj){
    var params= new Object();	
        params.action="mostrar_disponibilidad_ciclo"
        params.Id_ciclo=$('#ciclo option:selected').val()
        params.Id_docente=$('#Id_docente').val()
    var funcionExito= function(resp){
        $('#box_disponibilidad').html(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}


function seleccionarCheckbox(){
    if($('#seleccionar-todo').is(':checked')){
       $('.checkbox-table').prop('checked','checked');
    }else{
        $('.checkbox-table').prop('checked','');
    }
}
