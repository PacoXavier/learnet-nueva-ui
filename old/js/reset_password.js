var rutaAjax = "ajax/reset_password_aj.php"

function mostrar_error(tipo_error) {
    var error = ""
    if (tipo_error == "password_match") {
        error = 'LAS CONTRASEÑAS NO COINCIDEN';
    } else if (tipo_error == "password_security") {
        error = '<span>POR FAVOR ELIGE UNA CONTRASEÑA CON<br>POR LO MENOS 6 CARÁCTERES, NÚMEROS Y LETRAS</span>'
    }
    if (tipo_error == "ok") {
        error = 'CONTRASEÑA REESTABLECIDA CON ÉXITO'
    }

    $('.error').html(error)
    $('.error').css('visibility', 'visible')
    $('.error').animate({
        opacity: 1
    }, 500, function () {
        if (tipo_error == "ok") {
            window.location = "index.php"
        }
    })
}


function reset_password(tipo) {
    var params = new Object()
    params.action = "reset_password"
    params.pass = $('#pass').val()
    params.confir = $('#confir').val()
    params.key = $('#key').val()
    params.tipo = tipo
    if ($('#pass').val() == $('#confir').val()) {
        if ($('#pass').val().length >= 6) {
            var functionExito = function (resp) {
                if (resp.status == "sent") {
                    mostrar_error('ok')
                }
            }
            $.post(rutaAjax, params, functionExito, "json")

        } else {
            mostrar_error('password_security')
        }

    } else {
        mostrar_error('password_match')
    }
}
