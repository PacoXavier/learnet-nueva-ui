var rutaAjax = "ajax/reporte_calificaciones_grupo_aj.php"
$(window).ready(function(){
    $(".boxfil").draggable();
})



function filtro(Obj){
   $(Obj).text('Buscando...')
   var params= new Object()
       params.action="filtro"
       params.Id_docen=$('#Id_docente').attr('id-data')
       params.Id_grupo=$('#Id_grupo').attr('id-data')
       params.Id_turno=$('#turno option:selected').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Id_mat=$('#lista_materias option:selected').val()
       params.Id_ori=$('#orientaciones option:selected').val()
   if($('#ciclo option:selected').val()>0 && ($('#Id_docente').attr('id-data')>0 || $('#Id_grupo').attr('id-data')>0 || $('#lista_materias option:selected').val()>0)){
       var funcionExito= function(resp){
         $(Obj).text('Buscar')
         $('.table tbody').html(resp)
       }
       $.post(rutaAjax,params,funcionExito);	
   }else{
       alert("Completa los campos faltantes")
   }
}

function verificar_orientaciones(){
    var params= new Object();	
        params.action="verificar_orientaciones"
        params.Id_mat_esp=$('#lista_materias option:selected').attr('id_mat_esp')
    if($('#lista_materias option:selected').attr('id_mat_esp')>0){
    var funcionExito= function(resp){
        $('#box_orientacion').html(resp)
    }
    $.post(rutaAjax,params,funcionExito);
    }
}


function buscarDocent(Obj){
   if($.trim($(Obj).val()) && $(Obj).val().length>=4){
   var params= new Object()
       params.action="buscarDocent"
       params.buscar="%"
       params.Id_ciclo=$('#ciclo option:selected').val()
       if($.trim($(Obj).val())){
       params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id-data',$(this).attr('id-data'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post('index_aj.php',params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id-data')
  }
}


function buscarGruposCiclo(Obj){
   if($.trim($(Obj).val()) && $(Obj).val().length>=4){
   var params= new Object()
       params.action="buscarGruposCiclo"
       params.buscar="%"
       params.Id_ciclo=$('#ciclo option:selected').val()
       if($.trim($(Obj).val())){
       params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id-data',$(this).attr('id-data'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post('index_aj.php',params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id-data')
  }
}


