var action
var rutaAjax = "ajax/plantel_aj.php"

function mostrarFinder(tipo) {
    if (tipo == "logo") {
        action = "upload_image"
    } else if (tipo == "pdf") {
        action = "upload_formato_pago"
    }else if(tipo == "pdf_eventos"){
        action= "upload_formato_pago_evento"
    }else if(tipo == "etiqueta_libro"){
        action= "upload_etiqueta_libro"
    }else if(tipo == "etiqueta_activo"){
        action= "upload_etiqueta_activo"
    }else if(tipo == "formato_inscripcion"){
        action= "upload_formato_inscripcion"
    }
    $('#files').click()
}
if ($('#files').length) {
    document.getElementById('files').addEventListener('change', handleFileSelect, false);
}


function handleFileSelect(evt) {
    //Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // Great success! All the File APIs are supported.

        // FileList object
        var files = evt.target.files;

        var count = 0;
        var bin, name, type, size

        //Funcion loadStartImg
        var loadStartImg = function () {
            //alert("inicia") 
        }

        //Funcion loadEndImg
        var loadEndImg = function (e) {
            var xhr
            if (window.XMLHttpRequest) {
                xhr = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
            //var bin = reader.result;

            // progress bar loadend
            var eventSource = xhr.upload || xhr;
            eventSource.addEventListener("progress", function (e) {
                var pc = parseInt((e.loaded / e.total * 100));
                //$('#mascara_img span').html(pc+'%') 
                var mns = 'Cargando ...' + pc + '%'
                alerta(mns);
                if (pc == 100) {
                    ocultar_error_layer()
                }
            }, false);

            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                        if (action == "upload_image") {
                            $('#box-logo').attr('style', 'background-image:url(files/' + xhr.responseText + '.jpg)')
                        } else if (action == "upload_formato_pago") {
                            $('#formato_pago').attr('src', 'files/' + xhr.responseText + '.pdf')
                        }else if (action == "upload_formato_pago_evento") {
                            $('#formato_pago_evento').attr('src', 'files/' + xhr.responseText + '.pdf')
                        }if (action == "upload_etiqueta_libro") {
                            $('#box-eti-libro').attr('style', 'background-image:url(files/' + xhr.responseText + '.jpg)')
                        }if (action == "upload_etiqueta_activo") {
                            $('#box-eti-activo').attr('style', 'background-image:url(files/' + xhr.responseText + '.jpg)')
                        }if (action == "upload_formato_inscripcion") {
                            $('#formato_inscripcion').attr('src', 'files/' + xhr.responseText + '.pdf')
                        }

                        $('#files').val('')
                }
            }
            
            xhr.open('POST', rutaAjax+'?action='+action+'&Id_plantel=' + $('#Id_plantel').val(), true);
            var boundary = 'xxxxxxxxx';
            var body = '--' + boundary + "\r\n";
            body += "Content-Disposition: form-data; name='upload'; filename='" + name + "'\r\n";
            body += "Content-Type: application/octet-stream\r\n\r\n";
            body += bin + "\r\n";
            body += '--' + boundary + '--';
            xhr.setRequestHeader('content-type', 'multipart/form-data; boundary=' + boundary);
            // Firefox 3.6 provides a feature sendAsBinary ()
            if (xhr.sendAsBinary != null) {
                xhr.sendAsBinary(body);
                // Chrome 7 sends data but you must use the base64_decode on the PHP side
            } else {
                xhr.open('POST', rutaAjax+'?action='+action+'&Id_plantel=' + $('#Id_plantel').val() + '&base64=ok&FileName=' + name + '&TypeFile=' + type, true);
                xhr.setRequestHeader('UP-FILENAME', name);
                xhr.setRequestHeader('UP-SIZE', size);
                xhr.setRequestHeader('UP-TYPE', type);
                xhr.send(window.btoa(bin));
            }
            if (status) {
                //document.getElementById(status).innerHTML = '';
            }
        }

        //Funcion loadErrorImg
        var loadErrorImg = function (evt) {
            switch (evt.target.error.code) {
                case evt.target.error.NOT_FOUND_ERR:
                    alert('File Not Found!');
                    break;
                case evt.target.error.NOT_READABLE_ERR:
                    alert('File is not readable');
                    break;
                case evt.target.error.ABORT_ERR:
                    break; // noop
                default:
                    alert('An error occurred reading this file.');
            }
            ;
        }

        //Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {
            /*
            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }
                            */

            var reader = new FileReader();
            var preview = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail binari.
                    bin = e.target.result
                    name = theFile.name
                    type = theFile.type
                    size = theFile.size
                };
            })(f);

            preview.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    /*
                     var li = document.createElement('li');
                     $(li).attr('id','img_'+count)
                     li.innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/><div id="mascara_img"><span></span></div>'].join('');
                     $('#visor_subir_img').append(li)
                     $('#visor_subir_img li img').css({'width':'85px','height':'85px'});
                     $('#ruta_img').val($('#files').val())
                     count++;
                     */
                };
            })(f);

            //Read in the image file as a binary string.
            reader.readAsBinaryString(f);
            //Read in the image file as a data URL.
            preview.readAsDataURL(f);

            // Firefox 3.6, WebKit
            if (reader.addEventListener) {
                reader.addEventListener('loadend', loadEndImg, false);
                reader.addEventListener('loadstart', loadStartImg, false);
                if (status != null) {
                    reader.addEventListener('error', loadErrorImg, false);
                }
                // Chrome 7
            } else {
                reader.onloadend = loadEndImg;
                reader.onloadend = loadStartImg;
                if (status != null) {
                    reader.onerror = loadErrorImg;
                }
            }
        }

    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
}

function save_plantel() {
    var params = new Object();
    params.action = "save_plantel"
    params.Id_plantel = $('#Id_plantel').val()
    params.Id_dir = $('#Id_dir').val()
    params.Nombre = $('#nombre').val()
    params.Clave = $('#clave').val()
    params.Telefono = $('#tel').val()
    params.abreviatura = $('#abreviatura').val()
    params.email = $('#email').val()
    params.dominio = $('#dominio').val()

    params.calle = $('#calle').val()
    params.numInt = $('#numInt').val()
    params.numExt = $('#numExt').val()
    params.colonia = $('#colonia').val()
    params.cp = $('#cp').val()
    params.ciudad = $('#ciudad').val()
    params.estado = $('#estado').val()
    
    params.Banco = $('#Banco').val()
    params.Beneficiario = $('#Beneficiario').val()
    params.Convenio = $('#Convenio').val()
    params.Transferencias = $('#Transferencias').val()
    params.RFC = $('#RFC').val()
    params.Id_dato_banc= $('#Id_dato_banc').val()
    
    params.dias= new Array();
    $('.dias').each(function(){
        if($(this).is(':checked')){
            params.dias.push($(this).val());
        }
    })

    if ($('#nombre').val().length > 0 && $('#email').val().length > 0 && $('#dominio').val().length > 0 && params.dias.length>0) {
        var funcionExito = function (resp) {
            if (resp.tipo == "new") {
                window.location = "plantel.php?id=" + resp.id
            } else {
                update_pag(resp.id)
                alert('Datos guardados con éxito');
            }

        }
        $.post(rutaAjax, params, funcionExito, "json");
    } else {
        alert("Completa los campos faltantes");
    }
}



function update_pag(Id_plantel) {
    var params = new Object();
    params.action = "update_pag"
    params.Id_plantel = Id_plantel
    var funcionExito = function (resp) {
        $('#contenido').html(resp)
    }
    $.post(rutaAjax, params, funcionExito);
}



function mostrar_box_ofertas() {
    var params = new Object();
    params.action = "mostrar_box_ofertas"
    params.Id_plantel = $('#Id_plantel').val()
    var funcionExito = function (resp) {
        mostrar_error_layer(resp);
    }
    $.post(rutaAjax, params, funcionExito);
}




function save_ofertas_plantel() {
    var params = new Object();
    params.action = "save_ofertas_plantel"
    params.Id_plantel = $('#Id_plantel').val()
    params.Ofertas = new Array()
    $('#list-ofertas li').each(function () {
        if ($(this).find('input[type="checkbox"]').is(":checked")) {
            params.Ofertas.push($(this).find('input[type="checkbox"]').val())
        }
    })
    if (params.Ofertas.length > 0) {
        var funcionExito = function (resp) {
            $('#listaOfertas tbody').html(resp)
            ocultar_error_layer()
        }
        $.post(rutaAjax, params, funcionExito);
    } else {
        alert("Selecciona las ofertas del plantel");
    }
}



