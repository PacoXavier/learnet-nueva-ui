var rutaAjax = "ajax/materia_aj.php"
function save_materia() {
    var params = new Object();
    params.action = "save_materia"
    params.Id_mat = $('#Id_mat').val()
    params.Nombre = $('#Nombre').val()
    params.Clave_mat = $('#Clave_mat').val()
    params.Precio = $('#Precio').val()
    params.Creditos = $('#Creditos').val()
    params.Dias_porSemana = $('#Dias_porSemana').val()
    params.Horas_porSemana = $('#Horas_porSemana').val()
    params.Horas_conDocente = $('#Horas_conDocente').val()
    params.Horas_independientes = $('#Horas_independientes').val()
    params.Tipo_aula = $('#Tipo_aula option:selected').val()
    params.Promedio_min = $('#Promedio_min').val()
    params.Valor_cadaEvaluacion = $('#Valor_cadaEvaluacion').val()
    params.Duracion_examen = $('#Duracion_examen').val()
    params.Max_alumExamen = $('#Max_alumExamen').val()


    if ($('#Nombre').val().length > 0 && $('#Clave_mat').val().length > 0 && $('#Precio').val().length > 0 && $('#Creditos').val().length > 0) {
        var funcionExito = function (resp) {
            if (resp.tipo == "new") {
                window.location = "materia.php?id_esp="+$('#Id_esp').val()+"&id_mat=" + resp.id
            } else {
                update_pag(resp.id)
                alert('Datos guardados con éxito');
            }
        }
        $.post(rutaAjax, params, funcionExito, "json");
    } else {
        alert("Completa los campos faltantes");
    }
}



function buscar_mat(Obj) {
    if ($.trim($(Obj).closest('.oferta').find('.buscarMateria').val())) {
        var params = new Object()
        params.action = "buscar_mat"
        params.buscar = $(Obj).closest('.oferta').find('.buscarMateria').val()
        params.Id_esp = $(Obj).closest('.oferta').find('.especialidades option:selected').val()
        if ($(Obj).closest('.oferta').find('.especialidades option:selected').val() > 0) {
            var funcionExito = function (resp) {
                $(Obj).closest('.oferta').find('.buscador_int').html(resp)
                $(Obj).closest('.oferta').find('.buscador_int li').on("click", function (event) {

                    $(Obj).closest('.oferta').find('.buscarMateria').val($(this).text())
                    $(Obj).closest('.oferta').find('.buscarMateria').attr('id_mat_esp', $(this).attr('id_mat_esp'))
                    $(Obj).closest('.oferta').find('.buscador_int').html('')
                });

            }
            $.post(rutaAjax, params, funcionExito);
        } else {
            alert("Selecciona una especialidad")
        }
    } else {
        $(Obj).closest('.oferta').find('.buscador_int').html('')
        $(Obj).closest('.oferta').find('.buscarMateria').attr('id_mat', '')
    }
}

function add_materia(Obj) {
    var params = new Object()
    params.action = "add_materia"
    params.Id_mat_esp = $(Obj).closest('.oferta').find('.buscarMateria').attr('id_mat_esp')
    if ($(Obj).closest('.oferta').find('.buscarMateria').attr('id_mat_esp') > 0) {
        var funcionExito = function (resp) {
            $(Obj).closest('.oferta').find('.list_usu tbody').append(resp)
            $(Obj).closest('.oferta').find('.buscarMateria').val('')
            $(Obj).closest('.oferta').find('.buscarMateria').attr('id_mat_esp', '')
        }
        $.post(rutaAjax, params, funcionExito);
    } else {
        alert("Selecciona una materia")
    }
}

function ver_materia(Id_mat) {
    window.location = "materia.php?&id_mat=" + Id_mat
}


function update_pag(Id_mat) {
    var params = new Object();
    params.action = "update_pag"
    params.Id_mat = Id_mat
    params.Id_esp = $('#Id_esp').val()
    var funcionExito = function (resp) {
        $('#contenido').html(resp)
    }
    $.post(rutaAjax, params, funcionExito);
}




function delete_pre(Id_mat_pre) {
    if (confirm("¿Está seguro de eliminar el requisito de la materia?")) {
        var params = new Object()
        params.action = "delete_pre"
        params.Id_mat_pre = Id_mat_pre
        params.Id_esp = $('#Id_esp').val()
        params.Id_mat = $('#Id_mat').val()
        var funcionExito = function (resp) {
            $('#contenido').html(resp)
            ocultar_error_layer()
        }
        $.post(rutaAjax, params, funcionExito);
    }
}

function delete_ofe_compartida(Id_mat_esp) {
    if (confirm("¿Está seguro de eliminar la oferta compartida?")) {
        var params = new Object()
        params.action = "delete_ofe_compartida"
        params.Id_mat_esp = Id_mat_esp
        params.Id_mat = $('#Id_mat').val()
        params.Id_esp = $('#Id_esp').val()
        var funcionExito = function (resp) {
            $('#contenido').html(resp)
            ocultar_error_layer()
        }
        $.post(rutaAjax, params, funcionExito);
    }
}

function delete_mat_add(Obj) {
    $(Obj).closest('tr').remove()
}




function delete_materia() {
    if (confirm("¿Está seguro de eliminar la materia?")) {
        var params = new Object()
        params.action = "delete_materia"
        params.Id_mat = $('#Id_mat').val()
        var funcionExito = function (resp) {
            window.location = "materia.php"
        }
        $.post(rutaAjax, params, funcionExito);
    }
}


function add_ponderacion(Obj) {
    var params = new Object()
    params.action = "add_ponderacion"
    params.Id_mat = $('#Id_mat').val()
    params.Nombre_eva = $(Obj).closest('.oferta').find('.nombre_ponderacion').val()
    params.ponderacion = $(Obj).closest('.oferta').find('.valor_ponderacion').val()
    if ($(Obj).closest('.oferta').find('.valor_ponderacion').val().length > 0) {
        var funcionExito = function (resp) {
            $(Obj).closest('.oferta').find('.list_evaluaciones tbody').append(resp)
            $(Obj).closest('.oferta').find('.nombre_ponderacion').val('')
            $(Obj).closest('.oferta').find('.valor_ponderacion').val('')
        }
        $.post(rutaAjax, params, funcionExito);
    } else {
        alert("Ingresa un porcentaje de ponderación")
    }
}


function delete_ponderacion(Id_eva) {
    if (confirm("¿Está seguro de eliminar la ponderación?")) {
        var params = new Object()
        params.action = "delete_ponderacion"
        params.Id_eva = Id_eva
        params.Id_esp = $('#Id_esp').val()
        params.Id_mat = $('#Id_mat').val()
        var funcionExito = function (resp) {
            $('#contenido').html(resp)
            ocultar_error_layer()
        }
        $.post(rutaAjax, params, funcionExito);
    }
}

function delete_pon_add(Obj) {
    $(Obj).closest('tr').remove()
}

function mostrar_ofe_compartida(Id_mat_esp) {
    var params = new Object();
    params.action = "mostrar_ofe_compartida"
    params.Id_mat_esp = Id_mat_esp
    var funcionExito = function (resp) {
        mostrar_error_layer(resp)
    }
    $.post(rutaAjax, params, funcionExito);
}


function update_curso(Obj) {
    var params = new Object();
    params.action = "update_curso"
    params.Id_oferta = $(Obj).closest('.oferta').find('.ofertas_compartidas option:selected').val()
    var funcionExito = function (resp) {
        $(Obj).closest('.oferta').find('.especialidades').html(resp)
        $(Obj).closest('.oferta').find('.buscarMateria').val('')
        $(Obj).closest('.oferta').find('.buscarMateria').attr('id_mat', '')
        update_grado(Obj);
    }
    $.post(rutaAjax, params, funcionExito);
}


function update_grado(Obj) {
    var params = new Object();
    params.action = "update_grado"
    params.Id_esp = $(Obj).closest('.oferta').find('.especialidades option:selected').val()
    var funcionExito = function (resp) {
        $(Obj).closest('.oferta').find('.Grado_mat').html(resp)
    }
    $.post(rutaAjax, params, funcionExito);
}



function save_materia_esp(Id_mat_esp, Obj) {
    var params = new Object();
    params.action = "save_materia_esp"
    params.Id_mat_esp = Id_mat_esp
    params.Id_mat = $('#Id_mat').val()
    params.Id_esp = $('.especialidades option:selected').val()
    params.Grado_mat = $('.Grado_mat option:selected').val()
    params.NombreDiferente = $('#nombre_especial').val()
    params.Evaluacion_extraordinaria = 0
    if ($('.Evaluacion_extraordinaria').is(':checked')) {
        params.Evaluacion_extraordinaria = 1
    }
    params.Evaluacion_especial = 0
    if ($('.Evaluacion_especial').is(':checked')) {
        params.Evaluacion_especial = 1
    }
    params.orientacion = 0
    if ($('.orientacion').is(':checked')) {
        params.orientacion = 1
    }
    params.evaluaciones = new Array();
    $(Obj).closest('.oferta').find('.list_evaluaciones tbody tr').each(function () {
        var eva = new Object()
        eva.Id_eva = $(this).attr('id_pon')
        eva.Nombre_eva = $(this).find('.Nombre_eva').val()
        eva.Ponderacion = $(this).find('.ponderacion').val()
        params.evaluaciones.push(eva)
    })

    params.materias = new Array();
    $(Obj).closest('.oferta').find('.list_usu tbody tr').each(function () {
        var pre = new Object()
        pre.Id_mat_esp_pre = $(this).attr('id_mat_esp')
        pre.Nombre_pre = $(this).find('.Nombre_pre').val()
        pre.Clave_mat = $(this).find('.Clave_mat').val()
        params.materias.push(pre)
    })

    if ($('.especialidades option:selected').val() > 0 && $('.Grado_mat option:selected').val() > 0 && params.evaluaciones.length > 0) {
        var funcionExito = function (resp) {
            update_pag(params.Id_mat)
            alert('Datos guardados con éxito');
            ocultar_error_layer()
        }
        $.post(rutaAjax, params, funcionExito);
    } else {
        alert("Completa los campos faltantes");
    }
}
