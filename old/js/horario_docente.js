var rutaAjax = "ajax/horario_docente_aj.php"

$(window).ready(function(){
    var url=$(location).attr('href')
    if(url.indexOf("id=")!=-1){
          var id=url.substr(url.indexOf("id=")+3)
          //show_horario_docente(id);
    }
})

function validarHorasChecadas(Obj){
    
    if($(Obj).is(":checked")){
        $('#countHorasPorMateria').val($('#countHorasPorMateria').val()*1+1)
    }else{
        $('#countHorasPorMateria').val($('#countHorasPorMateria').val()*1-1)
    }
    if(($('#countHorasPorMateria').val()*1)>($('#horasPorSemanaMateria').val()*1)){
        alert("Solo puede seleccionar "+$('#horasPorSemanaMateria').val()+" horas por semana para esta materia")
        $(Obj).prop('checked', false);
        $('#countHorasPorMateria').val($('#countHorasPorMateria').val()*1-1)
    }
}



function mostrar_horario(){
  var params= new Object()
       params.action="mostrar_horario"
       params.Id_docente=$('#Id_docente').val()
       params.Id_ciclo=$('#ciclo option:selected').val()
  var funcionExito =function(resp){
      $('#tabla').html(resp) 
  }
  $.post(rutaAjax,params,funcionExito);
}

function mostrar_box_horario(Id_grupo,Id_mat,Id_aula,Id_ciclo ){
  var params= new Object()
     params.action="mostrar_box_horario"
     params.Id_docente=$('#Id_docente').val()
     params.Id_grupo=Id_grupo
     params.Id_mat=Id_mat
     params.Id_aula=Id_aula
     params.Id_ciclo=Id_ciclo
  var funcionExito =function(resp){
      mostrar_error_layer(resp);
  }
  $.post(rutaAjax,params,funcionExito);
}

function buscar_int(){
 if($.trim($('#email_int').val())){
       var params= new Object()
           params.action="buscar_int"
           params.buscar=$('#email_int').val()
      var funcionExito =function(resp){
          $('#buscador_int').html(resp) 
      }
      $.post(rutaAjax,params,funcionExito);
  }else{
      $('#buscador_int').html('')
  }
}

function save_horario_materia(){
  var params= new Object()
     params.action="save_horario_materia"
     params.Id_docente=$('#Id_docente').val()
     params.Id_grupo=$('#list_grupos option:selected').val()
     params.Id_mat=$('#list_materias option:selected').val()
     params.Id_aula=$('#list_aulas option:selected').val()
     params.Id_ciclo=$('#ciclo option:selected').val()
     params.Horas_materia=new Array();
     $('#list_usu tbody tr').each(function(){
        var dias= new Object();
           dias.hora=$(this).attr('id_hora')
           dias.lunes=0
           if($(this).find('.L').is(':checked') && $(this).find('.L').attr('disabled')==undefined){
             dias.lunes=1  
           }
           dias.martes=0
           if($(this).find('.M').is(':checked') &&  $(this).find('.M').attr('disabled')==undefined){
             dias.martes=1  
           }
           dias.miercoles=0
           if($(this).find('.I').is(':checked') && $(this).find('.I').attr('disabled')==undefined){
             dias.miercoles=1  
           }
           dias.jueves=0
           if($(this).find('.J').is(':checked') && $(this).find('.J').attr('disabled')==undefined){
             dias.jueves=1  
           }
           dias.viernes=0
           if($(this).find('.V').is(':checked') && $(this).find('.V').attr('disabled')==undefined){
             dias.viernes=1  
           }
           dias.sabado=0
           if($(this).find('.S').is(':checked') && $(this).find('.S').attr('disabled')==undefined){
             dias.sabado=1  
           }
           dias.domingo=0
           if($(this).find('.D').is(':checked') && $(this).find('.D').attr('disabled')==undefined){
             dias.domingo=1  
           }
           if(dias.lunes==1 || dias.martes==1 || dias.miercoles==1 || dias.jueves==1 || dias.viernes==1 || dias.sabado==1 || dias.domingo==1){
             params.Horas_materia.push(dias)
           }
     })
  if($('#list_grupos option:selected').val()>0 && $('#list_aulas option:selected').val()>0){
  var funcionExito =function(resp){
      $('#tabla').html(resp) 
      ocultar_error_layer();
 
  }
  $.post(rutaAjax,params,funcionExito);
  }else{
    alerta("Completa los campos faltantes")
  }
}


function show_horario_aula(){
 var params= new Object()
     params.action="show_horario_aula"
     params.Id_docente=$('#Id_docente').val()
     params.Id_grupo=$('#list_grupos option:selected').val()
     params.Id_mat=$('#list_materias option:selected').val()
     params.Id_aula=$('#list_aulas option:selected').val()
     params.Id_ciclo=$('#ciclo option:selected').val()
  var funcionExito =function(resp){
   $('#box_emergente').html(resp)
  }
  $.post(rutaAjax,params,funcionExito);
}




function show_horario_docente(Id_docente){
 var params= new Object()
     params.action="show_horario_aula"
     params.Id_docente=Id_docente
     params.Id_grupo=0
     params.Id_aula=0
     params.Id_mat=0
     params.Id_ciclo=0
  var funcionExito =function(resp){
     $('#box_emergente').html(resp)
  }
  $.post(rutaAjax,params,funcionExito);
}
function get_materias_ciclo_docente(){
 var params= new Object()
     params.action="get_materias_ciclo_docente"
     params.Id_docente=$('#Id_docente').val()
     params.Id_ciclo=$('#ciclo option:selected').val()
  var funcionExito =function(resp){
     $('#list_materias').html(resp)
  }
  $.post(rutaAjax,params,funcionExito);
}


function get_grupos_ciclo_docente(){
 var params= new Object()
     params.action="get_grupos_ciclo_docente"
     params.Id_docente=$('#Id_docente').val()
     params.Id_ciclo=$('#ciclo option:selected').val()
     params.Id_mat=$('#list_materias option:selected').val()
  var funcionExito =function(resp){
    $('#list_grupos').html(resp)
  }
  $.post(rutaAjax,params,funcionExito);
}


function deleteGrupo(){
    if(confirm("¿Está seguro de eliminar el grupo?")){
     var params= new Object()
         params.action="deleteGrupo"
         params.Id_docente=$('#Id_docente').val()
         params.Id_grupo=$('#list_grupos option:selected').val()
         params.Id_aula=$('#list_aulas option:selected').val()
         params.Id_ciclo=$('#ciclo option:selected').val()
      var funcionExito =function(resp){
            if(resp.indexOf('id="column_one"')>0){
               $('#tabla').html(resp) 
                ocultar_error_layer();           
            }else{
                alert(resp)
            }
      }
      $.post(rutaAjax,params,funcionExito);
    }
}