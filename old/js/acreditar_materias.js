var rutaAjax = "ajax/acreditar_materias_aj.php"

function mostrar_box_materias(){
    var params= new Object();	
        params.action="mostrar_box_materias"
        params.Id_esp=$('#Id_esp').val()
        params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
        
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}


function update_materias_grado(){
    var params= new Object();	
        params.action="update_materias_grado"
        params.Id_esp=$('#Id_esp').val()
        params.Id_grado_ofe=$('#grado option:selected').val()
    var funcionExito= function(resp){
        $('#materia').html(resp)
    }
    $.post(rutaAjax,params,funcionExito);
}

function save_acreditacion(){
    var params= new Object();	
        params.action="save_acreditacion"
        params.Id_alum=$('#Id_alum').val()
        params.Id_esp=$('#Id_esp').val()
        params.Id_ciclo=$('#ciclo option:selected').attr('id-ciclo')
        params.Id_ciclo_alum=$('#ciclo option:selected').val()
        params.Id_mat_esp=$('#materia option:selected').val()
        params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
    if($('#ciclo option:selected').val()>0 && $('#grado option:selected').val()>0 && $('#materia option:selected').val()>0){
    var funcionExito= function(resp){
        if(resp.indexOf('column_one')>=0){
        $('#tabla').html(resp)
        ocultar_error_layer()
        }else{
            alert("La materia ya se encuentra acreditada")
        }
    }
    $.post(rutaAjax,params,funcionExito);
     }else{
         alert("Completa los campos faltantes")
     }
}


function delete_mat_ac(Id_mat_ac){
   if(confirm("¿Está seguro de eliminar la materia acreditada?, tenga en cuenta que se borrara el cargo asignado?")){
	    var params= new Object();	
	        params.action="delete_mat_ac"
	        params.Id_mat_ac=Id_mat_ac
                params.Id_oferta_alumno=$('#Id_oferta_alumno').val()
	    var funcionExito= function(resp){
            $('#tabla').html(resp)
            ocultar_error_layer()
	    }
	    $.post(rutaAjax,params,funcionExito);
    }
}




