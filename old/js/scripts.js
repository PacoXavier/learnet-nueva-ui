$(window).ready(function () {

    if ($('.mega-menu').length > 0) {
        $('.mega-menu').dcMegaMenu({
            rowItems: '3',
            speed: 'fast',
            effect: 'fade'
        });
    }
    var url = $(location).attr('href')
    url = url.substr(url.lastIndexOf("/") + 1)
    if (url == "home.php") {
        buscarNotificaciones();
    }


})
/*
 (function(){
 var notice = $.pnotify({
 title: 'Test notification',
 text: 'Click me to dismiss'
 }).click(function(e){
 if($(e.target).is('.ui-pnotify-closer *, .ui-pnotify-sticker *'))
 {
 return;
 }
 notice.pnotify_remove();
 });
 })();
 */


function buscarNotificaciones() {
    var params = new Object();
    params.action = "buscarNotificaciones_interesados"
    var funcionExito = function (resp) {
        /*
         if(resp.indexOf('<li>')!=-1){
         mostrar_notificaciones(resp)
         }
         */
        $.each(resp, function (key, val) {
            var notice = new PNotify({
                title: val.Titulo,
                text: val.Texto,
                styling: 'jqueryui',
                addclass: 'notificiaciones',
                type: 'info',
                icon: 'ui-icon ui-icon-locked',
                delay: 8000,
                buttons: {
                    sticker: val.Sticker,
                    closer: val.Close
                }
            })
            notice.get().css('cursor', 'pointer').click(function (e) {
                if ($(e.target).is('.ui-pnotify-closer *')) {
                    if (val.Tipo == "Interesados") {
                        readnot(val.Id_not)
                    }if (val.Tipo == "Masiva") {
                        readnotificacion(val.Id_not);
                    }if (val.Tipo == "Personalizada") {
                        readnotificacionPersonalizada(val.Id_not);
                    }
                }
            });
        })
    }
    $.post("index_aj.php", params, funcionExito, "json");
}
function mostrar_reportes() {
    $('#list_reportes').slideToggle(200, function () {
    })
}


function readnot(Id_log) {
    var params = new Object();
    params.action = "readnot"
    params.Id_log = Id_log
    var funcionExito = function (resp) {
        /*
         $('#list-noificaciones').css({'visibility':'hidden','opacity':'0'})
         $('#list-noificaciones').html(resp)
         $('#list-noificaciones').css('visibility','visible')
         $('#list-noificaciones').animate({
         opacity: 1
         }, 300);
         */
    }
    $.post("index_aj.php", params, funcionExito);
}

function readnotificacion(Id_not) {
    var params = new Object();
    params.action = "readnotificacion"
    params.Id_not = Id_not
    var funcionExito = function (resp) {
        //alert(resp)
    }
    $.post("index_aj.php", params, funcionExito);
}


function readnotificacionPersonalizada(Id_not) {
    var params = new Object();
    params.action = "readnotificacionPersonalizada"
    params.Id_not = Id_not
    var funcionExito = function (resp) {
        //alert(resp)
    }
    $.post("index_aj.php", params, funcionExito);
}






function mostrar_inte() {
    var heightTabla = $('#list_usu').height()
    $('#mascara_tabla').animate({
        height: heightTabla + 'px'
    }, 500, function () {
        $('#mostrar').html('Ocultar')
        $('#mostrar').attr('onClick', 'ocultar_inte()')

        $('html,body').animate({
            scrollTop: $(document).height()
        }, 500);

    })
}

function ocultar_inte() {
    $('#mascara_tabla').animate({
        height: '220px'
    }, 500, function () {
        $('#mostrar').html('Mostrar m&aacute;s')
        $('#mostrar').attr('onClick', 'mostrar_inte()')
        $('html,body').animate({
            scrollTop: 0
        }, 500);
    })
}

function mostrar_alerta(text) {
    $('#box_alerta p').html(text)
    $('#box_alerta').css('visibility', 'visible')
    $('#box_alerta').animate({
        opacity: 1
    }, 400, function () {

    })
}

function ocultar_alerta() {
    $('#box_alerta').animate({
        opacity: 0
    }, 400, function () {
        $('#box_alerta').css('visibility', 'hidden')
        $('#box_alerta p').html('')
    })
}


function mostrar_error_layer(text) {
    $('#errorLayer').html(text)
    $('#errorLayer').css('visibility', 'visible')
    $('#errorLayer').animate({
        opacity: 1
    }, 300);
}

function ocultar_error_layer() {
    $('#errorLayer').animate({
        opacity: 0
    }, 300, function () {
        $('#errorLayer').css('visibility', 'hidden')
        $('#errorLayer').html('')
    });
}


function alerta(text) {
    var div = '<div id="box_emergente"><h1>' + text + '</h1><p><button onclick="ocultar_error_layer()" class="button">Aceptar</button></p></div>'
    mostrar_error_layer(div);
}


function mostrar_filtro() {
    $('.boxfil').css('visibility', 'visible')
    $('.boxfil').animate({
        opacity: 1
    }, 400, function () {

    })
}

function ocultar_filtro() {
    $('.boxfil').animate({
        opacity: 0
    }, 400, function () {
        $('.boxfil').css('visibility', 'hidden')
    })
}


function mostrar_notificaciones(text) {
    $('#box-notificaciones').html(text)
    $('#box-notificaciones').css('visibility', 'visible')
    $('#box-notificaciones').animate({
        opacity: 1
    }, 500);
}

function ocultar_notificaciones() {
    $('#box-notificaciones').animate({
        opacity: 0
    }, 500, function () {
        $('#box-notificaciones').css('visibility', 'hidden')
        $('#box-notificaciones').html('')
    });
}

/********************************************/
/********************************************/

function update_curso_box_curso() {
    var params = new Object();
    params.action = "update_curso"
    params.Id_oferta = $('#oferta option:selected').val()
    var funcionExito = function (resp) {
        $('#curso').html(resp)
        $('#box_orientacion').html('')
        if ($('#grado').length > 0) {
            $('#grado').html('')
        }
        if ($('#lista_materias').length > 0) {
            $('#lista_materias').html('')
        }



    }
    $.post("index_aj.php", params, funcionExito);
}


function update_orientacion_box_curso() {
    var params = new Object();
    params.action = "update_orientacion_box_curso"
    params.Id_esp = $('#curso option:selected').val()
    var funcionExito = function (resp) {
        $('#box_orientacion').html(resp)
        if ($('#lista_materias').length > 0) {
            $('#lista_materias').html('')
        }
    }
    $.post("index_aj.php", params, funcionExito);
}


function update_grados_ofe() {
    var params = new Object();
    params.action = "update_grados_ofe"
    params.Id_esp = $('#curso option:selected').val()
    var funcionExito = function (resp) {
        $('#grado').html(resp)
        if ($('#lista_materias').length > 0) {
            $('#lista_materias').html('')
        }
    }
    $.post("index_aj.php", params, funcionExito);
}


var editor
function mostrar_box_email() {
    var params = new Object();
    params.action = "mostrar_box_email"
    var funcionExito = function (resp) {
        mostrar_error_layer(resp);

        // The instanceReady event is fired, when an instance of CKEditor has finished
        // its initialization.
        CKEDITOR.on('instanceReady', function (ev) {
        });
        CKEDITOR.replace('editor1', {
            height: '100px',
            enterMode: CKEDITOR.ENTER_P


        });
        editor = CKEDITOR.instances.editor1;

        CKEDITOR.instances.editor1.on('key', function (e) {
            if (e.data.keyCode === 13) {
                // do your enter handling
                //$('#boton-email').attr('onclick','send_email(this)')
                //alert(editor)
            }

        });

    }
    $.post("index_aj.php", params, funcionExito);
}


function mostrarFinder() {
    $('#files').click()
}

function delete_attac(Id_attc) {
    if (confirm("¿Está seguro de eliminar el archivo?")) {
        var params = new Object();
        params.action = "delete_attac"
        params.Id_attc = Id_attc
        var funcionExito = function (resp) {
            $('#list_archivos').html(resp)
        }
        $.post("index_aj.php", params, funcionExito);
    }
}


function send_email(Obj) {
    var params = new Object();
    params.action = "send_email"
    params.asunto = $('#asunto').val()
    params.mensaje = editor.getData()
    params.alumnos = new Array()
    $('.table tbody tr ').each(function () {
        if ($(this).find('input[type="checkbox"]').is(":checked")) {
            params.alumnos.push($(this).attr('id_alum'))
        }
    })
    $(Obj).html('Enviando..')
    $(Obj).removeAttr('onclick')
    if (params.alumnos.length > 0) {
        if ($('#asunto').val().length > 0 && params.mensaje.length > 0) {
            var funcionExito = function (resp) {
                ocultar_error_layer()
                //editor.destroy();
                $(Obj).html('Enviar')
                $(Obj).attr('onclick', 'send_email(this)')
            }
            $.post("index_aj.php", params, funcionExito);
        } else {
            alert("Completa los campos faltantes")
        }
    } else {
        alert("Selecciona a los alumnos")
    }
}



function marcar_alumnos() {
    if ($('#all_alumns').is(":checked")) {
        $('.table tbody tr  input[type="checkbox"]').prop('checked', true);
    } else {
        $('.table tbody tr  input[type="checkbox"]').prop('checked', false);
    }
}


function update_materias() {
    var params = new Object();
    params.action = "update_materias"
    params.Id_oferta = $('#oferta option:selected').val()
    params.Id_esp = $('#curso option:selected').val()
    var funcionExito = function (resp) {
        $('#lista_materias').html(resp)
    }
    $.post("index_aj.php", params, funcionExito);
}

function buscarAlum() {
    if ($.trim($('.buscarFiltro').val()) && $('.buscarFiltro').val().length >= 4) {
        var params = new Object()
        params.action = "buscarAlum"
        params.buscar = "%"
        if ($.trim($('.buscarFiltro').val())) {
            params.buscar = $('.buscarFiltro').val()
        }
        var funcionExito = function (resp) {
            $('.Ulbuscador').html(resp)
            $('.Ulbuscador li').on("click", function (event) {
                $('.buscarFiltro').val($(this).text())
                $('.buscarFiltro').attr('id_alum', $(this).attr('id_alum'))
                $('.Ulbuscador').html('')
            });
        }
        $.post('index_aj.php', params, funcionExito);
    } else {
        $('.Ulbuscador').html('')
        $('.buscarFiltro').removeAttr('id_alum')
    }
}



function buscarDocent() {
    if ($.trim($('.buscarFiltro').val()) && $('.buscarFiltro').val().length >= 4) {
        var params = new Object()
        params.action = "buscarDocent"
        params.buscar = "%"
        if ($.trim($('.buscarFiltro').val())) {
            params.buscar = $('.buscarFiltro').val()
        }
        var funcionExito = function (resp) {
            $('.Ulbuscador').html(resp)
            $('.Ulbuscador li').on("click", function (event) {
                $('.buscarFiltro').val($(this).text())
                $('.buscarFiltro').attr('id-data', $(this).attr('id-data'))
                $('.Ulbuscador').html('')
            });
        }
        $.post('index_aj.php', params, funcionExito);
    } else {
        $('.Ulbuscador').html('')
        $('.buscarFiltro').removeAttr('id-data')
    }
}

function buscarUsu() {
    if ($.trim($('.buscarFiltro').val()) && $('.buscarFiltro').val().length >= 4) {
        var params = new Object()
        params.action = "buscarUsu"
        params.buscar = "%"
        if ($.trim($('.buscarFiltro').val())) {
            params.buscar = $('.buscarFiltro').val()
        }
        var funcionExito = function (resp) {
            $('.Ulbuscador').html(resp)
            $('.Ulbuscador li').on("click", function (event) {
                $('.buscarFiltro').val($(this).text())
                $('.buscarFiltro').attr('id-data', $(this).attr('id-data'))
                $('.Ulbuscador').html('')
            });
        }
        $.post('index_aj.php', params, funcionExito);
    } else {
        $('.Ulbuscador').html('')
        $('.buscarFiltro').removeAttr('id-data')
    }
}

function buscarMateria() {
    if ($.trim($('.buscarFiltro').val()) && $('.buscarFiltro').val().length >= 4) {
        var params = new Object()
        params.action = "buscarMateria"
        params.buscar = "%"
        if ($.trim($('.buscarFiltro').val())) {
            params.buscar = $('.buscarFiltro').val()
        }
        var funcionExito = function (resp) {
            $('.Ulbuscador').html(resp)
            $('.Ulbuscador li').on("click", function (event) {
                $('.buscarFiltro').val($(this).text())
                $('.buscarFiltro').attr('id-data', $(this).attr('id-data'))
                $('.Ulbuscador').html('')
            });
        }
        $.post('index_aj.php', params, funcionExito);
    } else {
        $('.Ulbuscador').html('')
        $('.buscarFiltro').removeAttr('id-data')
    }
}


function buscarGruposCiclo() {
    if ($.trim($('.buscarFiltro').val()) && $('.buscarFiltro').val().length >= 4) {
        var params = new Object()
        params.action = "buscarGruposCiclo"
        params.buscar = "%"
        params.Id_ciclo = $('#ciclo option:selected').val()
        if ($.trim($('.buscarFiltro').val())) {
            params.buscar = $('.buscarFiltro').val()
        }
        var funcionExito = function (resp) {
            $('.Ulbuscador').html(resp)
            $('.Ulbuscador li').on("click", function (event) {
                $('.buscarFiltro').val($(this).text())
                $('.buscarFiltro').attr('id-data', $(this).attr('id-data'))
                $('.Ulbuscador').html('')
            });
        }
        $.post('index_aj.php', params, funcionExito);
    } else {
        $('.Ulbuscador').html('')
        $('.buscarFiltro').removeAttr('id-data')
    }
}


function mostrar_botones(Obj) {
    if ($(Obj).next().is(":hidden")) {
        $(Obj).next().slideDown("fast");
        $(Obj).html('Ocultar')
    } else {
        $(Obj).next().slideUp("fast");
        $(Obj).html('Mostrar')
    }
}

function deleteAttachments() {
    var params = new Object();
    params.action = "deleteAttachments"
    var funcionExito = function (resp) {
    }
    $.post("index_aj.php", params, funcionExito);
}

function mostrarModal(html) {
    $("#myModal").modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    })
    $("#myModal").html(html)
}

function ocultarModal() {
    $("#myModal").modal('hide')
}

function imprimirErrores(errores) {
    $string = "Campos faltantes\n\n";
    $.each(errores, function (key, val) {
        $string += "-" + val + "\n"
    })
    alert($string)
}




