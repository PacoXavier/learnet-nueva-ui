
function mostrar_log_seguimiento(Id_pago){
    var params= new Object();	
        params.action="mostrar_log_seguimiento"
        params.Id_pago=Id_pago
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("estado_cuenta_aj.php",params,funcionExito);
}

function save_comentario(Id_pago){
    var params= new Object();	
        params.action="save_comentario"
        params.Id_pago=Id_pago
        params.comentario=$('#comentario').val()
    if($('#comentario').val().length>0){
    var funcionExito= function(resp){
        $('#comentario').val('')
        $('#tabla_comentarios tbody').html(resp)
        //ocultar_error_layer();
    }
    $.post("estado_cuenta_aj.php",params,funcionExito);
    }else{
        alert("Ingresa un comentario")
    }
}

function send_email_bancario(Obj){
   if(confirm("¿Enviar email?")){
   var params= new Object()
       params.action="send_email_bancario"
       params.alumnos= new Array()
       params.id=$('#id_alumno').val()
       $(Obj).html('Enviando...')
       $(Obj).removeAttr('onClick')
       var funcionExito= function(resp){
           $(Obj).html('Enviar datos bancarios')
           $(Obj).attr('onClick','send_email_bancario(this)')
           alert('Mensaje enviado con éxito')
       }
        $.post('estado_cuenta_aj.php',params,funcionExito);	
   }
}


function mostrar_botones(Obj){
   if($(Obj).next().is(":hidden")) {
	   $(Obj).next().slideDown("fast");
           $(Obj).html('Ocultar opciones')
   } else {
	   $(Obj).next().slideUp("fast");
           $(Obj).html('Mostrar opciones')
   }
}




function update_recargos(Id_pago){
    var params= new Object();	
        params.action="update_recargos"
        params.Id_pago=Id_pago
        params.fecha_deposito=$('#fecha_deposito').val() 
    var funcionExito= function(resp){
        $('#recargos-pago').html('$'+resp.recargos)
        $('#td-adeudo').html('$'+resp.adeudo)
        $('#adeudo').val(resp.adeudo)
        $('#monto_cubrir').val(resp.adeudo)
    }
    $.post("estado_cuenta_aj.php",params,funcionExito,"json");
}



var ObjBotom
function mostrar_pago_carrito(Obj,Id_ciclo){
    var params= new Object();	
        params.action="mostrar_pago_carrito"
        params.Id_ciclo=Id_ciclo
        params.Pagos= new Array()
        $(Obj).closest('.list_usu').find('tbody tr').each(function(){
           if($(this).find('input[type="checkbox"]').is(":checked")){
                var pago= new Object()
                    pago.Id_pago=$(this).attr('id-pago')
                    params.Pagos.push(pago)
           }
        })
        ObjBotom=Obj
        if(params.Pagos.length>0){
        var funcionExito= function(resp){
            mostrar_error_layer(resp);
        }
        $.post("estado_cuenta_aj.php",params,funcionExito);
    }else{
        alert("Seleccione las mensualidades a cubrir")
    }

}


function select_miscelaneo(){
    var params= new Object();	
        params.action="select_miscelaneo"
        params.Id_mis=$('#list_mis option:selected').val()
    var funcionExito= function(resp){
        $('#costo_miscelaneo').val(resp)
    }
    $.post("estado_cuenta_aj.php",params,funcionExito);
}


/*++++++++++*/

function buscar_int(){
 if($.trim($('#email_int').val())){
       var params= new Object()
           params.action="buscar_int"
           params.buscar=$('#email_int').val()
      var funcionExito =function(resp){
          $('#buscador_int').html(resp) 
      }
      $.post("estado_cuenta_aj.php",params,funcionExito);
  }else{
      $('#buscador_int').html('')
  }
}


function showBoxCargo(Id_cargo){
    var params= new Object();	
        params.action="showBoxCargo"
        params.Id_cargo=Id_cargo
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("estado_cuenta_aj.php",params,funcionExito);
}



function save_cargo(){
    var params= new Object();	
        params.action="save_cargo"
        params.nombre=$('#nombre').val()
        params.monto=$('#monto').val() 
        params.fechaLimite=$('#fechaLimite').val() 
        params.Id_usu=$('#Id_usu').val() 
        params.TipoUsu=$('#TipoUsu').val() 
    if($('#nombre').val().length>0 && $('#monto').val().length>0 && $('#fechaLimite').val().length>0){
        var funcionExito= function(resp){
          $('#tabla').html(resp)
          ocultar_error_layer();
        } 
        $.post("estado_cuenta_aj.php",params,funcionExito);
    }else{
        alert("Completa los campos")
    }
}



function show_box_recargo(Id_pago){
    var params= new Object();	
        params.action="show_box_recargo"
        params.Id_pago=Id_pago
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("estado_cuenta_aj.php",params,funcionExito);
}


function save_recargo(Id_pago,Obj){
    var params= new Object();	
        params.action="save_recargo"
        params.Id_pago=Id_pago
        params.Fecha_recargo=$('#fecha_recargo').val() 
        params.monto=$('#monto').val() 
        params.Id_usu=$('#Id_usu').val() 
        params.TipoUsu=$('#TipoUsu').val() 
    if($('#fecha_recargo').val().length>0 && $('#monto').val()>0){    
        $(Obj).removeAttr('onClick')
        var funcionExito= function(resp){
            $('#tabla').html(resp)
            $(Obj).attr('onClick','save_recargo('+Id_pago+',this)')
            ocultar_error_layer();
        }
        $.post("estado_cuenta_aj.php",params,funcionExito);
    }else{
        alert("Completa los campos faltantes")
    }
}


function delete_recargo(Id_recargo){
   if(confirm("¿Está seguro de eliminar el recargo?")){
        var params= new Object();	
            params.action="delete_recargo"
            params.Id_recargo=Id_recargo
            params.Id_usu=$('#Id_usu').val() 
            params.TipoUsu=$('#TipoUsu').val() 
        var funcionExito= function(resp){
            $('#tabla').html(resp)
            ocultar_error_layer();
        }
        $.post("estado_cuenta_aj.php",params,funcionExito);
    }
}


function delete_cargo(Id_cargo){
    if(confirm("¿Está seguro de eliminar el cargo?")){
       var params= new Object();	
            params.action="delete_cargo"
            params.Id_cargo=Id_cargo
            params.Id_usu=$('#Id_usu').val() 
            params.TipoUsu=$('#TipoUsu').val() 
        var funcionExito= function(resp){
           $('#tabla').html(resp)
             ocultar_error_layer() 
        }
        $.post("estado_cuenta_aj.php",params,funcionExito);   
    }
}


function mostrar_fecha_pago(Id_pago,Num_pago){
    var params= new Object();	
        params.action="mostrar_fecha_pago"
        params.Id_pago=Id_pago
        params.Num_pago=Num_pago
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("estado_cuenta_aj.php",params,funcionExito);
}


function save_fecha(Id_pago){
    var params= new Object();	
        params.action="save_fecha"
        params.Id_pago=Id_pago
        params.fecha_pago=$('#fecha_pago').val() 
        params.Id_usu=$('#Id_usu').val() 
        params.TipoUsu=$('#TipoUsu').val() 
    if($('#fecha_pago').val().length>0){
    var funcionExito= function(resp){
        $('#tabla').html(resp)
        ocultar_error_layer();
    }
    $.post("estado_cuenta_aj.php",params,funcionExito);
     }else{
         alert("Ingresa una fecha")
     }
}


function mostrar_descuento(Id_pago,Num_pago){
    var params= new Object();	
        params.action="mostrar_descuento"
        params.Id_pago=Id_pago
        params.Num_pago=Num_pago
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("estado_cuenta_aj.php",params,funcionExito);
}


function save_descuento(Id_pago){
    var params= new Object();	
        params.action="save_descuento"
        params.Id_pago=Id_pago
        params.descuento=$('#descuento').val() 
        params.Id_usu=$('#Id_usu').val() 
        params.TipoUsu=$('#TipoUsu').val() 
    if($('#descuento').val().length>0){
    var funcionExito= function(resp){
        $('#tabla').html(resp)
        ocultar_error_layer();
    }
    $.post("estado_cuenta_aj.php",params,funcionExito);
     }else{
         alert("Ingresa una cantidad")
     }
}


function mostrar_pago(Id_pago){
    var params= new Object();	
        params.action="mostrar_pago"
        params.Id_pago=Id_pago
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("estado_cuenta_aj.php",params,funcionExito);
}


function save_monto(Id_pago,Obj){
    var params= new Object();	
        params.action="save_monto"
        params.Id_pago=Id_pago
        params.concepto=$('#concepto').val()
        params.monto=$('#monto_cubrir').val()
        params.Fecha_deposito=$('#fecha_deposito').val() 
        params.Comentarios=$('#Comentarios').val() 
        params.metodo_pago=$('#metodo_pago option:selected').val() 
        params.Id_usu=$('#Id_usu').val() 
        params.TipoUsu=$('#TipoUsu').val() 
    if($('#monto_cubrir').val()>0 && $('#metodo_pago option:selected').val() >0 && $('#fecha_deposito').val().length>0 && $('#concepto').val().length>0){
            $(Obj).removeAttr('onclick')
            var funcionExito= function(resp){
                $('#tabla').html(resp)
                ocultar_error_layer();
                $(Obj).attr('onclick','save_monto('+Id_pago+',this)')
            }
            $.post("estado_cuenta_aj.php",params,funcionExito);  
    }else{
        alert("Completa los campos")
    }
}



function delete_pago(Id_pp){
   if(confirm("¿Está seguro de eliminar el cobro?")){
        var params= new Object();	
            params.action="delete_pago"
            params.Id_pp=Id_pp
            params.Id_usu=$('#Id_usu').val() 
            params.TipoUsu=$('#TipoUsu').val() 
        var funcionExito= function(resp){
            $('#tabla').html(resp)
            ocultar_error_layer();

        }
        $.post("estado_cuenta_aj.php",params,funcionExito);
    }
}


function mostrar_fecha_recargo(Id_recargo){
    var params= new Object();	
        params.action="mostrar_fecha_recargo"
        params.Id_recargo=Id_recargo
    var funcionExito= function(resp){
        mostrar_error_layer(resp);
    }
    $.post("estado_cuenta_aj.php",params,funcionExito);
}


function save_fecha_recargo(Id_recargo){
    var params= new Object();	
        params.action="save_fecha_recargo"
        params.Id_recargo=Id_recargo
        params.fecha_pago=$('#fecha_pago').val() 
        params.Id_usu=$('#Id_usu').val() 
        params.TipoUsu=$('#TipoUsu').val() 
    if($('#fecha_pago').val().length>0){
    var funcionExito= function(resp){
        $('#tabla').html(resp)
        ocultar_error_layer();
    }
    $.post("estado_cuenta_aj.php",params,funcionExito);
     }else{
         alert("Ingresa una fecha")
     }
}


function mostrar_recibo(Id_pp,Id_ciclo){
    window.open("comprobante_pago.php?id_pp="+Id_pp+"&id_ciclo="+Id_ciclo+"&IdRel="+$('#Id_usu').val()+"&TipoRel="+$('#TipoUsu').val());
}

function send_recibo(Id_pp,Id_ciclo,Obj){
    if(confirm("¿Esta seguro de enviar el recibo?")){
    var params= new Object();	
        params.action="send_recibo"
        params.id_pp=Id_pp
        params.id_ciclo=Id_ciclo 
        params.Id_usu=$('#Id_usu').val() 
        params.TipoUsu=$('#TipoUsu').val() 
    var funcionExito= function(resp){
        if(resp.status!="sent"){
            alert("Recibo enviado por email")
        }else{
           alert("Error al enviar el recibo");  
        }  

    }
    $.post("estado_cuenta_aj.php",params,funcionExito,"json");
   }
}
  