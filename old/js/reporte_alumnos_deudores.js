var rutaAjax = "ajax/reporte_alumnos_deudores_aj.php"
$(window).ready(function(){
    $(".boxfil").draggable();
})



function filtro(Obj){
   var params= new Object()
       params.action="filtro"
       params.Tipo=$('#tipo option:selected').val()
       params.Id_alum=$('.buscarFiltro').attr('id_alum')
       params.Id_grupo=$('#grupo').attr('id-data')
       params.Id_ciclo=$('#ciclo option:selected').val()
       params.Turno=$('#turno option:selected').val()
       params.Id_oferta=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
       params.fecha_ini=$('#fecha_ini').val()
       params.fecha_fin=$('#fecha_fin').val()
       $(Obj).text('Buscando...')
   var funcionExito= function(resp){
       $(Obj).text('Buscar')
       $('.table tbody').html(resp)
   }
   $.post(rutaAjax,params,funcionExito);	

}


function send_email(Obj){
   if(confirm("¿Enviar email de cobranza?")){
   var params= new Object()
       params.action="send_email"
       params.alumnos= new Array()
       $('.table tbody tr ').each(function(){
           if($(this).find('input[type="checkbox"]').is(":checked")){
               var alum= new Object()
                   alum.id=$(this).attr('id_alum')
                   alum.id_ofe_alum=$(this).attr('id_ofe_alum')
               params.alumnos.push(alum)
           }
       })
   if(params.alumnos.length>0){
           $(Obj).html('Enviando...')
           $(Obj).removeAttr('onClick')
           var funcionExito= function(resp){
               $(Obj).html('Enviar email cobranza')
               $(Obj).attr('onClick','send_email(this)')
               alert('Mensaje enviado con  éxito')
           }
            $.post(rutaAjax,params,funcionExito);	
   }else{
       alert("Selecciona alumnos para enviar el mensaje")
   }
   }
}


function mostrar_box_seguimiento(Id_alum){
    var params= new Object();	
        params.action="mostrar_box_seguimiento"
        params.Id_alum=Id_alum
    var funcionExito= function(resp){
         mostrar_error_layer(resp);
    }
    $.post(rutaAjax,params,funcionExito);
}


function capturar(Id_alum){
    var params= new Object();	
        params.action="capturar"
        params.Id_alum=Id_alum
        params.comentario=$('#comentario').val()
    if($('#comentario').val().length>0){
        var funcionExito= function(resp){
            ocultar_error_layer(resp);
        }
        $.post(rutaAjax,params,funcionExito);
    }else{
        alert("Ingresa un comentario")
    }
}

function download_excel(){
   var params= new Object()
       params.action="download_excel"
       params.Tipo=$('#tipo option:selected').val()
       params.Id_alum=$('.buscarFiltro').attr('id_alum')
       params.fecha_ini=$('#fecha_ini').val()
       params.fecha_fin=$('#fecha_fin').val()
       params.Id_oferta=$('#oferta option:selected').val()
       params.Id_esp=$('#curso option:selected').val()
       params.Id_ori=$('#orientacion option:selected').val()
   var query=$.param( params );
   window.open(rutaAjax+"?"+query, '_blank');
}


function buscarAlum(Obj){
  if($.trim($(Obj).val()) && $('.buscarFiltro').val().length>=4){
   var params= new Object()
       params.action="buscarAlum"
       params.buscar="%"
       if($.trim($(Obj).val())){
           params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id_alum',$(this).attr('id_alum'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post(rutaAjax,params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id_alum')
  }
}



function buscarGruposCiclo(Obj){
   if($.trim($(Obj).val())){
   var params= new Object()
       params.action="buscarGruposCiclo"
       params.buscar="%"
       params.Id_ciclo=$('#ciclo option:selected').val()
       if($.trim($(Obj).val())){
           params.buscar=$(Obj).val()
       }
   var funcionExito= function(resp){
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html(resp)
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').find('li').on("click", function(event){
          $(Obj).val($(this).text())
          $(Obj).attr('id-data',$(this).attr('id-data'))
          $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      });
   }
   $.post('index_aj.php',params,funcionExito);	
   }else{
      $(Obj).closest('.boxUlBuscador').find('.Ulbuscador').html('')
      $(Obj).removeAttr('id-data')
  }
}