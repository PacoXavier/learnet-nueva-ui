<?php
//error_reporting(E_ALL);
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
require_once('clases/DaoAmonestaciones.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoDocentes.php');
require_once('clases/modelos/base.php');
require_once("Connections/cnn.php");
require_once('estandares/class_interesados.php');
require_once('estandares/class_ofertas.php');
require_once('estandares/class_usuarios.php');
require_once('estandares/class_direcciones.php');
require_once('estandares/class_miscelaneos.php');
require_once('estandares/Fpdf/fpdf.php'); 
require_once('estandares/Fpdf/fpdi.php'); 
require_once("estandares/QRcode/qrlib.php");
require_once('estandares/cantidad_letra.php'); 
require_once('estandares/class_descuentos.php'); 
require_once('estandares/class_metodos_pago.php');
require_once('estandares/class_pagos.php');

require_once('clases/DaoDatosBancarios.php');
require_once('clases/DaoPlanteles.php');
require_once('clases/DaoDirecciones.php');
require_once('clases/DaoPagosCiclo.php');
require_once('clases/DaoPagos.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoCiclosAlumno.php');
require_once('clases/DaoRecargos.php');
require_once('clases/DaoMiscelaneos.php');
require_once('clases/DaoMetodosPago.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoPlanteles.php');
require_once('clases/modelos/Recargos.php');
require_once('clases/modelos/base.php');
require_once('clases/modelos/PagosCiclo.php');


if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");
date_default_timezone_set('America/Mexico_City');


if($_POST['action']=="buscar_int"){
    /*
    ?>
    <li><b style="color: black;">Alumnos</b></li>
    <?php 
    $DaoAlumnos= new DaoAlumnos();
    foreach($DaoAlumnos->buscarAlumno($_POST['buscar']) as $k=>$v){
      ?>
    <a href="amolestaciones.php?id=<?php echo $v->getId()?>&tipo=alum"> <li id-data="<?php echo $v->getId();?>" tipo_usu="alum"><?php echo $v->getNombre() . " " . $v->getApellidoP() . " " . $v->getApellidoM() ?></li></a>
      <?php      
    }
    */
    ?>
    <li><b style="color: black;">Usuarios</b></li>
    <?php 
    $DaoUsuarios= new DaoUsuarios();
    foreach($DaoUsuarios->buscarUsuario($_POST['buscar']) as $k=>$v){
      ?>
    <a href="estado_cuenta.php?id=<?php echo $v->getId()?>&t=u"> <li id-data="<?php echo $v->getId();?>" tipo_usu="usu"><?php echo $v->getNombre_usu()." ".$v->getApellidoP_usu()." ".$v->getApellidoM_usu() ?></li></a>
     <?php      
    }
    ?>
    <li><b style="color: black;">Docentes</b></li>
    <?php 
    $DaoDocentes= new DaoDocentes();
    foreach($DaoDocentes->buscarDocente($_POST['buscar']) as $k=>$v){
      ?>
    <a href="estado_cuenta.php?id=<?php echo $v->getId()?>&t=d"> <li id-data="<?php echo $v->getId();?>" tipo_usu="docen"><?php echo $v->getNombre_docen()." ".$v->getApellidoP_docen()." ".$v->getApellidoM_docen() ?></li></a>
      <?php      
    }
}



if($_POST['action']=="select_miscelaneo"){
    $DaoMiscelaneos= new DaoMiscelaneos();
    $mis=$DaoMiscelaneos->show($_POST['Id_mis']);
    echo $mis->getCosto_mis();
}


/*
if($_POST['action']=="send_email_bancario"){
        $base= new base();
        $DaoUsuarios= new DaoUsuarios();
        $usu=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        
        $DaoPlanteles= new DaoPlanteles();
        $plantel=$DaoPlanteles->show($usu->getId_plantel());
        
        $DaoAlumnos= new DaoAlumnos();
        $alum=$DaoAlumnos->show($_POST['id']);
        
        $Banco="";
        $Beneficiario="";
        $Convenio="";
        $Transferencias="";
        $RFC="";
        $DaoDatosBancarios=new DaoDatosBancarios();
        $datosB=$DaoDatosBancarios->getDatosBancariosPlantel($plantel->getId());
        foreach($datosB as $dato){
            $Banco=$dato->getBanco();
            $Beneficiario=$dato->getBeneficiario();
            $Convenio=$dato->getConvenio();
            $Transferencias=$dato->getTrasferencias();
            $RFC=$dato->getRFC();   
        }
        

        $asunto="Datos bancarios";
        $mensaje="Fecha: ".date('Y-m-d')."<br><br>

        Estimado <b>".mb_strtoupper($alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM())."</b>:<br><br>

        Por este medio te hacemos llegar tú número de referencia, indispensable para que ".$plantel->getNombre_plantel()." pueda registrar tus pagos en tiempo.<br><br>

        Recuerda que este número es único e intransferible, ya que es la referencia a tu matrícula.<br><br>

        Banco: ".$Banco."<br>
        Beneficiario: ".$plantel->getNombre_plantel()."<br>
        Convenio: ".$Convenio."<br>
        Referencia: <b>".$alum->getRefencia_pago()."</b><br><br>

        Transferencias: ".$Transferencias."<br><br>
        RFC: ".$RFC."<br><br>

        Atentamente,<br><br>

        ".$plantel->getNombre_plantel();


  $arrayData= array();
  $arrayData['Asunto']=$asunto;
  $arrayData['Mensaje']=$mensaje;
  $arrayData['Destinatarios']=array();
  
  $Data= array();
  $Data['email']= $alum->getEmail();
  //$Data['email']= "christian310332@gmail.com";
  $Data['name']= mb_strtoupper($alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM());
  array_push($arrayData['Destinatarios'], $Data);

  echo $base->send_email($arrayData);
}
 */



if ($_POST['action'] == "showBoxCargo") {
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $DaoPagosCiclo= new DaoPagosCiclo();
    ?>
    <div class="box-emergente-form box-miscelaneo">
        <h1>Cargo</h1>
        <p>Nombre<br><input type="text"  id="nombre"/></p>   
        <p>Monto<br><input type="number"  id="monto"/></p> 
        <p>Fecha limite de pago<br><input type="date"  id="fechaLimite"/></p> 
        <button onclick="save_cargo()">A&ntilde;adir cargo</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
    <?php
}



if($_POST['action']=="save_cargo"){
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $DaoPagosCiclo= new DaoPagosCiclo();

    $fechaCapturaCargo=date('Y-m-d H:i:s');
    $PagosCiclo= new PagosCiclo();
    $PagosCiclo->setConcepto($_POST['nombre']);
    $PagosCiclo->setFecha_pago($_POST['fechaLimite']);
    $PagosCiclo->setMensualidad($_POST['monto']);
    $PagosCiclo->setTipo_pago("pago");
    $PagosCiclo->setIdRel($_POST['Id_usu']);
    $PagosCiclo->setTipoRel($_POST['TipoUsu']);
    $PagosCiclo->setUsuCapturaCargo($_COOKIE['admin/Id_usu']);
    $PagosCiclo->setFechaCapturaCargo($fechaCapturaCargo);
    $Id_pago=$DaoPagosCiclo->add($PagosCiclo); 
    
    if($_POST['TipoUsu']=="usu"){
        $usu=$DaoUsuarios->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
        $tipo="usuario";
    }else if($_POST['TipoUsu']=="docen"){
        $usu=$DaoDocentes->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_docen()." ".$usu->getApellidoP_docen()." ".$usu->getApellidoM_docen(); 
        $tipo="docente";
    }

    //Crear historial
    $TextoHistorial="Añade cargo de $".number_format($_POST['monto'],2)." por concepto ".$_POST['nombre']." con número de folio #".$PagosCiclo->getId().", para el ".$tipo." ".$nombre.", el día ".$fechaCapturaCargo;
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Cargos ".$tipo);
    
    updatePagos($_POST['Id_usu'],$_POST['TipoUsu']);
}


if($_POST['action']=="save_recargo"){
    $DaoRecargos= new DaoRecargos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $DaoPagosCiclo= new DaoPagosCiclo();
    
    $Recargos=new Recargos();
    $Recargos->setFecha_recargo($_POST['Fecha_recargo']);
    $Recargos->setId_pago($_POST['Id_pago']);
    $Recargos->setMonto_recargo($_POST['monto']);
    $DaoRecargos->add($Recargos);
    
    if($_POST['TipoUsu']=="usu"){
        $usu=$DaoUsuarios->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
        $tipo="usuario";
    }else if($_POST['TipoUsu']=="docen"){
        $usu=$DaoDocentes->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_docen()." ".$usu->getApellidoP_docen()." ".$usu->getApellidoM_docen(); 
        $tipo="docente";
    }
    
    $cargo=$DaoPagosCiclo->show($_POST['Id_pago']);

    //Crear historial
    $TextoHistorial="Añade recargo de $".  number_format($_POST['monto'],2) .", para el cargo ".$cargo->getConcepto()." con número de folio #".$_POST['Id_pago'].", para el ".$tipo." ".$nombre.", el día ".date('Y-m-d H:i:s');
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Cargos ".$tipo);
    
    updatePagos($_POST['Id_usu'],$_POST['TipoUsu']);
}


if($_POST['action']=="delete_recargo"){
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoRecargos= new DaoRecargos();
    
    $recargos=$DaoRecargos->show($_POST['Id_recargo']);
    $PagosCiclo=$DaoPagosCiclo->show($recargos->getId_pago());
    $cargo=$DaoPagosCiclo->show($PagosCiclo->getId());

    if($_POST['TipoUsu']=="usu"){
        $usu=$DaoUsuarios->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
        $tipo="usuario";
    }else if($_POST['TipoUsu']=="docen"){
        $usu=$DaoDocentes->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_docen()." ".$usu->getApellidoP_docen()." ".$usu->getApellidoM_docen(); 
        $tipo="docente";
    }
    
    //Crear historial
    $TextoHistorial="Elimina recargo de $".  number_format($recargos->getMonto_recargo(), 2)." para el cargo con número de folio #".$PagosCiclo->getId().":  ".$cargo->getConcepto().",  para el ".$tipo." ".$nombre.", el día ".date('Y-m-d H:i:s');
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Cargos ".$tipo);
    
    $DaoRecargos->delete($_POST['Id_recargo']);
    updatePagos($_POST['Id_usu'],$_POST['TipoUsu']);
}


if ($_POST['action']=="delete_cargo"){
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $DaoPagosCiclo= new DaoPagosCiclo();

    $PagosCiclo=$DaoPagosCiclo->show($_POST['Id_cargo']);
    
    if($_POST['TipoUsu']=="usu"){
        $usu=$DaoUsuarios->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
        $tipo="usuario";
    }else if($_POST['TipoUsu']=="docen"){
        $usu=$DaoDocentes->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_docen()." ".$usu->getApellidoP_docen()." ".$usu->getApellidoM_docen(); 
        $tipo="docente";
    }
    
    //Crear historial
    $TextoHistorial="Elimino el cargo ".$PagosCiclo->getConcepto()." con número de folio #".$PagosCiclo->getId().",  para el ".$tipo." ".$nombre.", el día ".date('Y-m-d H:i:s');
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Cargos ".$tipo);
    
    //Actualizamos quien lo elimino
    $PagosCiclo->setDeadCargo(date('Y-m-d H:i:s'));
    $PagosCiclo->setDeadCargoUsu($_COOKIE['admin/Id_usu']);
    $DaoPagosCiclo->update($PagosCiclo);

    updatePagos($_POST['Id_usu'],$_POST['TipoUsu']);
}

if($_POST['action']=="mostrar_fecha_pago"){
  ?>   
   <div class="box-emergente-form box-feha-cargo">
        <h1>Editar fecha límite</h1>
        <p>Fecha<br><input type="date"  id="fecha_pago"/></p>     
        <button onclick="save_fecha(<?php echo $_POST['Id_pago']?>)">Guardar</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
<?php
}


if($_POST['action']=="save_fecha"){
  $DaoPagosCiclo= new DaoPagosCiclo();
  $PagosCiclo=$DaoPagosCiclo->show($_POST['Id_pago']);

  //Crear historial
  $DaoUsuarios= new DaoUsuarios();
  $DaoDocentes= new DaoDocentes();
  $DaoCiclosAlumno= new DaoCiclosAlumno();
  $cargo=$DaoPagosCiclo->show($_POST['Id_pago']);
  
    if($_POST['TipoUsu']=="usu"){
        $usu=$DaoUsuarios->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
        $tipo="usuario";
    }else if($_POST['TipoUsu']=="docen"){
        $usu=$DaoDocentes->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_docen()." ".$usu->getApellidoP_docen()." ".$usu->getApellidoM_docen(); 
        $tipo="docente";
    }

  $TextoHistorial="Edita la fecha para el cargo con número de folio #".$_POST['Id_pago'].": ".$cargo->getConcepto()." de ".$cargo->getFecha_pago()." a ".$_POST['fecha_pago'].", para el ".$tipo." ".$nombre.", el día ".date('Y-m-d H:i:s');
  $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Cargos ".$tipo);

  $PagosCiclo->setFecha_pago($_POST['fecha_pago']);
  $DaoPagosCiclo->update($PagosCiclo);

  updatePagos($_POST['Id_usu'],$_POST['TipoUsu']);
}


if($_POST['action']=="mostrar_descuento"){
  ?>
    <div class="box-emergente-form box-feha-descuento">
        <h1>Editar descuento</h1>
        <p>Monto<br><input type="number"  id="descuento"/></p>    
        <button onclick="save_descuento(<?php echo $_POST['Id_pago']?>)">Guardar</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
<?php
}


if($_POST['action']=="save_descuento"){
  $DaoPagosCiclo= new DaoPagosCiclo();

  //Crear historial
  $DaoUsuarios= new DaoUsuarios();
  $DaoDocentes= new DaoDocentes();
 
  $cargo=$DaoPagosCiclo->show($_POST['Id_pago']);
  $descuento=  number_format($cargo->getDescuento(),2,",",".");
  if($cargo->getDescuento()==null){
     $descuento=0; 
  }
  if($_POST['TipoUsu']=="usu"){
      $usu=$DaoUsuarios->show($_POST['Id_usu']);
      $nombre=$usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
      $tipo="usuario";
  }else if($_POST['TipoUsu']=="docen"){
      $usu=$DaoDocentes->show($_POST['Id_usu']);
      $nombre=$usu->getNombre_docen()." ".$usu->getApellidoP_docen()." ".$usu->getApellidoM_docen(); 
      $tipo="docente";
  }
  $nuevoDescuento=  number_format($_POST['descuento'], 2,",",".");

  $TextoHistorial="Edita el descuento para el cargo con número de folio #".$_POST['Id_pago'].":  ".$cargo->getConcepto()."  de $".$descuento." a $".$nuevoDescuento.",  para el ".$tipo." ".$nombre.", el día ".date('Y-m-d H:i:s');
  $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Descuentos");

  $cargo->setDescuento($_POST['descuento']);
  $cargo->setUsuCapturaDescuento($_COOKIE['admin/Id_usu']);
  $cargo->setFechaCapturaDescuento(date('Y-m-d H:i:s'));
  $DaoPagosCiclo->update($cargo);
  updatePagos($_POST['Id_usu'],$_POST['TipoUsu']);
}


if($_POST['action']=="mostrar_pago"){
    $text="Nuevo";
    $DaoPagos= new DaoPagos();
    $DaoMetodosPago= new DaoMetodosPago();
    $monto="";
    $fecha="";
    $comentarios="";
    $concepto="";
    $metodox="";
    if($_POST['Id_pago']>0){
       $text="Editar";
       $pago=$DaoPagos->show($_POST['Id_pago']);
       $monto=$pago->getMonto_pp();
       $fecha=$pago->getFecha_pp();
       $comentarios=$pago->getComentarios();
       $concepto=$pago->getConcepto();
       $metodox= $pago->getMetodo_pago();
    }
  ?>
  
   <div class="box-emergente-form box-pagos">
        <h1><?php echo $text?> pago</h1>
        <p>Concepto<br><input type="text"  id="concepto"  value="<?php echo $concepto?>"/></p>  
        <p>Monto<br><input type="number"  id="monto_cubrir"  value="<?php echo $monto?>"/></p>     
        <p>Fecha de dep&oacute;sito<br><input type="date"  id="fecha_deposito"  value="<?php echo $fecha?>"/></p>  
        <p>M&eacute;todo de pago<br>
            <select id="metodo_pago">
              <option value="0"></option>
            <?php
            foreach($DaoMetodosPago->showAll() as $metodo){
                ?>
              <option value="<?php echo $metodo->getId(); ?>" <?php if($metodox==$metodo->getId()){ ?> selected="selected" <?php } ?>><?php echo $metodo->getNombre() ?></option>
                <?php  
            }
            ?>
            </select>
        </p> 
        <p>Comentarios<br><input type="text" id="Comentarios" value="<?php echo $comentarios?>"/></p>  
        <?php
        $Id_pago=0;
        if($_POST['Id_pago']>0){
           $Id_pago=$_POST['Id_pago'];
        }
        ?>
        <button onclick="save_monto(<?php echo $Id_pago?>,this)">Confirmar pago</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
<?php
}

if($_POST['action']=="save_monto"){   
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $DaoPagosCiclo= new DaoPagosCiclo();
    $DaoRecargos= new DaoRecargos();
    $DaoPagos= new DaoPagos();
    
    
    if($_POST['TipoUsu']=="usu"){
        $usu=$DaoUsuarios->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
        $tipo="usuario";
    }else if($_POST['TipoUsu']=="docen"){
        $usu=$DaoDocentes->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_docen()." ".$usu->getApellidoP_docen()." ".$usu->getApellidoM_docen(); 
        $tipo="docente";
    }

    if($_POST['Id_pago']>0){
        $Pagos=$DaoPagos->show($_POST['Id_pago']);
        
        //Crear historial
        $TextoHistorial="Edita pago de $".  number_format($Pagos->getMonto_pp(),2)." a $". number_format($_POST['monto'],2)." por concepto de ".$_POST['concepto']." el día ".date('Y-m-d H:i:s')." con número de folio #".$Id_pago.", para el ".$tipo." ".$nombre;
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Pagos");
        
        $Pagos->setMonto_pp($_POST['monto']);
        $Pagos->setId_usu($_COOKIE['admin/Id_usu']);
        $Pagos->setFecha_pp($_POST['Fecha_deposito']);
        $Pagos->setMetodo_pago($_POST['metodo_pago']);
        $Pagos->setFecha_captura(date('Y-m-d H:i:s'));
        $Pagos->setComentarios($_POST['Comentarios']);
        $Pagos->setConcepto($_POST['concepto']);
        $DaoPagos->update($Pagos);  
        $Id_pago=$_POST['Id_pago'];

    }else{
        $Folio=$DaoPagos->generarFolio();
        $usu_x=$DaoUsuarios->show($_COOKIE['admin/Id_usu']);
        $Pagos=new Pagos();
        $Pagos->setMonto_pp($_POST['monto']);
        $Pagos->setId_usu($_COOKIE['admin/Id_usu']);
        $Pagos->setFecha_pp($_POST['Fecha_deposito']);
        $Pagos->setMetodo_pago($_POST['metodo_pago']);
        $Pagos->setFecha_captura(date('Y-m-d H:i:s'));
        $Pagos->setComentarios($_POST['Comentarios']);
        $Pagos->setConcepto($_POST['concepto']);
        $Pagos->setId_plantel($usu_x->getId_plantel());
        $Pagos->setActivo(1);
        $Pagos->setFolio($Folio);
        $Pagos->setIdRel($_POST['Id_usu']);
        $Pagos->setTipoRel($_POST['TipoUsu']);
        $Id_pago=$DaoPagos->add($Pagos);  
        //Crear historial
        $TextoHistorial="Añade pago de $". number_format($_POST['monto'],2)." por concepto de ".$_POST['concepto']." el día ".date('Y-m-d H:i:s')." con número de folio #".$Id_pago.", para el ".$tipo." ".$nombre;
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Pagos");
    }
    updatePagos($_POST['Id_usu'],$_POST['TipoUsu']);
}


if ($_POST['action']=="delete_pago"){
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $DaoPagos= new DaoPagos();

    $pago=$DaoPagos->show($_POST['Id_pp']);

    if($_POST['TipoUsu']=="usu"){
        $usu=$DaoUsuarios->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
        $tipo="usuario";
    }else if($_POST['TipoUsu']=="docen"){
        $usu=$DaoDocentes->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_docen()." ".$usu->getApellidoP_docen()." ".$usu->getApellidoM_docen(); 
        $tipo="docente";
    }
    
    //Crear historial
    $TextoHistorial="Elimino el pago por concepto de ".$pago->getConcepto()." con número de folio #".$pago->getId().", para el ".$tipo." ".$nombre;
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Pagos");
    
    $DaoPagos->delete($_POST['Id_pp']);

    updatePagos($_POST['Id_usu'],$_POST['TipoUsu']);
}


if($_POST['action']=="mostrar_fecha_recargo"){
  ?>
    <div class="box-emergente-form box-fecha-recargo">
        <h1>Editar fecha</h1>
        <p>Fecha<br><input type="date"  id="fecha_pago"/></p>  
        <button onclick="save_fecha_recargo(<?php echo $_POST['Id_recargo']?>)">Guardar</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
<?php
}


if($_POST['action']=="save_fecha_recargo"){
    $DaoRecargos= new DaoRecargos();
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $DaoPagosCiclo= new DaoPagosCiclo();
    
    $Recargos=$DaoRecargos->show($_POST['Id_recargo']);

    $cargo=$DaoPagosCiclo->show($Recargos->getId_pago());
    
    if($_POST['TipoUsu']=="usu"){
        $usu=$DaoUsuarios->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
        $tipo="usuario";
    }else if($_POST['TipoUsu']=="docen"){
        $usu=$DaoDocentes->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_docen()." ".$usu->getApellidoP_docen()." ".$usu->getApellidoM_docen(); 
        $tipo="docente";
    }

    //Crear historial
    $TextoHistorial="Edita fecha de recargo de ".  $Recargos->getFecha_recargo() ." a ".$_POST['fecha_pago'].",para el cargo ".$cargo->getConcepto()." con número de folio #".$cargo->getId().", para el ".$tipo." ".$nombre;
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Pagos");
    
    $Recargos->setFecha_recargo($_POST['fecha_pago']);
    $DaoRecargos->update($Recargos);
    
    updatePagos($_POST['Id_usu'],$_POST['TipoUsu']);
          
}


if($_POST['action']=="show_box_recargo"){
  ?>
    <div class="box-emergente-form box-recargos">
        <h1>Recargos</h1>
        <p>Fecha<br><input type="date" id="fecha_recargo"/></p>     
        <p>Monto<br><input type="number" id="monto" value="150"/></p> 
        
         <button onclick="save_recargo(<?php echo $_POST['Id_pago']?>,this)">Guardar</button>
        <button onclick="ocultar_error_layer()">Cancelar</button>
    </div>
    <?php
}


if($_POST['action']=="send_recibo"){
    //require_once('estandares/cURL.php');
    $base= new base();

    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoPagos = new DaoPagos();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoCiclos = new DaoCiclos();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoRecargos = new DaoRecargos();
    $DaoMetodosPago = new DaoMetodosPago();
    $DaoPlanteles= new DaoPlanteles();
    
    
    if($_POST['TipoUsu']=="usu"){
        $usu=$DaoUsuarios->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
        $codigo=$usu->getClave_usu();
        $email=$usu->getEmail_usu();
    }else if($_POST['TipoUsu']=="docen"){
        $usu=$DaoDocentes->show($_POST['Id_usu']);
        $nombre=$usu->getNombre_docen()." ".$usu->getApellidoP_docen()." ".$usu->getApellidoM_docen(); 
        $codigo=$usu->getClave_docen();
        $email=$usu->getEmail_docen();
    }
    
    $pago = $DaoPagos->show($_REQUEST['id_pp']);
    $metodo = $DaoMetodosPago->show($pago->getMetodo_pago());
    $ciclo = $DaoCiclos->show($_REQUEST['id_ciclo']);

    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $plantel = $DaoPlanteles->show($usu->getId_plantel());
    
    if (strlen($plantel->getFormato_pago()) > 0) {

        // initiate FPDI 
            $pdf = new FPDI();

        // add a page 
            $pdf->AddPage();

        // set the sourcefile 
           $pdf->setSourceFile('files/'.$plantel->getFormato_pago().".pdf");

        // import page 1 
            $tplIdx = $pdf->importPage(1);

        // use the imported page and place it at point 10,10 with a width of 100 mm 
            $pdf->useTemplate($tplIdx);

        // now write some text above the imported page 
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->SetTextColor(0, 0, 0);

            $pdf->SetXY(179, 4);
            $pdf->Write(10, utf8_decode(date('Y-m-d')));

            $pdf->SetXY(77, 61);
            $pdf->Write(10, mb_strtoupper(utf8_decode($nombre)));

            $pdf->SetXY(77, 69.5);
            $pdf->Write(10, utf8_decode($codigo));

            $subtotal = 0;
            if ($pago->getId() > 0) {

                $pdf->SetXY(179, 12);
                $pdf->Write(10, utf8_decode($pago->getFolio()));

                $pdf->SetXY(77, 95);
                $pdf->MultiCell(90, 3, mb_strtoupper(utf8_decode($pago->getConcepto() . " / " . $ciclo->getClave())), 0, 'L');
                $pdf->SetXY(77, 97);
                $pdf->Write(10, mb_strtoupper(utf8_decode($pago->getComentarios())));

                $pdf->SetXY(77, 117);
                $pdf->Write(10, mb_strtoupper(utf8_decode($metodo->getNombre())));
                $pdf->SetXY(77, 78);
                $pdf->Write(10, "$" . number_format($pago->getMonto_pp(), 2));
                $pdf->SetXY(77, 83.5);
                $pdf->Write(10, strtoupper(num2letras($pago->getMonto_pp(), 2)));
                $pdf->SetXY(77, 105);
                $pdf->Write(10, "CICLO " . $ciclo->getClave() . " " . utf8_decode($pago->getFecha_pp()));

                // add QRcode
                //texto a encliptar                                       //Ruta donde se guarda la imagen
                QRcode::png('?matricula=' . $codigo . '&folio=' . $pago->getFolio() . '&cantidad=' . number_format($pago->getMonto_pp(), 2), 'files/test.png');
                $pdf->Image("files/test.png", 23, 27, 33, 33);
            }

            $content=$pdf->Output('', 'S'); 

            //Enviar email por medio de mandrill
            $KeySistema = $base->getParam('KeyMandrill');
            $arrayParams = array();
            $arrayParams['key'] = $KeySistema;
            $arrayParams['message']['html'] = '';
            $arrayParams['message']['text'] = '';
            $arrayParams['message']['subject'] = "Comprobante de pago #".$pago->getId()." ".date('Y -m-d');
            $arrayParams['message']['from_email'] = $plantel->getEmail();
            $arrayParams['message']['from_name'] = $plantel->getNombre_plantel();
            $arrayParams['message']['to']=array();

            $arrayTo=array();
            $arrayTo["email"]=$email;
            //$arrayTo["email"]="christian310332@gmail.com";
            $arrayTo["name"]=$nombre;
            array_push($arrayParams['message']['to'], $arrayTo);
/*
            $arrayTo=array();
            $arrayTo["email"]="recibos@ulm.mx";
            $arrayTo["name"]=$plantel->getNombre_plantel();;
            array_push($arrayParams['message']['to'], $arrayTo);
 * 
 */


            $array_global_merge_vars = array();
            $array_global_merge_vars["name"] = 'example name';
            $array_global_merge_vars["content"] = 'example content';

            $arrayParams['message']['global_merge_vars'] = array();
            array_push($arrayParams['message']['global_merge_vars'], $array_global_merge_vars);

            $array_merge_vars = array();
            $array_merge_vars["rcpt"] = 'example rcpt';

            $array_merge_vars['vars'] = array();
            $array_vars_attr = array();
            $array_vars_attr['name'] = 'example name';
            $array_vars_attr['content'] = 'example content';
            array_push($array_merge_vars['vars'], $array_vars_attr);

            $arrayParams['message']['merge_vars'] = array();
            array_push($arrayParams['message']['merge_vars'], $array_merge_vars);

            $arrayParams['message']['tags'] = array();
            array_push($arrayParams['message']['tags'], "example tags[]");

            $arrayParams['message']['google_analytics_domains'] = array();
            array_push($arrayParams['message']['google_analytics_domains'], "...");

            $arrayParams['message']['google_analytics_campaign'] = "...";
            $arrayParams['message']['metadata'] = array();
            array_push($arrayParams['message']['metadata'], "...");

            $array_recipient_metadata = array();
            $array_recipient_metadata["rcpt"] = 'example rcpt';

            $array_recipient_metadata['values'] = array();
            array_push($array_recipient_metadata['values'], "...");

            $arrayParams['message']['recipient_metadata'] = array();
            array_push($arrayParams['message']['recipient_metadata'], $array_recipient_metadata);

            $arrayParams['message']['attachments'] = array();
            //$file = file_get_contents("attachments/".$row_Attachments['Llave_atta'].".ulm");
            $arrarAttrAttachments = array();
            $arrarAttrAttachments['type'] = "application/pdf";
            $arrarAttrAttachments['name'] = "Comprobante de pago ".date('Y-m-d');
            $arrarAttrAttachments['content'] = base64_encode($content);
            array_push($arrayParams['message']['attachments'], $arrarAttrAttachments);

            $arrayParams['message']['async'] = '...';
            //create a new cURL resource
            $cc = new cURL();
            $url = "https://mandrillapp.com/api/1.0/messages/send.json";

            $params= array();    
            array_push($params, json_encode($arrayParams));       
            $data=$base->simpleCurl("POST",false,$url,$params,false);

            //Leer cadena json recibida
            //La cadena recibida tienes que cortarla asta que apetezca puro json
            $data = substr($data, strpos($data, "{"));
            $data = substr($data, 0, strpos($data, "}") + 1);
            $arr = json_decode($data, true);

            echo json_encode($arr);
    }
}



function updatePagos($Id_usu, $TipoUsu){
    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
    $usu = $class_usuarios->get_usu();
    
    $perm=array();
    foreach($usu['Permisos'] as $k=>$v){
        $perm[$v['Id_perm']]=1;
    }
    
    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoPagos = new DaoPagos();
    $DaoCiclos = new DaoCiclos();
    $DaoRecargos = new DaoRecargos();
    $DaoMetodosPago = new DaoMetodosPago();
    $DaoUsuarios= new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();

    ?>
   <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-university"></i> Estado de cuenta</h1>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <li>Buscar<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Usuario u docente"/>
                          <ul id="buscador_int"></ul>
                        </li>
                    </ul>
                </div>
              <?php

              if ($Id_usu > 0) {
                
                 if($TipoUsu=="usu"){
                     $usuario = $DaoUsuarios->show($Id_usu);
                     $tipo="usu";
                     $texto="Usuario";
                     $nombre=$usuario->getNombre_usu()." ".$usuario->getApellidoP_usu()." ".$usuario->getApellidoM_usu();
                     $codigo=$usuario->getClave_usu();
                     $ingreso=$usuario->getAlta_usu();
                  }elseif($TipoUsu=="docen"){
                     $usuario = $DaoDocentes->show($Id_usu);
                     $tipo="docen";
                     $texto="Docente";
                     $nombre=$usuario->getNombre_docen()." ".$usuario->getApellidoP_docen()." ".$usuario->getApellidoM_docen(); 
                     $codigo=$usuario->getClave_docen();
                     $ingreso=$usuario->getAlta_docent();
                  }
                  
                  $SaldoPagos = $DaoPagos->getTotalPagosUsuario($Id_usu,$tipo);
                ?>
                <div class="seccion">
                    <table  id="info_alumn">
                        <tbody><tr><th style="padding-bottom: 15px;text-align: left;">
                                    <font size="3">DATOS DEL <?php echo strtoupper ($texto)?></font>
                                </th></tr>
                            <tr><td>
                                    <table width="100%">
                                        <tbody><tr>
                                                <th ><font>C&oacute;digo:</font></th>
                                                <td><font><?php echo $codigo ?></font></td>
                                                <th ><font>Nombre:</font></th>
                                                <td colspan="5"><font><?php echo $nombre ?></font></td>
                                            </tr>
                                            <tr>
                                                <th><font>Ingreso:</font></th>
                                                <td><font><?php echo $ingreso; ?></font></td>
         
                                            </tr>
                                        </tbody></table>
                                </td></tr>
                        </tbody>
                    </table>
                </div>
                <div class="seccion" id="list_ofertas">
                    <table id="estado-cuenta">
                    <tr>
                        <td id="seccion-cargos">
                            <button id="add_pago_miscelaneo" class="boton-normal" onclick="showBoxCargo()"><i class="fa fa-plus-square"></i> A&ntilde;adir cargo</button>
                            <!--<button id="buttonBancario" class="boton-normal" onclick="send_email_bancario()"><i class="fa fa-paper-plane"></i> Enviar datos bancarios</button>-->
                            <?php
                            $totalCargosOferta = 0;
                            $totalDescuentosOferta = 0;
                                ?>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td colspan="9" class="clave-ciclo">Cargos del <?php echo $texto;?></td>
                                        </tr>
                                        <tr>
                                            <td class="td-center">#</td>
                                            <td>Concepto</td>
                                            <td class="td-center" width="65px;">Fecha límite</td>
                                            <td class="td-center">Monto</td>
                                            <td class="td-center">Descuento</td>
                                            <td class="td-center">Aplica Desct.</td>
                                            <td class="td-center">Cantidad pagada</td>
                                            <td class="td-center">Adeudo</td>
                                            <td class="td-center"></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $mensualidad = 0;
                                        $Descuento = 0;
                                        $Adeudo = 0;
                                        $totalAdeudo = 0;
                                        $totalrecargos = 0;
                                        $totalmiscelaneos = 0;
                                        $totalPagado = 0;
                                        foreach ($DaoPagosCiclo->getCargosUsuario($Id_usu,$tipo) as $cargo) {
                                            //Recargos
                                            $recargos = 0;
                                            foreach ($DaoRecargos->getRecargosPago($cargo->getId()) as $recargo) {
                                                $recargos+=$recargo->getMonto_recargo();
                                            }
                                            
                                            $mensualidad+= $cargo->getMensualidad() + $recargos;
                                            $Descuento+=$cargo->getDescuento();

                                            $TotalCargo = round($cargo->getMensualidad() - $cargo->getDescuento() - $cargo->getDescuento_beca() + $recargos);
                                            $Adeudo = $TotalCargo;
                                            $MontoPagado = 0;
                                            if ($SaldoPagos > 0) {
                                                $MontoPagado = $TotalCargo;

                                                if ($SaldoPagos >= $TotalCargo) {
                                                    $SaldoPagos = $SaldoPagos - $TotalCargo;
                                                    $Adeudo = 0;
                                                } elseif ($SaldoPagos < $TotalCargo) {
                                                    $MontoPagado = $SaldoPagos;
                                                    $Adeudo = $TotalCargo - $MontoPagado;
                                                    $SaldoPagos = 0;
                                                }
                                            }
                                            $totalPagado+=$MontoPagado;
                                            $totalAdeudo+=$Adeudo;
                                            $totalrecargos+=$recargos;

                                            //Fondo
                                            $classAdeudo = "";
                                            //Esconder boton editar fecha y adeudos
                                            $style_edit = '';
                                            if ($Adeudo <= 0) {
                                                $style_edit = 'style="visibility: hidden"';
                                                $classAdeudo = 'adeudo';
                                                $cargo->setPagado(1);
                                                $DaoPagosCiclo->update($cargo);
                                            }else{
                                                $cargo->setPagado(0);
                                                $DaoPagosCiclo->update($cargo);  
                                            }

                                            $NombreUsuDescuento="";
                                            if($cargo->getUsuCapturaDescuento()>0){
                                            $aplicaDescuento=$DaoUsuarios->show($cargo->getUsuCapturaDescuento());
                                            $NombreUsuDescuento=$aplicaDescuento->getNombre_usu()." ".$aplicaDescuento->getApellidoP_usu();
                                            }
                                            ?>
                                            <tr  class="<?php echo $classAdeudo; ?>" id-pago="<?php echo $cargo->getId() ?>">
                                                <td class="td-center"><?php echo $i + 1 ?></td>
                                                <td><?php echo $cargo->getConcepto() ?></td>
                                                <td class="td-center"><?php echo $cargo->getFecha_pago() ?> <i class="fa fa-pencil-square-o" onclick="mostrar_fecha_pago(<?php echo $cargo->getId() ?>,<?php echo $i + 1 ?>)" title="Editar fecha limite de pago"></i></td>
                                                <td class="td-center"><?php echo "$" . number_format($cargo->getMensualidad(), 2) ?></td>
                                                <td class="td-center"><?php echo "$" . number_format($cargo->getDescuento(), 2) ?> 
                                                    <?php
                                                    if (isset($perm['37'])) {
                                                        ?>
                                                    <i class="fa fa-pencil-square-o" onclick="mostrar_descuento(<?php echo $cargo->getId() ?>,<?php echo $i + 1 ?>)" title="Editar descuento"></i>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo $NombreUsuDescuento;?></td>
                                                <td class="td-center"><?php echo "$" . number_format($MontoPagado, 2) ?></td>
                                                <td class="td-center"><?php echo "$" . number_format($Adeudo, 2) ?></td>
                                                <td class="right">
                                                    <i class="fa fa-plus-square" onclick="show_box_recargo(<?php echo $cargo->getId(); ?>)" title="Añadir recargo"></i>
                                                    <?php
                                                    if (isset($perm['69'])) {
                                                        ?>
                                                        <i class="fa fa-trash" onclick="delete_cargo(<?php echo $cargo->getId(); ?>)" title="Eliminar cargo"></i>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                            $query = "SELECT * FROM Recargos_pago WHERE Id_pago=" . $cargo->getId() . " AND Fecha_recargo<='" . date('Y-m-d') . "' ORDER BY Fecha_recargo ASC";
                                            foreach ($DaoRecargos->advanced_query($query) as $recargo) {
                                                ?>
                                            
                                                <tr class="recargo">
                                                    <td></td>
                                                    <td>Recargo</td>
                                                    <td class="td-center"><?php echo $recargo->getFecha_recargo() ?> <i class="fa fa-pencil-square-o" onclick="mostrar_fecha_recargo(<?php echo $recargo->getId() ?>)"></i> </td>
                                                    <td class="td-center">$<?php echo number_format($recargo->getMonto_recargo(), 2) ?></td>
                                                    <td colspan="4"></td>
                                                    <td class="right">
                                                        <?php
                                                        //if ($cargo->getCantidad_Pagada() < $Adeudo) {
                                                            if (isset($perm['38'])) {
                                                                ?>
                                                                <i class="fa fa-trash" onclick="delete_recargo(<?php echo $recargo->getId() ?>)" title="Eliminar recargo"></i>
                                                                <?php
                                                            }
                                                        //}
                                                        ?>

                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            $i++;
                                        }
                                        $totalCargosOferta+=$mensualidad;
                                        $totalDescuentosOferta+=$Descuento;
                                        ?>
                                            <tr>
                                                <td colspan="9"></td>
                                            </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3"><b>Total</b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($mensualidad), 2) ?></b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($Descuento), 2) ?></b></td>
                                            <td></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($totalPagado), 2) ?></b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($totalAdeudo), 2) ?></b></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b>Total Regargos</b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($totalrecargos), 2) ?></b></td>
                                            <td class="td-center" colspan="7"></td>
                                        </tr>
                                    </tfoot>
                                </table>
                        </td>
                        <td id="seccion-pagos">
                            <button class="boton-normal" id="mostrar-box-pago" onclick="mostrar_pago()"><i class="fa fa-plus-square"></i> Añadir pago</button>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td colspan="9" class="clave-ciclo">Historial de pagos</td>
                                    </tr>
                                    <tr>

                                        <td class="td-center">Folio</td>
                                        <td class="td-center">Fecha de captura</td>
                                        <td class="td-center">Fecha de pago</td>
                                        <td>Concepto</td>
                                        <td class="td-center">Monto</td>
                                        <td class="td-center">Método de pago</td>
                                        <td class="td-center">Usuario</td>
                                        <td>Comentarios</td>
                                        <td class="td-center">Acciones</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $totalPagado = 0;
                                    $i = 1;
                                    foreach ($DaoPagos->getPagosUsuario($Id_usu,$tipo) as $pago) {
                                        $usuCapturo = $DaoUsuarios->show($pago->getId_usu());
                                        $metodo = $DaoMetodosPago->show($pago->getMetodo_pago());
                                        $totalPagado+=$pago->getMonto_pp();
                                        $ciclo=$DaoCiclos->getCicloPorFecha($pago->getFecha_captura());
                                        ?>
                                        <tr  id-pago="<?php echo $pago->getId() ?>">
                                            <td class="td-center"><?php echo $pago->getFolio() ?></td>
                                            <td class="td-center"><?php echo $pago->getFecha_captura() ?></td>
                                            <td class="td-center"><?php echo $pago->getFecha_pp() ?></td>
                                            <td><?php echo $pago->getConcepto()?></td>
                                            <td class="td-center"><?php echo "$" . number_format($pago->getMonto_pp(), 2) ?></td>
                                            <td class="td-center"><?php echo $metodo->getNombre() ?></td>
                                            <td class="td-center"><?php echo $usuCapturo->getNombre_usu() . " " . $usuCapturo->getApellidoP_usu() ?></td>
                                            <td><?php echo $pago->getComentarios() ?></td>
                                            <td class="menu td-center">
                                                <i class="fa fa-pencil-square-o" onclick="mostrar_pago(<?php echo $pago->getId()?>)" title="Editar pago"></i>
                                                <?php
                                                if (isset($perm['69'])) {
                                                    ?>
                                                    <i class="fa fa-trash" onclick="delete_pago(<?php echo $pago->getId(); ?>)" title="Eliminar pago"></i>
                                                    <?php
                                                }
                                                ?>
                                                <i class="fa fa-file-pdf-o" onclick="mostrar_recibo(<?php echo $pago->getId(); ?>,<?php echo $ciclo->getId(); ?>)" title="Mostrar recibo"></i>
                                                <i class="fa fa-paper-plane" onclick="send_recibo(<?php echo $pago->getId() ?>,<?php echo $ciclo->getId(); ?>, this)"title="Enviar recibo"></i>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td colspan="8" class="clave-ciclo">Totales</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="3"><b>Total cargos</b></td>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($totalCargosOferta), 2) ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Total pagado</b></td>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($totalPagado), 2) ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Total descuento</b></td>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($totalDescuentosOferta), 2) ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Adeudo</b></td>
                                        <?php
                                        $z = ($totalCargosOferta - $totalPagado - $totalDescuentosOferta)
                                        ?>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($z), 2) ?></b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
                </div>
              <?php
              }
              ?>
            </div>
        </td>
        <td id="column_two">
           
        </td>
    </tr>
    <?php
}