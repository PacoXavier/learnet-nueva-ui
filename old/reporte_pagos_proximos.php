<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoCiclosAlumno= new DaoCiclosAlumno();
$DaoRecargos= new DaoRecargos();
$DaoPagosCiclo= new DaoPagosCiclo();
$DaoCiclos= new DaoCiclos();
$DaoGrupos= new DaoGrupos();
$DaoMateriasCicloAlumno= new DaoMateriasCicloAlumno();
$DaoTurnos= new DaoTurnos();


$query = "SELECT Pagos_ciclo.Id_alum,Matricula,Nombre_ins,ApellidoP_ins,ApellidoM_ins,Concepto,Mensualidad,Fecha_pago,Descuento,ciclos_alum_ulm.Id_ofe_alum FROM Pagos_ciclo 
 INNER JOIN inscripciones_ulm on Pagos_ciclo.Id_alum =  inscripciones_ulm.Id_ins 
   and inscripciones_ulm.Activo_alum = '1' and Pagos_ciclo.Pagado = 0 and Pagos_ciclo.DeadCargo IS NULL
 INNER JOIN ciclos_alum_ulm on Pagos_ciclo.Id_ciclo_alum = ciclos_alum_ulm.Id_ciclo_alum
 INNER JOIN ofertas_alumno on ciclos_alum_ulm.Id_ofe_alum = ofertas_alumno.Id_ofe_alum and ofertas_alumno.Activo_oferta='1' ORDER BY Fecha_pago DESC LIMIT 50";

$pagos_proximos_query=$base->advanced_query($query);

links_head("Pagos próximos");
write_head_body();
write_body();
?>

<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-usd" aria-hidden="true"></i> Pagos Próximos</h1>
                </div>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Filtros</li>
                        <li onclick="download_excel()"><i class="fa fa-download"></i> Descargar</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            
                            <tr>
                                <td>#</td>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Concepto</td>
                                <td style="width: 200px">Mensualidad</td>
                                <td>Descuento</td>
                                <td>Fecha de pago</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        
                            // while ($row_consulta =  mysqli_fetch_array($pagos_proximos_query)) {
                            //   echo $row_consulta['Nombre_ins'];
                            //   }

                            $count = 1;
                            $total_descuentos= 0;
                            $total_pagos= 0;
                             while ($row_consulta =  mysqli_fetch_array($pagos_proximos_query)) {
                                      ?>
                                              <tr id_alum="<?php echo $row_consulta['Id_alum']?>" id_ofe_alum="<?php echo $row_consulta['Id_ofe_alum']?>">
                                                <td width="30px;"><?php echo $count;?></td>
                                                <td><a href="alumno.php?id=<?php echo $row_consulta['Id_alum']?>"><?php echo $row_consulta['Matricula'] ?></a></td>
                                                <td style="width: 115px;"><a href="alumno.php?id=<?php echo $row_consulta['Id_alum']?>"><?php echo $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins'] ?></a></td>
                                                <td><?php echo $row_consulta['Concepto'] ?></td>
                                                <td><?php echo $row_consulta['Mensualidad'] ?></td>
                                                <td><?php echo $row_consulta['Descuento'] ?></td>
                                                <td><?php echo $row_consulta['Fecha_pago'] ?></td>

                                    
                                              </tr>
                                              <?php
                                              $count++;
                                              $total_pagos += $row_consulta['Mensualidad'];
                                              $total_descuentos += $row_consulta['Descuento'];

                                        }  
                                
                                ?>
                                <tr>
                                       <td colspan="4"></td>
                                       <td style="color:red" class="td-center"><b>$<?php echo number_format($total_pagos,2)?> </b></td>
                                       <td style="color:red" class="td-center"><b>$<?php echo number_format($total_descuentos,2)?> </b></td>
                                       <td style="color:red"><b>Total: $<?php echo number_format($total_pagos - $total_descuentos,2)?></b></td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
        <div class="boxUlBuscador">
            <p>Buscar<br><input type="search"  class="buscarFiltro" onkeyup="buscarAlum(this)" placeholder="Nombre" id_alum="0"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
        <p>Fecha inicio<br><input type="date" id="fecha_ini"/>
        <p>Fecha fin<br><input type="date" id="fecha_fin"/>

        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();



