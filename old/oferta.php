<?php
require_once('estandares/includes.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');

links_head("Niveles acad&eacute;micos");
write_head_body();
write_body();

if ($_REQUEST['id'] > 0) {
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();

    $oferta = $DaoOfertas->show($_REQUEST['id']);
    ?>
    <table id="tabla">
        <tr>
            <td id="column_one">
                <div class="fondo">
                    <div class="box_top">
                        <h1><i class="fa fa-cog" aria-hidden="true"></i> Especialidades <?php echo $oferta->getNombre_oferta() ?></h1>
                    </div>
                    <div class="seccion">						
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Nombre especialidad</td>
                                    <td>Orientaci&oacute;n</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                foreach ($DaoEspecialidades->getEspecialidadesOferta($_REQUEST['id']) as $k => $v) {
                                    ?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td><?php echo $v->getNombre_esp() ?></td>
                                                    <td style="text-align: center;width: 10%;">
                                                        <a href="curso.php?id=<?php echo $v->getId() ?>">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                        </a>

                                                        <i class="fa fa-trash" onclick="delete_esp(<?php echo $v->getId() ?>)"></i>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table class="orientaciones">
                                                <?php
                                                foreach ($DaoOrientaciones->getOrientacionesEspecialidad($v->getId()) as $k2 => $v2) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $v2->getNombre() ?></td>
                                                        <td>
                                                            <i class="fa fa-pencil-square-o" onclick="new_subnivel(<?php echo $v2->getId() ?>)"></i>
                                                            <i class="fa fa-trash" onclick="delete_sub(<?php echo $v2->getId() ?>)"></i>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </table>
                                        </td>
                                    </tr>
                                    <?php
                                    $count++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>	
            </td>
            <td id="column_two">
                <div id="box_menus">
                    <?php
                    require_once 'estandares/menu_derecho.php';
                    ?>
                    <ul>
                        <li><span><a href="curso.php?id_ofe=<?php echo $_REQUEST['id'] ?>">Nueva especialidad</a></span></li>
                        <?php
                        if (count($DaoEspecialidades->getEspecialidadesOferta($_REQUEST['id'])) > 0) {
                            ?>
                            <li><span onclick="new_subnivel()">Nueva orientaci&oacute;n</span></li>
                            <?php
                        }
                        ?>
                        <li><span><a href="ofertas_academicas.php">Regresar</a></span></li>
                    </ul>
                </div>
            </td>
        </tr>
    </table>
    <div id="box_nivel" class="form"></div>
    <div id="box_subnivel" class="form"></div>
    <div id="box_oferta" class="form"></div>
    <input type="hidden" id="Id_oferta" value="<?php echo $_REQUEST['id'] ?>"/>
    <?php
}
write_footer();

