<?php
//error_reporting(E_ALL);
require_once('Connections/cnn.php');
require_once('clases/DaoPlanteles.php');
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$DaoPlanteles= new DaoPlanteles();

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="es" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="es" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="es" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="es" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<html lang="es" class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<!--[if IE]><![endif]-->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Login</title>
<meta name="description" content="">
<meta name="keywords" content="" />
<meta name="author" content="GeneraWeb">
<meta name="viewport" content="width=980, initial-scale=1.0">

<!-- !CSS -->
<link rel="stylesheet" href="css/index.css?v=2011.5.5.13.35">
<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
</head>
<!-- !Body -->

<body>
	<div id="errorLayer"></div>
	<div id="container">
		<div id="contenido">
		     <div id="box_login">
                         <?php
                         $imgLogo="";
                         $Nombre="";
                         $server=$_SERVER["SERVER_NAME"];
                         if(strpos($server,"www")!==false){
                            $server=substr($server,strpos($server,"www")+4);
                         }
                         foreach($DaoPlanteles->showAll() as $plantel){
                             if($server==$plantel->getDominio()){
                                 $imgLogo='src="files/'.$plantel->getId_img_logo().'.jpg"';
                                 $Nombre=$plantel->getNombre_plantel();
                             }
                         }
                         
                         ?>
		          <p><img <?php echo $imgLogo?>></p>
		          <h1>Bienvenido</h1>
		          <div id="box_user"><input type="text" id="user" placeholder="Email" autocomplete="off"></div>
		          <div id="box_pass"><input type="password" id="pass"  placeholder="Contrase&ntilde;a" autocomplete="off"></div>
		          <p><button id="buttonLogin" onclick="login()">Continuar</button></p>
		          <span id="olvide"><a href="reset_account.php">Olvide mi password</a></span>
		     </div>
		</div>
		<!--!/#contenido -->
		<div id="box_img_foot"><img src="images/img_foot.png" alt="img_foot" width="1187" height="21">
			<p>&copy; <?php echo date('Y')?> <?php echo $Nombre;?></p>
		</div>
	</div>
	<!--!/#container -->
	<footer> </footer>
	<!-- !Javascript - at the bottom for fast page loading -->
	<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
	<script src="js/jquery_1.11.0.js?a=2"></script>
	<script src="js/index.js?a=2"></script>
</body>
</html>

