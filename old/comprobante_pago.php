<?php
error_reporting(E_ALL);
if ($_REQUEST['id_pp'] > 0 && $_REQUEST['id_ciclo'] > 0) {
    require_once('estandares/Fpdf/fpdf.php');
    require_once('estandares/Fpdf/fpdi.php');
    require_once("estandares/QRcode/qrlib.php");
    require_once('estandares/cantidad_letra.php');
    require_once('clases/DaoPagosCiclo.php');
    require_once('clases/DaoPagos.php');
    require_once('clases/DaoRecargos.php');
    require_once('clases/DaoAlumnos.php');
    require_once('clases/DaoOfertasAlumno.php');
    require_once('clases/DaoOfertas.php');
    require_once('clases/DaoEspecialidades.php');
    require_once('clases/DaoOrientaciones.php');
    require_once('clases/DaoCiclos.php');
    require_once('clases/DaoCiclosAlumno.php');
    require_once('clases/DaoMetodosPago.php');
    require_once('clases/DaoPlanteles.php');
     require_once('clases/DaoUsuarios.php');
     require_once('clases/DaoDocentes.php');

    $DaoPagosCiclo = new DaoPagosCiclo();
    $DaoPagos = new DaoPagos();
    $DaoOfertasAlumno = new DaoOfertasAlumno();
    $DaoAlumnos = new DaoAlumnos();
    $DaoOfertas = new DaoOfertas();
    $DaoEspecialidades = new DaoEspecialidades();
    $DaoOrientaciones = new DaoOrientaciones();
    $DaoCiclos = new DaoCiclos();
    $DaoCiclosAlumno = new DaoCiclosAlumno();
    $DaoRecargos = new DaoRecargos();
    $DaoMetodosPago = new DaoMetodosPago();
    $DaoPlanteles = new DaoPlanteles();
    $DaoUsuarios = new DaoUsuarios();
    $DaoDocentes= new DaoDocentes();



    $usu = $DaoUsuarios->show($_COOKIE['admin/Id_usu']);
    $plantel = $DaoPlanteles->show($usu->getId_plantel());

    
    $pago = $DaoPagos->show($_REQUEST['id_pp']);
    $metodo = $DaoMetodosPago->show($pago->getMetodo_pago());
    $ciclo = $DaoCiclos->show($_REQUEST['id_ciclo']);

    $usuarioCapturo=$DaoUsuarios->show($pago->getId_usu());
    $nombreUsuCapturo=$usuarioCapturo->getNombre_usu()." ".$usuarioCapturo->getApellidoP_usu()." ".$usuarioCapturo->getApellidoM_usu();
    if (strlen($plantel->getFormato_pago()) > 0) {
        
        //Si el recibo viene de laseccion de cobranza de mensualidades del alumno
        if($_REQUEST['id']>0){
            $alum = $DaoAlumnos->show($_REQUEST['id']);

            $oferta_alumno = $DaoOfertasAlumno->show($pago->getId_ofe_alum());
            $oferta = $DaoOfertas->show($oferta_alumno->getId_ofe());
            $esp = $DaoEspecialidades->show($oferta_alumno->getId_esp());
            if ($oferta_alumno->getId_ori() > 0) {
                $ori = $DaoOrientaciones->show($oferta_alumno->getId_ori());
                $nombre_ori = $ori->getNombre();
            }
            
            $nombre=$alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM();
            $codigo=$alum->getMatricula();
            $detalleCarrera="/ " .$pago->getDetalleCarrera();
        }else{
            
             //Si viene de la seccion de estado de cuenta
             if($_GET['TipoRel']=="usu"){
                $usu=$DaoUsuarios->show($_GET['IdRel']);
                $nombre=$usu->getNombre_usu()." ".$usu->getApellidoP_usu()." ".$usu->getApellidoM_usu();
                $codigo=$usu->getClave_usu();
            }else if($_GET['TipoRel']=="docen"){
                $usu=$DaoDocentes->show($_GET['Id_usu']);
                $nombre=$usu->getNombre_docen()." ".$usu->getApellidoP_docen()." ".$usu->getApellidoM_docen(); 
                $codigo=$usu->getClave_docen();
            }
        }

// initiate FPDI 
        $pdf = new FPDI();

// add a page 
        $pdf->AddPage();

// set the sourcefile 
        $pdf->setSourceFile('files/'.$plantel->getFormato_pago().".pdf");

// import page 1 
        $tplIdx = $pdf->importPage(1);

// use the imported page and place it at point 10,10 with a width of 100 mm 
        $pdf->useTemplate($tplIdx);

// now write some text above the imported page 
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->SetTextColor(0, 0, 0);

        //$pdf->Image("files/t5qebqhbxwwfb.jpg",50,50);


        $pdf->SetXY(170, 1);
        $pdf->Write(10, $pago->getFecha_captura());
        
        $pdf->SetXY(77, 56);
        $pdf->Write(10, mb_strtoupper(utf8_decode($nombre)));

        $pdf->SetXY(77, 65);
        $pdf->Write(10, utf8_decode($codigo));


//Segunda Hoja
        $pdf->SetXY(170, 144);
        $pdf->Write(10, $pago->getFecha_captura());
        
        $pdf->SetXY(77, 199);
        $pdf->Write(10, mb_strtoupper(utf8_decode($nombre)));

        $pdf->SetXY(77, 208);
        $pdf->Write(10, utf8_decode($codigo));




        $subtotal = 0;
        if ($pago->getId() > 0) {

            $pdf->SetXY(170, 7);
            $pdf->Write(10, utf8_decode($pago->getFolio()));

            $pdf->SetXY(77, 90.5);
            $pdf->MultiCell(90, 3, mb_strtoupper(utf8_decode($pago->getConcepto() . " ".$detalleCarrera." / " . $ciclo->getClave())), 0, 'L');
            $pdf->SetXY(77, 94);
            $pdf->Write(10, mb_strtoupper(utf8_decode($pago->getComentarios())));

            $pdf->SetXY(77, 112);
            $pdf->Write(10, mb_strtoupper(utf8_decode($metodo->getNombre())));
            $pdf->SetXY(77, 74);
            $pdf->Write(10, "$" . number_format($pago->getMonto_pp(), 2));
            $pdf->SetXY(77, 80.5);
            $pdf->Write(10, strtoupper(num2letras($pago->getMonto_pp(), 2)));
            $pdf->SetXY(77, 102);
            $pdf->Write(10, "CICLO " . $ciclo->getClave() . " " . utf8_decode($pago->getFecha_pp()));
    
            $pdf->SetXY(77, 125);
            $pdf->Write(10, mb_strtoupper(utf8_decode($nombreUsuCapturo)));
            // add QRcode
            //texto a encliptar                                       //Ruta donde se guarda la imagen
            QRcode::png('?matricula=' . $codigo . '&folio=' . $pago->getFolio() . '&cantidad=' . number_format($pago->getMonto_pp(), 2), 'files/test.png');
            $pdf->Image("files/test.png", 0, 27, 33, 33);


//Segunda Hoja
            $pdf->SetXY(170, 150);
            $pdf->Write(10, utf8_decode($pago->getFolio()));

            $pdf->SetXY(77, 233.5);
            $pdf->MultiCell(90, 3, mb_strtoupper(utf8_decode($pago->getConcepto() . " ".$detalleCarrera." / " . $ciclo->getClave())), 0, 'L');
            $pdf->SetXY(77, 237);
            $pdf->Write(10, mb_strtoupper(utf8_decode($pago->getComentarios())));

            $pdf->SetXY(77, 257);
            $pdf->Write(10, mb_strtoupper(utf8_decode($metodo->getNombre())));
            $pdf->SetXY(77, 217);
            $pdf->Write(10, "$" . number_format($pago->getMonto_pp(), 2));
            $pdf->SetXY(77, 223.5);
            $pdf->Write(10, strtoupper(num2letras($pago->getMonto_pp(), 2)));
            $pdf->SetXY(77, 245);
            $pdf->Write(10, "CICLO " . $ciclo->getClave() . " " . utf8_decode($pago->getFecha_pp()));
    
            $pdf->SetXY(77, 266);
            $pdf->Write(10, mb_strtoupper(utf8_decode($nombreUsuCapturo)));
            // add QRcode
            //texto a encliptar                                       //Ruta donde se guarda la imagen
            $pdf->Image("files/test.png", 0, 170, 33, 33);
        }

        $pdf->Output('recibo.pdf', 'I');
    } else {
        ?>
        Favor de cargar el formato de pago en el plantel actual
        <?php
    }
}