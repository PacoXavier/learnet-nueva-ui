<?php
require_once('estandares/includes.php');
require_once('require_daos.php');

if(!isset($perm['46'])){
  header('Location: home.php');
}
$DaoDocentes= new DaoDocentes();

links_head("Docentes  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-users" aria-hidden="true"></i> Docentes</h1>
                </div>
               <ul class="form">
                    <li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarDocen()" /></li>
                </ul>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_box_email()"><i class="fa fa-envelope"></i> Email</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Clave</td>
                                <td>Nombre</td>
                                <td>Telefono</td>
                                <td>Email</td>
                                <td>Estatus</td>
                                <td class="td-center"><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                                <td class="td-center">Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                            $count=1;
                            $query="SELECT * FROM Docentes WHERE Id_plantel=" . $_usu->getId_plantel() . " ORDER BY Id_docen DESC";
                            foreach($DaoDocentes->advanced_query($query) as $k=>$v){ 
                               $status="Activo"; 
                               $color="color:green";
                               if(strlen($v->getBaja_docen())>0){
                                  $status="Inactivo";
                                  $color="color:red";
                               }
                            ?>
                                <tr id-data="<?php echo $v->getId()?>" style="<?php echo $color;?>">
                                     <td onclick="mostrar(<?php echo $v->getId()?>)"><?php echo $count;?></td>
                                        <td><?php echo $v->getClave_docen();?></td>
                                         <td><?php echo $v->getNombre_docen()." ".$v->getApellidoP_docen()." ".$v->getApellidoM_docen()?></td>
                                         <td><?php echo $v->getTelefono_docen()?></td>
                                         <td><?php echo $v->getEmail_docen()?></td>
                                         <td><?php echo $status?></td>
                                         <td class="td-center"><input type="checkbox"> </td>
                                         <td class="td-center">
                                         <button onclick="mostrar(<?php echo $v->getId() ?>)">Editar</button>
                                         <?php
                                         if(strlen($v->getBaja_docen())>0){
                                         ?>
                                           <button onclick="reactivarDocente(<?php echo $v->getId()?>)">Dar de alta</button>
                                         <?php
                                         }else{
                                             ?>
                                             <button onclick="verificarGrupos(<?php echo $v->getId()?>)">Dar de baja</button>
                                             <?php
                                         }
                                         ?>
                                         </td>
                                 </tr>
                           <?php
                           $count++; 
                           }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div id="box_button_mostrar"><span id="mostrar" onclick="mostrar_inte()">Mostrar m&aacute;s</span></div>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <h2><img src="images/flecha_blue.png" alt="flecha_blue" width="6" height="11" />Opciones Docente</h2>
                <ul>
                    <li><a href="docente.php" class="link">Nuevo</a></li>
                    <!--<li><span onclick="mostrar_box_busqueda()">Buscar</span></li>-->
                </ul>
            </div>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<script src="js/ckeditor/ckeditor.js"></script>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();
