<?php
require_once('estandares/includes.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoEspecialidades.php');

links_head("Oferta acad&eacute;mica  ");
write_head_body();
write_body();

$DaoOfertas = new DaoOfertas();
$DaoEspecialidades = new DaoEspecialidades();

$text = "Nueva";
$readonly = '';
$Id_ofe = 0;
$Nombre_esp = "";
$RegistroValidez_Oficial = "";
$Institucion_certifica = "";
$Modalidad = "";
$DuracionSemanas = "";
$ClavePlan_estudios = "";
$Escala_calificacion = "";
$Minima_aprobatotia = "";
$Limite_faltas = "";
$Tipo_insc = "";
$Tipo_ciclos = "";
$Num_pagos = "";
$Num_ciclos = "";
$Precio_curso = "";
$Inscripcion_curso = "";
$Id_esp = 0;
$tipoPlan = 0;
//$tipoPlan 1=plan con ciclos,2 plan sin ciclos
if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $esp = $DaoEspecialidades->show($_REQUEST['id']);
    $Nombre_esp = $esp->getNombre_esp();
    $RegistroValidez_Oficial = $esp->getRegistroValidez_Oficial();
    $Institucion_certifica = $esp->getInstitucion_certifica();
    $Modalidad = $esp->getModalidad();
    $DuracionSemanas = $esp->getDuracionSemanas();
    $ClavePlan_estudios = $esp->getClavePlan_estudios();
    $Escala_calificacion = $esp->getEscala_calificacion();
    $Minima_aprobatotia = $esp->getMinima_aprobatotia();
    $Limite_faltas = $esp->getLimite_faltas();
    $Tipo_insc = $esp->getTipo_insc();
    $Tipo_ciclos = $esp->getTipo_ciclos();
    $Num_pagos = $esp->getNum_pagos();
    $Num_ciclos = $esp->getNum_ciclos();
    $Precio_curso = $esp->getPrecio_curso();
    $Inscripcion_curso = $esp->getInscripcion_curso();
    $Id_ofe = $esp->getId_ofe();
    $ofe = $DaoOfertas->show($Id_ofe);
    $tipoPlan = $ofe->getTipoOferta();
    $text = "Editar";
    $Id_esp = $_REQUEST['id'];
}

if (isset($_REQUEST['id_ofe']) && $_REQUEST['id_ofe'] > 0) {
    $Id_ofe = $_REQUEST['id_ofe'];
    $ofe = $DaoOfertas->show($Id_ofe);
    $tipoPlan = $ofe->getTipoOferta();
}

$tipo = "";
if (isset($_REQUEST['tipo']) && $_REQUEST['tipo'] > 0) {
    $tipo = $_REQUEST['tipo'];
}
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div class="seccion">
                    <div class="box_top">
                        <h1><i class="fa fa-cog" aria-hidden="true"></i> <?php echo $text ?> especialidad</h1>
                    </div>
                    <ul class="form">
                        <li><span class="campo-requerido">*</span>Nombre<br><input type="text" value="<?php echo $Nombre_esp ?>" id="nombre_curso"/></li>
                        <li><span class="campo-requerido">*</span>Nivel ac&aacute;demico<br><select id="oferta">
                                <option></option>
                                <?php
                                foreach ($DaoOfertas->showAll() as $oferta) {
                                    ?>
                                    <option value="<?php echo $oferta->getId() ?>" <?php if ($Id_ofe == $oferta->getId() || ($Id_ofe > 0 && $Id_ofe == $oferta->getId())) { ?> selected="selected" <?php } ?>>
                                        <?php echo $oferta->getNombre_oferta() ?>
                                    </option>
                                    <?php
                                }
                                ?>

                            </select></li>
                        <li>Registro de validez oficial<br><input type="text" value="<?php echo $RegistroValidez_Oficial ?>" id="resgistro" <?php echo $readonly ?>/></li>
                        <li>Instituci&oacute;n que certifica<br><input type="text" value="<?php echo $Institucion_certifica ?>" id="insti" <?php echo $readonly ?>/></li>
                        <li>Modalidad<br><select id="modalidad">
                                <option></option>
                                <option value="1" <?php if ($Modalidad == 1) { ?> selected="selected"<?php } ?>>Escolarizada</option>
                                <option value="2" <?php if ($Modalidad == 2) { ?> selected="selected"<?php } ?>>Virtual</option>
                                <option value="3" <?php if ($Modalidad == 3) { ?> selected="selected"<?php } ?>>Semiescolarizada</option>
                                <option value="4" <?php if ($Modalidad == 4) { ?> selected="selected"<?php } ?>>Mixta</option>
                            </select> </li>
                        <li>Clave del plan de Estudios<br><input type="email" id="clave_Plan" value="<?php echo $ClavePlan_estudios ?>" <?php echo $readonly ?>/></li>
                        <li>Duraci&oacute;n (número de semanas)<br><input type="number" id="duracion" value="<?php echo $DuracionSemanas ?>" <?php echo $readonly ?>/></li>
                        <li><span class="campo-requerido">*</span>Número de grados<br><input type="number" id="Num_ciclos" value="<?php echo $Num_ciclos ?>" /></li>
                        <li><span class="campo-requerido">*</span>Escala de Calificacion<br><select id="escala">
                                <option></option>
                                <option value="1" <?php if ($Escala_calificacion == 1) { ?> selected="selected"<?php } ?>>0-10</option>
                                <option value="2" <?php if ($Escala_calificacion == 2) { ?> selected="selected"<?php } ?>>10-100</option>
                            </select></li>
                        <li><span class="campo-requerido">*</span>Calificación mínima aprobatoria<br><input type="number" id="minima_apro" value="<?php echo $Minima_aprobatotia ?>"/></li>
                        <li>Limite de Faltas (porcentaje)<br><input type="text" id="limite_faltas" value="<?php echo $Limite_faltas ?>"/></li>
                    </ul>
                </div> 
                <div class="seccion">
                    <h2>Opciones de cobranza</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Tipo de inscripci&oacute;n<br><select id="tipo_insc">
                                <option></option>
                                <option value="1" <?php if ($Tipo_insc == 1) { ?> selected="selected"<?php } ?>>Inscripci&oacute;n Anual</option>
                                <option value="2" <?php if ($Tipo_insc == 2) { ?> selected="selected"<?php } ?>>Inscripci&oacute;n Cuatrimestral</option>
                                <option value="3" <?php if ($Tipo_insc == 3) { ?> selected="selected"<?php } ?>>Inscripci&oacute;n Unica</option>
                            </select> </li>
                        <li><span class="campo-requerido">*</span>Tipo de ciclo<br><select id="tipoCiclo">
                                <option></option>
                                <option value="1" <?php if ($Tipo_ciclos == 1) { ?> selected="selected"<?php } ?>>Semestre</option>
                                <option value="2" <?php if ($Tipo_ciclos == 2) { ?> selected="selected"<?php } ?>>Bimestre</option>
                                <option value="3" <?php if ($Tipo_ciclos == 3) { ?> selected="selected"<?php } ?>>Mensual</option>
                                <option value="4" <?php if ($Tipo_ciclos == 4) { ?> selected="selected"<?php } ?>>Cuatrimestre</option>
                            </select>
                        </li>
                        <li><span class="campo-requerido">*</span>Número de pagos por grado<br><input type="number" id="num_pagos" value="<?php echo $Num_pagos ?>"/></li>
                        <li>Costo de inscripci&oacute;n<br><input type="number" id="Inscripcion_curso" value="<?php echo $Inscripcion_curso ?>"/></li>
                        <li><span class="campo-requerido">*</span>Costo total<br><input type="number" id="Precio_curso" value="<?php echo $Precio_curso ?>"/></li>

                    </ul>
                </div>
                <button id="button_ins" onclick="save_curso()">Guardar</button>
            </div>	
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><span><a href="curso.php?id_ofe=<?php echo $Id_ofe; ?>">Nueva especialidad</a></span></li>
                    <?php
                    if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                        ?>
                        <li><span><a href="plan_estudio.php?id=<?php echo $_REQUEST['id']; ?>">Plan de Estudios</a></span></li>
                        <li><a href="materia.php?id_esp=<?php echo $_REQUEST['id']; ?>" class="link">Agregar Materia</a></li>
                        <?php
                        if ($tipoPlan == 2) {
                            ?>
                            <li><a href="cursos_especialidad.php?id=<?php echo $_REQUEST['id']; ?>" class="link">Agregar curso</a></li>
                            <?php
                        }
                    }

                    if (isset($_REQUEST['id_ofe']) && $_REQUEST['id_ofe'] > 0) {
                        ?>
                        <li><span><a href="oferta.php?id=<?php echo $_REQUEST['id_ofe']; ?>">Regresar</a></span></li>
                        <?php
                    }
                    ?>

                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_curso" value="<?php echo $Id_esp ?>"/>
<input type="hidden" id="tipo" value="<?php echo $tipo ?>"/>
<?php
write_footer();
