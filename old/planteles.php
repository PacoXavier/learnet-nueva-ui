<?php
require_once('estandares/includes.php');
require_once('clases/DaoPlanteles.php');
require_once('clases/DaoDirecciones.php');
require_once('clases/DaoOfertasPlantel.php');

$DaoPlanteles= new DaoPlanteles();
$DaoDirecciones= new DaoDirecciones();
$DaoOfertasPlantel= new DaoOfertasPlantel();
links_head("Planteles  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-building"></i> Planteles</h1>
                </div>
               <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Nombre</td>
                                <td>Ciudad</td>
                                <td>Colonia</td>
                                <td class="td-center">Num.ofertas</td>
                                <td class="td-center">Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count=1;
                            foreach ($DaoPlanteles->showAll() as $k => $v) {
                                    $getCiudad_dir="";
                                    $getEstado_dir="";
                                    $plantel = $DaoPlanteles->show($v->getId());
                                    if ($plantel->getId_dir_plantel() > 0) {
                                        $dir = $DaoDirecciones->show($plantel->getId_dir_plantel());
                                        $getCalle_dir=$dir->getCalle_dir();
                                        $getNumExt_dir=$dir->getNumExt_dir();
                                        $getNumInt_dir=$dir->getNumInt_dir();
                                        $getColonia_dir=$dir->getColonia_dir();
                                        $getCiudad_dir=$dir->getCiudad_dir();
                                        $getEstado_dir=$dir->getEstado_dir();
                                        $getCp_dir=$dir->getCp_dir();
                                        $Id_dir=$plantel->getId_dir_plantel();
                                     }
         
                                ?>
                                <tr>
                                    <td><?php echo $count ?></td>
                                    <td><?php echo $v->getNombre_plantel() ?></td>
                                    <td><?php echo $getCiudad_dir;?></td>
                                    <td><?php echo $getEstado_dir;?></td>
                                    <td class="td-center"><?php echo count($DaoOfertasPlantel->getOfertasPlantel($v->getId()))?></td>
                                    <td class="td-center">
                                        <button onclick="mostrar(<?php echo $v->getId() ?>)">Editar</button>
                                        <button onclick="delete_plantel(<?php echo $v->getId() ?>)">Eliminar</button>
                                    </td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <table id="usu_login">
                    <?php
                    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                    $usu = $class_usuarios->get_usu();
                    ?>
                    <tr>
                        <td><div class="nom_usu"><div id="img_usu" 
                                                      <?php if (strlen($usu['Img_usu']) > 0) { ?> style="background-image:url(files/<?php echo $usu['Img_usu'] ?>.jpg) <?php } ?>"></div>
<?php echo $usu['Nombre_usu'] . " " . $usu['ApellidoP_usu'] . " " . $usu['ApellidoM_usu'] ?>
                                <img src="images/linea_uno.png" alt="linea_uno" width="185" height="5" class="lineas"/></div>
                        </td>
                        <td>
                            <div class="opcion">
                                <a href="perfil.php?id=<?php echo $usu['Id_usu'] ?>">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Mi perfil</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/>
                            </div>
                        </td>
                        <td><div class="opcion">
                                <a href="logout.php">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Salir</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/></div>
                        </td>
                    </tr>
                </table>
                <h2><img src="images/flecha_blue.png" alt="flecha_blue" width="6" height="11" />Opciones</h2>
                <ul>
                    <li><a href="plantel.php" class="link">Nuevo</a></li>
                    <!--<li><span onclick="mostrar_box_busqueda()">Buscar</span></li>-->
                </ul>
            </div>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<?php
write_footer();


