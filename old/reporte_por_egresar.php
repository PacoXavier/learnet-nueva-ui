<?php
set_time_limit(0);
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoGrupos.php');
require_once('clases/DaoMateriasCicloAlumno.php');
require_once('clases/DaoMateriasEspecialidad.php');


$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoGrados= new DaoGrados();
$DaoMateriasEspecialidad= new DaoMateriasEspecialidad();

links_head("Por egresar  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Alumnos por egresar</h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Filtros</li>
                        <!--<li onclick="mostrar_box_email()"><i class="fa fa-envelope"></i> Email</li>-->
                        <li onclick="download_excel()"><i class="fa fa-download"></i> Descargar</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td>Grado</td>
                                <td>Opcion de pago</td>
                                <td>Materias cursadas</td>
                                <!--<td><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>-->
                                <td>Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($DaoAlumnos->getAlumnos() as $k => $v) {
                                $nombre_ori = "";
                                $oferta = $DaoOfertas->show($v['Id_ofe']);
                                $esp = $DaoEspecialidades->show($v['Id_esp']);
                                if ($v['Id_ori'] > 0) {
                                    $ori = $DaoOrientaciones->show($v['Id_ori']);
                                    $nombre_ori = $ori->getNombre();
                                }
                                $opcion = "Plan por materias";
                                if ($v['Opcion_pago'] == 2) {
                                    $opcion = "Plan completo";
                                }

                                $Grado = $DaoOfertasAlumno->getLastCicloOfertaSinCiclo($v['Id_ofe_alum']);
                                $Grado = $DaoGrados->show($Grado['Id_grado']);
                             
                                $statusOferta=$DaoAlumnos->getStatusOferta($v['Id_ofe_alum']);
                                $MateriasCursadas = $statusOferta['cant_mat_cursadas'];
                                if ($statusOferta['status'] == 1) {
                                    ?>
                                    <tr id_alum="<?php echo $v['Id_ins']; ?>" id-ofe-alum="<?php echo $v['Id_ofe_alum']?>">
                                        <td><?php echo $v['Matricula'] ?></td>
                                        <td style="width: 115px;"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                                        <td><?php echo $oferta->getNombre_oferta(); ?></td>
                                        <td><?php echo $esp->getNombre_esp(); ?></td>
                                        <td><?php echo $nombre_ori; ?></td>
                                        <td><?php echo $Grado->getGrado(); ?></td>
                                        <td><?php echo $opcion; ?></td>
                                        <td style="text-align: center;"><?php echo $MateriasCursadas; ?></td>
                                        <!--<td><input type="checkbox"> </td>-->
                                        <td><button onclick="mostrar_box_egresar(<?php echo $v['Id_ofe_alum']; ?>)">Egresar</button></td>
                                    </tr>
                                    <?php
                                }
                            }
                            
                            
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h1>Filtros</h1>
    <p>Oferta<br>
        <select id="oferta" onchange="update_curso_box_curso()">
            <option value="0"></option>
            <?php
            foreach ($DaoOfertas->showAll() as $k => $v) {
                ?>
                <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
                <?php
            }
            ?>
        </select>
    </p>
    <p>Especialidad:<br>
        <select id="curso" onchange="update_orientacion_box_curso()">
            <option value="0"></option>
        </select>
    </p>
    <div id="box_orientacion"></div>
    <!--
    <p>Grado:<br>
        <select id="grado">
          <option value="0"></option>
        </select>
    </p>
    <p>Opci&oacute;n de pago:<br>
        <select id="opcion">
          <option value="0"></option>
          <option value="1">POR MATERIAS</option>
          <option value="2">PLAN COMPLETO</option>
        </select>
    </p>
    -->
    <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<script src="js/ckeditor/ckeditor.js"></script>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();
