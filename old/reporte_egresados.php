<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoMediosEnterar.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoMateriasEspecialidad.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoMotivosBaja.php');

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoCiclos= new DaoCiclos();

links_head("Egresados ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                   <h1><i class="fa fa-graduation-cap"></i> Alumnos egresados</h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Filtros</li>
                        <li onclick="mostrar_box_email()"><i class="fa fa-envelope"></i> Email</li>
                        <li onclick="download_excel()"><i class="fa fa-download"></i> Descargar</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td>Opcion de pago</td>
                                <td>Ciclo</td>
                                <td style="text-align: center;"><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $ciclo=$DaoCiclos->getActual();
                            if($ciclo->getId()>0){
                             $query=" AND IdCicloEgreso=".$ciclo->getId();
                             foreach ($DaoAlumnos->getAlumnosEgresados($query,null,null) as $k=>$v){
                                       $nombre_ori="";
                                       $oferta = $DaoOfertas->show($v['Id_ofe']);
                                       $esp = $DaoEspecialidades->show($v['Id_esp']);
                                       if ($v['Id_ori'] > 0) {
                                          $ori = $DaoOrientaciones->show($v['Id_ori']);
                                          $nombre_ori = $ori->getNombre();
                                        }
                                        $opcion="Plan por materias"; 
                                        if($v['Opcion_pago']==2){
                                          $opcion="Plan completo";  
                                        }
                                        $ciclo=$DaoCiclos->show($v['IdCicloEgreso']);
                                      ?>
                                              <tr id_alum="<?php echo $v['Id_ins'];?>">
                                                  <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $v['Matricula'] ?></td>
                                                <td style="width: 115px;" onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $v['Nombre_ins'] . " " . $v['ApellidoP_ins'] . " " . $v['ApellidoM_ins'] ?></td>
                                                <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $oferta->getNombre_oferta(); ?></td>
                                                <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $esp->getNombre_esp(); ?></td>
                                                <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $nombre_ori; ?></td>
                                                <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $opcion; ?></td>
                                                <td onclick="alumno(<?php echo $v['Id_ins'];?>)"><?php echo $ciclo->getClave(); ?></td>
                                                <td style="text-align: center;"><input type="checkbox"> </td>
                                              </tr>
                                              <?php
                                    }
                                    
                                   }
                                  ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
        <p>Matrícula<br><input type="search"  id="matricula"/></p>
	<p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"><?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso()">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
        <p>Ciclo<br>
          <select id="Id_ciclo">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<script src="js/ckeditor/ckeditor.js"></script>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();
