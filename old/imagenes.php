<?php
require_once('estandares/includes.php');
require_once('estandares/class_imagenes.php');
links_head("Imagenes  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-file-image-o"></i> Im&aacute;genes</h1>
                    <p style="font-size: 15px;margin-top: 10px;">Tamaño m&aacute;ximo de ancho 540px</p>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <span class="spanfiltros" onclick="mostrarFinder()">Seleccionar im&aacute;gen</span>
                <div id="mascara_tabla">
                    <ul id="lista-imagenes">
                        <?php
                        $class_imagenes= new class_imagenes();
                        foreach($class_imagenes->get_imagenes()as $k=>$v){
                            $ruta="imagenes/".$v['Llave'].".jpg";
                        ?>
                        <li onclick="delete_img(<?php echo $v['Id']?>)" style="background-image: url(<?php echo $ruta;?>)"></li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </td>
    </tr>
</table>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();
