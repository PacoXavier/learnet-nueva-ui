<?php
require_once('estandares/includes.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoGrupos.php');
require_once('clases/DaoPermutasClase.php');
require_once('clases/DaoHoras.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/modelos/PermutasClase.php');
require_once('clases/modelos/base.php');

$DaoAlumnos= new DaoAlumnos();
$DaoCiclos= new DaoCiclos();
$DaoUsuarios= new DaoUsuarios();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoEspecialidades= new DaoEspecialidades();


links_head("Aplicar becas  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-asterisk"></i> Aplicar beca</h1>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <?php
                        $nombre="";
                        if ($_REQUEST['id'] > 0) {
                            $alumno = $DaoAlumnos->show($_REQUEST['id']);
                            $nombre=$alumno->getNombre()." ".$alumno->getApellidoP()." ".$alumno->getApellidoM();
                        }
                        ?>
                        <li>Buscar Alumno<br><input type="text" id="email_int"  onkeyup="buscar_int()" value="<?php echo $nombre ?>"/>
                            <ul id="buscador_int"></ul>
                        </li><br>
                        <?php
                        if ($_REQUEST['id'] > 0) {
                        ?>
                        <li>Especialidad<br>
                            <select id="OfertaAlumno" onchange="getCiclosBeca()">
                                <option value="0">Selecciona una especialidad</option>
                              <?php
                              foreach ($DaoOfertasAlumno->getOfertasAlumno($_REQUEST['id']) as $k => $v) {
                                  $esp = $DaoEspecialidades->show($v->getId_esp());
                              ?>
                                <option value="<?php echo $v->getId() ?>"><?php echo $esp->getNombre_esp() ?></option>
                              <?php
                              }
                              ?>

                            </select></li>
                            <?php
                         }
                        ?>
                    </ul>
                    <div id="box-info"></div>
                </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_alum" value="<?php echo $_REQUEST['id']; ?>"/>
<div class="box-dias-ciclo"></div>
<?php
write_footer();


