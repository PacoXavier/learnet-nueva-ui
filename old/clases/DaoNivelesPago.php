<?php
require_once 'modelos/base.php';
require_once 'modelos/NivelesPago.php';

class DaoNivelesPago extends base{
    
        public $table="Niveles_docentes";
    
	public function add(NivelesPago $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Nombre_nivel, Puntos_nivel, Puntos_nivel_max,Pago_nivel,Id_tipo_nivel,Activo) VALUES (%s, %s,%s,%s, %s,%s)",
            $this->GetSQLValueString($x->getNombre_nivel(), "text"), 
            $this->GetSQLValueString($x->getPuntosMin(), "int"), 
            $this->GetSQLValueString($x->getPuntosMax(), "int"),
            $this->GetSQLValueString($x->getPagoNivel(), "double"),
            $this->GetSQLValueString($x->getId_tipo_nivel(), "int"),
            $this->GetSQLValueString($x->getActivo(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(NivelesPago $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Nombre_nivel=%s, Puntos_nivel=%s, Puntos_nivel_max=%s,Pago_nivel=%s,Id_tipo_nivel=%s,Activo=%s WHERE Id_nivel=%s",
            $this->GetSQLValueString($x->getNombre_nivel(), "text"), 
            $this->GetSQLValueString($x->getPuntosMin(), "int"), 
            $this->GetSQLValueString($x->getPuntosMax(), "int"),
            $this->GetSQLValueString($x->getPagoNivel(), "double"),
            $this->GetSQLValueString($x->getId_tipo_nivel(), "int"),
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	
	public function delete($Id){
            $query = sprintf("UPDATE ".$this->table." SET Activo=0 WHERE Id_nivel =%s",
            $this->GetSQLValueString($Id, "int")); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	public function show($Id){
	    $query=sprintf("SELECT * FROM ".$this->table." WHERE Id_nivel=%s",
            $this->GetSQLValueString($Id, "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new NivelesPago();
            $x->setId($row['Id_nivel']);
            $x->setNombre_nivel($row['Nombre_nivel']);
            $x->setPuntosMin($row['Puntos_nivel']);
            $x->setPuntosMax($row['Puntos_nivel_max']);
            $x->setPagoNivel($row['Pago_nivel']);
            $x->setId_tipo_nivel($row['Id_tipo_nivel']);
            $x->setActivo($row['Activo']);
            return $x;
	}


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getNivelesCategoria($Id_cat){
            $query="SELECT * FROM ".$this->table." WHERE Activo=1 AND Id_tipo_nivel=".$Id_cat." ORDER BY Id_nivel ASC";
            return $this->advanced_query($query);
        }

}
