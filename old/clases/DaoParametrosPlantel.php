<?php
require_once 'modelos/base.php';
require_once 'modelos/ParametrosPlantel.php';
require_once 'DaoParametros.php';

class DaoParametrosPlantel extends base{
	public function add(ParametrosPlantel $x){
	    $query=sprintf("INSERT INTO ParametrosPlantel (Id_param, Id_plantel, Valor) VALUES (%s, %s,%s)",
            $this->GetSQLValueString($x->getId_param(), "int"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"), 
            $this->GetSQLValueString($x->getValor(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(ParametrosPlantel $x){
	    $query=sprintf("UPDATE ParametrosPlantel SET Id_param=%s, Id_plantel=%s, Valor=%s WHERE Id=%s",
            $this->GetSQLValueString($x->getId_param(), "int"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"), 
            $this->GetSQLValueString($x->getValor(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM ParametrosPlantel WHERE Id =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM ParametrosPlantel WHERE Id= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function existeParametro($Id_param){
	    $query="SELECT * FROM ParametrosPlantel WHERE Id_param= ".$Id_param." AND Id_plantel=".$this->Id_plantel;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        

	public function create_object($row){
            $x = new ParametrosPlantel();
            $x->setId($row['Id']);
            $x->setId_param($row['Id_param']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setValor($row['Valor']);
            return $x;
	}

                
	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getParametrosPlantel($Id_plantel){
            $query="SELECT * FROM ParametrosPlantel WHERE Id_plantel=".$Id_plantel;
            return $this->advanced_query($query);
        }
        
        public function getParametrosPlantelArray(){
            $params=array();
            $DaoParametros= new DaoParametros();
            $query="SELECT * FROM ParametrosPlantel WHERE Id_plantel=".$this->Id_plantel;
            foreach($this->advanced_query($query) as $param){
                if(strlen($param->getValor())>0){
                   $parametro=$DaoParametros->mostrarParametro($param->getId_param());
                   $params[$parametro->getText_param()]=$param->getValor();  
                }
            }
            return $params;
        }
        
        
        public function checkGoogleTokenDrivePlantel($params){
            $headers=array();
            array_push($headers, "Authorization: Bearer ".$params['AccessTokenGoogleDrive']);
            $url="https://www.googleapis.com/plus/v1/people/me";
            $user_info=$this->simpleCurl("GET", $headers, $url);
            $user_info=json_decode($user_info);
            if(count($user_info->error)>0){
                    $headers=array();
                    $url="https://accounts.google.com/o/oauth2/token";
                    $data=array();
                    $data["client_id"]="client_id=".$params['ClientIdGoogle'];
                    $data["client_secret"]="client_secret=".$params['ClientSecretGoogle'];
                    $data["refresh_token"]="refresh_token=".$params['RefreshTokenGoogleDrive'];
                    $data["grant_type"]="grant_type=refresh_token";
                    $access_token=$this->simpleCurl("POST", $headers, $url, $data);
                    $access_token=json_decode($access_token);
                    if(strlen($access_token->access_token)>0){
                        $ParametrosPlantel=$this->existeParametro(22);
                        if($ParametrosPlantel->getId()>0){
                            $ParametrosPlantel->setValor($access_token->access_token);
                            $this->update($ParametrosPlantel);
                        }
                    }
                    return $access_token;
            }else{
                return true;
            }
    }


}
