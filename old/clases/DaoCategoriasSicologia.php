<?php
require_once 'modelos/base.php';
require_once 'modelos/CategoriasSicologia.php';

class DaoCategoriasSicologia extends base{

	public function add(CategoriasSicologia $x){
              $query=sprintf("INSERT INTO CategoriasSicologia (Nombre,Activo,Id_plantel) VALUES (%s ,%s, %s)",
              $this->GetSQLValueString($x->getNombre(), "text"),
              $this->GetSQLValueString($x->getActivo(), "int"),
              $this->GetSQLValueString($x->getId_plantel(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(CategoriasSicologia $x){
	    $query=sprintf("UPDATE CategoriasSicologia SET Nombre=%s, Activo=%s, Id_plantel=%s WHERE Id = %s",
              $this->GetSQLValueString($x->getNombre(), "text"),
              $this->GetSQLValueString($x->getActivo(), "int"),
              $this->GetSQLValueString($x->getId_plantel(), "int"),
              $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}
	
	
	
	public function delete($Id){
                $query = sprintf("DELETE FROM CategoriasSicologia WHERE Id=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
	
	public function show($Id){
	    $query="SELECT * FROM CategoriasSicologia WHERE Id= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM CategoriasSicologia WHERE Activo=1 AND  Id_plantel=".$this->Id_plantel." ORDER BY Orden ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
        
        public function create_object($row){
            $x = new CategoriasSicologia();
            $x->setId($row['Id']);
            $x->setNombre($row['Nombre']);
            $x->setActivo($row['Activo']); 
            $x->setId_plantel($row['Id_plantel']); 
            $x->setOrden($row['Orden']);
            $x->setRangoMax($row['RangoMax']);
            return $x;
	}
        

        public function advanced_query($query){
		    $resp=array();
		    $consulta=$this->_cnn->query($query);
		    if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	}

	

}

