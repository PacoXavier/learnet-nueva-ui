<?php
require_once 'modelos/base.php';
require_once 'modelos/PermutasClase.php';

class DaoPermutasClase extends base{
	public function add(PermutasClase $x){
	    $query=sprintf("INSERT INTO PermutasClase (Id_grupo, Id_docente_asignado, Dia, DateCreated, Id_usu, Id_ciclo,Comentario,Id_hora,DiaNoCubierto, Id_aula) VALUES (%s, %s,%s, %s,%s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getId_grupo(), "int"), 
            $this->GetSQLValueString($x->getId_docente_asignado(), "int"), 
            $this->GetSQLValueString($x->getDia(), "date"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getComentario(), "text"),
            $this->GetSQLValueString($x->getId_hora(),"int"),
            $this->GetSQLValueString($x->getDiaNoCubierto(), "date"),
            $this->GetSQLValueString($x->getId_aula(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(PermutasClase $x){
	    $query=sprintf("UPDATE PermutasClase SET  Id_grupo=%, Id_docente_asignado=%, Dia=%, DateCreated=%, Id_usu=%, Id_ciclo=%,Comentario=%,Id_hora=%s,DiaNoCubierto=%s, Id_aula=%s WHERE Id_perm=%s",
            $this->GetSQLValueString($x->getId_grupo(), "int"), 
            $this->GetSQLValueString($x->getId_docente_asignado(), "int"), 
            $this->GetSQLValueString($x->getDia(), "date"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getComentario(), "text"),
            $this->GetSQLValueString($x->getId_hora(),"int"),
            $this->GetSQLValueString($x->getDiaNoCubierto(), "date"),
            $this->GetSQLValueString($x->getId_aula(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM PermutasClase  WHERE Id_perm =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM PermutasClase WHERE Id_perm= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new PermutasClase();
            $x->setId($row['Id_perm']);
            $x->setId_grupo($row['Id_grupo']);
            $x->setId_docente_asignado($row['Id_docente_asignado']);
            $x->setDia($row['Dia']);
            $x->setDateCreated($row['DateCreated']);
            $x->setId_usu($row['Id_usu']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setComentario($row['Comentario']);
            $x->setId_hora($row['Id_hora']);
            $x->setDiaNoCubierto($row['DiaNoCubierto']);
            $x->setId_aula($row['Id_aula']);
            return $x;
	}
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getPermutasDocenteByCiclo($Id_doc,$Id_ciclo,$FechaIni=null,$FechaFin=null){
            $resp=array();
            $query_fechas = "";
            if (strlen($FechaIni) > 0 && $FechaFin == null) {
                $query_fechas = $query_fechas . " AND Dia>='" . $FechaIni . "'";
            } elseif (strlen($FechaFin) > 0 && $FechaIni == null) {
                $query_fechas = $query_fechas . " AND Dia<='" . $FechaFin . "'";
            } elseif (strlen($FechaIni) > 0 && strlen($FechaFin) > 0) {
                $query_fechas = $query_fechas . " AND (Dia>='" . $FechaIni . "' AND Dia<='" . $FechaFin . "')";
            }
            $query= "SELECT * FROM PermutasClase WHERE Id_docente_asignado=".$Id_doc." AND Id_ciclo=".$Id_ciclo." ".$query_fechas." ORDER BY Dia DESC";
            foreach($this->advanced_query($query) as $k=>$v){
                array_push($resp, $v);
            }
            return $resp;
        } 

        public function existePermutaGrupo($Id_grupo,$Id_docente,$Fecha){
            $query= "SELECT * FROM PermutasClase WHERE Id_grupo=".$Id_grupo." AND Id_docente_asignado!=".$Id_docente." AND Dia='".$Fecha."'";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
        } 
        
        public function getPermutaDocenteGrupoFecha($Id_docente,$Id_grupo,$Fecha){
            $query= "SELECT * FROM PermutasClase WHERE Id_grupo=".$Id_grupo." AND Id_docente_asignado=".$Id_docente." AND Dia='".$Fecha."'";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
        } 
        
        public function getPermutasDocenteGrupo($Id_docente,$Id_grupo){
             $query= "SELECT * FROM PermutasClase WHERE Id_grupo=".$Id_grupo." AND Id_docente_asignado=".$Id_docente." GROUP BY Dia";
             return $this->advanced_query($query);
        } 
 
        public function getPermutasGrupo($Id_grupo){
             $query= "SELECT * FROM PermutasClase WHERE Id_grupo=".$Id_grupo." GROUP BY Dia";
             return $this->advanced_query($query);
        } 

}
