<?php
require_once 'modelos/base.php';
require_once 'modelos/PenalizacionDocente.php';

class DaoPenalizacionDocente extends base{

	public function add(PenalizacionDocente $x){
              $query=sprintf("INSERT INTO PenalizacionDocente (DateCreated,Id_usu_created,Fecha, Grupo, Id_ciclo, Comentarios, Hora, Id_docen) VALUES (%s ,%s, %s, %s, %s, %s, %s, %s)",
              $this->GetSQLValueString($x->getDateCreated(), "date"),
              $this->GetSQLValueString($x->getId_usu_created(), "int"),
              $this->GetSQLValueString($x->getFecha(), "date"),
              $this->GetSQLValueString($x->getGrupo(), "int"),
              $this->GetSQLValueString($x->getId_ciclo(), "int"),
              $this->GetSQLValueString($x->getComentarios(), "text"),
              $this->GetSQLValueString($x->getHora(), "int"),
              $this->GetSQLValueString($x->getId_docen(), "int"));
              $Result1=$this->_cnn->query($query);
              if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
              }
	}

	
	public function update(PenalizacionDocente $x){
	    $query=sprintf("UPDATE PenalizacionDocente SET DateCreated=%s,Id_usu_created=%s,Fecha=%s, Grupo=%s, Id_ciclo=%s, Comentarios=%s, Hora=%s, Id_docen=%s  WHERE Id_pen = %s",
              $this->GetSQLValueString($x->getDateCreated(), "date"),
              $this->GetSQLValueString($x->getId_usu_created(), "int"),
              $this->GetSQLValueString($x->getFecha(), "date"),
              $this->GetSQLValueString($x->getGrupo(), "int"),
              $this->GetSQLValueString($x->getId_ciclo(), "int"),
              $this->GetSQLValueString($x->getComentarios(), "text"),
              $this->GetSQLValueString($x->getHora(), "int"),
              $this->GetSQLValueString($x->getId_docen(), "int"),
              $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}
	
	
	
	public function delete($Id){
                $query = sprintf("DELETE FROM PenalizacionDocente WHERE Id_pen=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
	
	public function show($Id){
	    $query="SELECT * FROM PenalizacionDocente WHERE Id_pen= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	public function getPenalizacionesDocente($Id_docen,$Id_ciclo,$Id_grupo=null,$FechaIni=null,$FechaFin=null){
            $q="";
            if($Id_grupo!=null){
                $q.=" AND Grupo=".$Id_grupo;
            }
            if(strlen($FechaIni)>0 && $FechaFin==null){
                $q.=$q." AND Fecha>='".$FechaIni."'";
            }elseif(strlen($FechaFin)>0 && $FechaIni==null){
                $q.=$q." AND Fecha<='".$FechaFin."'";
            }elseif(strlen($FechaIni)>0 && strlen($FechaFin)>0){
                $q.=$q." AND (Fecha>='".$FechaIni."' AND Fecha<='".$FechaFin."')";
            }
            $query="SELECT * FROM PenalizacionDocente WHERE Id_docen=".$Id_docen." AND Id_ciclo=".$Id_ciclo." ".$q." ORDER BY DateCreated DESC";
             return $this->advanced_query($query);
	}
	
	
	public function create_object($row){
            $x = new PenalizacionDocente();
            $x->setId($row['Id_pen']);
            $x->setDateCreated($row['DateCreated']);
            $x->setId_usu_created($row['Id_usu_created']);
            $x->setGrupo($row['Grupo']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setComentarios($row['Comentarios']);
            $x->setHora($row['Hora']);
            $x->setId_docen($row['Id_docen']);
            $x->setFecha($row['Fecha']);
            return $x;
	}
        
        
        
         public function advanced_query($query){
		    $resp=array();
		    $consulta=$this->_cnn->query($query);
		    if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	}
        
}

