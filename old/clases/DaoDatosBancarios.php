<?php
require_once 'modelos/base.php';
require_once 'modelos/DatosBancarios.php';

class DaoDatosBancarios extends base{
    
	public function add(DatosBancarios $x){
	    $query=sprintf("INSERT INTO DatosBancarios (Banco, Beneficiario, Convenio, Trasferencias, RFC, Id_plantel) VALUES (%s, %s,%s, %s,%s, %s)",
            $this->GetSQLValueString($x->getBanco(), "text"), 
            $this->GetSQLValueString($x->getBeneficiario(), "text"), 
            $this->GetSQLValueString($x->getConvenio(), "text"), 
            $this->GetSQLValueString($x->getTrasferencias(), "text"), 
            $this->GetSQLValueString($x->getRFC(), "text"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(DatosBancarios $x){
	    $query=sprintf("UPDATE DatosBancarios SET  Banco=%s, Beneficiario=%s, Convenio=%s, Trasferencias=%s, RFC=%s, Id_plantel=%s WHERE Id=%s",
            $this->GetSQLValueString($x->getBanco(), "text"), 
            $this->GetSQLValueString($x->getBeneficiario(), "text"), 
            $this->GetSQLValueString($x->getConvenio(), "text"), 
            $this->GetSQLValueString($x->getTrasferencias(), "text"), 
            $this->GetSQLValueString($x->getRFC(), "text"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM DatosBancarios WHERE Id =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM DatosBancarios WHERE Id= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM DatosBancarios WHERE  Id_plantel=".$this->Id_plantel;
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new DatosBancarios();
            $x->setId($row['Id']);
            $x->setBanco($row['Banco']);
            $x->setBeneficiario($row['Beneficiario']);
            $x->setConvenio($row['Convenio']);
            $x->setTrasferencias($row['Trasferencias']);
            $x->setRFC($row['RFC']);
            $x->setId_plantel($row['Id_plantel']);
            return $x;
	}
        


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getDatosBancariosPlantel($Id_plantel){
            $query="SELECT * FROM DatosBancarios WHERE Id_plantel=".$Id_plantel;
             return  $this->advanced_query($query);
        }


}
