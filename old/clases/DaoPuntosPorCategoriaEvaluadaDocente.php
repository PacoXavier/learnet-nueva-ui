<?php
require_once 'modelos/base.php';
require_once 'modelos/PuntosPorCategoriaEvaluadaDocente.php';

class DaoPuntosPorCategoriaEvaluadaDocente extends base{
    
        public $table="PuntosPorCategoriaEvaluadaDocente";
    
	public function add(PuntosPorCategoriaEvaluadaDocente $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_nivel,Pago_por_hora,Id_eva) VALUES (%s, %s,%s)",
            $this->GetSQLValueString($x->getId_nivel(), "int"), 
            $this->GetSQLValueString($x->getPago_por_hora(), "double"),
            $this->GetSQLValueString($x->getId_eva(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(PuntosPorCategoriaEvaluadaDocente $x){
	    $query=sprintf("UPDATE ".$this->table." SET Id_nivel=%s, Pago_por_hora=%s,Id_eva=%s WHERE Id=%s",
            $this->GetSQLValueString($x->getId_nivel(), "int"), 
            $this->GetSQLValueString($x->getPago_por_hora(), "double"),
            $this->GetSQLValueString($x->getId_eva(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	
	public function delete($Id){
            $query = sprintf("DELETE FROM ".$this->table." WHERE Id =%s",
            $this->GetSQLValueString($Id, "int")); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function deletePorEvaluacion($Id){
            $query = sprintf("DELETE FROM ".$this->table." WHERE Id_eva =%s",
            $this->GetSQLValueString($Id, "int")); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	public function show($Id){
	    $query=sprintf("SELECT * FROM ".$this->table." WHERE Id=%s",
            $this->GetSQLValueString($Id, "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new PuntosPorCategoriaEvaluadaDocente();
            $x->setId($row['Id']);
            $x->setId_nivel($row['Id_nivel']);
            $x->setPago_por_hora($row['Pago_por_hora']);
            $x->setId_eva($row['Id_eva']);
            return $x;
	}


                
	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                   do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getPuntosPorCategoriaEvaluacion($Id_eva,$Id_nivel){
	    $query=sprintf("SELECT * FROM ".$this->table." WHERE Id_eva=%s AND Id_nivel=%s",
            $this->GetSQLValueString($Id_eva, "int"),
            $this->GetSQLValueString($Id_nivel, "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function getNivelPorCategoria($Id_eva,$Id_cat){
	    $query=sprintf("SELECT * FROM ".$this->table." 
             JOIN Niveles_docentes ON ".$this->table.".Id_nivel=Niveles_docentes.Id_nivel
             JOIN Tipos_docente ON Niveles_docentes.Id_tipo_nivel=Tipos_docente.Id_tipo
             WHERE ".$this->table.".Id_eva=%s AND Tipos_docente.Id_tipo=%s",
            $this->GetSQLValueString($Id_eva, "int"),
            $this->GetSQLValueString($Id_cat, "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
             

}
