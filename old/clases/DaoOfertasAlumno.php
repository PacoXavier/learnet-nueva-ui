<?php

require_once 'modelos/base.php';
require_once 'modelos/OfertasAlumno.php';

class DaoOfertasAlumno extends base {

    public function add(OfertasAlumno $x) {
        $query = sprintf("INSERT INTO ofertas_alumno (Id_ofe, Id_alum, Id_esp, Id_ori, beca,
            Opcion_pago, Tipo_beca, Activo_oferta, Turno, Alta_ofe, Baja_ofe, Id_mot_baja, 
            Comentario_baja, UltimaGeneracion_credencial, Id_usu_captura_baja, FechaCapturaEgreso, 
            IdCicloEgreso, IdUsuEgreso,GenerarAlerta,Id_curso_esp) VALUES (%s, %s,%s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s,%s)", 
                $this->GetSQLValueString($x->getId_ofe(), "int"), 
                $this->GetSQLValueString($x->getId_alum(), "int"), 
                $this->GetSQLValueString($x->getId_esp(), "int"), 
                $this->GetSQLValueString($x->getId_ori(), "int"), 
                $this->GetSQLValueString($x->getBeca(), "int"), 
                $this->GetSQLValueString($x->getOpcionPago(), "int"), 
                $this->GetSQLValueString($x->getTipo_beca(), "int"), 
                $this->GetSQLValueString($x->getActivo(), "int"), 
                $this->GetSQLValueString($x->getTurno(), "int"), 
                $this->GetSQLValueString($x->getAlta_ofe(), "date"), 
                $this->GetSQLValueString($x->getBaja_ofe(), "date"), 
                $this->GetSQLValueString($x->getId_mot_baja(), "int"), 
                $this->GetSQLValueString($x->getComentario_baja(), "text"), 
                $this->GetSQLValueString($x->getUltimaGeneracion_credencial(), "date"), 
                $this->GetSQLValueString($x->getId_usu_captura_baja(), "int"), 
                $this->GetSQLValueString($x->getFechaCapturaEgreso(), "date"), 
                $this->GetSQLValueString($x->getIdCicloEgreso(), "int"), 
                $this->GetSQLValueString($x->getIdUsuEgreso(), "int"),
                $this->GetSQLValueString($x->getGenerarAlerta(), "int"),
                $this->GetSQLValueString($x->getId_curso_esp(), "int"));
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function update(OfertasAlumno $x) {
        $query = sprintf("UPDATE ofertas_alumno SET Id_ofe=%s, Id_alum=%s, Id_esp=%s, Id_ori=%s, beca=%s, Opcion_pago=%s,
             Tipo_beca=%s, Activo_oferta=%s, Turno=%s, Alta_ofe=%s, Baja_ofe=%s, Id_mot_baja=%s, 
            Comentario_baja=%s, UltimaGeneracion_credencial=%s, Id_usu_captura_baja=%s, FechaCapturaEgreso=%s, 
            IdCicloEgreso=%s, IdUsuEgreso=%s ,GenerarAlerta=%s ,Id_curso_esp=%s    
            WHERE Id_ofe_alum=%s", 
                $this->GetSQLValueString($x->getId_ofe(), "int"), 
                $this->GetSQLValueString($x->getId_alum(), "int"), 
                $this->GetSQLValueString($x->getId_esp(), "int"), 
                $this->GetSQLValueString($x->getId_ori(), "int"), 
                $this->GetSQLValueString($x->getBeca(), "int"), 
                $this->GetSQLValueString($x->getOpcionPago(), "int"), 
                $this->GetSQLValueString($x->getTipo_beca(), "int"), 
                $this->GetSQLValueString($x->getActivo(), "int"), 
                $this->GetSQLValueString($x->getTurno(), "int"), 
                $this->GetSQLValueString($x->getAlta_ofe(), "date"), 
                $this->GetSQLValueString($x->getBaja_ofe(), "date"), 
                $this->GetSQLValueString($x->getId_mot_baja(), "int"), 
                $this->GetSQLValueString($x->getComentario_baja(), "text"), 
                $this->GetSQLValueString($x->getUltimaGeneracion_credencial(), "date"), 
                $this->GetSQLValueString($x->getId_usu_captura_baja(), "int"), 
                $this->GetSQLValueString($x->getFechaCapturaEgreso(), "date"), 
                $this->GetSQLValueString($x->getIdCicloEgreso(), "int"), 
                $this->GetSQLValueString($x->getIdUsuEgreso(), "int"), 
                $this->GetSQLValueString($x->getGenerarAlerta(), "int"),
                $this->GetSQLValueString($x->getId_curso_esp(), "int"),
                $this->GetSQLValueString($x->getId(), "int"));
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }

    public function delete($Id) {
        $query = sprintf("UPDATE ofertas_alumno SET Activo_oferta=0 WHERE Id_ofe_alum =" . $Id);
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return true;
        }
    }
    
    public function deleteTrue($Id) {
        $query = sprintf("DELETE FROM ofertas_alumno WHERE Id_ofe_alum =" . $Id);
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return true;
        }
    }

    public function show($Id) {
        $query = "SELECT * FROM ofertas_alumno WHERE Id_ofe_alum= " . $Id;
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->create_object($Result1->fetch_assoc());
        }
    }


    public function create_object($row) {
        $x = new OfertasAlumno();
        $x->setId($row['Id_ofe_alum']);
        $x->setId_ofe($row['Id_ofe']);
        $x->setId_alum($row['Id_alum']);
        $x->setId_esp($row['Id_esp']);
        $x->setId_ori($row['Id_ori']);
        $x->setBeca($row['beca']);
        $x->setOpcionPago($row['Opcion_pago']);
        $x->setTipo_beca($row['Tipo_beca']);
        $x->setActivo($row['Activo_oferta']);
        $x->setTurno($row['Turno']);
        $x->setAlta_ofe($row['Alta_ofe']);
        $x->setBaja_ofe($row['Baja_ofe']);
        $x->setId_mot_baja($row['Id_mot_baja']);
        $x->setComentario_baja($row['Comentario_baja']);
        $x->setUltimaGeneracion_credencial($row['UltimaGeneracion_credencial']);
        $x->setId_usu_captura_baja($row['Id_usu_captura_baja']);
        $x->setFechaCapturaEgreso($row['FechaCapturaEgreso']);
        $x->setIdCicloEgreso($row['IdCicloEgreso']);
        $x->setIdUsuEgreso($row['IdUsuEgreso']);
        $x->setGenerarAlerta($row['GenerarAlerta']);
        $x->setId_curso_esp($row['Id_curso_esp']);
        return $x;
    }

    public function advanced_query($query) {
        $resp = array();
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                do {
                    array_push($resp, $this->create_object($row_consulta));
                } while ($row_consulta = $consulta->fetch_assoc());
            }
        }
        return $resp;
    }
    
    
   public function getLastOfertaAlumno($Id_alum) {
        $query = "SELECT * FROM ofertas_alumno WHERE Id_alum=" . $Id_alum . " AND Activo_oferta=1 AND Baja_ofe IS NULL AND FechaCapturaEgreso IS NULL ORDER BY Id_ofe_alum DESC LIMIT 1";
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->create_object($Result1->fetch_assoc());
        }
    }
    

    public function getOfertasAlumno($Id_alum) {
        $query = "SELECT * FROM ofertas_alumno WHERE Id_alum=" . $Id_alum . " AND Activo_oferta=1 AND Baja_ofe IS NULL AND FechaCapturaEgreso IS NULL ORDER BY Id_ofe_alum DESC";
        return $this->advanced_query($query);
    }
    
    public function getOfertasAlumnoAltasYBajas($Id_alum) {
        $query = "SELECT * FROM ofertas_alumno WHERE Id_alum=" . $Id_alum . " ORDER BY Id_ofe_alum DESC";
        return $this->advanced_query($query);
    }
    
    public function getOfertasEgresadasAlumno($Id_alum) {
        $query = "SELECT * FROM ofertas_alumno WHERE Id_alum=" . $Id_alum . " AND Activo_oferta=1 AND FechaCapturaEgreso IS NOT NULL ORDER BY Id_ofe_alum DESC";
        return $this->advanced_query($query);
    }

    public function getLastCicloOferta($Id_ofe_alum) {
        $resp = array();
        $query = "SELECT * FROM ciclos_alum_ulm JOIN ciclos_ulm ON ciclos_alum_ulm.Id_ciclo=ciclos_ulm.Id_ciclo
        WHERE Id_ofe_alum=" . $Id_ofe_alum . " ORDER BY ciclos_alum_ulm.Id_ciclo DESC LIMIT 1";
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                $resp = $row_consulta;
            }
        }
        return $resp;
    }

    public function getFirstCicloOferta($Id_ofe_alum) {
        $resp = array();
        $query = "SELECT * FROM ciclos_alum_ulm 
        JOIN ciclos_ulm ON ciclos_alum_ulm.Id_ciclo=ciclos_ulm.Id_ciclo
        WHERE Id_ofe_alum=" . $Id_ofe_alum . " ORDER BY ciclos_alum_ulm.Id_ciclo ASC LIMIT 1";
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                $resp = $row_consulta;
            }
        }
        return $resp;
    }
    
    public function getLastGradoOferta($Id_ofe_alum) {
        $resp = array();
        $query = "SELECT * FROM ciclos_alum_ulm 
        JOIN grados_ulm ON ciclos_alum_ulm.Id_grado=grados_ulm.Id_grado_ofe
        WHERE Id_ofe_alum=" . $Id_ofe_alum . " ORDER BY Grado DESC LIMIT 1";
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                $resp = $row_consulta;
            }
        }
        return $resp;
    }
    
    public function existeQuery($query) {
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->create_object($Result1->fetch_assoc());
        }
    }
    
    public function getFirstCicloOfertaSinCiclo($Id_ofe_alum) {
        $resp = array();
        $query = "SELECT * FROM ciclos_alum_ulm 
        WHERE Id_ofe_alum=" . $Id_ofe_alum . " ORDER BY Id_ciclo_alum ASC LIMIT 1";
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                $resp = $row_consulta;
            }
        }
        return $resp;
    }
    
    public function getLastCicloOfertaSinCiclo($Id_ofe_alum) {
        $resp = array();
        $query = "SELECT * FROM ciclos_alum_ulm 
        WHERE Id_ofe_alum=" . $Id_ofe_alum . " ORDER BY Id_ciclo_alum DESC LIMIT 1";
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                $resp = $row_consulta;
            }
        }
        return $resp;
    }
    

}
