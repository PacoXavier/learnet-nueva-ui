<?php
require_once 'modelos/base.php';
require_once 'modelos/Aulas.php';

class DaoAulas extends base{
	public function add(Aulas $x){
	    $query=sprintf("INSERT INTO aulas (Clave_aula, Nombre_aula, Tipo_aula, Capacidad_aula, Activo_aula, Id_plantel) VALUES (%s, %s,%s, %s,%s, %s)",
            $this->GetSQLValueString($x->getClave_aula(), "text"), 
            $this->GetSQLValueString($x->getNombre_aula(), "text"), 
            $this->GetSQLValueString($x->getTipo_aula(), "int"), 
            $this->GetSQLValueString($x->getCapacidad_aula(), "int"), 
            $this->GetSQLValueString($x->getActivo_aula(), "int"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Aulas $x){
	    $query=sprintf("UPDATE aulas SET  Clave_aula=%s, Nombre_aula=%s, Tipo_aula=%s, Capacidad_aula=%s, Activo_aula=%s, Id_plantel=%s WHERE Id_aula=%s",
            $this->GetSQLValueString($x->getClave_aula(), "text"), 
            $this->GetSQLValueString($x->getNombre_aula(), "text"), 
            $this->GetSQLValueString($x->getTipo_aula(), "int"), 
            $this->GetSQLValueString($x->getCapacidad_aula(), "int"), 
            $this->GetSQLValueString($x->getActivo_aula(), "int"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE aulas SET Activo_aula=0 WHERE Id_aula =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM aulas WHERE Id_aula= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM aulas WHERE Activo_aula=1 AND Id_plantel=".$this->Id_plantel." ORDER BY Nombre_aula ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new Aulas();
            $x->setId($row['Id_aula']);
            $x->setClave_aula($row['Clave_aula']);
            $x->setNombre_aula($row['Nombre_aula']);
            $x->setTipo_aula($row['Tipo_aula']);
            $x->setCapacidad_aula($row['Capacidad_aula']);
            $x->setActivo_aula($row['Activo_aula']);
            $x->setId_plantel($row['Id_plantel']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getTipoAula($Tipo_aula){
	    $query="SELECT * FROM aulas WHERE Tipo_aula= ".$Tipo_aula;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}


}
