<?php
require_once 'modelos/base.php';
require_once 'modelos/CiclosAlumno.php';

class DaoCiclosAlumno extends base{
	public function add(CiclosAlumno $x){
	    $query=sprintf("INSERT INTO ciclos_alum_ulm (Id_ciclo, Id_grado, Id_ofe_alum, Tipo_beca, Porcentaje_beca, IdUsuCapBeca, DiaCapBeca, MontoBeca) VALUES (%s, %s,%s, %s,%s,%s, %s,%s)",
            $this->GetSQLValueString($x->getId_ciclo(), "int"), 
            $this->GetSQLValueString($x->getId_grado(), "text"), 
            $this->GetSQLValueString($x->getId_ofe_alum(), "int"), 
            $this->GetSQLValueString($x->getTipo_beca(), "int"), 
            $this->GetSQLValueString($x->getPorcentaje_beca(), "int"),
            $this->GetSQLValueString($x->getIdUsuCapBeca(), "int"),
            $this->GetSQLValueString($x->getDiaCapBeca(), "date"),
            $this->GetSQLValueString($x->getMontoBeca(), "double"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(CiclosAlumno $x){
	    $query=sprintf("UPDATE ciclos_alum_ulm SET  Id_ciclo=%s, Id_grado=%s, Id_ofe_alum=%s, Tipo_beca=%s, Porcentaje_beca=%s,IdUsuCapBeca=%s, DiaCapBeca=%s, MontoBeca=%s WHERE Id_ciclo_alum=%s",
            $this->GetSQLValueString($x->getId_ciclo(), "int"), 
            $this->GetSQLValueString($x->getId_grado(), "int"), 
            $this->GetSQLValueString($x->getId_ofe_alum(), "int"), 
            $this->GetSQLValueString($x->getTipo_beca(), "int"), 
            $this->GetSQLValueString($x->getPorcentaje_beca(), "int"),
            $this->GetSQLValueString($x->getIdUsuCapBeca(), "int"),
            $this->GetSQLValueString($x->getDiaCapBeca(), "date"),
            $this->GetSQLValueString($x->getMontoBeca(), "double"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM ciclos_alum_ulm  WHERE Id_ciclo_alum =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM ciclos_alum_ulm WHERE Id_ciclo_alum= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM ciclos_alum_ulm";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new CiclosAlumno();
            $x->setId($row['Id_ciclo_alum']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setId_grado($row['Id_grado']);
            $x->setId_ofe_alum($row['Id_ofe_alum']);
            $x->setTipo_beca($row['Tipo_beca']);
            $x->setPorcentaje_beca($row['Porcentaje_beca']);
            $x->setIdUsuCapBeca($row['IdUsuCapBeca']);
            $x->setDiaCapBeca($row['DiaCapBeca']);
            $x->setMontoBeca($row['MontoBeca']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


        public function getPrimerCicloOferta($Id_ofe_alum){
	    $query="SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum= ".$Id_ofe_alum." ORDER BY Id_ciclo_alum ASC LIMIT 1";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function getLastCicloOferta($Id_ofe_alum){
	    $query="SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum= ".$Id_ofe_alum." ORDER BY Id_ciclo_alum DESC LIMIT 1";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function getCiclosOferta($Id_ofe_alum) {
            $query= "SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=" . $Id_ofe_alum . " ORDER BY Id_ciclo_alum ASC";
            return $this->advanced_query($query);
       }
       
       
       	public function getCicloAnteriorOferta($Id_ciclo,$Id_ofe_alum){
	        $query = "SELECT * FROM ciclos_alum_ulm WHERE Id_ciclo!=".$Id_ciclo." AND Id_ofe_alum=".$Id_ofe_alum." ORDER BY Id_ciclo DESC LIMIT 1";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function getCicloAlumnoAnterior($Id_ciclo_alum,$Id_ofe_alum){
	        $query = "SELECT * FROM ciclos_alum_ulm WHERE Id_ciclo_alum!=".$Id_ciclo_alum." AND Id_ofe_alum=".$Id_ofe_alum." ORDER BY Id_ciclo_alum DESC LIMIT 1";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        
        public function getCicloOferta($Id_ofe_alum,$Id_ciclo){
	    $query="SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum= ".$Id_ofe_alum." AND Id_ciclo=".$Id_ciclo;
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
       
        
        public function getCicloAlumnoOferta($Id_alumno,$Id_ofe_aulm,$Id_ciclo){
	    $query="SELECT * FROM ciclos_alum_ulm JOIN ofertas_alumno ON ciclos_alum_ulm.Id_ofe_alum=ofertas_alumno.Id_ofe_alum
                    WHERE ciclos_alum_ulm.Id_ciclo=".$Id_ciclo." AND ofertas_alumno.Id_alum=".$Id_alumno." AND ofertas_alumno.Id_ofe_alum=".$Id_ofe_aulm;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        
        public function deleteCiclosAlumnoByIdOfe($Id_ofe_alum){
            $query = sprintf("DELETE FROM ciclos_alum_ulm  WHERE Id_ofe_alum =".$Id_ofe_alum); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
       
       
}




