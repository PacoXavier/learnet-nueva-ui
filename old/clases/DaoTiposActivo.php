<?php
require_once 'modelos/base.php';
require_once 'modelos/TiposActivo.php';

class DaoTiposActivo extends base{
        public $table="TiposActivo";

	public function add(TiposActivo $x){
              $query=sprintf("INSERT INTO ".$this->table." (DateCreated, Id_usu, Id_plantel, Nombre,Activo) VALUES (%s ,%s, %s, %s, %s)",
              $this->GetSQLValueString($x->getDateCreated(), "date"),
              $this->GetSQLValueString($x->getId_usu(), "int"),
              $this->GetSQLValueString($x->getId_plantel(), "int"),
              $this->GetSQLValueString($x->getNombre(), "text"),
              $this->GetSQLValueString($x->getActivo(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(TiposActivo $x){
	    $query=sprintf("UPDATE ".$this->table." SET DateCreated=%s, Id_usu=%s, Id_plantel=%s, Nombre=%s,Activo=%s  WHERE Id_tipo = %s",
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getId_usu(), "int"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getNombre(), "text"),
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}

	public function delete($Id){
                $query = sprintf("DELETE FROM ".$this->table." WHERE Id_tipo=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
        
	
	public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_tipo= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new TiposActivo();
            $x->setId($row['Id_tipo']);
            $x->setDateCreated($row['DateCreated']);
            $x->setId_usu($row['Id_usu']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setNombre($row['Nombre']);
            $x->setActivo($row['Activo']);
            return $x;
	}
        
        

         public function advanced_query($query){
                $resp=array();
                $consulta=$this->_cnn->query($query);
                if(!$consulta){
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
	        return $resp;
	}
        
        public function getTipos() {
            $query  = "SELECT * FROM ".$this->table." WHERE Activo=1 AND Id_plantel=".$this->Id_plantel." ORDER BY Nombre ASC";
            return $this->advanced_query($query);
       }

}

