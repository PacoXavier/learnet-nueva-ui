<?php
require_once 'modelos/base.php';
require_once 'modelos/MediosContacto.php';

class DaoMediosContacto extends base{

        public $table="Medios_contacto";
    
	public function add(MediosContacto $x){
              $query=sprintf("INSERT INTO ".$this->table." (Nombre_medio,Id_plantel,Activo_medio) VALUES (%s ,%s, %s)",
              $this->GetSQLValueString($x->getNombre_medio(), "text"),
              $this->GetSQLValueString($x->getId_plantel(), "int"),
              $this->GetSQLValueString($x->getActivo_medio(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(MediosContacto $x){
	    $query=sprintf("UPDATE ".$this->table." SET Nombre_medio=%s,Id_plantel=%s,Activo_medio=%s  WHERE Id_medio = %s",
            $this->GetSQLValueString($x->getNombre_medio(), "text"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getActivo_medio(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}
	
	
	
	public function delete($Id){
                $query = sprintf("UPDATE ".$this->table." SET Activo_medio=0 WHERE Id_medio=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
	
	public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_medio= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
	
	
	public function create_object($row){
            $x = new MediosContacto();
            $x->setId($row['Id_medio']);
            $x->setNombre_medio($row['Nombre_medio']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setActivo_medio($row['Activo_medio']);
            return $x;
	}
        

         public function advanced_query($query){
                $resp=array();
                $consulta=$this->_cnn->query($query);
                if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	}
        
        public function getMedios() {
            $query= "SELECT * FROM ".$this->table." WHERE Id_plantel=" . $this->Id_plantel . " AND Activo_medio=1 ORDER BY Nombre_medio ASC";
            return $this->advanced_query($query);
       }

	

}

