<?php
require_once 'modelos/base.php';
require_once 'modelos/MateriasEspecialidad.php';

class DaoMateriasEspecialidad extends base{
    
	public function add(MateriasEspecialidad $x){
	    $query=sprintf("INSERT INTO Materias_especialidades (Id_esp, Id_mat, Activo_mat_esp, Grado_mat, Evaluacion_extraordinaria, Evaluacion_especial,Tiene_orientacion,NombreDiferente) VALUES (%s, %s,%s, %s,%s, %s, %s, %s)",
            $this->GetSQLValueString($x->getId_esp(), "int"), 
            $this->GetSQLValueString($x->getId_mat(), "int"), 
            $this->GetSQLValueString($x->getActivo_mat_esp(), "int"), 
            $this->GetSQLValueString($x->getGrado_mat(), "int"), 
            $this->GetSQLValueString($x->getEvaluacion_extraordinaria(), "double"),
            $this->GetSQLValueString($x->getEvaluacion_especial(), "double"),
            $this->GetSQLValueString($x->getTiene_orientacion(), "int"),
            $this->GetSQLValueString($x->getNombreDiferente(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->_cnn->insert_id; 
            }
	} 
    
	public function update(MateriasEspecialidad $x){
	    $query=sprintf("UPDATE Materias_especialidades SET  Id_esp=%s, Id_mat=%s, Activo_mat_esp=%s, Grado_mat=%s, Evaluacion_extraordinaria=%s, Evaluacion_especial=%s,Tiene_orientacion=%s,NombreDiferente=%s  WHERE Id_mat_esp=%s",
            $this->GetSQLValueString($x->getId_esp(), "int"), 
            $this->GetSQLValueString($x->getId_mat(), "int"), 
            $this->GetSQLValueString($x->getActivo_mat_esp(), "int"), 
            $this->GetSQLValueString($x->getGrado_mat(), "int"), 
            $this->GetSQLValueString($x->getEvaluacion_extraordinaria(), "double"),
            $this->GetSQLValueString($x->getEvaluacion_especial(), "double"),
            $this->GetSQLValueString($x->getTiene_orientacion(), "int"),
            $this->GetSQLValueString($x->getNombreDiferente(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE Materias_especialidades SET Activo_mat_esp=0 WHERE Id_mat_esp =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Materias_especialidades WHERE Id_mat_esp= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Materias_especialidades";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	public function create_object($row){
            $x = new MateriasEspecialidad();
            $x->setId($row['Id_mat_esp']);
            $x->setId_esp($row['Id_esp']);
            $x->setId_mat($row['Id_mat']);
            $x->setActivo_mat_esp($row['Activo_mat_esp']);
            $x->setGrado_mat($row['Grado_mat']);
            $x->setEvaluacion_extraordinaria($row['Evaluacion_extraordinaria']);
            $x->setEvaluacion_especial($row['Evaluacion_especial']);
            $x->setTiene_orientacion($row['Tiene_orientacion']);
            $x->setNombreDiferente($row['NombreDiferente']);
            return $x;
	}
        
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}

        
        public function getMateriasEspecialidad($Id_esp) {
            $query = "SELECT * FROM Materias_especialidades 
            JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat
            JOIN grados_ulm ON Materias_especialidades.Grado_mat=grados_ulm.Id_grado_ofe 
            WHERE Materias_especialidades.Id_esp=" . $Id_esp . " AND Activo_mat_esp=1 AND Activo_mat=1
            ORDER BY Grado ASC ";  
            return $this->advanced_query($query);
        }
        
        public function getMateriasGrado($Id_grado) {
            $query = "SELECT * FROM Materias_especialidades 
            JOIN materias_uml ON  Materias_especialidades.Id_mat=materias_uml.Id_mat
            WHERE Grado_mat=" . $Id_grado." AND Activo_mat_esp=1 AND Activo_mat=1";  
            return $this->advanced_query($query);
        }
        
        public function getMateriasEspecialidadByIdMat($Id_mat){
            $query= "SELECT * FROM Materias_especialidades WHERE Id_mat=" . $Id_mat . " AND Activo_mat_esp=1";
            return $this->advanced_query($query);
        }
        
        public function existeQuery($query) {
            $Result1 = $this->_cnn->query($query);
            if (!$Result1) {
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            } else {
                return $this->create_object($Result1->fetch_assoc());
            }
        }

}
