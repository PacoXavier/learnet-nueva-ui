<?php
require_once 'modelos/base.php';
require_once 'modelos/Recargos.php';

class DaoRecargos extends base{

	public function add(Recargos $x){
              $query=sprintf("INSERT INTO Recargos_pago (Id_pago,Monto_recargo,Fecha_recargo) VALUES (%s ,%s, %s)",
              $this->GetSQLValueString($x->getId_pago(), "int"),
              $this->GetSQLValueString($x->getMonto_recargo(), "double"),
              $this->GetSQLValueString($x->getFecha_recargo(), "date"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(Recargos $x){
	    $query=sprintf("UPDATE Recargos_pago SET Id_pago=%s, Monto_recargo=%s, Fecha_recargo=%s WHERE Id_recargo = %s",
              $this->GetSQLValueString($x->getId_pago(), "int"),
              $this->GetSQLValueString($x->getMonto_recargo(), "double"),
              $this->GetSQLValueString($x->getFecha_recargo(), "date"),
              $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}
	
	
	
	public function delete($Id){
                $query = sprintf("DELETE FROM Recargos_pago WHERE Id_recargo=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
        
        public function deleteRecargosPago($Id){
                $query = sprintf("DELETE FROM Recargos_pago WHERE Id_pago=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
        
        
       
	
	public function show($Id){
	    $query="SELECT * FROM Recargos_pago WHERE Id_recargo= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Recargos_pago";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
	
	
	
	public function create_object($row){
            $x = new Recargos();
            $x->setId($row['Id_recargo']);
            $x->setId_pago($row['Id_pago']);
            $x->setMonto_recargo($row['Monto_recargo']);
            $x->setFecha_recargo($row['Fecha_recargo']); 
            return $x;
	}

	public function advanced_query($query){
		$resp=array();
		$consulta=$this->_cnn->query($query);
		    if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	}
        
        
         public function getRecargosPago($Id_pago_ciclo){
              $resp=array();
              $query="SELECT * FROM Recargos_pago WHERE Id_pago=" .$Id_pago_ciclo."  AND Fecha_recargo<='".date('Y-m-d')."' ORDER BY Fecha_recargo ASC";
              foreach($this->advanced_query($query) as $k=>$v){
                  array_push($resp, $this->show($v->getId()));
              }
              return $resp;
         }
         
         
        public function existRecargoMes($Id_pago,$mes,$anio){
	    $query = "SELECT * FROM Recargos_pago WHERE Id_pago=" . $Id_pago . "  AND MONTH(Fecha_recargo)='" . $mes . "' and YEAR(Fecha_recargo)='" . $anio . "'";
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
}

