<?php
require_once 'modelos/base.php';
require_once 'modelos/Parametros.php';

class DaoParametros extends base{
    
	public function update(Parametros $x){
	    $query=sprintf("UPDATE parametros SET  valor_param=%s WHERE Id_param=%s",
            $this->GetSQLValueString($x->getValor_param(), "text"), 
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
        public function show($text_param,$Id_plantel){
	    $query="SELECT * FROM parametros WHERE text_param= '".$text_param."' AND Id_plantel=".$Id_plantel;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        

        public function mostrarParametro($Id_param){
	    $query="SELECT * FROM parametros WHERE Id_param=".$Id_param;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
	public function create_object($row){
            $x = new Parametros();
            $x->setId($row['Id_param']);
            $x->setText_param($row['text_param']);
            $x->setValor_param($row['valor_param']);
            $x->setId_plantel($row['Id_plantel']);
            return $x;
	}
        
	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
     
}
