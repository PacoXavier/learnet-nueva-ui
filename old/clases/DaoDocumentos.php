<?php
require_once 'modelos/base.php';
require_once 'modelos/Documentos.php';

class DaoDocumentos extends base{
	public function add(Documentos $x){
	  $query=sprintf("INSERT INTO archivos_ulm (Nombre_file, Id_plantel) VALUES (%s, %s)",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Documentos $x){
	    $query=sprintf("UPDATE archivos_ulm SET  Nombre_file=%s, Id_plantel=%s WHERE Id_file=%s",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	/*
	public function delete($Id){
            $query = sprintf("UPDATE archivos_ulm SET Activo_grupo=0 WHERE Id_file =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
         * 
         */
	
	public function show($Id){
	    $query="SELECT * FROM archivos_ulm WHERE Id_file= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	

	
	public function create_object($row){
            $x = new Documentos();
            $x->setId($row['Id_file']);
            $x->setNombre($row['Nombre_file']);
            $x->setId_plantel($row['Id_plantel']);

            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        


}
