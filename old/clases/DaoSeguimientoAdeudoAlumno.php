<?php
require_once 'modelos/base.php';
require_once 'modelos/SeguimientoAdeudoAlumno.php';

class DaoSeguimientoAdeudoAlumno extends base{
    
	public function add(SeguimientoAdeudoAlumno $x){
	    $query=sprintf("INSERT INTO SeguimientoAdeudoAlumno (Id_usu, DateCreated, Comentario, TipoRel,IdRel) VALUES (%s, %s,%s, %s, %s)",
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getComentario(), "text"), 
            $this->GetSQLValueString($x->getTipoRel(), "text"),
            $this->GetSQLValueString($x->getIdRel(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(SeguimientoAdeudoAlumno $x){
	    $query=sprintf("UPDATE SeguimientoAdeudoAlumno SET  Id_usu=%s, DateCreated=%s, Comentario=%s, TipoRel=%s,IdRel=%s WHERE Id=%s",
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getComentario(), "text"), 
            $this->GetSQLValueString($x->getTipoRel(), "text"),
            $this->GetSQLValueString($x->getIdRel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

        
        
	public function delete($Id){
            $query = sprintf("DELETE FROM SeguimientoAdeudoAlumno WHERE Id =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM SeguimientoAdeudoAlumno WHERE Id= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	
	public function create_object($row){
            $x = new SeguimientoAdeudoAlumno();
            $x->setId($row['Id']);
            $x->setId_usu($row['Id_usu']);
            $x->setDateCreated($row['DateCreated']);
            $x->setComentario($row['Comentario']);
            $x->setTipoRel($row['TipoRel']);
            $x->setIdRel($row['IdRel']);
            return $x;
	}
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


}
