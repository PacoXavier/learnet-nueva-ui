<?php
require_once 'modelos/base.php';
require_once 'modelos/Dias.php';

class DaoDias extends base{
    
    public function add(Dias $x){
           $query=sprintf("INSERT INTO Dias ( Nombre, Abreviacion) VALUES (%s, %s)",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getAbreviacion(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
public function update(Dias $x){
           $query=sprintf("UPDATE Dias SET  Nombre=%s, Abreviacion=%s WHERE Id=%s",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getAbreviacion(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
public function delete($Id){
            $query = sprintf("DELETE FROM  Dias WHERE Id=".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
}
	
public function show($Id,$abre){
      $q="";
       if($Id!=null){
           $q="Id= ".$Id;
       }
       if($abre!=null){
           $q="Abreviacion= ".$abre;
       }
        $query="SELECT * FROM Dias WHERE ".$q;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
}



	
public function create_object($row){
            $x = new Dias();
            $x->setId($row['Id']);
            $x->setNombre($row['Nombre']);
            $x->setAbreviacion($row['Abreviacion']);
            return $x;
}

        
        

    public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
    }

public function getDias(){
    $query="SELECT *FROM Dias";
    return $this->advanced_query($query);
}

}