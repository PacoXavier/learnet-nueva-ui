<?php
require_once 'modelos/base.php';
require_once 'modelos/OfertasPlantel.php';

class DaoOfertasPlantel extends base{

	public function add(OfertasPlantel $x){
              $query=sprintf("INSERT INTO OfertasPlantel (Id_plantel,Id_ofe) VALUES (%s ,%s)",
              $this->GetSQLValueString($x->getId_plantel(), "int"),
              $this->GetSQLValueString($x->getId_ofe(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}
	
	public function delete($Id){
                $query = sprintf("DELETE FROM OfertasPlantel WHERE Id=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
	
	public function show($Id){
	    $query="SELECT * FROM OfertasPlantel WHERE Id= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function getOfertasPlantel($Id_plantel){
            $resp=array();
            $query="SELECT * FROM OfertasPlantel WHERE Id_plantel=".$Id_plantel." ORDER BY Id_ofe ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
	

	
	public function create_object($row){
            $x = new OfertasPlantel();
            $x->setId($row['Id']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setId_ofe($row['Id_ofe']);
            return $x;
	}
        
         public function advanced_query($query){
		    $resp=array();
		    $consulta=$this->_cnn->query($query);
		    if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	} 
}

