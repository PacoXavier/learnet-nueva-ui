<?php
require_once 'modelos/base.php';
require_once 'modelos/MateriasAcreditadas.php';

class DaoMateriasAcreditadas extends base{
    
	public function add(MateriasAcreditadas $x){
	    $query=sprintf("INSERT INTO Materias_acreditadas (Id_mat_esp, Id_ofe_alum, Fecha_ac, Id_ciclo, Id_usu,Id_pago,Comentarios) VALUES (%s, %s,%s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getId_mat_esp(), "int"), 
            $this->GetSQLValueString($x->getId_ofe_alum(), "int"), 
            $this->GetSQLValueString($x->getFecha_ac(), "date"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getId_usu(), "int"),
            $this->GetSQLValueString($x->getId_pago(), "int"),
            $this->GetSQLValueString($x->getComentarios(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(MateriasAcreditadas $x){
	    $query=sprintf("UPDATE Materias_acreditadas SET  Id_mat_esp=%s, Id_ofe_alum=%s, Fecha_ac=%s, Id_ciclo=%s, Id_usu=%s, Id_pago=%s, Comentarios=%s WHERE Id_mat_ac=%s",
            $this->GetSQLValueString($x->getId_mat_esp(), "int"), 
            $this->GetSQLValueString($x->getId_ofe_alum(), "int"), 
            $this->GetSQLValueString($x->getFecha_ac(), "date"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"),
            $this->GetSQLValueString($x->getId_usu(), "int"),
            $this->GetSQLValueString($x->getId_pago(), "int"),
            $this->GetSQLValueString($x->getComentarios(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM Materias_acreditadas WHERE Id_mat_ac =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Materias_acreditadas WHERE Id_mat_ac= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Materias_acreditadas";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new MateriasAcreditadas();
            $x->setId($row['Id_mat_ac']);
            $x->setId_mat_esp($row['Id_mat_esp']);
            $x->setId_ofe_alum($row['Id_ofe_alum']);
            $x->setFecha_ac($row['Fecha_ac']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setId_usu($row['Id_usu']);
            $x->setId_pago($row['Id_pago']);
            $x->setComentarios($row['Comentarios']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        function getMateriasAcreditadasByOfeAlumno($Id_ofe_alum){
            $query = "SELECT * FROM Materias_acreditadas WHERE Id_ofe_alum=" .$Id_ofe_alum;
            return $this->advanced_query($query);
        }


}
