<?php
require_once 'modelos/base.php';
require_once 'modelos/TareasActividad.php';

class DaoTareasActividad extends base{
	public function add(TareasActividad $x){
	    $query=sprintf("INSERT INTO TareasActividad (Comentario, Id_usuCreated, Id_act, Id_usuRespon, Status, DateCreated, StartDate, EndDate, Prioridad,Orden) VALUES (%s,%s, %s,%s,%s, %s,%s, %s,%s,%s)",
            $this->GetSQLValueString($x->getComentario(), "text"), 
            $this->GetSQLValueString($x->getId_usuCreated(), "int"), 
            $this->GetSQLValueString($x->getId_act(), "int"), 
            $this->GetSQLValueString($x->getId_usuRespon(), "int"), 
            $this->GetSQLValueString($x->getStatus(), "text"),
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getStartDate(), "date"),
            $this->GetSQLValueString($x->getEndDate(), "date"),
            $this->GetSQLValueString($x->getPrioridad(), "text"),
            $this->GetSQLValueString($x->getOrder(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(TareasActividad $x){
	    $query=sprintf("UPDATE TareasActividad SET  Comentario=%s, Id_usuCreated=%s, Id_act=%s, Id_usuRespon=%s, Status=%s, DateCreated=%s, StartDate=%s, EndDate=%s, Prioridad=%s,Orden=%s,Id_usu_realizo=%s, Comentario_realizo=%s WHERE Id=%s",
            $this->GetSQLValueString($x->getComentario(), "text"), 
            $this->GetSQLValueString($x->getId_usuCreated(), "int"), 
            $this->GetSQLValueString($x->getId_act(), "int"), 
            $this->GetSQLValueString($x->getId_usuRespon(), "int"), 
            $this->GetSQLValueString($x->getStatus(), "text"),
            $this->GetSQLValueString($x->getDateCreated(), "date"),
            $this->GetSQLValueString($x->getStartDate(), "date"),
            $this->GetSQLValueString($x->getEndDate(), "date"),
            $this->GetSQLValueString($x->getPrioridad(), "text"),
            $this->GetSQLValueString($x->getOrder(), "int"),
            $this->GetSQLValueString($x->getId_usu_realizo(), "int"),
            $this->GetSQLValueString($x->getComentario_realizo(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM TareasActividad WHERE Id =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM TareasActividad WHERE Id= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM TareasActividad";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new TareasActividad();
            $x->setId($row['Id']);
            $x->setComentario($row['Comentario']);
            $x->setId_usuCreated($row['Id_usuCreated']);
            $x->setId_act($row['Id_act']);
            $x->setId_usuRespon($row['Id_usuRespon']);
            $x->setStatus($row['Status']);
            $x->setDateCreated($row['DateCreated']);
            $x->setStartDate($row['StartDate']);
            $x->setEndDate($row['EndDate']);
            $x->setPrioridad($row['Prioridad']);
            $x->setComentario_realizo($row['Comentario_realizo']);
            $x->setId_usu_realizo($row['Id_usu_realizo']);
            $x->setOrder($row['Orden']);
            return $x;
	}
        
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


        public function getTareasActividad($Id_act) {
            $query= "SELECT * FROM TareasActividad WHERE Id_act=" . $Id_act . " ORDER BY Orden ASC";
            return $this->advanced_query($query);
       }
      
       	public function nextOrder(){
	    $query="SELECT * FROM TareasActividad ORDER BY Orden DESC LIMIT 1";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                $orden=1;
                if($totalRows_consulta>0){ 
                    $orden=$row_consulta['Orden']+1;
                }
            }
            return $orden;
	}
        
        public function getTareasActividadUserFaltantes($Id_usu) {
            $query= "SELECT * FROM TareasActividad WHERE Id_usuRespon=" . $Id_usu . " AND Status=0 ORDER BY Orden ASC";
            return $this->advanced_query($query);
        }
        
      
}
