<?php
require_once 'modelos/base.php';
require_once 'modelos/Becas.php';

class DaoBecas extends base{
    
	public function add(Becas $x){
	    $query=sprintf("INSERT INTO Becas (Nombre, Monto, Descripcion, DateCreated, Id_usu, Activo,Id_plantel, CalificacionMin,Tipo,Contrato) VALUES (%s, %s,%s, %s,%s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getMonto(), "double"), 
            $this->GetSQLValueString($x->getDescripcion(), "text"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getCalificacionMin(), "double"),
            $this->GetSQLValueString($x->getTipo(), "text"),
            $this->GetSQLValueString($x->getContrato(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Becas $x){
	    $query=sprintf("UPDATE Becas SET  Nombre=%s, Monto=%s, Descripcion=%s, DateCreated=%s, Id_usu=%s, Activo=%s,Id_plantel=%s, CalificacionMin=%s,Tipo=%s,Contrato=%s  WHERE Id_beca=%s",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getMonto(), "double"), 
            $this->GetSQLValueString($x->getDescripcion(), "text"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getCalificacionMin(), "double"),
            $this->GetSQLValueString($x->getTipo(), "text"),
            $this->GetSQLValueString($x->getContrato(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE Becas SET Activo=0 WHERE Id_beca =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Becas WHERE Id_beca= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Becas WHERE Activo=1 AND Id_plantel=".$this->Id_plantel." ORDER BY DateCreated DESC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new Becas();
            $x->setId($row['Id_beca']);
            $x->setNombre($row['Nombre']);
            $x->setMonto($row['Monto']);
            $x->setDescripcion($row['Descripcion']);
            $x->setDateCreated($row['DateCreated']);
            $x->setId_usu($row['Id_usu']);
            $x->setActivo($row['Activo']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setCalificacionMin($row['CalificacionMin']);
            $x->setTipo($row['Tipo']);
            $x->setContrato($row['Contrato']);
            return $x;
	}
        
	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


}
