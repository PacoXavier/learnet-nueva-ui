<?php

require_once 'modelos/base.php';
require_once 'modelos/Docentes.php';
require_once 'DaoUsuarios.php';
require_once 'DaoAsistencias.php';
require_once 'DaoHorarioDocente.php';
require_once 'DaoCiclos.php';
require_once 'DaoGrupos.php';

class DaoDocentes extends base {

    public function add(Docentes $x) {
        $query = sprintf("INSERT INTO Docentes (Clave_docen, Nombre_docen, ApellidoP_docen, ApellidoM_docen, Telefono_docen, Email_docen,
            NivelEst_docen,Last_session,Pass_docen,Baja_docen, Alta_docent, Img_docen, Doc_RFC, Doc_CURP, Doc_Curriculum, Doc_CompDomicilio, Doc_Contrato, 
            Doc_CompEstudios, Cel_docen, copy_RFC, copy_Curriculum, copy_CompDomicilio, copy_Contrato, copy_CompEstudios, copy_CURP, Tipo_sangre, 
            Alergias, Enfermedades_cronicas, Preinscripciones_medicas, Id_plantel, Id_dir, Recover_docen, UltimaGeneracion_credencial, Tiempo_completo,FechaNacimiento, Sexo, Curp)
            VALUES (%s, %s,%s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s
            , %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", 
                $this->GetSQLValueString($x->getClave_docen(), "text"), 
                $this->GetSQLValueString($x->getNombre_docen(), "text"), 
                $this->GetSQLValueString($x->getApellidoP_docen(), "text"), 
                $this->GetSQLValueString($x->getApellidoM_docen(), "text"), 
                $this->GetSQLValueString($x->getTelefono_docen(), "text"), 
                $this->GetSQLValueString($x->getEmail_docen(), "text"), 
                $this->GetSQLValueString($x->getNivelEst_docen(), "int"), 
                $this->GetSQLValueString($x->getLast_session(), "date"), 
                $this->GetSQLValueString($x->getPass_docen(), "text"), 
                $this->GetSQLValueString($x->getBaja_docen(), "date"), 
                $this->GetSQLValueString($x->getAlta_docent(), "date"), 
                $this->GetSQLValueString($x->getImg_docen(), "text"), 
                $this->GetSQLValueString($x->getDoc_RFC(), "int"), 
                $this->GetSQLValueString($x->getDoc_CURP(), "int"), 
                $this->GetSQLValueString($x->getDoc_Curriculum(), "int"), 
                $this->GetSQLValueString($x->getDoc_CompDomicilio(), "int"), 
                $this->GetSQLValueString($x->getDoc_Contrato(), "int"), 
                $this->GetSQLValueString($x->getDoc_CompEstudios(), "int"), 
                $this->GetSQLValueString($x->getCel_docen(), "text"), 
                $this->GetSQLValueString($x->getCopy_RFC(), "int"), 
                $this->GetSQLValueString($x->getCopy_Curriculum(), "int"), 
                $this->GetSQLValueString($x->getCopy_CompDomicilio(), "int"), 
                $this->GetSQLValueString($x->getCopy_Contrato(), "int"), 
                $this->GetSQLValueString($x->getCopy_CompEstudios(), "int"), 
                $this->GetSQLValueString($x->getCopy_CURP(), "int"), 
                $this->GetSQLValueString($x->getTipo_sangre(), "text"), 
                $this->GetSQLValueString($x->getAlergias(), "text"), 
                $this->GetSQLValueString($x->getEnfermedades_cronicas(), "text"), 
                $this->GetSQLValueString($x->getPreinscripciones_medicas(), "text"), 
                $this->GetSQLValueString($x->getId_plantel(), "int"), 
                $this->GetSQLValueString($x->getId_dir(), "int"), 
                $this->GetSQLValueString($x->getRecover_docen(), "text"), 
                $this->GetSQLValueString($x->getUltimaGeneracion_credencial(), "date"), 
                $this->GetSQLValueString($x->getTiempo_completo(), "int"),
                $this->GetSQLValueString($x->getFechaNacimiento(), "text"),
                $this->GetSQLValueString($x->getSexo(), "text"),
                $this->GetSQLValueString($x->getCurp(), "text"));
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->_cnn->insert_id;
        }
    }

    public function update(Docentes $x) {
        $query = sprintf("UPDATE Docentes SET  Clave_docen=%s, Nombre_docen=%s, ApellidoP_docen=%s, ApellidoM_docen=%s, Telefono_docen=%s, Email_docen=%s,
            NivelEst_docen=%s,Last_session=%s,Pass_docen=%s,Baja_docen=%s, Alta_docent=%s, Img_docen=%s, Doc_RFC=%s, Doc_CURP=%s, Doc_Curriculum=%s, Doc_CompDomicilio=%s, Doc_Contrato=%s, 
            Doc_CompEstudios=%s, Cel_docen=%s, copy_RFC=%s, copy_Curriculum=%s, copy_CompDomicilio=%s, copy_Contrato=%s, copy_CompEstudios=%s, copy_CURP=%s, Tipo_sangre=%s, 
            Alergias=%s, Enfermedades_cronicas=%s, Preinscripciones_medicas=%s, Id_plantel=%s, Id_dir=%s, Recover_docen=%s, UltimaGeneracion_credencial=%s, Tiempo_completo=%s,FechaNacimiento=%s, Sexo=%s, Curp=%s WHERE Id_docen=%s", 
                $this->GetSQLValueString($x->getClave_docen(), "text"), 
                $this->GetSQLValueString($x->getNombre_docen(), "text"), 
                $this->GetSQLValueString($x->getApellidoP_docen(), "text"), 
                $this->GetSQLValueString($x->getApellidoM_docen(), "text"), 
                $this->GetSQLValueString($x->getTelefono_docen(), "text"), 
                $this->GetSQLValueString($x->getEmail_docen(), "text"), 
                $this->GetSQLValueString($x->getNivelEst_docen(), "int"), 
                $this->GetSQLValueString($x->getLast_session(), "date"), 
                $this->GetSQLValueString($x->getPass_docen(), "text"), 
                $this->GetSQLValueString($x->getBaja_docen(), "date"), 
                $this->GetSQLValueString($x->getAlta_docent(), "date"), 
                $this->GetSQLValueString($x->getImg_docen(), "text"), 
                $this->GetSQLValueString($x->getDoc_RFC(), "int"), 
                $this->GetSQLValueString($x->getDoc_CURP(), "int"), 
                $this->GetSQLValueString($x->getDoc_Curriculum(), "int"), 
                $this->GetSQLValueString($x->getDoc_CompDomicilio(), "int"), 
                $this->GetSQLValueString($x->getDoc_Contrato(), "int"), 
                $this->GetSQLValueString($x->getDoc_CompEstudios(), "int"), 
                $this->GetSQLValueString($x->getCel_docen(), "text"), 
                $this->GetSQLValueString($x->getCopy_RFC(), "int"), 
                $this->GetSQLValueString($x->getCopy_Curriculum(), "int"), 
                $this->GetSQLValueString($x->getCopy_CompDomicilio(), "int"), 
                $this->GetSQLValueString($x->getCopy_Contrato(), "int"), 
                $this->GetSQLValueString($x->getCopy_CompEstudios(), "int"), 
                $this->GetSQLValueString($x->getCopy_CURP(), "int"), 
                $this->GetSQLValueString($x->getTipo_sangre(), "text"), 
                $this->GetSQLValueString($x->getAlergias(), "text"), 
                $this->GetSQLValueString($x->getEnfermedades_cronicas(), "text"), 
                $this->GetSQLValueString($x->getPreinscripciones_medicas(), "text"), 
                $this->GetSQLValueString($x->getId_plantel(), "int"), 
                $this->GetSQLValueString($x->getId_dir(), "int"), 
                $this->GetSQLValueString($x->getRecover_docen(), "text"), 
                $this->GetSQLValueString($x->getUltimaGeneracion_credencial(), "date"), 
                $this->GetSQLValueString($x->getTiempo_completo(), "int"), 
                $this->GetSQLValueString($x->getFechaNacimiento(), "text"),
                $this->GetSQLValueString($x->getSexo(), "text"),
                $this->GetSQLValueString($x->getCurp(), "text"),
                $this->GetSQLValueString($x->getId(), "int"));
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $x->getId();
        }
    }
    

    public function delete($Id) {
        $query = sprintf("UPDATE Docentes SET Baja_docen=" . date('Y-m-d H:i:s') . " WHERE Id_docen =" . $Id);
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return true;
        }
    }

    public function show($Id) {
        $query = "SELECT * FROM Docentes WHERE Id_docen= " . $Id;
        $Result1 = $this->_cnn->query($query);
        if (!$Result1) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            return $this->create_object($Result1->fetch_assoc());
        }
    }

    public function showAll() {
        $resp = array();
        $query = "SELECT * FROM Docentes WHERE Baja_docen IS NULL  AND Id_plantel=" . $this->Id_plantel . " ORDER BY Id_docen DESC";
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
        }
        $totalRows_consulta = $consulta->num_rows;
        if ($totalRows_consulta > 0) {
            do {
                array_push($resp, $this->create_object($row_consulta));
            } while ($row_consulta = $consulta->fetch_assoc());
        }
        return $resp;
    }

    public function create_object($row) {
        $x = new Docentes();
        $x->setId($row['Id_docen']);
        $x->setClave_docen($row['Clave_docen']);
        $x->setNombre_docen($row['Nombre_docen']);
        $x->setApellidoP_docen($row['ApellidoP_docen']);
        $x->setApellidoM_docen($row['ApellidoM_docen']);
        $x->setTelefono_docen($row['Telefono_docen']);
        $x->setEmail_docen($row['Email_docen']);
        $x->setNivelEst_docen($row['NivelEst_docen']);
        $x->setLast_session($row['Last_session']);
        $x->setPass_docen($row['Pass_docen']);
        $x->setBaja_docen($row['Baja_docen']);
        $x->setAlta_docent($row['Alta_docent']);
        $x->setImg_docen($row['Img_docen']);
        $x->setDoc_RFC($row['Doc_RFC']);
        $x->setDoc_CURP($row['Doc_CURP']);
        $x->setDoc_Curriculum($row['Doc_Curriculum']);
        $x->setDoc_CompDomicilio($row['Doc_CompDomicilio']);
        $x->setDoc_Contrato($row['Doc_Contrato']);
        $x->setDoc_CompEstudios($row['Doc_CompEstudios']);
        $x->setCel_docen($row['Cel_docen']);
        $x->setCopy_RFC($row['copy_RFC']);
        $x->setCopy_Curriculum($row['copy_Curriculum']);
        $x->setCopy_CompDomicilio($row['copy_CompDomicilio']);
        $x->setCopy_Contrato($row['copy_Contrato']);
        $x->setCopy_CompEstudios($row['copy_CompEstudios']);
        $x->setCopy_CURP($row['copy_CURP']);
        $x->setTipo_sangre($row['Tipo_sangre']);
        $x->setAlergias($row['Alergias']);
        $x->setEnfermedades_cronicas($row['Enfermedades_cronicas']);
        $x->setPreinscripciones_medicas($row['Preinscripciones_medicas']);
        $x->setId_dir($row['Id_dir']);
        $x->setRecover_docen($row['Recover_docen']);
        $x->setUltimaGeneracion_credencial($row['UltimaGeneracion_credencial']);
        $x->setTiempo_completo($row['Tiempo_completo']);
        $x->setId_plantel($row['Id_plantel']);
        $x->setFechaNacimiento($row['FechaNacimiento']);
        $x->setSexo($row['Sexo']);
        $x->setCurp($row['Curp']);
        return $x;
    }

    public function advanced_query($query) {
        $resp = array();
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                do {
                    array_push($resp, $this->create_object($row_consulta));
                } while ($row_consulta = $consulta->fetch_assoc());
            }
        }
        return $resp;
    }

    public function generarClave() {
        $query = "SELECT * FROM Docentes WHERE Clave_docen IS NOT NULL AND Id_plantel=" . $this->Id_plantel . "  ORDER BY Id_docen DESC LIMIT 1";
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                $Codigo = $row_consulta['Clave_docen'];
                $Codigo++;
            }else{
                $Codigo=$this->Id_plantel."0000";   
            }
        }
        return $Codigo;
    }

    public function buscarDocente($buscar, $limit = 20) {
        $resp = array();
        $query = "SELECT *
            FROM Docentes 
                 WHERE  (Nombre_docen LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR ApellidoP_docen LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR ApellidoM_docen LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR Clave_docen LIKE '%".str_replace(' ','',$buscar)."%'
                    OR Email_docen LIKE '%".str_replace(' ','',$buscar)."%')
                 AND Baja_docen IS NULL AND Id_plantel=" . $this->Id_plantel . " LIMIT " . $limit;
        $consulta = $this->_cnn->query($query);
        if (!$consulta) {
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
        } else {
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta = $consulta->num_rows;
            if ($totalRows_consulta > 0) {
                do {
                    array_push($resp, $this->create_object($row_consulta));
                } while ($row_consulta = $consulta->fetch_assoc());
            }
        }
        return $resp;
    }

    public function getHorasGrupo($Id_docen, $Id_ciclo, $Id_grupo, $query_fechas) {
        $DaoAsistencias = new DaoAsistencias();
        $DaoHorarioDocente = new DaoHorarioDocente();
        $DaoGrupos = new DaoGrupos();

        $base = new base();
        $resp = array();
        $resp['Grupos'] = array();

        $TotalGrupos = 0;
        $query = "
                        SELECT 
                            Docentes.Id_docen,Docentes.Nombre_docen,
                            Horario_docente.Id_grupo,Horario_docente.Id_ciclo,
                            Grupos.Clave,Grupos.Id_mat_esp,
                            Materias_especialidades.Id_esp,
                            especialidades_ulm.Id_ofe,
                            ofertas_ulm.Nombre_oferta
                        FROM Docentes 
                        JOIN Horario_docente ON Docentes.Id_docen=Horario_docente.Id_docente
                        JOIN Grupos ON Horario_docente.Id_grupo=Grupos.Id_grupo
                        JOIN Materias_especialidades ON  Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
                        JOIN especialidades_ulm ON Materias_especialidades.Id_esp=especialidades_ulm.Id_esp
                        JOIN ofertas_ulm ON especialidades_ulm.Id_ofe=ofertas_ulm.Id_oferta
                        WHERE Id_docen=" . $Id_docen . " AND Horario_docente.Id_ciclo=" . $Id_ciclo . " AND Grupos.Id_grupo=" . $Id_grupo . " GROUP BY Grupos.Id_grupo";
        $grupos = $base->advanced_query($query);
        $row_grupos = $grupos->fetch_assoc();
        $totalRows_grupos = $grupos->num_rows;
        if ($totalRows_grupos > 0) {
            $TotalGrupos = $totalRows_grupos;
            do {

                $horasPorDia = $DaoGrupos->getHorasDiaGrupo($row_grupos['Id_grupo']);

                $LunesTrabajados = 0;
                $MartesTrabajados = 0;
                $MiercolesTrabajados = 0;
                $JuevesTrabajados = 0;
                $ViernesTrabajados = 0;
                $SabadoTrabajados = 0;
                $DomingosTrabajados=0;
                $query = "SELECT * From Asistencias WHERE Id_grupo=" . $row_grupos['Id_grupo'] . " " . $query_fechas . " GROUP BY Fecha_asis ORDER BY Fecha_asis ASC";
                foreach ($DaoAsistencias->advanced_query($query) as $asis) {
                    if (date("N", strtotime($asis->getFecha_asis())) == 1) {
                        $LunesTrabajados+=$horasPorDia['L'];
                    } elseif (date("N", strtotime($asis->getFecha_asis())) == 2) {
                        $MartesTrabajados+=$horasPorDia['M'];
                    } elseif (date("N", strtotime($asis->getFecha_asis())) == 3) {
                        $MiercolesTrabajados+=$horasPorDia['I'];
                    } elseif (date("N", strtotime($asis->getFecha_asis())) == 4) {
                        $JuevesTrabajados+=$horasPorDia['J'];
                    } elseif (date("N", strtotime($asis->getFecha_asis())) == 5) {
                        $ViernesTrabajados+=$horasPorDia['V'];
                    } elseif (date("N", strtotime($asis->getFecha_asis())) == 6) {
                        $SabadoTrabajados+=$horasPorDia['S'];
                    } elseif (date("N", strtotime($asis->getFecha_asis())) == 7) {
                        $DomingosTrabajados+=$horasPorDia['D'];
                    }
                }

                $Grupo = array();
                $Grupo['Id_ofe'] = $row_grupos['Id_ofe'];
                $Grupo['Id_grupo'] = $row_grupos['Id_grupo'];
                $Grupo['Lunes'] = $horasPorDia['L'];
                $Grupo['Martes'] = $horasPorDia['M'];
                $Grupo['Miercoles'] = $horasPorDia['I'];
                $Grupo['Jueves'] = $horasPorDia['J'];
                $Grupo['Viernes'] = $horasPorDia['V'];
                $Grupo['Sabado'] = $horasPorDia['S'];
                $Grupo['Domingo'] = $horasPorDia['D'];
                $Grupo['HorasPor_semana'] = $horasPorDia['L'] + $horasPorDia['M'] + $horasPorDia['I'] + $horasPorDia['J'] + $horasPorDia['V'] + $horasPorDia['S']+ $horasPorDia['D'];
                $Grupo['HorasPor_semana_trabajados'] = $LunesTrabajados + $MartesTrabajados + $MiercolesTrabajados + $JuevesTrabajados + $ViernesTrabajados + $SabadoTrabajados+$DomingosTrabajados;
                array_push($resp['Grupos'], $Grupo);
            } while ($row_grupos = $grupos->fetch_assoc());
        }

        $resp['NumGrupos'] = $TotalGrupos;
        return $resp;
    }

    //Se utiliza en el reporte de horas del docente
    public function getNumGruposDocente($Id_docen, $Id_ciclo, $Id_ofe, $FechaIniParam = null, $FechaFinParam = null) {
        $DaoAsistencias = new DaoAsistencias();
        $DaoHorarioDocente = new DaoHorarioDocente();
        $DaoCiclos = new DaoCiclos();
        $DaoGrupos = new DaoGrupos();
        $base = new base();

        $resp = array();
        $resp['Grupos'] = array();
        $TotalGrupos = 0;

        $query = "
                        SELECT 
                            Docentes.Id_docen,Docentes.Nombre_docen,
                            Horario_docente.Id_grupo,Horario_docente.Id_ciclo,
                            Grupos.Clave,Grupos.Id_mat_esp,Grupos.Id_grupo,
                            Materias_especialidades.Id_esp,
                            especialidades_ulm.Id_ofe,
                            ofertas_ulm.Nombre_oferta
                        FROM Docentes 
                        JOIN Horario_docente ON Docentes.Id_docen=Horario_docente.Id_docente
                        JOIN Grupos ON Horario_docente.Id_grupo=Grupos.Id_grupo
                        JOIN Materias_especialidades ON  Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
                        JOIN especialidades_ulm ON Materias_especialidades.Id_esp=especialidades_ulm.Id_esp
                        JOIN ofertas_ulm ON especialidades_ulm.Id_ofe=ofertas_ulm.Id_oferta
                        WHERE Id_docen=" . $Id_docen . " AND Horario_docente.Id_ciclo=" . $Id_ciclo . " AND Id_ofe= " . $Id_ofe . " GROUP BY Grupos.Id_grupo";
        $grupos = $base->advanced_query($query);
        $row_grupos = $grupos->fetch_assoc();
        $totalRows_grupos = $grupos->num_rows;
        if ($totalRows_grupos > 0) {
            $TotalGrupos = $totalRows_grupos;
            do {
                $horasPorDia = $DaoGrupos->getHorasDiaGrupo($row_grupos['Id_grupo']);

                $LunesTrabajados = 0;
                $MartesTrabajados = 0;
                $MiercolesTrabajados = 0;
                $JuevesTrabajados = 0;
                $ViernesTrabajados = 0;
                $SabadoTrabajados = 0;
                $DomingosTrabajados=0;
                
                $ciclo = $DaoCiclos->show($Id_ciclo);
                $FechaIni = $FechaIniParam;
                if ($FechaIniParam == null) {
                    $FechaIni = $ciclo->getFecha_ini();
                }
                $FechaFin = $FechaFinParam;
                if ($FechaFinParam == null) {
                    $FechaFin = $ciclo->getFecha_fin();
                }

                $query_fechas = "";
                if (strlen($FechaIni) > 0 && $FechaFin == null) {
                    $query_fechas = $query_fechas . " AND Fecha_asis>='" . $FechaIni . "'";
                } elseif (strlen($FechaFin) > 0 && $FechaIni == null) {
                    $query_fechas = $query_fechas . " AND Fecha_asis<='" . $FechaFin . "'";
                } elseif (strlen($FechaIni) > 0 && strlen($FechaFin) > 0) {
                    $query_fechas = $query_fechas . " AND (Fecha_asis>='" . $FechaIni . "' AND Fecha_asis<='" . $FechaFin . "')";
                }


                $query = "SELECT * From Asistencias WHERE Id_grupo=" . $row_grupos['Id_grupo'] . " " . $query_fechas . " GROUP BY Fecha_asis ORDER BY Fecha_asis ASC";
                foreach ($DaoAsistencias->advanced_query($query) as $asis) {
                    if (date("N", strtotime($asis->getFecha_asis())) == 1) {
                        $LunesTrabajados+=$horasPorDia['L'];
                    } elseif (date("N", strtotime($asis->getFecha_asis())) == 2) {
                        $MartesTrabajados+=$horasPorDia['M'];
                    } elseif (date("N", strtotime($asis->getFecha_asis())) == 3) {
                        $MiercolesTrabajados+=$horasPorDia['I'];
                    } elseif (date("N", strtotime($asis->getFecha_asis())) == 4) {
                        $JuevesTrabajados+=$horasPorDia['J'];
                    } elseif (date("N", strtotime($asis->getFecha_asis())) == 5) {
                        $ViernesTrabajados+=$horasPorDia['V'];
                    } elseif (date("N", strtotime($asis->getFecha_asis())) == 6) {
                        $SabadoTrabajados+=$horasPorDia['S'];
                    }elseif (date("N", strtotime($asis->getFecha_asis())) == 7) {
                        $DomingosTrabajados+=$horasPorDia['D'];
                    }
                }

                $Grupo = array();
                $Grupo['Id_grupo'] = $row_grupos['Id_grupo'];
                $Grupo['Lunes'] = $horasPorDia['L'];
                $Grupo['Martes'] = $horasPorDia['M'];
                $Grupo['Miercoles'] = $horasPorDia['I'];
                $Grupo['Jueves'] = $horasPorDia['J'];
                $Grupo['Viernes'] = $horasPorDia['V'];
                $Grupo['Sabado'] = $horasPorDia['S'];
                $Grupo['Domingo'] = $horasPorDia['D'];
                $Grupo['HorasPor_semana'] = $horasPorDia['L'] + $horasPorDia['M'] + $horasPorDia['I'] + $horasPorDia['J'] + $horasPorDia['V'] + $horasPorDia['S']+ $horasPorDia['D'];
                $Grupo['HorasPor_semana_trabajados'] = $LunesTrabajados + $MartesTrabajados + $MiercolesTrabajados + $JuevesTrabajados + $ViernesTrabajados + $SabadoTrabajados+$DomingosTrabajados;
                array_push($resp['Grupos'], $Grupo);
            } while ($row_grupos = $grupos->fetch_assoc());

        }
        $resp['NumGrupos'] = $TotalGrupos;
        return $resp;
    }
    
    public function validarRecover($Recover){
	    $query="SELECT * FROM Docentes WHERE Recover_docen= '".$Recover."'";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
    }
    
        public function validarEmail($email){
            $query = sprintf("SELECT * FROM Docentes WHERE Email_docen= %s AND Baja_docen IS NULL",
            $this->GetSQLValueString($email, "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
    }
    
    public function validarDocente($email,$pass){
	    $query = sprintf("SELECT * FROM Docentes WHERE Email_docen= %s AND Pass_docen=%s AND Baja_docen IS NULL",
            $this->GetSQLValueString($email, "text"),
            $this->GetSQLValueString($this->hashPassword($pass), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
    }

    /*
      public function getLastEvaluacion($Id_doc,$Id_oferta){
      $resp=array();
      $query_evaluacion = "SELECT * FROM Evaluacion_docente WHERE Id_doc=".$Id_doc." ORDER  BY Id_ciclo DESC";
      $evaluacion = mysql_query($query_evaluacion, $this->cnn) or die(mysql_error());
      $row_evaluacion= mysql_fetch_array($evaluacion);
      $totalRows_evaluacion = mysql_num_rows($evaluacion);
      if($totalRows_evaluacion>0){
      if($Id_oferta==9 || $Id_oferta==14){
      //Prepa o Academia
      $query_Niveles_docentes = "SELECT * FROM Niveles_docentes WHERE Id_nivel=".$row_evaluacion['Profesor_a'];
      $Niveles_docentes = mysql_query($query_Niveles_docentes, $this->cnn) or die(mysql_error());
      $row_Niveles_docentes = mysql_fetch_array($Niveles_docentes);
      $Tipo="Profesor A";
      $nivel= $row_Niveles_docentes['Nombre_nivel'];
      $PagoXhora=$row_Niveles_docentes['Pago_nivel'];

      }elseif($Id_oferta==10){
      //Licenciatura
      $query_Niveles_docentes = "SELECT * FROM Niveles_docentes WHERE Id_nivel=".$row_evaluacion['Profesor_c'];
      $Niveles_docentes = mysql_query($query_Niveles_docentes, $this->cnn) or die(mysql_error());
      $row_Niveles_docentes = mysql_fetch_array($Niveles_docentes);
      $Tipo="Profesor C";
      $nivel= $row_Niveles_docentes['Nombre_nivel'];
      $PagoXhora=$row_Niveles_docentes['Pago_nivel'];

      }elseif($Id_oferta==11){
      //Carrera tecnica
      $query_Niveles_docentes = "SELECT * FROM Niveles_docentes WHERE Id_nivel=".$row_evaluacion['Profesor_b'];
      $Niveles_docentes = mysql_query($query_Niveles_docentes, $this->cnn) or die(mysql_error());
      $row_Niveles_docentes = mysql_fetch_array($Niveles_docentes);
      $Tipo="Profesor B";
      $nivel= $row_Niveles_docentes['Nombre_nivel'];
      $PagoXhora=$row_Niveles_docentes['Pago_nivel'];
      }elseif($Id_oferta==13){
      //Taller
      $query_Niveles_docentes = "SELECT * FROM Niveles_docentes WHERE Id_nivel=".$row_evaluacion['Profesor_d'];
      $Niveles_docentes = mysql_query($query_Niveles_docentes, $this->cnn) or die(mysql_error());
      $row_Niveles_docentes = mysql_fetch_array($Niveles_docentes);
      $Tipo="Profesor D";
      $nivel= $row_Niveles_docentes['Nombre_nivel'];
      $PagoXhora=$row_Niveles_docentes['Pago_nivel'];
      }
      $resp['Tipo']=$Tipo;
      $resp['Puntos']=$row_evaluacion['Puntos_totales'];;
      $resp['nivel']=$nivel;
      $resp['PagoPorHora']=$PagoXhora;
      }
      return $resp;

      }
     * 
     */
}
