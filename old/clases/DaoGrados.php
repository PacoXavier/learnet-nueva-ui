<?php
require_once 'modelos/base.php';
require_once 'modelos/Grados.php';

class DaoGrados extends base{

	public function add(Grados $x){
              $query=sprintf("INSERT INTO grados_ulm (Id_esp,Grado,Activo_grado) VALUES (%s ,%s, %s)",
              $this->GetSQLValueString($x->getId_esp(), "int"),
              $this->GetSQLValueString($x->getGrado(), "int"),
              $this->GetSQLValueString($x->getActivo_grado(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(Grados $x){
	    $query=sprintf("UPDATE grados_ulm SET Id_esp=%s,Grado=%s,Activo_grado=%s  WHERE Id_grado_ofe = %s",
            $this->GetSQLValueString($x->getId_esp(), "int"),
            $this->GetSQLValueString($x->getGrado(), "int"),
            $this->GetSQLValueString($x->getActivo_grado(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}
	
	
	
	public function delete($Id){
                $query = sprintf("DELETE FROM grados_ulm WHERE Id_grado_ofe=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
	
	public function show($Id){
	    $query="SELECT * FROM grados_ulm WHERE Id_grado_ofe= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	public function showByEspecialidadGrado($Id_esp,$grado){
	    $query="SELECT * FROM grados_ulm WHERE Id_esp= ".$Id_esp." AND Grado=".$grado;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM grados_ulm WHERE Activo_grado=1 ORDER BY Id_grado_ofe ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
	
	
	
	public function create_object($row){
            $x = new Grados();
            $x->setId($row['Id_grado_ofe']);
            $x->setId_esp($row['Id_esp']);
            $x->setGrado($row['Grado']);
            $x->setActivo_grado($row['Activo_grado']);
            return $x;
	}
         public function advanced_query($query){
		    $resp=array();
		    $consulta=$this->_cnn->query($query);
		    if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	}
        
        public function getGradosEspecialidad($Id_esp) {
            $query= "SELECT * FROM grados_ulm WHERE Id_esp=" . $Id_esp . " AND Activo_grado=1 ORDER BY Grado ASC";
            return $this->advanced_query($query);
       }

	

}

