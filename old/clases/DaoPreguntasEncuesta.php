<?php
require_once 'modelos/base.php';
require_once 'modelos/PreguntasEncuesta.php';

class DaoPreguntasEncuesta extends base{
    
	public function add(PreguntasEncuesta $x){
	    $query=sprintf("INSERT INTO PreguntasEncuesta (Pregunta, Activo, TipoPre, Id_en) VALUES (%s, %s,%s, %s)",
            $this->GetSQLValueString($x->getPregunta(), "text"), 
            $this->GetSQLValueString($x->getActivo(), "int"), 
            $this->GetSQLValueString($x->getTipoPre(), "text"), 
            $this->GetSQLValueString($x->getId_en(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(PreguntasEncuesta $x){
	    $query=sprintf("UPDATE PreguntasEncuesta SET Pregunta=%s, Activo=%s, TipoPre=%s, Id_en=%s WHERE Id_pre=%s",
            $this->GetSQLValueString($x->getPregunta(), "text"), 
            $this->GetSQLValueString($x->getActivo(), "int"), 
            $this->GetSQLValueString($x->getTipoPre(), "text"), 
            $this->GetSQLValueString($x->getId_en(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE PreguntasEncuesta SET Activo=0 WHERE Id_pre =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM PreguntasEncuesta WHERE Id_pre= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	public function create_object($row){
            $x = new PreguntasEncuesta();
            $x->setId($row['Id_pre']);
            $x->setPregunta($row['Pregunta']);
            $x->setActivo($row['Activo']);
            $x->setTipoPre($row['TipoPre']);
            $x->setId_en($row['Id_en']);
            return $x;
	}


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getPreguntasEncuesta($Id_encuesta){
            $query="SELECT *FROM PreguntasEncuesta WHERE Activo=1 AND Id_en=".$Id_encuesta;
            return $this->advanced_query($query);
        }


}
