<?php
require_once 'modelos/base.php';
require_once 'modelos/DiasInhabiles.php';

class DaoDiasInhabiles extends base{
	public function add(DiasInhabiles $x){
            
	    $query=sprintf("INSERT INTO Dias_inhabiles_ciclo (fecha_inh, Id_ciclo, EsFinDeSemana) VALUES (%s, %s,%s)",
            $this->GetSQLValueString($x->getFecha_inh(), "date"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"), 
            $this->GetSQLValueString($x->getEsFinDeSemana(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(DiasInhabiles $x){
	    $query=sprintf("UPDATE Dias_inhabiles_ciclo SET  fecha_inh=%s, Id_ciclo=%s, EsFinDeSemana=%s WHERE Id_dia=%s",
            $this->GetSQLValueString($x->getFecha_inh(), "date"), 
            $this->GetSQLValueString($x->getId_ciclo(), "int"), 
            $this->GetSQLValueString($x->getEsFinDeSemana(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM Dias_inhabiles_ciclo  WHERE Id_dia =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Dias_inhabiles_ciclo WHERE Id_dia= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function getDiasInhabilesCiclo($Id_ciclo){
            $resp=array();
            $query="SELECT * FROM Dias_inhabiles_ciclo WHERE Id_ciclo=".$Id_ciclo;
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new DiasInhabiles();
            $x->setId($row['Id_dia']);
            $x->setFecha_inh($row['fecha_inh']);
            $x->setId_ciclo($row['Id_ciclo']);
            $x->setEsFinDeSemana($row['EsFinDeSemana']);
            return $x;
	}


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


}
