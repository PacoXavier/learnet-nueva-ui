<?php
require_once 'modelos/base.php';
require_once 'modelos/RecargosCargoPeriodo.php';

class DaoRecargosCargoPeriodo extends base{
    
        public $table="RecargosCargoPeriodo";
    
	public function add(RecargosCargoPeriodo $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Fecha_recargo, Id_cargo, Monto, Comentarios, Id_usu_captura) VALUES (%s, %s, %s, %s,%s)",
            $this->GetSQLValueString($x->getFecha_recargo(), "date"), 
            $this->GetSQLValueString($x->getId_cargo(), "int"), 
            $this->GetSQLValueString($x->getMonto(), "double"), 
            $this->GetSQLValueString($x->getComentarios(), "text"),
            $this->GetSQLValueString($x->getId_usu_captura(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(RecargosCargoPeriodo $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Fecha_recargo=%s, Id_cargo=%s, Monto=%s, Comentarios=%s, Id_usu_captura=%s WHERE Id_rec=%s",
            $this->GetSQLValueString($x->getFecha_recargo(), "date"), 
            $this->GetSQLValueString($x->getId_cargo(), "int"), 
            $this->GetSQLValueString($x->getMonto(), "double"), 
            $this->GetSQLValueString($x->getComentarios(), "text"),
            $this->GetSQLValueString($x->getId_usu_captura(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	public function create_object($row){
            $x = new RecargosCargoPeriodo();
            $x->setId($row['Id_rec']);
            $x->setFecha_recargo($row['Fecha_recargo']);
            $x->setId_cargo($row['Id_cargo']);
            $x->setMonto($row['Monto']);
            $x->setComentarios($row['Comentarios']);
            $x->setId_usu_captura($row['Id_usu_captura']);
            return $x;
	}

        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_rec= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
       public function delete($Id){
            $query = sprintf("DELETE FROM  ".$this->table." WHERE Id_rec =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function getRecargosCargo($Id_cargo){
            $query="SELECT * FROM ".$this->table." WHERE Id_cargo=".$Id_cargo." ORDER BY Fecha_recargo ASC";
            return $this->advanced_query($query);
        }
        
        
        public function getTotalRecargosCargo($Id_cargo){
            $total=0;
            $query="SELECT * FROM ".$this->table." WHERE Id_cargo=".$Id_cargo." ORDER BY Fecha_recargo ASC";
            foreach($this->advanced_query($query) as $recargo){
                $total+=$recargo->getMonto();
            }
            return $total;
        }

}
