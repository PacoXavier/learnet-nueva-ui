<?php
require_once 'modelos/base.php';
require_once 'modelos/TiposUsuarios.php';

class DaoTiposUsuarios extends base{
    
	public function add(TiposUsuarios $x){
	    $query=sprintf("INSERT INTO tipos_usu_ulm (Nombre_tipo, Activo_tipo, Id_plantel) VALUES (%s, %s,%s)",
            $this->GetSQLValueString($x->getNombre_tipo(), "text"), 
            $this->GetSQLValueString($x->getActivo_tipo(), "int"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(TiposUsuarios $x){
	    $query=sprintf("UPDATE tipos_usu_ulm SET  Nombre_tipo=%s, Activo_tipo=%s, Id_plantel=%s WHERE Id_tipo=%s",
            $this->GetSQLValueString($x->getNombre_tipo(), "text"), 
            $this->GetSQLValueString($x->getActivo_tipo(), "int"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE tipos_usu_ulm SET Activo_tipo=0 WHERE Id_tipo =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM tipos_usu_ulm WHERE Id_tipo= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	
	public function create_object($row){
            $x = new TiposUsuarios();
            $x->setId($row['Id_tipo']);
            $x->setNombre_tipo($row['Nombre_tipo']);
            $x->setActivo_tipo($row['Activo_tipo']);
            $x->setId_plantel($row['Id_plantel']);
            return $x;
	}
                
	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}

        public function  getTiposUsuarios(){
            $query="SELECT * FROM tipos_usu_ulm WHERE Activo_tipo=1 AND Id_plantel=".$this->Id_plantel;
            return $this->advanced_query($query);
        }

}
