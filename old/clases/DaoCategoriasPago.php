<?php
require_once 'modelos/base.php';
require_once 'modelos/CategoriasPago.php';

class DaoCategoriasPago extends base{
    
        public $table="Tipos_docente";
    
	public function add(CategoriasPago $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Nombre_tipo, Descripcion, Activo,Id_plantel) VALUES (%s, %s,%s,%s)",
            $this->GetSQLValueString($x->getNombre_tipo(), "text"), 
            $this->GetSQLValueString($x->getDescripcion(), "int"), 
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getId_plantel(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(CategoriasPago $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Nombre_tipo=%s, Descripcion=%s, Activo=%s,Id_plantel=%s  WHERE Id_tipo=%s",
            $this->GetSQLValueString($x->getNombre_tipo(), "text"), 
            $this->GetSQLValueString($x->getDescripcion(), "int"), 
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	
	public function delete($Id){
            $query = sprintf("UPDATE ".$this->table." SET Activo=0 WHERE Id_tipo =%s",
            $this->GetSQLValueString($Id, "int")); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	public function show($Id){
	    $query=sprintf("SELECT * FROM ".$this->table." WHERE Id_tipo=%s",
            $this->GetSQLValueString($Id, "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new CategoriasPago();
            $x->setId($row['Id_tipo']);
            $x->setNombre_tipo($row['Nombre_tipo']);
            $x->setDescripcion($row['Descripcion']);
            $x->setActivo($row['Activo']);
            $x->setId_plantel($row['Id_plantel']);
            return $x;
	}


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        
        public function getCategoriasPago(){
            $query="SELECT * FROM ".$this->table." WHERE Activo=1 AND Id_plantel=".$this->Id_plantel." ORDER BY Id_tipo ASC";
            return $this->advanced_query($query);
        }

}
