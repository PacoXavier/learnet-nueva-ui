<?php
require_once 'modelos/base.php';
require_once 'modelos/Miscelaneos.php';

class DaoMiscelaneos extends base{

	public function add(Miscelaneos $x){
              $query=sprintf("INSERT INTO Micelaneos (Nombre_mis,Costo_mis,Activo_mis,Id_plantel) VALUES (%s ,%s, %s, %s)",
              $this->GetSQLValueString($x->getNombre_mis(), "text"),
              $this->GetSQLValueString($x->getCosto_mis(), "int"),
              $this->GetSQLValueString($x->getActivo_mis(), "int"),
              $this->GetSQLValueString($x->getId_plantel(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(Miscelaneos $x){
	  $query=sprintf("UPDATE Micelaneos SET Nombre_mis=%s,Costo_mis=%s,Activo_mis=%s,Id_plantel=%s WHERE Id_mis = %s",
          $this->GetSQLValueString($x->getNombre_mis(), "text"),
          $this->GetSQLValueString($x->getCosto_mis(), "int"),
          $this->GetSQLValueString($x->getActivo_mis(), "int"),
          $this->GetSQLValueString($x->getId_plantel(), "int"),
          $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
        

	public function delete($Id){
                $query = sprintf("UPDATE FROM Miscelaneos SET Activo_mis=0 WHERE Id_mis=".$Id); 
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                      throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                        return true;
                }
	}
	
        
	public function show($Id){
	    $query="SELECT * FROM Micelaneos WHERE Id_mis= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Micelaneos WHERE Activo_mis=1 AND Id_plantel=".$this->Id_plantel." ORDER BY Id_mis DESC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
	
	
	
	public function create_object($row){
            $x = new Miscelaneos();
            $x->setId($row['Id_mis']);
            $x->setNombre_mis($row['Nombre_mis']);
            $x->setCosto_mis($row['Costo_mis']);
            $x->setActivo_mis($row['Activo_mis']); 
            $x->setId_plantel($row['Id_plantel']);
            return $x;
	}

	public function advanced_query($query){
		$resp=array();
		$consulta=$this->_cnn->query($query);
		    if(!$consulta){
	            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
			    if($totalRows_consulta>0){
				   do{
				      array_push($resp, $this->create_object($row_consulta));
			       }while($row_consulta= $consulta->fetch_assoc());  
			    }
	        }
		    return $resp;
	}

}


