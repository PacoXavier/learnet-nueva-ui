<?php
require_once 'modelos/base.php';
require_once 'modelos/Activos.php';


class DaoActivos extends base{
    
	public function add(Activos $x){
	    $query=sprintf("INSERT INTO Activos (Nombre, Modelo, Descripcion, Id_img, Valor, Codigo, Tipo_act , Fecha_adq , Disponible , Garantia_meses , Mantenimiento_frecuencia , Id_plantel , Baja_activo , Id_aula , Comentario_baja, Marca, Num_serie) VALUES (%s, %s,%s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getModelo(), "text"), 
            $this->GetSQLValueString($x->getDescripcion(), "text"), 
            $this->GetSQLValueString($x->getId_img(), "text"), 
            $this->GetSQLValueString($x->getValor(), "double"), 
            $this->GetSQLValueString($x->getCodigo(), "text"), 
            $this->GetSQLValueString($x->getTipo_act(), "int"), 
            $this->GetSQLValueString($x->getFecha_adq(), "date"), 
            $this->GetSQLValueString($x->getDisponible(), "int"), 
            $this->GetSQLValueString($x->getGarantia_meses(), "text"), 
            $this->GetSQLValueString($x->getMantenimiento_frecuencia(), "int"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"), 
            $this->GetSQLValueString($x->getBaja_activo(), "date"), 
            $this->GetSQLValueString($x->getId_aula(), "int"), 
            $this->GetSQLValueString($x->getComentario_baja(), "text"), 
            $this->GetSQLValueString($x->getMarca(), "text"), 
            $this->GetSQLValueString($x->getNum_serie(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Activos $x){
	    $query=sprintf("UPDATE Activos SET  Nombre=%s, Modelo=%s, Descripcion=%s, Id_img=%s, Valor=%s, Codigo=%s, Tipo_act=%s , Fecha_adq=%s , Disponible=%s , Garantia_meses=%s , Mantenimiento_frecuencia=%s , Id_plantel=%s , Baja_activo=%s , Id_aula=%s , Comentario_baja=%s, Marca=%s, Num_serie=%s WHERE Id_activo=%s",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getModelo(), "text"), 
            $this->GetSQLValueString($x->getDescripcion(), "text"), 
            $this->GetSQLValueString($x->getId_img(), "text"), 
            $this->GetSQLValueString($x->getValor(), "double"), 
            $this->GetSQLValueString($x->getCodigo(), "text"), 
            $this->GetSQLValueString($x->getTipo_act(), "int"), 
            $this->GetSQLValueString($x->getFecha_adq(), "date"), 
            $this->GetSQLValueString($x->getDisponible(), "int"), 
            $this->GetSQLValueString($x->getGarantia_meses(), "text"), 
            $this->GetSQLValueString($x->getMantenimiento_frecuencia(), "int"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"), 
            $this->GetSQLValueString($x->getBaja_activo(), "date"), 
            $this->GetSQLValueString($x->getId_aula(), "int"), 
            $this->GetSQLValueString($x->getComentario_baja(), "text"), 
            $this->GetSQLValueString($x->getMarca(), "text"), 
            $this->GetSQLValueString($x->getNum_serie(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	
	public function show($Id){
	    $query="SELECT * FROM Activos WHERE Id_activo= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Activos WHERE Baja_activo IS NULL AND Id_plantel=".$this->Id_plantel;
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new Activos();
            $x->setId($row['Id_activo']);
            $x->setNombre($row['Nombre']);
            $x->setModelo($row['Modelo']);
            $x->setDescripcion($row['Descripcion']);
            $x->setId_img($row['Id_img']);
            $x->setValor($row['Valor']);
            $x->setCodigo($row['Codigo']);
            $x->setTipo_act($row['Tipo_act']);
            $x->setFecha_adq($row['Fecha_adq']);
            $x->setDisponible($row['Disponible']);
            $x->setGarantia_meses($row['Garantia_meses']);
            $x->setMantenimiento_frecuencia($row['Mantenimiento_frecuencia']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setBaja_activo($row['Baja_activo']);
            $x->setId_aula($row['Id_aula']);
            $x->setComentario_baja($row['Comentario_baja']);
            $x->setMarca($row['Marca']);
            $x->setNum_serie($row['Num_serie']);
            
            return $x;
	}
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
       public function buscarActivo($buscar,$limit=20){
            $resp=array();
            $query = "
            SELECT *, CONCAT(
                IFNULL(REPLACE(Nombre,' ',''),''),
                IFNULL(REPLACE(Modelo,' ','') ,''),
                IFNULL(REPLACE(Codigo,' ','') ,''),
                IFNULL(REPLACE(Num_serie,' ','') ,'')) AS Buscar 
            FROM Activos   
            WHERE  (Nombre LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR Modelo LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR Codigo LIKE '%".str_replace(' ','',$buscar)."%' 
                    OR Num_serie LIKE '%".str_replace(' ','',$buscar)."%')
             AND Id_plantel=".$this->Id_plantel." LIMIT ".$limit;
                $consulta=$this->_cnn->query($query);
                if(!$consulta){
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                    $row_consulta= $consulta->fetch_assoc();
                    $totalRows_consulta= $consulta->num_rows;
                    if($totalRows_consulta>0){
                           do{
                              array_push($resp, $this->create_object($row_consulta));
                       }while($row_consulta= $consulta->fetch_assoc());  
                    }
                }
                return $resp;
    }
    
    public function generar_codigo_activo($Id_tipo) {
      //Generamos la matricula del activo
      //Tomando como base el primer digito es del tipo al que pertence
      //y los restantes son la ultima matricula existente
      $query = "SELECT * FROM Activos WHERE  Codigo IS NOT NULL AND Id_plantel=".$this->Id_plantel." AND Tipo_act =".$Id_tipo." ORDER BY Codigo DESC LIMIT 1";
      $consulta = $this->_cnn->query($query);
      $row_consulta= $consulta->fetch_assoc();
      $totalRows_consulta= $consulta->num_rows;
      $matricula=0;
      if (strlen($row_consulta['Codigo'])>0) {
            $matricula = substr($row_consulta['Codigo'], 1);
            $matricula = (int) $Id_tipo.$matricula;
            $matricula++;
      }else{
            $matricula++;
            $ceros="";
            for($i=0;$i<(4-strlen($matricula));$i++){
               $ceros.="0";
            }
            $matricula=$Id_tipo.$ceros.$matricula;
      }
      return $matricula;
    }


}
