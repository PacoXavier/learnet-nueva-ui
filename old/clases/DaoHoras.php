<?php
require_once 'modelos/base.php';
require_once 'modelos/Horas.php';


class DaoHoras extends base{

	public function show($Id){
	    $query="SELECT * FROM Horas WHERE Id_hora= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

        public function getTextoHora($Texto_hora){
	    $query="SELECT * FROM Horas WHERE Texto_hora LIKE '%" . $Texto_hora . "%'";
            $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Horas ORDER BY Id_hora ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new Horas();
            $x->setId($row['Id_hora']);
            $x->setTexto_hora($row['Texto_hora']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getRangoEntreHoras($HoraIni,$HoraFin){
            $query="SELECT * FROM Horas WHERE Id_hora BETWEEN ".$HoraIni." AND ".$HoraFin;
            return $this->advanced_query($query);
        }
        
        public function getTotalHoras($HoraIni,$HoraFin){
            $textoHoraIni=$this->getTxtHora($HoraIni);
            $textoHoraFin=$this->getTxtHora($HoraFin);
            
            $idHoraIni=$this->getTextoHora($textoHoraIni);
            $idHoraFin=$this->getTextoHora($textoHoraFin);
            
            $total=0;
            $query="SELECT * FROM Horas WHERE Id_hora BETWEEN ".$idHoraIni->getId()." AND ".$idHoraFin->getId();
            foreach($this->advanced_query($query) as $i){
                $total++;
            }
            if($total>0){
               $total=$total-1;
            }
            return $total;
        }
}



