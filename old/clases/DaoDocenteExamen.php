<?php
require_once 'modelos/base.php';
require_once 'modelos/DocenteExamen.php';
require_once 'DaoHoras.php';

class DaoDocenteExamen extends base{
	public function add(DocenteExamen $x){
	    $query=sprintf("INSERT INTO DocenteExamen (Id_docen, IdHorarioExamen) VALUES (%s, %s)",
            $this->GetSQLValueString($x->getId_docen(), "int"), 
            $this->GetSQLValueString($x->getIdHorarioExamen(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(DocenteExamen $x){
	    $query=sprintf("UPDATE DocenteExamen SET Id_docen=%s, IdHorarioExamen=%s WHERE IdDocExamen=%s",
            $this->GetSQLValueString($x->getId_docen(), "int"), 
            $this->GetSQLValueString($x->getIdHorarioExamen(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM DocenteExamen WHERE IdDocExamen =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        
       public function deleteDocentesExamen($Id){
            $query = sprintf("DELETE FROM DocenteExamen WHERE IdHorarioExamen =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function getDocenteExamen($Id){
	    $query="SELECT * FROM DocenteExamen WHERE IdDocExamen= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
	
	public function create_object($row){
            $x = new DocenteExamen();
            $x->setId($row['IdDocExamen']);
            $x->setId_docen($row['Id_docen']);
            $x->setIdHorarioExamen($row['IdHorarioExamen']);
            return $x;
	}
                
	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}

        public function getDocentesExamenHorario($IdHorarioExamen) {
            $query= "SELECT * FROM DocenteExamen WHERE IdHorarioExamen=" . $IdHorarioExamen . " ORDER BY IdDocExamen ASC";
            return $this->advanced_query($query);
       }
       
       
       	public function getDocenteExamenByHorario($Id_docen,$IdHorarioExamen){
	    $query="SELECT * FROM DocenteExamen WHERE Id_docen= ".$Id_docen." AND IdHorarioExamen=".$IdHorarioExamen;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
       
        
        public function getExamenesDocenteCiclo($Id_ciclo,$Id_docen,$Id_grupo=null) {
            $q_grupo="";
            if($Id_grupo!=null){
               $q_grupo=" AND GrupoExamen.IdGrupo=".$Id_grupo;
            }
            $resp=array();
            $query="SELECT * FROM HorarioExamenGrupo 
                    JOIN DocenteExamen ON HorarioExamenGrupo.Id_horario=DocenteExamen.IdHorarioExamen
                    JOIN GrupoExamen ON HorarioExamenGrupo.Id_horario=GrupoExamen.IdHorarioExamen
                    WHERE  HorarioExamenGrupo.Id_ciclo=".$Id_ciclo." AND DocenteExamen.Id_docen=".$Id_docen." ".$q_grupo;
            $consulta=base::advanced_query($query);
            $row_consulta = $consulta->fetch_assoc();
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $row_consulta);
                   
               } while ($row_consulta = $consulta->fetch_assoc());
            }
            return $resp;
       }
       
       	public function getExamenDocenteGrupoFecha($Id_docente,$Id_grupo,$dia){
	    $query="SELECT * FROM HorarioExamenGrupo 
                    JOIN DocenteExamen ON HorarioExamenGrupo.Id_horario=DocenteExamen.IdHorarioExamen
                    JOIN GrupoExamen ON HorarioExamenGrupo.Id_horario=GrupoExamen.IdHorarioExamen
                    WHERE  DocenteExamen.Id_docen=".$Id_docente."
                           AND GrupoExamen.IdGrupo=".$Id_grupo."
                           AND HorarioExamenGrupo.FechaAplicacion='".$dia."'";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function getTotalHorasTrabajadasExamen($Id_ciclo,$Id_docen,$Id_oferta=null,$Id_grupo=null,$FechaIni=null,$FechaFin=null) {
            $DaoHoras= new DaoHoras();
            $q_grupo="";
            if($Id_grupo!=null){
               $q_grupo=" AND Asistencias.Id_grupo=".$Id_grupo;
            }

            $q_oferta="";
            if($Id_oferta!=null){
               $q_oferta=" AND ofertas_ulm.Id_oferta=".$Id_oferta;
            }
            
            $q_fechas = "";
            if (strlen($FechaIni) > 0 && $FechaFin == null) {
                $q_fechas = $q_fechas . " AND HorarioExamenGrupo.FechaAplicacion>='" . $FechaIni . "'";
            } elseif (strlen($FechaFin) > 0 && $FechaIni == null) {
                $q_fechas = $q_fechas . " AND HorarioExamenGrupo.FechaAplicacion<='" . $FechaFin . "'";
            } elseif (strlen($FechaIni) > 0 && strlen($FechaFin) > 0) {
                $q_fechas = $q_fechas . " AND (HorarioExamenGrupo.FechaAplicacion>='" . $FechaIni . "' AND HorarioExamenGrupo.FechaAplicacion<='" . $FechaFin . "')";
            }
            
            $total=0;
            $query="SELECT Asistencias.*,HorarioExamenGrupo.* FROM HorarioExamenGrupo 
                        JOIN DocenteExamen ON HorarioExamenGrupo.Id_horario=DocenteExamen.IdHorarioExamen
                        JOIN GrupoExamen ON HorarioExamenGrupo.Id_horario=GrupoExamen.IdHorarioExamen
                        JOIN Asistencias ON (GrupoExamen.IdGrupo=Asistencias.Id_grupo AND HorarioExamenGrupo.FechaAplicacion=Asistencias.Fecha_asis)
                        JOIN Grupos ON GrupoExamen.IdGrupo=Grupos.Id_grupo
                        JOIN Materias_especialidades ON  Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
                        JOIN especialidades_ulm ON Materias_especialidades.Id_esp=especialidades_ulm.Id_esp
                        JOIN ofertas_ulm ON especialidades_ulm.Id_ofe=ofertas_ulm.Id_oferta
                    WHERE  HorarioExamenGrupo.Id_ciclo=".$Id_ciclo."
                            AND DocenteExamen.Id_docen=".$Id_docen."
                            ".$q_grupo."
                            ".$q_oferta."
                            ".$q_fechas."
                     GROUP BY Id_grupo ORDER BY FechaAplicacion DESC";
            foreach(base::advanced_query($query) as $row){
                    $total+=$DaoHoras->getTotalHoras($row['Start'], $row['End']);
            }
            return $total;
        }
        
        
        public function getDetalleHorasTrabajadasExamen($Id_ciclo,$Id_docen,$Id_oferta=null,$Id_grupo=null,$FechaIni=null,$FechaFin=null) {
            $q_grupo="";
            if($Id_grupo!=null){
               $q_grupo=" AND Asistencias.Id_grupo=".$Id_grupo;
            }

            $q_oferta="";
            if($Id_oferta!=null){
               $q_oferta=" AND ofertas_ulm.Id_oferta=".$Id_oferta;
            }
            
            $q_fechas = "";
            if (strlen($FechaIni) > 0 && $FechaFin == null) {
                $q_fechas = $q_fechas . " AND HorarioExamenGrupo.FechaAplicacion>='" . $FechaIni . "'";
            } elseif (strlen($FechaFin) > 0 && $FechaIni == null) {
                $q_fechas = $q_fechas . " AND HorarioExamenGrupo.FechaAplicacion<='" . $FechaFin . "'";
            } elseif (strlen($FechaIni) > 0 && strlen($FechaFin) > 0) {
                $q_fechas = $q_fechas . " AND (HorarioExamenGrupo.FechaAplicacion>='" . $FechaIni . "' AND HorarioExamenGrupo.FechaAplicacion<='" . $FechaFin . "')";
            }
            $query="SELECT Asistencias.*,HorarioExamenGrupo.*, aulas.Clave_aula, materias_uml.Nombre,Grupos.Clave
                   FROM HorarioExamenGrupo 
                        JOIN DocenteExamen ON HorarioExamenGrupo.Id_horario=DocenteExamen.IdHorarioExamen
                        JOIN GrupoExamen ON HorarioExamenGrupo.Id_horario=GrupoExamen.IdHorarioExamen
                        JOIN Asistencias ON (GrupoExamen.IdGrupo=Asistencias.Id_grupo AND HorarioExamenGrupo.FechaAplicacion=Asistencias.Fecha_asis)
                        JOIN Grupos ON GrupoExamen.IdGrupo=Grupos.Id_grupo
                        JOIN Materias_especialidades ON  Grupos.Id_mat_esp=Materias_especialidades.Id_mat_esp
                        JOIN especialidades_ulm ON Materias_especialidades.Id_esp=especialidades_ulm.Id_esp
                        JOIN ofertas_ulm ON especialidades_ulm.Id_ofe=ofertas_ulm.Id_oferta
                        JOIN aulas ON HorarioExamenGrupo.Id_aula=aulas.Id_aula
                        JOIN materias_uml ON Grupos.Id_mat=materias_uml.Id_mat
                    WHERE  HorarioExamenGrupo.Id_ciclo=".$Id_ciclo."
                            AND DocenteExamen.Id_docen=".$Id_docen."
                            ".$q_grupo."
                            ".$q_oferta."
                            ".$q_fechas."
                     GROUP BY Id_grupo ORDER BY FechaAplicacion DESC";
            return base::advanced_query($query);
        }

}
