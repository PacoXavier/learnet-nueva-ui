<?php
require_once 'modelos/base.php';
require_once 'modelos/Attachments.php';

class DaoAttachments extends base{

	public function add(Attachments $x){
              $query=sprintf("INSERT INTO Attachments (Nombre_atta,Mime_atta,Llave_atta,Id_plantel,IdRel,TipoRel) VALUES (%s ,%s, %s,%s, %s,%s)",
              $this->GetSQLValueString($x->getNombre_atta(), "text"),
              $this->GetSQLValueString($x->getMime_atta(), "text"),
              $this->GetSQLValueString($x->getLlave_atta(), "text"),
              $this->GetSQLValueString($x->getId_plantel(), "int"),
              $this->GetSQLValueString($x->getIdRel(), "int"),
              $this->GetSQLValueString($x->getTipoRel(), "text"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                       return $this->_cnn->insert_id; 
                }
	}

	
	public function update(Attachments $x){
	    $query=sprintf("UPDATE Attachments SET Nombre_atta=%s,Mime_atta=%s,Llave_atta=%s,Id_plantel=%s,IdRel=%s,TipoRel=%s  WHERE Id_atta = %s",
            $this->GetSQLValueString($x->getNombre_atta(), "text"),
            $this->GetSQLValueString($x->getMime_atta(), "text"),
            $this->GetSQLValueString($x->getLlave_atta(), "text"),
            $this->GetSQLValueString($x->getId_plantel(), "int"),
            $this->GetSQLValueString($x->getIdRel(), "int"),
            $this->GetSQLValueString($x->getTipoRel(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}
	
	
	
	public function delete($Id){
            if($Id>0){
                $ata=$this->show($Id);
                if($ata->getId()>0){
                    if (file_exists("attachments/".$ata->getLlave_atta().".ulm")) {
                        $query = sprintf("DELETE FROM Attachments WHERE Id_atta=".$Id); 
                        $Result1=$this->_cnn->query($query);
                        if(!$Result1) {
                              throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                        }else{
                             unlink("attachments/".$ata->getLlave_atta().".ulm");
                             return true;
                        }
                    }
                }
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Attachments WHERE Id_atta= ".$Id;
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Attachments WHERE Id_plantel=".$this->Id_plantel." ORDER BY Id_atta ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
                   do{
                      array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
	
	
	public function create_object($row){
            $x = new Attachments();
            $x->setId($row['Id_atta']);
            $x->setNombre_atta($row['Nombre_atta']);
            $x->setMime_atta($row['Mime_atta']);
            $x->setLlave_atta($row['Llave_atta']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setIdRel($row['IdRel']);
            $x->setTipoRel($row['TipoRel']);
            return $x;
	}
        
        
         public function advanced_query($query){
                $resp=array();
                $consulta=$this->_cnn->query($query);
                if(!$consulta){
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
	        }else{
	            $row_consulta= $consulta->fetch_assoc();
	            $totalRows_consulta= $consulta->num_rows;
                    if($totalRows_consulta>0){
                           do{
                              array_push($resp, $this->create_object($row_consulta));
                       }while($row_consulta= $consulta->fetch_assoc());  
                    }
	        }
		return $resp;
	}
        
        public function getAttachmentsUsu($IdRel,$TipoRel){
            $query="SELECT * FROM Attachments WHERE IdRel=".$IdRel." AND TipoRel='".$TipoRel."' and Id_plantel=".$this->Id_plantel;
            return $this->advanced_query($query);
        }
        
        public function deleteAllAttachments($Id){
            $query="SELECT * FROM Attachments WHERE Id_plantel=".$Id;
            foreach($this->advanced_query($query) as $ata){
                    $this->delete($ata->getId());
            }
	}
        
        public function deleteAttachmentsUsu($IdRel,$TipoRel){
            foreach($this->getAttachmentsUsu($IdRel,$TipoRel) as $ata){
                    $this->delete($ata->getId());
            }
	}
        
}

