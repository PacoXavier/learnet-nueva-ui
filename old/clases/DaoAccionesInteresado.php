<?php
require_once 'modelos/base.php';
require_once 'modelos/AccionesInteresado.php';

class DaoAccionesInteresado extends base{
	public function add(AccionesInteresado $x){
	    $query=sprintf("INSERT INTO Acciones_interesado (Id_int, TipoAccion, Id_ofe_alum, Asistio, CostoAccion, FechaDeposito, Pago, FechaAccion, CalificacionExamen, FechaSession, Usuario, Plantel) VALUES (%s, %s,%s, %s,%s,%s, %s,%s, %s,%s,%s,%s)",
            $this->GetSQLValueString($x->getId_int(), "int"), 
            $this->GetSQLValueString($x->getTipoAccion(), "int"), 
            $this->GetSQLValueString($x->getId_ofe_alum(), "int"), 
            $this->GetSQLValueString($x->getAsistio(), "int"), 
            $this->GetSQLValueString($x->getCostoAccion(), "double"),
            $this->GetSQLValueString($x->getFechaDeposito(), "int"),
            $this->GetSQLValueString($x->getPago(), "int"),
            $this->GetSQLValueString($x->getFechaAccion(), "date"),
            $this->GetSQLValueString($x->getCalificacionExamen(), "double"),
            $this->GetSQLValueString($x->getFechaSession(), "date"),
            $this->GetSQLValueString($x->getUsuario(), "int"),
            $this->GetSQLValueString($x->getPlantel(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(AccionesInteresado $x){
	    $query=sprintf("UPDATE Acciones_interesado SET  Id_int=%s, TipoAccion=%s, Id_ofe_alum=%s, Asistio=%s, CostoAccion=%s, FechaDeposito=%s, Pago=%s, FechaAccion=%s, CalificacionExamen=%s, FechaSession=%s, Usuario=%s, Plantel=%s WHERE Id_accion=%s",
            $this->GetSQLValueString($x->getId_int(), "int"), 
            $this->GetSQLValueString($x->getTipoAccion(), "int"), 
            $this->GetSQLValueString($x->getId_ofe_alum(), "int"), 
            $this->GetSQLValueString($x->getAsistio(), "int"), 
            $this->GetSQLValueString($x->getCostoAccion(), "double"),
            $this->GetSQLValueString($x->getFechaDeposito(), "int"),
            $this->GetSQLValueString($x->getPago(), "int"),
            $this->GetSQLValueString($x->getFechaAccion(), "date"),
            $this->GetSQLValueString($x->getCalificacionExamen(), "double"),
            $this->GetSQLValueString($x->getFechaSession(), "date"),
            $this->GetSQLValueString($x->getUsuario(), "int"),
            $this->GetSQLValueString($x->getPlantel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM Acciones_interesado  WHERE Id_accion =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Acciones_interesado WHERE Id_accion= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Acciones_interesado WHERE Plantel=".$this->Id_plantel." AND Asistio IS NULL ORDER BY Id_accion DESC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new AccionesInteresado();
            $x->setId($row['Id_accion']);
            $x->setId_int($row['Id_int']);
            $x->setTipoAccion($row['TipoAccion']);
            $x->setId_ofe_alum($row['Id_ofe_alum']);
            $x->setAsistio($row['Asistio']);
            $x->setCostoAccion($row['CostoAccion']);
            $x->setFechaDeposito($row['FechaDeposito']);
            $x->setPago($row['Pago']);
            $x->setFechaAccion($row['FechaAccion']);
            $x->setCalificacionExamen($row['CalificacionExamen']);
            $x->setFechaSession($row['FechaSession']);
            $x->setUsuario($row['Usuario']);
            $x->setPlantel($row['Plantel']);
            
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}

}




