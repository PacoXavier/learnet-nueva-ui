<?php
require_once 'modelos/base.php';
require_once 'modelos/NotificacionesUsuario.php';

class DaoNotificacionesUsuario extends base{
    
	public function add(NotificacionesUsuario $x){
	  $query=sprintf("INSERT INTO NotificacionesUsuario (Titulo, Texto, DateCreated, DateRead, TipoRel, IdRel) VALUES (%s, %s,%s, %s,%s, %s)",
            $this->GetSQLValueString($x->getTitulo(), "text"), 
            $this->GetSQLValueString($x->getTexto(), "text"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getDateRead(), "date"), 
            $this->GetSQLValueString($x->getTipoRel(), "text"), 
            $this->GetSQLValueString($x->getIdRel(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(NotificacionesUsuario $x){
	    $query=sprintf("UPDATE NotificacionesUsuario SET  Titulo=%s, Texto=%s, DateCreated=%s, DateRead=%s, TipoRel=%s, IdRel=%s WHERE Id_not=%s",
            $this->GetSQLValueString($x->getTitulo(), "text"), 
            $this->GetSQLValueString($x->getTexto(), "text"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getDateRead(), "date"), 
            $this->GetSQLValueString($x->getTipoRel(), "text"), 
            $this->GetSQLValueString($x->getIdRel(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
                $Result1=$this->_cnn->query($query);
                if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
                }else{
                      return $x->getId();  
                }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM NotificacionesUsuario WHERE Id_not =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM NotificacionesUsuario WHERE Id_not=".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}



	
	public function create_object($row){
            $x = new NotificacionesUsuario();
            $x->setId($row['Id_not']);
            $x->setTitulo($row['Titulo']);
            $x->setTexto($row['Texto']);
            $x->setDateCreated($row['DateCreated']);
            $x->setDateRead($row['DateRead']);
            $x->setTipoRel($row['TipoRel']);
            $x->setIdRel($row['IdRel']);
            return $x;
	}
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}

        public function getNotificacionesUsuario($Id_usu,$Tipo){
            $query= "SELECT * FROM NotificacionesUsuario WHERE DateRead IS NULL AND TipoRel='".$Tipo."' AND IdRel=".$Id_usu." ORDER BY Id_not DESC";
            return $this->advanced_query($query);

        } 

       public function deleteNotificacionesUsuarioByFecha($Id_usu,$Tipo,$DateCreated){
            $query= "DELETE FROM NotificacionesUsuario WHERE TipoRel='".$Tipo."' AND IdRel=".$Id_usu." AND DateCreated='".$DateCreated."'";
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }

        } 
        
}
