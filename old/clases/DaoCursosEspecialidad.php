<?php
require_once 'modelos/base.php';
require_once 'modelos/CursosEspecialidad.php';


class DaoCursosEspecialidad extends base{
    
        public $table="CursosEspecialidad";
    
	public function add(CursosEspecialidad $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_esp, FechaInicio, FechaFin, DateCreated, TipoPago, DiaPago, Id_usu_capura , Activo , Clave,Num_horas, Num_sesiones, Generar_cargos_recurrentes, Dia_cargo_recurrente, Turno) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s,%s, %s,%s,%s)",
            $this->GetSQLValueString($x->getId_esp(), "int"), 
            $this->GetSQLValueString($x->getFechaInicio(), "date"), 
            $this->GetSQLValueString($x->getFechaFin(), "date"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getTipoPago(), "int"), 
            $this->GetSQLValueString($x->getDiaPago(), "int"), 
            $this->GetSQLValueString($x->getId_usu_capura(), "int"), 
            $this->GetSQLValueString($x->getActivo(), "int"), 
            $this->GetSQLValueString($x->getClave(), "text"),
            $this->GetSQLValueString($x->getNum_horas(), "int"),
            $this->GetSQLValueString($x->getNum_sesiones(), "int"),
            $this->GetSQLValueString($x->getGenerar_cargos_recurrentes(), "int"),
            $this->GetSQLValueString($x->getDia_cargo_recurrente(), "int"),
            $this->GetSQLValueString($x->getTurno(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(CursosEspecialidad $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Id_esp=%s, FechaInicio=%s, FechaFin=%s, DateCreated=%s, TipoPago=%s, DiaPago=%s, Id_usu_capura=%s, Activo=%s, Clave=%s, Num_horas=%s, Num_sesiones=%s, Generar_cargos_recurrentes=%s, Dia_cargo_recurrente=%s, Turno=%s WHERE Id_curso_esp=%s",
            $this->GetSQLValueString($x->getId_esp(), "int"), 
            $this->GetSQLValueString($x->getFechaInicio(), "date"), 
            $this->GetSQLValueString($x->getFechaFin(), "date"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getTipoPago(), "int"), 
            $this->GetSQLValueString($x->getDiaPago(), "int"), 
            $this->GetSQLValueString($x->getId_usu_capura(), "int"), 
            $this->GetSQLValueString($x->getActivo(), "int"), 
            $this->GetSQLValueString($x->getClave(), "text"),
            $this->GetSQLValueString($x->getNum_horas(), "int"),
            $this->GetSQLValueString($x->getNum_sesiones(), "int"),
            $this->GetSQLValueString($x->getGenerar_cargos_recurrentes(), "int"),
            $this->GetSQLValueString($x->getDia_cargo_recurrente(), "int"),
            $this->GetSQLValueString($x->getTurno(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	public function create_object($row){
            $x = new CursosEspecialidad();
            $x->setId($row['Id_curso_esp']);
            $x->setId_esp($row['Id_esp']);
            $x->setFechaInicio($row['FechaInicio']);
            $x->setFechaFin($row['FechaFin']);
            $x->setDateCreated($row['DateCreated']);
            $x->setTipoPago($row['TipoPago']);
            $x->setDiaPago($row['DiaPago']);
            $x->setId_usu_capura($row['Id_usu_capura']);
            $x->setActivo($row['Activo']);
            $x->setClave($row['Clave']);
            $x->setNum_horas($row['Num_horas']);
            $x->setNum_sesiones($row['Num_sesiones']);
            $x->setGenerar_cargos_recurrentes($row['Generar_cargos_recurrentes']);
            $x->setDia_cargo_recurrente($row['Dia_cargo_recurrente']);
            $x->setTurno($row['Turno']);
            return $x;
	}
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_curso_esp= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
       public function delete($Id){
            $query = sprintf("UPDATE ".$this->table." SET Activo=0 WHERE Id_curso_esp =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function getCursosEspecialidad($Id_esp,$activos=null,$turno=null){
            $q="";
            if($activos!=null){
               $q=" AND ((FechaInicio<='".date('Y-m-d')."' AND FechaFin>='".date('Y-m-d')."') OR (FechaInicio IS NULL AND FechaFin IS NULL) OR (FechaInicio>='".date('Y-m-d')."'))";
            }
            
            if($turno!=null){
               $q.=" AND Turno=".$turno;
            }
            $query="SELECT * FROM ".$this->table." WHERE Activo=1 AND Id_esp=".$Id_esp." ".$q." ORDER BY FechaInicio DESC";
            return $this->advanced_query($query);
        }
        
        
        public function getFechaPagoInscripcion($Id_curso_esp){
            $FechaDePago="";
            $cursoEsp=$this->show($Id_curso_esp);
            if($cursoEsp->getTipoPago()==1){
               //Fecha de inicio del curso
               $FechaDePago= $cursoEsp->getFechaInicio();
            }elseif($cursoEsp->getTipoPago()==2){
               //Dia especifico del mes
                $FechaDePago = date("Y-m-d", mktime(0, 0, 0, date('m') , $cursoEsp->getDiaPago(), date('Y')));
                /*
                if($FechaDePago<date('Y-m-d')){
                   $FechaDePago = date("Y-m-d", mktime(0, 0, 0, date('m')+1 , $cursoEsp->getDiaPago(), date('Y')));
                }
                 * 
                 */
            }elseif($cursoEsp->getTipoPago()==3){
               //Dia en que ingresa el alumo
               $FechaDePago=date('Y-m-d');
            }
            return $FechaDePago;
        }
        
        
       public function getFechaPago($Id_curso_esp){
            $FechaDePago="";
            $cursoEsp=$this->show($Id_curso_esp);
            if($cursoEsp->getTipoPago()==1){
               //Fecha de inicio del curso
               $FechaDePago= $cursoEsp->getFechaInicio();
            }elseif($cursoEsp->getTipoPago()==2){
               //Dia especifico del mes
                $FechaDePago = date("Y-m-d", mktime(0, 0, 0, date('m') , $cursoEsp->getDiaPago(), date('Y')));
                /*
                if($FechaDePago<date('Y-m-d')){
                   $FechaDePago = date("Y-m-d", mktime(0, 0, 0, date('m')+1 , $cursoEsp->getDiaPago(), date('Y')));
                }
                 * 
                 */
            }elseif($cursoEsp->getTipoPago()==3){
               //Dia en que ingresa el alumo
               $FechaDePago=date('Y-m-d');
            }
            return $FechaDePago;
        }
}
