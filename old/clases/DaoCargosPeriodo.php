<?php
require_once 'modelos/base.php';
require_once 'modelos/CargosPeriodo.php';
require_once 'DaoPeriodosAlumnoCiclo.php';
require_once 'DaoAlumnos.php';
require_once 'DaoAccionesCargoPeriodo.php';

class DaoCargosPeriodo extends base{
    
        public $table="CargosPeriodo";
    
	public function add(CargosPeriodo $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_per_ciclo, DateCreated , FechaDePago, Concepto, Activo,Monto) VALUES (%s, %s, %s, %s,%s,%s)",
            $this->GetSQLValueString($x->getId_per_ciclo(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getFechaDePago(), "date"), 
            $this->GetSQLValueString($x->getConcepto(), "text"),
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getMonto(), "double"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(CargosPeriodo $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Id_per_ciclo=%s, DateCreated=%s , FechaDePago=%s, Concepto=%s, Activo=%s, Monto=%s WHERE Id_cargo_perido=%s",
            $this->GetSQLValueString($x->getId_per_ciclo(), "int"), 
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getFechaDePago(), "date"), 
            $this->GetSQLValueString($x->getConcepto(), "text"),
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getMonto(), "double"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	public function create_object($row){
            $x = new CargosPeriodo();
            $x->setId($row['Id_cargo_perido']);
            $x->setDateCreated($row['DateCreated']);
            $x->setId_per_ciclo($row['Id_per_ciclo']);
            $x->setFechaDePago($row['FechaDePago']);
            $x->setConcepto($row['Concepto']);
            $x->setActivo($row['Activo']);
            $x->setMonto($row['Monto']);
            return $x;
	}


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_cargo_perido= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
       public function delete($Id){
            $query = sprintf("UPDATE ".$this->table." SET Activo=0 WHERE Id_cargo_perido =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function getCargosPeriodo($Id_per_ciclo){
            $query="SELECT * FROM ".$this->table." WHERE Id_per_ciclo=".$Id_per_ciclo." AND Activo=1 ORDER BY Id_cargo_perido ASC";
            return $this->advanced_query($query);
        }
        
        public function getPrimerCargoPeriodo($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_per_ciclo= ".$Id." ORDER BY Id_cargo_perido ASC LIMIT 1";
	    $Result1=$this->_cnn->query($query);
	    if(!$Result1){
            throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
   
       public function getPagoInscripcionPeriodo($Id_per_ciclo){
	    $query= "SELECT * FROM ".$this->table." WHERE Id_per_ciclo=".$Id_per_ciclo." AND Concepto='Inscripción' AND Activo=1";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function verificarPagoInscripcion($Id_ofe_alum){  
          //Obtener el primer ciclo de la oferta para verificar si ya pago la inscripcion
          $existe=0;
          $DaoPeriodosAlumnoCiclo= new DaoPeriodosAlumnoCiclo();
          $primerPeriodo=$DaoPeriodosAlumnoCiclo->getPrimerPeriodoOferta($Id_ofe_alum);
          $cargo=$this->getPagoInscripcionPeriodo($primerPeriodo->getId());
          if($cargo->getId()>0){
              
              //En acciones cargo periodo se incluyen los descuentos que se aplican al cargo 
              //ya sea por beca aplicada directamente o por un paquete de beca
              $totalDescuento=0;
              $DaoAccionesCargoPeriodo= new DaoAccionesCargoPeriodo();
              foreach($DaoAccionesCargoPeriodo->getAccionesCargoPeriodo($cargo->getId()) as $accion){
                  $totalDescuento+=$accion->getMonto();
              }
              $DaoAlumnos= new DaoAlumnos();
              $total=$DaoAlumnos->getCantidadPagadaPorOfertaAlumno($Id_ofe_alum);
              $mensualidad=$cargo->getMonto()-$totalDescuento;
              if($total>=$mensualidad){
                  $existe=1;
              }
               
          }else{
              //Se puede dar el caso que no tenga inscripcion ya que esta puede ser cargada solo una vez cada año
              //Y para este caso solo verificamos si ya
              $NumCargos=$this->getCargosPeriodo($primerPeriodo->getId());
              if($NumCargos>0){
                 $existe=1; 
              }
          }

          return $existe;
        }
        
        public function perteneceCargoAOfertaAlumno($Id_ofe_alum,$Id_cargo_per){
            /*
	    $query= "SELECT * FROM Pagos_ciclo 
                    JOIN ciclos_alum_ulm ON Pagos_ciclo.Id_ciclo_alum=ciclos_alum_ulm.Id_ciclo_alum
                    JOIN ofertas_alumno ON ciclos_alum_ulm.Id_ofe_alum=ofertas_alumno.Id_ofe_alum
                    WHERE ofertas_alumno.Id_ofe_alum=".$Id_ofe_alum." 
                          AND Pagos_ciclo.Id_pago_ciclo=".$Id_pago_ciclo."
                          AND Pagos_ciclo.DeadCargo IS NULL 
                          AND Pagos_ciclo.DeadCargoUsu IS NULL";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
             * 
             */
	}

}
