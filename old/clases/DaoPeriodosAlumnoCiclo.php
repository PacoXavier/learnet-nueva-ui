<?php
require_once 'modelos/base.php';
require_once 'modelos/PeriodosAlumnoCiclo.php';


class DaoPeriodosAlumnoCiclo extends base{
    
        public $table="PeriodosAlumnoCiclo";
    
	public function add(PeriodosAlumnoCiclo $x){
	    $query=sprintf("INSERT INTO ".$this->table." (DateCreated, Activo, Id_grado, Id_ofe_alum) VALUES (%s, %s, %s, %s)",
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getActivo(), "int"), 
            $this->GetSQLValueString($x->getId_grado(), "int"),
            $this->GetSQLValueString($x->getId_ofe_alum(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(PeriodosAlumnoCiclo $x){
	    $query=sprintf("UPDATE ".$this->table." SET  DateCreated=%s, Activo=%s, Id_grado=%s, Id_ofe_alum=%s WHERE Id_per_ciclo=%s",
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getActivo(), "int"), 
            $this->GetSQLValueString($x->getId_grado(), "int"),
            $this->GetSQLValueString($x->getId_ofe_alum(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	public function create_object($row){
            $x = new PeriodosAlumnoCiclo();
            $x->setId($row['Id_per_ciclo']);
            $x->setDateCreated($row['DateCreated']);
            $x->setActivo($row['Activo']);
            $x->setId_grado($row['Id_grado']);
            $x->setId_ofe_alum($row['Id_ofe_alum']);
            return $x;
	}
        


	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_per_ciclo= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
       public function delete($Id){
            $query = sprintf("UPDATE ".$this->table." SET Activo=0 WHERE Id_per_ciclo =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function getPeriodosAlumnoCiclo($Id_ciclo_alum){
            $query="SELECT * FROM ".$this->table." WHERE Id_ciclo_alum=".$Id_ciclo_alum." AND Activo=1 ORDER Id_per_ciclo ASC";
            return $this->advanced_query($query);
        }
        
        public function getPrimerPeriodoOferta($Id_ofe_alum){
	    $query="SELECT * FROM ".$this->table." WHERE Id_ofe_alum= ".$Id_ofe_alum." ORDER BY Id_per_ciclo ASC LIMIT 1";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
        public function getUltimoPeriodoOferta($Id_ofe_alum){
	    $query="SELECT * FROM ".$this->table." WHERE Id_ofe_alum= ".$Id_ofe_alum." ORDER BY Id_per_ciclo DESC LIMIT 1";
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

}
