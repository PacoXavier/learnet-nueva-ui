<?php
require_once 'modelos/base.php';
require_once 'modelos/PrestamoActivos.php';

class DaoPrestamoActivos extends base{
	public function add(PrestamoActivos $x){
	    $query=sprintf("INSERT INTO Historial_activo_prestamo (Usu_entrega, Usu_recibe, Estado_entrega, Estado_recibe,Comentarios, Fecha_entrega, Fecha_devolucion,Id_activo, Fecha_entregado, Tipo_usu ) VALUES (%s, %s,%s, %s,%s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getUsu_entrega(), "int"), 
            $this->GetSQLValueString($x->getUsu_recibe(), "int"), 
            $this->GetSQLValueString($x->getEstado_entrega(), "text"), 
            $this->GetSQLValueString($x->getEstado_recibe(), "text"), 
            $this->GetSQLValueString($x->getComentarios(), "text"), 
            $this->GetSQLValueString($x->getFecha_entrega(), "date"), 
            $this->GetSQLValueString($x->getFecha_devolucion(), "date"),
            $this->GetSQLValueString($x->getId_activo(), "int"),
            $this->GetSQLValueString($x->getFecha_entregado(), "date"),
            $this->GetSQLValueString($x->getTipo_usu(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(PrestamoActivos $x){
	    $query=sprintf("UPDATE Historial_activo_prestamo SET  Usu_entrega=%s, Usu_recibe=%s, Estado_entrega=%s, Estado_recibe=%s, Comentarios=%s, Fecha_entrega=%s, Fecha_devolucion=%s, Id_activo=%s, Fecha_entregado=%s, Tipo_usu=%s WHERE Id_prestamo=%s",
            $this->GetSQLValueString($x->getUsu_entrega(), "int"), 
            $this->GetSQLValueString($x->getUsu_recibe(), "int"), 
            $this->GetSQLValueString($x->getEstado_entrega(), "text"), 
            $this->GetSQLValueString($x->getEstado_recibe(), "text"), 
            $this->GetSQLValueString($x->getComentarios(), "text"), 
            $this->GetSQLValueString($x->getFecha_entrega(), "date"), 
            $this->GetSQLValueString($x->getFecha_devolucion(), "date"),
            $this->GetSQLValueString($x->getId_activo(), "int"),
            $this->GetSQLValueString($x->getFecha_entregado(), "date"),
            $this->GetSQLValueString($x->getTipo_usu(), "text"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM Historial_activo_prestamo  WHERE Id_prestamo =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM Historial_activo_prestamo WHERE Id_prestamo= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	/*
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM Historial_activo_prestamo WHERE Activo_aula=1 AND Id_plantel=".$this->Id_plantel;
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}
         
         */

	
	public function create_object($row){
            $x = new PrestamoActivos();
            $x->setId($row['Id_prestamo']);
            $x->setUsu_entrega($row['Usu_entrega']);
            $x->setUsu_recibe($row['Usu_recibe']);
            $x->setEstado_entrega($row['Estado_entrega']);
            $x->setEstado_recibe($row['Estado_recibe']);
            $x->setComentarios($row['Comentarios']);
            $x->setFecha_entrega($row['Fecha_entrega']);
            $x->setFecha_devolucion($row['Fecha_devolucion']);
            $x->setId_activo($row['Id_activo']);
            $x->setFecha_entregado($row['Fecha_entregado']);
            $x->setTipo_usu($row['Tipo_usu']);
            return $x;
	}
        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


}
