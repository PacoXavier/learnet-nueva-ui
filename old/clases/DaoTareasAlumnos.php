<?php
require_once 'modelos/base.php';
require_once 'modelos/TareasAlumnos.php';


class DaoTareasAlumnos extends base{
	public function add(TareasAlumnos $x){
	    $query=sprintf("INSERT INTO TareasAlumno (Id_tarea, Id_alum, FechaCaptura,Comentarios,FechaEntregada,Calificacion) VALUES (%s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getId_tarea(), "int"), 
            $this->GetSQLValueString($x->getId_alum(), "int"),
            $this->GetSQLValueString($x->getFechaCaptura(), "date"),
            $this->GetSQLValueString($x->getComentarios(), "text"),
            $this->GetSQLValueString($x->getFechaEntregada(), "date"),
            $this->GetSQLValueString($x->getCalificacion(), "double"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}
        

    
	public function update(TareasAlumnos $x){
	    $query=sprintf("UPDATE TareasAlumno SET  Id_tarea=%s, Id_alum=%s, FechaCaptura=%s,Comentarios=%s,FechaEntregada=%s,Calificacion=%s WHERE Id_ta=%s",
            $this->GetSQLValueString($x->getId_tarea(), "int"), 
            $this->GetSQLValueString($x->getId_alum(), "int"),
            $this->GetSQLValueString($x->getFechaCaptura(), "date"),
            $this->GetSQLValueString($x->getComentarios(), "text"),
            $this->GetSQLValueString($x->getFechaEntregada(), "date"),
            $this->GetSQLValueString($x->getCalificacion(), "double"),
            $this->GetSQLValueString($x->getId(), "text"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM TareasAlumno WHERE Id_ta =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM TareasAlumno WHERE Id_ta= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
        }
	

	public function create_object($row){
            $x = new TareasAlumnos();
            $x->setId($row['Id_ta']);
            $x->setId_tarea($row['Id_tarea']);
            $x->setId_alum($row['Id_alum']);
            $x->setFechaCaptura($row['FechaCaptura']);
            $x->setComentarios($row['Comentarios']);
            $x->setFechaEntregada($row['FechaEntregada']);
            $x->setCalificacion($row['Calificacion']);
            return $x;
	}
        

        
	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function getTareaAlumnoByActividad($Id_alum,$Id_act){
	    $query="SELECT * FROM TareasAlumno WHERE Id_alum= ".$Id_alum." AND Id_tarea=".$Id_act;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
        }
        
        public function getTareasAlumnoGrupo($Id_alum,$Id_grupo){
            $query="SELECT * FROM TareasAlumno 
                    JOIN TareasDocentes ON TareasAlumnos.Id_tarea=TareasDocentes.Id_tarea
                    WHERE Id_alum= ".$Id_alum." AND Id_grupo=".$Id_grupo;
            return $this->advanced_query($query);
        }
        
        public function getTareaAlumno($Id_alumno,$Id_tarea){
	    $query="SELECT * FROM TareasAlumno WHERE Id_alum= ".$Id_alumno." AND Id_tarea=".$Id_tarea;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
        }
        
       public function deleteTareaByAlumno($Id_alumno,$Id_tarea){
            $query = sprintf("DELETE FROM TareasAlumno WHERE Id_alum=".$Id_alumno." AND Id_tarea =".$Id_tarea); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return true;
            }
	}
}


