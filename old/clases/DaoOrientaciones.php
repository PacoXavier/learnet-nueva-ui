<?php
require_once 'modelos/base.php';
require_once 'modelos/Orientaciones.php';

class DaoOrientaciones extends base{
	public function add(Orientaciones $x){
	    $query=sprintf("INSERT INTO orientaciones_ulm (Id_esp, Nombre_ori, Activo_ori) VALUES (%s, %s,%s)",
            $this->GetSQLValueString($x->getId_esp(), "int"), 
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getActivo(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Orientaciones $x){
	    $query=sprintf("UPDATE orientaciones_ulm SET Id_esp=%s, Nombre_ori=%s, Activo_ori=%s WHERE Id_ori=%s",
            $this->GetSQLValueString($x->getId_esp(), "int"), 
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("UPDATE orientaciones_ulm SET Activo_ori=0 WHERE Id_ori =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function show($Id){
	    $query="SELECT * FROM orientaciones_ulm WHERE Id_ori= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM orientaciones_ulm WHERE Activo_ori=1 ORDER BY Nombre_ori ASC";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new Orientaciones();
            $x->setId($row['Id_ori']);
            $x->setId_esp($row['Id_esp']);
            $x->setNombre($row['Nombre_ori']);
            $x->setActivo($row['Activo_ori']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        
        public function getOrientacionesEspecialidad($Id_esp) {
            $query="SELECT * FROM orientaciones_ulm WHERE Id_esp=".$Id_esp." AND Activo_ori=1 ORDER BY Nombre_ori ASC";
            return $this->advanced_query($query);
        }
}
