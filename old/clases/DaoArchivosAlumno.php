<?php
require_once 'modelos/base.php';
require_once 'modelos/ArchivosAlumno.php';

class DaoArchivosAlumno extends base{

	public function add(ArchivosAlumno $x){
	    $query=sprintf("INSERT INTO archivos_alum_ulm (Id_file, Id_alum, Id_ofe, Id_file_copy, Id_usu, fecha_recibido, Id_ofe_alum, Id_file_original) VALUES (%s, %s,%s, %s,%s, %s,%s, %s)",
            $this->GetSQLValueString($x->getId_file(), "int"), 
            $this->GetSQLValueString($x->getId_alum(), "int"), 
            $this->GetSQLValueString($x->getId_ofe(), "int"), 
            $this->GetSQLValueString($x->getId_file_copy(), "int"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getFecha_recibido(), "date"),
            $this->GetSQLValueString($x->getId_ofe_alum(), "int"),
            $this->GetSQLValueString($x->getId_file_original(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(ArchivosAlumno $x){
	    $query=sprintf("UPDATE archivos_alum_ulm SET  Id_file=%s, Id_alum=%s, Id_ofe=%s, Id_file_copy=%s, Id_usu=%s, fecha_recibido=%s, Id_ofe_alum=%s, Id_file_original=%s WHERE Id_file_alum=%s",
            $this->GetSQLValueString($x->getId_file(), "int"), 
            $this->GetSQLValueString($x->getId_alum(), "int"), 
            $this->GetSQLValueString($x->getId_ofe(), "int"), 
            $this->GetSQLValueString($x->getId_file_copy(), "int"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getFecha_recibido(), "date"),
            $this->GetSQLValueString($x->getId_ofe_alum(), "int"),
            $this->GetSQLValueString($x->getId_file_original(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM archivos_alum_ulm  WHERE Id_file_alum =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
        public function deleteFilesByOferta($Id_ofe,$Id_alum){
            $query = sprintf("DELETE FROM archivos_alum_ulm WHERE Id_ofe=".$Id_ofe." AND Id_alum=".$Id_alum); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function deleteFilesByOfertaAlumno($Id_ofe_alum,$Id_alum){
            $query = sprintf("DELETE FROM archivos_alum_ulm WHERE Id_alum=".$Id_alum." AND Id_ofe_alum=".$Id_ofe_alum); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        
	public function show($Id){
	    $query="SELECT * FROM archivos_alum_ulm WHERE Id_file_alum= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function showAll(){
            $resp=array();
            $query="SELECT * FROM archivos_alum_ulm";
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                 throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                 $row_consulta = $consulta->fetch_assoc();
            }
            $totalRows_consulta= $consulta->num_rows;
            if($totalRows_consulta>0){
               do{
                   array_push($resp, $this->create_object($row_consulta));
               }while($row_consulta = $consulta->fetch_assoc());  
            }
            return $resp;
	}

	
	public function create_object($row){
            $x = new ArchivosAlumno();
            $x->setId($row['Id_file_alum']);
            $x->setId_file($row['Id_file']);
            $x->setId_alum($row['Id_alum']);
            $x->setId_file_copy($row['Id_file_copy']);
            $x->setId_file_original($row['Id_file_original']);
            $x->setId_ofe($row['Id_ofe']);
            $x->setId_ofe_alum($row['Id_ofe_alum']);
            $x->setId_usu($row['Id_usu']);
            $x->setFecha_recibido($row['fecha_recibido']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        /*
        public function getArchivosAlumnoByOferta($Id_ofe,$Id_alum){
                $resp=array(); 
                $query="SELECT archivos_ofertas_ulm.Id_file,Nombre_file,Id_ofe ,file_alum,fecha_recibido, Id_usu,Id_file_copy,Id_file_original 
                  FROM archivos_ofertas_ulm 
                 JOIN archivos_ulm ON archivos_ulm.Id_file=archivos_ofertas_ulm.Id_file
                 LEFT JOIN (SELECT Id_file as file_alum, fecha_recibido, Id_usu, Id_file_copy ,Id_file_original
                            FROM archivos_alum_ulm 
                            WHERE Id_ofe=" . $Id_ofe." AND Id_alum = ".$Id_alum."
                            ) AS files ON archivos_ofertas_ulm.Id_file = files.file_alum WHERE Id_ofe = ".$Id_ofe;    
                $consulta=base::advanced_query($query);
                $row_consulta = $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                   do{
                       array_push($resp, $row_consulta);

                   }while($row_consulta = $consulta->fetch_assoc());
                }
                return $resp;
        }
         * 
         */
       
        public function getArchivosAlumnoByOfertaAlumno($Id_ofe_alum){
                $resp=array(); 
                $query = "SELECT ofertas_alumno.Id_ofe_alum,Id_oferta,
                               ofertas_ulm.Nombre_oferta,
                               archivos_ulm.Id_file,archivos_ulm.Nombre_file,
                               archivos_alum_ulm.* 
                        FROM ofertas_alumno
                        JOIN ofertas_ulm ON ofertas_alumno.Id_ofe=ofertas_ulm.Id_oferta
                        JOIN archivos_ofertas_ulm ON ofertas_ulm.Id_oferta=archivos_ofertas_ulm.Id_ofe
                        JOIN archivos_ulm ON archivos_ofertas_ulm.Id_file=archivos_ulm.Id_file
                        LEFT JOIN (
                                   SELECT  Id_file_alum,Id_usu,fecha_recibido,Id_file AS ArchivoEntregado ,Id_file_original,Id_file_copy,Id_ofe
                                          FROM archivos_alum_ulm 
                                   WHERE Id_ofe_alum=".$Id_ofe_alum."
                                  ) AS archivos_alum_ulm ON archivos_ulm.Id_file=archivos_alum_ulm.ArchivoEntregado AND ofertas_alumno.Id_ofe=archivos_alum_ulm.Id_ofe
                         WHERE ofertas_alumno.Id_ofe_alum=".$Id_ofe_alum."
                               AND ofertas_alumno.Activo_oferta=1
                               ORDER BY archivos_ulm.Nombre_file ASC";
                $consulta=base::advanced_query($query);
                $row_consulta = $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                
                if($totalRows_consulta>0){
                   do{
                       array_push($resp, $row_consulta);

                   }while($row_consulta = $consulta->fetch_assoc());
                }
                return $resp;
        }
}
