<?php
require_once 'modelos/base.php';
require_once 'modelos/Prerrequisitos.php';

class DaoPrerrequisitos extends base{
    
        public $table="prerequisitos_ulm";
    
	public function add(Prerrequisitos $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Id_mat_esp, Id_mat_esp_pre) VALUES (%s, %s)",
            $this->GetSQLValueString($x->getId_mat_esp(), "int"), 
            $this->GetSQLValueString($x->getId_mat_esp_pre(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(Prerrequisitos $x){
	    $query=sprintf("UPDATE ".$this->table." SET Id_mat_esp=%s, Id_mat_esp_pre=%s WHERE Id_mat_pre=%s",
            $this->GetSQLValueString($x->getId_mat_esp(), "int"), 
            $this->GetSQLValueString($x->getId_mat_esp_pre(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	public function create_object($row){
            $x = new Prerrequisitos();
            $x->setId($row['Id_mat_pre']);
            $x->setId_mat_esp($row['Id_mat_esp']);
            $x->setId_mat_esp_pre($row['Id_mat_esp_pre']);
            return $x;
	}

        

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        public function show($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_mat_pre= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
        
       public function delete($Id){
            $query = sprintf("DELETE FROM  ".$this->table." WHERE Id_mat_pre =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function getPrerrequisitoByIdMatEsp($Id_mat_esp){
            $query="SELECT * FROM ".$this->table." WHERE Id_mat_esp=".$Id_mat_esp." ORDER BY Id_mat_pre ASC";
            return $this->advanced_query($query);
        }
        
        public function deleteByIdMatEsp($Id_mat_esp){
            $query = sprintf("DELETE FROM  ".$this->table." WHERE Id_mat_esp =".$Id_mat_esp); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
        public function getLatsPrerrequisito($Id){
	    $query="SELECT * FROM ".$this->table." WHERE Id_mat_esp= ".$Id." ORDER BY Id_mat_pre DESC";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

}
