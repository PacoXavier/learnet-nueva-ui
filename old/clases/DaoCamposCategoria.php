<?php
require_once 'modelos/base.php';
require_once 'modelos/CamposCategoria.php';

class DaoCamposCategoria extends base{
    
        public $table="CamposCategoria";
    
	public function add(CamposCategoria $x){
	    $query=sprintf("INSERT INTO ".$this->table." (Nombre, Valor, Id_cat,Tipo_camp,Activo) VALUES (%s, %s,%s,%s,%s)",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getValor(), "date"), 
            $this->GetSQLValueString($x->getId_cat(), "int"), 
            $this->GetSQLValueString($x->getTipo_camp(), "int"), 
            $this->GetSQLValueString($x->getActivo(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(CamposCategoria $x){
	    $query=sprintf("UPDATE ".$this->table." SET  Nombre=%s, Valor=%s, Id_cat=%s,Tipo_camp=%s,Activo=%s WHERE Id_cam=%s",
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getValor(), "date"), 
            $this->GetSQLValueString($x->getId_cat(), "int"), 
            $this->GetSQLValueString($x->getTipo_camp(), "int"), 
            $this->GetSQLValueString($x->getActivo(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}

	
	public function delete($Id){
            $query = sprintf("UPDATE ".$this->table." SET Activo=0 WHERE Id_cam =%s",
            $this->GetSQLValueString($Id, "int")); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
        
	public function show($Id){
	    $query=sprintf("SELECT * FROM ".$this->table." WHERE Id_cam=%s",
            $this->GetSQLValueString($Id, "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}

	
	public function create_object($row){
            $x = new CamposCategoria();
            $x->setId($row['Id_cam']);
            $x->setNombre($row['Nombre']);
            $x->setValor($row['Valor']);
            $x->setId_cat($row['Id_cat']);
            $x->setTipo_camp($row['Tipo_camp']);
            $x->setActivo($row['Activo']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                   do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}
        
        
        public function getCamposCategoria($Id_cat){
            $query="SELECT * FROM ".$this->table." WHERE Activo=1 AND Id_cat=".$Id_cat." ORDER BY Id_cam ASC";
            return $this->advanced_query($query);
        }

}
