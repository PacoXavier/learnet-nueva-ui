<?php
require_once 'modelos/base.php';
require_once 'modelos/DocumentosDrive.php';

class DaoDocumentosDrive extends base{
    
	public function add(DocumentosDrive $x){
	    $query=sprintf("INSERT INTO DocumentosDrive (DateCreated, Id_usu, Nombre, TipoDoc, TipoUsuario, Id_plantel, UID_drive, URL, Id_parent,Orden) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getTipoDoc(), "text"), 
            $this->GetSQLValueString($x->getTipoUsuario(), "text"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"), 
            $this->GetSQLValueString($x->getUID_drive(), "text"), 
            $this->GetSQLValueString($x->getURL(), "text"), 
            $this->GetSQLValueString($x->getId_parent(), "int"), 
            $this->GetSQLValueString($x->getOrden(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                        throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                   return $this->_cnn->insert_id; 
            }
	}

    
	public function update(DocumentosDrive $x){
	    $query=sprintf("UPDATE DocumentosDrive SET  DateCreated=%s, Id_usu=%s, Nombre=%s, TipoDoc=%s, TipoUsuario=%s, Id_plantel=%s, UID_drive=%s, URL=%s, Id_parent=%s,Orden=%s WHERE Id_doc=%s",
            $this->GetSQLValueString($x->getDateCreated(), "date"), 
            $this->GetSQLValueString($x->getId_usu(), "int"), 
            $this->GetSQLValueString($x->getNombre(), "text"), 
            $this->GetSQLValueString($x->getTipoDoc(), "text"), 
            $this->GetSQLValueString($x->getTipoUsuario(), "text"), 
            $this->GetSQLValueString($x->getId_plantel(), "int"), 
            $this->GetSQLValueString($x->getUID_drive(), "text"), 
            $this->GetSQLValueString($x->getURL(), "text"), 
            $this->GetSQLValueString($x->getId_parent(), "int"), 
            $this->GetSQLValueString($x->getOrden(), "int"),
            $this->GetSQLValueString($x->getId(), "int"));
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                    throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                  return $x->getId();  
            }
	}
	
	
	
	public function delete($Id){
            $query = sprintf("DELETE FROM DocumentosDrive WHERE Id_doc =".$Id); 
            $Result1=$this->_cnn->query($query);
            if(!$Result1) {
                  throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                    return true;
            }
	}
	
	public function getCarpetaPrincipal(){
	    $query="SELECT * FROM DocumentosDrive WHERE TipoUsuario IS NULL AND Id_plantel=".$this->Id_plantel." ORDER BY Id_doc ASC LIMIT 1";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
        }
        
	public function show($Id){
	    $query="SELECT * FROM DocumentosDrive WHERE Id_doc= ".$Id;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
        }
        
        public function getPrincipal(){
	    $query="SELECT * FROM DocumentosDrive WHERE Id_parent=0";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
        }
            
        public function showByKey($UID_drive){
	    $query="SELECT * FROM DocumentosDrive WHERE UID_drive= '".$UID_drive."'";
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
	}
            
        public function getCarpetaPorNombre($Nombre){
	    $query="SELECT * FROM DocumentosDrive WHERE Nombre='".$Nombre."' AND Id_plantel=".$this->Id_plantel;
                $Result1=$this->_cnn->query($query);
                if(!$Result1){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                return $this->create_object($Result1->fetch_assoc());
            }
        }
	
	public function create_object($row){
            $x = new DocumentosDrive();
            $x->setId($row['Id_doc']);
            $x->setDateCreated($row['DateCreated']);
            $x->setId_usu($row['Id_usu']);
            $x->setNombre($row['Nombre']);
            $x->setTipoDoc($row['TipoDoc']);
            $x->setTipoUsuario($row['TipoUsuario']);
            $x->setId_plantel($row['Id_plantel']);
            $x->setUID_drive($row['UID_drive']);
            $x->setURL($row['URL']);
            $x->setId_parent($row['Id_parent']);
            $x->setOrden($row['Orden']);
            return $x;
	}

	public function advanced_query($query){
            $resp=array();
            $consulta=$this->_cnn->query($query);
            if(!$consulta){
                throw new Exception("Error al insertar: (" . $this->_cnn->errno . ") " . $this->_cnn->error);
            }else{
                $row_consulta= $consulta->fetch_assoc();
                $totalRows_consulta= $consulta->num_rows;
                if($totalRows_consulta>0){
                       do{
                          array_push($resp, $this->create_object($row_consulta));
                   }while($row_consulta= $consulta->fetch_assoc());  
                }
            }
            return $resp;
	}


       /*
       public function getDocumentos($Id_parent=null) {
           $Id=0;
           if($Id_parent!=null){
               $Id=$Id_parent;
           }
            $query= "SELECT * FROM DocumentosDrive WHERE Id_plantel=" . $this->Id_plantel." AND Id_parent=".$Id;
            return $this->advanced_query($query);
       }
        * 
        */
       
       public function getDocumentos($Id_parent=null) {
           //$Id=0;
           $Id='(SELECT Id_doc FROM DocumentosDrive WHERE Id_parent=0 AND Id_plantel='.$this->Id_plantel.') ORDER BY Id_doc ASC';
           if($Id_parent!=null){
               $Id=$Id_parent;
           }
            $query= "SELECT * FROM DocumentosDrive WHERE Id_plantel=" . $this->Id_plantel." AND Id_parent=".$Id;
            return $this->advanced_query($query);
       }
       
       public function getDocumentosPlantel() {
            $query= "SELECT * FROM DocumentosDrive WHERE Id_plantel=" . $this->Id_plantel;
            return $this->advanced_query($query);
       }
       
        public function getRutaActual($Id_parent=null) {
           if($Id_parent!=null){
               $Id=$Id_parent;
           }else{
               $principal=$this->getPrincipal();
               if($principal->getId()>0){
                  $Id_parent=$principal->getId();
               }    
           }
           $resp=array();
           if($Id_parent>0){
              $padre=$this->show($Id_parent);
              array_unshift($resp, $padre);
              $Id_parent=$padre->getId_parent();
              do{
                  $padre=$this->show($Id_parent);
                  if($padre->getId()>0){
                      array_unshift($resp, $padre);
                      $Id_parent=$padre->getId_parent();
                  }else{
                      $Id_parent=0;
                  }
              }while($Id_parent>0);
           }
           return $resp;
       }

}
