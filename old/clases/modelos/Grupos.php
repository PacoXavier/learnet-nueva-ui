<?php

class Grupos {

    public $Id;
    public $Clave;
    public $Turno;
    public $Activo_grupo;
    public $Id_ciclo;
    public $Id_mat;
    public $Id_plantel;
    public $Capacidad;
    public $Id_mat_esp;
    public $Id_ori;

    function __construct() {
        $this->Id = "";
        $this->Clave = "";
        $this->Turno = "";
        $this->Activo_grupo = "";
        $this->Id_ciclo = "";
        $this->Id_mat = "";
        $this->Id_plantel = "";
        $this->Capacidad = "";
        $this->Id_mat_esp = "";
        $this->Id_ori = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getClave() {
        return $this->Clave;
    }

    public function getTurno() {
        return $this->Turno;
    }

    public function getActivo_grupo() {
        return $this->Activo_grupo;
    }

    public function getId_ciclo() {
        return $this->Id_ciclo;
    }

    public function getId_mat() {
        return $this->Id_mat;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function getCapacidad() {
        return $this->Capacidad;
    }

    public function getId_mat_esp() {
        return $this->Id_mat_esp;
    }

    public function getId_ori() {
        return $this->Id_ori;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setClave($Clave) {
        $this->Clave = $Clave;
    }

    public function setTurno($Turno) {
        $this->Turno = $Turno;
    }

    public function setActivo_grupo($Activo_grupo) {
        $this->Activo_grupo = $Activo_grupo;
    }

    public function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    public function setId_mat($Id_mat) {
        $this->Id_mat = $Id_mat;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    public function setCapacidad($Capacidad) {
        $this->Capacidad = $Capacidad;
    }

    public function setId_mat_esp($Id_mat_esp) {
        $this->Id_mat_esp = $Id_mat_esp;
    }

    public function setId_ori($Id_ori) {
        $this->Id_ori = $Id_ori;
    }




}
