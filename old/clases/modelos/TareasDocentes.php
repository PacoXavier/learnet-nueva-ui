<?php
class TareasDocentes{
   public  $Id;
   public $Titulo;
   public $Descripcion;
   public $Ponderacion;
   public $Id_grupo;
   public $FechaCaptura;
   public $Id_docen;
   public $FechaEntrega;
   public $Orden;

   function __construct() {
       $this->Id = "";
       $this->Titulo = "";
       $this->Descripcion = "";
       $this->Ponderacion = "";
       $this->Id_grupo = "";
       $this->FechaCaptura = "";
       $this->Id_docen = "";
       $this->FechaEntrega = "";
       $this->Orden = "";
   }
   
   function getId() {
       return $this->Id;
   }

   function getTitulo() {
       return $this->Titulo;
   }

   function getDescripcion() {
       return $this->Descripcion;
   }

   function getPonderacion() {
       return $this->Ponderacion;
   }

   function getId_grupo() {
       return $this->Id_grupo;
   }

   function getFechaCaptura() {
       return $this->FechaCaptura;
   }

   function getId_docen() {
       return $this->Id_docen;
   }

   function getFechaEntrega() {
       return $this->FechaEntrega;
   }

   function getOrden() {
       return $this->Orden;
   }

   function setId($Id) {
       $this->Id = $Id;
   }

   function setTitulo($Titulo) {
       $this->Titulo = $Titulo;
   }

   function setDescripcion($Descripcion) {
       $this->Descripcion = $Descripcion;
   }

   function setPonderacion($Ponderacion) {
       $this->Ponderacion = $Ponderacion;
   }

   function setId_grupo($Id_grupo) {
       $this->Id_grupo = $Id_grupo;
   }

   function setFechaCaptura($FechaCaptura) {
       $this->FechaCaptura = $FechaCaptura;
   }

   function setId_docen($Id_docen) {
       $this->Id_docen = $Id_docen;
   }

   function setFechaEntrega($FechaEntrega) {
       $this->FechaEntrega = $FechaEntrega;
   }

   function setOrden($Orden) {
       $this->Orden = $Orden;
   }



}

