<?php

class PreguntasEncuesta {
    
    public $Id;
    public $Pregunta;
    public $Activo;
    public $TipoPre;
    public $Id_en;

    function __construct() {
        $this->Id = "";
        $this->Pregunta = "";
        $this->Activo = "";
        $this->TipoPre = "";
        $this->Id_en = "";
    }

    function getId() {
        return $this->Id;
    }

    function getPregunta() {
        return $this->Pregunta;
    }

    function getActivo() {
        return $this->Activo;
    }

    function getTipoPre() {
        return $this->TipoPre;
    }

    function getId_en() {
        return $this->Id_en;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setPregunta($Pregunta) {
        $this->Pregunta = $Pregunta;
    }

    function setActivo($Activo) {
        $this->Activo = $Activo;
    }

    function setTipoPre($TipoPre) {
        $this->TipoPre = $TipoPre;
    }

    function setId_en($Id_en) {
        $this->Id_en = $Id_en;
    }


   
}
