<?php

class TareasAlumnos{
    public $Id;
    public $Id_tarea;
    public $Id_alum;
    public $FechaCaptura;
    public $Comentarios;
    public $FechaEntregada;
    public $Calificacion;
    
    function __construct() {
        $this->Id ="";
        $this->Id_tarea = "";
        $this->Id_alum = "";
        $this->FechaCaptura = "";
        $this->Comentarios = "";
        $this->FechaEntregada = "";
        $this->Calificacion="";
    }
    
    function getCalificacion() {
        return $this->Calificacion;
    }

    function setCalificacion($Calificacion) {
        $this->Calificacion = $Calificacion;
    }

        function getId() {
        return $this->Id;
    }

    function getId_tarea() {
        return $this->Id_tarea;
    }

    function getId_alum() {
        return $this->Id_alum;
    }

    function getFechaCaptura() {
        return $this->FechaCaptura;
    }

    function getComentarios() {
        return $this->Comentarios;
    }

    function getFechaEntregada() {
        return $this->FechaEntregada;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_tarea($Id_tarea) {
        $this->Id_tarea = $Id_tarea;
    }

    function setId_alum($Id_alum) {
        $this->Id_alum = $Id_alum;
    }

    function setFechaCaptura($FechaCaptura) {
        $this->FechaCaptura = $FechaCaptura;
    }

    function setComentarios($Comentarios) {
        $this->Comentarios = $Comentarios;
    }

    function setFechaEntregada($FechaEntregada) {
        $this->FechaEntregada = $FechaEntregada;
    }
}