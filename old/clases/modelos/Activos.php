<?php

class Activos {
    
    public $Id;
    public $Nombre;
    public $Modelo;
    public $Descripcion;
    public $Id_img;
    public $Valor;
    public $Codigo;
    public $Tipo_act;
    public $Fecha_adq;
    public $Disponible;
    public $Garantia_meses;
    public $Mantenimiento_frecuencia;
    public $Id_plantel;
    public $Baja_activo;
    public $Id_aula;
    public $Comentario_baja;
    public $Marca;
    public $Num_serie;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->Modelo = "";
        $this->Descripcion = "";
        $this->Id_img = "";
        $this->Valor = "";
        $this->Codigo = "";
        $this->Tipo_act = "";
        $this->Fecha_adq = "";
        $this->Disponible = "";
        $this->Garantia_meses = "";
        $this->Mantenimiento_frecuencia = "";
        $this->Id_plantel = "";
        $this->Baja_activo = "";
        $this->Id_aula = "";
        $this->Comentario_baja = "";
        $this->Marca = "";
        $this->Num_serie = "";
    }

    function getId() {
        return $this->Id;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getModelo() {
        return $this->Modelo;
    }

    function getDescripcion() {
        return $this->Descripcion;
    }

    function getId_img() {
        return $this->Id_img;
    }

    function getValor() {
        return $this->Valor;
    }

    function getCodigo() {
        return $this->Codigo;
    }

    function getTipo_act() {
        return $this->Tipo_act;
    }

    function getFecha_adq() {
        return $this->Fecha_adq;
    }

    function getDisponible() {
        return $this->Disponible;
    }

    function getGarantia_meses() {
        return $this->Garantia_meses;
    }

    function getMantenimiento_frecuencia() {
        return $this->Mantenimiento_frecuencia;
    }

    function getId_plantel() {
        return $this->Id_plantel;
    }

    function getBaja_activo() {
        return $this->Baja_activo;
    }

    function getId_aula() {
        return $this->Id_aula;
    }

    function getComentario_baja() {
        return $this->Comentario_baja;
    }

    function getMarca() {
        return $this->Marca;
    }

    function getNum_serie() {
        return $this->Num_serie;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setModelo($Modelo) {
        $this->Modelo = $Modelo;
    }

    function setDescripcion($Descripcion) {
        $this->Descripcion = $Descripcion;
    }

    function setId_img($Id_img) {
        $this->Id_img = $Id_img;
    }

    function setValor($Valor) {
        $this->Valor = $Valor;
    }

    function setCodigo($Codigo) {
        $this->Codigo = $Codigo;
    }

    function setTipo_act($Tipo_act) {
        $this->Tipo_act = $Tipo_act;
    }

    function setFecha_adq($Fecha_adq) {
        $this->Fecha_adq = $Fecha_adq;
    }

    function setDisponible($Disponible) {
        $this->Disponible = $Disponible;
    }

    function setGarantia_meses($Garantia_meses) {
        $this->Garantia_meses = $Garantia_meses;
    }

    function setMantenimiento_frecuencia($Mantenimiento_frecuencia) {
        $this->Mantenimiento_frecuencia = $Mantenimiento_frecuencia;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    function setBaja_activo($Baja_activo) {
        $this->Baja_activo = $Baja_activo;
    }

    function setId_aula($Id_aula) {
        $this->Id_aula = $Id_aula;
    }

    function setComentario_baja($Comentario_baja) {
        $this->Comentario_baja = $Comentario_baja;
    }

    function setMarca($Marca) {
        $this->Marca = $Marca;
    }

    function setNum_serie($Num_serie) {
        $this->Num_serie = $Num_serie;
    }

}
