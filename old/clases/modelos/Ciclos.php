<?php

class Ciclos {
    public $Id;
    public $Nombre_ciclo;
    public $Clave;
    public $Fecha_ini;
    public $Fecha_fin;
    public $Activo_ciclo;
    public $Id_plantel;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre_ciclo = "";
        $this->Clave = "";
        $this->Fecha_ini = "";
        $this->Fecha_fin = "";
        $this->Activo_ciclo = "";
        $this->Id_plantel = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getNombre_ciclo() {
        return $this->Nombre_ciclo;
    }

    public function getClave() {
        return $this->Clave;
    }

    public function getFecha_ini() {
        return $this->Fecha_ini;
    }

    public function getFecha_fin() {
        return $this->Fecha_fin;
    }

    public function getActivo_ciclo() {
        return $this->Activo_ciclo;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setNombre_ciclo($Nombre_ciclo) {
        $this->Nombre_ciclo = $Nombre_ciclo;
    }

    public function setClave($Clave) {
        $this->Clave = $Clave;
    }

    public function setFecha_ini($Fecha_ini) {
        $this->Fecha_ini = $Fecha_ini;
    }

    public function setFecha_fin($Fecha_fin) {
        $this->Fecha_fin = $Fecha_fin;
    }

    public function setActivo_ciclo($Activo_ciclo) {
        $this->Activo_ciclo = $Activo_ciclo;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

}
