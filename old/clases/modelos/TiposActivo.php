<?php
class TiposActivo{
    
    public $Id;
    public $DateCreated;
    public $Id_usu;
    public $Id_plantel;
    public $Nombre;
    public $Activo;
    
    function __construct() {
        $this->Id = "";
        $this->DateCreated = "";
        $this->Id_usu = "";
        $this->Id_plantel = "";
        $this->Nombre = "";
        $this->Activo = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function getId_usu() {
        return $this->Id_usu;
    }

    function getId_plantel() {
        return $this->Id_plantel;
    }

    function getNombre() {
        return $this->Nombre;
    }


    function getActivo() {
        return $this->Activo;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setActivo($Activo) {
        $this->Activo = $Activo;
    }
 
}