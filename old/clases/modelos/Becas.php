<?php

class Becas {
    
    public $Id;
    public $Nombre;
    public $Monto;
    public $Descripcion;
    public $DateCreated;
    public $Id_usu;
    public $Activo;
    public $Id_plantel;
    public $CalificacionMin;
    public $Tipo;
    public $Contrato;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->Monto = "";
        $this->Descripcion = "";
        $this->DateCreated = "";
        $this->Id_usu = "";
        $this->Activo = "";
        $this->Id_plantel = "";
        $this->CalificacionMin = "";
        $this->Tipo = "";
        $this->Contrato = "";
    }
    
    function getContrato() {
        return $this->Contrato;
    }

    function setContrato($Contrato) {
        $this->Contrato = $Contrato;
    }

        
    
    function getId() {
        return $this->Id;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getMonto() {
        return $this->Monto;
    }

    function getDescripcion() {
        return $this->Descripcion;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function getId_usu() {
        return $this->Id_usu;
    }

    function getActivo() {
        return $this->Activo;
    }

    function getId_plantel() {
        return $this->Id_plantel;
    }

    function getCalificacionMin() {
        return $this->CalificacionMin;
    }

    function getTipo() {
        return $this->Tipo;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setMonto($Monto) {
        $this->Monto = $Monto;
    }

    function setDescripcion($Descripcion) {
        $this->Descripcion = $Descripcion;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    function setActivo($Activo) {
        $this->Activo = $Activo;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    function setCalificacionMin($CalificacionMin) {
        $this->CalificacionMin = $CalificacionMin;
    }

    function setTipo($Tipo) {
        $this->Tipo = $Tipo;
    }


    
    
}
