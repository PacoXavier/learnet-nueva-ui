<?php
class CargosPeriodo{
    
    public $Id;
    public $Id_per_ciclo;
    public $DateCreated ;
    public $FechaDePago;
    public $Concepto;
    public $Activo;
    public $Monto;
    
    function __construct() {
        $this->Id = "";
        $this->Id_per_ciclo = "";
        $this->DateCreated = "";
        $this->FechaDePago = "";
        $this->Concepto = "";
        $this->Activo = "";
        $this->Monto = "";
    }
    
    function getMonto() {
        return $this->Monto;
    }

    function setMonto($Monto) {
        $this->Monto = $Monto;
    }

        function getId() {
        return $this->Id;
    }

    function getId_per_ciclo() {
        return $this->Id_per_ciclo;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function getFechaDePago() {
        return $this->FechaDePago;
    }

    function getConcepto() {
        return $this->Concepto;
    }

    function getActivo() {
        return $this->Activo;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_per_ciclo($Id_per_ciclo) {
        $this->Id_per_ciclo = $Id_per_ciclo;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    function setFechaDePago($FechaDePago) {
        $this->FechaDePago = $FechaDePago;
    }

    function setConcepto($Concepto) {
        $this->Concepto = $Concepto;
    }

    function setActivo($Activo) {
        $this->Activo = $Activo;
    }


    

}
