<?php
class EncuestasEvaluacion {
    
    public $Id;
    public $Nombre;
    public $DateCreated;
    public $Id_usu;
    public $Id_ciclo;
    public $Activo;
    public $FechaInicio;
    public $Id_plantel;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->DateCreated = "";
        $this->Id_usu = "";
        $this->Id_ciclo = "";
        $this->Activo = "";
        $this->FechaInicio = "";
        $this->Id_plantel = "";
    }

    function getId() {
        return $this->Id;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function getId_usu() {
        return $this->Id_usu;
    }

    function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function getActivo() {
        return $this->Activo;
    }

    function getFechaInicio() {
        return $this->FechaInicio;
    }

    function getId_plantel() {
        return $this->Id_plantel;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    function setActivo($Activo) {
        $this->Activo = $Activo;
    }

    function setFechaInicio($FechaInicio) {
        $this->FechaInicio = $FechaInicio;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }


}
