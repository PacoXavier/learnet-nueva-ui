<?php
class Planteles {
    public $Id;
    public $Nombre_plantel;
    public $Id_dir_plantel;
    public $Activo_plantel;
    public $Clave;
    public $Tel;
    public $DateCreated;
    public $Id_img_logo;
    public $Id_img_correo;
    public $Abreviatura;
    public $Email;
    public $Dominio;
    public $Formato_pago;
    public $Background_login;
    public $Formato_pago_evento;
    public $EtiquetaLibro;
    public $EtiquetaActivo;
    public $Formato_inscripcion;
            
    function __construct() {
        $this->Id = "";
        $this->Nombre_plantel = "";
        $this->Id_dir_plantel = "";
        $this->Activo_plantel = "";
        $this->Clave = "";
        $this->Tel = "";
        $this->DateCreated = "";
        $this->Id_img_logo = "";
        $this->Id_img_correo = "";
        $this->Abreviatura = "";
        $this->Email = "";
        $this->Dominio = "";
        $this->Formato_pago = "";
        $this->Background_login="";
        $this->Formato_pago_evento="";
        $this->EtiquetaLibro="";
        $this->EtiquetaActivo="";
        $this->Formato_inscripcion="";
    }
    
    function getFormato_inscripcion() {
        return $this->Formato_inscripcion;
    }

    function setFormato_inscripcion($Formato_inscripcion) {
        $this->Formato_inscripcion = $Formato_inscripcion;
    }

    function getEtiquetaLibro() {
        return $this->EtiquetaLibro;
    }

    function getEtiquetaActivo() {
        return $this->EtiquetaActivo;
    }

    function setEtiquetaLibro($EtiquetaLibro) {
        $this->EtiquetaLibro = $EtiquetaLibro;
    }

    function setEtiquetaActivo($EtiquetaActivo) {
        $this->EtiquetaActivo = $EtiquetaActivo;
    }

        
    function getFormato_pago_evento() {
        return $this->Formato_pago_evento;
    }

    function setFormato_pago_evento($Formato_pago_evento) {
        $this->Formato_pago_evento = $Formato_pago_evento;
    }

        
    function getBackground_login() {
        return $this->Background_login;
    }

    function setBackground_login($Background_login) {
        $this->Background_login = $Background_login;
    }

        function getFormato_pago() {
        return $this->Formato_pago;
    }

    function setFormato_pago($Formato_pago) {
        $this->Formato_pago = $Formato_pago;
    }

        
    function getDominio() {
        return $this->Dominio;
    }

    function setDominio($Dominio) {
        $this->Dominio = $Dominio;
    }

    function getId_img_logo() {
        return $this->Id_img_logo;
    }

    function getId_img_correo() {
        return $this->Id_img_correo;
    }

    function getAbreviatura() {
        return $this->Abreviatura;
    }

    function getEmail() {
        return $this->Email;
    }

    function setId_img_logo($Id_img_logo) {
        $this->Id_img_logo = $Id_img_logo;
    }

    function setId_img_correo($Id_img_correo) {
        $this->Id_img_correo = $Id_img_correo;
    }

    function setAbreviatura($Abreviatura) {
        $this->Abreviatura = $Abreviatura;
    }

    function setEmail($Email) {
        $this->Email = $Email;
    }

    public function getDateCreated() {
        return $this->DateCreated;
    }

    public function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

   public function getClave() {
        return $this->Clave;
    }

    public function getTel() {
        return $this->Tel;
    }

    public function setClave($Clave) {
        $this->Clave = $Clave;
    }

    public function setTel($Tel) {
        $this->Tel = $Tel;
    }

        
    public function getId() {
        return $this->Id;
    }

    public function getNombre_plantel() {
        return $this->Nombre_plantel;
    }

    public function getId_dir_plantel() {
        return $this->Id_dir_plantel;
    }

    public function getActivo_plantel() {
        return $this->Activo_plantel;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setNombre_plantel($Nombre_plantel) {
        $this->Nombre_plantel = $Nombre_plantel;
    }

    public function setId_dir_plantel($Id_dir_plantel) {
        $this->Id_dir_plantel = $Id_dir_plantel;
    }

    public function setActivo_plantel($Activo_plantel) {
        $this->Activo_plantel = $Activo_plantel;
    }

}
