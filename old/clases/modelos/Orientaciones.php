<?php
class Orientaciones {
    public $Id;
    public $Id_esp;
    public $Nombre;
    public $Activo;
    
    function __construct() {
        $this->Id = "";
        $this->Id_esp = "";
        $this->Nombre = "";
        $this->Activo = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getId_esp() {
        return $this->Id_esp;
    }

    public function getNombre() {
        return $this->Nombre;
    }

    public function getActivo() {
        return $this->Activo;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_esp($Id_esp) {
        $this->Id_esp = $Id_esp;
    }

    public function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    public function setActivo($Activo) {
        $this->Activo = $Activo;
    }

}

