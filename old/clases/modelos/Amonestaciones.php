<?php

class Amonestaciones {
    public $Id;
    public $Fecha_rep;
    public $Id_usu;
    public $Motivo;
    public $Id_ciclo;
    public $Id_recibe;
    public $Tipo_recibe;
    public $Id_reporta;
    public $Tipo_reporta;
    public $Id_plantel;
    public $FechaCaptura;

    function __construct() {
        $this->Id = "";
        $this->Fecha_rep = "";
        $this->Id_usu = "";
        $this->Motivo = "";
        $this->Id_ciclo = "";
        $this->Id_recibe = "";
        $this->Tipo_recibe = "";
        $this->Id_reporta = "";
        $this->Tipo_reporta = "";
        $this->Id_plantel = "";
        $this->FechaCaptura = "";
    }
    
    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function getFechaCaptura() {
        return $this->FechaCaptura;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    public function setFechaCaptura($FechaCaptura) {
        $this->FechaCaptura = $FechaCaptura;
    }

    public function getId() {
        return $this->Id;
    }

    public function getFecha_rep() {
        return $this->Fecha_rep;
    }

    public function getId_usu() {
        return $this->Id_usu;
    }

    public function getMotivo() {
        return $this->Motivo;
    }

    public function getId_ciclo() {
        return $this->Id_ciclo;
    }

    public function getId_recibe() {
        return $this->Id_recibe;
    }

    public function getTipo_recibe() {
        return $this->Tipo_recibe;
    }

    public function getId_reporta() {
        return $this->Id_reporta;
    }

    public function getTipo_reporta() {
        return $this->Tipo_reporta;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setFecha_rep($Fecha_rep) {
        $this->Fecha_rep = $Fecha_rep;
    }

    public function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    public function setMotivo($Motivo) {
        $this->Motivo = $Motivo;
    }

    public function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    public function setId_recibe($Id_recibe) {
        $this->Id_recibe = $Id_recibe;
    }

    public function setTipo_recibe($Tipo_recibe) {
        $this->Tipo_recibe = $Tipo_recibe;
    }

    public function setId_reporta($Id_reporta) {
        $this->Id_reporta = $Id_reporta;
    }

    public function setTipo_reporta($Tipo_reporta) {
        $this->Tipo_reporta = $Tipo_reporta;
    }



}
