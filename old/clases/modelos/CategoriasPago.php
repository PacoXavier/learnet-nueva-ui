<?php
class CategoriasPago{
    
    public $Id;
    public $Nombre_tipo;
    public $Descripcion;
    public $Activo;
    public $Id_plantel;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre_tipo = "";
        $this->Descripcion = "";
        $this->Activo = "";
        $this->Id_plantel = "";
    }
    
    function getId_plantel() {
        return $this->Id_plantel;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    function getId() {
        return $this->Id;
    }

    function getNombre_tipo() {
        return $this->Nombre_tipo;
    }

    function getDescripcion() {
        return $this->Descripcion;
    }

    function getActivo() {
        return $this->Activo;
    }

    function setId($id) {
        $this->Id = $id;
    }

    function setNombre_tipo($Nombre_tipo) {
        $this->Nombre_tipo = $Nombre_tipo;
    }

    function setDescripcion($Descripcion) {
        $this->Descripcion = $Descripcion;
    }

    function setActivo($Activo) {
        $this->Activo = $Activo;
    }



    
}