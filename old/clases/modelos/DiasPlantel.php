<?php
class DiasPlantel {
    
    public $Id;
    public $Id_plantel;
    public $Id_dia;
    
    function __construct() {
        $this->Id = "";
        $this->Id_plantel = "";
        $this->Id_dia = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getId_plantel() {
        return $this->Id_plantel;
    }

    function getId_dia() {
        return $this->Id_dia;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    function setId_dia($Id_dia) {
        $this->Id_dia = $Id_dia;
    }

}
