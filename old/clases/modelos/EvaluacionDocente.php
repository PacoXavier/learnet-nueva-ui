<?php
class EvaluacionDocente{
    
    public $Id;
    public $Id_doc;
    public $DateCreated;
    public $Id_usu_evaluador;
    public $Id_ciclo;
    public $Comentarios;
    public $puntosTotales;
    
    function __construct() {
        $this->Id = "";
        $this->Id_doc = "";
        $this->DateCreated = "";
        $this->Id_usu_evaluador = "";
        $this->Id_ciclo = "";
        $this->Comentarios ="";
        $this->puntosTotales ="";
    }
    
    function getPuntosTotales() {
        return $this->puntosTotales;
    }

    function setPuntosTotales($puntosTotales) {
        $this->puntosTotales = $puntosTotales;
    }

    function getId() {
        return $this->Id;
    }

    function getId_doc() {
        return $this->Id_doc;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function getId_usu_evaluador() {
        return $this->Id_usu_evaluador;
    }

    function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function getComentarios() {
        return $this->Comentarios;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_doc($Id_doc) {
        $this->Id_doc = $Id_doc;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    function setId_usu_evaluador($Id_usu_evaluador) {
        $this->Id_usu_evaluador = $Id_usu_evaluador;
    }

    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    function setComentarios($Comentarios) {
        $this->Comentarios = $Comentarios;
    }



}