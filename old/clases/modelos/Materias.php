<?php

class Materias {
    
    public $Id;
    public $Nombre;
    public $Clave_mat;
    public $Precio;
    public $Creditos;
    public $Tipo_aula;
    public $Dias_porSemana;
    public $Horas_independientes;
    public $Horas_porSemana;
    public $Horas_conDocente;
    public $Promedio_min;
    public $Duracion_examen;
    public $Max_alumExamen;
    public $Activo_mat;
    public $Id_plantel;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->Clave_mat = "";
        $this->Precio = "";
        $this->Creditos = "";
        $this->Tipo_aula = "";
        $this->Dias_porSemana = "";
        $this->Horas_independientes = "";
        $this->Horas_porSemana = "";
        $this->Horas_conDocente = "";
        $this->Promedio_min = "";
        $this->Duracion_examen = "";
        $this->Max_alumExamen = "";
        $this->Activo_mat = "";
        $this->Id_plantel = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getNombre() {
        return $this->Nombre;
    }

    public function getClave_mat() {
        return $this->Clave_mat;
    }

    public function getPrecio() {
        return $this->Precio;
    }

    public function getCreditos() {
        return $this->Creditos;
    }

    public function getTipo_aula() {
        return $this->Tipo_aula;
    }

    public function getDias_porSemana() {
        return $this->Dias_porSemana;
    }

    public function getHoras_independientes() {
        return $this->Horas_independientes;
    }

    public function getHoras_porSemana() {
        return $this->Horas_porSemana;
    }

    public function getHoras_conDocente() {
        return $this->Horas_conDocente;
    }

    public function getPromedio_min() {
        return $this->Promedio_min;
    }

    public function getDuracion_examen() {
        return $this->Duracion_examen;
    }

    public function getMax_alumExamen() {
        return $this->Max_alumExamen;
    }

    public function getActivo_mat() {
        return $this->Activo_mat;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    public function setClave_mat($Clave_mat) {
        $this->Clave_mat = $Clave_mat;
    }

    public function setPrecio($Precio) {
        $this->Precio = $Precio;
    }

    public function setCreditos($Creditos) {
        $this->Creditos = $Creditos;
    }

    public function setTipo_aula($Tipo_aula) {
        $this->Tipo_aula = $Tipo_aula;
    }

    public function setDias_porSemana($Dias_porSemana) {
        $this->Dias_porSemana = $Dias_porSemana;
    }

    public function setHoras_independientes($Horas_independientes) {
        $this->Horas_independientes = $Horas_independientes;
    }

    public function setHoras_porSemana($Horas_porSemana) {
        $this->Horas_porSemana = $Horas_porSemana;
    }

    public function setHoras_conDocente($Horas_conDocente) {
        $this->Horas_conDocente = $Horas_conDocente;
    }

    public function setPromedio_min($Promedio_min) {
        $this->Promedio_min = $Promedio_min;
    }

    public function setDuracion_examen($Duracion_examen) {
        $this->Duracion_examen = $Duracion_examen;
    }

    public function setMax_alumExamen($Max_alumExamen) {
        $this->Max_alumExamen = $Max_alumExamen;
    }

    public function setActivo_mat($Activo_mat) {
        $this->Activo_mat = $Activo_mat;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }



}