<?php
class SeguimientoAdeudoAlumno {
    
    public $Id;
    public $Id_usu;
    public $DateCreated;
    public $Comentario;
    public $TipoRel;
    public $IdRel;
    
    function __construct() {
        $this->Id = "";
        $this->Id_usu = "";
        $this->DateCreated = "";
        $this->Comentario = "";
        $this->TipoRel = "";
        $this->IdRel = "";
    }
    
    public function getTipoRel() {
        return $this->TipoRel;
    }

    public function getIdRel() {
        return $this->IdRel;
    }

    public function setTipoRel($TipoRel) {
        $this->TipoRel = $TipoRel;
    }

    public function setIdRel($IdRel) {
        $this->IdRel = $IdRel;
    }

        public function getId() {
        return $this->Id;
    }

    public function getId_usu() {
        return $this->Id_usu;
    }

    public function getDateCreated() {
        return $this->DateCreated;
    }

    public function getComentario() {
        return $this->Comentario;
    }


    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    public function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    public function setComentario($Comentario) {
        $this->Comentario = $Comentario;
    }

    
}
