<?php
class NotificacionesUsuario {
    
    public $Id;
    public $Titulo;
    public $Texto;
    public $DateCreated;
    public $DateRead;
    public $TipoRel;
    public $IdRel;
    
    function __construct() {
        $this->Id = "";
        $this->Titulo = "";
        $this->Texto = "";
        $this->DateCreated = "";
        $this->DateRead = "";
        $this->TipoRel = "";
        $this->IdRel = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getTitulo() {
        return $this->Titulo;
    }

    public function getTexto() {
        return $this->Texto;
    }

    public function getDateCreated() {
        return $this->DateCreated;
    }

    public function getDateRead() {
        return $this->DateRead;
    }

    public function getTipoRel() {
        return $this->TipoRel;
    }

    public function getIdRel() {
        return $this->IdRel;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setTitulo($Titulo) {
        $this->Titulo = $Titulo;
    }

    public function setTexto($Texto) {
        $this->Texto = $Texto;
    }

    public function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    public function setDateRead($DateRead) {
        $this->DateRead = $DateRead;
    }

    public function setTipoRel($TipoRel) {
        $this->TipoRel = $TipoRel;
    }

    public function setIdRel($IdRel) {
        $this->IdRel = $IdRel;
    }




}
