<?php
class AccionesInteresado {  
    
    public $Id;
    public $Id_int;
    public $TipoAccion;
    public $Id_ofe_alum;
    public $Asistio;
    public $CostoAccion;
    public $FechaDeposito;
    public $Pago;
    public $FechaAccion;
    public $CalificacionExamen;
    public $FechaSession;
    public $Usuario;
    public $Plantel;
    
    function __construct() {
        $this->Id = "";
        $this->Id_int ="";
        $this->TipoAccion = "";
        $this->Id_ofe_alum = "";
        $this->Asistio = "";
        $this->CostoAccion = "";
        $this->FechaDeposito = "";
        $this->Pago = "";
        $this->FechaAccion = "";
        $this->CalificacionExamen = "";
        $this->FechaSession = "";
        $this->Usuario = "";
        $this->Plantel = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getId_int() {
        return $this->Id_int;
    }

    public function getTipoAccion() {
        return $this->TipoAccion;
    }

    public function getId_ofe_alum() {
        return $this->Id_ofe_alum;
    }

    public function getAsistio() {
        return $this->Asistio;
    }

    public function getCostoAccion() {
        return $this->CostoAccion;
    }

    public function getFechaDeposito() {
        return $this->FechaDeposito;
    }

    public function getPago() {
        return $this->Pago;
    }

    public function getFechaAccion() {
        return $this->FechaAccion;
    }

    public function getCalificacionExamen() {
        return $this->CalificacionExamen;
    }

    public function getFechaSession() {
        return $this->FechaSession;
    }

    public function getUsuario() {
        return $this->Usuario;
    }

    public function getPlantel() {
        return $this->Plantel;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_int($Id_int) {
        $this->Id_int = $Id_int;
    }

    public function setTipoAccion($TipoAccion) {
        $this->TipoAccion = $TipoAccion;
    }

    public function setId_ofe_alum($Id_ofe_alum) {
        $this->Id_ofe_alum = $Id_ofe_alum;
    }

    public function setAsistio($Asistio) {
        $this->Asistio = $Asistio;
    }

    public function setCostoAccion($CostoAccion) {
        $this->CostoAccion = $CostoAccion;
    }

    public function setFechaDeposito($FechaDeposito) {
        $this->FechaDeposito = $FechaDeposito;
    }

    public function setPago($Pago) {
        $this->Pago = $Pago;
    }

    public function setFechaAccion($FechaAccion) {
        $this->FechaAccion = $FechaAccion;
    }

    public function setCalificacionExamen($CalificacionExamen) {
        $this->CalificacionExamen = $CalificacionExamen;
    }

    public function setFechaSession($FechaSession) {
        $this->FechaSession = $FechaSession;
    }

    public function setUsuario($Usuario) {
        $this->Usuario = $Usuario;
    }

    public function setPlantel($Plantel) {
        $this->Plantel = $Plantel;
    }



   
}
