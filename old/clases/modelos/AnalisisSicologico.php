<?php
class AnalisisSicologico {
    
    public $Id;
    public $Id_alum;
    public $DateCreated;
    public $Id_camp;
    public $Valor_camp;
    public $Id_usu;
    
    function __construct() {
        $this->Id = "";
        $this->Id_alum = "";
        $this->DateCreated = "";
        $this->Id_camp = "";
        $this->Valor_camp = "";
        $this->Id_usu = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getId_alum() {
        return $this->Id_alum;
    }

    public function getDateCreated() {
        return $this->DateCreated;
    }

    public function getId_camp() {
        return $this->Id_camp;
    }

    public function getValor_camp() {
        return $this->Valor_camp;
    }

    public function getId_usu() {
        return $this->Id_usu;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_alum($Id_alum) {
        $this->Id_alum = $Id_alum;
    }

    public function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    public function setId_camp($Id_camp) {
        $this->Id_camp = $Id_camp;
    }

    public function setValor_camp($Valor_camp) {
        $this->Valor_camp = $Valor_camp;
    }

    public function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }



}
