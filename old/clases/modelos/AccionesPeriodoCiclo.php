<?php
class AccionesPeriodoCiclo{
    
    public $Id;
    public $Id_periodo_ciclo;
    public $Fecha_captura;
    public $Id_usu_captura;
    public $Monto;
    public $Porcentaje;
    public $Comentarios;
    public $TipoRel;
    public $IdRel;
    
    function __construct() {
        $this->Id = "";
        $this->Id_periodo_ciclo = "";
        $this->Fecha_captura = "";
        $this->Id_usu_captura = "";
        $this->Monto = "";
        $this->Porcentaje = "";
        $this->Comentarios = "";
        $this->TipoRel = "";
        $this->IdRel = "";
    }

    function getId() {
        return $this->Id;
    }

    function getId_periodo_ciclo() {
        return $this->Id_periodo_ciclo;
    }

    function getFecha_captura() {
        return $this->Fecha_captura;
    }

    function getId_usu_captura() {
        return $this->Id_usu_captura;
    }

    function getMonto() {
        return $this->Monto;
    }

    function getPorcentaje() {
        return $this->Porcentaje;
    }

    function getComentarios() {
        return $this->Comentarios;
    }

    function getTipoRel() {
        return $this->TipoRel;
    }

    function getIdRel() {
        return $this->IdRel;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_periodo_ciclo($Id_periodo_ciclo) {
        $this->Id_periodo_ciclo = $Id_periodo_ciclo;
    }

    function setFecha_captura($Fecha_captura) {
        $this->Fecha_captura = $Fecha_captura;
    }

    function setId_usu_captura($Id_usu_captura) {
        $this->Id_usu_captura = $Id_usu_captura;
    }

    function setMonto($Monto) {
        $this->Monto = $Monto;
    }

    function setPorcentaje($Porcentaje) {
        $this->Porcentaje = $Porcentaje;
    }

    function setComentarios($Comentarios) {
        $this->Comentarios = $Comentarios;
    }

    function setTipoRel($TipoRel) {
        $this->TipoRel = $TipoRel;
    }

    function setIdRel($IdRel) {
        $this->IdRel = $IdRel;
    }


    
}
