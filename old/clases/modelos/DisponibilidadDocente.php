<?php
class DisponibilidadDocente {
    
    public $Id;
    public $Id_hora;
    public $Lunes;
    public $Martes;
    public $Miercoles;
    public $Jueves;
    public $Viernes;
    public $Sabado;
    public $Domingo;
    public $Id_docente;
    public $Id_ciclo;
    
    function __construct() {
        $this->Id = "";
        $this->Id_hora = "";
        $this->Lunes = "";
        $this->Martes = "";
        $this->Miercoles = "";
        $this->Jueves = "";
        $this->Viernes = "";
        $this->Sabado = "";
        $this->Domingo = "";
        $this->Id_docente = "";
        $this->Id_ciclo = "";
    }

    function getDomingo() {
        return $this->Domingo;
    }

    function setDomingo($Domingo) {
        $this->Domingo = $Domingo;
    }

        function getId() {
        return $this->Id;
    }

    function getId_hora() {
        return $this->Id_hora;
    }

    function getLunes() {
        return $this->Lunes;
    }

    function getMartes() {
        return $this->Martes;
    }

    function getMiercoles() {
        return $this->Miercoles;
    }

    function getJueves() {
        return $this->Jueves;
    }

    function getViernes() {
        return $this->Viernes;
    }

    function getSabado() {
        return $this->Sabado;
    }

    function getId_docente() {
        return $this->Id_docente;
    }

    function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_hora($Id_hora) {
        $this->Id_hora = $Id_hora;
    }

    function setLunes($Lunes) {
        $this->Lunes = $Lunes;
    }

    function setMartes($Martes) {
        $this->Martes = $Martes;
    }

    function setMiercoles($Miercoles) {
        $this->Miercoles = $Miercoles;
    }

    function setJueves($Jueves) {
        $this->Jueves = $Jueves;
    }

    function setViernes($Viernes) {
        $this->Viernes = $Viernes;
    }

    function setSabado($Sabado) {
        $this->Sabado = $Sabado;
    }

    function setId_docente($Id_docente) {
        $this->Id_docente = $Id_docente;
    }

    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }


    
}
