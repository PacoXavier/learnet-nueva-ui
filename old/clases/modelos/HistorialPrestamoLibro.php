<?php

class HistorialPrestamoLibro {
    
    public $Id;
    public $Usu_entrega;
    public $Usu_recibe;
    public $Estado_entrega;
    public $Estado_recibe;
    public $Comentarios;
    public $Fecha_entrega;
    public $Fecha_devolucion;
    public $Id_libro;
    public $Fecha_entregado;
    public $Tipo_usu;

    function __construct() {
        $this->Id = "";
        $this->Usu_entrega = "";
        $this->Usu_recibe = "";
        $this->Estado_entrega = "";
        $this->Estado_recibe = "";
        $this->Comentarios = "";
        $this->Fecha_entrega = "";
        $this->Fecha_devolucion = "";
        $this->Id_libro = "";
        $this->Fecha_entregado = "";
        $this->Tipo_usu = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getUsu_entrega() {
        return $this->Usu_entrega;
    }

    function getUsu_recibe() {
        return $this->Usu_recibe;
    }

    function getEstado_entrega() {
        return $this->Estado_entrega;
    }

    function getEstado_recibe() {
        return $this->Estado_recibe;
    }

    function getComentarios() {
        return $this->Comentarios;
    }

    function getFecha_entrega() {
        return $this->Fecha_entrega;
    }

    function getFecha_devolucion() {
        return $this->Fecha_devolucion;
    }

    function getId_libro() {
        return $this->Id_libro;
    }

    function getFecha_entregado() {
        return $this->Fecha_entregado;
    }

    function getTipo_usu() {
        return $this->Tipo_usu;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setUsu_entrega($Usu_entrega) {
        $this->Usu_entrega = $Usu_entrega;
    }

    function setUsu_recibe($Usu_recibe) {
        $this->Usu_recibe = $Usu_recibe;
    }

    function setEstado_entrega($Estado_entrega) {
        $this->Estado_entrega = $Estado_entrega;
    }

    function setEstado_recibe($Estado_recibe) {
        $this->Estado_recibe = $Estado_recibe;
    }

    function setComentarios($Comentarios) {
        $this->Comentarios = $Comentarios;
    }

    function setFecha_entrega($Fecha_entrega) {
        $this->Fecha_entrega = $Fecha_entrega;
    }

    function setFecha_devolucion($Fecha_devolucion) {
        $this->Fecha_devolucion = $Fecha_devolucion;
    }

    function setId_libro($Id_libro) {
        $this->Id_libro = $Id_libro;
    }

    function setFecha_entregado($Fecha_entregado) {
        $this->Fecha_entregado = $Fecha_entregado;
    }

    function setTipo_usu($Tipo_usu) {
        $this->Tipo_usu = $Tipo_usu;
    }



    
    

}
