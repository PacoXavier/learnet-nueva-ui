<?php

class Ofertas {
    public $Id;
    public $Id_plantel;
    public $Nombre_oferta;
    public $Activo_oferta;
    public $Clave_oferta;
    public $Tipo_recargo;
    public $Valor;
    public $TipoOferta;
    public $DateCreated;
    public $MontoCargoMateriaAcreditada;
    
    function __construct() {
        $this->Id = "";
        $this->Id_plantel = "";
        $this->Nombre_oferta = "";
        $this->Activo_oferta = "";
        $this->Clave_oferta = "";
        $this->Tipo_recargo = "";
        $this->Valor = "";
        $this->TipoOferta = "";
        $this->DateCreated = "";
        $this->MontoCargoMateriaAcreditada = "";
    }
    
    function getMontoCargoMateriaAcreditada() {
        return $this->MontoCargoMateriaAcreditada;
    }

    function setMontoCargoMateriaAcreditada($MontoCargoMateriaAcreditada) {
        $this->MontoCargoMateriaAcreditada = $MontoCargoMateriaAcreditada;
    }

    function getTipoOferta() {
        return $this->TipoOferta;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function setTipoOferta($TipoOferta) {
        $this->TipoOferta = $TipoOferta;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

        
    function getTipo_recargo() {
        return $this->Tipo_recargo;
    }

    function getValor() {
        return $this->Valor;
    }

    function setTipo_recargo($Tipo_recargo) {
        $this->Tipo_recargo = $Tipo_recargo;
    }

    function setValor($Valor) {
        $this->Valor = $Valor;
    }

   
    public function getId() {
        return $this->Id;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function getNombre_oferta() {
        return $this->Nombre_oferta;
    }

    public function getActivo_oferta() {
        return $this->Activo_oferta;
    }

    public function getClave_oferta() {
        return $this->Clave_oferta;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    public function setNombre_oferta($Nombre_oferta) {
        $this->Nombre_oferta = $Nombre_oferta;
    }

    public function setActivo_oferta($Activo_oferta) {
        $this->Activo_oferta = $Activo_oferta;
    }

    public function setClave_oferta($Clave_oferta) {
        $this->Clave_oferta = $Clave_oferta;
    }



}
