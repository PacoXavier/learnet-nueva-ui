<?php

class Direcciones {
    
    public $Id;
    public $Nombre_dir;
    public $Calle_dir;
    public $NumExt_dir;
    public $NumInt_dir;
    public $Colonia_dir;
    public $Ciudad_dir;
    public $Estado_dir;
    public $Cp_dir;
    public $Pais_dir;
    public $TipoRel_dir;
    public $IdRel_dir;
    public $Tel_dir;
    public $Movil_dir;

    function __construct() {
        $this->Id = "";
        $this->Nombre_dir = "";
        $this->Calle_dir = "";
        $this->NumExt_dir = "";
        $this->NumInt_dir = "";
        $this->Colonia_dir = "";
        $this->Ciudad_dir = "";
        $this->Estado_dir = "";
        $this->Cp_dir = "";
        $this->Pais_dir = "";
        $this->TipoRel_dir = "";
        $this->IdRel_dir = "";
        $this->Tel_dir = "";
        $this->Movil_dir = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getNombre_dir() {
        return $this->Nombre_dir;
    }

    function getCalle_dir() {
        return $this->Calle_dir;
    }

    function getNumExt_dir() {
        return $this->NumExt_dir;
    }

    function getNumInt_dir() {
        return $this->NumInt_dir;
    }

    function getColonia_dir() {
        return $this->Colonia_dir;
    }

    function getCiudad_dir() {
        return $this->Ciudad_dir;
    }

    function getEstado_dir() {
        return $this->Estado_dir;
    }

    function getCp_dir() {
        return $this->Cp_dir;
    }

    function getPais_dir() {
        return $this->Pais_dir;
    }

    function getTipoRel_dir() {
        return $this->TipoRel_dir;
    }

    function getIdRel_dir() {
        return $this->IdRel_dir;
    }

    function getTel_dir() {
        return $this->Tel_dir;
    }

    function getMovil_dir() {
        return $this->Movil_dir;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setNombre_dir($Nombre_dir) {
        $this->Nombre_dir = $Nombre_dir;
    }

    function setCalle_dir($Calle_dir) {
        $this->Calle_dir = $Calle_dir;
    }

    function setNumExt_dir($NumExt_dir) {
        $this->NumExt_dir = $NumExt_dir;
    }

    function setNumInt_dir($NumInt_dir) {
        $this->NumInt_dir = $NumInt_dir;
    }

    function setColonia_dir($Colonia_dir) {
        $this->Colonia_dir = $Colonia_dir;
    }

    function setCiudad_dir($Ciudad_dir) {
        $this->Ciudad_dir = $Ciudad_dir;
    }

    function setEstado_dir($Estado_dir) {
        $this->Estado_dir = $Estado_dir;
    }

    function setCp_dir($Cp_dir) {
        $this->Cp_dir = $Cp_dir;
    }

    function setPais_dir($Pais_dir) {
        $this->Pais_dir = $Pais_dir;
    }

    function setTipoRel_dir($TipoRel_dir) {
        $this->TipoRel_dir = $TipoRel_dir;
    }

    function setIdRel_dir($IdRel_dir) {
        $this->IdRel_dir = $IdRel_dir;
    }

    function setTel_dir($Tel_dir) {
        $this->Tel_dir = $Tel_dir;
    }

    function setMovil_dir($Movil_dir) {
        $this->Movil_dir = $Movil_dir;
    }



    
    

}
