<?php

class EncuestasAlumno {
    
    public $Id;
    public $Id_alum;
    public $Id_enc;
    public $Id_docen;
    public $DateCreated;
    
    function __construct() {
        $this->Id = "";
        $this->Id_alum = "";
        $this->Id_enc = "";
        $this->Id_docen = "";
        $this->DateCreated = "";
    }

    function getId() {
        return $this->Id;
    }

    function getId_alum() {
        return $this->Id_alum;
    }

    function getId_enc() {
        return $this->Id_enc;
    }

    function getId_docen() {
        return $this->Id_docen;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_alum($Id_alum) {
        $this->Id_alum = $Id_alum;
    }

    function setId_enc($Id_enc) {
        $this->Id_enc = $Id_enc;
    }

    function setId_docen($Id_docen) {
        $this->Id_docen = $Id_docen;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }


   
}
