<?php

class MateriasCicloAlumno {

    public $Id;
    public $Id_ciclo_alum;
    public $Id_mat_esp;
    public $Id_ori;
    public $Id_grupo;
    public $CalExtraordinario;
    public $CalEspecial;
    public $CalTotalParciales;
    public $Tipo;
    public $Ids_pagos;
    public $Activo;
    public $Id_alum;

    function __construct() {
        $this->Id = "";
        $this->Id_ciclo_alum = "";
        $this->Id_mat_esp = "";
        $this->Id_ori = "";
        $this->Id_grupo = "";
        $this->CalExtraordinario = "";
        $this->CalEspecial = "";
        $this->CalTotalParciales = "";
        $this->Tipo = "";
        $this->Ids_pagos = "";
        $this->Activo = "";
        $this->Id_alum = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getId_ciclo_alum() {
        return $this->Id_ciclo_alum;
    }

    public function getId_mat_esp() {
        return $this->Id_mat_esp;
    }

    public function getId_ori() {
        return $this->Id_ori;
    }

    public function getId_grupo() {
        return $this->Id_grupo;
    }

    public function getCalExtraordinario() {
        return $this->CalExtraordinario;
    }

    public function getCalEspecial() {
        return $this->CalEspecial;
    }

    public function getCalTotalParciales() {
        return $this->CalTotalParciales;
    }

    public function getTipo() {
        return $this->Tipo;
    }

    public function getIds_pagos() {
        return $this->Ids_pagos;
    }

    public function getActivo() {
        return $this->Activo;
    }

    public function getId_alum() {
        return $this->Id_alum;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setId_ciclo_alum($Id_ciclo_alum) {
        $this->Id_ciclo_alum = $Id_ciclo_alum;
    }

    public function setId_mat_esp($Id_mat_esp) {
        $this->Id_mat_esp = $Id_mat_esp;
    }

    public function setId_ori($Id_ori) {
        $this->Id_ori = $Id_ori;
    }

    public function setId_grupo($Id_grupo) {
        $this->Id_grupo = $Id_grupo;
    }

    public function setCalExtraordinario($CalExtraordinario) {
        $this->CalExtraordinario = $CalExtraordinario;
    }

    public function setCalEspecial($CalEspecial) {
        $this->CalEspecial = $CalEspecial;
    }

    public function setCalTotalParciales($CalTotalParciales) {
        $this->CalTotalParciales = $CalTotalParciales;
    }

    public function setTipo($Tipo) {
        $this->Tipo = $Tipo;
    }

    public function setIds_pagos($Ids_pagos) {
        $this->Ids_pagos = $Ids_pagos;
    }

    public function setActivo($Activo) {
        $this->Activo = $Activo;
    }

    public function setId_alum($Id_alum) {
        $this->Id_alum = $Id_alum;
    }



   
}
