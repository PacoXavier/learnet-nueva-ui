<?php

class MateriasAcreditadas {
    
    public $Id;
    public $Id_mat_esp;
    public $Id_ofe_alum;
    public $Fecha_ac;
    public $Id_ciclo;
    public $Id_usu;
    public $Id_pago;
    public $Comentarios;

    function __construct() {
        $this->Id = "";
        $this->Id_mat_esp = "";
        $this->Id_ofe_alum = "";
        $this->Fecha_ac = "";
        $this->Id_ciclo = "";
        $this->Id_usu = "";
        $this->Id_pago = "";
        $this->Comentarios = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getId_mat_esp() {
        return $this->Id_mat_esp;
    }

    function getId_ofe_alum() {
        return $this->Id_ofe_alum;
    }

    function getFecha_ac() {
        return $this->Fecha_ac;
    }

    function getId_ciclo() {
        return $this->Id_ciclo;
    }

    function getId_usu() {
        return $this->Id_usu;
    }

    function getId_pago() {
        return $this->Id_pago;
    }

    function getComentarios() {
        return $this->Comentarios;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_mat_esp($Id_mat_esp) {
        $this->Id_mat_esp = $Id_mat_esp;
    }

    function setId_ofe_alum($Id_ofe_alum) {
        $this->Id_ofe_alum = $Id_ofe_alum;
    }

    function setFecha_ac($Fecha_ac) {
        $this->Fecha_ac = $Fecha_ac;
    }

    function setId_ciclo($Id_ciclo) {
        $this->Id_ciclo = $Id_ciclo;
    }

    function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    function setId_pago($Id_pago) {
        $this->Id_pago = $Id_pago;
    }

    function setComentarios($Comentarios) {
        $this->Comentarios = $Comentarios;
    }


}
