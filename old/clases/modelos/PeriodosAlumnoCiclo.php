<?php
class PeriodosAlumnoCiclo{
    
    public $Id;
    public $DateCreated;
    public $Activo;
    public $Id_ofe_alum;
    public $Id_grado;

    function _construct(){
        $this->Id="";
        $this->DateCreated="";
        $this->Activo="";
        $this->Id_ofe_alum="";
        $this->Id_grado="";
    }
    
    function getId_ofe_alum() {
        return $this->Id_ofe_alum;
    }

    function getId_grado() {
        return $this->Id_grado;
    }

    function setId_ofe_alum($Id_ofe_alum) {
        $this->Id_ofe_alum = $Id_ofe_alum;
    }

    function setId_grado($Id_grado) {
        $this->Id_grado = $Id_grado;
    }

        
    function getId() {
        return $this->Id;
    }

    function getDateCreated() {
        return $this->DateCreated;
    }


    function getActivo() {
        return $this->Activo;
    }


    function setId($Id) {
        $this->Id = $Id;
    }

    function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }


    function setActivo($Activo) {
        $this->Activo = $Activo;
    }


}
