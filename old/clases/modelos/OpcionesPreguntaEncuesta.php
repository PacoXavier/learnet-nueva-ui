<?php

class OpcionesPreguntaEncuesta {
    
    public $Id;
    public $Id_pre;
    public $TextoOpcion;
    public $Activo;
    public $Valor;

    function __construct() {
        $this->Id = "";
        $this->Id_pre = "";
        $this->TextoOpcion = "";
        $this->Activo = "";
        $this->Valor = "";
    }

    function getId() {
        return $this->Id;
    }

    function getId_pre() {
        return $this->Id_pre;
    }

    function getTextoOpcion() {
        return $this->TextoOpcion;
    }

    function getActivo() {
        return $this->Activo;
    }

    function getValor() {
        return $this->Valor;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_pre($Id_pre) {
        $this->Id_pre = $Id_pre;
    }

    function setTextoOpcion($TextoOpcion) {
        $this->TextoOpcion = $TextoOpcion;
    }

    function setActivo($Activo) {
        $this->Activo = $Activo;
    }

    function setValor($Valor) {
        $this->Valor = $Valor;
    }


   
}
