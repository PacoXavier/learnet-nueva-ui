<?php

class TareasActividad {
    
    public $Id;
    public $Comentario;
    public $Id_usuCreated;
    public $Id_act;
    public $Id_usuRespon;
    public $Status;
    public $DateCreated;
    public $StartDate;
    public $EndDate;
    public $Prioridad;
    public $Order;
    public $Id_usu_realizo;
    public $Comentario_realizo;
    
    
    function __construct() {
        $this->Id = "";
        $this->Comentario = "";
        $this->Id_usuCreated = "";
        $this->Id_act = "";
        $this->Id_usuRespon = "";
        $this->Status = "";
        $this->DateCreated = "";
        $this->StartDate = "";
        $this->EndDate = "";
        $this->Prioridad = "";
        $this->Order = "";
        $this->Id_usu_realizo = "";
        $this->Comentario_realizo = "";
    }
    
    public function getId_usu_realizo() {
        return $this->Id_usu_realizo;
    }

    public function getComentario_realizo() {
        return $this->Comentario_realizo;
    }

    public function setId_usu_realizo($Id_usu_realizo) {
        $this->Id_usu_realizo = $Id_usu_realizo;
    }


    public function setComentario_realizo($Comentario_realizo) {
        $this->Comentario_realizo = $Comentario_realizo;
    }
    
    public function getOrder() {
        return $this->Order;
    }

    public function setOrder($Order) {
        $this->Order = $Order;
    }

        
    public function getId() {
        return $this->Id;
    }

    public function getComentario() {
        return $this->Comentario;
    }

    public function getId_usuCreated() {
        return $this->Id_usuCreated;
    }

    public function getId_act() {
        return $this->Id_act;
    }

    public function getId_usuRespon() {
        return $this->Id_usuRespon;
    }

    public function getStatus() {
        return $this->Status;
    }

    public function getDateCreated() {
        return $this->DateCreated;
    }

    public function getStartDate() {
        return $this->StartDate;
    }

    public function getEndDate() {
        return $this->EndDate;
    }

    public function getPrioridad() {
        return $this->Prioridad;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setComentario($Comentario) {
        $this->Comentario = $Comentario;
    }

    public function setId_usuCreated($Id_usuCreated) {
        $this->Id_usuCreated = $Id_usuCreated;
    }

    public function setId_act($Id_act) {
        $this->Id_act = $Id_act;
    }

    public function setId_usuRespon($Id_usuRespon) {
        $this->Id_usuRespon = $Id_usuRespon;
    }

    public function setStatus($Status) {
        $this->Status = $Status;
    }

    public function setDateCreated($DateCreated) {
        $this->DateCreated = $DateCreated;
    }

    public function setStartDate($StartDate) {
        $this->StartDate = $StartDate;
    }

    public function setEndDate($EndDate) {
        $this->EndDate = $EndDate;
    }

    public function setPrioridad($Prioridad) {
        $this->Prioridad = $Prioridad;
    }

}
