<?php
class CategoriasEvaluacion{
    
    public $Id;
    public $Nombre;
    public $DatecCeated;
    public $Id_usu;
    public $Activo;
    public $Id_plantel;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->DatecCeated = "";
        $this->Id_usu = "";
        $this->Activo = "";
        $this->Id_plantel = "";
    }
    
    function getId_plantel() {
        return $this->Id_plantel;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    function getId() {
        return $this->Id;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getDatecCeated() {
        return $this->DatecCeated;
    }

    function getId_usu() {
        return $this->Id_usu;
    }

    function getActivo() {
        return $this->Activo;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setDatecCeated($DatecCeated) {
        $this->DatecCeated = $DatecCeated;
    }

    function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    function setActivo($Activo) {
        $this->Activo = $Activo;
    }
}