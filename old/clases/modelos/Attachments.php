<?php

class Attachments {
    
    public $Id;
    public $Nombre_atta;
    public $Mime_atta;
    public $Llave_atta;
    public $Id_plantel;
    public $IdRel;
    public $TipoRel;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre_atta = "";
        $this->Mime_atta = "";
        $this->Llave_atta = "";
        $this->Id_plantel = "";
        $this->IdRel = "";
        $this->TipoRel = "";
    }
    function getIdRel() {
        return $this->IdRel;
    }

    function getTipoRel() {
        return $this->TipoRel;
    }

    function setIdRel($IdRel) {
        $this->IdRel = $IdRel;
    }

    function setTipoRel($TipoRel) {
        $this->TipoRel = $TipoRel;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getNombre_atta() {
        return $this->Nombre_atta;
    }

    public function getMime_atta() {
        return $this->Mime_atta;
    }

    public function getLlave_atta() {
        return $this->Llave_atta;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }
    
    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setNombre_atta($Nombre_atta) {
        $this->Nombre_atta = $Nombre_atta;
    }

    public function setMime_atta($Mime_atta) {
        $this->Mime_atta = $Mime_atta;
    }

    public function setLlave_atta($Llave_atta) {
        $this->Llave_atta = $Llave_atta;
    }



}
