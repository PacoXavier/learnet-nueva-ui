<?php

class LogsInscripciones {
    
    public $Id;
    public $Id_usu;
    public $Fecha_log;
    public $Comen_log;
    public $Idrel_log;
    public $Tipo_relLog;
    public $Dia_contactar;
    public $DateDead_not;
    public $Id_usuRead_not;
    public $TipoSeguimiento;
    
    function __construct() {
        $this->Id = "";
        $this->Id_usu = "";
        $this->Fecha_log = "";
        $this->Comen_log = "";
        $this->Idrel_log = "";
        $this->Tipo_relLog = "";
        $this->Dia_contactar = "";
        $this->DateDead_not = "";
        $this->Id_usuRead_not = "";
        $this->TipoSeguimiento = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getId_usu() {
        return $this->Id_usu;
    }

    function getFecha_log() {
        return $this->Fecha_log;
    }

    function getComen_log() {
        return $this->Comen_log;
    }

    function getIdrel_log() {
        return $this->Idrel_log;
    }

    function getTipo_relLog() {
        return $this->Tipo_relLog;
    }

    function getDia_contactar() {
        return $this->Dia_contactar;
    }

    function getDateDead_not() {
        return $this->DateDead_not;
    }

    function getId_usuRead_not() {
        return $this->Id_usuRead_not;
    }

    function getTipoSeguimiento() {
        return $this->TipoSeguimiento;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_usu($Id_usu) {
        $this->Id_usu = $Id_usu;
    }

    function setFecha_log($Fecha_log) {
        $this->Fecha_log = $Fecha_log;
    }

    function setComen_log($Comen_log) {
        $this->Comen_log = $Comen_log;
    }

    function setIdrel_log($Idrel_log) {
        $this->Idrel_log = $Idrel_log;
    }

    function setTipo_relLog($Tipo_relLog) {
        $this->Tipo_relLog = $Tipo_relLog;
    }

    function setDia_contactar($Dia_contactar) {
        $this->Dia_contactar = $Dia_contactar;
    }

    function setDateDead_not($DateDead_not) {
        $this->DateDead_not = $DateDead_not;
    }

    function setId_usuRead_not($Id_usuRead_not) {
        $this->Id_usuRead_not = $Id_usuRead_not;
    }

    function setTipoSeguimiento($TipoSeguimiento) {
        $this->TipoSeguimiento = $TipoSeguimiento;
    }



}
