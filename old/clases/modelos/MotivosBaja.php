<?php

class MotivosBaja {
    
    public $Id;
    public $Nombre;
    public $Activo_mot;
    public $Id_plantel;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->Activo_mot = "";
        $this->Id_plantel = "";
    }
    
    public function getId() {
        return $this->Id;
    }

    public function getNombre() {
        return $this->Nombre;
    }

    public function getActivo_mot() {
        return $this->Activo_mot;
    }

    public function getId_plantel() {
        return $this->Id_plantel;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    public function setActivo_mot($Activo_mot) {
        $this->Activo_mot = $Activo_mot;
    }

    public function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }



    
}
