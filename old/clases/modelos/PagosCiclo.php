<?php

class PagosCiclo {
    public $Id;
    public $Folio;
    public $Concepto;
    public $Fecha_pago;
    public $Mensualidad;
    public $Descuento;
    public $Cantidad_Pagada;
    public $Fecha_pagado;
    public $Id_ciclo_alum;
    public $Tipo_pago;
    public $Id_mis;
    public $Id_alum;
    public $Descuento_beca;
    public $DeadCargo;
    public $DeadCargoUsu;
    public $FechaCapturaBeca;
    public $UsuCapturaBeca;
    public $FechaCapturaDescuento;
    public $UsuCapturaDescuento;
    public $Pagado;
    public $IdRel;
    public $TipoRel;
    public $UsuCapturaCargo;
    public $FechaCapturaCargo;
            
    
    function __construct() {
        $this->Id = "";
        $this->Folio = "";
        $this->Concepto = "";
        $this->Fecha_pago = "";
        $this->Mensualidad = "";
        $this->Descuento = "";
        $this->Cantidad_Pagada = "";
        $this->Fecha_pagado = "";
        $this->Id_ciclo_alum = "";
        $this->Tipo_pago = "";
        $this->Id_mis = "";
        $this->Id_alum = "";
        $this->Descuento_beca = "";
        $this->DeadCargo = "";
        $this->DeadCargoUsu = "";
        $this->FechaCapturaBeca = "";
        $this->UsuCapturaBeca = "";
        $this->FechaCapturaDescuento = "";
        $this->UsuCapturaDescuento = "";
        $this->Pagado = "";
        $this->IdRel = "";
        $this->TipoRel = "";
        $this->UsuCapturaCargo = "";
        $this->FechaCapturaCargo = "";
    }
    
    function getUsuCapturaCargo() {
        return $this->UsuCapturaCargo;
    }

    function getFechaCapturaCargo() {
        return $this->FechaCapturaCargo;
    }

    function setUsuCapturaCargo($UsuCapturaCargo) {
        $this->UsuCapturaCargo = $UsuCapturaCargo;
    }

    function setFechaCapturaCargo($FechaCapturaCargo) {
        $this->FechaCapturaCargo = $FechaCapturaCargo;
    }

        
    function getIdRel() {
        return $this->IdRel;
    }

    function getTipoRel() {
        return $this->TipoRel;
    }

    function setIdRel($IdRel) {
        $this->IdRel = $IdRel;
    }

    function setTipoRel($TipoRel) {
        $this->TipoRel = $TipoRel;
    }

        
    function getPagado() {
        return $this->Pagado;
    }

    function setPagado($Pagado) {
        $this->Pagado = $Pagado;
    }

        
    function getDeadCargo() {
        return $this->DeadCargo;
    }

    function getDeadCargoUsu() {
        return $this->DeadCargoUsu;
    }

    function getFechaCapturaBeca() {
        return $this->FechaCapturaBeca;
    }

    function getUsuCapturaBeca() {
        return $this->UsuCapturaBeca;
    }

    function getFechaCapturaDescuento() {
        return $this->FechaCapturaDescuento;
    }

    function getUsuCapturaDescuento() {
        return $this->UsuCapturaDescuento;
    }

    function setDeadCargo($DeadCargo) {
        $this->DeadCargo = $DeadCargo;
    }

    function setDeadCargoUsu($DeadCargoUsu) {
        $this->DeadCargoUsu = $DeadCargoUsu;
    }

    function setFechaCapturaBeca($FechaCapturaBeca) {
        $this->FechaCapturaBeca = $FechaCapturaBeca;
    }

    function setUsuCapturaBeca($UsuCapturaBeca) {
        $this->UsuCapturaBeca = $UsuCapturaBeca;
    }

    function setFechaCapturaDescuento($FechaCapturaDescuento) {
        $this->FechaCapturaDescuento = $FechaCapturaDescuento;
    }

    function setUsuCapturaDescuento($UsuCapturaDescuento) {
        $this->UsuCapturaDescuento = $UsuCapturaDescuento;
    }

        
    public function getId() {
        return $this->Id;
    }

    public function getFolio() {
        return $this->Folio;
    }

    public function getConcepto() {
        return $this->Concepto;
    }

    public function getFecha_pago() {
        return $this->Fecha_pago;
    }

    public function getMensualidad() {
        return $this->Mensualidad;
    }

    public function getDescuento() {
        return $this->Descuento;
    }

    public function getCantidad_Pagada() {
        return $this->Cantidad_Pagada;
    }

    public function getFecha_pagado() {
        return $this->Fecha_pagado;
    }

    public function getId_ciclo_alum() {
        return $this->Id_ciclo_alum;
    }

    public function getTipo_pago() {
        return $this->Tipo_pago;
    }

    public function getId_mis() {
        return $this->Id_mis;
    }

    public function getId_alum() {
        return $this->Id_alum;
    }

    public function getDescuento_beca() {
        return $this->Descuento_beca;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setFolio($Folio) {
        $this->Folio = $Folio;
    }

    public function setConcepto($Concepto) {
        $this->Concepto = $Concepto;
    }

    public function setFecha_pago($Fecha_pago) {
        $this->Fecha_pago = $Fecha_pago;
    }

    public function setMensualidad($Mensualidad) {
        $this->Mensualidad = $Mensualidad;
    }

    public function setDescuento($Descuento) {
        $this->Descuento = $Descuento;
    }

    public function setCantidad_Pagada($Cantidad_Pagada) {
        $this->Cantidad_Pagada = $Cantidad_Pagada;
    }

    public function setFecha_pagado($Fecha_pagado) {
        $this->Fecha_pagado = $Fecha_pagado;
    }

    public function setId_ciclo_alum($Id_ciclo_alum) {
        $this->Id_ciclo_alum = $Id_ciclo_alum;
    }

    public function setTipo_pago($Tipo_pago) {
        $this->Tipo_pago = $Tipo_pago;
    }

    public function setId_mis($Id_mis) {
        $this->Id_mis = $Id_mis;
    }

    public function setId_alum($Id_alum) {
        $this->Id_alum = $Id_alum;
    }

    public function setDescuento_beca($Descuento_beca) {
        $this->Descuento_beca = $Descuento_beca;
    }



}
