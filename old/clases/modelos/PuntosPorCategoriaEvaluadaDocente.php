<?php
class PuntosPorCategoriaEvaluadaDocente{
    
    public $Id;
    public $Id_nivel;
    public $Pago_por_hora;
    public $Id_eva;
    
    function __construct() {
        $this->Id = "";
        $this->Id_nivel = "";
        $this->Pago_por_hora = "";
        $this->Id_eva = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getId_nivel() {
        return $this->Id_nivel;
    }


    function getPago_por_hora() {
        return $this->Pago_por_hora;
    }

    function getId_eva() {
        return $this->Id_eva;
    }

    function setId($Id) {
        $this->Id = $Id;
    }


    function setId_nivel($Id_nivel) {
        $this->Id_nivel = $Id_nivel;
    }


    function setPago_por_hora($Pago_por_hora) {
        $this->Pago_por_hora = $Pago_por_hora;
    }

    function setId_eva($Id_eva) {
        $this->Id_eva = $Id_eva;
    }



}

