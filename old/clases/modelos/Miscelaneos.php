<?php

class Miscelaneos {
    public $Id;
    public $Nombre_mis;
    public $Costo_mis;
    public $Activo_mis;
    public $Id_plantel;
            
    function __construct() {
        $this->Id = "";
        $this->Nombre_mis = "";
        $this->Costo_mis = "";
        $this->Activo_mis = "";
        $this->Id_plantel = "";
    }
    
    function getId_plantel() {
        return $this->Id_plantel;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

        
    public function getId() {
        return $this->Id;
    }

    public function getNombre_mis() {
        return $this->Nombre_mis;
    }

    public function getCosto_mis() {
        return $this->Costo_mis;
    }

    public function getActivo_mis() {
        return $this->Activo_mis;
    }

    public function setId($Id) {
        $this->Id = $Id;
    }

    public function setNombre_mis($Nombre_mis) {
        $this->Nombre_mis = $Nombre_mis;
    }

    public function setCosto_mis($Costo_mis) {
        $this->Costo_mis = $Costo_mis;
    }

    public function setActivo_mis($Activo_mis) {
        $this->Activo_mis = $Activo_mis;
    }



    
}
