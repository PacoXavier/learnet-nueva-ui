<?php
class NotificacionesTipoUsuario {
    
    public $Id;
    public $IdRel;
    public $TipoRel;
    public $DateRead;
    public $Id_not;

    function __construct() {
        $this->Id = "";
        $this->IdRel = "";
        $this->TipoRel = "";
        $this->DateRead = "";
        $this->Id_not = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getIdRel() {
        return $this->IdRel;
    }

    function getTipoRel() {
        return $this->TipoRel;
    }

    function getDateRead() {
        return $this->DateRead;
    }

    function getId_not() {
        return $this->Id_not;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setIdRel($IdRel) {
        $this->IdRel = $IdRel;
    }

    function setTipoRel($TipoRel) {
        $this->TipoRel = $TipoRel;
    }

    function setDateRead($DateRead) {
        $this->DateRead = $DateRead;
    }

    function setId_not($Id_not) {
        $this->Id_not = $Id_not;
    }



}
