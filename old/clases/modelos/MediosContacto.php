<?php

class MediosContacto {

    public $Id;
    public $Nombre_medio;
    public $Id_plantel;
    public $Activo_medio;
   
    function __construct() {
        $this->Id = "";
        $this->Nombre_medio = "";
        $this->Id_plantel = "";
        $this->Activo_medio = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getNombre_medio() {
        return $this->Nombre_medio;
    }

    function getId_plantel() {
        return $this->Id_plantel;
    }

    function getActivo_medio() {
        return $this->Activo_medio;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setNombre_medio($Nombre_medio) {
        $this->Nombre_medio = $Nombre_medio;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

    function setActivo_medio($Activo_medio) {
        $this->Activo_medio = $Activo_medio;
    }





}
