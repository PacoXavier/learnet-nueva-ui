<?php
class OfertasPorCategoria{
    
    public $Id;
    public $Id_ofe;
    public $Id_tipo;

    function __construct() {
        $this->Id = "";
        $this->Id_ofe = "";
        $this->Id_tipo = "";
    }

    function getId() {
        return $this->Id;
    }

    function getId_ofe() {
        return $this->Id_ofe;
    }

    function getId_tipo() {
        return $this->Id_tipo;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setId_ofe($Id_ofe) {
        $this->Id_ofe = $Id_ofe;
    }

    function setId_tipo($Id_tipo) {
        $this->Id_tipo = $Id_tipo;
    }
    
}