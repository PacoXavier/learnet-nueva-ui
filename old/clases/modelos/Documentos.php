<?php

class Documentos {
    public $Id;
    public $Nombre;
    public $Id_plantel;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre = "";
        $this->Id_plantel = "";
    }
    
    function getId() {
        return $this->Id;
    }

    function getNombre() {
        return $this->Nombre;
    }

    function getId_plantel() {
        return $this->Id_plantel;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setNombre($Nombre) {
        $this->Nombre = $Nombre;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }




}
