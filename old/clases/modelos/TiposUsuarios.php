<?php
class TiposUsuarios {
    
    public $Id;
    public $Nombre_tipo;
    public $Activo_tipo;
    public $Id_plantel;
    
    function __construct() {
        $this->Id = "";
        $this->Nombre_tipo = "";
        $this->Activo_tipo = "";
        $this->Id_plantel = "";
    }
    
    function getId_plantel() {
        return $this->Id_plantel;
    }

    function setId_plantel($Id_plantel) {
        $this->Id_plantel = $Id_plantel;
    }

        function getId() {
        return $this->Id;
    }

    function getNombre_tipo() {
        return $this->Nombre_tipo;
    }

    function getActivo_tipo() {
        return $this->Activo_tipo;
    }



    function setId($Id) {
        $this->Id = $Id;
    }

    function setNombre_tipo($Nombre_tipo) {
        $this->Nombre_tipo = $Nombre_tipo;
    }

    function setActivo_tipo($Activo_tipo) {
        $this->Activo_tipo = $Activo_tipo;
    }


}
