<?php
require_once('estandares/includes.php');
require_once('clases/DaoOfertas.php');
if (!isset($perm['42'])) {
    header('Location: home.php');
}
require_once('estandares/class_ofertas.php');
links_head("Niveles acad&eacute;micos");
write_head_body();
write_body();
$DaoOfertas = new DaoOfertas();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-list-ul" aria-hidden="true"></i> Niveles acad&eacute;micos</h1>
                </div>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>Nombre</td>
                            <td class="td-center">Fecha de alta</td>
                            <td class="td-center">Tipo de plan</td>
                            <td class="td-center">Tipo de recargo</td>
                            <td class="td-center">Monto recargo</td>
                            <td class="td-center">Monto mat. acred</td>
                            <td class="td-center">Estatus</td>
                            <td class="td-center">Acciones</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $count = 1;
                        foreach ($DaoOfertas->showAll() as $oferta) {
                           $tipo="Sin ciclos";
                            if($oferta->getTipoOferta()==1){
                                $tipo="Por ciclos";
                            }
                            $cargoAcre="";
                            if(strlen($oferta->getMontoCargoMateriaAcreditada())>0){
                                $cargoAcre="$".number_format($oferta->getMontoCargoMateriaAcreditada(),2);
                            }
                            ?>
                            <tr>
                                <td><?php echo $count ?></td>
                                <td><?php echo $oferta->getNombre_oferta() ?></td>
                                <td class="td-center"><?php echo $oferta->getDateCreated() ?></td>
                                <td class="td-center"><?php echo $tipo ?></td>
                                <td class="td-center"><?php echo $oferta->getTipo_recargo() ?></td>
                                <td class="td-center"><?php echo $oferta->getValor() ?></td>
                                <td class="td-center"><?php echo  $cargoAcre?></td>
                                <td class="td-center">Activo</td>
                                <td class="td-center">
                                    <button onclick="mostrar(<?php echo $oferta->getId() ?>)">Especialidades</button>
                                    <button onclick="box_update_oferta(<?php echo $oferta->getId() ?>)">Editar</button>
                                    <button onclick="delete_ofe(<?php echo $oferta->getId() ?>)">Eliminar</button>
                                </td>
                            </tr>
                            <?php
                            $count++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><span onclick="box_update_oferta()">Nuevo nivel académico</span></li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<?php
write_footer();