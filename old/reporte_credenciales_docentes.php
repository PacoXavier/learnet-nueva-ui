<?php
require_once('estandares/includes.php');
if(!isset($perm['62'])){
  header('Location: home.php');
}
require_once('estandares/class_docentes.php');
links_head("Generar credenciales  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-camera-retro"></i> Credenciales docentes</h1>
                </div>
                <ul class="form">
                </ul>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Mostrar filtros</li>
                        <li onclick="confirmarEnvio()"><i class="fa fa-cloud-download"></i> Generar</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td style="width: 80px">Clave</td>
                                <td>Nombre</td>
                                <td  style="text-align:center;">Ult. Renovaci&oacute;n</td>
                                <td  style="text-align:center;">Vigencia</td>
                                <td  style="text-align:center;"><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                                <td  style="text-align:center;">Estatus</td>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $class_docentes = new class_docentes();
                            foreach ($class_docentes->get_docentes() as $k => $v) {
                                if(strlen($v['Img_docen'])>0){
                                    $vigencia="";
                                    if(strlen($v['UltimaGeneracion_credencial'])>0){
                                        $anio=date("Y",strtotime($v['UltimaGeneracion_credencial']));
                                        $mes=date("m",strtotime($v['UltimaGeneracion_credencial']));
                                        $dia=date("d",strtotime($v['UltimaGeneracion_credencial']));
                                        $vigencia=date("Y-m-d",mktime(0, 0, 0, $mes, $dia, $anio+1));
                        
                                    }
                                    $hoy=date("Y-m-d");
                                    $style="background: lightgreen;";
                                    $Status="Vigente";
                                    if($hoy>$vigencia){
                                        $style="background: pink;";
                                        $Status="Renovar";
                                    }
                                ?>
                                <tr id_docen="<?php echo $v['Id_docen'];?>">
                                    <td><?php echo $v['Clave_docen'] ?></td>
                                    <td><?php echo $v['Nombre_docen'] . " " . $v['ApellidoP_docen'] . " " . $v['ApellidoM_docen'] ?></td>
                                    <td style="text-align:center;"><?php echo $v['UltimaGeneracion_credencial']?></td>
                                    <td style="text-align:center;"><?php echo $vigencia; ?></td>
                                    <td style="text-align:center;"><input type="checkbox"> </td>
                                    <td style="text-align:center;<?php echo $style?>"><span><?php echo $Status?></span></td>
                                </tr>
                                <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
        <div class="boxUlBuscador">
            <p>Docente<br><input type="search"  class="buscarFiltro" onkeyup="buscarDocen()" placeholder="Nombre"/></p>
            <ul class="Ulbuscador"></ul>
        </div>
       <p>Estatus<br>
            <select id="status">
              <option value="0"></option>
              <option value="1">Vigente</option>
              <option value="2">Renovar</option>
            </select>
	</p>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();
