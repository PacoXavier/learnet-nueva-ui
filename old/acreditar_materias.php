<?php
require_once('estandares/includes.php');
if(!isset($perm['63'])){
  header('Location: home.php');
}

require_once('clases/modelos/base.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoCiclosAlumno.php');
require_once('clases/DaoMiscelaneos.php');
require_once('clases/DaoMaterias.php');
require_once('clases/DaoMateriasEspecialidad.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoMateriasCicloAlumno.php');
require_once('clases/DaoMateriasAcreditadas.php');
require_once('clases/DaoPagosCiclo.php');
require_once('clases/modelos/MateriasAcreditadas.php');
require_once('clases/modelos/PagosCiclo.php');
require_once('clases/DaoOrientaciones.php');

$DaoAlumnos= new DaoAlumnos();
$DaoCiclos= new DaoCiclos();
$DaoOfertas= new DaoOfertas();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoEspecialidades=new DaoEspecialidades();
$DaoMateriasAcreditadas= new DaoMateriasAcreditadas();
$DaoMaterias= new DaoMaterias();
$DaoMateriasEspecialidad= new DaoMateriasEspecialidad();
$DaoGrados=new DaoGrados();
$DaoOrientaciones= new DaoOrientaciones();

links_head("Acreditar materias  ");
write_head_body();
write_body();

$id_esp="";
$id_ofe_alum="";
$id_alum="";

if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {

    $ofe_alum = $DaoOfertasAlumno->show($_REQUEST['id']);
    $alum = $DaoAlumnos->show($ofe_alum->getId_alum());
    $oferta = $DaoOfertas->show($ofe_alum->getId_ofe());
    $esp = $DaoEspecialidades->show($ofe_alum->getId_esp());
    $nombre_ori = "";
    $primer_ciclo=$DaoOfertasAlumno->getFirstCicloOferta($_REQUEST['id']);
    $ultimo_ciclo=$DaoOfertasAlumno->getLastCicloOferta($_REQUEST['id']);
    
    $id_esp=$ofe_alum->getId_esp();
    $id_ofe_alum=$ofe_alum->getId();
    $id_alum=$ofe_alum->getId_alum();


    if ($ofe_alum->getId_ori()> 0) {
        $ori = $DaoOrientaciones->show($ofe_alum->getId_ori());
        $nombre_ori = $ori->getNombre();
    }
    
    $Opcion_pago="";
    if($ofe_alum->getOpcionPago()==1){
        $Opcion_pago="Plan por materias";
    }else{
        $Opcion_pago="Plan completo";
    }
}
?>


<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1>Acreditar materias</h1>
                </div>
                <?php
                
                if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                    ?>
                    <div class="ciclo_alumno">
                        <table class="table">
                            <thead>
                                <tr><td colspan="8" style="text-align: center;font-size: 11px">Informaci&oacute;n del alumno</td></tr>
                                <tr>
                                    <td>Matricula</td>
                                    <td class="normal"><?php echo $alum->getMatricula() ?></td>
                                    <td>Nombre</td>
                                    <td colspan="3" class="normal"><?php echo $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM() ?></td>
                                    <td>Ciclo actual</td>
                                    <td class="normal"><?php echo $ultimo_ciclo['Clave']; ?></td>
                                </tr>
                                <tr>
                                     <td>Carrera</td>
                                     <td colspan="3" class="normal"><?php echo $esp->getNombre_esp(); ?></td>
                                     <td>Orientaci&oacute;n</td>
                                     <td class="normal"><?php echo $nombre_ori; ?></td>
                                     <td>Nivel</td>
                                     <td colspan="3" class="normal"><?php echo $oferta->getNombre_oferta(); ?></td>
                                </tr>
                                <tr>
                                    <td>Clave</td>
                                    <td style="width:300px;" colspan="2">Materia</td>
                                    <td>Tipo</td>
                                    <td>Nivel</td>
                                    <td>Creditos</td>
                                    <td>Usuario</td>
                                    <td>Acciones</td>
                                </tr>
                            </thead>
                            <tbody>
                               <?php
                               foreach($DaoMateriasAcreditadas->getMateriasAcreditadasByOfeAlumno($_REQUEST['id']) as $k=>$v) {
                                    $ciclo=$DaoCiclos->show($v->getId_ciclo());
                                    $mat_esp = $DaoMateriasEspecialidad->show($v->getId_mat_esp());
                                    $mat = $DaoMaterias->show($mat_esp->getId_mat());
                                    $grado=$DaoGrados->show($mat_esp->getGrado_mat());
                                    $usu_acre = $DaoUsuarios->show($v->getId_usu());
                                    ?>
                                    <tr>
                                        <td><?php echo $mat->getClave_mat()?></td>
                                        <td colspan="2"><?php echo $mat->getNombre()?></td>
                                        <td>Acreditada</td>
                                        <td><?php echo $grado->getGrado()?></td>
                                        <td><?php echo $mat->getCreditos()?></td>
                                        <td><?php echo $usu_acre->getNombre_usu() . " " . $usu_acre->getApellidoP_usu() . " " . $usu_acre->getApellidoM_usu()?></td>
                                        <td>
                                            <button onclick="delete_mat_ac(<?php echo $v->getId()?>)">Eliminar</button>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
            </div>
        </td>
        
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><span onclick="mostrar_box_materias()">Acreditar materia</span></li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<input type="hidden" id="Id_esp" value="<?php echo $id_esp?>"/>
<input type="hidden" id="Id_oferta_alumno" value="<?php echo $id_ofe_alum?>"/>
<input type="hidden" id="Id_alum" value="<?php echo $id_alum?>"/>
<?php
write_footer();