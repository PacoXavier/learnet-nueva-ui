<?php 
require_once('Connections/cnn.php');
require_once('estandares/Fpdf/fpdf.php'); 
require_once('estandares/Fpdf/fpdi.php'); 
require_once('estandares/cantidad_letra.php'); 

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');

// initiate FPDI 
$pdf = new FPDI(); 

// add a page 
$pdf->AddPage(); 

// set the sourcefile 
$pdf->setSourceFile('estandares/factura.pdf'); 

// import page 1 
$tplIdx = $pdf->importPage(1); 

// use the imported page and place it at point 10,10 with a width of 100 mm 
$pdf->useTemplate($tplIdx); 

$query_alum = "SELECT * FROM inscripciones_ulm WHERE Id_ins='".$_REQUEST['id']."'";
$alumn = mysql_query($query_alum, $cnn) or die(mysql_error());
$row_alumn = mysql_fetch_assoc($alumn);

// now write some text above the imported page 
$pdf->SetFont('Arial','B',8); 
$pdf->SetTextColor(0,0,0); 

$pdf->SetXY(47,41.5); 
$pdf->Write(10, utf8_decode(date('Y-m-d'))); 
$pdf->SetXY(168,12); 
$pdf->Write(10, utf8_decode("A - 279")); 
$pdf->SetXY(160,21.5); 
$pdf->Write(10, utf8_decode(date('Y-m-d H:i:s'))); 
$pdf->SetXY(160,31); 
$pdf->Write(10, utf8_decode(date('Y-m-d H:i:s'))); 
$pdf->SetXY(110,62.5); 
$pdf->Write(10, utf8_decode("Christian Alejandro Santos garcia")); 
$pdf->SetXY(110,66); 
$pdf->Write(10, strtoupper(utf8_decode("RFC: DGFSDFSDFSDFSD"))); 
$pdf->SetXY(110,69.5); 
$pdf->Write(10, utf8_decode("Calle: AV. 20 DE NOVIEMBRE 1923")); 
$pdf->SetXY(110,73); 
$pdf->Write(10, utf8_decode("Colonia: Lomas del camichin")); 
$pdf->SetXY(110,76.5); 
$pdf->Write(10, utf8_decode("Ciudad: Guadalajara, Jalisco, México,  C.P. 44720")); 


$pdf->SetXY(55,94); 
$pdf->Write(10, utf8_decode("Tarjeta de Débito")); 

$pdf->SetXY(147,88); 
$pdf->Write(10, utf8_decode("Pago en una sola exhibición")); 

$pdf->SetXY(147,94.5); 
$pdf->Write(10, utf8_decode("Banorte 2451")); 

//Pagos
$precio=1550;
$subtotal=0;
for($i=0;$i<5;$i++){
    $pdf->SetXY(13,115+($i*10)); 
    $pdf->Write(10, utf8_decode("CTE")); 
    $pdf->SetXY(35,115+($i*10)); 
    $pdf->Write(10,"1");
    $pdf->SetXY(50,115+($i*10)); 
    $pdf->Write(10, utf8_decode("Mensualidad")); 
    $pdf->SetXY(73,115+($i*10)); 
    $pdf->Write(10, utf8_decode("Colegiatura Carrera Técnica Ejecución")); 
    $pdf->SetXY(164,115+($i*10)); 
    $pdf->Write(10, "$".  number_format($precio,2)); 
    $pdf->SetXY(185,115+($i*10)); 
    $pdf->Write(10, "$".  number_format($precio,2)); 
    $precio+=100;
    $subtotal+=$precio;
}

//Subtotales
$iva=0;
$total=$subtotal+$iva;
$pdf->SetXY(180,187); 
$pdf->Write(10,"$".  number_format($subtotal,2)); 
$pdf->SetXY(180,193); 
$pdf->Write(10,"$".  number_format($iva,2)); 
$pdf->SetXY(180,199); 
$pdf->Write(10,"$".  number_format($total,2)); 

//Comentarios
$pdf->SetXY(33,213); 
$pdf->MultiCell(150,3 ,utf8_decode("Factura Correspondiente a la colegiatura de Mayo de la Carrera Técnica o numero 6347"),0,'L'); 
$pdf->SetXY(33,220); 
$pdf->Write(10,strtoupper(cantidad_letra($total,2))); 


$pdf->Output('admision_maupro.pdf', 'I'); 
