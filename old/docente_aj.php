<?php
error_log(E_ALL);
require_once('Connections/cnn.php');
require_once('estandares/class_bdd.php');
require_once('estandares/class_usuarios.php');
require_once('estandares/class_docentes.php');
require_once('estandares/class_direcciones.php');

require_once("clases/DaoDocentes.php");
require_once("clases/DaoUsuarios.php");

if (!function_exists("GetSQLValueString")) {

    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
        $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

        $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }

}

mysql_select_db($database_cnn, $cnn);
mysql_query("SET NAMES 'utf8'");

date_default_timezone_set('America/Mexico_City');

function resizeToFile($sourcefile, $dest_x, $dest_y, $targetfile, $jpegqual) {
    setMemoryForImage($sourcefile);
    /* Get the  dimensions of the source picture */
    $picsize = getimagesize("$sourcefile");

    $source_x = $picsize[0];
    $source_y = $picsize[1];
    $source_id = imageCreateFromJPEG("$sourcefile");

    /* Create a new image object (not neccessarily true colour) */

    $target_id = imagecreatetruecolor($dest_x, $dest_y);

    /* Resize the original picture and copy it into the just created image
      object. Because of the lack of space I had to wrap the parameters to
      several lines. I recommend putting them in one line in order keep your
      code clean and readable */


    $target_pic = imagecopyresampled($target_id, $source_id, 0, 0, 0, 0, $dest_x, $dest_y, $source_x, $source_y);

    /* Create a jpeg with the quality of "$jpegqual" out of the
      image object "$target_pic".
      This will be saved as $targetfile */

    imagejpeg($target_id, "$targetfile", $jpegqual);

    return true;
}

function setMemoryForImage($filename) {
    $imageInfo = getimagesize($filename);
    $MB = 1048576;  // number of bytes in 1M
    $K64 = 65536;    // number of bytes in 64K
    $TWEAKFACTOR = 2.5;  // Or whatever works for you
    $memoryNeeded = round(( $imageInfo[0] * $imageInfo[1] * $imageInfo['bits'] * $imageInfo['channels'] / 8 + $K64
            ) * $TWEAKFACTOR
    );
    //ini_get('memory_limit') only works if compiled with "--enable-memory-limit" also
    //Default memory limit is 8MB so well stick with that. 
    //To find out what yours is, view your php.ini file.
    $memoryLimit = 8 * $MB;
    if (function_exists('memory_get_usage') &&
            memory_get_usage() + $memoryNeeded > $memoryLimit) {
        $newLimit = $memoryLimitMB + ceil(( memory_get_usage() + $memoryNeeded - $memoryLimit
                        ) / $MB
        );
        ini_set('memory_limit', $newLimit . 'M');
        return true;
    } else {
        return false;
    }
}

if ($_GET['action'] == "upload_image") {

    if (count($_FILES) > 0) {
        $type = substr($_FILES["file"]["name"], strpos($_FILES["file"]["name"], ".") + 1);
        if (($type == "gif" || $type == "jpeg" || $type == "png" || $type == "jpg")) {

            if ($_FILES["file"]["error"] > 0) {

                echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
            } else {
                $query_Archivos = "SELECT * FROM Docentes WHERE Id_docen=" . $_GET['Id_usu'];
                $Archivos = mysql_query($query_Archivos, $cnn) or die(mysql_error());
                $row_Archivos = mysql_fetch_assoc($Archivos);
                $totalRows_Archivos = mysql_num_rows($Archivos);
                if (file_exists("files/" . $row_Archivos['Img_docen'] . ".jpg")) {
                    unlink("files/" . $row_Archivos['Img_docen'] . ".jpg");
                }
                $class_docentes = new class_docentes($_GET['Id_usu']);
                do {
                    $key = $class_docentes->generarKey();
                    $query_Archivos = "SELECT * FROM Docentes WHERE Img_docen='" . $key . "'";
                    $Archivos = mysql_query($query_Archivos, $cnn) or die(mysql_error());
                    $row_Archivos = mysql_fetch_assoc($Archivos);
                    $totalRows_Archivos = mysql_num_rows($Archivos);
                } while ($totalRows_Archivos > 0);

                $sql_nueva_entrada = sprintf("UPDATE Docentes SET Img_docen=%s WHERE Id_docen=%s", 
                        GetSQLValueString($key, "text"), 
                        GetSQLValueString($_GET['Id_usu'] , "int"));
                $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

                move_uploaded_file($_FILES["file"]["tmp_name"], "files/" . $key . ".jpg");
                //header("Location: contabilidad.php");
                //se usa cuando se suben archivos por carga de archivos normales
                // print_r($_FILES);
                $sourcefile = 'files/' . $key . '.jpg';
                $picsize = getimagesize($sourcefile);

                $source_x = $picsize[0] * 1;
                $source_y = $picsize[1] * 1;

                //modificar el tamaño de la imagen
                $result1 = resizeToFile($sourcefile, 300, 300 * $source_y / $source_x, $sourcefile, 80);

                echo $key;
            }
        } else {
            echo "Invalid file: " . $type;
        }
    } elseif (isset($_GET['action'])) {
        if (($_GET['TypeFile'] == "image/gif" || $_GET['TypeFile'] == "image/jpeg" || $_GET['TypeFile'] == "image/png")) {
            if ($_FILES["file"]["error"] > 0) {

                echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
            } else {

                $query_Archivos = "SELECT * FROM Docentes WHERE Id_docen=" . $_GET['Id_usu'];
                $Archivos = mysql_query($query_Archivos, $cnn) or die(mysql_error());
                $row_Archivos = mysql_fetch_assoc($Archivos);
                $totalRows_Archivos = mysql_num_rows($Archivos);
                if (file_exists("files/" . $row_Archivos['Img_docen'] . ".jpg")) {
                    unlink("files/" . $row_Archivos['Img_docen'] . ".jpg");
                }
                $class_docentes = new class_docentes($_GET['Id_usu']);
                do {
                    $key = $class_docentes->generarKey();
                    $query_Archivos = "SELECT * FROM Docentes WHERE Img_docen='" . $key . "'";
                    $Archivos = mysql_query($query_Archivos, $cnn) or die(mysql_error());
                    $row_Archivos = mysql_fetch_assoc($Archivos);
                    $totalRows_Archivos = mysql_num_rows($Archivos);
                } while ($totalRows_Archivos > 0);

                $sql_nueva_entrada = sprintf("UPDATE Docentes SET Img_docen=%s WHERE Id_docen=%s", 
                        GetSQLValueString($key, "text"), 
                        GetSQLValueString($_GET['Id_usu'] , "int"));
                $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());


                if (isset($_GET['base64'])) {
                    // If the browser does not support sendAsBinary ()
                    $content = base64_decode(file_get_contents('php://input'));
                } else {
                    $content = file_get_contents('php://input');
                }
                file_put_contents('files/' . $key . '.jpg', $content);

                $sourcefile = 'files/' . $key . '.jpg';
                $picsize = getimagesize($sourcefile);

                $source_x = $picsize[0] * 1;
                $source_y = $picsize[1] * 1;

                //modificar el tamaño de la imagen
                $result1 = resizeToFile($sourcefile, 300, 300 * $source_y / $source_x, $sourcefile, 80);

                echo $key;
            }
        } else {
            //print_r($_FILES);
            //print_r($_GET);
            echo "Invalid file: " . $_GET['TypeFile'];
        }
    }
}




if ($_POST['action']=="update_pag"){
update_page($_POST['Id_usu']);
}

function update_page($id_usu) {
global $cnn, $database_cnn;
?>
<table id="tabla">
<?php
if ($id_usu > 0) {

    $class_docentes = new class_docentes($id_usu);
    $usu = $class_docentes->get_docente();
    if ($usu['Id_dir'] > 0) {
      $class_direcciones = new class_direcciones($usu['Id_dir']);
      $dir = $class_direcciones->get_dir();
    }
}
?>
    <input type="hidden" id="Id_dir" value="<?php echo $usu['Id_dir'] ?>"/>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <div class="foto_alumno" 
<?php if (strlen($usu['Img_docen']) > 0) { ?> 
                             style="background-image:url(files/<?php echo $usu['Img_docen'] ?>.jpg) <?php } ?>">
                    </div>
                    <h1><?php echo $usu['Nombre_docen'] . " " . $usu['ApellidoP_docen'] . " " . $usu['ApellidoM_docen'] ?></h1>
                </div>
                <div class="seccion">
                    <h2>Datos Personales</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Clave<br><input type="text" value="<?php echo $usu['Clave_docen'] ?>" id="clave_usu" readonly="readonly"/></li>
                        <li>Nombre<br><input type="text" value="<?php echo $usu['Nombre_docen'] ?>" id="nombre_usu"/></li>
                        <li>Apellido Paterno<br><input type="text" value="<?php echo $usu['ApellidoP_docen'] ?>" id="apellidoP_usu"/></li>
                        <li>Apellido Materno<br><input type="text" value="<?php echo $usu['ApellidoM_docen'] ?>" id="apellidoM_usu"/></li>
                        <li>Email<br><input type="email" id="email_usu" value="<?php echo $usu['Email_docen'] ?>"/></li>
                        <li>Tel&eacute;fono Casa<br><input type="tel" id="tel_usu" value="<?php echo $usu['Telefono_docen'] ?>"/></li>
                        <li>Tel&eacute;fono Movil<br><input type="tel" id="cel_usu" value="<?php echo $usu['Cel_docen'] ?>"/></li>
                        <li>Ultimo Grado de Estudios<br>
                            <select id="escolaridad">
                                <option value="0"></option>
                                <option value="1" <?php if ($usu['NivelEst_docen'] == 1) { ?> selected="selected"<?php } ?>>Primaria</option>
                                <option value="2" <?php if ($usu['NivelEst_docen'] == 2) { ?> selected="selected"<?php } ?>>Secundaria</option>
                                <option value="3" <?php if ($usu['NivelEst_docen'] == 3) { ?> selected="selected"<?php } ?>>Preparatoria</option>
                                <option value="4" <?php if ($usu['NivelEst_docen'] == 4) { ?> selected="selected"<?php } ?>>Universidad</option>
                                <option value="5" <?php if ($usu['NivelEst_docen'] == 5) { ?> selected="selected"<?php } ?>>Posgrado</option>
                            </select>
                        </li>
                        <li>Tiempo completo<br><input type="checkbox" id="tiempo_completo" <?php if ($usu['Tiempo_completo'] == 1) { ?> checked="checked" <?php } ?> /></li>
                    </ul>
                </div>
                 <div class="seccion">
                      <h2>Direcci&oacute;n</h2>
                      <span class="linea"></span>
                      <ul class="form">
                        <li>Calle<br><input type="text" id="calle_docen" value="<?php echo $dir['Calle_dir'] ?>"/></li>
                        <li>N&uacute;mero exterior<br><input type="text" id="numExt_docen" value="<?php echo $dir['NumExt_dir'] ?>"/></li>
                        <li>N&uacute;mero interior<br><input type="text" id="numInt_docen" value="<?php echo $dir['NumInt_dir'] ?>"/></li>
                        <li>Colonia<br><input type="text" id="colonia_docen" value="<?php echo $dir['Colonia_dir'] ?>"/></li>
                        <li>C&oacute;digo postal<br><input type="text" id="cp_docen" value="<?php echo $dir['Cp_dir'] ?>"/></li>
                        <li>Ciudad<br><input type="text" id="ciudad_docen" value="<?php echo $dir['Ciudad_dir'] ?>"/></li>
                        <li>Estado<br><input type="text" id="estado_docen" value="<?php echo $dir['Estado_dir'] ?>"/></li>
                      </ul>
                    </div>
                <div class="seccion">
                    <h2>Datos de Emergencia</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Tipo de sangre<br><input type="text"  id="tipo_sangre" value="<?php echo $usu['Tipo_sangre'] ?>"/></li>
                        <li>Nombre del contacto<br><input type="text" id="nombre_contacto" value="<?php echo $usu['Contacto']['Nombre'] ?>"/></li>
                        <li>Parentesco<br><input type="text" id="parentesco_contacto" value="<?php echo $usu['Contacto']['Parentesco'] ?>"/></li>
                        <li>Direcci&oacute;n<br><input type="text" id="direccion_contacto" value="<?php echo $usu['Contacto']['Direccion'] ?>"/></li>
                        <li>Tel&eacute;fono Casa<br><input type="text" id="tel_contacto" value="<?php echo $usu['Contacto']['Tel_casa'] ?>"/></li>
                        <li>Tel&eacute;fono Oficina<br><input type="text" id="telOfic_contacto" value="<?php echo $usu['Contacto']['Tel_ofi'] ?>"/></li>
                        <li>Celular<br><input type="text" id="celular_contacto" value="<?php echo $usu['Contacto']['Cel'] ?>"/></li>
                        <li>Email<br><input type="text" id="email_contacto" value="<?php echo $usu['Contacto']['Email'] ?>"/></li>
                    </ul>
                    <input type="hidden" id="Id_contacto" value="<?php echo $usu['Contacto']['Id_cont'] ?>"/>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <li>Alergias<br><textarea  id="alergias_contacto"><?php echo $usu['Alergias'] ?></textarea></li>
                        <li>Enfermedades cr&oacute;nicas<br><textarea  id="cronicas_contacto"><?php echo $usu['Enfermedades_cronicas'] ?></textarea></li>
                        <li>Pre-escripciones medicas<br><textarea  id="preinscripciones_medicas_contacto"><?php echo $usu['Preinscripciones_medicas'] ?></textarea></li>
                    </ul>
                </div>
                <?php
        if ($id_usu > 0) {
      ?>
                 <div class="seccion">
                    <h2>Nivel Tabulador</h2>
                    <span class="linea"></span>
                    <table id="list_usu">
                        <thead>
                            <tr>
                                <td>Fecha</td>
                                <td>Nombre</td>
                                <td>Puntos</td>
                                <td>Profesor A</td>
                                <td>Profesor B</td>
                                <td>Profesor C</td>
                                <td>Profesor D</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
              $query_evaluacion = "SELECT * FROM Evaluacion_docente WHERE Id_doc=".$id_usu." ORDER  BY Id_eva DESC";
              $evaluacion = mysql_query($query_evaluacion, $cnn) or die(mysql_error());
              $row_evaluacion= mysql_fetch_array($evaluacion);
              $totalRows_evaluacion = mysql_num_rows($evaluacion);
              if($totalRows_evaluacion>0){
                    $query_profesor_a = "SELECT * FROM Niveles_docentes WHERE Id_nivel=".$row_evaluacion['Profesor_a'];
                    $profesor_a = mysql_query($query_profesor_a, $cnn) or die(mysql_error());
                    $row_profesor_a = mysql_fetch_array($profesor_a);
                    
                    $query_profesor_b = "SELECT * FROM Niveles_docentes WHERE Id_nivel=".$row_evaluacion['Profesor_b'];
                    $profesor_b = mysql_query($query_profesor_b, $cnn) or die(mysql_error());
                    $row_profesor_b = mysql_fetch_array($profesor_b);
                    
                    $query_profesor_c = "SELECT * FROM Niveles_docentes WHERE Id_nivel=".$row_evaluacion['Profesor_c'];
                    $profesor_c = mysql_query($query_profesor_c, $cnn) or die(mysql_error());
                    $row_profesor_c = mysql_fetch_array($profesor_c);
                    
                    $query_profesor_d = "SELECT * FROM Niveles_docentes WHERE Id_nivel=".$row_evaluacion['Profesor_d'];
                    $profesor_d = mysql_query($query_profesor_d, $cnn) or die(mysql_error());
                    $row_profesor_d = mysql_fetch_array($profesor_d);
            ?>
                                <tr>
                                    <td><?php echo $row_evaluacion['Fecha_evaluacion']?></td>
                                    <td><?php echo $usu['Nombre_docen']." ".$usu['ApellidoP_docen']."  ".$usu['ApellidoM_docen']?></td>
                                    <td><?php echo $row_evaluacion['Puntos_totales']?></td>
                                    <td><?php echo $row_profesor_a['Nombre_nivel']?></td>
                                    <td><?php echo $row_profesor_b['Nombre_nivel']?></td>
                                    <td><?php echo $row_profesor_c['Nombre_nivel']?></td>
                                    <td><?php echo $row_profesor_d['Nombre_nivel']?></td>
                                </tr>
                            <?php
            }
            ?>
                        </tbody>
                    </table>
                </div>
                 <?php
        }
       ?>
                <button id="button_ins" onclick="save_usu()">Guardar</button>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <table id="usu_login">
<?php
$class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
$usu = $class_usuarios->get_usu();
$perm=array();
  foreach($usu['Permisos'] as $k=>$v){
    $perm[$v['Id_perm']]=1;
}
?>
                    <tr>
                        <td><div class="nom_usu"><div id="img_usu" 
<?php if (strlen($usu['Img_usu']) > 0) { ?> style="background-image:url(files/<?php echo $usu['Img_usu'] ?>.jpg) <?php } ?>"></div>
<?php echo $usu['Nombre_usu'] . " " . $usu['ApellidoP_usu'] . " " . $usu['ApellidoM_usu'] ?>
                                <img src="images/linea_uno.png" alt="linea_uno" width="185" height="5" class="lineas"/></div>
                        </td>
                        <td>
                            <div class="opcion">
                                <a href="perfil.php?id=<?php echo $usu['Id_usu'] ?>">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Mi perfil</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/>
                            </div>
                        </td>
                        <td><div class="opcion">
                                <a href="logout.php">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Salir</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/></div>
                        </td>
                    </tr>
                </table>
                <h2><img src="images/flecha_blue.png" alt="flecha_blue" width="6" height="11" />Opciones Docente</h2>
                <ul>
                    <li><a href="docente.php" class="link">Nuevo</a></li>
                                                      <?php
                                                      if ($id_usu > 0) {
                                                          ?>
                        <li><span onclick="box_documents()">Documentos</span></li>
                        <li><span onclick="box_pass()">Cambiar contrase&ntilde;a</span></li>
                        <li><span onclick="mostrarFinder()">A&ntilde;adir fotografia</span></li>
                        <?php
                        if(isset($perm['49'])){
                        ?>
                            <li><a href="evaluaciones_docente.php?id=<?php echo $id_usu;?>">Evaluar Docente</span></li>
                          <?php
                        }
                        if(isset($perm['48'])){
                        ?>
                        <li><a href="disponibilidad_docente.php?id=<?php echo $id_usu;?>">Disponibilidad Docente</span></li>
                        <?php
                        }
                        if(isset($perm['50'])){
                        ?>
                        <li><a href="ligar_materias.php?id=<?php echo $id_usu;?>">Materias Docente</span></li>
                        
                        <?php
                        }

}
?>
                </ul>
                <input type="file" id="files" name="files" multiple="">
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_doc" value="<?php echo $id_usu ?>"/>
<?php
}

/**/


if ($_POST['action']=="box_pass"){
$class_docentes = new class_docentes($_POST['Id_doc']);
$docente = $class_docentes->get_docente();
?>
<div id="box_emergente"><h1>Cambiar contrase&ntilde;a</h1>
    <span class="linea"></span>
    <ul class="form" id="list_datos_acceso">
        <li>Contrase&ntilde;a<br><input type="password" id="pass"/></li>
        <li>Confirmar contrase&ntilde;a:<br><input type="password" id="conf"/></li>
    </ul>
    <p><button onclick="save_pass()">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
</div>
<?php
}



if ($_POST['action']=="save_pass"){
    $class_bdd = new class_bdd();
    $tipo_error = array();
    if ($_POST['pass']==$_POST['conf']){
        if (strlen($_POST['pass'])>6){
        $tipo_error['tipo'] = "ok";
        $sql_nueva_entrada = sprintf("UPDATE  Docentes SET Pass_docen=%s WHERE Id_docen=%s", 
                GetSQLValueString($class_bdd->hashPassword($_POST['pass']) , "text"),
                GetSQLValueString($_POST['Id_doc'] , "int"));
        $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
        
            //Crear historial
        $DaoDocentes= new DaoDocentes();
        $DaoUsuarios= new DaoUsuarios();
        $doc=$DaoDocentes->show($_POST['Id_doc']);
        $TextoHistorial="Cambia la contraseña pa el  docente ".$doc->getNombre_docen()." ".$doc->getApellidoP_docen()." ".$doc->getApellidoM_docen();
        $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Docentes");
    
        } else {
        $tipo_error['tipo'] = "password_security";
        }
    } else {
    $tipo_error['tipo'] = "password_match";
    }
    echo json_encode($tipo_error);
}




if ($_POST['action']=="box_documents"){
$class_docentes = new class_docentes($_POST['Id_doc']);
$docente = $class_docentes->get_docente();
?>
<div id="box_emergente"><h1>Documentos</h1>
    <table class="files_ofe">
        <thead>
        <td>Documento</td>
        <td>Original</td>
        <td>Copia</td>
        </thead>
        <tbody>
            <tr>
                <td>RFC</td>
                <td><input type="checkbox" <?php if ($docente['Doc_RFC'] > 0) { ?> checked="checked" <?php } ?> id="doc_RFC"></td>
                <td><input type="checkbox" <?php if ($docente['copy_RFC'] > 0) { ?> checked="checked" <?php } ?> id="copy_RFC" ></td>
            </tr>
            <tr>
                <td>CURP</td>
                <td><input type="checkbox" <?php if ($docente['Doc_CURP'] > 0) { ?> checked="checked" <?php } ?> id="doc_CURP"></td>
                <td><input type="checkbox" <?php if ($docente['copy_CURP'] > 0) { ?> checked="checked" <?php } ?> id="copy_CURP" ></td>
            </tr>
            <tr>
                <td>Curr&iacute;culum</td>
                <td><input type="checkbox"  <?php if ($docente['Doc_Curriculum'] > 0) { ?> checked="checked" <?php } ?> id="doc_Curriculum"></td>
                <td><input type="checkbox" <?php if ($docente['copy_Curriculum'] > 0) { ?> checked="checked" <?php } ?> id="copy_Curriculum" ></td>
            </tr>
            <tr>
                <td>Comp. Domicilio</td>
                <td><input type="checkbox"  <?php if ($docente['Doc_CompDomicilio'] > 0) { ?> checked="checked" <?php } ?> id="doc_CompDomicilio"></td>
                <td><input type="checkbox" <?php if ($docente['copy_CompDomicilio'] > 0) { ?> checked="checked" <?php } ?> id="copy_CompDomicilio" ></td>
            </tr>
            <tr>
                <td>Comp. Estudios</td>
                <td><input type="checkbox"  <?php if ($docente['Doc_CompEstudios'] > 0) { ?> checked="checked" <?php } ?> id="doc_CompEstudios"></td>
                <td><input type="checkbox" <?php if ($docente['copy_CompEstudios'] > 0) { ?> checked="checked" <?php } ?> id="copy_CompEstudios" ></td>
            </tr>
            <tr>
                <td>Contrato</td>
                <td><input type="checkbox"  <?php if ($docente['Doc_Contrato'] > 0) { ?> checked="checked" <?php } ?> id="doc_Contrato"></td>
                <td><input type="checkbox" <?php if ($docente['copy_Contrato'] > 0) { ?> checked="checked" <?php } ?> id="copy_Contrato" ></td>
            </tr>
        </tbody>
    </table>
    <p><button onclick="save_documentos_doc()">Guardar</button><button onclick="ocultar_error_layer()">Cancelar</button></p>
</div>
<?php
}




if ($_POST['action']=="save_documentos_doc"){
//RFC
$sql_nueva_entrada = sprintf("UPDATE Docentes SET Doc_RFC=%s WHERE Id_docen=%s", 
        GetSQLValueString($_POST['doc_RFC'], "int"), 
        GetSQLValueString($_POST['Id_doc'], "int"));
$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

$sql_nueva_entrada = sprintf("UPDATE Docentes SET copy_RFC=%s WHERE Id_docen=%s", 
        GetSQLValueString($_POST['copy_RFC'], "int"),
        GetSQLValueString($_POST['Id_doc'], "int"));
$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

//CURP	
$sql_nueva_entrada = sprintf("UPDATE Docentes SET Doc_CURP=%s WHERE Id_docen=%s", 
        GetSQLValueString($_POST['doc_CURP'], "int"),
        GetSQLValueString($_POST['Id_doc'], "int"));
$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

$sql_nueva_entrada = sprintf("UPDATE Docentes SET copy_CURP=%s WHERE Id_docen=%s", 
        GetSQLValueString($_POST['copy_CURP'], "int"), 
        GetSQLValueString($_POST['Id_doc'], "int"));
$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

//Curriculum
$sql_nueva_entrada = sprintf("UPDATE Docentes SET Doc_Curriculum=%s WHERE Id_docen=%s", 
        GetSQLValueString($_POST['doc_Curriculum'], "int"), 
        GetSQLValueString($_POST['Id_doc'], "int"));
$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

$sql_nueva_entrada = sprintf("UPDATE Docentes SET copy_Curriculum=%s WHERE Id_docen=%s", 
        GetSQLValueString($_POST['copy_Curriculum'], "int"), 
        GetSQLValueString($_POST['Id_doc'], "int"));
$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

//CompDomicilio
$sql_nueva_entrada = sprintf("UPDATE Docentes SET Doc_CompDomicilio=%s WHERE Id_docen=%s", 
        GetSQLValueString($_POST['doc_CompDomicilio'], "int"), 
        GetSQLValueString($_POST['Id_doc'], "int"));
$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

$sql_nueva_entrada = sprintf("UPDATE Docentes SET copy_CompDomicilio=%s WHERE Id_docen=%s", 
        GetSQLValueString($_POST['copy_CompDomicilio'], "int"), 
        GetSQLValueString($_POST['Id_doc'], "int"));
$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

//Contrato
$sql_nueva_entrada = sprintf("UPDATE Docentes SET Doc_Contrato=%s WHERE Id_docen=%s", 
        GetSQLValueString($_POST['doc_Contrato'], "int"), 
        GetSQLValueString($_POST['Id_doc'], "int"));
$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

$sql_nueva_entrada = sprintf("UPDATE Docentes SET copy_Contrato=%s WHERE Id_docen=%s", 
        GetSQLValueString($_POST['copy_Contrato'], "int"), 
        GetSQLValueString($_POST['Id_doc'], "int"));
$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

//CompEstudios
$sql_nueva_entrada = sprintf("UPDATE Docentes SET Doc_CompEstudios=%s WHERE Id_docen=%s", 
        GetSQLValueString($_POST['doc_CompEstudios'], "int"),
        GetSQLValueString($_POST['Id_doc'], "int"));
$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

$sql_nueva_entrada = sprintf("UPDATE Docentes SET copy_CompEstudios=%s WHERE Id_docen=%s", 
        GetSQLValueString($_POST['copy_CompEstudios'], "int"), 
        GetSQLValueString($_POST['Id_doc'], "int"));
$Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());

    //Crear historial
    $DaoDocentes= new DaoDocentes();
    $DaoUsuarios= new DaoUsuarios();
    $doc=$DaoDocentes->show($_POST['Id_doc']);
    $TextoHistorial="Edita los documentos para el docente ".$doc->getNombre_docen()." ".$doc->getApellidoP_docen()." ".$doc->getApellidoM_docen();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Docentes");
}


if ($_POST['action'] == "save_usu") {
    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
    $usu = $class_usuarios->get_usu();
    
    $DaoDocentes= new DaoDocentes();
    $resp = array();
    if ($_POST['Id_doc']>0){
    $sql_nueva_entrada = sprintf("UPDATE Docentes SET Nombre_docen=%s, ApellidoP_docen=%s, ApellidoM_docen=%s, Email_docen=%s, Telefono_docen=%s, Cel_docen=%s, NivelEst_docen=%s,
                                      Tipo_sangre=%s, Alergias=%s,Enfermedades_cronicas=%s, Preinscripciones_medicas=%s,Tiempo_completo=%s  WHERE Id_docen=%s", 
            GetSQLValueString($_POST['nombre_usu'], "text"), 
            GetSQLValueString($_POST['apellidoP_usu'], "text"), 
            GetSQLValueString($_POST['apellidoM_usu'], "text"), 
            GetSQLValueString($_POST['email_usu'], "text"), 
            GetSQLValueString($_POST['tel_usu'], "text"), 
            GetSQLValueString($_POST['cel_usu'], "text"), 
            GetSQLValueString($_POST['escolaridad'], "int"), 
            GetSQLValueString($_POST['tipo_sangre'], "text"),
            GetSQLValueString($_POST['alergias_contacto'], "text"), 
            GetSQLValueString($_POST['cronicas_contacto'], "text"), 
            GetSQLValueString($_POST['preinscripciones_medicas_contacto'], "text"), 
            GetSQLValueString($_POST['tiempo_completo'], "int"), 
            GetSQLValueString($_POST['Id_doc'], "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    
    //Crear historial
    $DaoUsuarios= new DaoUsuarios();
    $doc=$DaoDocentes->show($_POST['Id_doc']);
    $TextoHistorial="Actualiza los datos para el docente ".$doc->getNombre_docen()." ".$doc->getApellidoP_docen()." ".$doc->getApellidoM_docen();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Docentes");
   
    if ($_POST['Id_contacto'] > 0) {
    $sql_nueva_entrada = sprintf("UPDATE Contactos SET Nombre=%s, Parentesco=%s, Direccion=%s, Tel_casa=%s, Tel_ofi=%s, Cel=%s, Email=%s WHERE Id_cont=%s", 
            GetSQLValueString($_POST['nombre_contacto'], "text"), 
            GetSQLValueString($_POST['parentesco_contacto'], "text"), 
            GetSQLValueString($_POST['direccion_contacto'], "text"), 
            GetSQLValueString($_POST['tel_contacto'], "text"),
            GetSQLValueString($_POST['telOfic_contacto'], "text"), 
            GetSQLValueString($_POST['celular_contacto'], "text"), 
            GetSQLValueString($_POST['email_contacto'], "text"), 
            GetSQLValueString($_POST['Id_contacto'], "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    } elseif (strlen($_POST['nombre_contacto']) > 0 && strlen($_POST['parentesco_contacto']) > 0 && strlen($_POST['direccion_contacto']) > 0) {
    $sql_nueva_entrada = sprintf("INSERT INTO Contactos (Nombre, Parentesco, Direccion,  Tel_casa,  Tel_ofi, Cel,  Email, IdRel_con, TipoRel_con) VALUES (%s, %s,%s, %s,%s, %s,%s, %s,%s);", 
            GetSQLValueString($_POST['nombre_contacto'], "text"), 
            GetSQLValueString($_POST['parentesco_contacto'], "text"), 
            GetSQLValueString($_POST['direccion_contacto'], "text"), 
            GetSQLValueString($_POST['tel_contacto'], "text"), 
            GetSQLValueString($_POST['telOfic_contacto'], "text"), 
            GetSQLValueString($_POST['celular_contacto'], "text"), 
            GetSQLValueString($_POST['email_contacto'], "text"), 
            GetSQLValueString($_POST['Id_doc'] , "int"), 
            GetSQLValueString("docente", "text"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    }


    $id_usu = $_POST['Id_doc'];
    $resp['tipo'] = "update";
    $resp['id'] = $id_usu;
    } else {

    $sql_nueva_entrada = sprintf("INSERT INTO Docentes (Nombre_docen, ApellidoP_docen, ApellidoM_docen, Clave_docen, Email_docen, Telefono_docen, Cel_docen, 
                                          NivelEst_docen,Alta_docent, Tipo_sangre, Alergias,Enfermedades_cronicas, Preinscripciones_medicas,Id_plantel,Tiempo_completo) VALUES (%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s,%s,%s);", 
            GetSQLValueString($_POST['nombre_usu'], "text"), 
            GetSQLValueString($_POST['apellidoP_usu'], "text"), 
            GetSQLValueString($_POST['apellidoM_usu'], "text"),
            GetSQLValueString($DaoDocentes->generarClave(), "text"), 
            GetSQLValueString($_POST['email_usu'], "text"), 
            GetSQLValueString($_POST['tel_usu'], "text"), 
            GetSQLValueString($_POST['cel_usu'], "text"), 
            GetSQLValueString($_POST['escolaridad'], "int"), 
            GetSQLValueString(date('Y-m-d H:i:s'), "date"), 
            GetSQLValueString($_POST['tipo_sangre'], "text"), 
            GetSQLValueString($_POST['alergias_contacto'], "text"),
            GetSQLValueString($_POST['cronicas_contacto'], "text"),
            GetSQLValueString($_POST['preinscripciones_medicas_contacto'], "text"),
            GetSQLValueString($usu['Id_plantel'], "int"),
            GetSQLValueString($_POST['tiempo_completo'], "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    $id_usu = mysql_insert_id($cnn);
    
    $DaoUsuarios= new DaoUsuarios();
    $doc=$DaoDocentes->show($id_usu);
    $TextoHistorial="Añade como nuevo docente a ".$doc->getNombre_docen()." ".$doc->getApellidoP_docen()." ".$doc->getApellidoM_docen();
    $DaoUsuarios->capturarHistorialUsuario($TextoHistorial,"Docentes");

    if (strlen($_POST['nombre_contacto']) > 0 && strlen($_POST['parentesco_contacto']) > 0 && strlen($_POST['direccion_contacto']) > 0) {
    $sql_nueva_entrada = sprintf("INSERT INTO Contactos (Nombre, Parentesco, Direccion,  Tel_casa,  Tel_ofi, Cel,  Email, IdRel_con, TipoRel_con) VALUES (%s, %s,%s, %s,%s, %s,%s, %s,%s);", 
            GetSQLValueString($_POST['nombre_contacto'], "text"), 
            GetSQLValueString($_POST['parentesco_contacto'], "text"), 
            GetSQLValueString($_POST['direccion_contacto'], "text"), 
            GetSQLValueString($_POST['tel_contacto'], "text"), 
            GetSQLValueString($_POST['telOfic_contacto'], "text"), 
            GetSQLValueString($_POST['celular_contacto'], "text"), 
            GetSQLValueString($_POST['email_contacto'], "text"), 
            GetSQLValueString($id_usu, "int"), 
            GetSQLValueString("docente", "text"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    }

    $resp['tipo'] = "new";
    $resp['id'] = $id_usu;

    }
    
    if ($_POST['Id_dir']>0){
   
    $sql_nueva_entrada = sprintf("UPDATE direcciones_uml SET  Calle_dir=%s, NumExt_dir=%s, NumInt_dir=%s, Colonia_dir=%s,  Ciudad_dir=%s, Estado_dir=%s, Cp_dir=%s  WHERE Id_dir=%s",
            GetSQLValueString($_POST['calle_docen'], "text"),
            GetSQLValueString($_POST['numExt_docen'], "text"),
            GetSQLValueString($_POST['numInt_docen'], "text"),
            GetSQLValueString($_POST['colonia_docen'], "text"),
            GetSQLValueString($_POST['ciudad_docen'], "text"), 
            GetSQLValueString($_POST['estado_docen'], "text"), 
            GetSQLValueString($_POST['cp_docen'], "text"), 
            GetSQLValueString($_POST['Id_dir'], "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    $id_dir = $_POST['Id_dir'];

    } elseif(strlen($_POST['calle_docen']) > 0 && strlen($_POST['numExt_docen']) > 0 && strlen($_POST['ciudad_docen']) > 0) {
    
    $sql_nueva_entrada = sprintf("INSERT INTO direcciones_uml (Calle_dir,NumExt_dir, NumInt_dir ,Colonia_dir, Ciudad_dir, Estado_dir, Cp_dir,IdRel_dir,TipoRel_dir) VALUES (%s, %s,%s, %s, %s, %s, %s, %s, %s);", 
            GetSQLValueString($_POST['calle_docen'], "text"),
            GetSQLValueString($_POST['numExt_docen'], "text"),
            GetSQLValueString($_POST['numInt_docen'], "text"),
            GetSQLValueString($_POST['colonia_docen'], "text"),
            GetSQLValueString($_POST['ciudad_docen'], "text"), 
            GetSQLValueString($_POST['estado_docen'], "text"), 
            GetSQLValueString($_POST['cp_docen'], "text"), 
            GetSQLValueString($id_usu, "int"), 
            GetSQLValueString('docen', "text"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
    $id_dir = mysql_insert_id($cnn);
    }
   if($id_dir>0){
   $sql_nueva_entrada = sprintf("UPDATE Docentes SET Id_dir=%s WHERE Id_docen=%s",
            GetSQLValueString($id_dir, "int"), 
            GetSQLValueString($id_usu, "int"));
    $Result1 = mysql_query($sql_nueva_entrada, $cnn) or die(mysql_error());
   }
   echo json_encode($resp);


}


