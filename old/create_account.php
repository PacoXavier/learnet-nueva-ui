<?php
require_once('Connections/cnn.php');
require_once('clases/DaoPlanteles.php');
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$DaoPlanteles= new DaoPlanteles();
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_cnn, $cnn);
mysql_query ("SET NAMES 'utf8'");
date_default_timezone_set('America/Mexico_City');

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="es" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="es" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="es" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="es" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<html lang="es" class="no-js">

<!--<![endif]-->
<head>
<meta charset="utf-8">
<!--[if IE]><![endif]--> 
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Login</title>
<meta name="description" content="">
<meta name="keywords" content="" />
<meta name="author" content="GeneraWeb">
<meta name="viewport" content="width=980, initial-scale=1.0">

<!-- !CSS -->
<link rel="stylesheet" href="css/create_account.css?v=2011.5.5.13.35">
<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
</head>
<!-- !Body -->
<body>
	<div id="errorLayer"></div>
	<div id="container">
		<div id="contenido">
		     <div id="box_login">
                         <?php
                         $imgLogo="";
                         $Nombre="";
                         $server=$_SERVER["SERVER_NAME"];
                         if(strpos($server,"www")!==false){
                            $server=substr($server,strpos($server,"www")+4);
                         }
                         foreach($DaoPlanteles->showAll() as $plantel){
                             if($server==$plantel->getDominio()){
                                 $imgLogo='src="files/'.$plantel->getId_img_logo().'.jpg"';
                                 $Nombre=$plantel->getNombre_plantel();
                             }
                         }
                         
                         ?>
		          <p><img <?php echo $imgLogo?>></p>
		          <h1>Bienvenido</h1>
		    <?php
			if(strlen($_REQUEST['key'])>0){
				$query_usuarios_ulm = "SELECT * FROM usuarios_ulm WHERE Recover_usu='".$_REQUEST['key']."'";
				$usuarios_ulm = mysql_query($query_usuarios_ulm, $cnn) or die(mysql_error());
				$row_usuarios_ulm = mysql_fetch_assoc($usuarios_ulm);
				$totalRows_usuarios_ulm = mysql_num_rows($usuarios_ulm);
				if($totalRows_usuarios_ulm>0){
				?>
		          <p>Bienvenido al sistema <?php echo $Nombre?>, por favor elige una contrase&ntilde;a para iniciar sesi&oacute;n como  
		          <?php echo $row_usuarios_ulm['Email_usu']?></p>
		          <div class="box_pass"><input type="password" id="pass"  autocomplete="off"></div>
		          <div class="box_pass"><input type="password" id="confir"  autocomplete="off"></div>
		          <p><button id="buttonLogin" onclick="reset_password()">Reestablecer</button></p>
		          <p class="error"></p>
		          <?php
		          }else{
			       ?>
			       <p class="error">La llave proporcionada no es v&aacute;lida <a href="index.php">regresar</a></p>
			       <?php  
		          }
		       }else{
			       ?>
			       <p class="error">La llave proporcionada no es v&aacute;lida <a href="index.php">regresar</a></p>
			       <?php
		       }
		          ?>
		     </div>
		</div>
		<!--!/#contenido -->
		<div id="box_img_foot"><img src="images/img_foot.png" alt="img_foot" width="1187" height="21">
			<p>&copy; <?php echo date('Y')?> <?php echo $Nombre;?></p>
		</div>
	</div>
	<input type="hidden" id="key" value="<?php echo $_REQUEST['key']?>"/>
	<!--!/#container -->
	<footer> </footer>
	<!-- !Javascript - at the bottom for fast page loading -->
	<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>!window.jQuery.ui && document.write('<script src="js/jquery-ui.min.js"><\/script>')</script>
	<script src="js/create_account.js?a=2"></script>
</body>
</html>