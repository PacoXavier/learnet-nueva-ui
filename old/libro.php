<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 
links_head("Libro  ");
write_head_body();
write_body();

$DaoLibros= new DaoLibros();
$DaoAulas= new DaoAulas();
$DaoCategoriasLibros= new DaoCategoriasLibros();

?>
<table id="tabla">
<?php
$Id_lib="";
$Id_img="";
$Codigo="";
$Titulo="";
$Autor="";
$Editorial="";
$Anio="";
$Edicion="";
$ISBN="";
$Seccion="";
$Fecha_adq="";
$Valor="";
$Garantia_meses="";   
$Id_aula="";
$Id_cat="";
$Sinopsis="";
$Descripcion="";
$Id_subcat="";
    if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
        $libro = $DaoLibros->show($_REQUEST['id']);
        
        $Id_lib=$libro->getId();
        $Id_img=$libro->getId_img();
        $Codigo=$libro->getCodigo();
        $Titulo=$libro->getTitulo();
        $Autor=$libro->getAutor();
        $Editorial=$libro->getEditorial();
        $Anio=$libro->getAnio();
        $Edicion=$libro->getEdicion();
        $ISBN=$libro->getISBN();
        $Seccion=$libro->getSeccion();
        $Fecha_adq=$libro->getFecha_adq();
        $Valor=$libro->getValor();
        $Garantia_meses=$libro->getGarantia_meses();
        $Id_aula=$libro->getId_aula();
        $Id_cat=$libro->getId_cat();
        $Sinopsis=$libro->getSinopsis();
        $Descripcion=$libro->getDescripcion();
        $Id_subcat=$libro->getId_subcat();
    }
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <div class="foto_alumno" 
                    <?php if (strlen($Id_img) > 0) { ?> 
                             style="background-image:url(files/<?php echo $Id_img ?>.jpg) <?php } ?>">
                    </div>
                </div>
                <div class="seccion">
                    <h2>Datos del libro</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <?php
                        if ($Id_lib> 0) {
                        ?>
                        <li>C&oacute;digo<br><input type="text" value="<?php echo $Codigo ?>" id="codigo" readonly="readonly"/></li>
                        <?php
                            }
                         ?>
                        <li>T&iacute;tulo<br><input type="text" value="<?php echo $Titulo ?>" id="titulo"/></li>
                        <li>Autor<br><input type="text" value="<?php echo $Autor ?>" id="autor"/></li>
                        <li>Editorial<br><input type="text" value="<?php echo $Editorial ?>" id="editorial"/></li>
                        <li>Año<br><input type="text" value="<?php echo $Anio ?>" id="anio"/></li>
                        <li>Edici&oacute;n<br><input type="text" value="<?php echo $Edicion ?>" id="edicion"/></li>
                        <li>ISBN<br><input type="text" value="<?php echo $ISBN ?>" id="isbn"/></li>
                        <li>Secci&oacute;n<br><input type="text" value="<?php echo $Seccion ?>" id="seccion"/></li>
                        <li>Fecha de adquisici&oacute;n<br><input type="date" value="<?php echo $Fecha_adq ?>" id="fecha_adquisicion"/></li>
                        <li>Valor Factura<br><input type="text" value="<?php echo $Valor ?>" id="valor_factura"/></li>
                        <li>Meses de garantia<br><input type="text" id="meses_garantia" value="<?php echo $Garantia_meses ?>"/></li>
                        <li>Ubicaci&oacute;n<br>
                            <select id="aula">
                                <option value="0"></option>
                                <?php
                                foreach($DaoAulas->showAll() as $aula){
                                ?>
                                 <option value="<?php echo $aula->getId();?>" <?php if ($aula->getId() == $Id_aula) { ?> selected="selected"<?php } ?>><?php echo $aula->getClave_aula();?></option>
                                 <?php
                                 }
                                 ?>
                                 
                            </select>
                        </li>
                        <li>Categor&iacute;a<br>
                            <select id="categoria" onchange="update_subcat()">
                                <option value="0"></option>
                                <?php
                                foreach($DaoCategoriasLibros->showAll() as $cat){
                                ?>
                                 <option value="<?php echo $cat->getId();?>" <?php if ($cat->getId() == $Id_cat) { ?> selected="selected"<?php } ?>><?php echo $cat->getNombre()?></option>
                                 <?php
                                 }
                                 ?>
                            </select>
                        <li><div id="box-subcat">
                                <p>Subcategor&iacute;a<br>
                                <select id="subcat">
                                    <option value="0"></option>
                                    <?php
                                    if($Id_cat>0){
                                    foreach($DaoCategoriasLibros->getSubcategorias($Id_cat) as $cat){
                                    ?>
                                     <option value="<?php echo $cat->getId();?>" <?php if ($cat->getId() == $Id_subcat) { ?> selected="selected"<?php } ?>><?php echo $cat->getNombre()?></option>
                                     <?php
                                     }
                                    }
                                     ?>
                                </select>
                                </p>
                            </div></li><br>
                        <li>Sinopsis<br><textarea id="Sinopsis"><?php echo $Sinopsis?></textarea></li>
                        <li>Descripci&oacute;n<br><textarea id="descripcion"><?php echo $Descripcion?></textarea></li>
                    </ul>
                </div>
                <button id="button_ins" onclick="save_libro()">Guardar</button>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="libro.php" class="link">Nuevo</a></li>
                    <?php
                    if ($Id_lib> 0) {
                        ?>
                        <li><span onclick="mostrarFinder()">Añadir fotografia</span></li>
                        <li><a href="historial_prestamo_libro.php?id=<?php echo $Id_lib;?>" target="_blank">Historial de prestamos</a></li>
                        <li><a href="prestamo_libros.php?id=<?php echo $Id_lib;?>">Prestamo de libros</a></li>
                        <?php
                    }
                    ?>
                </ul>
                <input type="file" id="files" name="files" multiple="">
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_lib" value="<?php echo $Id_lib ?>"/>
<?php
write_footer();