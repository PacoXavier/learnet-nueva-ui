<?php
require_once('estandares/includes.php');
require_once('require_daos.php');

if (!isset($perm['48'])) {
    header('Location: home.php');
}
$DaoDocentes = new DaoDocentes();
$DaoHoras = new DaoHoras();
$DaoCiclos = new DaoCiclos();
$DaoDiasPlantel = new DaoDiasPlantel();

links_head("Categorías  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-clock-o"></i> Categorías a evaluar </h1>
                </div>

                <div class="seccion">
                    <ul class="form">
                        <li>Buscar Docente<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o clave"/>
                            <ul id="buscador_int"></ul>
                        </li>
                    </ul>
                </div>
                <?php
                $Id_docen = "";
                $Id_ciclo = "";
                if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                    $docente = $DaoDocentes->show($_REQUEST['id']);
                    $Id_docen = $docente->getId();
                    ?>
                    <div class="seccion" id="box_disponibilidad">
                        <h2>Sesioness disponibles para el profesor</h2>
                        <h2 style="font-size: 15px;"><?php echo $docente->getClave_docen() . "  - " . $docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . " " . $docente->getApellidoP_docen() ?></h2>
                        <h2 style="font-size: 15px;">Ciclo<br><select id="ciclo" onchange="mostrar_disponibilidad_ciclo(this)">
                                <option value="0">Selecciona el ciclo</option>
                                <?php
                                foreach ($DaoCiclos->getCiclosFuturos() as $k => $v) {
                                    ?>
                                    <option value="<?php echo $v->getId() ?>" <?php if ($Id_ciclo == $v->getId()) { ?> selected="selected" <?php } ?> ><?php echo $v->getClave() ?></option>
                                    <?php
                                }
                                ?>
                            </select></h2>
                        <span class="linea"></span>

                        <table id="list_usu">
                            <thead>
                                <tr>
                                    <td>Sesiones</td>
                                    <?php
                                    foreach ($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia) {
                                        ?>
                                        <td><?php echo $dia['Nombre'] ?></td>
                                        <?php
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($DaoHoras->showAll() as $hora) {
                                    ?>
                                    <tr id_hora="<?php echo $hora->getId(); ?>">
                                        <td><?php echo $hora->getTexto_hora() ?></td>
                                        <?php
                                        foreach ($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia) {
                                            ?>
                                            <td><input type="checkbox" class="<?php echo $dia['Abreviacion'] ?>" /></td>
                                            <?php
                                        }
                                        ?>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <?php
                    if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                        ?>
                        <li><span onclick="save_disponibilidad()">Guardar </span></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_docente" value="<?php echo $Id_docen; ?>"/>
<?php
write_footer();
?>
