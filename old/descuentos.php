<?php
header("Location: aplicar_becas.php");
/*
require_once('estandares/includes.php');
if(!isset($perm['36'])){
  header('Location: home.php');
}
require_once('estandares/class_interesados.php');
require_once('estandares/class_direcciones.php');

links_head("Becas  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <?php
    if ($_REQUEST['id'] > 0) {
        $class_interesados = new class_interesados($_REQUEST['id']);
        $alum = $class_interesados->get_interesado();
    }
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1>Becas</h1>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <li>Buscar<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o matricula"/>
                          <ul id="buscador_int"></ul>
                        </li>
                      
                    </ul>
                </div>
              <?php
              if ($_REQUEST['id'] > 0) {
                ?>
              <div class="seccion">
                    <h2>Ofertas del alumno</h2>
                    <span class="linea"></span>
                    <div id="datos">
                      <table>
                        <?php
              if($alum['Matricula']!=null){
                ?>
                              <tr>
                                <td>Matricula</td>
                                <td><?php echo $alum['Matricula']?></td>
                              </tr>
                               <tr>
                                <td>Referencia de pago</td>
                                <td><?php echo $alum['Refencia_pago']?></td>
                              </tr>
                              <?php
              }
              ?>
                        <tr>
                          <td>Nombre</td>
                          <td><?php echo $alum['Nombre_ins']." ".$alum['ApellidoP_ins']." ".$alum['ApellidoM_ins'] ?></td>
                        </tr>
                        <tr>
                          <td>Email</td>
                          <td><?php echo $alum['Email_ins']?></td>
                        </tr>
                      </table>
                    </div>
              </div>
                <div class="seccion" id="list_ofertas">
                    <table id="list_usu">
                        <thead>
                            <tr>
                                <td>Oferta</td>
                                <td>Curso</td>
                                <td>Ciclo</td>
                                <td>Beca</td>
                                <td>Precio</td>
                                <td>Inscripción</td>
                                <td>Pagos</td>
                                <td>Mensualidad</td>
                                <td>Aplicar</td>
                            </tr>

                        </thead>
                        <tbody>
                        <?php
                //print_r($alum['Ofertas']);
                    if (count($alum['Ofertas']) > 0) {
                        foreach ($alum['Ofertas'] as $k => $v) {
                            $class_ofertas = new class_ofertas();
                            $oferta = $class_ofertas->get_oferta($v['Id_ofe']);
                            $esp = $class_ofertas->get_especialidad($v['Id_esp']);


                            $nombre_ori="";

                            if($v['Id_ori']>0){
                                $ori = $class_ofertas->get_orientacion($v['Id_ori']);
                                $nombre_ori=$ori['Nombre_ori'];
                            }
                            
                            if ($v['Tipo_beca']==1) {
                              $Tipo_beca = "Rendimiento Acad&eacute;mico";
                            }elseif($v['Tipo_beca']==2){
                              $Tipo_beca = "Estudio Socioecon&oacute;mico";
                            }
                            ?>
                                    <tr>
                                        <td><?php echo $oferta['Nombre_oferta']; ?></td>
                                        <td><?php echo $esp['Nombre_esp']." ".$nombre_ori; ?></td>
                                        <td>
                                            <?php
                            $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=" .$v['Id_ofe_alum'] . " 
                                               ORDER BY Id_ciclo ASC LIMIT 1";
                            $ciclos_alum_ulm = mysql_query($query_ciclos_alum_ulm, $cnn) or die(mysql_error());
                            $row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm);

                            $query_ciclos_ulm = "SELECT * FROM ciclos_ulm WHERE Id_ciclo=" . $row_ciclos_alum_ulm['Id_ciclo'];
                            $ciclos_ulm = mysql_query($query_ciclos_ulm, $cnn) or die(mysql_error());
                            $row_ciclos_ulm = mysql_fetch_array($ciclos_ulm);
                            $totalRows_ciclos_ulm = mysql_num_rows($ciclos_ulm);

                              //Obtenemos los pagos del alumno del ciclo
                              $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=" . $v['Id_ofe_alum']." AND Id_ciclo=".$row_ciclos_alum_ulm['Id_ciclo'];
                              $ciclos_alum_ulm= mysql_query($query_ciclos_alum_ulm, $cnn) or die(mysql_error());
                              $row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm);

                              $query_Pagos_ciclo= "SELECT * FROM Pagos_ciclo WHERE Id_ciclo_alum=".$row_ciclos_alum_ulm['Id_ciclo_alum'];
                              $Pagos_Pagos_ciclo= mysql_query($query_Pagos_ciclo, $cnn) or die(mysql_error());
                              $row_Pagos_ciclo = mysql_fetch_array($Pagos_Pagos_ciclo);
                              $totalRows_Pagos_ciclo= mysql_num_rows($Pagos_Pagos_ciclo);

                            echo $row_ciclos_ulm['Clave'];
                            $Precio_curso=($esp['Precio_curso']/$esp['Num_ciclos'])/$esp['Num_pagos'];
                            ?>
                                        </td>
                                        <td><?php echo number_format($v['beca'], 2) ?> % <br><?php echo $Tipo_beca;?></td>
                                        <td><?php echo "$" . number_format($esp['Precio_curso'], 2) ?></td>
                                        <td><?php echo "$" . number_format($esp['Inscripcion_curso'], 2) ?></td>
                                        <td><?php echo $esp['Num_pagos'] ?></td>
                                        <td><?php echo "$" . number_format($Precio_curso, 2) ?></td>
                                        <td><button onclick="box_beca(<?php echo $v['Id_ofe_alum'];?>)">Beca</button></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
              <?php
              }
              ?>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <table id="usu_login">
                    <?php
                    $class_usuarios = new class_usuarios($_COOKIE['admin/Id_usu']);
                    $usu = $class_usuarios->get_usu();
                    ?>
                    <tr>
                        <td><div class="nom_usu"><div id="img_usu" 
                                                      <?php if (strlen($usu['Img_usu']) > 0) { ?> style="background-image:url(files/<?php echo $usu['Img_usu'] ?>.jpg) <?php } ?>"></div>
                                                      <?php echo $usu['Nombre_usu'] . " " . $usu['ApellidoP_usu'] . " " . $usu['ApellidoM_usu'] ?>
                                <img src="images/linea_uno.png" alt="linea_uno" width="185" height="5" class="lineas"/></div>
                        </td>
                        <td>
                            <div class="opcion">
                                <a href="perfil.php?id=<?php echo $usu['Id_usu'] ?>">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Mi perfil</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/>
                            </div>
                        </td>
                        <td><div class="opcion">
                                <a href="logout.php">
                                    <img src="images/flecha_azul.png" alt="flecha_azul" width="8" height="8" class="flechas"/>Salir</a>
                                <img src="images/linea_dos.png" alt="linea_dos" width="60" height="5" class="lineas"/></div>
                        </td>
                    </tr>
                </table>
                <h2><img src="images/flecha_blue.png" alt="flecha_blue" width="6" height="11" />Opciones</h2>
                <ul>
                    <li><a href="interesados.php" class="link">Interesados </a></li>
                    <li><a href="alumnos.php" class="link">Alumnos </a></li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<?php
write_footer();
*/
