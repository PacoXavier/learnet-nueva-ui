<?php
require_once('estandares/includes.php');
require_once('require_daos.php');

$base= new base();
$DaoDocentes= new DaoDocentes();
$DaoCiclos= new DaoCiclos();
$DaoOrientaciones= new DaoOrientaciones();
$DaoMateriasDocente= new DaoMateriasDocente();

links_head("Materias asignadas  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-list-alt"></i> Materias Docente </h1>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <li>Buscar Docente<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o clave"/>
                            <ul id="buscador_int"></ul>
                        </li>
                    </ul>
                </div>
                <?php
                if ($_REQUEST['id'] > 0) {
                    $docente = $DaoDocentes->show($_REQUEST['id']);
                    $Id_ciclo=0;
                    $ultimoCiclo=$DaoMateriasDocente->getUltimoCicloDocente($_REQUEST['id']);
                    if($ultimoCiclo->getId()>0){
                       $Id_ciclo=$ultimoCiclo->getId_ciclo();
                    }
                    ?>
                    <div class="seccion" id="box_info_materias">
                        <h2>Materias asignadas</h2>
                        <h2 style="font-size: 15px;margin-bottom: 15px;"><?php echo $docente->getClave_docen() . "  - " . $docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . " " . $docente->getApellidoM_docen() ?></h2>
                        <h2 style="font-size: 15px;">Ciclo<br>
                            <select id="Id_ciclo" onchange="get_mat_ciclo()">
                                <option value="0">Selecciona un ciclo</option>
                                <?php
                                foreach ($DaoCiclos->showAll() as $k=>$v ){
                                        ?>
                                        <option value="<?php echo $v->getId() ?>" <?php if ($v->getId() == $Id_ciclo) { ?> selected="selected" <?php } ?>><?php echo $v->getClave(); ?></option>
                                        <?php
                                }
                                ?>
                            </select></h2>
                        <span class="linea"></span>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>Ciclo</td>
                                    <td>Clave</td>
                                    <td>Nombre</td>
                                    <td>Orientaci&oacute;n</td>
                                    <td>Acciones</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($ultimoCiclo->getId()>0) {
                                    $query = "SELECT * FROM Materias_docentes 
                                                    JOIN materias_uml ON materias_uml.Id_mat =Materias_docentes.Id_materia
                                                    JOIN ciclos_ulm ON ciclos_ulm.Id_ciclo=Materias_docentes.Id_ciclo  
                                              WHERE Id_profesor=" . $_REQUEST['id'] . "  AND Materias_docentes.Id_ciclo=" . $Id_ciclo." ORDER BY Id_matp DESC";
                                    foreach($base->advanced_query($query) as $row) {
                                            $nombre_ori = "";
                                            if ($row['Id_orientacion'] > 0) {
                                                $orientacion = $DaoOrientaciones->show($row['Id_orientacion']);
                                                $nombre_ori = $orientacion->getNombre();
                                            }
                                            ?>
                                            <tr>
                                                <td><?php echo $row['Clave'] ?></td>
                                                <td><?php echo $row['Clave_mat'] ?></td>
                                                <td><?php echo $row['Nombre'] ?></td>
                                                <td><?php echo $nombre_ori ?></td>
                                                <td><button onclick="delete_mat_doc(<?php echo $row['Id_matp'] ?>)">Eliminar</button></td>
                                            </tr>
                                            <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <?php
                    if ($_REQUEST['id'] > 0) {
                        ?>
                        <li><span onclick="mostrar_box_materias()">Asignar materia </span></li>
                        <li><span onclick="mostrar_box_materias_ciclo()">Asignar materias del ciclo </span></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_docente" value="<?php echo $_REQUEST['id']; ?>"/>
<?php
write_footer();