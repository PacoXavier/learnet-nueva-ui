<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

$DaoAulas= new DaoAulas();
$DaoCiclos= new DaoCiclos();
$DaoHoras= new DaoHoras();
$DaoDiasPlantel= new DaoDiasPlantel();

links_head("Horario Aula  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-clock-o" aria-hidden="true"></i> Horario Aula</h1>
                </div>
                <span class="spanfiltros" onclick="mostrar_filtro()">Mostrar filtros</span>
                <div id="mascara_tabla">
                    <table class="table">
                            <thead>
                                <tr>
                                    <td>Sesiones</td>
                                    <?php
                                    foreach($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia){
                                        ?>
                                          <td><?php echo $dia['Nombre']?></td>
                                        <?php
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                                  foreach($DaoHoras->showAll() as $hora){    
                                 ?>
                                    <tr id_hora="<?php echo $hora->getId()?>">
                                        <td><?php echo $hora->getTexto_hora()?></td>
                                        <?php
                                        foreach($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia){
                                            ?>
                                              <td><span></span><br></td>
                                            <?php
                                        }
                                        ?>
                                    </tr>
                                 <?php
                                 }
                                 ?>
                            </tbody>
                        </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
        <p>Ciclo<br>
          <select id="ciclo">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->getCiclosFuturos() as $ciclo){
              ?>
               <option value="<?php echo $ciclo->getId(); ?>"><?php echo $ciclo->getClave() ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p>Aula<br>
           <select id="aula">
              <option value="0"></option>
              	<?php
                foreach($DaoAulas->showAll() as $aula){
                  	?>
                          <option value="<?php echo $aula->getId()?>"><?php echo $aula->getClave_aula()." - ".$aula->getNombre_aula() ?></option>
	                <?php  
                }
                ?>
            </select>
        </p>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();

