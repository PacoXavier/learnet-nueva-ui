<?php
require_once('estandares/includes.php');
require_once("clases/DaoUsuarios.php");
require_once('clases/DaoListaActividades.php');
require_once('clases/DaoTareasActividad.php');
require_once('clases/DaoDependencias.php');
require_once('clases/modelos/ListaActividades.php');
require_once('clases/modelos/TareasActividad.php');
require_once('clases/modelos/base.php');
require_once('clases/modelos/Dependencias.php');


$DaoListaActividades= new DaoListaActividades();
$DaoUsuarios= new DaoUsuarios();
$DaoTareasActividad= new DaoTareasActividad();
$DaoDependencias= new DaoDependencias();
$base= new base();

$Id_usuCookie=$_COOKIE['admin/Id_usu'];
//$Id_usuCookie=9;
$TipoUsu=$DaoUsuarios->show($Id_usuCookie);

$UsuariosPermitidos= array();
$query="SELECT * FROM usuarios_ulm WHERE Tipo_usu=1";
foreach($DaoUsuarios->advanced_query($query) as $k=>$v){
    array_push($UsuariosPermitidos, $v->getId());
}

links_head("Plan de acción  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-tasks"></i> Plan de acci&oacute;n correctivo</h1>
                </div>
               <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarDocen()" /></li>-->
                </ul>
                <h1 id="tituloActividad"><i class="fa fa-folder-open-o"></i> Actividades abiertas</h1>
                <div id="mascara_tabla">
                    <ul id="lista-actividades">
                       <?php
                        $count=1;
                        foreach($DaoListaActividades->getActividadesAbiertas() as $k=>$v){ 
                            $usuario=$DaoUsuarios->show($v->getId_usu());
                            $Id_usuAux=0;
                            $r=0;
                            //Visualiza solo las actividades en las que esta relacionado
                            foreach($DaoTareasActividad->getTareasActividad($v->getId()) as $k2=>$v2){
                                if(($v2->getId_usuRespon()==$Id_usuCookie) || ($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                                    $Id_usuAux=1;
                                }
                                $r++;
                            }
                            if($Id_usuAux==1 || $r==0){
                        ?>
                            <li id-act="<?php echo $v->getId()?>" class="actividad-li">
                                <table class="table">
                                    <tbody>
                                         <tr>
                                            <td style="width: 400px;"><?php echo $v->getNombre();?></td>
                                             <td><?php echo $v->getDateCreated()?></td>
                                             <td><?php echo $usuario->getNombre_usu()." ".$usuario->getApellidoP_usu()?></td>
                                             <td style="width: 90px;text-align: right;"> 
                                                 <?php
                                                  if(($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                                                 ?>
                                                    <i class="fa fa-pencil-square-o" onclick="mostrar_box_actividad(<?php echo $v->getId()?>)"></i>
                                                    <i class="fa fa-arrows"></i>
                                                    <i class="fa fa-times" onclick="delete_actividad(<?php echo $v->getId()?>)"></i>
                                                 <?php
                                                  }
                                                 ?>
                                                    <i class="fa fa-minus-square-o" onclick="mostrar_actividad(this)"></i>
                                             </td>
                                         </tr>
                                    </tbody>
                                </table>

                                <div class="box-tareas">
                                    <div class="comentario-act"><?php echo $v->getComentarios()?></div>
                                    <ul class="tareas">

                                        <?php
                                         foreach($DaoTareasActividad->getTareasActividad($v->getId()) as $k2=>$v2){
                                             $usuario=$DaoUsuarios->show($v2->getId_usuRespon());
                                             $classPrioridad="";
                                             $textoPrioridad="";
                                             if($v2->getPrioridad()=="alta"){
                                                $classPrioridad="prioridad-red"; 
                                                $textoPrioridad="Prioridad alta";
                                             }elseif($v2->getPrioridad()=="media"){
                                                $classPrioridad="prioridad-yellow";
                                                $textoPrioridad="Prioridad media";
                                             }elseif($v2->getPrioridad()=="baja"){
                                                $classPrioridad="prioridad-green";
                                                $textoPrioridad="Prioridad baja";
                                             }elseif($v2->getPrioridad()=="ninguna"){
                                                $classPrioridad="prioridad-none";
                                                $textoPrioridad="Sin prioridad";
                                             }

                                             $fecha="";
                                             if($v2->getStartDate()!=null && $v2->getEndDate()==null){
                                                $fecha="(Inicia el ".$base->formatFecha($v2->getStartDate()).")";
                                             }
                                             if($v2->getStartDate()==null && $v2->getEndDate()!=null){
                                                $fecha="(Termina el ".$base->formatFecha($v2->getEndDate()).")";
                                             }

                                             if($v2->getStartDate()!=null && $v2->getEndDate()!=null){
                                                $fecha="(Inicia el ".$base->formatFecha($v2->getStartDate()).", termina el ".$base->formatFecha($v2->getEndDate()).")";
                                             }
                                             //Puede visializar solo:
                                             //El usuario que creo la actividad
                                             //El usuario al que le asignaron la actividad
                                             //El que sea administrador 
                                             //if(($v2->getId_usuRespon()==$Id_usuCookie) || ($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                                                 $dependencias=$DaoDependencias->getDependenciasRealizadas($v2->getId());
                                                 $totalDependencias=count($DaoDependencias->getDependenciasTarea($v2->getId()));
                                            ?>
                                            <li id-tarea="<?php echo $v2->getId()?>" id-act="<?php echo  $v->getId()?>">
                                                <table class="table">
                                                    <tbody>
                                                         <tr>
                                                             <td style="width: 85px;padding-right: 0px">
                                                                 <?php
                                                                 $class="hidden-i";
                                                                 if(($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                                                                   $class="visible-i";
                                                                 }
                                                                 ?>
                                                                 <div class="box-i <?php echo $class;?>">
                                                                    <i class="fa fa-arrows"></i>
                                                                    <i class="fa fa-pencil-square-o" onclick="mostrar_box_tarea(<?php echo $v2->getId()?>,this,<?php echo $v->getId()?>)"></i>
                                                                    <i class="fa fa-times" onclick="delete_tarea(<?php echo $v2->getId()?>)"></i>
                                                                 </div>
                                                                 <div class="box-input" <?php if($dependencias==0 && $totalDependencias>0){?> title="<?php echo $textTitle?>" <?php }?> value="<?php echo $v2->getId()?>">
                                                                     <input type="checkbox" <?php if($v2->getStatus()==1){ ?> checked="checked" <?php }?> 
                                                                         <?php 
                                                                         //El usuario puede visualizar todas las tareas aunque no sea el usuario asignado
                                                                         //Pero solo podra dar click en las que este asignado
                                                                         if(($dependencias==0 && $totalDependencias>0) || (($v2->getId_usuRespon()!=$Id_usuCookie &&  $v->getId_usu()!=$Id_usuCookie) && (!in_array($Id_usuCookie, $UsuariosPermitidos)))){
                                                                         ?> 
                                                                            disabled="disabled" 
                                                                         <?php 
                                                                         }
                                                                         ?> 
                                                                            onchange="tareaStatus(this)" 
                                                                            data-id="<?php echo $v2->getId()?>"
                                                                       >
                                                                 </div>
                                                             </td>
                                                             <td><div class="asignado"><?php echo $usuario->getNombre_usu()." ".$usuario->getApellidoP_usu()?></div>
                                                                 <div class="comentarios-tarea"><?php echo $v2->getComentario()?></div>
                                                                 <div class="mas">
                                                                     <span onclick="mostrar_comentario(this)"> <?php echo strtolower("m&aacute;s")?> </span> 
                                                                     <i class="fa fa-exclamation-circle <?php echo $classPrioridad?>" title="<?php echo $textoPrioridad;?>"></i>
                                                                     <span class="fechas"><?php echo $fecha;?></span>
                                                                 </div>
                                                             </td>
                                                         </tr>
                                                         
                                                    </tbody>
                                                </table> 
                                                <div class="comentarios-tarea-completo">
                                                    <p><?php echo $v2->getComentario()?></p>
                                                    <?php
                                                      if(strlen($v2->getComentario_realizo())>0){
                                                    ?>
                                                    <br>
                                                    <p><b>¿C&oacute;mo se completo?</b></p> 
                                                    <p><?php echo $v2->getComentario_realizo()?></p>
                                                    <?php
                                                      }
                                                    ?>
                                                </div>
                                            </li>
                                        <?php
                                             //}
                                        }
                                        if(($v->getId_usu()==$Id_usuCookie) || ($TipoUsu->getTipo_usu()==1)){
                                        ?>
                                            <li><span class="button-tarea" onclick="mostrar_box_tarea(0,this,<?php echo $v->getId()?>)"><i class="fa fa-plus"></i> Añadir tarea</span></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </li>
                       <?php
                            }
                            
                       $count++; 
                       }
                       //print_r($UsuariosPermitidos);
                       ?>   
                    </ul>

                </div>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><span onclick="mostrar_box_actividad(0)"><i class="fa fa-plus" style="margin-right: 5px;"></i> Nueva actividad</span></li>
                    <li><span onclick="mostrar_tareas_abiertas()"><i class="fa fa-exclamation-triangle" style="color: #d6b73b;margin-right: 5px;"></i> Actividades abiertas</span></li>
                    <li><span onclick="mostrar_tareas_cerradas()"><i class="fa fa-exclamation-triangle" style="color: #63ad63;margin-right: 5px;"></i> Actividades cerradas</span></li>
                </ul>
            </div>
        </td>
    </tr>
</table>
<div id="box_buscador"></div>
<?php
write_footer();
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="css/actividades.css">


