<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoActivos.php');
require_once('clases/DaoHistorialPrestamoActivo.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoUsuarios.php');
require_once('clases/DaoCiclos.php');

$base = new base();
$DaoActivos = new DaoActivos();
$DaoHistorialPrestamoActivo = new DaoHistorialPrestamoActivo();
$DaoAlumnos = new DaoAlumnos();
$DaoDocentes = new DaoDocentes();
$DaoUsuarios = new DaoUsuarios();

$DaoCiclos = new DaoCiclos();
$ciclo = $DaoCiclos->getActual();

links_head("Historial de prestamo  ");
?>
<link rel="stylesheet" media="print" href="css/reporte_prestamo_activo_print.css?v=2011.6.16.18.36"> 
<?php
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-history"></i> Historial de prestamo de activos</h1>
                </div>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Filtros</li>
                        <li onclick="download_excel()"><i class="fa fa-download"></i> Descargar</li>
                        <li onclick="imprimir()"><i class="fa fa-download"></i> Imprimir</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Código</td>
                                <td>Nombre</td>
                                <td>Usuario</td>
                                <td>Solicitante</td>
                                <td>Fecha de prestamo</td>
                                <td>Fecha de entrega</td>
                                <td>fecha de recibido</td>
                                <td>Comentarios</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($DaoHistorialPrestamoActivo->getPrestamosActivo() as $historial) {
                                $activo = $DaoActivos->show($historial->getId_activo());
                                $usuEntrega = $DaoUsuarios->show($historial->getUsu_entrega());

                                if ($historial->getTipo_usu() == "usu") {
                                    $usuRecibe = $DaoUsuarios->show($historial->getUsu_recibe());
                                    $Nombreusu = $usuRecibe->getNombre_usu() . " " . $usuRecibe->getApellidoP_usu() . " " . $usuRecibe->getApellidoM_usu();
                                } elseif ($historial->getTipo_usu() == "docen") {
                                    $usuRecibe = $DaoDocentes->show($historial->getUsu_recibe());
                                    $Nombreusu = $usuRecibe->getNombre_docen() . " " . $usuRecibe->getApellidoP_docen() . " " . $usuRecibe->getApellidoM_docen();
                                } elseif ($historial->getTipo_usu() == "alum") {
                                    $usuRecibe = $DaoAlumnos->show($historial->getUsu_recibe());
                                    $Nombreusu = $usuRecibe->getNombre() . " " . $usuRecibe->getApellidoP() . " " . $usuRecibe->getApellidoM();
                                }
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $activo->getCodigo() ?></td>
                                    <td style="width: 115px;"><?php echo $activo->getNombre() ?></td>
                                    <td><?php echo $usuEntrega->getNombre_usu() . " " . $usuEntrega->getApellidoP_usu() . " " . $usuEntrega->getApellidoM_usu(); ?></td>
                                    <td><?php echo $Nombreusu; ?></td>
                                    <td><?php echo $historial->getFecha_entrega(); ?></td>
                                    <td><?php echo $historial->getFecha_devolucion(); ?></td>
                                    <td><?php echo $historial->getFecha_entregado(); ?></td>
                                    <td><?php echo $historial->getComentarios(); ?></td>
                                </tr>
                                <?php
                                $count++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
    <h1>Filtros</h1>
    <div class="boxUlBuscador">
        <p>Código<br><input type="search"  class="buscarFiltro" onkeyup="buscarActivo(this)" id="activo"/></p>
        <ul class="Ulbuscador"></ul>
    </div>
    <p>Fecha inicio de prestamo<br><input type="date" id="fecha_ini"/>
    <p>Fecha fin de prestamo<br><input type="date" id="fecha_fin"/>
    <div class="boxUlBuscador">
        <p>Usuario<br><input type="search"  class="buscarFiltro" onkeyup="buscarUsu(this)" id="usuario"/></p>
        <ul class="Ulbuscador"></ul>
    </div>
    <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<?php
write_footer();



