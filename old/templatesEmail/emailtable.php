<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html lang="en" class="no-js" style="background-color: #F8F8F8;">
    <head>
        
    </head>
    <body style="background-color: #F8F8F8; padding-top: 40px;" bgcolor="#F8F8F8">
        <div id="container" style="width: 600px; background-color: white; font-family: arial; font-size: 13px; margin: auto;">
            <div id="boxtext" style="width: 540px; padding: 30px;min-height: 300px;">
                <img src="http://www.ulm.mx/admin_ce/images/logoulm.png" alt="logo" width="211" height="80"><h1 style="font-size: 40px; font-family: arial; color: #606060; font-weight: 100; margin-top: 20px;">Mensaje de prueba</h1>
                <p style="margin-bottom: 20px;">Hola este es un mensaje de prueba</p>
                <?php
                    //Obtenemos el ciclo actual del alumno para verificar sus materias
                    $query_ciclos_alum_ulm = "SELECT * FROM ciclos_alum_ulm WHERE Id_ofe_alum=" . $v['Id_ofe_alum']." ".$query;
                    $ciclos_alum_ulm = mysql_query($query_ciclos_alum_ulm, $cnn) or die(mysql_error());
                    $row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm);
                    $totalRows_ciclos_alum_ulm = mysql_num_rows($ciclos_alum_ulm);
                    if ($totalRows_ciclos_alum_ulm > 0) {

                            $class_ofertas = new class_ofertas();
                            $oferta = $class_ofertas->get_oferta($v['Id_ofe']);
                            $esp = $class_ofertas->get_especialidad($v['Id_esp']);
                            if ($v['Id_ori'] > 0) {
                              $ori = $class_ofertas->get_orientacion($v['Id_ori']);
                              $nombre_ori = $ori['Nombre_ori'];
                            }

                            $Opcion_pago="";
                            if($v['Opcion_pago']==1){
                                $Opcion_pago="Plan por materias";
                            }else{
                                $Opcion_pago="Plan completo";
                            }
                            ?>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Matricula</td>
                                        <td class="normal"><?php echo $alum['Matricula'] ?></td>
                                        <td>Nombre</td>
                                        <td colspan="3" class="normal"><?php echo $alum['Nombre_ins'] . " " . $alum['ApellidoP_ins'] . " " . $alum['ApellidoM_ins'] ?></td>
                                        <td>Opci&oacute;n de pago</td>
                                        <td class="normal" colspan="4"><?php echo $Opcion_pago; ?></td>
                                    </tr>
                                    <tr>
                                         <td>Carrera</td>
                                         <td colspan="3" class="normal"><?php echo $esp['Nombre_esp']; ?></td>
                                         <td>Orientaci&oacute;n</td>
                                         <td class="normal"><?php echo $nombre_ori; ?></td>
                                         <td>Nivel</td>
                                         <td colspan="4" class="normal"><?php echo $oferta['Nombre_oferta']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Ciclo</td>
                                        <td style="width: 80px">CLAVE GRUPAL</td>
                                        <td style="width: 100px">MATERIA</td>
                                        <td>TURNO</td>
                                        <td>Faltas</td>
                                        <td>Asistencias</td>
                                        <td>Justificaciones</td>
                                        <td>Porcentaje</td>
                                        <td>Calificaci&oacute;n</td>
                                        <td>Evaluaci&oacute;n<br> Extraordinaria</td>
                                        <td>Evaluaci&oacute;n<br> Especial</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $countMat=0;
                                    $totalCal=0;
                                    do{
                                    $ciclo_alum = $class_interesados->get_ciclo_oferta($row_ciclos_alum_ulm['Id_ciclo_alum']);
                                    foreach ($ciclo_alum['Materias_ciclo_oferta'] as $k2 => $v2) {

                                            $class_grupos = new class_grupos();
                                            $grupo= array();
                                            if($v2['Id_grupo']>0){
                                                $grupo = $class_grupos->get_grupo($v2['Id_grupo']);
                                                $asis=$class_grupos->get_asistencias_grupo_alum($grupo['Id_grupo'],$id_alum);
                                                $total= $class_grupos->porcentaje_asistencias($asis['Asistencias'],$asis['Justificaciones'],$asis['Faltas']);
                                            }

                                            $class_materias = new class_materias();
                                            $mat_esp=$class_materias->get_esp_mat($v2['Id_mat_esp']);
                                            $mat = $class_materias->get_materia($mat_esp['Id_mat']);

                                            $colorCalTotalParciales="red";
                                            if($v2['CalTotalParciales']>$mat['Promedio_min']){
                                                $colorCalTotalParciales="green";
                                                $totalCal+=$v2['CalTotalParciales'];
                                            }
                                            $colorCalExtraordinario="red";
                                            if($v2['CalExtraordinario']>$mat['Promedio_min']){
                                                $colorCalExtraordinario="green";
                                                $totalCal+=$v2['CalExtraordinario'];

                                            }
                                            $colorCalEspecial="red";
                                            if($v2['CalEspecial']>$mat['Promedio_min']){
                                                $colorCalEspecial="green";
                                                $totalCal+=$v2['CalEspecial'];
                                            }

                                            $turno = "";
                                            if ($grupo['Turno'] == 1) {
                                                $turno = "MATUTINO";
                                            }if ($grupo['Turno'] == 2) {
                                                $turno = "VESPERTINO";
                                            }if ($grupo['Turno'] == 3) {
                                                $turno = "NOCTURNO";
                                            }
                                            $class_interesados = new class_interesados($id_alum);
                                            $alumn = $class_interesados->get_interesado();

                                             $NombreMat=$mat['Nombre'];
                                             if(strlen($mat_esp['NombreDiferente'])>0){
                                                $NombreMat=$mat_esp['NombreDiferente']; 
                                             }

                                             $class_ciclos = new class_ciclos($row_ciclos_alum_ulm['Id_ciclo']);
                                             $ciclo = $class_ciclos->get_ciclo();
                                            ?>
                                            <tr>
                                                <td><?php echo $ciclo['Clave'] ?></td>
                                                <td><?php echo $grupo['Clave'] ?> </td>
                                                <td style="width: 200px;"><?php echo $NombreMat ?></td>
                                                <td><?php echo $turno; ?></td>
                                                <td style="text-align: center;color: red;"><?php echo $asis['Faltas']; ?></td>
                                                <td style="text-align: center;color: green;"><?php echo $asis['Asistencias']; ?></td>
                                                <td style="text-align: center; color: black;"><?php echo $asis['Justificaciones']; ?></td>
                                                <td style="text-align: center; color: black;"><?php echo number_format($total, 2) ?>%</td>
                                                <td style="text-align: center; color: <?php echo $colorCalTotalParciales;?>;"><?php echo $v2['CalTotalParciales']." ".$mat['Minima_aprobatotia']?></td>
                                                <td style="text-align: center; color: <?php echo $colorCalExtraordinario;?>;"><?php echo $v2['CalExtraordinario']?></td>
                                                <td style="text-align: center; color: <?php echo $colorCalEspecial;?>;"><?php echo $v2['CalEspecial']?></td>
                                            </tr>
                                            <?php
                                            $countMat++;
                                    }
                                    }while($row_ciclos_alum_ulm = mysql_fetch_array($ciclos_alum_ulm));
                        
                        }
                        ?>
                    </tbody>
                </table>
                ?>
            </div>
            <div id="footer" style="background-color: #606060; color: white; font-size: 13px; width: 540px; padding: 30px;padding-top: 20px;padding-bottom: 20px;">
                <table><tr>
                        <td style="vertical-align: top; padding-right: 5px;" valign="top">
                            <div id="text_footer" style="width: 226px; font-size: 13px; color: white; font-family: arial;">
                                <p style="margin-bottom: 10px;">Tel&eacute;fonos<br> (0133) 36 16 27 69 | 01800 841 9955 </p>
                                <p style="margin-bottom: 10px;">Domicilio: <br>Av. Vallarta No. 1543 Col. Americana Guadalajara, Jalisco</p>
                            </div>		                 
                        </td>
                    </tr></table>
            </div>
        </div>
        <p class="footer" style="font-family: arial; font-size: 10px; color: black; width: 600px; margin: 5px auto auto;">2012 ULM Universidad Libre de M&uacute;sica</p>
    </body>
</html>
