<?php
require_once('estandares/includes.php');
if(!isset($perm['48'])){
  header('Location: home.php');
}
require_once('require_daos.php');
$DaoAlumnos= new DaoAlumnos();
$DaoCiclos= new DaoCiclos();

links_head("Justificar faltas  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-check-square-o"></i> Justificar faltas</h1>
                </div>
                <div class="seccion">
                    <div id="box_uno">
                        <?php
                        $Id_alum=0;
                        if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                            $alum = $DaoAlumnos->show($_REQUEST['id']);
                            $nombre = $alum->getNombre() . " " . $alum->getApellidoP() . " " . $alum->getApellidoM();
                            $Id_alum=$alum->getId();
                        }
                        ?>
                        <ul class="form">
                            <li>Alumno<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o clave" value="<?php echo $nombre ?>"/>
                                <ul id="buscador_int"></ul>
                            </li><br>
                            <?php
                            if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                                ?>
                                <li>Ciclo<br><select id="ciclo" onchange="mostrar_justificaciones()">
                                        <option value="0">Selecciona el ciclo</option>
                                        <?php
                                            foreach($DaoCiclos->getCiclosFuturos() as $ciclo){
                                             ?>
                                                <option value="<?php echo $ciclo->getId() ?>"><?php echo $ciclo->getClave() ?></option>
                                              <?php
                                              }
                                              ?>
                                        </select>
                                </li> 
                            <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_alum" value="<?php echo $Id_alum; ?>"/>
<?php
write_footer();
?>
<script type="text/javascript" src="js/timepicker/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/jquery.timepicker.css">
<script type="text/javascript" src="js/timepicker/lib/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="js/timepicker/lib/bootstrap-datepicker.css">
<script type="text/javascript" src="js/timepicker/lib/site.js"></script>
<script type="text/javascript" src="js/Datepair/dist/datepair.js"></script>
<script type="text/javascript" src="js/Datepair/dist/jquery.datepair.js"></script>
