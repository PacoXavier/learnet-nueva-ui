<?php
require_once('estandares/includes.php');
require_once('require_daos.php');

$DaoDocentes = new DaoDocentes();
$DaoDirecciones = new DaoDirecciones();
$DaoContactos = new DaoContactos();

$Id_docente = 0;
$Img_docen="";
$Nombre_docen = "";
$ApellidoP_docen = "";
$ApellidoM_docen = "";
$Clave_docen = "";
$Email_docen = "";
$Telefono_docen = "";
$Cel_docen = "";
$NivelEst_docen = "";
$Tiempo_completo = "";
$Alergias="";
$Enfermedades_cronicas=""; 
$Preinscripciones_medicas="";  
$Tipo_sangre="";
        
$ContactoNombre = "";
$ContactoParentesco = "";
$ContactoDireccion = "";
$ContactoTel_casa = "";
$ContactoTel_ofi = "";
$ContactoCel = "";
$ContactoEmail = "";
$ContactoId_cont = "";

$Calle_dir = "";
$NumExt_dir = "";
$NumInt_dir = "";
$Colonia_dir = "";
$Cp_dir = "";
$Ciudad_dir = "";
$Estado_dir = "";
$Id_dir=0;

links_head("Docente  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <?php
    if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {

        $docente = $DaoDocentes->show($_REQUEST['id']);
        $Id_docente = $docente->getId();
        $Img_docen=$docente->getImg_docen();
        $Nombre_docen = $docente->getNombre_docen();
        $ApellidoP_docen = $docente->getApellidoP_docen();
        $ApellidoM_docen = $docente->getApellidoM_docen();
        $Clave_docen = $docente->getClave_docen();
        $Email_docen = $docente->getEmail_docen();
        $Telefono_docen = $docente->getTelefono_docen();
        $Cel_docen = $docente->getCel_docen();
        $NivelEst_docen = $docente->getNivelEst_docen();
        $Tiempo_completo = $docente->getTiempo_completo();
        $Alergias=$docente->getAlergias();
        $Enfermedades_cronicas=$docente->getEnfermedades_cronicas();
        $Preinscripciones_medicas=$docente->getPreinscripciones_medicas();
        $Tipo_sangre=  $docente->getTipo_sangre();
        
        $contacto = $DaoContactos->getPrimerContacto($Id_docente, 'docente');

        $ContactoNombre = $contacto->getNombre();
        $ContactoParentesco = $contacto->getParentesco();
        $ContactoDireccion = $contacto->getDireccion();
        $ContactoTel_casa = $contacto->getTel_casa();
        $ContactoTel_ofi = $contacto->getTel_ofi();
        $ContactoCel = $contacto->getCel();
        $ContactoEmail = $contacto->getEmail();
        $ContactoId_cont = $contacto->getId();

        if ($docente->getId_dir() > 0) {
            $Id_dir = $docente->getId_dir();
            $dir = $DaoDirecciones->show($docente->getId_dir());
            $Calle_dir = $dir->getCalle_dir();
            $NumExt_dir = $dir->getNumExt_dir();
            $NumInt_dir = $dir->getNumInt_dir();
            $Colonia_dir = $dir->getColonia_dir();
            $Cp_dir = $dir->getCp_dir();
            $Ciudad_dir = $dir->getCiudad_dir();
            $Estado_dir = $dir->getEstado_dir();
        }
    }
    ?>
    <input type="hidden" id="Id_dir" value="<?php echo $Id_dir ?>"/>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <div class="foto_alumno" 
                    <?php if (strlen($Img_docen) > 0) { ?> 
                             style="background-image:url(files/<?php echo $Img_docen ?>.jpg) <?php } ?>">
                    </div>
                    <h1><?php echo ucwords(strtolower($Nombre_docen . " " . $ApellidoP_docen . " " . $ApellidoM_docen)) ?></h1>
                </div>
                <div class="seccion">
                    <h2>Datos Personales</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Clave<br><input type="text" value="<?php echo $Clave_docen ?>" id="clave_usu" readonly="readonly" /></li>
                        <li>Nombre<br><input type="text" value="<?php echo $Nombre_docen ?>" id="nombre_usu"/></li>
                        <li>Apellido Paterno<br><input type="text" value="<?php echo $ApellidoP_docen ?>" id="apellidoP_usu"/></li>
                        <li>Apellido Materno<br><input type="text" value="<?php echo $ApellidoM_docen ?>" id="apellidoM_usu"/></li>
                        <li>Email<br><input type="email" id="email_usu" value="<?php echo $Email_docen ?>"/></li>
                        <li>Tel&eacute;fono Casa<br><input type="tel" id="tel_usu" value="<?php echo $Telefono_docen ?>"/></li>
                        <li>Tel&eacute;fono Movil<br><input type="tel" id="cel_usu" value="<?php echo $Cel_docen ?>"/></li>
                        <li>Ultimo Grado de Estudios<br>
                            <select id="escolaridad">
                                <option value="0"></option>
                                <option value="1" <?php if ($NivelEst_docen == 1) { ?> selected="selected"<?php } ?>>Primaria</option>
                                <option value="2" <?php if ($NivelEst_docen == 2) { ?> selected="selected"<?php } ?>>Secundaria</option>
                                <option value="3" <?php if ($NivelEst_docen == 3) { ?> selected="selected"<?php } ?>>Preparatoria</option>
                                <option value="4" <?php if ($NivelEst_docen == 4) { ?> selected="selected"<?php } ?>>Universidad</option>
                                <option value="5" <?php if ($NivelEst_docen == 5) { ?> selected="selected"<?php } ?>>Posgrado</option>
                            </select>
                        </li>
                        <li>Tiempo completo<br><input type="checkbox" id="tiempo_completo" <?php if ($Tiempo_completo == 1) { ?> checked="checked" <?php } ?> /></li>
                    </ul>
                </div>
                <div class="seccion">
                    <h2>Direcci&oacute;n</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Calle<br><input type="text" id="calle_docen" value="<?php echo $Calle_dir ?>"/></li>
                        <li>N&uacute;mero exterior<br><input type="text" id="numExt_docen" value="<?php echo $NumExt_dir ?>"/></li>
                        <li>N&uacute;mero interior<br><input type="text" id="numInt_docen" value="<?php echo $NumInt_dir ?>"/></li>
                        <li>Colonia<br><input type="text" id="colonia_docen" value="<?php echo $Colonia_dir ?>"/></li>
                        <li>C&oacute;digo postal<br><input type="text" id="cp_docen" value="<?php echo $Cp_dir ?>"/></li>
                        <li>Ciudad<br><input type="text" id="ciudad_docen" value="<?php echo $Ciudad_dir ?>"/></li>
                        <li>Estado<br><input type="text" id="estado_docen" value="<?php echo $Estado_dir ?>"/></li>
                    </ul>
                </div>
                <div class="seccion">
                    <h2>Datos de Emergencia</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Tipo de sangre<br><input type="text"  id="tipo_sangre" value="<?php echo $Tipo_sangre ?>"/></li>
                        <li>Nombre del contacto<br><input type="text" id="nombre_contacto" value="<?php echo $ContactoNombre ?>"/></li>
                        <li>Parentesco<br><input type="text" id="parentesco_contacto" value="<?php echo $ContactoParentesco ?>"/></li>
                        <li>Direcci&oacute;n<br><input type="text" id="direccion_contacto" value="<?php echo $ContactoDireccion ?>"/></li>
                        <li>Tel&eacute;fono Casa<br><input type="text" id="tel_contacto" value="<?php echo $ContactoTel_casa ?>"/></li>
                        <li>Tel&eacute;fono Oficina<br><input type="text" id="telOfic_contacto" value="<?php echo $ContactoTel_ofi ?>"/></li>
                        <li>Celular<br><input type="text" id="celular_contacto" value="<?php echo $ContactoCel ?>"/></li>
                        <li>Email<br><input type="text" id="email_contacto" value="<?php echo $ContactoEmail ?>"/></li>
                    </ul>
                    <input type="hidden" id="Id_contacto" value="<?php echo $ContactoId_cont ?>"/>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <li>Alergias<br><textarea  id="alergias_contacto"><?php echo $Alergias ?></textarea></li>
                        <li>Enfermedades cr&oacute;nicas<br><textarea  id="cronicas_contacto"><?php echo $Enfermedades_cronicas ?></textarea></li>
                        <li>Pre-escripciones medicas<br><textarea  id="preinscripciones_medicas_contacto"><?php echo $Preinscripciones_medicas ?></textarea></li>
                    </ul>
                </div>
                <?php
                if ($Id_docente > 0) {
                    ?>
                    <div class="seccion">
                        <h2>Nivel Tabulador</h2>
                        <span class="linea"></span>
                        <table id="list_usu">
                            <thead>
                                <tr>
                                    <td>Fecha</td>
                                    <td>Nombre</td>
                                    <td>Puntos</td>
                                    <td>Profesor A</td>
                                    <td>Profesor B</td>
                                    <td>Profesor C</td>
                                    <td>Profesor D</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query_evaluacion = "SELECT * FROM Evaluacion_docente WHERE Id_doc=" . $Id_docente . " ORDER  BY Id_eva DESC";
                                $evaluacion = mysql_query($query_evaluacion, $cnn) or die(mysql_error());
                                $row_evaluacion = mysql_fetch_array($evaluacion);
                                $totalRows_evaluacion = mysql_num_rows($evaluacion);
                                if ($totalRows_evaluacion > 0) {
                                    $query_profesor_a = "SELECT * FROM Niveles_docentes WHERE Id_nivel=" . $row_evaluacion['Profesor_a'];
                                    $profesor_a = mysql_query($query_profesor_a, $cnn) or die(mysql_error());
                                    $row_profesor_a = mysql_fetch_array($profesor_a);

                                    $query_profesor_b = "SELECT * FROM Niveles_docentes WHERE Id_nivel=" . $row_evaluacion['Profesor_b'];
                                    $profesor_b = mysql_query($query_profesor_b, $cnn) or die(mysql_error());
                                    $row_profesor_b = mysql_fetch_array($profesor_b);

                                    $query_profesor_c = "SELECT * FROM Niveles_docentes WHERE Id_nivel=" . $row_evaluacion['Profesor_c'];
                                    $profesor_c = mysql_query($query_profesor_c, $cnn) or die(mysql_error());
                                    $row_profesor_c = mysql_fetch_array($profesor_c);

                                    $query_profesor_d = "SELECT * FROM Niveles_docentes WHERE Id_nivel=" . $row_evaluacion['Profesor_d'];
                                    $profesor_d = mysql_query($query_profesor_d, $cnn) or die(mysql_error());
                                    $row_profesor_d = mysql_fetch_array($profesor_d);
                                    ?>
                                    <tr>
                                        <td><?php echo $row_evaluacion['Fecha_evaluacion'] ?></td>
                                        <td><?php echo $docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . "  " . $docente->getApellidoM_docen() ?></td>
                                        <td><?php echo $row_evaluacion['Puntos_totales'] ?></td>
                                        <td><?php echo $row_profesor_a['Nombre_nivel'] ?></td>
                                        <td><?php echo $row_profesor_b['Nombre_nivel'] ?></td>
                                        <td><?php echo $row_profesor_c['Nombre_nivel'] ?></td>
                                        <td><?php echo $row_profesor_d['Nombre_nivel'] ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                }
                ?>
                <button id="button_ins" onclick="save_usu()">Guardar</button>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="docente.php" class="link">Nuevo</a></li>
                    <?php
                    if ($Id_docente > 0) {
                        ?>
                        <li><span onclick="box_documents()">Documentos</span></li>
                        <li><span onclick="box_pass()">Cambiar contrase&ntilde;a</span></li>
                        <li><span onclick="mostrarFinder()">A&ntilde;adir fotografia</span></li>
                        <?php
                        if (isset($perm['49'])) {
                            ?>
                            <li><a href="evaluaciones_docente.php?id=<?php echo $Id_docente; ?>">Evaluar Docente</span></li>
                            <?php
                        }
                        if (isset($perm['48'])) {
                            ?>
                            <li><a href="disponibilidad_docente.php?id=<?php echo $Id_docente; ?>">Disponibilidad Docente</span></li>
                            <?php
                        }
                        if (isset($perm['50'])) {
                            ?>
                            <li><a href="ligar_materias.php?id=<?php echo $Id_docente; ?>">Materias Docente</span></li>

                            <?php
                        }
                    }
                    ?>
                </ul>
                <input type="file" id="files" name="files" multiple="">
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_doc" value="<?php echo $Id_docente ?>"/>
<?php
write_footer();

