<?php
require_once('estandares/includes.php');
require_once('clases/modelos/base.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoGrados.php');
require_once('clases/DaoMediosEnterar.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoUsuarios.php');

$base= new base();
$DaoOfertas= new DaoOfertas();
$DaoAlumnos= new DaoAlumnos();
$DaoEspecialidades= new DaoEspecialidades();
$DaoOrientaciones= new DaoOrientaciones();
$DaoOfertasAlumno= new DaoOfertasAlumno();
$DaoGrados= new DaoGrados();
$DaoMediosEnterar= new DaoMediosEnterar();
$DaoCiclos= new DaoCiclos();
$ciclo=$DaoCiclos->getActual();
$DaoUsuarios= new DaoUsuarios();

links_head("Alumnos  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <img src="images/icon_ins_small.png" alt="icon_ins_small" width="39" height="39" /><h1>Ofertas de inter&eacute;s</h1>
                </div>
                <ul class="form">
                    <!--<li>Buscar<br><input type="search"  id="buscar" onkeyup="buscarAlum()" /></li>-->
                </ul>
                <div class="box-filter-reportes">
                    <ul>
                        <li onclick="mostrar_filtro()"><i class="fa fa-filter"></i> Filtros</li>
                        <li onclick="mostrar_box_email()"><i class="fa fa-envelope"></i> Email</li>
                        <li onclick="download_excel()"><i class="fa fa-download"></i> Descargar</li>
                    </ul>
                </div>
                <div id="mascara_tabla">
                    <table class="table">
                        <thead>
                            <tr>
                                <td style="width: 80px">Matricula</td>
                                <td>Nombre</td>
                                <td style="width: 150px">Oferta</td>
                                <td style="width: 200px">Carrera</td>
                                <td>Orientaci&oacute;n:</td>
                                <td>Ciclo de inter&eacute;s</td>
                                <td>Fecha de captura</td>
                                <td><input type="checkbox" id="all_alumns" onchange="marcar_alumnos()"></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            

                             if($ciclo->getId()>0){
                             $query = "SELECT * FROM inscripciones_ulm 
                             JOIN Ofertas_interes ON inscripciones_ulm.Id_ins=Ofertas_interes.Id_alum
                             WHERE tipo=1  AND inscripciones_ulm.Id_plantel=".$_usu->getId_plantel()." AND Id_ciclo=".$ciclo->getId()." ORDER BY DateCreated DESC";
                             $consulta=$base->advanced_query($query);
                             $row_consulta = $consulta->fetch_assoc();
                             $totalRows_consulta= $consulta->num_rows;
                             if($totalRows_consulta>0){
                               do{
                                       
                                        $nombre_ori="";
                                        $oferta = $DaoOfertas->show($row_consulta['Id_ofe']);
                                        $esp = $DaoEspecialidades->show($row_consulta['Id_esp']);
                                        if ($row_consulta['Id_ori'] > 0) {
                                           $ori = $DaoOrientaciones->show($row_consulta['Id_ori']);
                                           $nombre_ori = $ori->getNombre();
                                         }
                                         $opcion="Plan por materias"; 
                                         if($row_consulta['Opcion_pago']==2){
                                           $opcion="Plan completo";  
                                         }

                                         $MedioEnt="";
                                         if($row_consulta['Id_medio_ent']>0){
                                           $medio=$DaoMediosEnterar->show($row_consulta['Id_medio_ent']);
                                           $MedioEnt=$medio->getMedio();
                                         }
                                        $ciclo=$DaoCiclos->show($row_consulta['Id_ciclo']);
                                        
                                      ?>
                                              <tr id_alum="<?php echo $row_consulta['Id_ins'];?>">
                                                <td><?php echo $row_consulta['Matricula'] ?></td>
                                                <td style="width: 115px;"><?php echo $row_consulta['Nombre_ins'] . " " . $row_consulta['ApellidoP_ins'] . " " . $row_consulta['ApellidoM_ins'] ?></td>
                                                <td><?php echo $oferta->getNombre_oferta(); ?></td>
                                                <td><?php echo $esp->getNombre_esp(); ?></td>
                                                <td><?php echo $nombre_ori; ?></td>
                                                <td><?php echo $ciclo->getClave(); ?></td>
                                                <td><?php echo $row_consulta['DateCreated']; ?></td>
                                                <td><input type="checkbox"> </td>
                                              </tr>
                                              <?php
 
                                       }while($row_consulta = $consulta->fetch_assoc());
                                    }
                             }
                             ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="boxfil">
	<h1>Filtros</h1>
	<p>Oferta<br>
            <select id="oferta" onchange="update_curso_box_curso()">
              <option value="0"></option>
              <?php
              foreach($DaoOfertas->showAll() as $k=>$v){
              ?>
                  <option value="<?php echo $v->getId() ?>"> <?php echo $v->getNombre_oferta() ?> </option>
              <?php
              }
              ?>
            </select>
	</p>
        <p>Especialidad:<br>
            <select id="curso" onchange="update_orientacion_box_curso();update_grados_ofe()">
              <option value="0"></option>
            </select>
        </p>
        <div id="box_orientacion"></div>
        <p>Ciclo<br>
          <select id="ciclo">
              <option value="0"></option>
              <?php
              foreach($DaoCiclos->showAll() as $k=>$v){
              ?>
               <option value="<?php echo $v->getId() ?>"><?php echo $v->getClave(); ?></option>
              <?php
              }
              ?>
            </select>
        </p>
        <p><button onclick="filtro(this)">Buscar</button><button onclick="ocultar_filtro()">Cancelar</button></p>
</div>
<script src="js/ckeditor/ckeditor.js"></script>
<input type="file" id="files" name="files" multiple="">
<?php
write_footer();
