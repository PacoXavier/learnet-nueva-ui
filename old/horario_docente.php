<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

if(!isset($perm['48'])){
  header('Location: home.php');
}

$DaoDocentes= new DaoDocentes();
$DaoHoras= new DaoHoras();
$DaoCiclos= new DaoCiclos();
$DaoDiasPlantel= new DaoDiasPlantel();

links_head("Horario Docente  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-clock-o" aria-hidden="true"></i> Horario del docente</h1>
                </div>
                <div class="seccion" id="box_info_horario">
                    <div id="box_uno">
                        <?php
                        $Id_docen=0;
                        if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                            $docente = $DaoDocentes->show($_REQUEST['id']);
                            $nombre = $docente->getNombre_docen() . " " . $docente->getApellidoP_docen() . " " . $docente->getApellidoM_docen();
                            $Id_docen=$docente->getId();
                        }
                        ?>
                        <ul class="form">
                            <li>Docente<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Nombre o clave" value="<?php echo $nombre ?>"/>
                                <ul id="buscador_int"></ul>
                            </li>
                            <?php
                            if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                                ?>
                                <li>Ciclo<br><select id="ciclo" onchange="mostrar_horario()">
                                        <option value="0">Selecciona el ciclo</option>
                                        <?php
                                        foreach($DaoCiclos->showAll() as $ciclo){
                                        ?>
                                         <option value="<?php echo $ciclo->getId(); ?>"><?php echo $ciclo->getClave() ?></option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                </li> 
                            <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <table id="tabla_horario">
                            <thead>
                                <tr>
                                    <td>Sesiones</td>
                                    <?php
                                    foreach($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia){
                                        ?>
                                          <td><?php echo $dia['Nombre']?></td>
                                        <?php
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                                  foreach($DaoHoras->showAll() as $hora){    
                                 ?>
                                    <tr id_hora="<?php echo $hora->getId()?>">
                                        <td><?php echo $hora->getTexto_hora()?></td>
                                        <?php
                                        foreach($DaoDiasPlantel->getJoinDiasPlantel($_usu->getId_plantel()) as $dia){
                                            ?>
                                              <td><span></span><br></td>
                                            <?php
                                        }
                                        ?>
                                    </tr>
                                 <?php
                                 }
                                 ?>
                            </tbody>
                        </table>
                </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_docente" value="<?php echo $Id_docen; ?>"/>
<?php
write_footer();

