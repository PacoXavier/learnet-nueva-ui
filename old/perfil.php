<?php
require_once('estandares/includes.php');
require_once('require_daos.php'); 

if($_REQUEST['id']!=$_COOKIE['admin/Id_usu']){
  header('Location: home.php');
}

links_head("Mi perfil  ");
write_head_body();
write_body();

$Daousuarios= new DaoUsuarios();
$DaoPlanteles= new DaoPlanteles();
$DaoTiposUsuarios= new DaoTiposUsuarios();
$DaoContactos= new DaoContactos();

$Id_usu="";
$Img_usu="";
$Nombre_usu="";
$ApellidoP_usu="";
$IApellidoM_usu="";
$Clave_usu="";
$Email_usu="";
$Tel_usu="";
$Cel_usu="";
$Nivel_estudios="";
$Id_plantel="";
$Tipo_usu="";
$Tipo_sangre="";
$Alergias="";
$Enfermedades_cronicas="";
$Preinscripciones_medicas="";

$ContactoNombre="";
$ContactoParentesco="";
$ContactoDireccion="";
$ContactoTel_casa="";
$ContactoTel_ofi="";
$ContactoCel="";
$ContactoEmail="";
$ContactoId_cont="";
        
?>
<table id="tabla">
    <?php
    if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
        $usu_sis = $Daousuarios->show($_REQUEST['id']);
        $Id_usu=$usu_sis->getId();
        $Img_usu=$usu_sis->getImg_usu();
        $Nombre_usu=$usu_sis->getNombre_usu();
        $ApellidoP_usu=$usu_sis->getApellidoP_usu();
        $ApellidoM_usu=$usu_sis->getApellidoM_usu();
        $Clave_usu=$usu_sis->getClave_usu();
        $Email_usu=$usu_sis->getEmail_usu();
        $Tel_usu=$usu_sis->getTel_usu();
        $Cel_usu=$usu_sis->getCel_usu();
        $Nivel_estudios=$usu_sis->getNivel_estudios();
        $Id_plantel=$usu_sis->getId_plantel();
        $Tipo_usu=$usu_sis->getTipo_usu();
        $Tipo_sangre=$usu_sis->getTipo_sangre();
        $Alergias=$usu_sis->getAlergias();
        $Enfermedades_cronicas=$usu_sis->getEnfermedades_cronicas();
        $Preinscripciones_medicas=$usu_sis->getPreinscripciones_medicas();
        
        $contacto=$DaoContactos->getPrimerContacto($Id_usu,'usu');
        $ContactoNombre=$contacto->getNombre();
        $ContactoParentesco=$contacto->getParentesco();
        $ContactoDireccion=$contacto->getDireccion();
        $ContactoTel_casa=$contacto->getTel_casa();
        $ContactoTel_ofi=$contacto->getTel_ofi();
        $ContactoCel=$contacto->getCel();
        $ContactoEmail=$contacto->getEmail();
        $ContactoId_cont=$contacto->getId();
    }
    ?>
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <div class="foto_alumno" 
                    <?php if (strlen($Img_usu) > 0) { ?> 
                             style="background-image:url(files/<?php echo $Img_usu ?>.jpg) <?php } ?>">
                    </div>
                    <h1><?php echo ucwords(strtolower($Nombre_usu . " " . $ApellidoP_usu . " " . $ApellidoM_usu)) ?></h1>
                </div>
                <div class="seccion">
                    <h2>Datos Personales</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Clave<br><input type="text" value="<?php echo $Clave_usu ?>" id="clave_usu"/></li>
                        <li>Nombre<br><input type="text" value="<?php echo $Nombre_usu ?>" id="nombre_usu"/></li>
                        <li>Apellido Paterno<br><input type="text" value="<?php echo $ApellidoP_usu ?>" id="apellidoP_usu"/></li>
                        <li>Apellido Materno<br><input type="text" value="<?php echo $ApellidoM_usu ?>" id="apellidoM_usu"/></li>
                        <li>Email<br><input type="email" id="email_usu" value="<?php echo $Email_usu ?>"/></li>
                        <li>Tel&eacute;fono Casa<br><input type="tel" id="tel_usu" value="<?php echo $Tel_usu ?>"/></li>
                        <li>Tel&eacute;fono Movil<br><input type="tel" id="cel_usu" value="<?php echo $Cel_usu ?>"/></li>
                        <li>Ultimo Grado de Estudios<br>
                            <select id="escolaridad">
                                <option value="0"></option>
                                <option value="1" <?php if ($Nivel_estudios == 1) { ?> selected="selected"<?php } ?>>Primaria</option>
                                <option value="2" <?php if ($Nivel_estudios == 2) { ?> selected="selected"<?php } ?>>Secundaria</option>
                                <option value="3" <?php if ($Nivel_estudios == 3) { ?> selected="selected"<?php } ?>>Preparatoria</option>
                                <option value="4" <?php if ($Nivel_estudios == 4) { ?> selected="selected"<?php } ?>>Universidad</option>
                                <option value="5" <?php if ($Nivel_estudios == 5) { ?> selected="selected"<?php } ?>>Posgrado</option>
                            </select>
                        </li>
                        <li>Plantel<br>
                            <select id="plantel">
                                <option value="0"></option>
                                <?php
                                foreach($DaoPlanteles->showAll() as $plantel){
                                        ?>
                                        <option value="<?php echo $plantel->getId() ?>" <?php  if($plantel->getId()==$Id_plantel){ ?> selected="selected" <?php } ?> ><?php echo $plantel->getNombre_plantel() ?></option>
                                       <?php
                                }
                                ?>
                            </select>
                        </li>
                        <li>Tipo de usuario<br>
                            <select id="tipo_usu">
                                <option value="0"></option>
                                <?php
                                foreach($DaoTiposUsuarios->getTiposUsuarios() as $tipo){
                                    ?>
                                    <option value="<?php echo $tipo->getId() ?>" <?php  if($tipo->getId()==$Tipo_usu){ ?> selected="selected" <?php } ?> ><?php echo $tipo->getNombre_tipo() ?></option>
                                    <?php    
                                }
                                ?>
                            </select>
                        </li>
                    </ul>
                </div>
                <div class="seccion">
                    <h2>Datos de Emergencia</h2>
                    <span class="linea"></span>
                    <ul class="form">
                        <li>Tipo de sangre<br><input type="text"  id="tipo_sangre" value="<?php echo $Tipo_sangre ?>"/></li>
                        <li>Nombre del contacto<br><input type="text" id="nombre_contacto" value="<?php echo $ContactoNombre ?>"/></li>
                        <li>Parentesco<br><input type="text" id="parentesco_contacto" value="<?php echo $ContactoParentesco ?>"/></li>
                        <li>Direcci&oacute;n<br><input type="text" id="direccion_contacto" value="<?php echo $ContactoDireccion ?>"/></li>
                        <li>Tel&eacute;fono Casa<br><input type="text" id="tel_contacto" value="<?php echo $ContactoTel_casa ?>"/></li>
                        <li>Tel&eacute;fono Oficina<br><input type="text" id="telOfic_contacto" value="<?php echo $ContactoTel_ofi ?>"/></li>
                        <li>Celular<br><input type="text" id="celular_contacto" value="<?php echo $ContactoCel ?>"/></li>
                        <li>Email<br><input type="text" id="email_contacto" value="<?php echo $ContactoEmail ?>"/></li>
                    </ul>
                    <input type="hidden" id="Id_contacto" value="<?php echo $ContactoId_cont ?>"/>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <li>Alergias<br><textarea  id="alergias_contacto"><?php echo $Alergias ?></textarea></li>
                        <li>Enfermedades cr&oacute;nicas<br><textarea  id="cronicas_contacto"><?php echo $Enfermedades_cronicas ?></textarea></li>
                        <li>Pre-escripciones medicas<br><textarea  id="preinscripciones_medicas_contacto"><?php echo $Preinscripciones_medicas ?></textarea></li>
                    </ul>
                </div>
                <button class="btns btns-primary btns-lg" id="button_ins" onclick="save_usu()">Guardar</button>
            </div>
        </td>
        <td id="column_two">
            <div id="box_menus">
                <?php
                require_once 'estandares/menu_derecho.php';
                ?>
                <ul>
                    <li><a href="usuario.php" class="link"><i class="fa fa-user-plus"></i> Nuevo</a></li>
                    <?php
                    if ($Id_usu > 0) {
                     
                        ?>
                        <li><span onclick="box_pass()"><i class="fa fa-ellipsis-h"></i> Cambiar contrase&ntilde;a</span></li>
                        <li><span onclick="mostrarFinder()"><i class="fa fa-camera"></i> A&ntilde;adir fotografia</span></li>
                        <?php
                    }
                    ?>
                </ul>
                <input type="file" id="files" name="files" multiple="">
            </div>
        </td>
    </tr>
</table>
<input type="hidden" id="Id_usu" value="<?php echo $Id_usu ?>"/>
<?php
write_footer();
