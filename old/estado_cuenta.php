<?php
require_once('estandares/includes.php');
require_once('clases/DaoOfertasAlumno.php');
require_once('clases/DaoOfertas.php');
require_once('clases/DaoAlumnos.php');
require_once('clases/DaoEspecialidades.php');
require_once('clases/DaoOrientaciones.php');
require_once('clases/DaoPagosCiclo.php');
require_once('clases/DaoCiclosAlumno.php');
require_once('clases/DaoCiclos.php');
require_once('clases/DaoDocentes.php');
require_once('clases/DaoRecargos.php');
require_once('clases/DaoPagos.php');
require_once('clases/DaoMetodosPago.php');

$DaoPagosCiclo= new DaoPagosCiclo();
$DaoDocentes= new DaoDocentes();
$DaoCiclos= new DaoCiclos();
$DaoRecargos= new DaoRecargos();
$DaoPagos= new DaoPagos();
$DaoMetodosPago= new DaoMetodosPago();


links_head("Estado de cuenta  ");
write_head_body();
write_body();
?>
<table id="tabla">
    <tr>
        <td id="column_one">
            <div class="fondo">
                <div id="box_top">
                    <h1><i class="fa fa-university"></i> Estado de cuenta</h1>
                </div>
                <div class="seccion">
                    <ul class="form">
                        <li>Buscar<br><input type="text" id="email_int"  onkeyup="buscar_int()" placeholder="Usuario u docente"/>
                          <ul id="buscador_int"></ul>
                        </li>
                    </ul>
                </div>
              <?php
              if ($_REQUEST['id'] > 0) {
                 if($_REQUEST['t']=="u"){
                     $usuario = $DaoUsuarios->show($_REQUEST['id']);
                     $tipo="usu";
                     $texto="Usuario";
                     $nombre=$usuario->getNombre_usu()." ".$usuario->getApellidoP_usu()." ".$usuario->getApellidoM_usu();
                     $codigo=$usuario->getClave_usu();
                     $ingreso=$usuario->getAlta_usu();
                  }elseif($_REQUEST['t']=="d"){
                     $usuario = $DaoDocentes->show($_REQUEST['id']);
                     $tipo="docen";
                     $texto="Docente";
                     $nombre=$usuario->getNombre_docen()." ".$usuario->getApellidoP_docen()." ".$usuario->getApellidoM_docen(); 
                     $codigo=$usuario->getClave_docen();
                     $ingreso=$usuario->getAlta_docent();
                  }
                  $SaldoPagos = $DaoPagos->getTotalPagosUsuario($_REQUEST['id'],$tipo);
                ?>
                <div class="seccion">
                    <table  id="info_alumn">
                        <tbody><tr><th style="padding-bottom: 15px;text-align: left;">
                                    <font size="3">DATOS DEL <?php echo strtoupper ($texto)?></font>
                                </th></tr>
                            <tr><td>
                                    <table width="100%">
                                        <tbody><tr>
                                                <th ><font>C&oacute;digo:</font></th>
                                                <td><font><?php echo $codigo ?></font></td>
                                                <th ><font>Nombre:</font></th>
                                                <td colspan="5"><font><?php echo $nombre ?></font></td>
                                            </tr>
                                            <tr>
                                                <th><font>Ingreso:</font></th>
                                                <td><font><?php echo $ingreso; ?></font></td>
         
                                            </tr>
                                        </tbody></table>
                                </td></tr>
                        </tbody>
                    </table>
                </div>
                <div class="seccion" id="list_ofertas">
                    <table id="estado-cuenta">
                    <tr>
                        <td id="seccion-cargos">
                            <button id="add_pago_miscelaneo" class="boton-normal" onclick="showBoxCargo()"><i class="fa fa-plus-square"></i> A&ntilde;adir cargo</button>
                            <!--<button id="buttonBancario" class="boton-normal" onclick="send_email_bancario()"><i class="fa fa-paper-plane"></i> Enviar datos bancarios</button>-->
                            <?php
                            $totalCargosOferta = 0;
                            $totalDescuentosOferta = 0;
                                ?>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td colspan="9" class="clave-ciclo">Cargos del <?php echo $texto;?></td>
                                        </tr>
                                        <tr>
                                            <td class="td-center">#</td>
                                            <td>Concepto</td>
                                            <td class="td-center" width="65px;">Fecha límite</td>
                                            <td class="td-center">Monto</td>
                                            <td class="td-center">Descuento</td>
                                            <td class="td-center">Aplica Desct.</td>
                                            <td class="td-center">Cantidad pagada</td>
                                            <td class="td-center">Adeudo</td>
                                            <td class="td-center"></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $mensualidad = 0;
                                        $Descuento = 0;
                                        $Adeudo = 0;
                                        $totalAdeudo = 0;
                                        $totalrecargos = 0;
                                        $totalmiscelaneos = 0;
                                        $totalPagado = 0;
                                        foreach ($DaoPagosCiclo->getCargosUsuario($_REQUEST['id'],$tipo) as $cargo) {
                                            //Recargos
                                            $recargos = 0;
                                            foreach ($DaoRecargos->getRecargosPago($cargo->getId()) as $recargo) {
                                                $recargos+=$recargo->getMonto_recargo();
                                            }
                                            
                                            $mensualidad+= $cargo->getMensualidad() + $recargos;
                                            $Descuento+=$cargo->getDescuento();

                                            $TotalCargo = round($cargo->getMensualidad() - $cargo->getDescuento() - $cargo->getDescuento_beca() + $recargos);
                                            $Adeudo = $TotalCargo;
                                            $MontoPagado = 0;
                                            if ($SaldoPagos > 0) {
                                                $MontoPagado = $TotalCargo;

                                                if ($SaldoPagos >= $TotalCargo) {
                                                    $SaldoPagos = $SaldoPagos - $TotalCargo;
                                                    $Adeudo = 0;
                                                } elseif ($SaldoPagos < $TotalCargo) {
                                                    $MontoPagado = $SaldoPagos;
                                                    $Adeudo = $TotalCargo - $MontoPagado;
                                                    $SaldoPagos = 0;
                                                }
                                            }
                                            $totalPagado+=$MontoPagado;
                                            $totalAdeudo+=$Adeudo;
                                            $totalrecargos+=$recargos;

                                            //Fondo
                                            $classAdeudo = "";
                                            //Esconder boton editar fecha y adeudos
                                            $style_edit = '';
                                            if ($Adeudo <= 0) {
                                                $style_edit = 'style="visibility: hidden"';
                                                $classAdeudo = 'adeudo';
                                                $cargo->setPagado(1);
                                                $DaoPagosCiclo->update($cargo);
                                            }else{
                                                $cargo->setPagado(0);
                                                $DaoPagosCiclo->update($cargo);  
                                            }

                                            $NombreUsuDescuento="";
                                            if($cargo->getUsuCapturaDescuento()>0){
                                            $aplicaDescuento=$DaoUsuarios->show($cargo->getUsuCapturaDescuento());
                                            $NombreUsuDescuento=$aplicaDescuento->getNombre_usu()." ".$aplicaDescuento->getApellidoP_usu();
                                            }
                                            ?>
                                            <tr  class="<?php echo $classAdeudo; ?>" id-pago="<?php echo $cargo->getId() ?>">
                                                <td class="td-center"><?php echo $i + 1 ?></td>
                                                <td><?php echo $cargo->getConcepto() ?></td>
                                                <td class="td-center"><?php echo $cargo->getFecha_pago() ?> <i class="fa fa-pencil-square-o" onclick="mostrar_fecha_pago(<?php echo $cargo->getId() ?>,<?php echo $i + 1 ?>)" title="Editar fecha limite de pago"></i></td>
                                                <td class="td-center"><?php echo "$" . number_format($cargo->getMensualidad(), 2) ?></td>
                                                <td class="td-center"><?php echo "$" . number_format($cargo->getDescuento(), 2) ?> 
                                                    <?php
                                                    if (isset($perm['37'])) {
                                                        ?>
                                                    <i class="fa fa-pencil-square-o" onclick="mostrar_descuento(<?php echo $cargo->getId() ?>,<?php echo $i + 1 ?>)" title="Editar descuento"></i>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo $NombreUsuDescuento;?></td>
                                                <td class="td-center"><?php echo "$" . number_format($MontoPagado, 2) ?></td>
                                                <td class="td-center"><?php echo "$" . number_format($Adeudo, 2) ?></td>
                                                <td class="right">
                                                    <i class="fa fa-plus-square" onclick="show_box_recargo(<?php echo $cargo->getId(); ?>)" title="Añadir recargo"></i>
                                                    <?php
                                                    if (isset($perm['69'])) {
                                                        ?>
                                                        <i class="fa fa-trash" onclick="delete_cargo(<?php echo $cargo->getId(); ?>)" title="Eliminar cargo"></i>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                            $query = "SELECT * FROM Recargos_pago WHERE Id_pago=" . $cargo->getId() . " AND Fecha_recargo<='" . date('Y-m-d') . "' ORDER BY Fecha_recargo ASC";
                                            foreach ($DaoRecargos->advanced_query($query) as $recargo) {
                                                ?>
                                            
                                                <tr class="recargo">
                                                    <td></td>
                                                    <td>Recargo</td>
                                                    <td class="td-center"><?php echo $recargo->getFecha_recargo() ?> <i class="fa fa-pencil-square-o" onclick="mostrar_fecha_recargo(<?php echo $recargo->getId() ?>)"></i> </td>
                                                    <td class="td-center">$<?php echo number_format($recargo->getMonto_recargo(), 2) ?></td>
                                                    <td colspan="4"></td>
                                                    <td class="right">
                                                        <?php
                                                        //if ($cargo->getCantidad_Pagada() < $Adeudo) {
                                                            if (isset($perm['38'])) {
                                                                ?>
                                                                <i class="fa fa-trash" onclick="delete_recargo(<?php echo $recargo->getId() ?>)" title="Eliminar recargo"></i>
                                                                <?php
                                                            }
                                                        //}
                                                        ?>

                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            $i++;
                                        }
                                        $totalCargosOferta+=$mensualidad;
                                        $totalDescuentosOferta+=$Descuento;
                                        ?>
                                            <tr>
                                                <td colspan="9"></td>
                                            </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3"><b>Total</b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($mensualidad), 2) ?></b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($Descuento), 2) ?></b></td>
                                            <td></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($totalPagado), 2) ?></b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($totalAdeudo), 2) ?></b></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b>Total Regargos</b></td>
                                            <td class="td-center"><b><?php echo "$" . number_format(round($totalrecargos), 2) ?></b></td>
                                            <td class="td-center" colspan="7"></td>
                                        </tr>
                                    </tfoot>
                                </table>
                        </td>
                        <td id="seccion-pagos">
                            <button class="boton-normal" id="mostrar-box-pago" onclick="mostrar_pago()"><i class="fa fa-plus-square"></i> Añadir pago</button>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td colspan="9" class="clave-ciclo">Historial de pagos</td>
                                    </tr>
                                    <tr>

                                        <td class="td-center">Folio</td>
                                        <td class="td-center">Fecha de captura</td>
                                        <td class="td-center">Fecha de pago</td>
                                        <td>Concepto</td>
                                        <td class="td-center">Monto</td>
                                        <td class="td-center">Método de pago</td>
                                        <td class="td-center">Usuario</td>
                                        <td>Comentarios</td>
                                        <td class="td-center">Acciones</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $totalPagado = 0;
                                    $i = 1;
                                    foreach ($DaoPagos->getPagosUsuario($_REQUEST['id'],$tipo) as $pago) {
                                        $usuCapturo = $DaoUsuarios->show($pago->getId_usu());
                                        $metodo = $DaoMetodosPago->show($pago->getMetodo_pago());
                                        $totalPagado+=$pago->getMonto_pp();
                                        $ciclo=$DaoCiclos->getCicloPorFecha($pago->getFecha_captura());
                                        ?>
                                        <tr  id-pago="<?php echo $pago->getId() ?>">
                                            <td class="td-center"><?php echo $pago->getFolio() ?></td>
                                            <td class="td-center"><?php echo $pago->getFecha_captura() ?></td>
                                            <td class="td-center"><?php echo $pago->getFecha_pp() ?></td>
                                            <td><?php echo $pago->getConcepto()?></td>
                                            <td class="td-center"><?php echo "$" . number_format($pago->getMonto_pp(), 2) ?></td>
                                            <td class="td-center"><?php echo $metodo->getNombre() ?></td>
                                            <td class="td-center"><?php echo $usuCapturo->getNombre_usu() . " " . $usuCapturo->getApellidoP_usu() ?></td>
                                            <td><?php echo $pago->getComentarios() ?></td>
                                            <td class="menu td-center">
                                                <i class="fa fa-pencil-square-o" onclick="mostrar_pago(<?php echo $pago->getId()?>)" title="Editar pago"></i>
                                                <?php
                                                if (isset($perm['69'])) {
                                                    ?>
                                                    <i class="fa fa-trash" onclick="delete_pago(<?php echo $pago->getId(); ?>)" title="Eliminar pago"></i>
                                                    <?php
                                                }
                                                ?>
                                                <i class="fa fa-file-pdf-o" onclick="mostrar_recibo(<?php echo $pago->getId(); ?>,<?php echo $ciclo->getId(); ?>)" title="Mostrar recibo"></i>
                                                <i class="fa fa-paper-plane" onclick="send_recibo(<?php echo $pago->getId() ?>,<?php echo $ciclo->getId(); ?>, this)"title="Enviar recibo"></i>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td colspan="8" class="clave-ciclo">Totales</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="3"><b>Total cargos</b></td>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($totalCargosOferta), 2) ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Total pagado</b></td>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($totalPagado), 2) ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Total descuento</b></td>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($totalDescuentosOferta), 2) ?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><b>Adeudo</b></td>
                                        <?php
                                        $z = ($totalCargosOferta - $totalPagado - $totalDescuentosOferta)
                                        ?>
                                        <td class="td-center"colspan="5"><b><?php echo "$" . number_format(round($z), 2) ?></b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
                </div>
              <?php
              }
              ?>
            </div>
        </td>
        <td id="column_two">
           
        </td>
    </tr>
</table>
<input type="hidden" id="Id_usu" value="<?php echo $_REQUEST['id']?>"/>
<input type="hidden" id="TipoUsu" value="<?php echo $tipo?>"/>
<?php
write_footer();
